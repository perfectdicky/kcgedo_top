--覇王龍ズァーク
local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	Pendulum.AddProcedure(c,false)
	Link.AddProcedure(c,nil,2,99,s.matfilter)
	
  -- Level/Rank
	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_SET_AVAILABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_LEVEL_RANK_LINK)
	c:RegisterEffect(e0)
	
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_SET_AVAILABLE)
	e1:SetCode(EFFECT_CHANGE_LEVEL)
	e1:SetValue(13)
	c:RegisterEffect(e1)
	local e17=e1:Clone()
	e17:SetCode(EFFECT_CHANGE_RANK)
	c:RegisterEffect(e17)
	
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE+LOCATION_PZONE)
	e2:SetCode(EFFECT_CHANGE_CODE)
	e2:SetValue(13331639)
	c:RegisterEffect(e2)
	local e24=e2:Clone()
	e24:SetRange(LOCATION_MZONE)
	e24:SetCode(EFFECT_ADD_ATTRIBUTE)
	e24:SetValue(0x3f)
	c:RegisterEffect(e24)
	local e25=e24:Clone()
	e25:SetCode(EFFECT_ADD_RACE)
	e25:SetValue(RACE_DRAGON)
	c:RegisterEffect(e25)
			
	--special summon
	local e21=Effect.CreateEffect(c)
	e21:SetType(EFFECT_TYPE_FIELD)
	e21:SetCode(EFFECT_SPSUMMON_PROC)
	e21:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e21:SetRange(LOCATION_PZONE)
	e21:SetCondition(s.sppcon)
	e21:SetOperation(s.sppop)
	c:RegisterEffect(e21)
	--spsummon immune
	local e021=Effect.CreateEffect(c)
	e021:SetType(EFFECT_TYPE_SINGLE)
	e021:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e021:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e021)
	
	--destroy all
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(13331639,1))
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)   
	e4:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e4:SetCode(EVENT_SPSUMMON_SUCCESS)
	e4:SetTarget(s.destg)
	e4:SetOperation(s.desop)
	c:RegisterEffect(e4)
	
	--immune
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e5:SetCode(EFFECT_IMMUNE_EFFECT)
	e5:SetRange(LOCATION_MZONE)
	e5:SetTargetRange(LOCATION_MZONE,0)
	e5:SetValue(s.efilter)
	c:RegisterEffect(e5)	
	
	local e117 = Effect.CreateEffect(c)
	e117:SetType(EFFECT_TYPE_FIELD)
	e117:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e117:SetCode(EFFECT_CANNOT_RELEASE)
	e117:SetRange(LOCATION_MZONE)
	e117:SetTargetRange(0, 1)
	e117:SetTarget(s.rellimit)
	c:RegisterEffect(e117)

	local e100 = Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e112 = e100:Clone()
	e112:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e112)
	local e102 = e100:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103 = e100:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104 = e100:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e105 = e100:Clone()
	e105:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e105)
	local e106 = e100:Clone()
	e106:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	c:RegisterEffect(e106)
   -- local e107 = e100:Clone()
	--e107:SetCode(EFFECT_CANNOT_TURN_SET)
   -- c:RegisterEffect(e107)
	local e108 = e100:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109 = e100:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110 = e100:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e111 = e109:Clone()
	e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e111)

	
	--special summon
	local e7=Effect.CreateEffect(c)
	e7:SetDescription(aux.Stringid(13331639,2))
	e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DELAY)   
	e7:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e7:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e7:SetCode(EVENT_BATTLE_DESTROYING)
	e7:SetCondition(aux.bdocon)
	e7:SetTarget(s.sptg)
	e7:SetOperation(s.spop)
	c:RegisterEffect(e7)
	--Unaffected by Opponent Card Effects
	local e50=Effect.CreateEffect(c)
	e50:SetType(EFFECT_TYPE_SINGLE)
	e50:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e50:SetRange(LOCATION_MZONE)
	e50:SetCode(EFFECT_IMMUNE_EFFECT)
	e50:SetValue(s.unval)
	c:RegisterEffect(e50)
	--handes
	local e20=Effect.CreateEffect(c)
	e20:SetDescription(aux.Stringid(48739166,0))
	e20:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e20:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	e20:SetCode(EVENT_TO_HAND)
	e20:SetRange(LOCATION_MZONE)
	e20:SetCondition(s.hdcon)
	e20:SetTarget(s.hdtg)
	e20:SetOperation(s.hdop)
	c:RegisterEffect(e20)
	
	--pendulum
	local e8=Effect.CreateEffect(c)
	e8:SetDescription(aux.Stringid(13331639,3))
	e8:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e8:SetCode(EVENT_LEAVE_FIELD)
	e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DELAY)
	e8:SetCondition(s.pencon)
	--e8:SetTarget(s.pentg)
	e8:SetOperation(s.penop)
	c:RegisterEffect(e8)

	--special summon
	local e26=Effect.CreateEffect(c)
	e26:SetType(EFFECT_TYPE_FIELD)
	e26:SetCode(EFFECT_SPSUMMON_PROC)
	e26:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e26:SetRange(LOCATION_EXTRA)
	e26:SetCondition(s.spcon)
	e26:SetOperation(s.spop2)
	c:RegisterEffect(e26)

	--SpecialSummon
	local e27=Effect.CreateEffect(c)
	e27:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e27:SetDescription(aux.Stringid(id,0))
	e27:SetType(EFFECT_TYPE_IGNITION)
	e27:SetCountLimit(1)
	e27:SetRange(LOCATION_MZONE)
	e27:SetCost(s.ctcost)
	e27:SetTarget(s.cttg)
	e27:SetOperation(s.ctop)
	c:RegisterEffect(e27)
end
s.listed_series={0x20f8}
s.listed_names={48}

function s.matfilter(g,lc)
	local xg=g:Filter(Card.IsType,nil,TYPE_EXTRA+TYPE_PENDULUM+TYPE_RITUAL)
	return xg:GetClassCount(Card.GetType)>=6
end
function s.rellimit(e, c, tp, sumtp)
	return c == e:GetHandler()
end
function s.unval(e,te)
	return te:GetOwnerPlayer()~=e:GetHandlerPlayer()
end
function s.sppfilter(c)
	return c:IsFaceup() and c:IsReleasable() and c:IsSetCard(0xf8) or c:IsSetCard(0x10f8) or c:IsSetCard(0x20f8)
end
function s.sppcon(e,c)
	if c==nil then return true end
	return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>-1
		and e:GetHandler():IsFaceup()
		and Duel.IsExistingMatchingCard(s.sppfilter,c:GetControler(),LOCATION_HAND+LOCATION_MZONE,0,1,nil)
end
function s.sppop(e,tp,eg,ep,ev,re,r,rp,c)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_RELEASE)
	local g=Duel.SelectMatchingCard(tp,s.sppfilter,tp,LOCATION_HAND+LOCATION_MZONE,0,1,1,nil)
	Duel.SendtoGrave(g,REASON_COST)
end

function s.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=Duel.GetFieldGroup(tp,0,LOCATION_MZONE)
	if chk==0 then return g:GetCount()>0 end
	local tc=g:GetFirst()
	local tatk=0
	while tc do
	local atk=tc:GetAttack()
	if atk<0 then atk=0 end
	tatk=tatk+atk
	tc=g:GetNext() end  
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,tatk)	
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetFieldGroup(tp,0,LOCATION_MZONE)
	if g:GetCount()>0 then
		Duel.Destroy(g,REASON_EFFECT)
		local g2=Duel.GetOperatedGroup()
		Duel.BreakEffect()
		local tc=g2:GetFirst()
		local tatk=0
		while tc do
		local atk=tc:GetPreviousAttackOnField()
		if atk<0 then atk=0 end
		tatk=tatk+atk
		tc=g2:GetNext() end 
		Duel.Damage(1-tp,tatk,REASON_EFFECT)		
	end
end

--Immune
function s.efilter(e,te)
	local tc=te:GetHandler()
	return te:GetOwnerPlayer()~=e:GetHandlerPlayer() 
	and (tc:IsType(TYPE_XYZ+TYPE_FUSION+TYPE_SYNCHRO+TYPE_PENDULUM+TYPE_RITUAL+TYPE_LINK))
end

function s.ndcfilter(c)
	return c:IsFaceup() 
	and (c:IsType(TYPE_XYZ+TYPE_FUSION+TYPE_SYNCHRO+TYPE_PENDULUM+TYPE_RITUAL+TYPE_LINK))
end

function s.indfilter(c,tpe)
	return (c:IsLocation(LOCATION_GRAVE) or c:IsFaceup()) and c:IsType(tpe)
end
function s.indcon(e)
	return Duel.IsExistingMatchingCard(s.indfilter,0,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,nil,TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ+TYPE_PENDULUM+TYPE_RITUAL+TYPE_LINK)
end

function s.imfilter2(e,te)
	if not te then return false end
	return not (te:GetHandler():IsLocation(LOCATION_EXTRA) and te:GetCode()==EFFECT_SPSUMMON_PROC) and e:GetOwner()~=te:GetOwner()
end

function s.spfilter(c,e,tp)
	if not c:IsCanBeSpecialSummoned(e,0,tp,false,false) or not c:IsSetCard(0x20f8) then return false end
	local type=TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ
	if c:IsType(TYPE_PENDULUM+TYPE_LINK) then type=TYPE_PENDULUM+TYPE_LINK end
	if not c:IsLocation(LOCATION_EXTRA) then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
	else return Duel.GetLocationCountFromEx(tp,tp,nil,type)>0 end
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=Duel.GetMatchingGroup(s.spfilter,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA,0,nil,e,tp)
	if chk==0 then return g:GetCount()>0 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,2,0,0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
	local g=Duel.GetMatchingGroup(s.spfilter, tp, LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA, 0, nil, e, tp)
	local ft=math.min(2,Duel.GetUsableMZoneCount(tp))
	if #g>0 then
		local rg=Group.CreateGroup()
		for i=1,ft do
			local ft1=Duel.GetLocationCountFromEx(tp)
			local ft2=Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ)
			local ft3=Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_PENDULUM+TYPE_LINK)
			if Duel.IsPlayerAffectedByEffect(tp,CARD_BLUEEYES_SPIRIT) then
				if ft1>0 then ft1=1 end
				if ft2>0 then ft2=1 end
				if ft3>0 then ft3=1 end
			end
			local rg2=g:FilterSelect(tp,s.rescon,1,1,nil,ft1,ft2,ft3)
			if #rg2<1 then break end
			rg:Merge(rg2)
			Duel.SpecialSummon(rg2,0,tp,tp,false,false,POS_FACEUP_DEFENSE)
			--aux.SelectUnselectGroup(g,e,tp,1,1,s.rescon(ft1,ft2,ft3,ft),1,tp,HINTMSG_SPSUMMON)
			g:Sub(rg2)
		end
		rg:ForEach(function(c) c:CompleteProcedure() end)
	end
end
function s.exfilter1(c)
	return c:IsFacedown() and c:IsType(TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ)
end
function s.exfilter2(c)
	return c:IsType(TYPE_LINK) or (c:IsFaceup() and c:IsType(TYPE_PENDULUM))
end
function s.rescon(c,ft1,ft2,ft3)
	local res=false
	if ft2>0 and ft3>0 then res=true
	elseif ft3>0 then res=c:IsType(TYPE_LINK+TYPE_PENDULUM)
	elseif ft2>0 then res=c:IsType(TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ) 
	else res=false end
	return res
end
-- function s.rescon(ft1,ft2,ft3,ft)
--  return  function(sg,e,tp,mg)
--  local exnpct=sg:FilterCount(s.exfilter1,nil)
--  local expct=sg:FilterCount(s.exfilter2,nil)
--  local groupcount=#sg
--  --local classcount=sg:GetClassCount(Card.GetCode)
--  local res=ft2>=exnpct and ft3>=expct and ft>=groupcount
--  --and classcount==groupcount
--  return res, not res
--  end
-- end

function s.cfilter(c,tp)
	return c:IsControler(tp) and c:IsPreviousLocation(LOCATION_DECK)
end
function s.hdcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetCurrentPhase()~=PHASE_DRAW and eg:IsExists(s.cfilter,1,nil,1-tp)
end
function s.hdtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsRelateToEffect(e) and e:GetHandler():IsFaceup() end
	--Duel.SetOperationInfo(0,CATEGORY_DESTROY,eg,eg:GetCount(),tp,LOCATION_HAND)
end
function s.hdop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Overlay(e:GetHandler(), eg)
end

function s.pencon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return c:IsPreviousLocation(LOCATION_MZONE) and c:IsFaceup()
end
function s.pentg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckLocation(tp,LOCATION_PZONE,0) or Duel.CheckLocation(tp,LOCATION_PZONE,1) end
end
function s.penop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) then
		Duel.SendtoHand(Duel.GetFieldGroup(tp, LOCATION_PZONE, 0), nil, REASON_EFFECT)
		Duel.BreakEffect()
		if not Duel.CheckLocation(tp,LOCATION_PZONE,0) and not Duel.CheckLocation(tp,LOCATION_PZONE,1) then return end
		Duel.MoveToField(c,tp,tp,LOCATION_PZONE,POS_FACEUP,true)
	end
end

function s.zackfilter(c)
	return c:IsRace(RACE_DRAGON) and (c:IsType(TYPE_LINK) or c:IsType(TYPE_RITUAL) ) and c:IsAbleToRemoveAsCost() and (not c:IsCode(123456) and not c:IsCode(703) and not c:IsCode(511009441))
end
function s.hofilter(c,tp,xyzc)
	if not c:IsCanBeXyzMaterial(xyzc) then return false end
	return c:IsCanBeXyzMaterial(xyzc) and (c:IsCode(703) or c:IsCode(511009441) and not c:IsCode(123456))
end
function s.spcheck(sg,e,tp)
	return aux.ChkfMMZ(1)(sg,e,tp,nil) and sg:GetClassCount(Card.GetType)==#sg
end
function s.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	local g=Duel.GetMatchingGroup(s.zackfilter,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_ONFIELD+LOCATION_GRAVE+LOCATION_EXTRA,0,e:GetHandler())
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<0 then return false end
	return aux.SelectUnselectGroup(g,e,tp,2,2,s.spcheck,0) and Duel.IsExistingMatchingCard(s.hofilter,tp,LOCATION_ONFIELD,0,1,nil,tp,c)
end
function s.spop2(e,tp,eg,ep,ev,re,r,rp,c)
	local tp=c:GetControler()
	local mg=Duel.GetMatchingGroup(s.hofilter,tp,LOCATION_ONFIELD,0,nil,tp,c)
	if mg:GetCount()<0 then return end
	c:SetMaterial(mg)
	Duel.Overlay(c, mg)
	local g=Duel.GetMatchingGroup(s.zackfilter,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_ONFIELD+LOCATION_GRAVE+LOCATION_EXTRA,0,e:GetHandler())
	local sg=aux.SelectUnselectGroup(g,e,tp,2,2,s.spcheck,1,tp,HINTMSG_REMOVE)
	Duel.Remove(sg,POS_FACEUP,REASON_COST)
end

function s.ctcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,99,REASON_COST)
	local ct=Duel.GetOperatedGroup():GetCount()
	e:SetLabel(ct)
end
function s.ctfilter1(c,e,tp)
	return (c:IsType(TYPE_FUSION) or c:IsType(TYPE_SYNCHRO) or c:IsType(TYPE_XYZ) or c:IsType(TYPE_LINK) or c:IsType(TYPE_RITUAL)) and c:IsCanBeSpecialSummoned(e,0,tp,true,true) and (not c:IsCode(123456) and not c:IsCode(703) and not c:IsCode(511009441))
end
function s.cttg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>=0
		and Duel.IsExistingMatchingCard(s.ctfilter1,tp,LOCATION_HAND+LOCATION_EXTRA,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_HAND+LOCATION_EXTRA)
end
function s.ctop(e,tp,eg,ep,ev,re,r,rp)
	local zone=e:GetHandler():GetLinkedZone(tp)&~0x60
	local ct=e:GetLabel()
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,s.ctfilter1,tp,LOCATION_HAND+LOCATION_EXTRA,0,ct,ct,nil,e,tp)
		if g:GetCount()>0 then
			local tc=g:GetFirst()
			while tc do
				if tc:IsType(TYPE_FUSION) then
				Duel.SpecialSummonStep(tc,SUMMON_TYPE_FUSION,tp,tp,true,true,POS_FACEUP,zone)
				Duel.SpecialSummonComplete()
				tc=g:GetNext()
					elseif tc:IsType(TYPE_SYNCHRO) then
						Duel.SpecialSummonStep(tc,SUMMON_TYPE_SYNCHRO,tp,tp,true,true,POS_FACEUP,zone)
						Duel.SpecialSummonComplete()
						tc=g:GetNext()
					elseif tc:IsType(TYPE_XYZ) then
						if Duel.SpecialSummonStep(tc,SUMMON_TYPE_XYZ,tp,tp,true,true,POS_FACEUP,zone) then
						Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SMATERIAL)
						local g1=Duel.SelectMatchingCard(tp,Card.IsCanBeXyzMaterial,tp,LOCATION_ONFIELD,0,1,63,e:GetHandler())
						Duel.Overlay(tc,g1)
						tc=g:GetNext()
						Duel.SpecialSummonComplete()
						end
					elseif tc:IsType(TYPE_RITUAL) then
						Duel.SpecialSummonStep(tc,SUMMON_TYPE_RITUAL,tp,tp,true,true,POS_FACEUP,zone)
						Duel.SpecialSummonComplete()
						tc=g:GetNext()
					elseif tc:IsType(TYPE_LINK) then
						Duel.SpecialSummonStep(tc,SUMMON_TYPE_LINK,tp,tp,true,true,POS_FACEUP,zone)
			Duel.SpecialSummonComplete()
			tc=g:GetNext()
			end
		end
	end
end