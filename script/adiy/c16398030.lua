-- No.62 銀河眼の光子竜皇
local s, id = GetID()
Duel.LoadScript("rankup_functions.lua")
function s.initial_effect(c)
    -- xyz summon
    Xyz.AddProcedure(c, nil, 9, 3)
    aux.EnableCheckRankUp(c, nil, nil, 31801517)
    c:EnableReviveLimit()

    -- cannot destroyed
    local e0 = Effect.CreateEffect(c)
    e0:SetType(EFFECT_TYPE_SINGLE)
    e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e0:SetValue(s.indes)
    c:RegisterEffect(e0)

    -- Level/Rank
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_FIELD)
    e8:SetRange(LOCATION_MZONE)
    e8:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
    e8:SetCode(EFFECT_LEVEL_RANK)
    e8:SetTarget(function(e, c)
        return not c:IsType(TYPE_XYZ)
    end)
    c:RegisterEffect(e8)
    local e9=e8:Clone()
    e9:SetCode(EFFECT_LINK_RANK)
    c:RegisterEffect(e9)

    local e5 = Effect.CreateEffect(c)
    e5:SetDescription(aux.Stringid(id, 0))
    e5:SetType(EFFECT_TYPE_IGNITION)
    e5:SetRange(LOCATION_MZONE)
    e5:SetCountLimit(1,id)
    e5:SetOperation(s.rankop)
    c:RegisterEffect(e5)

    -- atk
    local e1 = Effect.CreateEffect(c)
    e1:SetDescription(aux.Stringid(id, 1))
    e1:SetCategory(CATEGORY_ATKCHANGE)
    e1:SetType(EFFECT_TYPE_IGNITION)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCountLimit(1)
    e1:SetCost(s.atkcost)
    e1:SetOperation(s.atkop)
    c:RegisterEffect(e1, false, REGISTER_FLAG_DETACH_XMAT)

    -- spsummon
    local e2 = Effect.CreateEffect(c)
    e2:SetDescription(aux.Stringid(31801517, 1))
    e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DELAY)
    e2:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_F)
    e2:SetCountLimit(1, EFFECT_COUNT_CODE_DUEL)
    e2:SetCode(EVENT_LEAVE_FIELD)
    e2:SetCondition(s.spcon)
    e2:SetOperation(s.spop)
    -- e2:SetLabel(RESET_EVENT+RESETS_STANDARD)
    local e4 = Effect.CreateEffect(c)
    e4:SetType(EFFECT_TYPE_SINGLE)
    e4:SetCode(EFFECT_RANKUP_EFFECT)
    e4:SetLabelObject(e2)
    c:RegisterEffect(e4)
end
s.xyz_number = 62
s.listed_series={0x48}
s.listed_names={31801517}

function s.indes(e, c)
    return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) and
               not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and
               not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end

function s.rankop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local g = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, nil)
    local tc = g:GetFirst()
    while tc do
        if tc:GetRank() < 13 then
            local e4 = Effect.CreateEffect(c)
            e4:SetType(EFFECT_TYPE_SINGLE)
            e4:SetCode(EFFECT_UPDATE_RANK)
            e4:SetReset(RESET_EVENT + 0x1fe0000)
            if tc:GetRank() < 12 then
                e4:SetValue(2)
            else
                e4:SetValue(1)
            end
            tc:RegisterEffect(e4)
        end
        tc = g:GetNext()
    end
end

function s.atkcost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():CheckRemoveOverlayCard(tp, 1, REASON_COST)
    end
    e:GetHandler():RemoveOverlayCard(tp, 1, 1, REASON_COST)
end
function s.atkop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_UPDATE_ATTACK)
    e1:SetValue(s.val1)
    e1:SetReset(RESET_EVENT + 0x1fe0000)
    c:RegisterEffect(e1)
end
function s.val1(e, c)
    local g = Duel.GetMatchingGroup(Card.IsFaceup, e:GetHandlerPlayer(), LOCATION_MZONE, LOCATION_MZONE, nil)
    return g:GetSum(Card.GetRank) * 100
end

function s.spcon(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local ct = c:GetOverlayCount()
    e:SetLabel(ct)
    return c:IsPreviousLocation(LOCATION_MZONE) and c:GetPreviousControler() == tp and ct > 0
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local count = e:GetLabel()
    local e1 = Effect.CreateEffect(c)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e1:SetRange(LOCATION_REMOVED + LOCATION_GRAVE)
    e1:SetCode(EVENT_PHASE + PHASE_STANDBY)
    if Duel.GetCurrentPhase() == PHASE_STANDBY and Duel.GetTurnPlayer() == tp then
        e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_STANDBY + RESET_SELF_TURN, count + 1)
    else
        e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_STANDBY + RESET_SELF_TURN, count)
    end
    e1:SetLabel(count)
    e1:SetCountLimit(1)
    e1:SetOperation(s.spop2)
    c:RegisterEffect(e1)
end
function s.spop2(e, tp, eg, ep, ev, re, r, rp)
    if Duel.GetTurnPlayer() ~= tp then
        return
    end
    local c = e:GetHandler()
    local count = e:GetLabel()
    local ct = c:GetTurnCounter()
    ct = ct + 1
    c:SetTurnCounter(ct)
    if ct ~= count then
        return
    end
    Duel.SpecialSummonStep(c, 0, tp, tp, false, false, POS_FACEUP)
    local e12 = Effect.CreateEffect(c)
    e12:SetType(EFFECT_TYPE_SINGLE)
    e12:SetRange(LOCATION_MZONE)
    e12:SetValue(1)
    e12:SetCode(EFFECT_CANNOT_DISABLE)
    e12:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e12:SetReset(RESET_EVENT + 0x1ff0000 + RESET_PHASE + PHASE_END)
    c:RegisterEffect(e12)
    local e11 = Effect.CreateEffect(c)
    e11:SetType(EFFECT_TYPE_SINGLE)
    e11:SetRange(LOCATION_MZONE)
    e11:SetValue(function(e, te)
        return te:GetHandler() ~= e:GetHandler()
    end)
    e11:SetCode(EFFECT_IMMUNE_EFFECT)
    e11:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e11:SetReset(RESET_EVENT + 0x1ff0000 + RESET_PHASE + PHASE_END)
    c:RegisterEffect(e11)
    -- atk
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCode(EFFECT_SET_ATTACK)
    e1:SetValue(c:GetAttack() * count)
    e1:SetReset(RESET_EVENT + 0x1ff0000 + RESET_PHASE + PHASE_END)
    c:RegisterEffect(e1)
    Duel.SpecialSummonComplete()
end
