-- Ｎｏ.93 希望皇ホープ
local s, id = GetID()

function Xyz.Target2(alterf, op)
    return function(e, tp, eg, ep, ev, re, r, rp, chk, c, must, og, min, max)
        if Duel.GetFlagEffect(tp, id) > 0 and c:IsCode(id) then
            return false
        end
        local cancel = not og and Duel.IsSummonCancelable()
        Xyz.ProcCancellable = cancel
        if og and not min then
            e:SetLabelObject(og:GetFirst())
            if op then
                op(e, tp, 1, og:GetFirst())
            end
            if c:IsCode(id) then
                Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
            end
            return true
        else
            local mg = nil
            if og then
                mg = og
            else
                mg = Duel.GetFieldGroup(tp, LOCATION_MZONE, LOCATION_MZONE)
            end
            local mustg = Auxiliary.GetMustBeMaterialGroup(tp, og, tp, c, mg, REASON_XYZ)
            if must then
                mustg:Merge(must)
            end
            local oc
            if #mustg > 0 then
                oc = mustg:GetFirst()
            else
                Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
                oc = mg:Filter(Xyz.AlterFilter, nil, alterf, c, e, tp, op):SelectUnselect(Group.CreateGroup(), tp,
                         false, cancel)
            end
            if not oc then
                return false
            end
            local ok = true
            if op then
                ok = op(e, tp, 1, oc)
            end
            if not ok then
                return false
            end
            e:SetLabelObject(oc)
            if c:IsCode(id) then
                Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
            end
            return true
        end
    end
end

function s.initial_effect(c)
    -- xyz summon
    Xyz.AddProcedure(c, nil, 13, 3, s.ovfilter, aux.Stringid(id, 0))
    c:EnableReviveLimit()

    -- cannot destroyed
    local e0 = Effect.CreateEffect(c)
    e0:SetType(EFFECT_TYPE_SINGLE)
    e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e0:SetValue(s.indes)
    c:RegisterEffect(e0)

    -- spsummon
    local e2 = Effect.CreateEffect(c)
    e2:SetDescription(aux.Stringid(2067935, 1))
    e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e2:SetType(EFFECT_TYPE_IGNITION)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCost(s.cost)
    e2:SetTarget(s.sptg)
    e2:SetOperation(s.spop)
    e2:SetCountLimit(1,id)
    c:RegisterEffect(e2)

    -- immune
    local e121 = Effect.CreateEffect(c)
    e121:SetType(EFFECT_TYPE_SINGLE)
    e121:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e121:SetRange(LOCATION_MZONE)
    e121:SetCode(EFFECT_IMMUNE_EFFECT)
    e121:SetValue(s.efilter1)
    c:RegisterEffect(e121)

    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_CHANGE_DAMAGE)
    e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
    e1:SetRange(LOCATION_MZONE)
    e1:SetTargetRange(1, 0)
    e1:SetValue(0)
    c:RegisterEffect(e1)

    local e3 = Effect.CreateEffect(c)
    e3:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
    e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e3:SetCode(EVENT_SPSUMMON_SUCCESS)
    e3:SetCondition(s.con)
    e3:SetOperation(s.op)
    c:RegisterEffect(e3)
end
s.xyz_number = 93
s.listed_series = {0x48}
s.listed_names={23187256}

function s.ovfilter(c,tp,xyzc)
    return c:IsFaceup() and c:IsRank(12) and c:IsAttribute(ATTRIBUTE_LIGHT) and c:IsType(TYPE_XYZ,xyzc,SUMMON_TYPE_XYZ,tp)
end

function s.indes(e, c)
    return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) and
               not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and
               not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end

function s.filter(c, e, tp)
    return c:IsType(TYPE_XYZ) and c:IsSetCard(0x48) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x2048) and
               c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_XYZ, tp, false, false)
end
function s.cost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.GetCurrentPhase() == PHASE_MAIN1
    end
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_CANNOT_BP)
    e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_OATH)
    e1:SetTargetRange(1, 0)
    e1:SetReset(RESET_PHASE + PHASE_END)
    Duel.RegisterEffect(e1, tp)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
    local c = e:GetHandler()
    local ocount = c:GetOverlayCount()
    if chk == 0 then
        return Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_XYZ) > 0 and
                   e:GetHandler():CheckRemoveOverlayCard(tp, 1, REASON_EFFECT) and
                   Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp)
    end
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, ocount, tp, LOCATION_EXTRA)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local ocount = c:GetOverlayCount()
    local ft = Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_XYZ)
    if ft <= 0 or ocount < 1 then
        return
    end
    if ocount > ft then
        ocount = ft
    end
    if Duel.IsPlayerAffectedByEffect(tp, 59822133) then
        ocount = 1
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
    local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_EXTRA, 0, 1, ocount, nil, e, tp)
    if g:GetCount() > 0 then
        for tc in aux.Next(g) do
            Duel.SpecialSummon(tc, SUMMON_TYPE_XYZ, tp, tp, false, false, POS_FACEUP)
            if c:GetFlagEffect(id) > 0 then
                local e5 = Effect.CreateEffect(c)
                e5:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
                e5:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
                e5:SetCode(EVENT_LEAVE_FIELD)
                e5:SetCondition(s.cotg)
                e5:SetOperation(s.coop)
                e5:SetReset(RESET_EVENT+RESET_OVERLAY+RESET_TOFIELD)
                tc:RegisterEffect(e5, true)
            end
            tc:CompleteProcedure()
        end
    end
    -- c:RemoveOverlayCard(tp,1,1,REASON_EFFECT)
end

function s.cotg(e, tp, eg, ep, ev, re, r, rp)
    return e:GetHandler():IsReason(REASON_EFFECT) and e:GetHandler():GetReasonPlayer()==1-tp
end
function s.coop(e, tp, eg, ep, ev, re, r, rp)
	local c=e:GetOwner()
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetProperty(EFFECT_FLAG_COPY_INHERIT)
	e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END,2)
	e1:SetValue(5000)
	c:RegisterEffect(e1)
end

function s.efilter(e)
    return Duel.IsExistingMatchingCard(s.spfilter, e:GetHandler():GetControler(), LOCATION_MZONE, 0, 1, e:GetHandler())
end
function s.efilter1(e, te)
    return te:GetOwner() ~= e:GetHandler()
end

function s.con(e, tp, eg, ep, ev, re, r, rp)
    return e:GetHandler():GetSummonType() == SUMMON_TYPE_XYZ and
               (e:GetHandler():GetOverlayGroup():IsExists(Card.IsCode, 1, nil, 23187256))
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
    e:GetHandler():RegisterFlagEffect(id, RESET_EVENT + 0x1ff0000, 0, 1)
end
