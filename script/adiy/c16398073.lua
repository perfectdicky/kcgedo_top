-- No.98 絶望皇ホープレス
local s, id = GetID()
Duel.LoadScript("rankup_functions.lua")
function s.initial_effect(c)
    -- xyz summon
    Xyz.AddProcedure(c, nil, 5, 3)
    aux.EnableCheckRankUp(c, nil, nil, 55470553)
    c:EnableReviveLimit()

    -- pos
    local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
    e1:SetCategory(CATEGORY_TOHAND)
    e1:SetType(EFFECT_TYPE_QUICK_O)
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCost(s.poscost)
    e1:SetTarget(s.postg)
    e1:SetOperation(s.posop)
    c:RegisterEffect(e1, false, REGISTER_FLAG_DETACH_XMAT)

    -- special summon
    local e2 = Effect.CreateEffect(c)
    e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e2:SetType(EFFECT_TYPE_QUICK_O)
    e2:SetProperty(EFFECT_FLAG_CARD_TARGET)
    e2:SetCode(EVENT_FREE_CHAIN)
    e2:SetHintTiming(0, TIMING_BATTLE_START)
    e2:SetRange(LOCATION_GRAVE)
    e2:SetCountLimit(1, id)
    e2:SetTarget(s.sptg)
    e2:SetOperation(s.spop)
    c:RegisterEffect(e2)

	local e5 = Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(id, 1))
    e5:SetCategory(CATEGORY_DAMAGE)
    e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
    e5:SetProperty(EFFECT_FLAG_DELAY+EFFECT_FLAG_PLAYER_TARGET)
    e5:SetCode(EVENT_DRAW)
    e5:SetRange(LOCATION_MZONE)
    e5:SetCountLimit(1, id)
    e5:SetCondition(s.drcon)
    e5:SetTarget(s.drtg)
    e5:SetOperation(s.drop)
	e5:SetLabel(RESET_EVENT + RESETS_STANDARD)
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetCode(EFFECT_RANKUP_EFFECT)
    e8:SetLabelObject(e5)
    c:RegisterEffect(e8)

    -- battle indestructable
    local e6 = Effect.CreateEffect(c)
    e6:SetType(EFFECT_TYPE_SINGLE)
    e6:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e6:SetValue(s.indes)
    c:RegisterEffect(e6)
end
s.listed_series = {0x48}
s.xyz_number = 98
s.listed_names = {55470553}

function s.poscost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():CheckRemoveOverlayCard(tp, 1, REASON_COST)
    end
    e:GetHandler():RemoveOverlayCard(tp, 1, 1, REASON_COST)
end
function s.posfilter(c)
    return c:IsAbleToHand()
end
function s.postg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then return
        Duel.IsExistingTarget(s.posfilter, tp, 0, LOCATION_MZONE, 1, nil)
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TARGET)
    local g = Duel.SelectTarget(tp, s.posfilter, tp, 0, LOCATION_MZONE, 1, 1, nil)
    Duel.SetOperationInfo(0, CATEGORY_TOHAND, g, 1, 0, LOCATION_MZONE)
end
function s.posop(e, tp, eg, ep, ev, re, r, rp)
    local tc = Duel.GetFirstTarget()
    if tc and tc:IsRelateToEffect(e) then
        Duel.SendtoHand(tc, nil, REASON_EFFECT)
    end
end

function s.spfilter(c, tp)
    return c:IsFaceup() and not c:IsType(TYPE_TOKEN) and (c:IsControler(tp) or c:IsAbleToChangeControler())
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
    if chkc then
        return chkc:IsLocation(LOCATION_MZONE) and s.spfilter(chkc, tp)
    end
    if chk == 0 then
        return e:GetHandler():IsCanBeSpecialSummoned(e, 0, tp, false, false, POS_FACEUP_DEFENSE) and
                   Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and
                   Duel.IsExistingTarget(s.spfilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil, tp)
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TARGET)
    Duel.SelectTarget(tp, s.spfilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, nil, tp)
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, e:GetHandler(), 1, 0, 0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = Duel.GetFirstTarget()
    if c:IsRelateToEffect(e) and Duel.SpecialSummon(c, 0, tp, tp, false, false, POS_FACEUP_DEFENSE) ~= 0 and
        tc:IsRelateToEffect(e) and not tc:IsImmuneToEffect(e) then
        local og = tc:GetOverlayGroup()
        if #og > 0 then
            Duel.SendtoGrave(og, REASON_RULE)
        end
        Duel.Overlay(c, Group.FromCards(tc))
    end
end

function s.drcon(e, tp, eg, ep, ev, re, r, rp)
	return ep ~= tp and re
end
function s.damfilter(c)
    return c:IsType(TYPE_MONSTER)
end
function s.drtg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return eg:FilterCount(s.damfilter, nil) > 0
    end
    local g = eg:Filter(s.damfilter, nil)
    Duel.SetTargetPlayer(1 - tp)
    Duel.SetOperationInfo(0, CATEGORY_DAMAGE, nil, 0, 1 - tp, g:GetSum(Card.GetAttack))
end
function s.drop(e, tp, eg, ep, ev, re, r, rp)
    local g = eg:Filter(s.damfilter, nil)
    local p = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER)
    Duel.ConfirmCards(tp, eg)
    Duel.Damage(p, g:GetSum(Card.GetAttack), REASON_EFFECT)
end

function s.indes(e, c)
    return not c:IsSetCard(0x48)
end
