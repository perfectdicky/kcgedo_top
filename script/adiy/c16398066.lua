--No.54 反骨の闘士ライオンハート
local s, id = GetID()
Duel.LoadScript("rankup_functions.lua")
function s.initial_effect(c)
	--xyz summon
    Xyz.AddProcedure(c,nil,2,4)
    aux.EnableCheckRankUp(c, nil, nil, 54366836)
    c:EnableReviveLimit()
    
    -- battle indestructable
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e1:SetValue(s.indes)
    c:RegisterEffect(e1)

    local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_REFLECT_BATTLE_DAMAGE)
	e2:SetValue(1)
    c:RegisterEffect(e2)
    
	local e121=Effect.CreateEffect(c)
	e121:SetType(EFFECT_TYPE_SINGLE)
	e121:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e121:SetRange(LOCATION_MZONE)
	e121:SetCode(EFFECT_IMMUNE_EFFECT)
	e121:SetValue(s.efilter)
    c:RegisterEffect(e121)  
    local e122=e121:Clone()
    e122:SetCode(EFFECT_ADD_RACE)
    e122:SetValue(RACE_BEASTWARRIOR)
    c:RegisterEffect(e122)
    
    local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetCode(EFFECT_ONLY_BE_ATTACKED)
	c:RegisterEffect(e4)
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetCode(EFFECT_ONLY_ATTACK_MONSTER)
	e5:SetRange(LOCATION_MZONE)
	e5:SetTargetRange(0,LOCATION_MZONE)
    c:RegisterEffect(e5)
    local e123=e5:Clone()
    e123:SetCode(EFFECT_MUST_ATTACK)
    c:RegisterEffect(e123)   
    
    local e3=Effect.CreateEffect(c)
    e3:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e3:SetType(EFFECT_TYPE_QUICK_O)
    e3:SetCode(EVENT_FREE_CHAIN)
    e3:SetRange(LOCATION_MZONE)
    e3:SetCountLimit(1)
	e3:SetCost(s.damcost)
	e3:SetTarget(s.damtg)
    e3:SetOperation(s.damop)
    e3:SetLabel(RESET_EVENT + RESETS_STANDARD)
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetCode(EFFECT_RANKUP_EFFECT)
    e8:SetLabelObject(e3)
	c:RegisterEffect(e8,false,REGISTER_FLAG_DETACH_XMAT)
end
s.xyz_number=54
s.listed_series = {0x48}
s.listed_names = {54366836}

function s.indes(e, c)
    return not c:IsSetCard(0x48)
end

function s.efilter(e,te)
	return te:GetOwner()~=e:GetOwner()
end

function s.damcost(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return c:CheckRemoveOverlayCard(tp,1,REASON_COST) end
	c:RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.damtg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then return true end
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE)
    local code = Duel.AnnounceCard(tp,12744567,OPCODE_ISCODE,67173574,OPCODE_ISCODE,OPCODE_OR,20785975,OPCODE_ISCODE,OPCODE_OR,49456901,OPCODE_ISCODE,OPCODE_OR,85121942,OPCODE_ISCODE,OPCODE_OR,55888045,OPCODE_ISCODE,OPCODE_OR,68396121,OPCODE_ISCODE,OPCODE_OR
        ,513000017,OPCODE_ISCODE,OPCODE_OR,511001429,OPCODE_ISCODE,OPCODE_OR,511001430,OPCODE_ISCODE,OPCODE_OR,511002871,OPCODE_ISCODE,OPCODE_OR,511002872,OPCODE_ISCODE,OPCODE_OR,511001431,OPCODE_ISCODE,OPCODE_OR,511010207,OPCODE_ISCODE,OPCODE_OR
        ,97,OPCODE_ISCODE,OPCODE_OR,255,OPCODE_ISCODE,OPCODE_OR,260,OPCODE_ISCODE,OPCODE_OR,257,OPCODE_ISCODE,OPCODE_OR,253,OPCODE_ISCODE,OPCODE_OR,249,OPCODE_ISCODE,OPCODE_OR,98,OPCODE_ISCODE,OPCODE_OR)
    e:SetLabel(code)
    Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,0,0)
end
function s.damop(e, tp, eg, ep, ev, re, r, rp)
    local c=e:GetHandler()
    local code = e:GetLabel()
    local tc=Duel.CreateToken(tp, code)
    Duel.SendtoDeck(tc, tp, 0, REASON_RULE)
    local mg=c:GetOverlayGroup()
	if mg:GetCount()~=0 then
		Duel.Overlay(tc,mg)
	end
	tc:SetMaterial(Group.FromCards(c))
	Duel.Overlay(tc,Group.FromCards(c))
	Duel.SpecialSummon(tc,SUMMON_TYPE_XYZ,tp,tp,true,true,POS_FACEUP)
	tc:CompleteProcedure()
end