-- No.60 刻不知のデュガレス
local s, id = GetID()
Duel.LoadScript("rankup_functions.lua")
function s.initial_effect(c)
    -- xyz summon
    Xyz.AddProcedure(c, nil, 5, 3)
    aux.EnableCheckRankUp(c, nil, nil, 66011101)
    c:EnableReviveLimit()

    -- draw
    local e1 = Effect.CreateEffect(c)
    e1:SetDescription(aux.Stringid(id, 0))
    e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
    e1:SetCategory(CATEGORY_DRAW)
    e1:SetType(EFFECT_TYPE_IGNITION)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCountLimit(1, id)
    e1:SetCost(s.cost)
    e1:SetTarget(s.drtg)
    e1:SetOperation(s.drop)
    c:RegisterEffect(e1, false, REGISTER_FLAG_DETACH_XMAT)

    -- spsummon
    local e2 = e1:Clone()
    e2:SetDescription(aux.Stringid(id, 1))
    e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e2:SetTarget(s.sptg)
    e2:SetOperation(s.spop)
    c:RegisterEffect(e2, false, REGISTER_FLAG_DETACH_XMAT)

    -- -- atk
    local e3 = e1:Clone()
    e3:SetDescription(aux.Stringid(id, 2))
    e3:SetCategory(CATEGORY_ATKCHANGE)
    e3:SetTarget(s.atktg)
    e3:SetOperation(s.atkop)
    c:RegisterEffect(e3, false, REGISTER_FLAG_DETACH_XMAT)
    local e4 = e3:Clone()
    e4:SetDescription(aux.Stringid(id, 3))
    e4:SetOperation(s.atkop2)
    c:RegisterEffect(e4, false, REGISTER_FLAG_DETACH_XMAT)

    local e5 = Effect.CreateEffect(c)
    e5:SetProperty(EFFECT_FLAG_DELAY)
    e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
    e5:SetRange(LOCATION_MZONE)
    e5:SetCode(EVENT_BATTLE_DESTROYING)
    e5:SetCondition(s.eqpcon)
    e5:SetCost(s.eqco)
    e5:SetOperation(s.skipop)
    e5:SetLabel(RESET_EVENT + RESETS_STANDARD)
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetCode(EFFECT_RANKUP_EFFECT)
    e8:SetLabelObject(e5)
    c:RegisterEffect(e8)

    -- battle indestructable
    local e6 = Effect.CreateEffect(c)
    e6:SetType(EFFECT_TYPE_SINGLE)
    e6:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e6:SetValue(s.indes)
    c:RegisterEffect(e6)
end
s.listed_series = {0x48}
s.xyz_number = 60
s.listed_names={66011101}

function s.cost(e, tp, eg, ep, ev, re, r, rp, chk)
    local c = e:GetHandler()
    if chk == 0 then
        return c:CheckRemoveOverlayCard(tp, c:GetOverlayCount(), REASON_COST)
    end
    c:RemoveOverlayCard(tp, c:GetOverlayCount(), c:GetOverlayCount(), REASON_COST)
    Duel.Hint(HINT_OPSELECTED, 1 - tp, e:GetDescription())
end
function s.drtg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsPlayerCanDraw(tp, 3)
    end
    Duel.SetTargetPlayer(tp)
    Duel.SetTargetParam(3)
    Duel.SetOperationInfo(0, CATEGORY_DRAW, nil, 0, tp, 3)
    Duel.SetOperationInfo(0, CATEGORY_HANDES, nil, 0, tp, 2)
end
function s.drop(e, tp, eg, ep, ev, re, r, rp)
    local p = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER)
    if Duel.Draw(p, 3, REASON_EFFECT) == 3 then
        Duel.ShuffleHand(tp)
        Duel.BreakEffect()
        Duel.DiscardHand(tp, nil, 2, 2, REASON_EFFECT + REASON_DISCARD)
    end
end

function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and
                   Duel.IsExistingMatchingCard(Card.IsCanBeSpecialSummoned, tp, LOCATION_GRAVE + LOCATION_REMOVED, 0, 1,
                       nil, e, 0, tp, false, false)
    end
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_GRAVE + LOCATION_REMOVED)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    if Duel.GetLocationCount(tp, LOCATION_MZONE) <= 0 then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
    local g = Duel.SelectMatchingCard(tp, Card.IsCanBeSpecialSummoned, tp, LOCATION_GRAVE + LOCATION_REMOVED, 0, 1, 1,
                  nil, e, 0, tp, false, false)
    if #g > 0 then
        Duel.SpecialSummon(g, 0, tp, tp, false, false, POS_FACEUP)
    end
end

function s.atktg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsExistingMatchingCard(Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil)
    end
end
function s.atkop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATTACK)
    local tc = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, nil):GetFirst()
    if tc then
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_SET_ATTACK_FINAL)
        e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
        e1:SetValue(tc:GetAttack() * 2)
        tc:RegisterEffect(e1)
    end
end

function s.atkop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATTACK)
    local tc = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, nil):GetFirst()
    if tc then
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_SET_ATTACK_FINAL)
        e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
        e1:SetValue(0)
        tc:RegisterEffect(e1)
    end
end

function s.eqpcon(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local rc = eg:GetFirst()
    return rc:IsRelateToBattle() and rc:IsStatus(STATUS_OPPO_BATTLE) and rc:IsControler(tp) and rc:IsRace(RACE_FIEND)
end
function s.eqco(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():GetFlagEffect(id)==0
    end
    e:GetHandler():RegisterFlagEffect(id,RESET_EVENT+RESETS_STANDARD,0,1)
end
function s.skipop(e, tp, eg, ep, ev, re, r, rp)
    local sel = Duel.SelectOption(1 - tp, aux.Stringid(id, 4), aux.Stringid(id, 5), aux.Stringid(id, 6))
    if sel == 0 then
        local e5 = Effect.CreateEffect(e:GetHandler())
        e5:SetType(EFFECT_TYPE_FIELD)
        e5:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
        e5:SetTargetRange(0, 1)
        e5:SetCode(EFFECT_SKIP_DP)
        if (Duel.GetTurnPlayer() == 1 - tp and Duel.GetCurrentPhase() == PHASE_DRAW) then
            e5:SetReset(RESET_PHASE + PHASE_DRAW)
        else
            e5:SetReset(RESET_PHASE + PHASE_DRAW + RESET_OPPO_TURN)
        end
        Duel.RegisterEffect(e5, tp)
    end
    if sel == 1 then
        local e7 = Effect.CreateEffect(e:GetHandler())
        e7:SetType(EFFECT_TYPE_FIELD)
        e7:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
        e7:SetTargetRange(0, 1)
        e7:SetCode(EFFECT_SKIP_M1)
        if (Duel.GetTurnPlayer() == 1 - tp and Duel.GetCurrentPhase() == PHASE_MAIN1) then
            e7:SetReset(RESET_PHASE + PHASE_MAIN1)
        else
            e7:SetReset(RESET_PHASE + PHASE_MAIN1 + RESET_OPPO_TURN)
        end
        Duel.RegisterEffect(e7, tp)
    end
    if sel == 2 then
        local e8 = Effect.CreateEffect(e:GetHandler())
        e8:SetType(EFFECT_TYPE_FIELD)
        e8:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
        e8:SetTargetRange(0, 1)
        e8:SetCode(EFFECT_SKIP_BP)
        if (Duel.GetTurnPlayer() == 1 - tp and Duel.GetCurrentPhase() == PHASE_BATTLE) then
            e8:SetReset(RESET_PHASE+ PHASE_BATTLE)
        else
            e8:SetReset(RESET_PHASE + PHASE_BATTLE + RESET_OPPO_TURN)
        end
        Duel.RegisterEffect(e8, tp)
    end
end

function s.indes(e, c)
    return not c:IsSetCard(0x48)
end