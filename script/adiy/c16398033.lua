--No.56 ゴールドラット
local s,id=GetID()
Duel.LoadScript("rankup_functions.lua")
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,2,4)
	aux.EnableCheckRankUp(c, nil, nil, 55935416)
    c:EnableReviveLimit()
    
	--battle indestructable
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e5:SetValue(s.indes)
	c:RegisterEffect(e5)

	--damage
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id,0))
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCategory(CATEGORY_DRAW)
    e1:SetType(EFFECT_TYPE_QUICK_O)
    e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1)
	e1:SetCost(s.drcost)
	e1:SetTarget(s.drtg)
	e1:SetOperation(s.drop)
    c:RegisterEffect(e1,false,REGISTER_FLAG_DETACH_XMAT)
    
    local e2=Effect.CreateEffect(c)
    e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DELAY)
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
    e2:SetCode(EVENT_DESTROYED)
	e2:SetRange(LOCATION_GRAVE)
    e2:SetCondition(s.spcon)
	e2:SetTarget(s.sptg)
	e2:SetOperation(s.spop)
    c:RegisterEffect(e2)   
    
    local e3=Effect.CreateEffect(c)
    e3:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DELAY)
	e3:SetCategory(CATEGORY_TODECK+CATEGORY_TOGRAVE)
    e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
    e3:SetCode(EVENT_REMOVE)
	e3:SetRange(LOCATION_MZONE)
    e3:SetCountLimit(1)
    e3:SetCondition(s.rmcon)
	e3:SetTarget(s.rmtg)
	e3:SetOperation(s.rmop)   
	e3:SetLabel(RESET_EVENT+RESETS_STANDARD)
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetCode(EFFECT_RANKUP_EFFECT)
	e8:SetLabelObject(e3)
	c:RegisterEffect(e8)    
end
s.xyz_number=56
s.listed_names={55935416}
s.listed_series={0x48}

function s.drcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.drtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,1) end
	Duel.SetTargetPlayer(tp)
	Duel.SetTargetParam(1)
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,1)
end
function s.drop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	local ct=Duel.Draw(p,d,REASON_EFFECT)
end

function s.spfilter(c,tp)
	return c:IsPreviousLocation(LOCATION_MZONE) 
	and c:IsPreviousControler(tp)
	and c:IsPreviousPosition(POS_FACEUP)
	and c:IsAttribute(ATTRIBUTE_EARTH)
	and (c:IsReason(REASON_BATTLE) or c:IsReason(REASON_EFFECT))
end
function s.spcon(e,tp,eg,ep,ev,re,r,rp)
	return not eg:IsContains(e:GetHandler()) and eg:FilterCount(s.spfilter,nil,tp)>0
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and e:GetHandler():IsCanBeSpecialSummoned(e,0,tp,false,false) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) then
        if Duel.SpecialSummon(c,0,tp,tp,false,false,POS_FACEUP)>0 then
            Duel.BreakEffect()
            Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
            local g=Duel.SelectMatchingCard(tp,Card.IsType,tp,LOCATION_GRAVE,LOCATION_GRAVE,1,1,nil,TYPE_MONSTER)
            if #g>0 then
                Duel.Overlay(c,g)
            end
        end
	end
end

function s.rmfilter(c,tp)
    return c:IsPreviousControler(tp)
    and (c:IsRace(RACE_BEAST) or c:IsRace(RACE_BEASTWARRIOR) or c:IsRace(RACE_WINGEDBEAST))
end
function s.rmcon(e,tp,eg,ep,ev,re,r,rp)
	return eg:FilterCount(s.rmfilter,nil,tp)>0
end
function s.rmtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
    local g=eg:Filter(s.rmfilter,nil,tp)
    Duel.SetOperationInfo(0,CATEGORY_TODECK,g,#g,0,LOCATION_REMOVED)
    Duel.SetOperationInfo(0,CATEGORY_TOGRAVE,g,#g,0,LOCATION_REMOVED)
end
function s.rmop(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
    local g=eg:Filter(s.rmfilter,nil,tp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_OPTION)
    local op=Duel.SelectOption(tp,aux.Stringid(id,1),aux.Stringid(id,2))
    if op==0 then Duel.SendtoGrave(g, REASON_EFFECT)
    else Duel.SendtoDeck(g, tp, 2, REASON_EFFECT) end
end

function s.indes(e,c)
	return not c:IsSetCard(0x48)
end