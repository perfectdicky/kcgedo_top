-- No.64 古狸三太夫
Duel.LoadScript("rankup_functions.lua")
local s, id = GetID()
function s.initial_effect(c)
    -- xyz summon
    Xyz.AddProcedure(c, aux.FilterBoolFunctionEx(Card.IsRace, RACE_BEAST), 3, 3)
    aux.EnableCheckRankUp(c, nil, nil, 39972129)
    c:EnableReviveLimit()

    -- token
    local e1 = Effect.CreateEffect(c)
    e1:SetDescription(aux.Stringid(39972129, 0))
    e1:SetCategory(CATEGORY_SPECIAL_SUMMON + CATEGORY_TOKEN)
    e1:SetType(EFFECT_TYPE_IGNITION)
    e1:SetCountLimit(1)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCost(s.spcost)
    e1:SetTarget(s.sptg)
    e1:SetOperation(s.spop)
    c:RegisterEffect(e1, false, REGISTER_FLAG_DETACH_XMAT)

    -- indestructable
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE)
    e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
    e2:SetCondition(s.indcon)
    e2:SetValue(1)
    e2:SetLabel(RESET_EVENT+RESETS_STANDARD)
    local e4 = e2:Clone()
    e4:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
    local e5 = Effect.CreateEffect(c)
    e5:SetType(EFFECT_TYPE_SINGLE)
    e5:SetCode(EFFECT_INDESTRUCTABLE)
    e5:SetCondition(s.indcon)
    e5:SetValue(1)
    e5:SetLabel(RESET_EVENT + RESETS_STANDARD)
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetCode(EFFECT_RANKUP_EFFECT)
	e8:SetLabelObject(e2)
    c:RegisterEffect(e8) 
    local e9 = e8:Clone()  
    e9:SetLabelObject(e4)
    c:RegisterEffect(e9) 
    local e10 = e8:Clone()  
    e10:SetLabelObject(e5)
    c:RegisterEffect(e10)

    -- battle indestructable
    local e3 = Effect.CreateEffect(c)
    e3:SetType(EFFECT_TYPE_SINGLE)
    e3:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e3:SetValue(aux.NOT(aux.TargetBoolFunction(Card.IsSetCard, 0x48)))
    c:RegisterEffect(e3)
end
s.listed_names = {39972130, 3790062}
s.listed_series = {0x48}
s.xyz_number = 64

function s.spcost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():CheckRemoveOverlayCard(tp, 1, REASON_COST)
    end
    e:GetHandler():RemoveOverlayCard(tp, 1, 1, REASON_COST)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and
                   Duel.IsPlayerCanSpecialSummonMonster(tp, 39972130, 0, TYPES_TOKEN, -2, 0, 1, RACE_BEAST,
                       ATTRIBUTE_EARTH)
    end
    local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
    Duel.SetOperationInfo(0, CATEGORY_TOKEN, nil, ft, 0, 0)
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, ft, tp, 0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    if Duel.GetLocationCount(tp, LOCATION_MZONE) <= 0 or
        not Duel.IsPlayerCanSpecialSummonMonster(tp, 39972130, 0, TYPES_TOKEN, -2, 0, 1, RACE_BEAST, ATTRIBUTE_EARTH) then
        return
    end
    local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
    for i = 1, ft do
        local token = Duel.CreateToken(tp, 39972130)
        Duel.SpecialSummonStep(token, 0, tp, tp, false, false, POS_FACEUP)
        -- local g, atk = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, nil):GetMaxGroup(Card.GetAttack)
        --atk check
        local e7=Effect.CreateEffect(e:GetHandler())
       	e7:SetType(EFFECT_TYPE_SINGLE)
        e7:SetCode(id)
        e7:SetReset(RESET_EVENT + RESETS_STANDARD)
     	token:RegisterEffect(e7)
        local e1 = Effect.CreateEffect(e:GetHandler())
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_SET_ATTACK_FINAL)
        e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_REPEAT+EFFECT_FLAG_DELAY)
	    e1:SetRange(LOCATION_MZONE)
        e1:SetValue(s.adval)
        e1:SetReset(RESET_EVENT + RESETS_STANDARD)
        token:RegisterEffect(e1)
        local e2=e1:Clone()
        e2:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
        token:RegisterEffect(e2)
        Duel.SpecialSummonComplete()
    end
end
function s.filter(c)
    return c:IsFaceup() and not c:IsHasEffect(id)
end
function s.adval(e, c)
    local g = Duel.GetMatchingGroup(s.filter, 0, LOCATION_MZONE, LOCATION_MZONE, nil)
    if #g == 0 then
        return 0
    else
        local tg, val = g:GetMaxGroup(Card.GetAttack)
        if not tg:IsExists(aux.TRUE, 1, e:GetHandler()) then
            g:RemoveCard(e:GetHandler())
            tg, val = g:GetMaxGroup(Card.GetAttack)
        end
        return val
    end
end

function s.ifilter(c)
    return c:IsFaceup() and c:IsCode(39972130)
end
function s.indcon(e)
    return Duel.IsExistingMatchingCard(s.ifilter, e:GetHandlerPlayer(), LOCATION_MZONE, 0, 1, e:GetHandler())
end
