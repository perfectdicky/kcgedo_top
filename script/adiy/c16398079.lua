-- Number 48: Shadow Lich (Manga)
local s, id = GetID()
Duel.LoadScript("rankup_functions.lua")
function s.initial_effect(c)
    -- xyz summon
    Xyz.AddProcedure(c, nil, 4, 3)
    aux.EnableCheckRankUp(c, nil, nil, 1426714)
    c:EnableReviveLimit()

    -- battle indestructable
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e1:SetValue(s.indes)
    c:RegisterEffect(e1)

    -- summon tokens
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_QUICK_O)
    e2:SetCode(EVENT_FREE_CHAIN)
    e2:SetCountLimit(1)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCost(s.spcost)
    e2:SetTarget(s.sptg)
    e2:SetOperation(s.spop)
    c:RegisterEffect(e2, false, REGISTER_FLAG_DETACH_XMAT)

    -- change name
    local e4 = Effect.CreateEffect(c)
    e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e4:SetType(EFFECT_TYPE_SINGLE)
    e4:SetCode(EFFECT_ADD_CODE)
    e4:SetRange(LOCATION_MZONE)
    e4:SetValue(16398080)
    c:RegisterEffect(e4)

    -- No battle damage
    local e5 = Effect.CreateEffect(c)
    e5:SetType(EFFECT_TYPE_FIELD)
    e5:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
    e5:SetRange(LOCATION_MZONE)
    e5:SetTargetRange(LOCATION_MZONE, 0)
    e5:SetTarget(aux.TargetBoolFunction(Card.IsCode, 16398080))
    e5:SetValue(1)
    c:RegisterEffect(e5)

    -- atkup
    local e3 = Effect.CreateEffect(c)
    e3:SetCategory(CATEGORY_ATKCHANGE)
    e3:SetType(EFFECT_TYPE_QUICK_O)
    e3:SetCode(EVENT_FREE_CHAIN)
    e3:SetRange(LOCATION_MZONE)
    e3:SetCountLimit(1)
    e3:SetCondition(s.con)
    e3:SetOperation(s.op)
    c:RegisterEffect(e3)

    -- destroy damage
	local e6 = Effect.CreateEffect(c)
	e6:SetProperty(EFFECT_FLAG_DELAY+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_PLAYER_TARGET)
    e6:SetCategory(CATEGORY_DAMAGE)
    e6:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
    e6:SetCode(EVENT_DESTROY)
    e6:SetRange(LOCATION_MZONE)
	e6:SetCountLimit(1)
	e6:SetTarget(s.damtg)
    e6:SetOperation(s.damop)
    e6:SetLabel(RESET_EVENT + RESETS_STANDARD)
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetCode(EFFECT_RANKUP_EFFECT)
    e8:SetLabelObject(e6)
    c:RegisterEffect(e8)
end
s.xyz_number = 48
s.listed_series = {0x48}
s.listed_names = {16398080, 1426714}

function s.indes(e, c)
    return not c:IsSetCard(0x48)
end

function s.spcost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():CheckRemoveOverlayCard(tp, 1, REASON_COST)
    end
    e:GetHandler():RemoveOverlayCard(tp, 1, 1, REASON_COST)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and
                   Duel.IsPlayerCanSpecialSummonMonster(tp, 16398080, 0, TYPES_TOKEN, -2, 0, 1, RACE_ZOMBIE,
                       ATTRIBUTE_DARK)
    end
    local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
    Duel.SetOperationInfo(0, CATEGORY_TOKEN, nil, ft, 0, 0)
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, ft, tp, 0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    if Duel.GetLocationCount(tp, LOCATION_MZONE) <= 0 or
        not Duel.IsPlayerCanSpecialSummonMonster(tp, 16398080, 0, TYPES_TOKEN, -2, 0, 1, RACE_ZOMBIE, RACE_ZOMBIE) then
        return
    end
    local c = e:GetHandler()
    local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
    for i = 1, ft do
        local token = Duel.CreateToken(tp, 16398080)
        Duel.SpecialSummonStep(token, 0, tp, tp, false, false, POS_FACEUP)
        local e1 = Effect.CreateEffect(e:GetHandler())
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
        e1:SetReset(RESET_EVENT + RESETS_STANDARD)
        token:RegisterEffect(e1)
        Duel.SpecialSummonComplete()
    end
end

function s.Ocode(c)
    return c:IsOriginalCode(16398080) and c:IsFaceup()
end
function s.con(e, tp, eg, ep, ev, re, r, rp)
    return Duel.IsExistingMatchingCard(s.Ocode, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local g = Duel.GetMatchingGroup(s.Ocode, tp, LOCATION_MZONE, 0, nil)
    if #g > 0 and c:IsFaceup() and c:IsRelateToEffect(e) then
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(g:GetSum(Card.GetAttack))
        e1:SetReset(RESET_EVENT + RESETS_STANDARD_DISABLE + RESET_PHASE + PHASE_END)
        c:RegisterEffect(e1)
    end
end

function s.damfilter(c, tp)
    return c:IsPreviousLocation(LOCATION_MZONE) and c:GetPreviousControler() == tp and c:IsOriginalCode(16398080)
end
function s.damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:FilterCount(s.damfilter, nil, tp)>0 end
	local g=eg:Filter(s.damfilter, nil, tp)
	Duel.SetTargetPlayer(1-tp)
	Duel.SetTargetParam(g:GetSum(Card.GetAttack))
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,g:GetSum(Card.GetAttack)) 
end
function s.damop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Damage(p,d,REASON_EFFECT)
end