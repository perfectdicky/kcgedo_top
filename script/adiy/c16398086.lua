--Number 95: Galaxy Eyes Dark Matter Dragon
local s,id=GetID()
Duel.LoadScript("rankup_functions.lua")
function Xyz.Target2(alterf, op)
    return function(e, tp, eg, ep, ev, re, r, rp, chk, c, must, og, min, max)
        if Duel.GetFlagEffect(tp, id) > 0 and c:IsCode(id) then
            return false
        end
        local cancel = not og and Duel.IsSummonCancelable()
        Xyz.ProcCancellable = cancel
        if og and not min then
            e:SetLabelObject(og:GetFirst())
            if op then
                op(e, tp, 1, og:GetFirst())
            end
            if c:IsCode(id) then
                Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
            end
            return true
        else
            local mg = nil
            if og then
                mg = og
            else
                mg = Duel.GetFieldGroup(tp, LOCATION_MZONE, LOCATION_MZONE)
            end
            local mustg = Auxiliary.GetMustBeMaterialGroup(tp, og, tp, c, mg, REASON_XYZ)
            if must then
                mustg:Merge(must)
            end
            local oc
            if #mustg > 0 then
                oc = mustg:GetFirst()
            else
                Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
                oc = mg:Filter(Xyz.AlterFilter, nil, alterf, c, e, tp, op):SelectUnselect(Group.CreateGroup(), tp,
                         false, cancel)
            end
            if not oc then
                return false
            end
            local ok = true
            if op then
                ok = op(e, tp, 1, oc)
            end
            if not ok then
                return false
            end
            e:SetLabelObject(oc)
            if c:IsCode(id) then
                Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
            end
            return true
        end
    end
end

function s.initial_effect(c)
	--xyz summon
    Xyz.AddProcedure(c, nil, 10, 4, s.ovfilter, aux.Stringid(id, 0))
    aux.EnableCheckRankUp(c, nil, nil, 58820923)
    c:EnableReviveLimit()
    
	--banish
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id,0))
	e1:SetCategory(CATEGORY_REMOVE+CATEGORY_ATKCHANGE)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
    e1:SetCode(EVENT_SPSUMMON_SUCCESS)
    e1:SetCountLimit(1,id)
    e1:SetCondition(s.scon)
	e1:SetTarget(s.bantg)
	e1:SetOperation(s.banop)
	c:RegisterEffect(e1)
    
	--remove
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_REMOVE)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e4:SetCode(EVENT_PRE_DAMAGE_CALCULATE)
	e4:SetTarget(s.target)
	e4:SetOperation(s.operation)
    c:RegisterEffect(e4)
    
    local e2 = Effect.CreateEffect(c)
    e2:SetDescription(aux.Stringid(31801517, 1))
    e2:SetCategory(CATEGORY_TODECK+CATEGORY_DAMAGE)
    e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DELAY)
    e2:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_O)
    e2:SetCode(EVENT_LEAVE_FIELD)
    e2:SetCountLimit(1,id+1)
    e2:SetCondition(s.spcon)
    e2:SetTarget(s.sptarget)
    e2:SetOperation(s.spop)
    c:RegisterEffect(e2)

	local e7=Effect.CreateEffect(c)
	e7:SetDescription(aux.Stringid(68396121,1))
	e7:SetType(EFFECT_TYPE_IGNITION)
    e7:SetRange(LOCATION_MZONE)
    e7:SetCountLimit(1,id+2)
    e7:SetCost(s.atkcost)
	e7:SetOperation(s.atkop)
	e7:SetLabel(RESET_EVENT+RESETS_STANDARD)
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetCode(EFFECT_RANKUP_EFFECT)
	e8:SetLabelObject(e7)
	c:RegisterEffect(e8,false,REGISTER_FLAG_DETACH_XMAT)

	--battle indestructable
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e5:SetValue(s.indes)
	c:RegisterEffect(e5)
end
s.xyz_number=95
s.listed_names={58820923}
s.listed_series={0x48}

function s.ovfilter(c,tp,xyzc)
    return c:IsFaceup() and c:IsRank(9) and c:IsSetCard(0x107b,xyzc,SUMMON_TYPE_XYZ,tp) and c:IsType(TYPE_XYZ,xyzc,SUMMON_TYPE_XYZ,tp)
end

function s.scon(e, tp, eg, ep, ev, re, r, rp)
    return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_XYZ)==SUMMON_TYPE_XYZ
end
function s.banfilter(c)
	return c:IsAbleToRemove()
end
function s.bantg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chk==0 then return Duel.IsExistingMatchingCard(s.banfilter,1-tp,LOCATION_DECK,0,1,nil) and Duel.GetFieldGroupCount(tp, LOCATION_REMOVED, 0) >0 end
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,nil,Duel.GetFieldGroupCount(tp, LOCATION_REMOVED, 0),0,0)
end
function s.banop(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetOwner()
    local ct=Duel.GetFieldGroupCount(tp,LOCATION_REMOVED,0)
    if ct<1 then return end
	Duel.Hint(HINT_SELECTMSG,1-tp,HINTMSG_REMOVE)
	local g=Duel.SelectMatchingCard(1-tp,s.banfilter,1-tp,LOCATION_DECK,0,ct,ct,nil)
	if #g>0 then
		Duel.Remove(g,POS_FACEUP,REASON_EFFECT)
    end
    local g2=Duel.GetOperatedGroup()
    if #g2>0 then
        local e1=Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetProperty(EFFECT_FLAG_COPY_INHERIT)
        e1:SetReset(RESET_EVENT+RESETS_STANDARD)
        e1:SetValue(#g2*200)
        c:RegisterEffect(e1)      
    end    
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	local a=Duel.GetAttacker()
	local t=Duel.GetAttackTarget()
	if chk==0 then
		return (t==c and a:IsAbleToRemove())
			or (a==c and t~=nil and t:IsAbleToRemove())
	end
	local g=Group.CreateGroup()
	if a:IsRelateToBattle() and a~=c then g:AddCard(a) end
	if t~=nil and t:IsRelateToBattle() and t~=c then g:AddCard(t) end
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,g,#g,0,0)
end
function s.operation(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
	local a=Duel.GetAttacker()
	local d=Duel.GetAttackTarget()
	local g=Group.CreateGroup()
	if a:IsRelateToBattle() and a~=c then g:AddCard(a) end
	if t~=nil and t:IsRelateToBattle() and t~=c then g:AddCard(t) end
	Duel.Remove(g,POS_FACEUP,REASON_EFFECT)
end

function s.spcon(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local ct = c:GetOverlayCount()
    return c:IsPreviousLocation(LOCATION_MZONE) and ct > 0
end
function s.sptarget(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetFieldGroupCount(tp, LOCATION_REMOVED, LOCATION_REMOVED) >0 end
    local g=Duel.GetFieldGroup(tp, LOCATION_REMOVED, LOCATION_REMOVED)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,g,#g,0,0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local g=Duel.GetFieldGroup(tp, LOCATION_REMOVED, LOCATION_REMOVED)
    Duel.SendtoDeck(g,nil,0,REASON_EFFECT)
    local g2=Duel.GetOperatedGroup()
    if #g2<1 then return end
    Duel.Damage(1-tp, #g2*100, REASON_EFFECT)
end

function s.atkcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) and c:IsFaceup() then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_EXTRA_ATTACK)
		e1:SetValue(2)
		e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e1)
	end
end

function s.indes(e,c)
	return not c:IsSetCard(0x48)
end
