--No.11 ビッグ・アイ
local s,id=GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,8,3)
    c:EnableReviveLimit()
    
	--attack up
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_CONTROL)
	e1:SetDescription(aux.Stringid(id,0))
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetCountLimit(1,id)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(s.cost)
	e1:SetTarget(s.target)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1,false,REGISTER_FLAG_DETACH_XMAT)
	
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	e3:SetCondition(s.con)
	e3:SetOperation(s.op)
	c:RegisterEffect(e3)	
end
s.xyz_number=11
s.listed_names={80117527}
s.listed_series={0x48}

function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.cfilter(c)
	return c:IsControlerCanBeChanged()
end
function s.cfilter2(c)
	return not c:IsHasEffect(EFFECT_CANNOT_CHANGE_CONTROL)
end
function s.cfilter3(c)
	return c:IsControlerCanBeChanged() or not c:IsHasEffect(EFFECT_CANNOT_CHANGE_CONTROL)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return (Duel.IsExistingMatchingCard(s.cfilter,tp,0,LOCATION_MZONE,1,nil) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0) or (Duel.IsExistingMatchingCard(s.cfilter2,tp,0,LOCATION_SZONE,1,nil) and Duel.GetLocationCount(tp,LOCATION_SZONE)>0) end
	Duel.SetOperationInfo(0,CATEGORY_CONTROL,nil,1,0,0)
end
function s.operation(e,tp,eg,ep,ev,re,r,rp)
	local g=Group.CreateGroup()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CONTROL)
	if (Duel.IsExistingMatchingCard(s.cfilter,tp,0,LOCATION_MZONE,1,nil) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0) and not (Duel.IsExistingMatchingCard(s.cfilter2,tp,0,LOCATION_SZONE,1,nil) and Duel.GetLocationCount(tp,LOCATION_SZONE)>0) then
		g=Duel.SelectMatchingCard(tp,s.cfilter,tp,0,LOCATION_MZONE,1,1,nil)
	elseif not (Duel.IsExistingMatchingCard(s.cfilter,tp,0,LOCATION_MZONE,1,nil) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0) and (Duel.IsExistingMatchingCard(s.cfilter2,tp,0,LOCATION_SZONE,1,nil) and Duel.GetLocationCount(tp,LOCATION_SZONE)>0) then
		g=Duel.SelectMatchingCard(tp,s.cfilter2,tp,0,LOCATION_SZONE,1,1,nil)
	else
		g=Duel.SelectMatchingCard(tp,s.cfilter3,tp,0,LOCATION_ONFIELD,1,1,nil)
	end
	if g then
		local tc=g:GetFirst()
		if tc:IsType(TYPE_MONSTER) then
			Duel.GetControl(tc,tp)
		else
			local loc=LOCATION_SZONE
			if tc:IsLocation(LOCATION_FZONE) then loc=LOCATION_FZONE end
			Duel.MoveToField(tc, tp, tp, loc, tc:GetPosition(), true)
		end
		--tc:RegisterFlagEffect(id,RESET_EVENT+RESETS_STANDARD,0,1,e:GetHandler():GetFieldID())
        local e1=Effect.CreateEffect(e:GetHandler())
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_OATH)
        e1:SetCode(EFFECT_CANNOT_ATTACK)
        e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e1)
		if e:GetHandler():GetFlagEffect(id)==0 then return end
        local e2=Effect.CreateEffect(e:GetHandler())
        e2:SetDescription(aux.Stringid(10110717,0))
        e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
        e2:SetCode(EVENT_BATTLE_DESTROYING)
		e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
		e2:SetCountLimit(1,id)
		e2:SetCondition(aux.bdocon)
        e2:SetTarget(s.sptg2)
		e2:SetOperation(s.spop2)
		e2:SetReset(RESET_EVENT+RESETS_STANDARD)	
		tc:RegisterEffect(e2)
	end
end
-- function s.flag(c,tc)
-- 	return c:GetFlagEffectLabel(id)==tc:GetFieldID()
-- end

-- function s.spcon2(e,tp,eg,ep,ev,re,r,rp)
-- 	return e:GetOwner():GetFlagEffectLabel(511000685)==80117527
-- end
function s.spfilter2(c,e,tp)
	return c:IsCanBeSpecialSummoned(e,0,e:GetOwnerPlayer(),false,false) and c:IsPreviousControler(1-tp)
end
function s.sptg2(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	if chk==0 then return Duel.GetLocationCount(e:GetOwnerPlayer(),LOCATION_MZONE)>0 and s.spfilter2(bc,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,e:GetOwnerPlayer(),LOCATION_MZONE)
end
function s.spop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	if Duel.GetLocationCount(e:GetOwnerPlayer(),LOCATION_MZONE)<1 then return end
	Duel.SpecialSummon(bc,0,tp,e:GetOwnerPlayer(),false,false,POS_FACEUP)
end

function s.con(e,tp,eg,ep,ev,re,r,rp) 
	return e:GetHandler():GetSummonType()==SUMMON_TYPE_XYZ and
	  (e:GetHandler():GetOverlayGroup():IsExists(Card.IsCode,1,nil,80117527))
end
function s.op(e,tp,eg,ep,ev,re,r,rp)
	e:GetHandler():RegisterFlagEffect(id,RESET_EVENT+0x1ff0000,0,1)
end