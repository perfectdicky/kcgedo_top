--Number 72: Line Monster Chariot Hisha
local s,id=GetID()
Duel.LoadScript("rankup_functions.lua")
function s.initial_effect(c)
	--xyz summon
    Xyz.AddProcedure(c,nil,7,3)
    aux.EnableCheckRankUp(c, nil, nil, 75253697)
    c:EnableReviveLimit()
    
	--battle indestructable
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetValue(s.indes)
	c:RegisterEffect(e1)

	--destroy
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(698785,0))
	e2:SetCategory(CATEGORY_DESTROY)
	e2:SetType(EFFECT_TYPE_QUICK_O)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCountLimit(1)
	e2:SetCost(s.cost)
	e2:SetTarget(s.target)
	e2:SetOperation(s.operation)
    c:RegisterEffect(e2,false,REGISTER_FLAG_DETACH_XMAT)
	
	local e5=Effect.CreateEffect(c)
	e5:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetCode(EFFECT_INDESTRUCTABLE)
	e5:SetRange(LOCATION_MZONE)
	e5:SetTargetRange(LOCATION_MZONE,0)
	e5:SetTarget(s.infilter)
	e5:SetValue(1)
	e5:SetLabel(RESET_EVENT + RESETS_STANDARD)
	local e6=e5:Clone()
	e6:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
    e6:SetValue(aux.tgoval)
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetCode(EFFECT_RANKUP_EFFECT)
    e8:SetLabelObject(e5)
	c:RegisterEffect(e8)
	local e9=e8:Clone()
    e9:SetLabelObject(e6)
	c:RegisterEffect(e9)	
end
s.listed_series = {0x48}
s.listed_names={75253697}
s.xyz_number = 72

function s.indes(e,c)
	return not c:IsSetCard(0x48)
end

function s.filter(c)
	return c:IsDestructable() and c:IsLocation(LOCATION_ONFIELD)
end
function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=e:GetHandler():GetColumnGroup(1,1)
	if chk==0 then return g:FilterCount(s.filter, e:GetHandler())>0 end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g:Filter(s.filter, e:GetHandler()),g:FilterCount(s.filter, e:GetHandler()),0,0)
end
function s.operation(e,tp,eg,ep,ev,re,r,rp)
	if not e:GetHandler():IsLocation(LOCATION_ONFIELD) then return end
	local g=e:GetHandler():GetColumnGroup(1,1)
	if #g<1 then return end
	local dg=g:Filter(s.filter, e:GetHandler())
	if #dg<1 then return end
	Duel.Destroy(dg,REASON_EFFECT)
end

function s.infilter(e,c)
	return c:GetSequence()==4-e:GetHandler():GetSequence()+1 or c:GetSequence()==4-e:GetHandler():GetSequence()-1
end
