--黑暗氣息 (KA)
function c571.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY+CATEGORY_DISABLE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetHintTiming(0,TIMING_END_PHASE)
	e1:SetTarget(c571.target)
	e1:SetOperation(c571.activate)
	c:RegisterEffect(e1)	
end

function c571.filter(c)
	return c:IsType(TYPE_SPELL+TYPE_TRAP)
end
function c571.filter2(c)
	return c:IsSetCard(0x900) and c:IsFaceup() and c:IsType(TYPE_MONSTER)
end
function c571.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	local gc=Duel.GetMatchingGroupCount(c571.filter2,tp,LOCATION_MZONE,0,nil)
	local g=Duel.GetMatchingGroup(c571.filter,tp,0,LOCATION_ONFIELD,nil)
	if chk==0 then return g:GetCount()>0 and gc>0 end
	if g:GetCount()<gc then gc=g:GetCount() end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,gc,0,0) 
	Duel.SetOperationInfo(0,CATEGORY_DISABLE,g,gc,0,0)
end
function c571.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local gc=Duel.GetMatchingGroupCount(c571.filter2,tp,LOCATION_MZONE,0,nil)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	local g=Duel.SelectMatchingCard(tp,c571.filter,tp,0,LOCATION_ONFIELD,gc,gc,nil)
	if g:GetCount()<1 then return end
	local tc=g:GetFirst()
	while tc do
		Duel.NegateRelatedChain(tc,RESET_TURN_SET)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetValue(RESET_TURN_SET)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e2)
		if tc:IsType(TYPE_TRAPMONSTER) then
			local e3=Effect.CreateEffect(c)
			e3:SetType(EFFECT_TYPE_SINGLE)
			e3:SetCode(EFFECT_DISABLE_TRAPMONSTER)
			e3:SetReset(RESET_EVENT+0x1fe0000)
			tc:RegisterEffect(e3)
	   end
	   Duel.Destroy(tc,REASON_EFFECT)
	   tc=g:GetNext()
	   end
end
