--祭祀之神官
function c10000071.initial_effect(c)
	--作为幻神召唤祭品当成两只使用
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_DOUBLE_TRIBUTE)
	e1:SetValue(c10000071.condition)
	c:RegisterEffect(e1)
	
	--不能特殊召唤
	local e2=Effect.CreateEffect(c)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_SPSUMMON_CONDITION)
	c:RegisterEffect(e2)
end

function c10000071.condition(e,c)
	return c:IsRace(RACE_DIVINE)
end
