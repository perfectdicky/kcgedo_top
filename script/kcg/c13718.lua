-- CXyz Barian, the King of Wishes
local s, id = GetID()
function s.initial_effect(c)
    -- xyz summon
    Xyz.AddProcedure(c, nil, 7, 3, nil, nil, 99)
    c:EnableReviveLimit()

    -- special summon 
    local e00 = Effect.CreateEffect(c)
    e00:SetType(EFFECT_TYPE_FIELD)
    e00:SetCode(EFFECT_SPSUMMON_PROC)
    e00:SetProperty(EFFECT_FLAG_UNCOPYABLE)
    e00:SetRange(LOCATION_EXTRA)
    e00:SetCondition(s.spcon)
    e00:SetOperation(s.spop)
    e00:SetValue(SUMMON_TYPE_XYZ)
    c:RegisterEffect(e00)

    -- atk
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE)
    e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCode(EFFECT_SET_ATTACK)
    e2:SetValue(s.atkval)
    c:RegisterEffect(e2)

    local e1 = Effect.CreateEffect(c)
    e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCode(511001363)
    c:RegisterEffect(e1)
    aux.GlobalCheck(s, function()
        local ge = Effect.GlobalEffect()
        ge:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        ge:SetCode(EVENT_ADJUST)
        ge:SetCondition(s.con)
        ge:SetOperation(s.op)
        Duel.RegisterEffect(ge, 0)
    end)

    local e3 = Effect.CreateEffect(c)
    e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DAMAGE_CAL)
    e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e3:SetCode(EVENT_ADJUST)
    e3:SetRange(LOCATION_MZONE)
    e3:SetOperation(s.op2)
    c:RegisterEffect(e3)
end
s.listed_series = {0x1048}

function s.spcon(e, c)
    if c == nil then
        return true
    end
    local g = Duel.GetMatchingGroup(s.ovfilter, c:GetControler(), LOCATION_MZONE, LOCATION_MZONE, nil, c)
    return Duel.IsExistingMatchingCard(s.ovfilter, c:GetControler(), LOCATION_MZONE, LOCATION_MZONE, 3, nil, c) and
               g:GetCount() > 3 and Duel.GetLocationCountFromEx(c:GetControler(), c:GetControler(), g, c) > 0
end
function s.ovfilter(c, tc)
    local no = c.xyz_number
    return c:IsFaceup() and c:IsCanBeXyzMaterial(tc)
    and ((no and no>=101 and no<=107 and c:IsSetCard(0x1048,tc,SUMMON_TYPE_XYZ,tc:GetControler()))
     or (c:IsXyzLevel(tc,7)))
end
function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
    local c = e:GetHandler()
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
    local tg = Duel.SelectMatchingCard(tp, s.ovfilter, c:GetControler(), LOCATION_MZONE, LOCATION_MZONE, 3, 99, nil, c)
    local ag = tg
    local tc = tg:GetFirst()
    while tc do
        local ttc = tc:GetOverlayGroup()
        if ttc ~= nil then
            local btc = ttc:GetFirst()
            while btc do
                Duel.Overlay(e:GetHandler(), btc)
                btc = ttc:GetNext()
            end
        end
        Duel.Overlay(e:GetHandler(), tc)
        ag:Merge(ttc)
        tc = tg:GetNext()
    end
    c:SetMaterial(tg)
end

function s.op2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local g = c:GetOverlayGroup()
    for tc in aux.Next(g) do
        local code = tc:GetOriginalCode()
        if c:GetFlagEffect(code) == 0 then
            c:RegisterFlagEffect(code, RESET_EVENT + RESETS_STANDARD, 0, 1)
        end
    end
end

function s.con(e)
	return Duel.IsExistingMatchingCard(s.cfilter,0,LOCATION_ALL,LOCATION_ALL,1,nil)
end
function s.cfilter(c)
    return c:IsHasEffect(511002571) and c:GetFlagEffect(5110013630)==0
end
function s.op(e)
    local g = Duel.GetMatchingGroup(s.cfilter, 0, LOCATION_ALL, LOCATION_ALL, nil)
    for c in aux.Next(g) do
        local effs = {c:GetCardEffect(511002571)}
        for _, eff in ipairs(effs) do
            local te = eff:GetLabelObject()
            if te:GetCode()==EFFECT_RANKUP_EFFECT and te:GetLabelObject() then te=te:GetLabelObject() end
            local resetflag, resetcount = te:GetReset()
            local max, code = te:GetCountLimit()
            local category = te:GetCategory()
            local prop1, prop2 = te:GetProperty()
            local range = te:GetRange()
            local targetrange1, targetrange2 = te:GetTargetRange()
            if not targetrange1 then
                targetrange1 = 0
            end
            if not targetrange2 then
                targetrange2 = 0
            end
            local label = te:GetLabel()
            local labelob = te:GetLabelObject()
            local e1 = Effect.CreateEffect(c)
            if te:GetDescription() then
                e1:SetDescription(te:GetDescription())
            end
            e1:SetType(EFFECT_TYPE_XMATERIAL+te:GetType()&(~EFFECT_TYPE_SINGLE))
            if range then
                e1:SetRange(range)
            end
            if targetrange1~=0 or targetrange2~=0 then
                e1:SetTargetRange(targetrange1, targetrange2)
            end
            if te:GetCode() and te:GetCode()>0 then
                e1:SetCode(te:GetCode())
            end
            e1:SetCategory(category)
            e1:SetProperty(prop1, prop2)
            if label then
                e1:SetLabel(label)
            end
            e1:SetLabelObject(te)
            e1:SetCondition(s.copycon)
            e1:SetCost(s.copycost)
            if max and max > 0 then
                e1:SetCountLimit(max, code)
            end
            if te:GetTarget() then
                e1:SetTarget(te:GetTarget())
            end
            if te:GetOperation() then
                e1:SetOperation(te:GetOperation())
            end
            if resetflag > 0 and resetcount > 0 then
                e1:SetReset(resetflag, resetcount)
            elseif resetflag > 0 then
                e1:SetReset(resetflag)
            end
            c:RegisterEffect(e1, true)
            c:RegisterFlagEffect(5110013630,resetflag,prop1,resetcount)
        end
    end
end
function s.copycon(e, tp, eg, ep, ev, re, r, rp)
    local con = e:GetLabelObject():GetCondition()
    return e:GetHandler():IsHasEffect(511001363) 
    and e:GetOwner():GetFlagEffect(id) == 0 
    and (not con or con(e, tp, eg, ep, ev, re, r, rp))
end
function s.copycost(e, tp, eg, ep, ev, re, r, rp, chk)
    local c = e:GetHandler()
    local tc = e:GetOwner()
    local te = e:GetLabelObject()
    local cost = te:GetCost()
    local a = c:CheckRemoveOverlayCard(tp, 1, REASON_COST)
    local b = Duel.CheckLPCost(tp, 400)
    local ov = c:GetOverlayGroup()
    if chk == 0 then
        return (a or b) and (cost == nil or cost(e, tp, eg, ep, ev, re, r, rp, 0))
    end
    Duel.Hint(HINT_CARD, 0, tc:GetOriginalCode())
    -- Duel.SetTargetCard(tc)
    local op = 0
    Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(76922029, 0))
    if a and b then
        op = Duel.SelectOption(tp, aux.Stringid(81330115, 0), aux.Stringid(21454943, 1))
    elseif a and not b then
        Duel.SelectOption(tp, aux.Stringid(81330115, 0))
        op = 0
    else
        Duel.SelectOption(tp, aux.Stringid(21454943, 1))
        op = 1
    end
    if op == 0 then
        Duel.SendtoGrave(tc, REASON_COST)
    else
        Duel.PayLPCost(tp, 400)
    end
    tc:RegisterFlagEffect(id, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, 0, 1)
    if te:GetLabelObject() then
        te:SetLabelObject(te:GetLabelObject())
    end
end

function s.atkval(e, c)
    return c:GetOverlayCount() * 1000
end
