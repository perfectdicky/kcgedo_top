--No.107 銀河眼の時空竜
local s, id = GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,8,2)
	c:EnableReviveLimit()
	  --cannot destroyed
		local e0=Effect.CreateEffect(c)
	  e0:SetType(EFFECT_TYPE_SINGLE)
	  e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	  e0:SetValue(s.indes)
	  c:RegisterEffect(e0)
	local e2=Effect.CreateEffect(c)
	  e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CHAINING)
	e2:SetCondition(s.regcon)
	e2:SetOperation(s.regop)
	Duel.RegisterEffect(e2,0)
	local e22=Effect.CreateEffect(c)
	  e22:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e22:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e22:SetCode(EVENT_PHASE+PHASE_END)
	  e22:SetCountLimit(1)
	  e22:SetLabelObject(e2)
	e22:SetOperation(s.regop2)
	Duel.RegisterEffect(e22,0)
	--negate
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(88177324,0))
	--e1:SetProperty(0)   
	e1:SetType(EFFECT_TYPE_QUICK_O)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_MZONE)
	--e1:SetHintTiming(TIMING_BATTLE_PHASE)
	  e1:SetCountLimit(1)
	e1:SetCondition(s.negcon)
	e1:SetCost(s.negcost)
	--e1:SetTarget(s.negtg)
	  e1:SetLabelObject(e2)
	e1:SetOperation(s.negop)
	c:RegisterEffect(e1,false,REGISTER_FLAG_DETACH_XMAT)
	
	if not s.global_check then
		s.global_check=true
		local ge3=Effect.CreateEffect(c)
		ge3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge3:SetCode(EVENT_ATTACK_DISABLED)
		ge3:SetOperation(s.check)
		Duel.RegisterEffect(ge3,0)
	end 
end
s.xyz_number=107
s.listed_series = {0x48}

 function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end
function s.desfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_MONSTER) and (not c:IsSetCard(0x48) or c:IsSetCard(0x1048))
end
function s.descon(e)
	local c=e:GetHandler()
	return Duel.IsExistingMatchingCard(s.desfilter,c:GetControler(),0,LOCATION_MZONE,1,c)
end
 function s.check(e,tp,eg,ep,ev,re,r,rp)
	local tc=eg:GetFirst()
	local cn=tc:GetFlagEffectLabel(511010107)
	if cn then
		tc:SetFlagEffectLabel(511010107,cn+1)
	else
		tc:RegisterFlagEffect(511010107,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,1)
	end
end
 function s.attfilter(c)
	return c:CanAttack() 
end
function s.filter11(c)
	local cfn=c:GetFlagEffectLabel(511010107)
	local cxm=c:GetEffectCount(EFFECT_EXTRA_ATTACK_MONSTER)
	local cxa=c:GetEffectCount(EFFECT_EXTRA_ATTACK)
	if (cfn and cfn~=0) and (cxa and cxa>0) and (cxm and cxm>0) then
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()+cfn) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and c:GetAttackAnnouncedCount()<=math.ceil(cxa+cxm)
	elseif (cfn and cfn~=0) and (cxa and cxa>0) and not cxm then
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()+cfn) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and (c:GetEffectCount(EFFECT_EXTRA_ATTACK_MONSTER)==0 and c:GetAttackAnnouncedCount()<=cxa)
	elseif (cfn and cfn~=0) and (cxm and cxm>0) and not cxa then
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()+cfn) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and (c:GetEffectCount(EFFECT_EXTRA_ATTACK)==0 and c:GetAttackAnnouncedCount()<=cxm)
	elseif (cfn and cfn~=0) and (not cxa and not cxm) then
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()+cfn) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and (c:GetEffectCount(EFFECT_EXTRA_ATTACK)==0 and c:GetEffectCount(EFFECT_EXTRA_ATTACK_MONSTER)==0)
	elseif (cxa and cxa>0) and (cxm and cxm>0) and not cfn then
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and c:GetAttackAnnouncedCount()<=math.ceil(cxa+cxm)
	elseif (cxa and cxa>0) and (not cfn and not cxm) then
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and (c:GetEffectCount(EFFECT_EXTRA_ATTACK_MONSTER)==0 and c:GetAttackAnnouncedCount()<=cxa)
	elseif (cxm and cxm>0) and (not cfn and not cxa) then
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and (c:GetEffectCount(EFFECT_EXTRA_ATTACK)==0 and c:GetAttackAnnouncedCount()<=cxm)
	else
	return c:IsFaceup() and (c:GetAttackAnnouncedCount()==0 or c:GetAttackAnnouncedCount()~=c:GetBattledGroupCount()) and Duel.GetTurnPlayer()==c:GetControler() and (c:GetEffectCount(EFFECT_CANNOT_ATTACK)==0 and c:GetEffectCount(EFFECT_CANNOT_ATTACK_ANNOUNCE)==0) and (c:IsAttackPos() or c:GetEffectCount(EFFECT_DEFENSE_ATTACK)>0) and (c:GetEffectCount(EFFECT_EXTRA_ATTACK)==0 and c:GetEffectCount(EFFECT_EXTRA_ATTACK_MONSTER)==0)
	end
end
function s.negcon(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(s.filter11,0,LOCATION_MZONE,LOCATION_MZONE,nil) 
	if g:GetCount()~=0 then
	return Duel.GetCurrentPhase()==PHASE_BATTLE
	else
	return (Duel.GetCurrentPhase()>=PHASE_BATTLE_START and Duel.GetCurrentPhase()<=PHASE_BATTLE)
	end
end
function s.negcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.filter1(c)
	return c:IsFaceup() and c:IsType(TYPE_EFFECT) and not c:IsDisabled()
end
function s.filter2(c)
	return c:IsFaceup() and (c:GetAttack()~=c:GetBaseAttack() or c:GetDefense()~=c:GetBaseDefense())
end
function s.filter3(c)
	return c:IsFaceup() and c:IsType(TYPE_EFFECT)
end
function s.negtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
end
function s.negop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(s.filter3,0,LOCATION_MZONE,LOCATION_MZONE,c)
	if g:GetCount()>0 then
	local tc=g:GetFirst()
	while tc do
		--Duel.NegateRelatedChain(tc,RESET_TURN_SET)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e2)
		tc=g:GetNext()
	end end
	Duel.AdjustInstantly()
	local g2=Duel.GetMatchingGroup(s.filter2,0,LOCATION_MZONE,LOCATION_MZONE,c)
	if g2:GetCount()>0 then
	local tc2=g2:GetFirst()
	while tc2 do
		if tc2:GetAttack()~=tc2:GetBaseAttack() then
			local e1=Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_SET_ATTACK_FINAL)
			e1:SetValue(tc2:GetBaseAttack())
			e1:SetReset(RESET_EVENT+0x1fe0000)
			tc2:RegisterEffect(e1)
		end
		if tc2:GetDefense()~=tc2:GetBaseDefense() then
			local e2=Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_SINGLE)
			e2:SetCode(EFFECT_SET_DEFENSE_FINAL)
			e2:SetValue(tc2:GetBaseDefense())
			e2:SetReset(RESET_EVENT+0x1fe0000)
			tc2:RegisterEffect(e2)
		end
		tc2=g2:GetNext()
	end end
	if Duel.GetTurnPlayer()==tp then
	local val=math.max(c:GetEffectCount(EFFECT_EXTRA_ATTACK)+1,c:GetAttackAnnouncedCount()) end
	if c:GetEffectCount(EFFECT_EXTRA_ATTACK_MONSTER)>0 then
	local val=math.max(c:GetEffectCount(EFFECT_EXTRA_ATTACK)+c:GetEffectCount(EFFECT_EXTRA_ATTACK_MONSTER)+1,c:GetAttackAnnouncedCount()) end   
	if e:GetLabelObject():GetLabel()>1 then
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(1000*(e:GetLabelObject():GetLabel()-1))
	e1:SetReset(RESET_EVENT+0x1ff0000+RESET_PHASE+PHASE_BATTLE)
	c:RegisterEffect(e1) end
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_EXTRA_ATTACK)
	e2:SetValue(1)
	e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_BATTLE)
	c:RegisterEffect(e2)
end
function s.regcon(e,tp,eg,ep,ev,re,r,rp)
	local ph=Duel.GetCurrentPhase()
	return 
(ph>=PHASE_BATTLE_START and ph<=PHASE_BATTLE)
--ph==PHASE_BATTLE
	--return rp~=tp and e:GetHandler():GetFlagEffect(99)>0
end
function s.regop(e,tp,eg,ep,ev,re,r,rp)
	  local a=e:GetLabel()
	  e:SetLabel(a+1)
end
function s.regop2(e,tp,eg,ep,ev,re,r,rp)
	  e:GetLabelObject():SetLabel(0)
end