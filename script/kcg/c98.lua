--CNo.107 超銀河眼の時空龍
Duel.LoadScript("rankup_functions.lua")
local s, id = GetID()
function s.initial_effect(c)
	--xyz summon
	aux.EnableCheckRankUp(c,nil,nil,88177324)
	Xyz.AddProcedure(c,nil,9,3)
	c:EnableReviveLimit()
	--cannot destroyed
	  local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e0:SetValue(s.indes)
	c:RegisterEffect(e0)
	--negate
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(13718,9))
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1:SetType(EFFECT_TYPE_QUICK_O)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_MZONE)
	e1:SetHintTiming(0,TIMING_END_PHASE)
	e1:SetCountLimit(1)
	e1:SetCost(s.negcost)
	--e1:SetTarget(s.negtg)
	e1:SetOperation(s.negop)
	c:RegisterEffect(e1,false,REGISTER_FLAG_DETACH_XMAT)
	--atk
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(68396121,1))
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCondition(s.atkcon)
	e2:SetCost(s.atkcost)
	e2:SetTarget(s.atktg)
	e2:SetOperation(s.atkop)
	e2:SetLabel(RESET_EVENT+RESETS_STANDARD)
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_RANKUP_EFFECT)
	e3:SetLabelObject(e2)
	c:RegisterEffect(e3)	
	  
	aux.GlobalCheck(s,function() 
		local ge1=Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_CHAIN_SOLVED)
		ge1:SetOperation(s.checkop)
		Duel.RegisterEffect(ge1,0)  
		local ge2=Effect.CreateEffect(c)
		ge2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge2:SetCode(EVENT_PHASE+PHASE_DRAW)
		ge2:SetCountLimit(1)
		ge2:SetOperation(s.check2op)
		Duel.RegisterEffect(ge2,0)
	end)  
end
s.xyz_number=107
s.listed_series = {0x48}

function s.checkop(e,tp,eg,ep,ev,re,r,rp)
	local rc=re:GetHandler()
	if rc then
		rc:RegisterFlagEffect(511010207,RESET_PHASE+PHASE_END,0,1,1)
	end
end
function s.check2op(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(aux.TRUE,tp,0xff,0xff,nil)
	if g:GetCount()>0 then
		local tc=g:GetFirst()
		while tc do
			tc:RegisterFlagEffect(511010208,RESET_PHASE+PHASE_END,0,1,tc:GetLocation())
			tc:RegisterFlagEffect(511010209,RESET_PHASE+PHASE_END,0,1,tc:GetControler())
			tc:RegisterFlagEffect(511010210,RESET_PHASE+PHASE_END,0,1,tc:GetPosition())
			tc:RegisterFlagEffect(511010211,RESET_PHASE+PHASE_END,0,1,tc:GetSequence())
			tc=g:GetNext()
		end
	end
end

function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end
function s.desfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_MONSTER) and (not c:IsSetCard(0x48) or c:IsSetCard(0x1048))
end
function s.descon(e)
	local c=e:GetHandler()
	return Duel.IsExistingMatchingCard(s.desfilter,c:GetControler(),0,LOCATION_MZONE,1,c)
end

function s.negcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.filter(c)
	return c:IsFaceup() 
--and (c:IsLocation(LOCATION_SZONE) or c:IsType(TYPE_EFFECT)) and not c:IsDisabled()
end
-- function s.negtg(e,tp,eg,ep,ev,re,r,rp,chk)
-- 	if chk==0 then return true end
-- end
function s.retfilter(c,tid)
	return c:GetFlagEffect(511010207)>0
end
function s.negop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(s.filter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,c)
	local tc=g:GetFirst()
	while tc do
		--Duel.NegateRelatedChain(tc,RESET_TURN_SET)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e1)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e2)
		if tc:IsType(TYPE_TRAPMONSTER) then
			local e3=Effect.CreateEffect(c)
			e3:SetType(EFFECT_TYPE_SINGLE)
			e3:SetCode(EFFECT_DISABLE_TRAPMONSTER)
			e3:SetReset(RESET_EVENT+0x1fe0000)
			tc:RegisterEffect(e3)
		end
		tc=g:GetNext()
	end
	Duel.AdjustInstantly()
	local g2=Duel.GetMatchingGroup(s.retfilter,tp,0x9d,0x9d,c)
	local tc2=g2:GetFirst()
	while tc2 do
			local e1=Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_DISABLE)
			e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
			tc2:RegisterEffect(e1)
			local e2=Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_SINGLE)
			e2:SetCode(EFFECT_DISABLE_EFFECT)
			e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
			tc2:RegisterEffect(e2)
			Duel.AdjustInstantly()
			if tc2:GetFlagEffectLabel(511010208)==LOCATION_HAND then
				Duel.SendtoHand(tc2,tc2:GetFlagEffectLabel(511010209),REASON_EFFECT)
			elseif tc2:GetFlagEffectLabel(511010208)==LOCATION_GRAVE then
				Duel.SendtoGrave(tc2,REASON_EFFECT,tc2:GetFlagEffectLabel(511010209))
			elseif tc2:GetFlagEffectLabel(511010208)==LOCATION_REMOVED then
				Duel.Remove(tc2,tc2:GetPreviousPosition(),REASON_EFFECT,tc2:GetFlagEffectLabel(511010209))
			elseif tc2:GetFlagEffectLabel(511010208)==LOCATION_DECK then
				Duel.SendtoDeck(tc2,tc2:GetFlagEffectLabel(511010209),0,REASON_EFFECT)
			elseif tc2:GetFlagEffectLabel(511010208)==LOCATION_EXTRA then
				Duel.SendtoDeck(tc2,tc2:GetFlagEffectLabel(511010209),0,REASON_EFFECT)
			else
			if not tc2:IsImmuneToEffect(e) then
				Duel.MoveToField(tc2,tc2:GetFlagEffectLabel(511010209),tc2:GetFlagEffectLabel(511010209),tc2:GetFlagEffectLabel(511010208),tc2:GetFlagEffectLabel(511010210),true)
				Duel.MoveSequence(tc2,tc2:GetFlagEffectLabel(511010211)) end
			end
			tc2=g2:GetNext()
	end
	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(13718,11))
	e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_CHAINING)
	e5:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e5:SetRange(LOCATION_MZONE)
	e5:SetCondition(s.discon)
	e5:SetTarget(s.distg)
	e5:SetOperation(s.disop)
	e5:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	c:RegisterEffect(e5)
end
function s.aclimit(e,re,tp)
	return re:GetHandler():IsOnField()
end
function s.atkcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnCount()~=1 and Duel.GetCurrentPhase()==PHASE_MAIN1
			--and e:GetHandler():GetFlagEffect(98)~=0
end
function s.atkfilter(c)
	return c:GetAttackedCount()==0
end
function s.atkcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckReleaseGroup(tp,nil,2,e:GetHandler()) end
	local g=Duel.SelectReleaseGroup(tp,s.atkfilter,2,2,e:GetHandler())
	Duel.Release(g,REASON_COST)
end
function s.atktg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetEffectCount(EFFECT_EXTRA_ATTACK)==0 end
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) and c:IsFaceup() then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_EXTRA_ATTACK)
		e1:SetValue(2)
		e1:SetReset(RESET_EVENT+0x1ff0000+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e1)
	end
end
function s.dircon(e)
	return e:GetHandler():GetAttackAnnouncedCount()>0
end
function s.dircon2(e)
	return e:GetHandler():IsDirectAttacked()
end
 function s.discon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return not c:IsStatus(STATUS_BATTLE_DESTROYED) and not c:IsStatus(STATUS_CHAINING) and Duel.IsChainNegatable(ev) 
	  and re:GetHandler():IsOnField() and not (re:GetHandler():IsType(TYPE_PENDULUM) and re:IsHasType(EFFECT_TYPE_ACTIVATE))
end
function s.distg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetChainLimit(aux.FALSE)
end
function s.disop(e,tp,eg,ep,ev,re,r,rp)
	  local tc=re:GetHandler()
	  if not Duel.SelectYesNo(tp,aux.Stringid(13718,11)) then
	  if Duel.NegateActivation(ev) then
	  tc:CancelToGrave()
	  if not tc:IsType(TYPE_CONTINUOUS) and not tc:IsType(TYPE_EQUIP) and not tc:IsType(TYPE_FIELD) and not tc:IsType(TYPE_EFFECT) and not tc:IsType(TYPE_FLIP) and not tc:IsType(TYPE_PENDULUM) then
		 if re:IsActiveType(TYPE_MONSTER) then Duel.ChangePosition(tc,POS_FACEDOWN_DEFENSE)
		 else Duel.ChangePosition(tc,POS_FACEDOWN) end 
	  end end end
end
 function s.con(e,tp,eg,ep,ev,re,r,rp)
	  return e:GetHandler():GetSummonType()==SUMMON_TYPE_XYZ and
	  e:GetHandler():GetOverlayGroup():IsExists(Card.IsCode,1,nil,88177324) 
end
function s.op(e,tp,eg,ep,ev,re,r,rp)
	  e:GetHandler():RegisterFlagEffect(98,RESET_EVENT+0x1ff0000,0,1)
end