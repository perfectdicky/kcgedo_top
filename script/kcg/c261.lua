--ZW－天風精霊翼
function c261.initial_effect(c)
	--c:SetUniqueOnField(1,0,261)

	--equip
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(45082499,0))
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetCategory(CATEGORY_EQUIP)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCondition(c261.eqcon)
	e1:SetTarget(c261.eqtg)
	e1:SetOperation(c261.eqop)
	c:RegisterEffect(e1)

      local e3=Effect.CreateEffect(c)
      e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
      e3:SetCode(EVENT_SPSUMMON_SUCCESS)
      e3:SetRange(LOCATION_SZONE)
      e3:SetCategory(CATEGORY_ATKCHANGE)
      e3:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e3:SetCondition(c261.spcon)
	e3:SetOperation(c261.spop)
      c:RegisterEffect(e3)

      local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(9260791,1))
      e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
      e4:SetCode(EVENT_ATTACK_ANNOUNCE)
      e4:SetRange(LOCATION_SZONE)
	e4:SetCondition(c261.attcon)
	e4:SetOperation(c261.attop)
      c:RegisterEffect(e4)
end

function c261.eqcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():CheckUniqueOnField(tp)
end
function c261.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x107f)
end
function c261.eqtg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(tp) and c261.filter(chkc) end
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_SZONE)>0
		and Duel.IsExistingTarget(c261.filter,tp,LOCATION_MZONE,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EQUIP)
	Duel.SelectTarget(tp,c261.filter,tp,LOCATION_MZONE,0,1,1,nil)
end
function c261.eqop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if not c:IsRelateToEffect(e) then return end
	if c:IsLocation(LOCATION_MZONE) and c:IsFacedown() then return end
	local tc=Duel.GetFirstTarget()
	if Duel.GetLocationCount(tp,LOCATION_SZONE)<=0 or tc:GetControler()~=tp or tc:IsFacedown() or not tc:IsRelateToEffect(e) or not c:CheckUniqueOnField(tp) then
		Duel.SendtoGrave(c,REASON_EFFECT)
		return
	end
	Duel.Equip(tp,c,tc,true)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_EQUIP_LIMIT)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	e1:SetValue(c261.eqlimit)
	c:RegisterEffect(e1)
	--atkup
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_EQUIP)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetValue(800)
	e2:SetReset(RESET_EVENT+0x1fe0000)
	c:RegisterEffect(e2)
end
function c261.eqlimit(e,c)
	return c:IsSetCard(0x107f)
end

function c261.spcon(e,tp,eg,ep,ev,re,r,rp)
	return eg:FilterCount(c261.spfilter,nil,tp)>0
end
function c261.spfilter(c,tp)
	return c:IsType(TYPE_XYZ) and c:IsControler(1-tp)
end
function c261.spop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	--atkup
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_EQUIP)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetValue(1600)
	e2:SetReset(RESET_EVENT+0x1fe0000)
	c:RegisterEffect(e2)
end

function c261.attcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetEquipTarget() and eg:GetFirst()==e:GetHandler():GetEquipTarget()
end
function c261.attop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
      if e:GetHandler():GetEquipTarget() then
      Duel.Overlay(e:GetHandler():GetEquipTarget(),c) end
end
