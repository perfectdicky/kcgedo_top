--No.88 ギミック·パペット－デステニー·レオ
local s, id = GetID()
function s.initial_effect(c)
	--c:EnableCounterPermit(0x302b)
	--xyz summon
	Xyz.AddProcedure(c,nil,8,3)
	c:EnableReviveLimit()
	--cannot destroyed
      local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e0:SetValue(s.indes)
	c:RegisterEffect(e0)
	--counter
	local e1=Effect.CreateEffect(c)
	--e1:SetCategory(CATEGORY_COUNTER)
	e1:SetDescription(aux.Stringid(48995978,0))
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetCountLimit(1)
	e1:SetRange(LOCATION_MZONE)
	--e1:SetCondition(s.condition)
	--e1:SetCost(s.cost)
	e1:SetTarget(s.target)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1)
end
s.xyz_number=88
s.listed_series = {0x48}

function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
      and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end

function s.filter(c)
	return c:GetSequence()<5
end
function s.condition(e,tp,eg,ep,ev,re,r,rp)
	return not Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_SZONE,0,1,nil)
end
function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetCurrentPhase()==PHASE_MAIN1 end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_BP)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET+EFFECT_FLAG_OATH)
	e1:SetTargetRange(1,0)
	e1:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e1,tp)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_EFFECT) end
		--and e:GetHandler():IsCanAddCounter(0x302b,1) end
	--Duel.SetOperationInfo(0,CATEGORY_COUNTER,nil,1,0,0x302b)
end
function s.mgfilter(c)
	return c:GetFlagEffect(151)~=0
end
function s.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) and c:IsFaceup() and c:RemoveOverlayCard(tp,1,1,REASON_EFFECT) then
		--c:AddCounter(0x302b,1)
		--if c:GetCounter(0x302b)==3 then      
            e:GetHandler():RegisterFlagEffect(151,RESET_EVENT+0x1ff0000,0,1)    
            if e:GetHandler():GetFlagEffect(151)>=2 and e:GetHandler():GetOverlayCount()==0 then
		local WIN_REASON_DESTINY_LEO=0x17
		Duel.Win(c:GetControler(),WIN_REASON_DESTINY_LEO)
	      end
	end
end
