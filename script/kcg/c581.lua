--太阳神之翼神龙
function c581.initial_effect(c)
	c:EnableReviveLimit()

	--cannot special summon
	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SPSUMMON_CONDITION)
	c:RegisterEffect(e0)

	--特殊召唤方式
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(c581.spcon)
	e1:SetOperation(c581.spop)
	c:RegisterEffect(e1)
	
	local e5=Effect.CreateEffect(c)
	e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	e5:SetOperation(c581.atkop)
	c:RegisterEffect(e5)
	
	--支付LP1000破坏对方场上所有怪兽
	local e6=Effect.CreateEffect(c)
	e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e6:SetDescription(aux.Stringid(10000011,1))
	e6:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e6:SetType(EFFECT_TYPE_QUICK_O)
	e6:SetCode(EVENT_FREE_CHAIN)
	e6:SetRange(LOCATION_ONFIELD)
	e6:SetCountLimit(1)
	e6:SetCost(c581.descost)
	e6:SetTarget(c581.destg)
	e6:SetOperation(c581.desop)
	c:RegisterEffect(e6)

	--解放怪兽增加攻守
	local e70=Effect.CreateEffect(c)
	e70:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e70:SetDescription(aux.Stringid(10000011,0))
	e70:SetCategory(CATEGORY_ATKCHANGE)
	e70:SetType(EFFECT_TYPE_QUICK_O)
	  e70:SetCode(EVENT_FREE_CHAIN)
	e70:SetRange(LOCATION_ONFIELD)
	e70:SetCountLimit(1)
	e70:SetCost(c581.otkcost2)
	e70:SetOperation(c581.otkop2)
	c:RegisterEffect(e70)

	--LP减至1增加攻守
	local e7=Effect.CreateEffect(c)
	e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e7:SetDescription(aux.Stringid(110000010,2))
	e7:SetCategory(CATEGORY_ATKCHANGE)
	e7:SetType(EFFECT_TYPE_QUICK_O)
	  e7:SetCode(EVENT_FREE_CHAIN)
	e7:SetRange(LOCATION_MZONE)
	e7:SetCountLimit(1)
	e7:SetCost(c581.otkcost)
	e7:SetOperation(c581.otkop)
	--c:RegisterEffect(e7)
	
	--不受陷阱效果以及魔法效果怪兽生效一回合
	local e82=Effect.CreateEffect(c)
	e82:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e82:SetCode(EVENT_CHAINING)
	e82:SetRange(LOCATION_ONFIELD)
	--e82:SetCondition(c581.sdcon2)
	e82:SetOperation(c581.sdop2)
	--c:RegisterEffect(e82)
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e8:SetRange(LOCATION_ONFIELD)
	e8:SetCode(EFFECT_IMMUNE_EFFECT)
	e8:SetValue(c581.efilterr)
	--c:RegisterEffect(e8)
	local e83=Effect.CreateEffect(c)
	e83:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e83:SetRange(LOCATION_ONFIELD)
	e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_CANNOT_DISABLE)
	e83:SetCountLimit(1)
	e83:SetCode(EVENT_PHASE+PHASE_END)
	e83:SetOperation(c581.setop)
	--C:RegisterEffect(e83) 

	--特召后成为攻击目标
	local e17=Effect.CreateEffect(c)
	e17:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e17:SetRange(LOCATION_MZONE)
	e17:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e17:SetCode(EVENT_ATTACK_ANNOUNCE)
	e17:SetCondition(c581.atcon2)
	e17:SetOperation(c581.atop)
	c:RegisterEffect(e17)   
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_FIELD)
	e9:SetRange(LOCATION_ONFIELD)
	e9:SetTargetRange(LOCATION_ONFIELD,0)
	e9:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_CANNOT_DISABLE)
	e9:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
	e9:SetCondition(c581.atcon)
	e9:SetTarget(c581.atlimit)
	e9:SetValue(Auxiliary.imval1)
	--c:RegisterEffect(e9)
	--destroy replace
	local e72=Effect.CreateEffect(c)
	e72:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e72:SetCode(EFFECT_DESTROY_REPLACE)
	e72:SetRange(LOCATION_ONFIELD)
	e72:SetCondition(c581.atcon)
	e72:SetTarget(c581.reptg)
	e72:SetValue(c581.repval)
	e72:SetOperation(c581.repop)
	c:RegisterEffect(e72)
	
	--通常召唤、特殊召唤、反转召唤不会被无效化
	local e10=Effect.CreateEffect(c)
	e10:SetType(EFFECT_TYPE_SINGLE)
	e10:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
	e10:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e10)
	local e11=e10:Clone()
	e11:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	c:RegisterEffect(e11)
	local e12=e11:Clone()
	e12:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
	c:RegisterEffect(e12)
	
	--同时当作创造神族怪兽使用
	local e14=Effect.CreateEffect(c)
	e14:SetType(EFFECT_TYPE_SINGLE)
	e14:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e14:SetCode(EFFECT_ADD_RACE)
	e14:SetValue(RACE_CREATORGOD+RACE_DRAGON)
	c:RegisterEffect(e14)
	
	--不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变成里侧表示、作为特殊召唤素材
	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_ONFIELD)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e101)
	local e102=e101:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103=e102:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104=e103:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e105=e104:Clone()
	e105:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e105)
	local e106=e105:Clone()
	e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e106)
	local e107=e106:Clone()
	e107:SetCode(EFFECT_CANNOT_TURN_SET)
	c:RegisterEffect(e107)
	local e108=e107:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109=e108:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110=e109:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e111=e109:Clone()
	e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e111)

	local e21=Effect.CreateEffect(c)
	e21:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e21:SetRange(LOCATION_ONFIELD)
	e21:SetCode(EFFECT_IMMUNE_EFFECT)
	e21:SetType(EFFECT_TYPE_SINGLE)
	e21:SetValue(c581.eefilter)
	e21:SetCondition(c581.ocon)
	c:RegisterEffect(e21)
	local e22=Effect.CreateEffect(c)
	e22:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e22:SetRange(LOCATION_ONFIELD)
	e22:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e22:SetCountLimit(1)
	e22:SetCode(EVENT_PHASE+PHASE_END)
	e22:SetCondition(c581.ocon2)
	e22:SetOperation(c581.ermop)
	c:RegisterEffect(e22)   

	local e400=Effect.CreateEffect(c)
	e400:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e400:SetCode(EVENT_SUMMON_SUCCESS)
	e400:SetOperation(c581.sumsuc)
	c:RegisterEffect(e400)
end

function c581.sumsuc(e,tp,eg,ep,ev,re,r,rp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

function c581.spmfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsReleasableByEffect()
end-----------------------------------------------------------
function c581.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	local a=Duel.GetMatchingGroupCount(c581.spmfilter,tp,LOCATION_MZONE,0,nil)
	return a>=3 and (Duel.GetLocationCount(tp,LOCATION_MZONE)>=-3)
end
function c581.spop(e,tp,eg,ep,ev,re,r,rp,c)
	if Duel.GetLocationCount(tp, LOCATION_MZONE)<-3 then return end	
	local g=Duel.SelectMatchingCard(c:GetControler(),c581.spmfilter,c:GetControler(),LOCATION_ONFIELD,0,3,3,nil)
	Duel.Release(g,REASON_COST)
	  local g2=Duel.GetOperatedGroup()
	  local tc=g2:GetFirst()
	  local tatk=0
	  local tdef=0
	  while tc do
	  local atk=tc:GetAttack()
	  local def=tc:GetDefense()
	  if atk<0 then atk=0 end
	  if def<0 then def=0 end
	  tatk=tatk+atk 
	  tdef=tdef+def 
	  tc=g2:GetNext() end
	Original_ATK=tatk
	Original_DEF=tdef
end

function c581.ofilter2(c)
	return c:IsFaceup() and c:IsSetCard(0x900) and c:IsType(TYPE_FIELD)
end
function c581.ocon(e)
	return Duel.IsExistingMatchingCard(c581.ofilter2,e:GetHandlerPlayer(),LOCATION_SZONE,LOCATION_SZONE,1,nil)
end

function c581.ocon2(e)
	return not Duel.IsExistingMatchingCard(c581.ofilter2,e:GetHandlerPlayer(),LOCATION_SZONE,LOCATION_SZONE,1,nil)
end
function c581.ermop(e,tp,eg,ep,ev,re,r,rp,c)
			Duel.Remove(e:GetHandler(),POS_FACEUP,REASON_RULE)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c581.tgcon(e,tp,eg,ep,ev,re,r,rp)
	return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_SPECIAL)==SUMMON_TYPE_SPECIAL
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c581.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetValue(Original_ATK)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EFFECT_SET_DEFENSE)
	e2:SetValue(Original_DEF)
	c:RegisterEffect(e2)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c581.descost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckLPCost(tp,1000) end
	Duel.PayLPCost(tp,1000)
end

function c581.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(nil,tp,0,LOCATION_MZONE,1,nil) end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_OATH)
	e1:SetCode(EFFECT_CHANGE_CODE)
	e1:SetValue(10000049)
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	--e:GetHandler():RegisterEffect(e1)
	local g=Duel.GetMatchingGroup(nil,tp,0,LOCATION_MZONE,nil)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,g:GetCount()*500)
end

function c581.desop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(nil,tp,0,LOCATION_MZONE,nil)
	local ct=Duel.Destroy(g,REASON_RULE)
	Duel.Damage(1-tp,ct*500,REASON_EFFECT)
end
function c581.eefilter(e,te)
	return te:GetOwner()~=e:GetOwner()
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c581.otkcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLP(tp)>1 end
	local lp=Duel.GetLP(tp)
	Duel.SetLP(tp,1)
	e:SetLabel(lp-1)
end
function c581.otkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsFaceup() and c:IsRelateToEffect(e) then
		--c:RegisterFlagEffect(10000012,RESET_EVENT+0x1ff0000,nil,1)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e1:SetRange(LOCATION_MZONE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(e:GetLabel())
		e1:SetReset(RESET_EVENT+0x1fe0000)
		c:RegisterEffect(e1)
		local e2=e1:Clone()
		e2:SetCode(EFFECT_UPDATE_DEFENSE)
		c:RegisterEffect(e2)
		local e3=e1:Clone()
		e3:SetCode(EFFECT_ADD_TYPE)
		e3:SetValue(TYPE_FUSION)
		c:RegisterEffect(e3)
		  local e4=Effect.CreateEffect(c)
		  e4:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
		  e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		  e4:SetCode(EVENT_ADJUST)
		e4:SetRange(LOCATION_MZONE)
		  e4:SetOperation(c581.lpop)
		  e4:SetReset(RESET_EVENT+0x1fe0000)
		  c:RegisterEffect(e4) 
		local e5=e4:Clone()
		e5:SetCode(EVENT_CHAIN_SOLVED)
		c:RegisterEffect(e5) 
		local e6=e5:Clone()
		  e6:SetOperation(c581.lpop2)
		c:RegisterEffect(e6) 
	end
end
function c581.lpop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	  local ttp=c:GetControler()
	local lp=Duel.GetLP(ttp)
	if c:IsType(TYPE_FUSION) and lp>1 and c:GetFlagEffect(10000082)==0 then
		  Duel.SetLP(ttp,1)
			local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e1:SetRange(LOCATION_MZONE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(lp-1)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		c:RegisterEffect(e1)
		local e2=e1:Clone()
		e2:SetCode(EFFECT_UPDATE_DEFENSE)
		c:RegisterEffect(e2)
	end 
end
function c581.lpop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	  local ttp=c:GetControler()
	if c:GetFlagEffect(10000082)~=0 then
			c:ResetFlagEffect(10000082)
	end 
end
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function c581.otkcost2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckReleaseGroup(tp,nil,1,e:GetHandler()) end
	local g=Duel.SelectReleaseGroup(tp,nil,1,99,e:GetHandler())
	local tc=g:GetFirst()
    local tatk = 0
    local tdef = 0
    while tc do
        local atk = tc:GetAttack()
        local def = tc:GetDefense()
        if atk < 0 then
            atk = 0
        end
        if def < 0 then
            def = 0
        end
        tatk = tatk + atk
        tdef = tdef + def
        tc = g:GetNext()
    end
    e:SetLabelObject({tatk,tdef})
	Duel.Release(g,REASON_COST)
end

function c581.otkop2(e,tp,eg,ep,ev,re,r,rp)
    local c = e:GetHandler()
    local tatk=e:GetLabelObject()[1]
    local tdef=e:GetLabelObject()[2]
    if c:IsFaceup() and c:IsRelateToEffect(e) then
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
        e1:SetRange(LOCATION_MZONE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(tatk)
        e1:SetReset(RESET_EVENT + 0x1fe0000)
        c:RegisterEffect(e1)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_UPDATE_DEFENSE)
        e2:SetValue(tdef)
        c:RegisterEffect(e2)
    end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c581.efilterr(e,te)   
	local c=e:GetHandler()  
	  local tc=te:GetOwner()
	return te:IsActiveType(TYPE_TRAP)  
	  or ((te:IsActiveType(TYPE_MONSTER) or te:IsActiveType(TYPE_SPELL)) and tc~=e:GetOwner()  
			 and (( te:GetType()==EFFECT_TYPE_FIELD or te:GetType()==EFFECT_TYPE_EQUIP)  
			 and Duel.GetTurnCount()-tc:GetTurnID()>=1 )
			  or (te:GetCode()==EFFECT_CANNOT_ATTACK or te:GetCode()==EFFECT_CANNOT_ATTACK_ANNOUNCE) )
			 --and tc:GetFlagEffect(100000120)~=0 and tc:GetFlagEffectLabel(100000120)==c:GetFieldID())
end

function c581.sdcon2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()  
	local tc=re:GetHandler()
	return re:GetOwner()~=e:GetOwner()  
	  and (tc:GetFlagEffect(100000120)==0 or (tc:GetFlagEffect(100000120)~=0 and tc:GetFlagEffectLabel(100000120)~=c:GetFieldID()))
end
function c581.sdop2(e,tp,eg,ep,ev,re,r,rp)   
	local c=e:GetHandler()
	local tc=re:GetHandler()	
			--tc:RegisterFlagEffect(10000012,RESET_EVENT+0x1fe00000,0,1) 
	  if re:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then tc:RegisterFlagEffect(8888,0,0,0) end
	local e83=Effect.CreateEffect(c)
	e83:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e83:SetRange(LOCATION_MZONE)
	e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_CANNOT_DISABLE)
	e83:SetCountLimit(1)
	e83:SetCode(EVENT_PHASE+PHASE_END)
	  e83:SetLabelObject(re)
	e83:SetOperation(c581.setop2)
	--c:RegisterEffect(e83) 
end
function c581.setop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local te=e:GetLabelObject()
	  local tc=te:GetHandler() 
	  if tc:GetFlagEffect(8888)~=0 then return end
	  if tc:GetFlagEffect(10000012)==0 then return end
	tc:RegisterFlagEffect(100000120,RESET_EVENT+0x1fe00000,0,1) 
	  tc:SetFlagEffectLabel(100000120,c:GetFieldID())
end
function c581.setop(e,tp,eg,ep,ev,re,r,rp,c)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(nil,tp,0xff,0xff,c)
			local tc=g:GetFirst() 
			while tc do
			if tc:GetOriginalCode()~=c:GetOriginalCode() and tc:GetFlagEffect(8888)==0 then
			c:ResetEffect(tc:GetOriginalCode(),RESET_CARD) end
			tc=g:GetNext() end
end

function c581.efilter(e,te)   
	  if (te:IsActiveType(TYPE_MONSTER) or te:IsActiveType(TYPE_SPELL) and te:GetHandler()~=e:GetOwner())  then
--and (te:GetHandler():GetFlagEffect(100000120)==0 or (te:GetHandler():GetFlagEffect(100000120)~=0  and te:GetHandler():GetFlagEffectLabel(100000120)~=e:GetHandler():GetFieldID()))
			 --and (te:GetHandler():GetFlagEffect(10000012)==0 or (te:GetHandler():GetFlagEffect(10000012)~=0 and te:GetHandler():GetFlagEffectLabel(10000012)~=e:GetHandler():GetFieldID())) then
	  te:GetHandler():RegisterFlagEffect(10000012,RESET_EVENT+0x1ff0000,0,1) 
	  te:GetHandler():SetFlagEffectLabel(10000012, e:GetHandler():GetFieldID()) end
	 return te:IsActiveType(TYPE_TRAP)  and te:GetHandler():GetFlagEffect(100000120)~=0 and te:GetHandler():GetFlagEffect(10000012)==0 
--and (te:GetHandler()~=e:GetOwner() or (te:GetHandler():GetFlagEffect(100000120)~=0 and te:GetHandler():GetFlagEffectLabel(100000120)==e:GetHandler():GetFieldID()))
end
function c581.setfilter(c,tc) 
	  return c:GetFlagEffect(10000012)~=0 and c:GetFlagEffectLabel(10000012)==tc:GetFieldID()
end
function c581.setop2(e,tp,eg,ep,ev,re,r,rp,c)
	  local g=Duel.GetMatchingGroup(c581.setfilter,tp,LOCATION_ONFIELD+LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_HAND+LOCATION_DECK+LOCATION_EXTRA,LOCATION_ONFIELD+LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_HAND+LOCATION_DECK+LOCATION_EXTRA,e:GetHandler(),e:GetHandler())
	  local tc=g:GetFirst()
	  while tc do
	  tc:ResetFlagEffect(10000012)
	  tc:RegisterFlagEffect(100000120,RESET_EVENT+0x1ff0000,0,1)
	  tc:SetFlagEffectLabel(100000120, e:GetHandler():GetFieldID())
	  tc=g:GetNext() end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c581.atcon(e)
	return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_SPECIAL)==SUMMON_TYPE_SPECIAL and e:GetHandler():GetTurnID()==Duel.GetTurnCount()
end
function c581.atcon2(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetAttacker()
	local tc2=Duel.GetAttackTarget()
	return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_SPECIAL)==SUMMON_TYPE_SPECIAL and tc:IsFaceup() and tc:IsControler(1-tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2~=e:GetHandler()
end
function c581.atop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetAttacker()
	local tc2=Duel.GetAttackTarget()	
	if tc:IsFaceup() and tc:IsControler(1-tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2~=e:GetHandler() then 
		Duel.ChangeAttackTarget(e:GetHandler())
	end
end

function c581.atlimit(e,c)
	return c~=e:GetHandler()
end

function c581.repfilter(c,tc,tp)
	return c:IsControler(tp) and c~=tc and c:IsLocation(LOCATION_MZONE) and (c:IsReason(REASON_EFFECT) or c:IsReason(REASON_RULE)) and not c:IsReason(REASON_REPLACE) and not c:IsStatus(STATUS_DESTROY_CONFIRMED+STATUS_BATTLE_DESTROYED)
end
function c581.reptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:IsExists(c581.repfilter,1,e:GetHandler(),e:GetHandler(),tp) and not e:GetHandler():IsStatus(STATUS_DESTROY_CONFIRMED+STATUS_BATTLE_DESTROYED) end
	return Duel.SelectYesNo(tp,aux.Stringid(19333131,0))
end
function c581.repval(e,c)
	return c581.repfilter(c,e:GetHandler(),e:GetHandlerPlayer())
end
function c581.repop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Destroy(e:GetHandler(),REASON_RULE+REASON_REPLACE)
end
