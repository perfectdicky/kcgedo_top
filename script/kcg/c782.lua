--GO－DDD神零王ゼロゴッド・レイジ
function c782.initial_effect(c)
	--pendulum summon
	Pendulum.AddProcedure(c)
	--c:EnableReviveLimit()

	--avoid damage
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CHANGE_DAMAGE)
	e1:SetRange(LOCATION_PZONE)
	e1:SetTargetRange(1,0)
	e1:SetCondition(c782.damcon)
	e1:SetValue(c782.damval)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EFFECT_NO_EFFECT_DAMAGE)
	c:RegisterEffect(e2)

	--get effect
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(40227329,3))
	e3:SetType(EFFECT_TYPE_IGNITION)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCost(c782.effcost)
	--e3:SetCondition(c782.effcon)  
	e3:SetOperation(c782.effop)
	c:RegisterEffect(e3)

	--attack
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e9:SetCode(EVENT_ATTACK_ANNOUNCE)
	e9:SetOperation(c782.atkafter)
	c:RegisterEffect(e9)
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e4:SetCode(EVENT_DAMAGE_STEP_END)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCondition(c782.atkcon)
	e4:SetOperation(c782.atkop)
	c:RegisterEffect(e4)

	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e8:SetCode(EVENT_BE_BATTLE_TARGET)
	e8:SetOperation(c782.atkop3)
	c:RegisterEffect(e8)

	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e5:SetValue(1)
	--c:RegisterEffect(e5)
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE)
	e6:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
	e6:SetValue(1)
	--c:RegisterEffect(e6)

	--reduce tribute
	local e7=Effect.CreateEffect(c)
	e7:SetDescription(aux.Stringid(40227329,4))
	e7:SetType(EFFECT_TYPE_FIELD)
	e7:SetTargetRange(LOCATION_HAND,0)
	e7:SetCode(EFFECT_SUMMON_PROC)
	e7:SetRange(LOCATION_PZONE)
	e7:SetCountLimit(1)
	e7:SetCondition(c782.ntcon)
	e7:SetTarget(c782.nttg)
	c:RegisterEffect(e7)
end

function c782.ddfilter(c)
	return c:IsFaceup() and c:IsSetCard(0x20af)
end

function c782.damcon(e)
	return e:GetHandler():GetFlagEffect(40227329)==0
end
function c782.damval(e,re,val,r,rp,rc)
	local c=e:GetHandler()
	if bit.band(r,REASON_EFFECT)~=0 and c:GetFlagEffect(40227329)==0 then
		c:RegisterFlagEffect(40227329,RESET_PHASE+PHASE_END,0,1)
		return 0
	end
	return val
end

function c782.ntcon(e,c,minc)
	if c==nil then return true end
	return minc==0 and Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0
end
function c782.nttg(e,c)
	return c:IsLevelAbove(5) and c:IsSetCard(0xaf)
end

function c782.effcost(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=Duel.GetReleaseGroup(tp)
	g:RemoveCard(e:GetHandler())
	if chk==0 then return g:GetCount()>=1 and g:FilterCount(Card.IsReleasable,nil)==g:GetCount() end
	Duel.Release(g,REASON_COST)
	local g1=Duel.GetOperatedGroup()
	if g1:GetCount()<1 then return end
	local g2=g1:Filter(Card.IsPreviousSetCard,nil,0x20af)
	if g2:GetCount()<1 then return end
	e:SetLabel(g2:GetCount())
end
function c782.effcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return not (c:IsHasEffect(EFFECT_DIRECT_ATTACK) and Duel.GetFlagEffect(tp,40227330)>0 and Duel.GetFlagEffect(tp,40227331)>0)
end
function c782.effop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local gcount=e:GetLabel()
	if gcount<1 then
	elseif gcount<3 then
		local e8=Effect.CreateEffect(c)
		e8:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
		e8:SetCode(EVENT_ATTACK_ANNOUNCE)
		e8:SetOperation(c782.atkop2)
		e8:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e8)
	elseif gcount==3 then
		local e4=Effect.CreateEffect(c)
		e4:SetType(EFFECT_TYPE_FIELD)
		e4:SetCode(EFFECT_IGNORE_BATTLE_TARGET)
		e4:SetRange(LOCATION_MZONE)
		e4:SetTargetRange(0,LOCATION_MZONE)
		e4:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e4)  
		local e1=e4:Clone()
		e1:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
		c:RegisterEffect(e1)  
		local e2=e4:Clone()
		e2:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
		c:RegisterEffect(e2)  
		local e3=e4:Clone()
		e3:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
		c:RegisterEffect(e3) 
		local e5=e4:Clone()
		e5:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
		c:RegisterEffect(e5)
		local e6=e4:Clone()
		e6:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
		c:RegisterEffect(e6)
		local e7=e4:Clone()
		e7:SetCode(EFFECT_UNRELEASABLE_EFFECT) 
		c:RegisterEffect(e7)
		local e10=e4:Clone()
		e10:SetCode(EFFECT_UNRELEASABLE_NONSUM) 
		c:RegisterEffect(e10)
		local e11=e4:Clone()
		e11:SetCode(EFFECT_UNRELEASABLE_SUM) 
		c:RegisterEffect(e11)
		local e8=e4:Clone()
		e8:SetCode(EFFECT_CANNOT_USE_AS_COST)
		c:RegisterEffect(e8)
		local e9=Effect.CreateEffect(c)
		e9:SetType(EFFECT_TYPE_FIELD)
		e9:SetCode(EFFECT_HAND_LIMIT)
		e9:SetRange(LOCATION_MZONE)
		e9:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e9:SetTargetRange(0,1)
		e9:SetValue(100)
		c:RegisterEffect(e9)
		Duel.RegisterFlagEffect(1-tp,782,RESET_PHASE+PHASE_END,0,1)
	else
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetCode(EFFECT_DIRECT_ATTACK)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		--c:RegisterEffect(e1)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD)
		e2:SetCode(EFFECT_CANNOT_ACTIVATE)
		e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e2:SetTargetRange(0,1)
		e2:SetValue(c782.aclimit1)
		e2:SetReset(RESET_PHASE+PHASE_END)
		--Duel.RegisterEffect(e2,tp)
		local tg=Duel.GetFieldGroup(tp,0,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_ONFIELD)
		local tg2=tg:Filter(function(c) return not c:IsHasEffect(EFFECT_NECRO_VALLEY) end,nil)
		Duel.Exile(tg2,REASON_EFFECT)
		--Duel.RegisterFlagEffect(tp,40227330,RESET_PHASE+PHASE_END,0,1)
		--Duel.RegisterEffect(e3,tp)
		--Duel.RegisterFlagEffect(tp,40227331,RESET_PHASE+PHASE_END,0,1)
	end
end

function c782.adjustop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	--local pg=e:GetLabelObject()
	local dg=Duel.GetMatchingGroup(nil,1-tp,LOCATION_ONFIELD+LOCATION_HAND+LOCATION_GRAVE,0,c)
	for tc in aux.Next(dg) do 
	local og=tc:GetOverlayGroup()
	local ogchk=0
	if og:GetCount()>0 then
	for ac in aux.Next(og) do 
	   Duel.Remove(ac,POS_FACEUP,REASON_RULE) Duel.BreakEffect()
	   ac:RegisterFlagEffect(783,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,tc:GetOriginalCode())
	   ac:RegisterFlagEffect(tc:GetOriginalCode(),RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,tc:GetFieldID())
	end
	ogchk=tc:GetFieldID()
	end
	Duel.Remove(tc,tc:GetPosition(),REASON_RULE+REASON_TEMPORARY) Duel.BreakEffect()
	if ogchk~=0 then tc:RegisterFlagEffect(tc:GetOriginalCode(),RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,ogchk) end
	if tc:IsPreviousLocation(LOCATION_ONFIELD) then
	tc:RegisterFlagEffect(782,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,1) end 
	if tc:IsPreviousLocation(LOCATION_GRAVE) then
	tc:RegisterFlagEffect(782,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,2) end 
	if tc:IsPreviousLocation(LOCATION_HAND) and tc:GetPreviousControler()==tp then
	tc:RegisterFlagEffect(782,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,3) end 
	if tc:IsPreviousLocation(LOCATION_HAND) and tc:GetPreviousControler()==1-tp then
	tc:RegisterFlagEffect(782,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1,4) end 
	--pg:Clear()  
	--pg:Merge(dg)
	Duel.Readjust() end 
end

function c782.efilter(e,te)
	return te~=e:GetLabelObject()
end
function c782.ttfilter(c)
	return c:GetFlagEffect(782)~=0
end
function c782.ttfilter2(c)
	return c:GetFlagEffect(783)~=0
end
function c782.adjustop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local b=e:GetLabelObject()
	b:Reset()

	local dg=Duel.GetMatchingGroup(c782.ttfilter,tp,LOCATION_REMOVED,LOCATION_REMOVED,nil)
	for tc in aux.Next(dg) do 
	   if tc:GetFlagEffectLabel(782)==1 then Duel.ReturnToField(tc) Duel.BreakEffect() end
	   if tc:GetFlagEffectLabel(782)==2 then Duel.SendtoGrave(tc,REASON_RULE) Duel.BreakEffect() end
	   if tc:GetFlagEffectLabel(782)==3 then Duel.SendtoHand(tc,tp,REASON_RULE) Duel.BreakEffect() end
	   if tc:GetFlagEffectLabel(782)==4 then Duel.SendtoHand(tc,1-tp,REASON_RULE) Duel.BreakEffect() end

	   local og=Duel.GetMatchingGroup(c782.ttfilter2,tp,LOCATION_REMOVED,LOCATION_REMOVED,nil)
	   for ac in aux.Next(og) do 
		  if tc:GetFlagEffectLabel(tc:GetOriginalCode()) and ac:GetFlagEffectLabel(783)==tc:GetOriginalCode() and ac:GetFlagEffectLabel(tc:GetOriginalCode())==tc:GetFlagEffectLabel(tc:GetOriginalCode()) then 
			 Duel.Overlay(tc,ac) Duel.BreakEffect() 
		  end
	   end
	end


end

function c782.atkop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local at=Duel.GetAttackTarget()  
	if at and at:IsFaceup() and at:IsControler(1-tp) and at:IsRelateToBattle() and at:GetAttack()>0 and not at:IsImmuneToEffect(e) then
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK_FINAL)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_DAMAGE)
	e1:SetValue(0)
	at:RegisterEffect(e1) 
	end
end
function c782.atkop3(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local at=Duel.GetAttacker()
	if at and at:IsFaceup() and at:IsControler(1-tp) and c==Duel.GetAttackTarget() and at:IsRelateToBattle() and at:GetAttack()>0 and not at:IsImmuneToEffect(e) then
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK_FINAL)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_DAMAGE)
	e1:SetValue(0)
	at:RegisterEffect(e1) 
	end
end

function c782.aclimit1(e,re,tp)
	return re:GetActivateLocation()==LOCATION_SZONE or re:GetActivateLocation()==LOCATION_GRAVE or re:GetActivateLocation()==LOCATION_HAND
end
function c782.aclimit2(e,re,tp)
	return re:GetActivateLocation()==LOCATION_GRAVE or re:GetActivateLocation()==LOCATION_HAND
end

function c782.atkafter(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	c:RegisterFlagEffect(783,RESET_EVENT+0x1fe0000,0,1) 
end
function c782.atkcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(783)>0
end
function c782.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	Duel.SetLP(1-tp,0)
end
