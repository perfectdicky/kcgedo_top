--神之进化
function c10000083.initial_effect(c)
	
	--场上幻神兽族怪兽变为创造神族怪兽并加攻防
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_ATKCHANGE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c10000083.target)
	e1:SetOperation(c10000083.activate)
	c:RegisterEffect(e1)
	
	--效果不会被无效化
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e2)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c10000083.filter(c)
	return c:IsFaceup() and c:IsRace(RACE_DIVINE) and not c:IsRace(RACE_CREATORGOD)
	--and c:IsCode(10000000,10000020)
end

function c10000083.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and c10000083.filter(chkc) end
	if chk==0 then return Duel.IsExistingMatchingCard(c10000083.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil) end
end

function c10000083.activate(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
	local tc=Duel.SelectMatchingCard(tp,c10000083.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,nil):GetFirst()
	if tc then
		local type=tc:GetOriginalType()
		local code=0
		if tc:IsCode(10000000,10000020) then
			if tc:IsCode(10000000) then code=820 end
			if tc:IsCode(10000020) then code=823 end	
			tc:SetEntityCode(code,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
			tc:SetCardData(CARDDATA_TYPE, type)
		end
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(1000)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1,true)
		local e2=e1:Clone()
		e2:SetCode(EFFECT_UPDATE_DEFENSE)
		e2:SetValue(1000)
		tc:RegisterEffect(e2,true)
		if not tc:IsRace(RACE_CREATORGOD) then
			local e3=e1:Clone()
			e3:SetCode(EFFECT_ADD_RACE)
			e3:SetValue(RACE_CREATORGOD)
			tc:RegisterEffect(e3,true)
		end	
	end
end
