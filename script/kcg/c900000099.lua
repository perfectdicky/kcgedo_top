--精灵兽 迪尔邦多
function c900000099.initial_effect(c)
	
	--不能成为对方的卡的效果对象
	--local e1=Effect.CreateEffect(c)
	--e1:SetType(EFFECT_TYPE_SINGLE)
	--e1:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
	--e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	--e1:SetRange(LOCATION_MZONE)
	--e1:SetValue(c900000099.tgvalue)
	--c:RegisterEffect(e1)
	
	--1回合1次不被战斗破坏
	--local e2=Effect.CreateEffect(c)
	--e2:SetType(EFFECT_TYPE_SINGLE)
	--e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	--e2:SetRange(LOCATION_MZONE)
	--e2:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
	--e2:SetCountLimit(1)
	--e2:SetValue(c900000099.valcon)
	--c:RegisterEffect(e2)

	local e6=Effect.CreateEffect(c)
	e6:SetCode(EVENT_ADJUST)
	e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e6:SetRange(LOCATION_MZONE)
	e6:SetTarget(c900000099.adtg)
	c:RegisterEffect(e6)
	local e7=e6:Clone()
	e7:SetCode(EVENT_CHAIN_END)
	e7:SetTarget(c900000099.adtg2)
	c:RegisterEffect(e7)
	
	--战斗破坏对方怪兽加一半攻击
	local e3=Effect.CreateEffect(c)
	e3:SetCode(EVENT_BATTLE_DESTROYING)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetCondition(c900000099.upcon)
	e3:SetTarget(c900000099.uptg)
	e3:SetOperation(c900000099.upop)
	c:RegisterEffect(e3)
	
	--战斗破坏对方怪兽可以吸收其效果
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(900000099,2))
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCode(EVENT_BATTLE_DESTROYING)
	e4:SetCondition(c900000099.copycon)
	e4:SetOperation(c900000099.copyop)
	c:RegisterEffect(e4)
	
	--有场地存在时不能把这张卡作为攻击对象
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e5:SetRange(LOCATION_MZONE)
	e5:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
	e5:SetCondition(c900000099.atkcon)
	e5:SetValue(Auxiliary.imval1)
	c:RegisterEffect(e5)
end
---------------------------------------------------------------------------------------------
function c900000099.adfilter(c)
	return c:GetFlagEffect(900000099)==0 and c:IsCode(24094653)
end
function c900000099.adtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c900000099.adfilter,tp,0xff,0xff,1,nil)and Duel.GetFlagEffect(0,900000099)==0 and Duel.GetFlagEffect(1,900000099)==0 end
	local c=e:GetHandler()
	Duel.RegisterFlagEffect(0,900000099,0,0,0) Duel.RegisterFlagEffect(1,900000099,0,0,0)
	local g=Duel.GetMatchingGroup(c900000099.adfilter,tp,0xff,0xff,nil)
	for tc in aux.Next(g) do
	  local e3=Effect.CreateEffect(c)
	  e3:SetDescription(aux.Stringid(13719,15))
	  e3:SetType(EFFECT_TYPE_ACTIVATE)
	  e3:SetProperty(EFFECT_FLAG_CARD_TARGET)
	  e3:SetCode(EVENT_FREE_CHAIN)
	  e3:SetTarget(c900000099.ratg2)
	  e3:SetOperation(c900000099.raop2)
	  tc:RegisterEffect(e3,true)
	  tc:RegisterFlagEffect(900000099,0,0,0)
	end	
end
function c900000099.adtg2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c900000099.adfilter,tp,0xff,0xff,1,nil)end
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(c900000099.adfilter,tp,0xff,0xff,nil)
	for tc in aux.Next(g) do
	  local e3=Effect.CreateEffect(c)
	  e3:SetDescription(aux.Stringid(13719,15))
	  e3:SetType(EFFECT_TYPE_ACTIVATE)
	  e3:SetProperty(EFFECT_FLAG_CARD_TARGET)
	  e3:SetCode(EVENT_FREE_CHAIN)
	  e3:SetTarget(c900000099.ratg2)
	  e3:SetOperation(c900000099.raop2)
	  tc:RegisterEffect(e3,true)
	  tc:RegisterFlagEffect(900000099,0,0,0)
	end	
end

function c900000099.rafilter3(c,e,tp)
	return c:GetOriginalCode()==900000099 and not c:IsImmuneToEffect(e)
			 and Duel.IsExistingTarget(c900000099.rafilter4,tp,LOCATION_MZONE,0,1,c,e)
end
function c900000099.rafilter4(c,e)
	return c:IsType(TYPE_MONSTER) and not c:IsImmuneToEffect(e)
end
function c900000099.ratg2(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(tp) and c900000099.rafilter3(chkc,e,tp) end
	if chk==0 then return Duel.IsExistingTarget(c900000099.rafilter3,tp,LOCATION_MZONE,0,1,nil,e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	local g=Duel.SelectTarget(tp,c900000099.rafilter3,tp,LOCATION_MZONE,0,1,1,nil,e,tp) 
end
function c900000099.raop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if not (tc:IsRelateToEffect(e) and tc:IsFaceup()) then return end
	  Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
	  local g2=Duel.SelectTarget(tp,c900000099.rafilter4,tp,LOCATION_MZONE,0,1,20,tc,e)
	  local gc=g2:GetFirst()
	  local tatk=0
	  while gc do
	  local atk=tc:GetAttack()
	  if atk<0 then atk=0 end
	  tatk=tatk+atk
	  local code=gc:GetOriginalCode()
	  tc:CopyEffect(code,0,0)
	  gc=g2:GetNext() end
	  Duel.SendtoGrave(g2,REASON_EFFECT)
	if tc then
	local e0=Effect.CreateEffect(tc)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_UPDATE_ATTACK)
	  e0:SetValue(tatk)
	tc:RegisterEffect(e0)
	end
end

----------------------------------------------
function c900000099.tgvalue(e,re,rp)
	return rp~=e:GetHandlerPlayer()
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000099.valcon(e,re,r,rp)
	return bit.band(r,REASON_BATTLE)~=0
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000099.upcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	  e:SetLabelObject(bc)
	return c:IsRelateToBattle() and bc:IsType(TYPE_MONSTER) and bc:IsLocation(LOCATION_GRAVE)
end

function c900000099.uptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return not e:GetHandler():IsHasEffect(900000099) end
	local tc=e:GetLabelObject()
	Duel.SetTargetCard(tc)
	Duel.SetOperationInfo(0,CATEGORY_LEAVE_GRAVE,tc,1,0,0)
end

function c900000099.upop(e,tp,eg,ep,ev,re,r,rp)
	local bc=Duel.GetAttackTarget()
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	e1:SetValue(bc:GetAttack()/2)
	e:GetHandler():RegisterEffect(e1)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000099.copycon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	  e:SetLabelObject(bc)
	return c:IsRelateToBattle() and bc:IsType(TYPE_MONSTER) and bc:IsLocation(LOCATION_GRAVE)
end

function c900000099.copyop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=c:GetBattleTarget()
	if tc and c:IsFaceup() and c:IsRelateToEffect(e) then
		local code=tc:GetOriginalCode()
		c:CopyEffect(code, RESET_EVENT)
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000099.atkcon(e)
	local tc=Duel.GetFieldCard(0,LOCATION_SZONE,5)
	if tc and tc:IsFaceup() then return true end
	tc=Duel.GetFieldCard(1,LOCATION_SZONE,5)
	return tc and tc:IsFaceup()
end
