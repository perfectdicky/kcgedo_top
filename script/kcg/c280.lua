-- The Fang of Critias
function c280.initial_effect(c)
    -- Doom Virus Dragon
    local e1 = Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
    e1:SetDescription(aux.Stringid(13707, 7))
    e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetCode(EVENT_FREE_CHAIN)
    ---e1:SetCountLimit(1, 288)
    e1:SetTarget(c280.target)
    e1:SetOperation(c280.activate)
    c:RegisterEffect(e1)

    -- Activate
    local e4 = Effect.CreateEffect(c)
    e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
    e4:SetDescription(aux.Stringid(13707, 9))
    e4:SetType(EFFECT_TYPE_ACTIVATE)
    e4:SetCode(EVENT_FREE_CHAIN)
    --e4:SetCountLimit(1, 288)
    e4:SetTarget(c280.target4)
    e4:SetOperation(c280.activate4)
    c:RegisterEffect(e4)
    local e100 = Effect.CreateEffect(c)
    e100:SetType(EFFECT_TYPE_SINGLE)
    e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e100:SetRange(LOCATION_SZONE)
    e100:SetCode(EFFECT_CANNOT_DISABLE)
    e100:SetValue(1)
    c:RegisterEffect(e100)
end
c280.list = {
    [57728570] = 279,
    [44095762] = 283,
    [170000149] = 285
}
c280.a = 0

function c280.filter1(c, e, tp)
    local code = c:GetOriginalCode()
    local tcode = c280.list[code]
    return Duel.GetLocationCountFromEx(tp, tp, c, TYPE_FUSION) > 0 and (c:IsControler(tp) or c:IsFaceup()) and tcode and
               not c:IsImmuneToEffect(e)
    -- and Duel.IsExistingMatchingCard(c280.filter2,tp,LOCATION_EXTRA,0,1,nil,tcode,e,tp)
end
function c280.filter2(c, tcode, e, tp)
    return c:IsCode(tcode) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, true, false)
end
function c280.target(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsExistingMatchingCard(c280.filter1, tp, LOCATION_SZONE, 0, 1, nil, e, tp)
    end
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_EXTRA)
end
function c280.activate(e, tp, eg, ep, ev, re, r, rp)
    if not Duel.IsExistingMatchingCard(c280.filter1, tp, LOCATION_SZONE, 0, 1, nil, e, tp) then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FMATERIAL)
    local rg = Duel.SelectMatchingCard(tp, c280.filter1, tp, LOCATION_SZONE, 0, 1, 1, nil, e, tp)
    if rg:GetFirst():IsFacedown() then
        Duel.ConfirmCards(tp, rg:GetFirst())
    end
    if Duel.GetLocationCountFromEx(tp, tp, rg:GetFirst(), TYPE_FUSION) < 1 then
        return
    end
    local code = rg:GetFirst():GetCode()
    local tcode = c280.list[code]
    -- Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
    -- local tc=Duel.SelectMatchingCard(tp,c280.filter2,tp,LOCATION_EXTRA,0,1,1,nil,tcode,e,tp):GetFirst()
    local tc = Duel.CreateToken(tp, tcode, nil, nil, nil, nil, nil, nil)
    tc:SetMaterial(rg)
    Duel.SendtoGrave(rg, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
    Duel.BreakEffect()
    if tc and Duel.SendtoDeck(tc, tp, 0, REASON_RULE) > 0 and
        Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, false, POS_FACEUP) ~= 0 then
        tc:CompleteProcedure()
    end
end

function c280.filter7(c, e, tp)
    local code = c:GetOriginalCode()
    local tcode = c280.list[code]
    return Duel.GetLocationCountFromEx(tp, tp, c, TYPE_FUSION) > 0 and c:IsType(TYPE_TRAP) and not tcode
                and not c:IsImmuneToEffect(e)
    -- and Duel.IsExistingMatchingCard(c280.filter8,tp,LOCATION_EXTRA,0,1,nil,e,tp)
end
function c280.filter8(c, e, tp)
    return c:IsCode(43) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, true, false)
end
function c280.target4(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsExistingMatchingCard(c280.filter7, tp, LOCATION_SZONE, 0, 1, nil, e, tp)
    end
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_EXTRA)
end
function c280.activate4(e, tp, eg, ep, ev, re, r, rp)
    if not Duel.IsExistingMatchingCard(c280.filter7, tp, LOCATION_SZONE, 0, 1, nil, e, tp) then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FMATERIAL)
    local rg = Duel.SelectTarget(tp, c280.filter7, tp, LOCATION_SZONE, 0, 1, 1, nil, e, tp)
    if rg:GetFirst():IsFacedown() then
        Duel.ConfirmCards(tp, rg:GetFirst())
    end
    local tc = Duel.CreateToken(tp, 43, nil, nil, nil, nil, nil, nil)
    tc:SetMaterial(rg)
    Duel.SendtoGrave(rg, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
    Duel.BreakEffect()
    if tc and Duel.SendtoDeck(tc, tp, 0, REASON_RULE) > 0 then
        Duel.SpecialSummonStep(tc, SUMMON_TYPE_FUSION, tp, tp, true, false, POS_FACEUP)
        tc:SetCardData(CARDDATA_SETCODE, {rg:GetFirst():GetOriginalSetCard(), tc:GetOriginalSetCard()})
        Duel.SpecialSummonComplete()
        tc:CompleteProcedure()

        local e2 = Effect.CreateEffect(tc)
        e2:SetType(EFFECT_TYPE_IGNITION)
        e2:SetRange(LOCATION_MZONE)
        e2:SetCondition(c280.con)
        e2:SetOperation(c280.ignition)
        e2:SetCountLimit(1)
        e2:SetLabelObject(rg:GetFirst())
        -- e2:SetReset(RESET_EVENT+EVENT_TO_DECK)
        tc:RegisterEffect(e2, true)

        -- tc:CopyEffect(g:GetOriginalCode(),0,0) 
    end
end
function c280.con(e, tp, eg, ep, ev, re, r, rp)
    -- local tc=c280.a
    local tc = e:GetLabelObject()
    local tec = tc.trap
    local actchk = 0
    if not tec then
        return false
    end
    for _, te in ipairs(tec) do
        local chk, aeg, aep, aev, are, ar, arp = Duel.CheckEvent(te:GetCode(), true)
        if (chk or te:GetCode() == EVENT_FREE_CHAIN) and
            (te:GetCondition() == nil or te:GetCondition()(te, tp, aeg, aep, aev, are, ar, arp)) and
            (te:GetTarget() == nil or te:GetTarget()(te, tp, aeg, aep, aev, are, ar, arp, 0)) then
            actchk = 1
            break
        end
    end
    return tc.trap and actchk == 1
    -- tc:CheckActivateEffect(false,true,false)~=nil
end
function c280.ignition(e, tp, eg, ep, ev, re, r, rp)
    -- local tc=c170000151.a
    local tc = e:GetLabelObject()
    local tec = tc.trap
    if not tec then
        return
    end
    local te = nil
    local acd = {}
    local ac = {}
    for _, temp in ipairs(tec) do
        local tg = temp:GetTarget()
        if tg == nil or tg(temp, tp, Group.CreateGroup(), PLAYER_NONE, 0, e, REASON_EFFECT, PLAYER_NONE, 0) then
            table.insert(ac, temp)
            table.insert(acd, temp:GetDescription())
        end
    end
    if #ac <= 0 then
        return
    end
    Duel.BreakEffect()
    Duel.Hint(HINT_CARD, 0, tc:GetCode())
    if #ac == 1 then
        te = ac[1]
    elseif #ac > 1 then
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EFFECT)
        op = Duel.SelectOption(tp, table.unpack(acd))
        op = op + 1
        te = ac[op]
    end
    if not te then
        return
    end
    local tcost = te:GetCost()
    local tg = te:GetTarget()
    local op = te:GetOperation()
    if tg then
        tg(te, tp, Group.CreateGroup(), PLAYER_NONE, 0, e, REASON_EFFECT, PLAYER_NONE, 1)
    end
    Duel.BreakEffect()
    tc:CreateEffectRelation(te)
    Duel.BreakEffect()
    local g = Duel.GetChainInfo(0, CHAININFO_TARGET_CARDS)
    if g then
        local etc = g:GetFirst()
        while etc do
            etc:CreateEffectRelation(te)
            etc = g:GetNext()
        end
    end
    if op then
        op(te, tp, Group.CreateGroup(), PLAYER_NONE, 0, e, REASON_EFFECT, PLAYER_NONE, 1)
    end
    tc:ReleaseEffectRelation(te)
    if etc then
        etc = g:GetFirst()
        while etc do
            etc:ReleaseEffectRelation(te)
            etc = g:GetNext()
        end
    end
end
