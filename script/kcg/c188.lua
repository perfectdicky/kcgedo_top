--虚無
function c188.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_CANNOT_INACTIVATE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c188.condition)
	e1:SetOperation(c188.operation)
	c:RegisterEffect(e1)
end

function c188.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c188.filter2,tp,LOCATION_ONFIELD,0,1,nil)
		-- and (e:GetHandler():GetFlagEffect(511310104)~=0 or e:GetHandler():GetFlagEffect(511310105)~=0)
end
function c188.filter2(c)
	return c:IsFaceup() and c:IsCode(511310100) and not c:IsDisabled()
end
function c188.filter(c,code)
	return c:IsFaceup() and c:IsCode(code) and not c:IsDisabled()
end
function c188.filter3(c,seq1,seq2)
	return c:IsFacedown() and c:GetSequence()<seq1 and c:GetSequence()>seq2
end

function c188.getflag(g,tp)
    local flag = 0
    for c in aux.Next(g) do
        flag = flag|((1<<c:GetSequence())<<(8+(16*c:GetControler())))
    end
    if tp~=0 then
        flag=((flag<<16)&0xffff)|((flag>>16)&0xffff)
    end
    return ~flag
end
function c188.SelectCardByZone(g,tp,hint)
	if hint then Duel.Hint(HINT_SELECTMSG,tp,hint) end
	local sel=Duel.SelectFieldZone(tp,1,LOCATION_SZONE,0,c188.getflag(g,tp))>>8
	local seq=math.log(sel,2)
	local c=Duel.GetFieldCard(tp,LOCATION_SZONE,seq)
	return c
end
function c188.operation(e,tp,eg,ep,ev,re,r,rp)
	--if e:GetHandler():GetFlagEffect(511310105)~=0 then return end
	--if not Duel.IsExistingMatchingCard(c188.filter2,tp,LOCATION_ONFIELD,0,1,nil) then return end
	local gd=Duel.GetMatchingGroup(Card.IsFacedown,tp,LOCATION_SZONE,0,nil)
	if gd:GetCount()>0 then
		-- local sg=gd:Select(tp,1,1,nil)
		-- local tc=sg:GetFirst()
		local tc=c188.SelectCardByZone(gd,tp,HINTMSG_RESOLVEEFFECT)
		if not tc or Duel.ChangePosition(tc,POS_FACEUP)<1 then return end
		--tc:RegisterFlagEffect(511310104,RESET_EVENT+0x1fe0000+RESET_CHAIN,0,1)
		if tc:IsCode(511310105) then
			local fseq=tc:GetSequence()
			local seq=e:GetHandler():GetSequence()
			if fseq>seq then local s=seq seq=fseq fseq=s end
			local sqc=Duel.GetMatchingGroup(c188.filter3,tp,LOCATION_SZONE,0,nil,seq,fseq)
			if Duel.ChangePosition(sqc,POS_FACEUP)<1 then return end
			sqc=Duel.GetOperatedGroup()
			local sqtc=sqc:GetMinGroup(Card.GetSequence):GetFirst()
			for sqtc2 in aux.Next(sqc) do
				sqtc2:RegisterFlagEffect(sqtc:GetOriginalCode(),RESET_EVENT+0x1fe0000+RESET_CHAIN+RESET_PHASE+PHASE_END,0,1)
			end
			while sqc:GetCount()>0 do
				local sqtc2=sqc:GetMinGroup(Card.GetSequence):GetFirst()
				sqc:RemoveCard(sqtc2)
				c188.zero(sqtc2,e)
				if not sqtc2:IsType(TYPE_CONTINUOUS) and not sqtc2:IsHasEffect(EFFECT_REMAIN_FIELD) then Duel.SendtoGrave(sqtc2,REASON_RULE) end 
			end
					
		else
			c188.zero(tc,e)
			if not tc:IsType(TYPE_CONTINUOUS) and not tc:IsHasEffect(EFFECT_REMAIN_FIELD) then Duel.SendtoGrave(tc,REASON_RULE) end
		end
	end
end

function c188.zero(tc,e)
	local te=tc:GetActivateEffect()
	if te==nil or tc:CheckActivateEffect(false,false,false)==nil then return end
	local tep=tc:GetControler()
	local condition=te:GetCondition()
	local cost=te:GetCost()
	local target=te:GetTarget()
	local operation=te:GetOperation()
	e:SetProperty(te:GetProperty())
	Duel.ClearTargetCard()
	if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
	if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
	tc:CreateEffectRelation(te)
	local gg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	if gg then  
		local etc=gg:GetFirst()	
		while etc do
			etc:CreateEffectRelation(te)
			etc=gg:GetNext()
		end
	end						
	if operation then operation(te,tep,eg,ep,ev,re,r,rp) end
	tc:ReleaseEffectRelation(te)					
	if gg then  
		local etc=gg:GetFirst()												 
		while etc do
			etc:ReleaseEffectRelation(te)
			etc=gg:GetNext()
		end
	end 
end