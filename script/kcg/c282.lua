-- The Claw of Hermos
function c282.initial_effect(c)
    -- Activate
    local e1 = Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_SPECIAL_SUMMON + CATEGORY_EQUIP)
    e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetDescription(aux.Stringid(13707, 7))
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
    --e1:SetCountLimit(1, 282)
    e1:SetTarget(c282.target)
    e1:SetOperation(c282.activate)
    c:RegisterEffect(e1)
    local e2 = Effect.CreateEffect(c)
    e2:SetCategory(CATEGORY_SPECIAL_SUMMON + CATEGORY_EQUIP)
    e2:SetType(EFFECT_TYPE_ACTIVATE)
    e2:SetDescription(aux.Stringid(13707, 10))
    e2:SetCode(EVENT_FREE_CHAIN)
    --e2:SetCountLimit(1, 282)
    e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
    e2:SetTarget(c282.target2)
    e2:SetOperation(c282.activate2)
    c:RegisterEffect(e2)
    local e100 = Effect.CreateEffect(c)
    e100:SetType(EFFECT_TYPE_SINGLE)
    e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e100:SetRange(LOCATION_SZONE)
    e100:SetCode(EFFECT_CANNOT_DISABLE)
    e100:SetValue(1)
    c:RegisterEffect(e100)
end
c282.list = {
    [25652259] = 289,
    [74677422] = 288,
    [71625222] = 170000195,
    [110000110] = 290,
    [30860696] = 291
}
c282.a = 0
c282.atk = 0

function c282.filter1(c, e, tp)
	local code=c:GetOriginalCode()
	local tcode=c282.list[code]
    return (c:IsControler(tp) or c:IsFaceup()) and tcode and c:IsCanBeFusionMaterial() and not c:IsImmuneToEffect(e) and not c:IsSetCard(0xa1) 
    and Duel.IsExistingMatchingCard(Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, 1, c)
end
function c282.filter2(c, tcode)
    return c:IsCode(tcode)
end
function c282.target(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return (Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and e:GetHandler():IsLocation(LOCATION_SZONE) or Duel.GetLocationCount(tp, LOCATION_SZONE) > 1)
            and Duel.IsExistingMatchingCard(c282.filter1, tp, LOCATION_MZONE, 0, 1, nil, e, tp)
    end
    Duel.SetOperationInfo(0, CATEGORY_EQUIP, nil, 1, 0, 0)
end
function c282.activate(e, tp, eg, ep, ev, re, r, rp)
    if not Duel.IsExistingMatchingCard(c282.filter1, tp, LOCATION_MZONE, 0, 1, nil, e, tp) then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FMATERIAL)
    local rg = Duel.SelectMatchingCard(tp, c282.filter1, tp, LOCATION_MZONE, 0, 1, 1, nil, e, tp)
    if rg:GetFirst():IsFacedown() then
        Duel.ConfirmCards(tp, rg:GetFirst())
    end
    local code = rg:GetFirst():GetOriginalCode()
    local tcode = c282.list[code]
    local tc = Duel.CreateToken(tp, tcode, nil, nil, nil, nil, nil, nil)
    tc:SetMaterial(rg)
    Duel.SendtoGrave(rg, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
    Duel.BreakEffect()
    if tc and Duel.SendtoDeck(tc, tp, 0, REASON_RULE) > 0 then
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EQUIP)
        local et =
            Duel.SelectTarget(tp, Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, rg:GetFirst()):GetFirst()
        Duel.Equip(tp, tc, et)
        tc:CompleteProcedure()
        -- Equip limit
        local e2 = Effect.CreateEffect(tc)
        e2:SetType(EFFECT_TYPE_SINGLE)
        e2:SetCode(EFFECT_EQUIP_LIMIT)
        e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
        e2:SetValue(Auxiliary.EquipLimit(nil))
        tc:RegisterEffect(e2)
    end
end

function c282.filter11(c, e, tp)
	local code=c:GetOriginalCode()
	local tcode=c282.list[code]
    return (c:IsControler(tp) or c:IsFaceup()) and
               (code ~= 25652259 and code ~= 74677422 and code ~= 71625222 and code ~= 110000110 and code ~= 30860696) and
               not c:IsSetCard(0xa1) and c:IsCanBeFusionMaterial() and not c:IsImmuneToEffect(e) and
               Duel.IsExistingMatchingCard(Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, 1, c)
end
function c282.filter22(c)
    return c:IsCode(44)
end
function c282.target2(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return (Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and e:GetHandler():IsLocation(LOCATION_SZONE) or Duel.GetLocationCount(tp, LOCATION_SZONE) > 1)
            and Duel.IsExistingMatchingCard(c282.filter11, tp, LOCATION_MZONE, 0, 1, nil, e, tp)
    end
    Duel.SetOperationInfo(0, CATEGORY_EQUIP, nil, 1, 0, 0)
end
function c282.activate2(e, tp, eg, ep, ev, re, r, rp)
    if not Duel.IsExistingMatchingCard(c282.filter11, tp, LOCATION_MZONE, 0, 1, nil, e, tp) then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FMATERIAL)
    local rg = Duel.SelectMatchingCard(tp, c282.filter11, tp, LOCATION_MZONE, 0, 1, 1, nil, e, tp)
    if rg:GetFirst():IsFacedown() then
        Duel.ConfirmCards(tp, rg:GetFirst())
    end
    local gc = rg:GetFirst()
    local code=gc:GetOriginalCode()
    local tc = Duel.CreateToken(tp, 44, nil, nil, nil, nil, nil, nil)
    tc:SetMaterial(rg)
    Duel.SendtoGrave(rg, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
    Duel.BreakEffect()
    if tc and Duel.SendtoDeck(tc, tp, 0, REASON_RULE) > 0 then
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EQUIP)
        local et =
            Duel.SelectTarget(tp, Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, gc):GetFirst()
        if not Duel.Equip(tp, tc, et) then
            return
        end
        tc:CompleteProcedure()
        -- Equip limit
        local e2 = Effect.CreateEffect(tc)
        e2:SetType(EFFECT_TYPE_SINGLE)
        e2:SetCode(EFFECT_EQUIP_LIMIT)
        e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
        e2:SetValue(Auxiliary.EquipLimit(nil))
        tc:RegisterEffect(e2)
        tc:SetCardData(CARDDATA_SETCODE, {gc:GetOriginalSetCard(), tc:GetOriginalSetCard()})
        if gc and gc:IsType(TYPE_NORMAL) then
            local e2 = Effect.CreateEffect(tc)
            e2:SetType(EFFECT_TYPE_EQUIP)
            e2:SetCode(EFFECT_UPDATE_ATTACK)
            e2:SetValue(c282.value)
            e2:SetLabelObject(gc)
            tc:RegisterEffect(e2, true)
        end
        if gc and gc:IsType(TYPE_EFFECT) then
            local e3 = Effect.CreateEffect(tc)
            e3:SetCategory(CATEGORY_DESTROY)
            e3:SetType(EFFECT_TYPE_QUICK_O)
            e3:SetCode(EVENT_FREE_CHAIN)
            e3:SetRange(LOCATION_SZONE)
            e3:SetCountLimit(1)
            e3:SetCondition(c282.con)
            e3:SetTarget(c282.tar)
            e3:SetOperation(c282.act)
            tc:RegisterEffect(e3, true)
            tc:GetEquipTarget():CopyEffect(code, RESET_EVENT + EVENT_TO_DECK, 1)
        end
    end
end
function c282.value(e, c)
    return Duel.GetMatchingGroupCount(c282.atkfilter, c:GetControler(), LOCATION_MZONE, LOCATION_MZONE, nil, e) * 500 +
               1000
end
function c282.atkfilter(c, e)
    local gc = e:GetLabelObject()
    return c:IsRace(gc:GetRace()) and c:IsFaceup()
end

function c282.con(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler():GetEquipTarget()
    local at = c:GetBattleTarget()
    return at and at:IsFaceup() and not e:GetHandler():IsStatus(STATUS_CHAINING)
end
function c282.tar(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.GetMatchingGroup(aux.TRUE, tp, 0, LOCATION_MZONE, nil)
    end
    local g = Duel.GetMatchingGroup(aux.TRUE, tp, 0, LOCATION_MZONE, nil)
    Duel.SetOperationInfo(0, CATEGORY_DESTROY, g, g:GetCount(), 0, 0, nil)
end
function c282.act(e, tp, eg, ep, ev, re, r, rp)
    if not e:GetHandler():IsFaceup() then
        return
    end
    local g = Duel.GetMatchingGroup(aux.TRUE, tp, 0, LOCATION_MZONE, nil)
    Duel.Destroy(g, REASON_EFFECT)
end
