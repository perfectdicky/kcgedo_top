--Numeron Direct
function c597.initial_effect(c)
--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c597.condition)
	e1:SetTarget(c597.target)
	e1:SetOperation(c597.activate)
	c:RegisterEffect(e1)
end
function c597.filter1(c)
	return c:IsFaceup() and c:IsCode(41418852)
end
function c597.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c597.filter1,tp,LOCATION_SZONE,0,1,nil)
end
function c597.filter(c,e,tp)
	return c:GetAttack()==0 and c:IsSetCard(0x14b)
		and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function c597.target(e,tp,eg,ep,ev,re,r,rp,chk)
		if chk==0 then return 
		(Duel.GetLocationCount(tp,LOCATION_MZONE)>3 
		and not Duel.IsPlayerAffectedByEffect(tp,59822133)
		and Duel.IsExistingMatchingCard(c597.filter,tp,LOCATION_DECK,0,4,nil,e,tp))
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,4,0,LOCATION_DECK)
end
function c597.activate(e,tp,eg,ep,ev,re,r,rp)
            local c=e:GetHandler()
	local count=0
	if Duel.IsPlayerAffectedByEffect(tp,59822133) then return end	
	local ft1=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if ft1>3 then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local g=Duel.SelectMatchingCard(tp,c597.filter,tp,LOCATION_DECK,0,4,4,nil,e,tp)
		if g:GetCount()>0 then
			local tc=g:GetFirst()
			while tc do
				Duel.SpecialSummonStep(tc,0,tp,tp,false,false,POS_FACEUP)
 	local de=Effect.CreateEffect(c)
	de:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	de:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	de:SetRange(LOCATION_MZONE)
	de:SetCode(EVENT_PHASE+PHASE_END)
	de:SetCountLimit(1)
	de:SetOperation(c597.rmop)
	de:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	tc:RegisterEffect(de,true)
                        Duel.SpecialSummonComplete()
                        tc:CompleteProcedure()
				tc=g:GetNext()
				end
		end
	end
end
function c597.rmop(e,tp,eg,ep,ev,re,r,rp)
            local c=e:GetHandler()
	Duel.Remove(c,POS_FACEUP,REASON_EFFECT)
end
