--大秘儀之力V-教皇 (KA)

function c640.initial_effect(c)

	--coin

	local e1=Effect.CreateEffect(c)

	e1:SetDescription(aux.Stringid(8396952,0))

	e1:SetCategory(CATEGORY_COIN)

	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)

	e1:SetCode(EVENT_SUMMON_SUCCESS)

	e1:SetTarget(c640.cointg)

	e1:SetOperation(c640.coinop)

	c:RegisterEffect(e1)

	local e2=e1:Clone()

	e2:SetCode(EVENT_SPSUMMON_SUCCESS)

	c:RegisterEffect(e2)

	local e3=e1:Clone()

	e3:SetCode(EVENT_FLIP_SUMMON_SUCCESS)

	c:RegisterEffect(e3)

end


function c640.cointg(e,tp,eg,ep,ev,re,r,rp,chk)

	if chk==0 then return true end

	Duel.SetOperationInfo(0,CATEGORY_COIN,nil,0,tp,1)

end

function c640.coinop(e,tp,eg,ep,ev,re,r,rp)

	local c=e:GetHandler()

	if not c:IsRelateToEffect(e) or c:IsFacedown() then return end

	local res=0

	if c:IsHasEffect(73206827) then

		res=1-Duel.SelectOption(tp,60,61)

	else res=Duel.TossCoin(tp,1) end

	c640.arcanareg(c,res)

end

function c640.arcanareg(c,coin)

   --coin effect

	local e2=Effect.CreateEffect(c)

	e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)

	e2:SetCode(EVENT_PRE_BATTLE_DAMAGE)

	e2:SetOperation(c640.rdop)

	e2:SetReset(RESET_EVENT+0x1ff0000)

	c:RegisterEffect(e2)

	--

	local e3=Effect.CreateEffect(c)

	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)

	e3:SetCode(EVENT_CHAIN_SOLVED)

	e3:SetRange(LOCATION_MZONE)

	e3:SetOperation(c640.speop)

	e3:SetReset(RESET_EVENT+0x1ff0000)

	c:RegisterEffect(e3)

	c:RegisterFlagEffect(36690018,RESET_EVENT+0x1ff0000,EFFECT_FLAG_CLIENT_HINT,1,coin,63-coin)

end


function c640.rdop(e,tp,eg,ep,ev,re,r,rp)

	local c=e:GetHandler()

	if c:GetFlagEffectLabel(36690018)==0 then

	Duel.ChangeBattleDamage(tp,Duel.GetBattleDamage(tp)*2) end

end


function c640.speop(e,tp,eg,ep,ev,re,r,rp)

	local c=e:GetHandler()

	if not re:IsActiveType(TYPE_SPELL) or not re:IsHasType(EFFECT_TYPE_ACTIVATE) or rp~=c:GetControler() then return end

	local val=c:GetFlagEffectLabel(36690018)

	if val==1 then

		Duel.Recover(tp,500,REASON_EFFECT)

	end

end