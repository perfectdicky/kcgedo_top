local s, id = GetID()
if s then
    function s.initial_effect(c)
    end
end
if not id then
    id = 85
end
if not DestinyDraw then
    DestinyDraw = {}
    Announce = {}
    aux.GlobalCheck(DestinyDraw, function()
        DestinyDraw[0] = nil
        DestinyDraw[1] = nil
        Announce[0] = {}
        Announce[1] = {}
        -- xyztempg0=Group.CreateGroup()
		-- xyztempg0:KeepAlive()
		-- xyztempg1=Group.CreateGroup()
		-- xyztempg1:KeepAlive()
    end)
    local function finishsetup()
        local e1 = Effect.GlobalEffect()
        e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        e1:SetCode(EVENT_STARTUP)
        e1:SetOperation(DestinyDraw.op)
        Duel.RegisterEffect(e1, 0)

        local e2 = Effect.GlobalEffect()
        e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
        e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        e2:SetCode(EVENT_PREDRAW)
        e2:SetCondition(DestinyDraw.drcon)
        e2:SetOperation(DestinyDraw.drop)
        Duel.RegisterEffect(e2, 0)
        local e3 = e2:Clone()
        Duel.RegisterEffect(e3, 1)

        local e4 = Effect.GlobalEffect()
        e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
        e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        e4:SetCode(EVENT_PREEFFECT_DRAW)
        e4:SetCondition(DestinyDraw.drcon2)
        e4:SetOperation(DestinyDraw.drop2)
        Duel.RegisterEffect(e4, 0)   
        local e5 = e4:Clone()
        Duel.RegisterEffect(e5, 1)     
    end

    function DestinyDraw.op(e, tp, eg, ep, ev, re, r, rp)
        local e1 = Effect.GlobalEffect()
        e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_DELAY)
        e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        e1:SetCode(EVENT_SPSUMMON_SUCCESS)
        e1:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
        e1:SetCondition(DestinyDraw.xyzsumcon)
        e1:SetOperation(DestinyDraw.xyzsumop)
        Duel.RegisterEffect(e1, tp)
        local e2=e1:Clone()
        e2:SetCondition(DestinyDraw.removecon)
        e2:SetOperation(DestinyDraw.removeop)
        Duel.RegisterEffect(e2, tp)       

        local ge2 = Effect.GlobalEffect()
        ge2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        ge2:SetCode(EVENT_PHASE + PHASE_DRAW)
        ge2:SetCountLimit(1)
        ge2:SetOperation(aux.check2op)
        Duel.RegisterEffect(ge2, 0)

        -- local g=Duel.GetMatchingGroup(function(sc) return sc:GetCode()~=sc:GetOriginalCode() and sc.xyz_number~=nil end,0,LOCATION_EXTRA,LOCATION_EXTRA,nil)
        -- for tc in aux.Next(g) do 
        -- 	--Duel.CreateToken(tp,tc:GetCode())
        -- 	Duel.LoadCardScript(tc:GetCode())
        -- end		

        local kp = {0, 1}
        -- if Duel.GetMatchingGroupCount(Card.IsCode, 0, LOCATION_DECK + LOCATION_HAND, 0, nil, 85) > 0 and
        --     Duel.GetMatchingGroupCount(Card.IsCode, 1, LOCATION_DECK + LOCATION_HAND, 0, nil, 85) == 0 then
        --     kp = {0}
        --     aux.AIchk0 = 1
        -- end
        -- if Duel.GetMatchingGroupCount(Card.IsCode, 1, LOCATION_DECK + LOCATION_HAND, 0, nil, 85) > 0 and
        --     Duel.GetMatchingGroupCount(Card.IsCode, 0, LOCATION_DECK + LOCATION_HAND, 0, nil, 85) == 0 then
        --     kp = {1}
        --     aux.AIchk1 = 1
        -- end
        for _, ttp in ipairs(kp) do
            local b1 = (Duel.GetMatchingGroupCount(DestinyDraw.DDfiler2, ttp, LOCATION_EXTRA, 0, nil) > 0)
            local b2 = (Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_EXTRA, 0, nil, 0x7f) > 0)
            local b3 = (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 24094653) > 0 
                and Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0x3008) > 0)
            local b4 = (Duel.GetMatchingGroupCount(DestinyDraw.RDSfiler, ttp, LOCATION_EXTRA, 0, nil) > 0 or
                           (Duel.GetMatchingGroupCount(DestinyDraw.RRDSfiler, ttp, LOCATION_EXTRA, 0, nil) > 0 and
                               Duel.GetMatchingGroupCount(DestinyDraw.RRDSfiler2, ttp, LOCATION_DECK + LOCATION_HAND, 0,
                                   nil, 1) > 0))
            local b5 =
                (Duel.GetMatchingGroupCount(DestinyDraw.DTMfiler, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, tp) > 0)
            local b6 = ((Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 10000020) >
                           0 and
                           Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 10000000) >
                           0 and
                           Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 10000010) >
                           0) or
                           (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 281) > 0 and
                               Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 280) >
                               0 and
                               Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 282) >
                               0))
            local b7 = (Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_FIELD) > 0)
            local b8 = (Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_EXTRA, 0, nil, TYPE_MONSTER) == 0 and
                           Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0x909) >
                           9)
            local b9 = (Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0x5) > 9)
            local b10 = ((Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0x9f) >
                            1) or
                            (Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0xf8) >
                                1))
            local b11 = (Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_EXTRA, 0, nil, TYPE_LINK) > 9)
            local b12 =
                ((Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 69890967) > 0) and
                    (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 32491822) > 0) and
                    (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND, 0, nil, 6007213) > 0))
            local sel = 0
            local off = 1
            local ops = {}
            local opval = {}
            ops[off] = aux.Stringid(826, 13)
            opval[off - 1] = 1
            off = off + 1
            if b1 or b2 or b3 or b4 or b5 or b6 or b7 or b8 or b9 or b10 or b11 or b12 then
                ops[off] = aux.Stringid(826, 12)
                opval[off - 1] = 2
                off = off + 1
            end
            local op = Duel.SelectOption(ttp, table.unpack(ops))
            if opval[op] == 1 then
                local g = Duel.GetFieldGroup(ttp, LOCATION_DECK + LOCATION_HAND, 0)
                if DestinyDraw[ttp]==nil then
                    Duel.Hint(HINT_SELECTMSG, ttp, aux.Stringid(826, 11))
                    DestinyDraw[ttp] = aux.SelectUnselectGroup(g, e, ttp, 1, 5, aux.dncheck, 1, ttp)
                    DestinyDraw[ttp]:KeepAlive()
                    Duel.ShuffleDeck(ttp)
                    Duel.RegisterFlagEffect(ttp, 99999980, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,512)
                end
            end
            local tt
            if opval[op] == 2 then
                -- local tck0= Duel.CreateToken(0, 827)
                -- local tck1= Duel.CreateToken(1, 827)
                --xyztempg0:AddCard(tck0) xyztempg1:AddCard(tck1)
                tt = Duel.CreateToken(ttp, 157)
                tt:SetCardData(CARDDATA_TYPE, TYPE_SPELL)
                Duel.Remove(tt, POS_FACEUP, REASON_RULE)
                DestinyDraw.extra(Group.FromCards(tt), 0)
                if Duel.GetMatchingGroupCount(Card.IsCode,ttp,LOCATION_DECK+LOCATION_HAND,0,nil,85)>0 then 
					Duel.RegisterFlagEffect(ttp, 158, 0, 0, 1)
                end 
                Duel.RegisterFlagEffect(ttp, 157, 0, 0, 1)
                local sel = 0
                local off = 1
                local ops = {}
                local opval = {}
                if b1 then
                    ops[off] = aux.Stringid(123106, 8)
                    opval[off - 1] = 1
                    off = off + 1
                end
                if b2 then
                    ops[off] = aux.Stringid(111011901, 2)
                    opval[off - 1] = 2
                    off = off + 1
                end
                if b3 then
                    ops[off] = aux.Stringid(123106, 11)
                    opval[off - 1] = 3
                    off = off + 1
                end
                if b4 then
                    ops[off] = aux.Stringid(13709, 15)
                    opval[off - 1] = 4
                    off = off + 1
                end
                if b5 then
                    ops[off] = aux.Stringid(826, 7)
                    opval[off - 1] = 5
                    off = off + 1
                end
                if b6 then
                    ops[off] = aux.Stringid(826, 6)
                    opval[off - 1] = 6
                    off = off + 1
                end
                if b7 then
                    ops[off] = aux.Stringid(48934760, 0)
                    opval[off - 1] = 7
                    off = off + 1
                end
                if b8 then
                    ops[off] = aux.Stringid(13713, 8)
                    opval[off - 1] = 8
                    off = off + 1
                end
                if b9 then
                    ops[off] = aux.Stringid(23846921, 0)
                    opval[off - 1] = 9
                    off = off + 1
                end
                if b10 then
                    ops[off] = aux.Stringid(827, 1)
                    opval[off - 1] = 10
                    off = off + 1
                end
                if b11 then
                    ops[off] = aux.Stringid(826, 5)
                    opval[off - 1] = 11
                    off = off + 1
                end
                if b12 then
                    ops[off] = aux.Stringid(826, 10)
                    opval[off - 1] = 12
                    off = off + 1
                end
                local op = Duel.SelectOption(ttp, table.unpack(ops))
                if opval[op] == 1 then
                    Duel.RegisterFlagEffect(ttp, 90999980, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,513)
                    --tt:SetEntityCode(513,nil)
                    Duel.CreateToken(ttp, 98)
                end
                if opval[op] == 2 then
                    Duel.RegisterFlagEffect(ttp, 91999980, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,514)
                    --tt:SetEntityCode(514,nil)
                    local token1=Duel.CreateToken(ttp, 265)
                    local token2=Duel.CreateToken(ttp, 2896663)
                    local token3=Duel.CreateToken(ttp, 75402014)
                    
                    local token4 = Duel.CreateToken(ttp, 364)
                    local token5=Duel.CreateToken(ttp, 209)
                    local token6=Duel.CreateToken(ttp, 65)
                    local token7=Duel.CreateToken(ttp, 61)
                    local token8=Duel.CreateToken(ttp, 723)
                    local token9=Duel.CreateToken(ttp, 37)
                    local tokeng=Group.FromCards(token1,token2,token3,token4,token5,token6,token7,token8,token9)
                    DestinyDraw.extra(tokeng, RESET_EVENT+RESETS_STANDARD)
                end
                if opval[op] == 3 then
                    Duel.RegisterFlagEffect(ttp, 92999980, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,515)
                    --tt:SetEntityCode(515,nil)
                end
                if opval[op] == 4 then
                    Duel.RegisterFlagEffect(ttp, 93999980, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,517)
                    --tt:SetEntityCode(517,nil)
                    local token1=Duel.CreateToken(ttp, 100)
                    local token2=Duel.CreateToken(ttp, 7841112)
                    local token3=Duel.CreateToken(ttp, 63180841)
                    local token4=Duel.CreateToken(ttp, 68431965)
                    local token5=Duel.CreateToken(ttp, 511000774)
                    local token6=Duel.CreateToken(ttp, 511001658)
                    local token7=Duel.CreateToken(ttp, 101105104)
                    
                    local token8=Duel.CreateToken(ttp, 513000078)
                    local token9=Duel.CreateToken(ttp, 513000013)
                    local token10=Duel.CreateToken(ttp, 511001603)
                    local token11=Duel.CreateToken(ttp, 99585850)
                    
                    local token12=Duel.CreateToken(ttp, 40139997)
                    
                    local token13=Duel.CreateToken(ttp, 101)

                    local tokeng=Group.FromCards(token1,token2,token3,token4,token5,token6,token7,token8,token9,token10,token11,token12,token13)
                    DestinyDraw.extra(tokeng, RESET_EVENT+RESETS_STANDARD)
                end
                if opval[op] == 5 then
                    Duel.RegisterFlagEffect(ttp, 90899980, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,518)
                    --tt:SetEntityCode(518,nil)
                end
                if opval[op] == 6 then
                    Duel.RegisterFlagEffect(ttp, 90799980, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,519)
                    --tt:SetEntityCode(519,nil)
                    local token1=Duel.CreateToken(ttp, 805)
                    local tokeng=Group.FromCards(token1)
                    DestinyDraw.extra(tokeng, RESET_EVENT+RESETS_STANDARD)
                end
                if opval[op] == 7 then
                    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TARGET)
                    local field =
                        Duel.SelectMatchingCard(ttp, Card.IsType, ttp, LOCATION_DECK, 0, 1, 1, nil, TYPE_FIELD):GetFirst()
                    Duel.ShuffleDeck(ttp)
                    local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                    if not tac2 then return end
                    aux.SwapEntity(field, tac2)
                    --Duel.MoveSequence(field, 0)
                end
                if opval[op] == 8 then
                    Duel.RegisterFlagEffect(ttp, 388, 0, 0, 1)
                    local tokeng = Group.CreateGroup()
                    Duel.Hint(HINT_SKILL,ttp,520)
                    --tt:SetEntityCode(520,nil)
                    for i = 1, 100 do
                        if aux.list[i] then
                            local notoken = Duel.CreateToken(ttp, aux.list[i])
                            -- if not notoken:IsCode(13714) then 
                            -- 	notoken:ReplaceEffect(241,RESET_EVENT+RESETS_REDIRECT)
                            -- 	local e92=Effect.CreateEffect(notoken)
                            -- 	e92:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP)
                            -- 	e92:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
                            -- 	e92:SetCode(EVENT_SPSUMMON_SUCCESS)
                            -- 	e92:SetOperation(function(...) notoken:ReplaceEffect(notoken:GetOriginalCode(),0) end)
                            -- 	notoken:RegisterEffect(e92,true)
                            -- end
                            -- notoken:RegisterFlagEffect(85,0,0,0)
                            tokeng:AddCard(notoken)
                            -- local e90=Effect.CreateEffect(notoken)
                            -- e90:SetDescription(1165)
                            -- e90:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_UNCOPYABLE)
                            -- e90:SetType(EFFECT_TYPE_IGNITION)
                            -- e90:SetRange(LOCATION_EXTRA)
                            -- e90:SetCondition(DestinyDraw.xyzscon)
                            -- e90:SetTarget(DestinyDraw.distg) 
                            -- e90:SetOperation(DestinyDraw.xyzsactivate)
                            -- notoken:RegisterEffect(e90,true)
                            -- if notoken.alterxyz_filter~=nil then
                            -- 	local e91=Effect.CreateEffect(notoken)
                            -- 	e91:SetDescription(notoken.desc)
                            -- 	e91:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_UNCOPYABLE)
                            -- 	e91:SetType(EFFECT_TYPE_IGNITION)
                            -- 	e91:SetRange(LOCATION_EXTRA)
                            -- 	e91:SetCondition(DestinyDraw.xxyzcon1)
                            -- 	e91:SetTarget(DestinyDraw.distg)
                            -- 	e91:SetOperation(DestinyDraw.xyzsactivate2)
                            -- 	notoken:RegisterEffect(e91,true) 
                            -- end
                        end
                    end
                    Duel.SendtoDeck(tokeng, ttp, 0, REASON_RULE)
                end

                if opval[op] == 9 then
                    local e3 = Effect.GlobalEffect()
                    e3:SetType(EFFECT_TYPE_FIELD)
                    e3:SetCode(73206827)
                    e3:SetTargetRange(LOCATION_ONFIELD, LOCATION_ONFIELD)
                    e3:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0x5))
                    e3:SetCondition(aux.effectcon)
                    Duel.RegisterEffect(e3, ttp)
                end

                if opval[op] == 10 then
                    Duel.RegisterFlagEffect(ttp, 703, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,521)
                    --tt:SetEntityCode(521,nil)
                    local token1=Duel.CreateToken(ttp, 511009517)
                    local token2 = Duel.CreateToken(ttp, 511009508)
                    
                    local token3=Duel.CreateToken(ttp, 511003047)
                    local token4 = Duel.CreateToken(ttp, 511009588)
                    
                    local token5=Duel.CreateToken(ttp, 511009340)
                    local token6 = Duel.CreateToken(ttp, 511009387)
                    local token7 = Duel.CreateToken(ttp, 511001791)
                    local token8 = Duel.CreateToken(ttp, 98452268)
                    local token9 = Duel.CreateToken(ttp, 95685352)
                    
                    local token10 = Duel.CreateToken(ttp, 511600082)
                    local token11 = Duel.CreateToken(ttp, 511002709)
                    
                    local tokeng=Group.FromCards(token1,token2,token3,token4,token5,token6,token7,token8,token9,token10,token11)
                    DestinyDraw.extra(tokeng, RESET_EVENT+RESETS_STANDARD)
                    if Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_PENDULUM) > 1 and
                        Duel.SelectYesNo(ttp, aux.Stringid(827, 1)) then
                        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TARGET)
                        local destinytc = Duel.SelectMatchingCard(ttp, Card.IsType, ttp, LOCATION_DECK, 0, 1, 2, nil,
                                              TYPE_PENDULUM)
                        Duel.ShuffleDeck(ttp)         
                        local tac2=Duel.GetDecktopGroup(ttp, #destinytc)
                        if #tac2<1 then return end
                        aux.SwapEntity(destinytc, tac2)        
                        -- Duel.MoveSequence(destinytc:GetFirst(), 0)
                        -- if destinytc:GetCount() > 1 then
                        --     Duel.MoveSequence(destinytc:GetNext(), 0)
                        -- end
                        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(827, 1))
                    end
                end

                if opval[op] == 11 then
                    Duel.RegisterFlagEffect(ttp, 784, 0, 0, 1, Duel.GetLP(ttp))
                    Duel.Hint(HINT_SKILL,ttp,14)
                    --tt:SetEntityCode(14,nil)
                end
                if opval[op] == 12 then
                    Duel.RegisterFlagEffect(ttp, 10000004, 0, 0, 1)
                    Duel.Hint(HINT_SKILL,ttp,25)
                    --tt:SetEntityCode(25,nil)
                end
            end
        end
        if Duel.GetMatchingGroupCount(Card.IsCode, 0, LOCATION_DECK + LOCATION_HAND, LOCATION_DECK + LOCATION_HAND, nil, 85) > 0 then
            Duel.Exile(Duel.GetMatchingGroup(Card.IsCode, 0, LOCATION_DECK + LOCATION_HAND, LOCATION_DECK + LOCATION_HAND, nil, 85), REASON_RULE)
        end
        e:Reset()
    end

    function aux.check2op(e, tp, eg, ep, ev, re, r, rp)
        local g = Duel.GetMatchingGroup(aux.TRUE, tp, 0xff, 0xff, nil)
        if g:GetCount() > 0 then
            local tc = g:GetFirst()
            while tc do
                tc:RegisterFlagEffect(511010208, RESET_PHASE + PHASE_END, 0, 1, tc:GetLocation())
                tc:RegisterFlagEffect(511010209, RESET_PHASE + PHASE_END, 0, 1, tc:GetControler())
                tc:RegisterFlagEffect(511010210, RESET_PHASE + PHASE_END, 0, 1, tc:GetPosition())
                tc:RegisterFlagEffect(511010211, RESET_PHASE + PHASE_END, 0, 1, tc:GetSequence())
                tc = g:GetNext()
            end
        end
    end

    function DestinyDraw.extra(tokeng, reset)
        Duel.Remove(tokeng, POS_FACEDOWN, REASON_RULE)
        local sid = 0
        tokeng:ForEach(function(c)
        local ttp=c:GetOwner()
        c:RegisterFlagEffect(157, reset, 0, 1)
        local ae = {c:GetCardEffect(EFFECT_SPSUMMON_PROC)}
        for _, te in ipairs(ae) do
            if te:GetRange() == LOCATION_EXTRA and te:GetType() == EFFECT_TYPE_FIELD then
                local e1 = Effect.CreateEffect(c)
                e1:SetType(EFFECT_TYPE_FIELD)
                e1:SetDescription(aux.Stringid(517, sid))
                e1:SetCode(EFFECT_SPSUMMON_PROC)
                e1:SetProperty(te:GetProperty())
                e1:SetRange(LOCATION_REMOVED)
                if te:GetCondition() then
                    e1:SetCondition(te:GetCondition())
                end
                e1:SetOperation(function(...)
                    Duel.SendtoDeck(c, ttp, 0, REASON_RULE)
                    if c:IsType(TYPE_SYNCHRO) then
                    Duel.SynchroSummon(ttp, c, nil) end
                    if c:IsType(TYPE_XYZ) then
                    Duel.XyzSummon(ttp, c, nil) end
                    te:Reset()
                end)
                e1:SetValue(te:GetValue())
                c:RegisterEffect(e1)       
            end
        end

        local e4 = Effect.CreateEffect(c)
        e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_SINGLE_RANGE)
        e4:SetType(EFFECT_TYPE_SINGLE)
        e4:SetCode(EFFECT_CANNOT_DISABLE)
        e4:SetRange(LOCATION_REMOVED)
        e4:SetReset(reset)
        c:RegisterEffect(e4)
        local e121 = Effect.CreateEffect(c)
        e121:SetType(EFFECT_TYPE_SINGLE)
        e121:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
        e121:SetRange(LOCATION_REMOVED)
        e121:SetCode(EFFECT_IMMUNE_EFFECT)
        e121:SetValue(function(e,te) return te:GetOwner()~=e:GetOwner() end)
        e121:SetReset(reset)
        c:RegisterEffect(e121)
        local e122 = Effect.CreateEffect(c)
        e122:SetType(EFFECT_TYPE_SINGLE)
        e122:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
        e122:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
        e122:SetRange(LOCATION_REMOVED)
        e122:SetValue(1)
        e122:SetReset(reset)
        c:RegisterEffect(e122)
        local e123 = e121:Clone()
        e123:SetCode(EFFECT_GOD_IMMUNE)
        c:RegisterEffect(e123)
        sid = sid + 1
        end)
    end

    function DestinyDraw.xyzsumcon(e, tp, eg, ep, ev, re, r, rp)
        return eg:IsExists(DestinyDraw.xyzsumfilter, 1, nil)
    end
    function DestinyDraw.xyzsumfilter(c)
        return c:IsSetCard(0x48)
    end
    function DestinyDraw.xyzsumop(e, tp, eg, ep, ev, re, r, rp)
        if not eg:IsExists(DestinyDraw.xyzsumfilter, 1, nil) then
            return
        end
        local g = eg:Filter(DestinyDraw.xyzsumfilter, nil)
        local tc = g:GetFirst()
        while tc do
            -- cannot destroyed
            local e0 = Effect.CreateEffect(tc)
            e0:SetType(EFFECT_TYPE_SINGLE)
            e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
            e0:SetValue(DestinyDraw.indes)
            tc:RegisterEffect(e0, true)
            tc = g:GetNext()
        end
    end
    function DestinyDraw.indes(e, c)
        return not e:GetHandler():GetBattleTarget():IsSetCard(0x48)
    end

    function DestinyDraw.distg(e, tp, eg, ep, ev, re, r, rp, chk)
        if chk == 0 then
            return true
        end
        Duel.SetChainLimit(aux.FALSE)
    end

    function DestinyDraw.removecon(e, tp, eg, ep, ev, re, r, rp)
        return eg:IsExists(Card.IsCode, 1, nil, 946)
    end
    function DestinyDraw.removeop(e, tp, eg, ep, ev, re, r, rp)
        if not eg:IsExists(Card.IsCode, 1, nil, 946) then
            return
        end
        local g = eg:Filter(Card.IsCode, nil, 946)
        local tc = g:GetFirst()
        while tc do
            Duel.Exile(tc, REASON_RULE)
            tc = g:GetNext()
        end
    end

    -- Over-hundred
    function DestinyDraw.chk2(c, code)
        return c:GetOriginalCode() == code
    end
    function DestinyDraw.DDfiler(c)
        return c:IsType(TYPE_XYZ) and
                   (Duel.GetMatchingGroupCount(DestinyDraw.chk2, c:GetControler(), LOCATION_DECK + LOCATION_HAND, 0,
                       nil, 11370100) > 0 or
                       Duel.GetMatchingGroupCount(DestinyDraw.chk2, c:GetControler(), LOCATION_DECK + LOCATION_HAND, 0,
                           nil, 13705015) > 0 or
                       Duel.GetMatchingGroupCount(DestinyDraw.chk2, c:GetControler(), LOCATION_DECK + LOCATION_HAND, 0,
                           nil, 13) > 0 or
                       Duel.GetMatchingGroupCount(DestinyDraw.chk2, c:GetControler(), LOCATION_DECK + LOCATION_HAND, 0,
                           nil, 453) > 0 or
                       Duel.GetMatchingGroupCount(DestinyDraw.chk2, c:GetControler(), LOCATION_DECK + LOCATION_HAND, 0,
                           nil, 454) > 0 or
                       Duel.GetMatchingGroupCount(DestinyDraw.chk2, c:GetControler(), LOCATION_DECK + LOCATION_HAND, 0,
                           nil, 66) > 0)
    end
    function DestinyDraw.DDfiler2(c)
        local no = c.xyz_number
        return no and no >= 101 and no <= 107
        -- and not c:IsSetCard(0x1048) and not c:IsSetCard(0x2048)
    end
    -- Cartoon World
    function DestinyDraw.DTMfiler(c, tp)
        return Duel.GetMatchingGroupCount(DestinyDraw.DTMfiler2, tp, LOCATION_DECK + LOCATION_HAND, 0, nil) > 0 and
                   Duel.GetMatchingGroupCount(Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 15259703) > 0
    end
    function DestinyDraw.DTMfiler2(c)
        return c:IsSetCard(0x62) and c:IsType(TYPE_MONSTER)
    end
    function DestinyDraw.DTMfiler3(c)
        return c:IsSetCard(0x62) and c:IsFaceup()
    end
    function DestinyDraw.DTMfiler4(c)
        return c:IsCode(15259703) and c:IsFaceup()
    end
    -- Gods
    function DestinyDraw.DGDfiler(c, ttp, code1, code2, code3, code4, code5, code6)
        local TID = Duel.GetTurnCount()
        return (c:GetCode() == code1 or c:GetCode() == code4) and c:GetTurnID() == TID and c:GetPosition() ==
                   POS_FACEUP_ATTACK and
                   Duel.CheckReleaseGroup(ttp, DestinyDraw.DGDfiler3, 1, nil, code2, code5, c:GetTurnID()) and
                   Duel.CheckReleaseGroup(ttp, DestinyDraw.DGDfiler3, 1, nil, code3, code6, c:GetTurnID())
    end
    function DestinyDraw.DGDfiler3(c, code, code2, ID)
        return (c:GetCode() == code or c:GetCode() == code2) and c:GetTurnID() == ID and c:GetPosition() ==
                   POS_FACEUP_ATTACK
    end
    -- Hope
    function DestinyDraw.DDPfiler(c, qt, tp)
        return Duel.GetMatchingGroupCount(Card.IsCode, tp, 0x13 + LOCATION_REMOVED, 0, nil, c:GetCode()) < qt
    end
    function DestinyDraw.DDCfiler(c)
        return c:IsSetCard(0x7f) and c:IsSetCard(0x1048)
    end
    -- Syncho
    function DestinyDraw.RDSfiler(c)
        return c:IsCode(44508094) or c:IsCode(70902743)
    end

    function DestinyDraw.RRDSfiler(c)
        return c:IsCode(2403771)
    end
    function DestinyDraw.RRDSfiler2(c, qt)
        return c:IsType(TYPE_TUNER) and c:GetLevel() == qt
    end

    function DestinyDraw.drcon(e, tp, eg, ep, ev, re, r, rp)
        return Duel.GetTurnPlayer() == tp
    end
    function DestinyDraw.drop(e, tp, eg, ep, ev, re, r, rp)
        DestinyDraw.KDraw(tp, 1)
    end

    function DestinyDraw.drcon2(e, tp, eg, ep, ev, re, r, rp)
        return ep == tp
    end
    function DestinyDraw.drop2(e, tp, eg, ep, ev, re, r, rp)
        DestinyDraw.KDraw(tp, ev)
    end

    function DestinyDraw.KDraw(ttp, count)
        local e = Effect.GlobalEffect()
        if Duel.GetTurnCount() ~= 1 and Duel.GetMatchingGroupCount(nil, ttp, LOCATION_DECK, 0, nil) > 0 and
            ((Duel.GetFlagEffect(ttp, 99999980) ~= 0 and Duel.GetFlagEffect(ttp, 99999980) < 2 and DestinyDraw[ttp] and DestinyDraw[ttp]:GetCount()>0 and
                DestinyDraw[ttp]:FilterCount(Card.IsLocation, nil, LOCATION_DECK) > 0 
                and ((Duel.GetLP(ttp) < Duel.GetLP(1 - ttp) and Duel.GetLP(ttp) < 2500) or ((Duel.GetLP(1 - ttp) - Duel.GetLP(ttp)) >= 4000))) or
                (Duel.GetFlagEffect(ttp, 90999980) ~= 0 and Duel.GetFlagEffect(ttp, 90999980) < 3) or
                (Duel.GetFlagEffect(ttp, 92999980) ~= 0 and (Duel.GetLP(1 - ttp) - Duel.GetLP(ttp)) >= 4000) or
                (Duel.GetFlagEffect(ttp, 93999980) ~= 0 and Duel.GetLP(1 - ttp) >= Duel.GetLP(ttp) and
                    Duel.GetFlagEffect(ttp, 99999960) == 0 and
                    (Duel.GetMatchingGroupCount(c157.RDSfiler, ttp, LOCATION_MZONE, 0, nil) > 0 or
                        (Duel.GetMatchingGroupCount(c157.RRDSfiler, ttp, LOCATION_MZONE, 0, nil) > 0 and
                            Duel.GetMatchingGroupCount(c157.RRDSfiler2, ttp, LOCATION_MZONE, 0, nil, 1) > 0))) or
                (Duel.GetFlagEffect(ttp, 90799980) ~= 0 and Duel.GetLP(ttp) <= 4000 and
                    Duel.GetFieldGroupCount(ttp, LOCATION_HAND, 0) < 3) or Duel.GetFlagEffect(ttp, 388) ~= 0 or
                (Duel.GetLP(ttp) <= 2000 and Duel.GetFlagEffect(ttp, 91999980) > 0 and Duel.GetFlagEffect(ttp, 91999980) < 4) or
                (Duel.GetFlagEffect(ttp, 703) ~= 0 and
                    ((Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 16195942) > 0 and
                        Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x95) > 0) or
                        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 42160203) > 0 and
                            Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x95) > 0) or
                        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 90036274) > 0 and
                            Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_TUNER) > 0) or
                        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 82044279) > 0 and
                            Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_TUNER) > 0) or
                        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 70771599) > 0 and
                            Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_TUNER) > 0) or
                        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 41209827) > 0 and
                            Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x46) > 0) or
                        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 511009415) > 0 and
                            Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x46) > 0) or
                        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 43387895) > 0 and
                            Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x46) > 0)))) then
    
            for i = 1, count do
                local op = 0
                if Duel.GetFlagEffect(ttp, 99999980) ~= 0 and DestinyDraw[ttp]~=nil and DestinyDraw[ttp]:GetCount()>0 and DestinyDraw[ttp]:FilterCount(Card.IsLocation, nil, LOCATION_DECK)>0 and Duel.SelectYesNo(ttp, aux.Stringid(13709, 11)) then
                    local g = DestinyDraw[ttp]:Filter(Card.IsLocation, nil, LOCATION_DECK)
                    if #g > 0 then
                        DestinyDraw[ttp]:DeleteGroup()
                        DestinyDraw[ttp] = nil
                        Duel.Hint(HINT_CARD, 0, 512)
                        local tc = g:RandomSelect(ttp, 1):GetFirst()
                        local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                        if not tac2 then return end
                        aux.SwapEntity(tc, tac2)
                        --Duel.MoveSequence(tc, 0)
                        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(13709, 14))
                    end
                    Duel.RegisterFlagEffect(ttp, 99999980, 0, 0, 1)
                end
    
                if Duel.GetFlagEffect(ttp, 90999980) ~= 0 and Duel.SelectYesNo(ttp, aux.Stringid(123106, 8)) then
                    if Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD +
                        LOCATION_GRAVE + LOCATION_REMOVED, 0, nil, 57734012) < 4 and
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_HAND, 0, nil, 57734012) == 0 then
                        op = Duel.SelectOption(ttp, aux.Stringid(13717, 1), aux.Stringid(123106, 8))
                    end
                    if op == 0 then
                        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TARGET)
                        local destinytc = Duel.SelectMatchingCard(ttp, nil, ttp, LOCATION_DECK, 0, 1, 1, nil):GetFirst()
                        Duel.ShuffleDeck(ttp)    
                        local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                        if not tac2 then return end
                        aux.SwapEntity(destinytc, tac2)
                        --Duel.MoveSequence(destinytc, 0)
                    end
                    if op == 1 then
                        if Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 57734012) == 0 then
                            local token = Duel.CreateToken(ttp, 11370100)
                            Duel.DisableShuffleCheck()
                            Duel.SendtoDeck(token, ttp, 0, REASON_RULE)
                        else
                            local destinytc = Duel.GetFirstMatchingCard(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 57734012)
                            local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                            if not tac2 then return end
                            aux.SwapEntity(destinytc, tac2)
                            --Duel.MoveSequence(destinytc, 0)
                        end
                    end
                    Duel.RegisterFlagEffect(ttp, 90999980, 0, 0, 1)
                end
    
                if Duel.GetFlagEffect(ttp, 89999998) ~= 0 then
                    Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(123106, 8))
                    Duel.RegisterFlagEffect(ttp, 99999960, 0, 0, 1)
                end
    
                if Duel.GetFlagEffect(ttp, 91999980) ~= 0 and Duel.SelectYesNo(ttp, aux.Stringid(111011901, 1)) then
                    if Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_REMOVED + LOCATION_MZONE, 0, nil, 0x7f) > 0 or
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 93717133) > 0 then
                        op = Duel.SelectOption(ttp, aux.Stringid(13717, 1), aux.Stringid(13717, 2))
                    end
                    if op == 0 then
                        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TARGET)
                        local destinytc = Duel.SelectMatchingCard(ttp, nil, ttp, LOCATION_DECK, 0, 1, 1, nil):GetFirst()
                        Duel.ShuffleDeck(ttp) 
                        local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                        if not tac2 then return end
                        aux.SwapEntity(destinytc, tac2)
                        --Duel.MoveSequence(destinytc, 0)
                    end
                    if op == 1 then
                        -- local ZWP = Group.CreateGroup()
                        -- if Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_MZONE, 0, nil, 0x7f) > 0 then
                        --     local pd1 = Duel.CreateToken(ttp, 266, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 81471108) < 1 then
                        --         ZWP:AddCard(pd1)
                        --     end
                        --     local pd2 = Duel.CreateToken(ttp, 264, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 45082499) < 1 then
                        --         ZWP:AddCard(pd2)
                        --     end
                        --     local pd3 = Duel.CreateToken(ttp, 262, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 2648201) < 1 and
                        --         Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 56840427) < 1 then
                        --         ZWP:AddCard(pd3)
                        --     end
                        --     local pd4 = Duel.CreateToken(ttp, 40941889, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 40941889) < 1 then
                        --         ZWP:AddCard(pd4)
                        --     end
                        --     local pd14 = Duel.CreateToken(ttp, 261, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 261) < 1 then
                        --         ZWP:AddCard(pd14)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_MZONE, 0, nil, 0x7f) > 0 and
                        --     Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_GRAVE, 0, nil, 0x7e) > 0 then
                        --     local pd5 = Duel.CreateToken(ttp, 12927849, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 12927849) < 1 then
                        --         ZWP:AddCard(pd5)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(function(c) return c:IsType(TYPE_XYZ) and c:IsFaceup() end, ttp, LOCATION_REMOVED, 0, nil) > 0 then
                        --     local pd6 = Duel.CreateToken(ttp, 263, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 18865703) < 1 then
                        --         ZWP:AddCard(pd6)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_MZONE, 0, nil, 0x7f) > 0 and
                        --     (Duel.GetLP(1 - ttp) - Duel.GetLP(ttp)) >= 2000 then
                        --     local pd7 = Duel.CreateToken(ttp, 29353756, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 29353756) < 1 then
                        --         ZWP:AddCard(pd7)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 56840427) > 0 then
                        --     local pd8 = Duel.CreateToken(ttp, 76080032, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 76080032) < 1 then
                        --         ZWP:AddCard(pd8)
                        --     end
                        --     local pd9 = Duel.CreateToken(ttp, 267, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 87008374) < 1 then
                        --         ZWP:AddCard(pd9)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_MZONE, 0, nil, 0x7f) > 0 and
                        --     Duel.GetMatchingGroupCount(c157.rankfiler, 1 - ttp, 0, LOCATION_MZONE, nil) > 0 then
                        --     local pd10 = Duel.CreateToken(ttp, 511010513, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 71345905) < 1 then
                        --         ZWP:AddCard(pd10)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(c157.DDCfiler, ttp, LOCATION_MZONE, 0, nil) > 0 then
                        --     local pd12 = Duel.CreateToken(ttp, 258, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 6330307) < 1 then
                        --         ZWP:AddCard(pd12)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 93717133) > 0 then
                        --     local pd13 = Duel.CreateToken(ttp, 51865604, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 51865604) < 1 then
                        --         ZWP:AddCard(pd13)
                        --     end
                        -- end
                        -- if Duel.GetMatchingGroupCount(c157.hopefilter, ttp, LOCATION_MZONE, 0, nil) > 0 and
                        --     Duel.IsExistingTarget(c157.zspfilter, ttp, LOCATION_GRAVE, 0, 1, nil, e, ttp) then
                        --     local pd16 = Duel.CreateToken(ttp, 438, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 438) < 1 then
                        --         ZWP:AddCard(pd16)
                        --     end
                        -- end
                        -- if Duel.IsExistingMatchingCard(c157.filterno, ttp, LOCATION_GRAVE, 0, 2, nil, e, ttp) then
                        --     local pd17 = Duel.CreateToken(ttp, 366, nil, nil, nil, nil, nil, nil)
                        --     if Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                        --         LOCATION_DECK + LOCATION_HAND + LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_REMOVED, 0,
                        --         nil, 366) < 1 then
                        --         ZWP:AddCard(pd17)
                        --     end
                        -- end
                        -- local dtz = ZWP:Filter(c157.DDPfiler, nil, 1, ttp)
                        -- if not dtz then
                        --     dtz = ZWP:Filter(c157.DDPfiler, nil, 3, ttp)
                        -- end
                        -- local bpd = Duel.GetMatchingGroupCount(nil, ttp, LOCATION_DECK, 0, nil)
                        -- local btc = 0
                        -- local dtzc = dtz:GetCount()
                        -- if dtzc == 0 then
                        --     dtzc = 1
                        -- end
                        -- if bpd > 0 then
                        --     btc = bpd % (dtzc)
                        -- end
                        -- local ttc = dtz:GetFirst()
                        -- if dtz:GetCount() > 0 then
                        --     while btc > 0 do
                        --         ttc = dtz:GetNext()
                        --         btc = btc - 1
                        --     end
                        -- end
                        -- local token = Duel.CreateToken(ttp, ttc:GetOriginalCode(), nil, nil, nil, nil, nil, nil)
                        -- Duel.DisableShuffleCheck()
                        -- Duel.SendtoDeck(token, ttp, 0, REASON_RULE)
                        Duel.Hint(HINT_SELECTMSG,ttp,HINTMSG_CODE)
                        local code = Duel.AnnounceCard(ttp, 0x7e, OPCODE_ISSETCARD,TYPE_XYZ, OPCODE_ISTYPE, OPCODE_NOT, OPCODE_AND, OPCODE_ALLOW_ALIASES)
                        local tg = Duel.GetFirstMatchingCard(Card.IsCode, ttp, LOCATION_DECK, 0, nil, code)
                        if tg then
                            local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                            if not tac2 then return end
                            aux.SwapEntity(tg, tac2)
                            --Duel.MoveSequence(tg, 0)
                        else
                            local token = Duel.CreateToken(ttp, code)
                            Duel.SendtoDeck(token, ttp, 0, REASON_RULE)
                        end
                    end
                    if Duel.GetFlagEffect(ttp, 89999997) ~= 0 and Duel.GetFlagEffect(ttp, 91999980) == 1 then
                        Duel.Hint(HINT_CARD, 0, 510000103)
                    end
                    if Duel.GetFlagEffect(ttp, 89999997) ~= 0 and Duel.GetFlagEffect(ttp, 91999980) == 2 then
                        Duel.Hint(HINT_CARD, 0, 510000104)
                    end
                    if Duel.GetFlagEffect(ttp, 89999997) ~= 0 and Duel.GetFlagEffect(ttp, 91999980) == 3 then
                        Duel.Hint(HINT_CARD, 0, 510000105)
                    end
                    Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(111011901, 2))
                    Duel.RegisterFlagEffect(ttp, 99999960, 0, 0, 1)
                    Duel.RegisterFlagEffect(ttp, 91999980, 0, 0, 1)
                end
    
                if Duel.GetFlagEffect(ttp, 92999980) ~= 0 and
                    Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_SPELL) > 0 and
                    Duel.SelectYesNo(ttp, aux.Stringid(123106, 7)) then
                    local g = Duel.GetMatchingGroup(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_SPELL)
                    local g2 = g:RandomSelect(ttp, 1):GetFirst()
                    local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                    if not tac2 then return end
                    aux.SwapEntity(g2, tac2)
                    --Duel.MoveSequence(g2:GetFirst(), 0)
                    if Duel.GetFlagEffect(ttp, 89999995) ~= 0 then
                        Duel.Hint(HINT_CARD, 0, 510000106)
                    end
                    Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(123106, 7))
                end
    
                if Duel.GetFlagEffect(ttp, 93999980) ~= 0 then
                    if Duel.GetMatchingGroupCount(c157.RDSfiler, ttp, LOCATION_MZONE, 0, nil) > 0 and
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp,
                            LOCATION_ONFIELD + LOCATION_GRAVE + LOCATION_HAND + LOCATION_REMOVED, 0, nil, 21159309) < 3 and
                        Duel.SelectYesNo(ttp, aux.Stringid(13709, 12)) then
                        if Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 21159309) == 0 then
                            local token = Duel.CreateToken(ttp, 21159309, nil, nil, nil, nil, nil, nil)
                            Duel.DisableShuffleCheck()
                            Duel.SendtoDeck(token, ttp, 0, REASON_RULE)
                        else
                            local destinytc = Duel.GetFirstMatchingCard(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 7841112)      
                            local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                            if not tac2 then return end
                            aux.SwapEntity(destinytc, tac2) 
                            --Duel.MoveSequence(destinytc, 0)
                        end
                        if Duel.GetFlagEffect(ttp, 89999996) ~= 0 then
                            Duel.Hint(HINT_CARD, 0, 510000107)
                        end
                        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(13709, 15))
                        Duel.RegisterFlagEffect(ttp, 93999960, 0, 0, 1)
                        Duel.RegisterFlagEffect(ttp, 99999960, 0, 0, 1)
                    end
                end
    
                if Duel.GetFlagEffect(ttp, 90799980) ~= 0 and Duel.SelectYesNo(ttp, aux.Stringid(826, 6)) then
                    Duel.Hint(HINT_SELECTMSG,ttp,HINTMSG_CODE)
                    local code = Duel.AnnounceCard(ttp, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK, OPCODE_ISTYPE,
                                     OPCODE_NOT,OPCODE_ALLOW_ALIASES)
                    local tg = Duel.GetFirstMatchingCard(Card.IsCode, ttp, LOCATION_DECK, 0, nil, code)
                    if tg then
                        local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                        if not tac2 then return end
                        aux.SwapEntity(tg, tac2)
                        --Duel.MoveSequence(tg, 0)
                    else
                        local token = Duel.CreateToken(ttp, code)
                        Duel.SendtoDeck(token, ttp, 0, REASON_RULE)
                    end
                    if Duel.GetFlagEffect(ttp, 89999993) ~= 0 then
                        Duel.Hint(HINT_CARD, 0, 510000108)
                    end
                    Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(826, 6))
                    -- Duel.RegisterFlagEffect(ttp,99999960,0,0,1)
                end
    
                if Duel.GetFlagEffect(ttp, 388) ~= 0 and
                    Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x95) > 0 and
                    Duel.SelectYesNo(ttp, aux.Stringid(13713, 8)) then
                    Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
                    local destinytc =
                        Duel.SelectMatchingCard(ttp, Card.IsSetCard, ttp, LOCATION_DECK, 0, 1, 1, nil, 0x95):GetFirst()
                    Duel.ShuffleDeck(ttp) 
                    local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                    if not tac2 then return end
                    aux.SwapEntity(destinytc, tac2)        
                    --Duel.MoveSequence(destinytc, 0)
                    Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(13713, 8))
                end
    
                if Duel.GetFlagEffect(ttp, 703) ~= 0 and Duel.SelectYesNo(ttp, aux.Stringid(827, 1)) then
                    local off = 1
                    local ops = {}
                    local opval = {}
                    if (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 16195942) > 0 or
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 42160203) > 0) and
                        Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x95) > 0 then
                        ops[off] = aux.Stringid(827, 2)
                        opval[off - 1] = 1
                        off = off + 1
                    end
                    if (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 90036274) > 0 or
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 82044279) > 0 or
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 70771599) > 0) and
                        Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, TYPE_TUNER) > 0 then
                        ops[off] = aux.Stringid(827, 3)
                        opval[off - 1] = 2
                        off = off + 1
                    end
                    if (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 41209827) > 0 or
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 511009415) > 0 or
                        Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_MZONE, 0, nil, 43387895) > 0) and
                        Duel.GetMatchingGroupCount(Card.IsSetCard, ttp, LOCATION_DECK, 0, nil, 0x46) > 0 then
                        ops[off] = aux.Stringid(827, 4)
                        opval[off - 1] = 3
                        off = off + 1
                    end
                    local op = Duel.SelectOption(ttp, table.unpack(ops))
                    if opval[op] == 1 then
                        Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
                        local destinytc = Duel.SelectMatchingCard(ttp, Card.IsSetCard, ttp, LOCATION_DECK, 0, 1, 1, nil,
                                              0x95):GetFirst()
                        Duel.ShuffleDeck(ttp)  
                        local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                        if not tac2 then return end
                        aux.SwapEntity(destinytc, tac2)                           
                        --Duel.MoveSequence(destinytc, 0)
                        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(827, 1))
                    end
                    if opval[op] == 2 then
                        Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
                        local destinytc = Duel.SelectMatchingCard(ttp, Card.IsType, ttp, LOCATION_DECK, 0, 1, 1, nil,
                                              TYPE_TUNER):GetFirst()
                        Duel.ShuffleDeck(ttp)       
                        local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                        if not tac2 then return end
                        aux.SwapEntity(destinytc, tac2)                   
                        --Duel.MoveSequence(destinytc, 0)
                        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(827, 1))
                    end
                    if opval[op] == 3 then
                        Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
                        local destinytc = Duel.SelectMatchingCard(ttp, Card.IsSetCard, ttp, LOCATION_DECK, 0, 1, 1, nil,
                                              0x46):GetFirst()
                        Duel.ShuffleDeck(ttp)    
                        local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
                        if not tac2 then return end
                        aux.SwapEntity(destinytc, tac2)                     
                        --Duel.MoveSequence(destinytc, 0)
                        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(827, 1))
                    end
                end
            end
        end
    end    
    
    finishsetup()
end

-- local draw = Duel.Draw
-- function Duel.Draw(ttp, count, reason)
--     aux.KDraw(ttp, count)
--     return draw(ttp, count, reason)
-- end

local matchg = Duel.GetMatchingGroup
function Duel.GetMatchingGroup(f, tp, s, o, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    -- local nf = function(c) return f(c, ...) and c:GetFlagEffect(157)==0 end
    local nf =f
    local mattg = matchg(nf, tp, s, o, ex, ...)
    if not mattg then
		return nil
    end

    local re = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_EFFECT)
    local rp = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_PLAYER)
    local gcount = 0
    if bit.band(s, LOCATION_EXTRA) ~= 0 and s ~= 0xff 
    and (re==nil or (re and re:IsHasCategory(CATEGORY_SPECIAL_SUMMON))) and rp==tp
    and Duel.GetFlagEffect(tp, 157) ~= 0 then
        --if tp==0 then mattg:Merge(xyztempg0) else mattg:Merge(xyztempg1) end
  
        if re and Duel.SelectYesNo(tp, aux.Stringid(826, 12)) then
            gcount = gcount + 1
            local sp,tg,count=Duel.GetOperationInfo(0, CATEGORY_SPECIAL_SUMMON)
            local exa=Group.CreateGroup()
            for i = 1, count do
                if i==1 or Duel.SelectYesNo(tp, aux.Stringid(826, 12)) then
                    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE)
                    local code = Duel.AnnounceCard(tp, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK, OPCODE_ISTYPE, 827, OPCODE_ISCODE, OPCODE_NOT, OPCODE_AND,OPCODE_ALLOW_ALIASES)
                    local token= Duel.CreateToken(tp, code)
                    for index, value in ipairs(Announce[tp]) do
                        if value == code and Duel.GetFlagEffect(tp, 158) == 0 then
                            goto continue 
                        end
                    end
                    token:SetEntityCode(code, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
                    -- if tp==0 then 
                    --     xyztempg0:RemoveCard(extc)
                    --     xyztempg0:AddCard(token)
                    -- else 
                    --     xyztempg1:RemoveCard(extc)
                    --     xyztempg1:AddCard(token)
                    -- end
                    if nf(token, ...) then
                        Duel.SendtoDeck(token, tp, 0, REASON_RULE)
                        exa:AddCard(token)
                        table.insert(Announce[tp], code)               
                    end
                    ::continue::
                end
            end
            local exg2 = mattg
            if gcount < count then
                local extra = exg2:Select(tp, 1, count - gcount, false)
                exa:Merge(extra)
            end
            return exa
            --DestinyDraw.extra(exa, RESET_CHAIN)
        end
    end
    return mattg
end

-- local gselun = Group.SelectUnselect
-- function Group.SelectUnselect(g, sg, tp, btok, cancelable, minc, maxc)
--     if not minc then
--         minc = 1
--     end
--     if not maxc then
--         maxc = 1
--     end
--     local exa = Group.CreateGroup()
--     local gcount = 0
--     if g:FilterCount(function(c)
--         return (c:IsLocation(LOCATION_EXTRA) and c:IsControler(tp) and c:IsFacedown() and
--                    bit.band(c:GetType(), TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK) ~= 0) or c:IsCode(827)
--     end, nil) > 0 and Duel.GetFlagEffect(tp, 157) ~= 0 then
--         local exg = g:Filter(Card.IsCode, nil, 827)
--         g:Sub(exg)
--         local extc=nil
--         if exg:GetCount() > 0 then
--             extc = exg:GetFirst()
--         end
--         for i = 1, maxc do
--             if gcount >= minc and not Duel.SelectYesNo(tp, aux.Stringid(826, 12)) then
--                 break
--             end
--             if g:GetCount() < 1 or (gcount < minc and Duel.SelectYesNo(tp, aux.Stringid(826, 12)) or gcount >= minc) then
--                 Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE)
--                 local code = Duel.AnnounceCard(tp, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK, OPCODE_ISTYPE, 827, OPCODE_ISCODE, OPCODE_NOT, OPCODE_AND,OPCODE_ALLOW_ALIASES)
--                 if extc==nil then break
--                     --extc = Duel.CreateToken(tp, code)
--                 end
--                 extc:SetEntityCode(code, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
--                 Duel.SendtoDeck(extc, tp, 0, REASON_RULE)
--                 exa:Merge(Group.FromCards(extc))
--                 local token= Duel.CreateToken(tp, 827)
--                 if tp==0 then 
--                     xyztempg0:RemoveCard(extc)
--                     xyztempg0:AddCard(token)
--                 else 
--                     xyztempg1:RemoveCard(extc)
--                     xyztempg1:AddCard(token)
--                 end
--             else
--                 local bg = g:Select(tp, 1, 1, nil)
--                 exa:Merge(bg)
--                 g:Sub(bg)
--             end
--             extc = exg:GetNext()
--             gcount = gcount + 1
--         end
--     end
--     g:Sub(exa)
--     sg:Merge(exa)
--     local minct = minc - gcount
--     if minct < 1 then
--         minct = 1
--     end
--     if maxc - gcount < 1 then
--         return sg
--     end
--     if #g < 1 then
--         return
--     end
--     return gselun(g, sg, tp, btok, cancelable, minct, maxc - gcount)
-- end
-- local grandsel = Group.RandomSelect
-- function Group.RandomSelect(g, tp, count)
--     if tp == nil then
--         tp = 0
--     end
--     local exa = Group.CreateGroup()
--     local gcount = 0
--     local re = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_EFFECT)
--     if g:FilterCount(function(c)
--         return (c:IsLocation(LOCATION_EXTRA) and c:IsControler(tp) and c:IsFacedown() and
--                    bit.band(c:GetType(), TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK) ~= 0) or c:IsCode(827)
--     end, nil) > 0 
--         and re and re:IsHasCategory(CATEGORY_SPECIAL_SUMMON)
--         and Duel.GetFlagEffect(tp, 157) ~= 0 then
--         local exg = g:Filter(Card.IsCode, nil, 827)
--         g:Sub(exg)
--         local extc=nil
--         if exg:GetCount() > 0 then
--             extc = exg:GetFirst()
--         end
--         for i = 1, count do
--             if g:GetCount() < 1 or Duel.SelectYesNo(tp, aux.Stringid(826, 12)) then
--                 Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE)
--                 local code = Duel.AnnounceCard(tp, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK, OPCODE_ISTYPE, 827, OPCODE_ISCODE, OPCODE_NOT, OPCODE_AND,OPCODE_ALLOW_ALIASES)
--                 if extc==nil then break
--                     --extc = Duel.CreateToken(tp, code)
--                 end
--                 Announce = Announce0
--                 if tp == 1 then
--                     Announce = Announce1
--                 end
--                 for index, value in ipairs(Announce) do
--                     if value == code and Duel.GetFlagEffect(tp, 158) == 0 then
--                         goto continue 
--                     end
--                 end
--                 extc:SetEntityCode(code, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
--                 Duel.SendtoDeck(extc, tp, 0, REASON_RULE)
--                 if tp == 0 then
--                     table.insert(Announce, code)
--                 else table.insert(Announce1, code)
--                 end                  
--                 exa:Merge(Group.FromCards(extc))
--                 local token= Duel.CreateToken(tp, 827)
--                 if tp==0 then 
--                     xyztempg0:RemoveCard(extc)
--                     xyztempg0:AddCard(token)
--                 else 
--                     xyztempg1:RemoveCard(extc)
--                     xyztempg1:AddCard(token)
--                 end
--             else
--                 local bg = grandsel(g, tp, 1)
--                 exa:Merge(bg)
--                 g:Sub(bg)
--             end
--             ::continue::
--             extc = exg:GetNext()
--             gcount = gcount + 1
--         end
--     end
--     local minct = count - gcount
--     if minct < 1 then
--         return exa
--     end
--     if #g < 1 then
--         return Group.CreateGroup()
--     end
--     local fg = grandsel(g:Filter(function(c) return not c:IsCode(827) and not c:IsCode(946) and not exa:IsContains(c) end,nil), tp, minct)
--     if #exa < 1 then
--         return fg
--     end
--     fg:Merge(exa)
--     return fg
-- end
-- local gexist=Group.IsExists
-- function Group.IsExists(g, f, count, ex, ...)
--     if tp==nil then tp=0 end
--     if g:FilterCount(function(c) return c:IsLocation(LOCATION_EXTRA) and c:IsControler(tp) and bit.band(c:GetType(),TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ+TYPE_LINK)~=0 end,nil)>0 then return true end
--     return gexist(g, f, count, ex, ...)
-- end
-- local filsel = Group.FilterSelect
-- function Group.FilterSelect(g, tp, f, min, max, cancel, ex, ...)
--     if tp == nil then
--         tp = 0
--     end
--     local nf = function(c) return f(c, ...) and c:GetFlagEffect(157)==0 end
--     local params={ex,...}
--     if cancel==nil or type(cancel)=="Card" or type(cancel)=="Group" then
--         ex=cancel
--         cancel=false
--     else params={...}
--     end

--     local exa = Group.CreateGroup()
--     local gcount = 0
--     local re = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_EFFECT)
--     if g:FilterCount(function(c)
--         return (c:IsLocation(LOCATION_EXTRA) and c:IsControler(tp) and c:IsFacedown() 
--         and bit.band(c:GetType(), TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK) ~= 0) or c:IsCode(827) end, nil) > 0 
--         and re and re:IsHasCategory(CATEGORY_SPECIAL_SUMMON)
--         and Duel.GetFlagEffect(tp, 157) ~= 0 then
--         local exg = g:Filter(Card.IsCode, nil, 827)
--         g:Sub(exg)
--         local extc=nil
--         if exg:GetCount() > 0 then
--             extc = exg:GetFirst()
--         end
--         for i = 1, max do
--             if gcount >= min and not Duel.SelectYesNo(tp, aux.Stringid(826, 12)) then
--                 break
--             end
--             if g:FilterCount(nf, ex, table.unpack(params)) < 1 or
--                 (gcount < min and Duel.SelectYesNo(tp, aux.Stringid(826, 12)) or gcount >= min) then
--                 Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE)    
--                 local code = Duel.AnnounceCard(tp, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK, OPCODE_ISTYPE, 827, OPCODE_ISCODE, OPCODE_NOT, OPCODE_AND,OPCODE_ALLOW_ALIASES)
--                 if extc==nil then break
--                     --extc = Duel.CreateToken(tp, code)
--                 end
--                 Announce = Announce0
--                 if tp == 1 then
--                     Announce = Announce1
--                 end
--                 for index, value in ipairs(Announce) do
--                     if value == code and Duel.GetFlagEffect(tp, 158) == 0 then
--                         goto continue 
--                     end
--                 end
--                 extc:SetEntityCode(code, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
--                 Duel.SendtoDeck(extc, tp, 0, REASON_RULE)
--                 if tp == 0 then
--                     table.insert(Announce, code)
--                 else table.insert(Announce1, code)
--                 end                  
--                 if f(extc, table.unpack(params)) then
--                     exa:Merge(Group.FromCards(extc))
--                 local token= Duel.CreateToken(tp, 827)
--                 if tp==0 then 
--                     xyztempg0:RemoveCard(extc)
--                     xyztempg0:AddCard(token)
--                 else 
--                     xyztempg1:RemoveCard(extc)
--                     xyztempg1:AddCard(token)
--                 end
--                 end
--             else
--                 local bg = filsel(g, tp, nf, 1, 1, ex, table.unpack(params))
--                 exa:Merge(bg)
--                 g:Sub(bg)
--             end
--             ::continue::
--             gcount = gcount + 1
--             extc = exg:GetNext()
--         end
--     end
--     local minct = min - gcount
--     if minct < 1 then
--         minct = 1
--     end
--     if max - gcount < 1 then
--         return exa
--     end
--     if #g < 1 then
--         return Group.CreateGroup()
--     end
--     local fg = filsel(g:Filter(function(c) return not c:IsCode(827) and not c:IsCode(946) and not exa:IsContains(c) end,nil), tp, nf, minct, max - gcount, cancel, ex, table.unpack(params))
--     if #exa < 1 then
--         return fg
--     end
--     fg:Merge(exa)
--     return fg
-- end
-- local gsel = Group.Select
-- function Group.Select(g, tp, min, max, cancel, ex)
--     if tp == nil then
--         tp = 0
--     end
--     if cancel==nil or type(cancel)=="Card" or type(cancel)=="Group" then
--         ex=cancel
--         cancel=false
--     end

--     local exa = Group.CreateGroup()
--     local gcount = 0
--     local re = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_EFFECT)
--     if g:FilterCount(function(c)
--         return (c:IsLocation(LOCATION_EXTRA) and c:IsControler(tp) and c:IsFacedown() 
--         and bit.band(c:GetType(), TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK) ~= 0) or c:IsCode(827) end, nil) > 0 
--         and re and re:IsHasCategory(CATEGORY_SPECIAL_SUMMON) 
--         and Duel.GetFlagEffect(tp, 157) ~= 0 then
--         local exg = g:Filter(Card.IsCode, nil, 827)
--         g:Sub(exg)
--         local extc=nil
--         if exg:GetCount() > 0 then
--             extc = exg:GetFirst()
--         end
--         for i = 1, max do
--             if gcount >= min and not Duel.SelectYesNo(tp, aux.Stringid(826, 12)) then
--                 break
--             end
--             if g:GetCount() < 1 or (gcount < min and Duel.SelectYesNo(tp, aux.Stringid(826, 12)) or gcount >= min) then
--                 Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE)
--                 local code = Duel.AnnounceCard(tp, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK, OPCODE_ISTYPE, 827, OPCODE_ISCODE, OPCODE_NOT, OPCODE_AND,OPCODE_ALLOW_ALIASES)
--                 if extc==nil then break
--                     --extc = Duel.CreateToken(tp, code)
--                 end
--                 Announce = Announce0
--                 if tp == 1 then
--                     Announce = Announce1
--                 end
--                 for index, value in ipairs(Announce) do
--                     if value == code and Duel.GetFlagEffect(tp, 158) == 0 then
--                         goto continue 
--                     end
--                 end
--                 extc:SetEntityCode(code, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
--                 Duel.SendtoDeck(extc, tp, 0, REASON_RULE)
--                 if tp == 0 then
--                     table.insert(Announce, code)
--                 else table.insert(Announce1, code)
--                 end                  
--                 exa:Merge(Group.FromCards(extc))
--                 local token= Duel.CreateToken(tp, 827)
--                 if tp==0 then 
--                     xyztempg0:RemoveCard(extc)
--                     xyztempg0:AddCard(token)
--                 else 
--                     xyztempg1:RemoveCard(extc)
--                     xyztempg1:AddCard(token)
--                 end
--             else
--                 local bg = gsel(g, tp, 1, 1, ex)
--                 exa:Merge(bg)
--                 g:Sub(bg)
--             end
--             extc = exg:GetNext()
--             ::continue::
--             gcount = gcount + 1
--         end
--     end
--     local minct = min - gcount
--     if minct < 1 then
--         minct = 1
--     end
--     if max - gcount < 1 then
--         return exa
--     end
--     if #g < 1 then
--         return Group.CreateGroup()
--     end
--     local fg = gsel(g:Filter(function(c) return not c:IsCode(827) and not c:IsCode(946) and not exa:IsContains(c) end,nil), tp, minct, max - gcount, cancel, ex)
--     if #exa < 1 then
--         return fg
--     end
--     fg:Merge(exa)
--     return fg
-- end

local exist = Duel.IsExistingMatchingCard
function Duel.IsExistingMatchingCard(f, tp, s, o, count, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end
    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end

    if bit.band(s, LOCATION_EXTRA) ~= 0 and s ~= 0xff and Duel.GetFlagEffect(tp, 157) ~= 0 then
        return true
    end
    return exist(nf, tp, s, o, count, ex, ...)
end
local existt = Duel.IsExistingTarget
function Duel.IsExistingTarget(f, tp, s, o, count, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end
    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end

    if bit.band(s, LOCATION_EXTRA) ~= 0 and s ~= 0xff and Duel.GetFlagEffect(tp, 157) ~= 0 then
        return true
    end
    return existt(nf, tp, s, o, count, ex, ...)
end
local selectc = Duel.SelectMatchingCard
function Duel.SelectMatchingCard(sel_player, f, tp, s, o, mint, maxt, cancel, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end
    local params={ex,...}
    if cancel==nil or type(cancel)=="Card" or type(cancel)=="Group" then
        ex=cancel
        cancel=false
    else params={...}
    end

    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end

    local exa = Group.CreateGroup()
    local exa2 = Group.CreateGroup()
    local g = matchg(nf, tp, s, o, ex, table.unpack(params))
    --if g:GetCount() < 1 then return nil end
    -- local exg = g:Filter(Card.IsCode, nil, 827)
    -- g:Sub(exg)
    local gcount = 0
    local re = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_EFFECT)
    if bit.band(s, LOCATION_EXTRA) ~= 0 and sel_player == tp 
    and re and re:IsHasCategory(CATEGORY_SPECIAL_SUMMON)
    and Duel.GetFlagEffect(tp, 157) ~= 0 then
        for i = 1, maxt do
            if gcount >= mint and not Duel.SelectYesNo(sel_player, aux.Stringid(826, 12)) then
                break
            end
            if g:GetCount() < 1 or
                (gcount < mint and Duel.SelectYesNo(sel_player, aux.Stringid(826, 12)) or gcount >= mint) then
                Duel.Hint(HINT_SELECTMSG,sel_player,HINTMSG_CODE)    
                local code = Duel.AnnounceCard(sel_player, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK,
                                 OPCODE_ISTYPE,OPCODE_ALLOW_ALIASES)
                local token = Duel.CreateToken(sel_player, code)
                for index, value in ipairs(Announce[sel_player]) do
                    if value == code and Duel.GetFlagEffect(sel_player, 158) == 0 then
                        goto continue
                    end
                end
                table.insert(Announce[sel_player], code)
                if code == 27 or code == 28 or code == 29 or code == 36 then
                    aux.chaoschange(code, token)
                end
                if nf(token, table.unpack(params)) then
                    Duel.SendtoDeck(token, sel_player, 0, REASON_RULE)
                    exa:Merge(Group.FromCards(token))
                else
                    exa2:Merge(Group.FromCards(token))
                end
            else
                if g:GetCount() >0 then
                    local bg = g:Select(tp, 1, 1, false)
                    exa:Merge(bg)
                    g:Sub(bg)
                end
            end
            ::continue::
            gcount = gcount + 1
        end
    end
    local minct = mint - gcount
    if minct < 1 then
        minct = 1
    end
    if maxt - gcount < 1 then
        return exa
    end
    local fg = selectc(sel_player, function(c) return nf(c, table.unpack(params)) and not c:IsCode(827) and not c:IsCode(946) and not exa:IsContains(c) and not exa2:IsContains(c) end, tp, s, o, minct, maxt - gcount, ex)
    fg:Merge(exa)
    return fg
end
local selectt = Duel.SelectTarget
function Duel.SelectTarget(sel_player, f, tp, s, o, mint, maxt, cancel, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end
    local params={ex,...}
    if cancel==nil or type(cancel)=="Card" or type(cancel)=="Group" then
        ex=cancel
        cancel=false
    else params={...}
    end

    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end

    local exa = Group.CreateGroup()
    local exa2 = Group.CreateGroup()
    local g = matchg(function(c) return nf(c, table.unpack(params)) and c:IsCanBeEffectTarget() end, tp, s, o, ex)
    --if g:GetCount()<1 then return nil end
    -- local exg = g:Filter(Card.IsCode, nil, 827)
    -- g:Sub(exg)
    local gcount = 0
    local re = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_EFFECT)
    if bit.band(s, LOCATION_EXTRA) ~= 0 and sel_player == tp 
    and re and re:IsHasCategory(CATEGORY_SPECIAL_SUMMON)
    and Duel.GetFlagEffect(tp, 157) ~= 0 then
        for i = 1, maxt do
            if gcount >= mint and not Duel.SelectYesNo(sel_player, aux.Stringid(826, 12)) then
                break
            end
            if g:GetCount() < 1 or 
                (gcount < mint and Duel.SelectYesNo(sel_player, aux.Stringid(826, 12)) or gcount >= mint) then
                Duel.Hint(HINT_SELECTMSG,sel_player,HINTMSG_CODE) 
                local code = Duel.AnnounceCard(sel_player, TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK,
                                 OPCODE_ISTYPE,OPCODE_ALLOW_ALIASES)
                local token = Duel.CreateToken(sel_player, code)
                for index, value in ipairs(Announce[sel_player]) do
                    if value == code and Duel.GetFlagEffect(sel_player, 158) == 0 then
                        goto continue 
                    end
                end
                table.insert(Announce[sel_player], code)
                if code == 27 or code == 28 or code == 29 or code == 36 then
                    aux.chaoschange(code, token)
                end
                if nf(token,table.unpack(params)) then
                    Duel.SendtoDeck(token, sel_player, 0, REASON_RULE)
                    exa:Merge(Group.FromCards(token))
                else
                    exa2:Merge(Group.FromCards(token))
                end
            else
                if g:GetCount() >0 then
                    local bg = g:Select(tp, 1, 1, false)
                    exa:Merge(bg)
                    g:Sub(bg)
                end
            end
            gcount = gcount + 1
            ::continue::
        end
    end
    local minct = mint - gcount
    if minct < 1 then
        minct = 1
    end
    if maxt - gcount < 1 then
        return exa
    end
    local fg = selectt(sel_player, function(c) return nf(c, table.unpack(params)) and not c:IsCode(827) and not c:IsCode(946) and not exa:IsContains(c) and not exa2:IsContains(c) end, tp, s, o, minct, maxt - gcount, ex)
    fg:Merge(exa)
    return fg
end

function aux.chaoschange(code, xyz)
    local g = Duel.GetChainInfo(0, CHAININFO_TARGET_CARDS)
    if g:GetCount() == 1 and g:GetFirst():IsType(TYPE_XYZ) then
        local tc = g:GetFirst()
        local nochk = 0
        local alias = tc:GetOriginalCodeRule()
        if (code == 27 and not tc:IsSetCard(0x48) and not tc:IsSetCard(0x1073) and not tc:IsSetCard(0x4073) and
            tc:GetRank() < 13 and tc:GetRank() == tc:GetOriginalRank() and not aux.cxlist[alias]) or
            (code == 28 and tc:IsSetCard(0x48) and not tc:IsSetCard(0x1048) and not tc:IsSetCard(0x2048) and
                tc:GetRank() < 13 and tc:GetRank() == tc:GetOriginalRank() and not aux.notoclist[tc.xyz_number]) or
            (code == 29 and not tc:IsSetCard(0x48) and not tc:IsSetCard(0x1073) and not tc:IsSetCard(0x2073) and
                tc:GetRank() < 13 and tc:GetRank() == tc:GetOriginalRank()) or
            (code == 36 and tc:IsSetCard(0x48) and not tc:IsSetCard(0x1048) and not tc:IsSetCard(0x2048) and
                tc:GetRank() < 13 and tc:GetRank() == tc:GetOriginalRank() and tc.xyz_number ~= 39 and tc.xyz_number ~=
                0 and tc.xyz_number ~= 93) then
            local ocode = tc:GetOriginalCode()
            xyz:SetEntityCode(ocode, code, {tc:GetOriginalSetCard(), xyz:GetOriginalSetCard()}, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
            xyz:SetCardData(CARDDATA_LEVEL, tc:GetOriginalRank() + 1)
            if code == 27 or code == 28 then
                xyz:SetCardData(CARDDATA_ATTACK, tc:GetBaseAttack() + 500)
                xyz:SetCardData(CARDDATA_DEFENSE, tc:GetBaseDefense() + 1000)
            else
                xyz:SetCardData(CARDDATA_ATTACK, tc:GetBaseAttack() + tc:GetOverlayCount() * 300)
            end
        end
    end
end