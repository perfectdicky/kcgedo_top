-- 欧贝利斯克之巨神兵
local s, id = GetID()
function s.initial_effect(c)
    local e1 = aux.AddNormalSummonProcedure(c, true, false, 3, 3)
    local e2 = aux.AddNormalSetProcedure(c, true, false, 3, 3)
    -- 解放3只祭品通召
    -- local e1=Effect.CreateEffect(c)
    -- e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
    -- e1:SetType(EFFECT_TYPE_SINGLE)
    -- e1:SetCode(EFFECT_LIMIT_SUMMON_PROC)
    -- e1:SetCondition(s.ttcon)
    -- e1:SetOperation(s.ttop)
    -- e1:SetValue(SUMMON_TYPE_TRIBUTE)
    -- c:RegisterEffect(e1)
    -- local e2=Effect.CreateEffect(c)
    -- e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
    -- e2:SetType(EFFECT_TYPE_SINGLE)
    -- e2:SetCode(EFFECT_LIMIT_SET_PROC)
    -- e2:SetCondition(s.ttcon)
    -- e2:SetOperation(s.ttop)
    -- c:RegisterEffect(e2)

    -- 特召回合结束送去特召前区域
    local e3 = Effect.CreateEffect(c)
    -- e3:SetDescription(aux.Stringid(819,2))
    e3:SetCategory(CATEGORY_TOGRAVE + CATEGORY_TOHAND + CATEGORY_TODECK + CATEGORY_REMOVE)
    e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_F)
    e3:SetRange(LOCATION_MZONE)
    e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e3:SetCountLimit(1)
    e3:SetCode(EVENT_PHASE + PHASE_END)
    e3:SetCondition(s.tgcon)
    e3:SetTarget(s.tgtg)
    e3:SetOperation(s.tgop)
    c:RegisterEffect(e3)

    local te = Effect.CreateEffect(c)
    te:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    te:SetRange(LOCATION_MZONE)
    te:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    te:SetCountLimit(1)
    te:SetCode(EVENT_PHASE + PHASE_END)
    te:SetOperation(s.reop2)
    c:RegisterEffect(te)

    -- 解放2只怪破坏对方场上怪兽
    local e4 = Effect.CreateEffect(c)
    e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e4:SetDescription(aux.Stringid(10000000, 1))
    e4:SetCategory(CATEGORY_DESTROY)
    e4:SetType(EFFECT_TYPE_IGNITION)
    e4:SetRange(LOCATION_MZONE)
    e4:SetCountLimit(1)
    e4:SetCost(s.descost)
    e4:SetTarget(s.destg)
    e4:SetOperation(s.desop)
    c:RegisterEffect(e4)

    -- 不受陷阱效果以及魔法效果怪兽生效一回合
    local e82 = Effect.CreateEffect(c)
    e82:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e82:SetCode(EVENT_CHAIN_SOLVED)
    e82:SetRange(LOCATION_MZONE)
    e82:SetCondition(s.sdcon2)
    e82:SetOperation(s.sdop2)
    c:RegisterEffect(e82)    
    local e80 = Effect.CreateEffect(c)
    e80:SetType(EFFECT_TYPE_SINGLE)
    e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e80:SetRange(LOCATION_MZONE)
    e80:SetCode(EFFECT_IMMUNE_EFFECT)
    e80:SetValue(s.efilterr)
    c:RegisterEffect(e80)
    local e106 = e80:Clone()
    e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
    e106:SetValue(1)
    c:RegisterEffect(e106)

    -- 解放2只怪提升攻击为无限
    local e6 = Effect.CreateEffect(c)
    e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e6:SetDescription(aux.Stringid(10000010, 0))
    e6:SetCategory(CATEGORY_ATKCHANGE)
    e6:SetType(EFFECT_TYPE_QUICK_O)
    e6:SetCode(EVENT_FREE_CHAIN)
    e6:SetHintTiming(TIMING_BATTLE_PHASE)
    e6:SetRange(LOCATION_MZONE)
    e6:SetCountLimit(1)
    e6:SetCondition(s.atkcon)
    e6:SetCost(s.atkcost)
    e6:SetOperation(s.atkop)
    c:RegisterEffect(e6)

    -- 特召后成为攻击目标
    local e17 = Effect.CreateEffect(c)
    e17:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
    e17:SetRange(LOCATION_MZONE)
    e17:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e17:SetCode(EVENT_ATTACK_ANNOUNCE)
    e17:SetCondition(s.atcon2)
    e17:SetOperation(s.atop)
    c:RegisterEffect(e17)
    local e7 = Effect.CreateEffect(c)
    e7:SetType(EFFECT_TYPE_FIELD)
    e7:SetRange(LOCATION_MZONE)
    e7:SetTargetRange(LOCATION_MZONE, 0)
    e7:SetProperty(EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_CANNOT_DISABLE)
    e7:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
    e7:SetCondition(s.atcon)
    e7:SetTarget(s.atlimit)
    e7:SetValue(Auxiliary.imval1)
    -- c:RegisterEffect(e7)
    -- destroy replace
    local e72 = Effect.CreateEffect(c)
    e72:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
    e72:SetCode(EFFECT_DESTROY_REPLACE)
    e72:SetRange(LOCATION_MZONE)
    e72:SetCondition(s.atcon)
    e72:SetTarget(s.reptg)
    e72:SetValue(s.repval)
    e72:SetOperation(s.repop)
    c:RegisterEffect(e72)

    -- 通常召唤、特殊召唤、反转召唤不会被无效化
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
    e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    c:RegisterEffect(e8)
    local e9 = e8:Clone()
    e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
    c:RegisterEffect(e9)
    local e10 = e9:Clone()
    e10:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
    c:RegisterEffect(e10)

    local e117 = Effect.CreateEffect(c)
    e117:SetType(EFFECT_TYPE_FIELD)
    e117:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
    e117:SetCode(EFFECT_CANNOT_RELEASE)
    e117:SetRange(LOCATION_MZONE)
    e117:SetTargetRange(0, 1)
    e117:SetTarget(s.rellimit)
    c:RegisterEffect(e117)

    -- 不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变成里侧表示、作为特殊召唤素材		
    local e9 = Effect.CreateEffect(c)
    e9:SetType(EFFECT_TYPE_SINGLE)
    e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e9:SetCode(EFFECT_CANNOT_ATTACK)
    e9:SetCondition(s.atcon)
    c:RegisterEffect(e9)

    local e400 = Effect.CreateEffect(c)
    e400:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
    e400:SetCode(EVENT_SUMMON_SUCCESS)
    e400:SetOperation(s.sumsuc)
    c:RegisterEffect(e400)
    local e401 = e400:Clone()
    e401:SetCode(EVENT_SPSUMMON_SUCCESS)
    c:RegisterEffect(e401)
    local e402 = e400:Clone()
    e402:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
    c:RegisterEffect(e402)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.sumsuc(e, tp, eg, ep, ev, re, r, rp)
    Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

function s.ttcon(e, c, minc)
    if c == nil then
        return true
    end
    return minc <= 3 and Duel.CheckTribute(c, 3)
end

function s.ttop(e, tp, eg, ep, ev, re, r, rp, c)
    local g = Duel.SelectTribute(tp, c, 3, 3)
    c:SetMaterial(g)
    Duel.Release(g, REASON_SUMMON + REASON_MATERIAL)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.tgcon(e, tp, eg, ep, ev, re, r, rp)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL
end

function s.tgtg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
        Duel.SetOperationInfo(0, CATEGORY_TOGRAVE, e:GetHandler(), 1, 0, 0)
    elseif e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
        Duel.SetOperationInfo(0, CATEGORY_REMOVE, e:GetHandler(), 1, 0, 0)
    elseif e:GetHandler():GetPreviousLocation() == LOCATION_HAND then
        Duel.SetOperationInfo(0, CATEGORY_TOHAND, e:GetHandler(), 1, 0, 0)
    elseif e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
        Duel.SetOperationInfo(0, CATEGORY_TODECK, e:GetHandler(), 1, 0, 0)
    else
        return
    end
end

function s.tgop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsRelateToEffect(e) and c:IsFaceup() then
        if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
            Duel.SendtoGrave(c, REASON_RULE)
        elseif e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
            Duel.Remove(c, 0, REASON_RULE)
        elseif e:GetHandler():GetPreviousLocation() == LOCATION_HAND then
            Duel.SendtoHand(c, nil, REASON_RULE)
        elseif e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
            Duel.SendtoDeck(c, nil, 2, REASON_RULE)
        else
            Duel.MoveToField(c, tp, c:GetPreviousControler(), c:GetPreviousLocation(), c:GetPreviousPosition(), true)
        end
    end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.descost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.CheckReleaseGroup(tp, nil, 2, e:GetHandler())
    end
    local g = Duel.SelectReleaseGroup(tp, nil, 2, 2, e:GetHandler())
    Duel.Release(g, REASON_COST)
end

function s.filter(c, atk)
    return ((c:IsPosition(POS_FACEUP_ATTACK) and c:GetAttack() < atk) or
               (c:IsPosition(POS_FACEUP_DEFENSE) and c:GetDefense() < atk) or c:IsFacedown())
end

function s.destg(e, tp, eg, ep, ev, re, r, rp, chk)
    local c = e:GetHandler()
    local atk = c:GetAttack()
    if chk == 0 then
        return Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil, atk)
    end
    local g = Duel.GetMatchingGroup(s.filter, tp, 0, LOCATION_MZONE, nil, atk)
    Duel.SetOperationInfo(0, CATEGORY_DESTROY, g, g:GetCount(), 0, 0)
end

function s.desop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local atk = c:GetAttack()
    local g = Duel.GetMatchingGroup(s.filter, tp, 0, LOCATION_MZONE, nil, atk)
    Duel.Hint(HINT_ANIME, tp, aux.Stringid(828, 0))
    Duel.Destroy(g, REASON_RULE)
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e1:SetCode(EFFECT_DIRECT_ATTACK)
    e1:SetRange(LOCATION_MZONE)
    e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
    c:RegisterEffect(e1)
end
-----------------------------------------------------------------------
function s.efilterr(e, te)
    local c = e:GetHandler()
    local tc = te:GetOwner()
    if tc == e:GetOwner() or te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then
        return false
    else
        return te:IsActiveType(TYPE_TRAP) or (te:IsActiveType(TYPE_MONSTER) and
                   (tc:IsLevelBelow(c:GetLevel()) or tc:IsRankBelow(c:GetLevel()) or tc:IsLinkBelow(c:GetLevel())))
    end
end

function s.sdcon2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=1
                    break 
                end
            end
        end
    end
    return eff==1
end
function s.sdop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=te
                    break 
                end
            end
        end
    end
    if eff==0 then return end
    local e83 = Effect.CreateEffect(c)
    e83:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e83:SetRange(LOCATION_MZONE)
    e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
    e83:SetCountLimit(1)
    e83:SetCode(EVENT_PHASE + PHASE_END)
    e83:SetLabelObject(eff)
    e83:SetOperation(s.setop2)
    e83:SetReset(RESET_PHASE + PHASE_END)
    c:RegisterEffect(e83) 
end
function s.setop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local te = e:GetLabelObject()
    if not te or te==0 then return end
    te:Reset()
end   
-------------------------------------------
function s.rellimit(e, c, tp, sumtp)
    return c == e:GetHandler()
end

function s.atkcon(e, tp, eg, ep, ev, re, r, rp)
    local ph = Duel.GetCurrentPhase()
    return (ph >= PHASE_BATTLE_START and ph <= PHASE_BATTLE) and Duel.GetActivityCount(tp, ACTIVITY_ATTACK) == 0
end

function s.atkcost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.CheckReleaseGroup(tp, nil, 2, e:GetHandler())
    end
    local g = Duel.SelectReleaseGroup(tp, nil, 2, 2, e:GetHandler())
    Duel.Release(g, REASON_COST)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_CANNOT_ATTACK)
    e1:SetProperty(EFFECT_FLAG_OATH)
    e1:SetTargetRange(LOCATION_MZONE, 0)
    e1:SetTarget(s.ftarget)
    e1:SetLabel(e:GetHandler():GetFieldID())
    e1:SetReset(RESET_PHASE + PHASE_END)
    Duel.RegisterEffect(e1, tp)
end
function s.ftarget(e, c)
    return e:GetLabel() ~= c:GetFieldID()
end
function s.atkop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsFacedown() or not c:IsRelateToEffect(e) then
        return
    end
    local e1 = Effect.CreateEffect(c)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_SET_ATTACK)
    e1:SetValue(999999)
    e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
    c:RegisterEffect(e1)
    Duel.Hint(HINT_ANIME, tp, aux.Stringid(828, 1))
end
-----------------------------------------------------
function s.atcon(e)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL
end
function s.atcon2(e, tp, eg, ep, ev, re, r, rp)
    local tc = Duel.GetAttacker()
    local tc2 = Duel.GetAttackTarget()
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL and tc:IsFaceup() and
               tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler()
end
function s.atop(e, tp, eg, ep, ev, re, r, rp)
    local tc = Duel.GetAttacker()
    local tc2 = Duel.GetAttackTarget()
    if tc:IsFaceup() and tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler() then
        Duel.ChangeAttackTarget(e:GetHandler())
    end
end

function s.atlimit(e, c)
    return c ~= e:GetHandler()
end

function s.repfilter(c, tc, tp)
    return c:IsControler(tp) and c ~= tc and c:IsLocation(LOCATION_MZONE) and
               (c:IsReason(REASON_EFFECT) or c:IsReason(REASON_RULE)) and not c:IsReason(REASON_REPLACE)
end
function s.reptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return eg:IsExists(s.repfilter, 1, e:GetHandler(), e:GetHandler(), tp) and
                   not e:GetHandler():IsStatus(STATUS_DESTROY_CONFIRMED + STATUS_BATTLE_DESTROYED)
    end
    return Duel.SelectYesNo(tp, aux.Stringid(19333131, 0))
end
function s.repval(e, c)
    return s.repfilter(c, e:GetHandler(), e:GetHandlerPlayer())
end
function s.repop(e, tp, eg, ep, ev, re, r, rp)
    Duel.Destroy(e:GetHandler(), REASON_RULE + REASON_REPLACE)
end

function s.reop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if te:GetOwner() ~= e:GetOwner() and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then
                    local e80 = Effect.CreateEffect(c)
                    e80:SetType(EFFECT_TYPE_SINGLE)
                    e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
                    e80:SetRange(LOCATION_MZONE)
                    e80:SetCode(EFFECT_IMMUNE_EFFECT)
                    e80:SetValue(function(e, te2)
                        return te2 == te
                    end)
                    c:RegisterEffect(e80)
                end
            end
        end
    end
end
