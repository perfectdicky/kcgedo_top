--CNo.2 混沌源數之門-負貳 (KA)
local s, id = GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,2,4,s.ovfilter,aux.Stringid(13704,0))
	c:EnableReviveLimit()

	--cannot destroyed
	  local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e0:SetValue(s.indes)
	c:RegisterEffect(e0)

	--selfdes
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCode(EFFECT_SELF_DESTROY)
	e2:SetCondition(s.descon)
	c:RegisterEffect(e2)

	--Banish and Damage
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	e3:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e3:SetProperty(EFFECT_FLAG_DELAY+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e3:SetCondition(s.rmcon)
	e3:SetTarget(s.target)
	e3:SetOperation(s.rmop)
	c:RegisterEffect(e3)
end 
s.xyz_number=4
s.listed_series = {0x48}

function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end

function s.ovfilter(c)
	return c:IsFaceup() and c:IsCode(13704) 
end
function s.descon(e)
	local c=e:GetHandler()
	return not Duel.IsExistingMatchingCard(s.damfilter,tp,LOCATION_SZONE,LOCATION_SZONE,1,nil)
end
function s.dfilter(c)
	return c:IsFaceup()
end

function s.damfilter(c)
	return c:IsFaceup() and c:IsCode(41418852)
end
function s.filter0(c,e,tp)
	return c:IsSetCard(0x1048) and c:IsSetCard(0x14b) and not c:IsHasEffect(EFFECT_NECRO_VALLEY) 
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
end
function s.filter02(c,e,tp,g)
	return c:GetRank()==11 and c.minxyzct and c.minxyzct==4 and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
			and g:FilterCount(s.filter3,nil,c)==4
end
function s.filter3(c,tc)
	return c:IsSetCard(0x1048) and c:IsSetCard(0x14b) and c:IsCanBeXyzMaterial(tc) 
end
function s.filter(c,e,tp,tc)
	return c:IsSetCard(0x1048) and c:IsSetCard(0x14b) and not c:IsHasEffect(EFFECT_NECRO_VALLEY) and c:IsCanBeXyzMaterial(tc) 
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
end
function s.filter2(c,e,tp)
	return c:GetRank()==11 and c.minxyzct and c.minxyzct==4 and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
			and Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_GRAVE+LOCATION_REMOVED,0,3,nil,e,tp,c)
end
function s.rmcon(e,tp,eg,ep,ev,re,r,rp)
	return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_XYZ)==SUMMON_TYPE_XYZ 
			and Duel.IsExistingMatchingCard(s.damfilter,tp,LOCATION_SZONE,0,1,nil)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(s.filter0,tp,LOCATION_GRAVE+LOCATION_REMOVED,0,3,nil,e,tp,c)
		and Duel.GetLocationCount(tp,LOCATION_MZONE)>2 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,3,0,LOCATION_GRAVE+LOCATION_REMOVED)
end
function s.rmop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local count=0
	local ft1=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if ft1>3 then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local g=Duel.SelectMatchingCard(tp,s.filter0,tp,LOCATION_GRAVE+LOCATION_REMOVED,0,3,3,nil,e,tp)
		if g:GetCount()==3 then
			local tc=g:GetFirst()
			while tc do
				Duel.SpecialSummon(tc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
												tc:CompleteProcedure()
												tc:RegisterFlagEffect(585,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_STANDBY+RESET_OPPO_TURN,0,1)
						tc=g:GetNext()
			end
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e4:SetCode(EVENT_PHASE+PHASE_STANDBY)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCondition(s.spcon)
	e4:SetTarget(s.sptg)
	e4:SetOperation(s.spop)
			e4:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_STANDBY+RESET_OPPO_TURN)
	c:RegisterEffect(e4)
	end end
end
function s.spcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()==1-tp
end
function s.rrfilter(c)
	return c:GetFlagEffect(585)~=0
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	 local c=e:GetHandler()
			 local g=Duel.GetMatchingGroup(s.rrfilter,tp,LOCATION_MZONE,0,nil)
			 g:AddCard(c)
			 if chk==0 then return g:FilterCount(Card.IsLocation,nil,LOCATION_MZONE)==4 and Duel.IsExistingMatchingCard(s.filter02,tp,LOCATION_EXTRA,0,1,nil,e,tp,g) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,0,LOCATION_EXTRA)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp)
	 local c=e:GetHandler()
			 local g=Duel.GetMatchingGroup(s.rrfilter,tp,LOCATION_MZONE,0,nil)
			 g:AddCard(c)
			 Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	 local g2=Duel.SelectMatchingCard(tp,s.filter02,tp,LOCATION_EXTRA,0,1,1,nil,e,tp,g)
			 local sc=g2:GetFirst()
			 local gg=g:Filter(Card.IsLocation,nil,LOCATION_MZONE)
	 local tc2=gg:GetFirst()
	while tc2 do
						local mg=tc2:GetOverlayGroup()
				if mg:GetCount()~=0 then
						 Duel.Overlay(sc,mg)
				end
						tc2=gg:GetNext()
	end
			sc:SetMaterial(gg)
	Duel.Overlay(sc,gg)
	Duel.SpecialSummon(sc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
			sc:CompleteProcedure() 
end