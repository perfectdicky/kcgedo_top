--Sin World
function c95.initial_effect(c)
	--aux.addcode(c,27564031)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	--e1:SetOperation(c95.actop) 
	c:RegisterEffect(e1)

	--search
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(27564031,0))
	e2:SetCategory(CATEGORY_TOHAND+CATEGORY_SEARCH)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetRange(LOCATION_FZONE)
	e2:SetCondition(c95.condition)
	e2:SetTarget(c95.target)
	e2:SetOperation(c95.operation)
	c:RegisterEffect(e2)

	  --cannot attack
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CANNOT_ATTACK)
	  e3:SetRange(LOCATION_FZONE)
	  e3:SetTargetRange(LOCATION_MZONE,0)
	  e3:SetCondition(c95.condition2)
	  e3:SetTarget(c95.target2)
	c:RegisterEffect(e3)
	
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e4:SetRange(LOCATION_FZONE)
	e4:SetCode(EFFECT_IMMUNE_EFFECT)
	e4:SetValue(c95.immu)
	c:RegisterEffect(e4)	
end

function c95.immu(e,te)
   return te:GetOwner()~=e:GetOwner()
end
function c95.fromhandfilter(c)
	return (c:IsCode(10) or c:IsCode(11) or c:IsCode(12) or c:IsCode(12201) or c:IsCode(190)) and c:IsFaceup()
end
function c95.fromhandcon(e,tp,eg,ep,ev,re,r,rp)
	  return Duel.GetTurnPlayer()==e:GetHandler():GetControler()
	  --and Duel.GetFlagEffect(e:GetHandler():GetControler(),5)==0 
	  and not Duel.IsExistingMatchingCard(c95.fromhandfilter,tp,LOCATION_SZONE,LOCATION_SZONE,1,nil)
end
function c95.fromhandop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetHandler()
	if tc then
		--Duel.RegisterFlagEffect(e:GetHandler():GetControler(),5,0,0,0)
		local oppfield=Duel.GetFieldCard(1-tp,LOCATION_SZONE,5)
		local myfield=Duel.GetFieldCard(tp,LOCATION_SZONE,5)
		local g=Duel.GetMatchingGroup(Card.IsSetCard,tp,LOCATION_MZONE,LOCATION_MZONE,nil,0x23)
		if oppfield~=nil and oppfield:IsFaceup() and (oppfield:IsCode(10) or oppfield:IsCode(11) or oppfield:IsCode(12) or oppfield:IsCode(12201) or oppfield:IsCode(190)) then return end
		if myfield~=nil and myfield:IsFaceup() and (myfield:IsCode(10) or myfield:IsCode(11) or myfield:IsCode(12) or myfield:IsCode(12201) or myfield:IsCode(190)) then return end
		if oppfield~=nil then
		Duel.Destroy(oppfield,REASON_RULE)
		if g:GetCount()~=0 then Duel.Destroy(g,REASON_RULE) end
		Duel.BreakEffect() end
		if myfield~=nil then
		Duel.Destroy(myfield,REASON_RULE)
		if g:GetCount()~=0 then Duel.Destroy(g,REASON_RULE) end
		Duel.BreakEffect() end
		Duel.MoveToField(tc,tp,tp,LOCATION_SZONE,POS_FACEUP,true)
		--if tc:IsLocation(LOCATION_SZONE) and tc:IsFaceup() then
		--local te=tc:GetActivateEffect()
		--local tep=tc:GetControler()
	  --local condition=te:GetCondition()
	  --local cost=te:GetCost()
	  --local target=te:GetTarget()
	  --local operation=te:GetOperation()
	  --e:SetProperty(te:GetProperty())
	  --tc:CreateEffectRelation(te) 
	  --if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
	  --if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
	  --if operation then operation(te,tep,eg,ep,ev,re,r,rp) end
	  --tc:ReleaseEffectRelation(te)  
		--Duel.RaiseEvent(tc,EVENT_CHAIN_SOLVED,tc:GetActivateEffect(),0,tp,tp,Duel.GetCurrentChain()) end
	end
end

function c95.actop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetHandler()
	local oppfield=Duel.GetFieldCard(1-tp,LOCATION_SZONE,5)
	if (oppfield~=nil and not (oppfield:IsCode(10) or oppfield:IsCode(11) or oppfield:IsCode(12) or oppfield:IsCode(12201) or oppfield:IsCode(190))) 
	or (oppfield~=nil and (oppfield:IsCode(10) or oppfield:IsCode(11) or oppfield:IsCode(12) or oppfield:IsCode(12201) or oppfield:IsCode(190)) and oppfield:IsFacedown()) then
	Duel.Destroy(oppfield,REASON_RULE) end
end

function c95.condition(e,tp,eg,ep,ev,re,r,rp)
	return tp==Duel.GetTurnPlayer() and Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)>0
		and Duel.GetDrawCount(tp)>0
end
function c95.filter(c)
	return c:IsSetCard(0x23) and c:IsAbleToHand()
end
function c95.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c95.filter,tp,LOCATION_DECK,0,1,nil) end
	local dt=Duel.GetDrawCount(tp)
	if dt~=0 then
		_replace_count=0
		_replace_max=dt
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1,0)
		e1:SetReset(RESET_PHASE+PHASE_DRAW)
		e1:SetValue(0)
		Duel.RegisterEffect(e1,tp)
	end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,0,LOCATION_DECK)
end
function c95.operation(e,tp,eg,ep,ev,re,r,rp)
	_replace_count=_replace_count+1
	if _replace_count>_replace_max or not e:GetHandler():IsRelateToEffect(e) then return end
	local g=Duel.GetMatchingGroup(Card.IsSetCard,tp,LOCATION_DECK,0,nil,0x23)
	if g:GetCount()>=1 then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
		local sg=g:RandomSelect(tp,1)
			local sg1=sg:GetFirst()
			if sg1:IsAbleToHand() then Duel.SendtoHand(sg1,nil,REASON_EFFECT) else Duel.SendtoGrave(sg1,REASON_EFFECT) end
		Duel.ConfirmCards(1-tp,sg)
		Duel.ShuffleDeck(tp)
	end
end

function c95.filter2(c)
	return c:IsSetCard(0x23) and c:IsFaceup()
end
function c95.condition2(e)
	return Duel.IsExistingMatchingCard(c95.filter2,e:GetHandlerPlayer(),LOCATION_MZONE,0,1,nil)
end
function c95.target2(e,c)
	  return not c:IsSetCard(0x23)
end
