--奥西里斯之天空龙
function c580.initial_effect(c)
	c:EnableReviveLimit()

	--cannot special summon
	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SPSUMMON_CONDITION)
	c:RegisterEffect(e0)

	--特殊召唤方式
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(c580.spcon)
	e1:SetOperation(c580.spop)
	c:RegisterEffect(e1)
	
	--下降对手怪兽攻击、破坏
	local e6=Effect.CreateEffect(c)
	e6:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_CANNOT_DISABLE)
	e6:SetDescription(aux.Stringid(10000020,1))
	e6:SetCategory(CATEGORY_ATKCHANGE+CATEGORY_DESTROY)
	e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCountLimit(10)
	e6:SetCode(EVENT_SUMMON_SUCCESS)
	e6:SetCondition(c580.atkcon)
	e6:SetTarget(c580.atktg)
	e6:SetOperation(c580.atkop)
	c:RegisterEffect(e6)
	local e7=e6:Clone()
	e7:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e7)
	local e8=e7:Clone()
	e8:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
	c:RegisterEffect(e8)
	
	--下降对手怪兽防御、破坏
	local e9=Effect.CreateEffect(c)
	e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e9:SetDescription(aux.Stringid(10000020,2))
	e9:SetCategory(CATEGORY_DEFCHANGE+CATEGORY_DESTROY)
	e9:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e9:SetRange(LOCATION_MZONE)
	e9:SetCountLimit(10)
	e9:SetCode(EVENT_SUMMON_SUCCESS)
	e9:SetCondition(c580.defcon)
	e9:SetTarget(c580.deftg)
	e9:SetOperation(c580.defop)
	--c:RegisterEffect(e9)
	local e10=e9:Clone()
	e10:SetCode(EVENT_SPSUMMON_SUCCESS)
	--c:RegisterEffect(e10)
	local e98=e9:Clone()
	e98:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
	--c:RegisterEffect(e98)
	
	--不受陷阱效果以及魔法效果怪兽生效一回合
	local e82=Effect.CreateEffect(c)
	e82:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e82:SetCode(EVENT_CHAINING)
	e82:SetRange(LOCATION_ONFIELD)
	e82:SetCondition(c580.sdcon2)
	e82:SetOperation(c580.sdop2)
	--c:RegisterEffect(e82)
	local e80=Effect.CreateEffect(c)
	e80:SetType(EFFECT_TYPE_SINGLE)
	e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e80:SetRange(LOCATION_ONFIELD)
	e80:SetCode(EFFECT_IMMUNE_EFFECT)
	e80:SetValue(c580.efilterr)
	--c:RegisterEffect(e80)
	local e83=Effect.CreateEffect(c)
	e83:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e83:SetRange(LOCATION_ONFIELD)
	e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_CANNOT_DISABLE)
	e83:SetCountLimit(1)
	e83:SetCode(EVENT_PHASE+PHASE_END)
	e83:SetOperation(c580.setop)
	--c:RegisterEffect(e83)   
	
	--特召后成为攻击目标
	local e17=Effect.CreateEffect(c)
	e17:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e17:SetRange(LOCATION_MZONE)
	e17:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e17:SetCode(EVENT_ATTACK_ANNOUNCE)
	e17:SetCondition(c580.atcon2)
	e17:SetOperation(c580.atop)
	c:RegisterEffect(e17)   
	local e12=Effect.CreateEffect(c)
	e12:SetType(EFFECT_TYPE_FIELD)
	e12:SetRange(LOCATION_ONFIELD)
	e12:SetTargetRange(LOCATION_MZONE,0)
	e12:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_CANNOT_DISABLE)
	e12:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
	e12:SetCondition(c580.atcon)
	e12:SetTarget(c580.atlimit)
	e12:SetValue(Auxiliary.imval1)
	--c:RegisterEffect(e12)
	--destroy replace
	local e72=Effect.CreateEffect(c)
	e72:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e72:SetCode(EFFECT_DESTROY_REPLACE)
	e72:SetRange(LOCATION_ONFIELD)
	e72:SetCondition(c580.atcon)
	e72:SetTarget(c580.reptg)
	e72:SetValue(c580.repval)
	e72:SetOperation(c580.repop)
	c:RegisterEffect(e72)
	
	--通常召唤、特殊召唤、反转召唤不会被无效化
	local e13=Effect.CreateEffect(c)
	e13:SetType(EFFECT_TYPE_SINGLE)
	e13:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
	e13:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e13)
	local e14=e13:Clone()
	e14:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	c:RegisterEffect(e14)
	local e15=e14:Clone()
	e15:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
	c:RegisterEffect(e15)

	--同时当作创造神族怪兽使用
	local e114=Effect.CreateEffect(c)
	e114:SetType(EFFECT_TYPE_SINGLE)
	e114:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e114:SetCode(EFFECT_ADD_RACE)
	e114:SetValue(RACE_DRAGON)
	c:RegisterEffect(e114)
	
	--不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变成里侧表示、作为特殊召唤素材
	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_ONFIELD)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e101)
	local e102=e101:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103=e102:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104=e103:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e105=e104:Clone()
	e105:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e105)
	local e106=e105:Clone()
	e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e106)
	local e107=e106:Clone()
	e107:SetCode(EFFECT_CANNOT_TURN_SET)
	c:RegisterEffect(e107)
	local e108=e107:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109=e108:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110=e109:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e111=e109:Clone()
	e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e111)

	  local e19=Effect.CreateEffect(c)
	e19:SetType(EFFECT_TYPE_SINGLE) 
	e19:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e19:SetCode(EFFECT_CANNOT_ATTACK) 
	e19:SetCondition(c580.atcon)
	c:RegisterEffect(e19)

	local e21=Effect.CreateEffect(c)
	e21:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e21:SetRange(LOCATION_ONFIELD)
	e21:SetCode(EFFECT_IMMUNE_EFFECT)
	e21:SetType(EFFECT_TYPE_SINGLE)
	  e21:SetValue(c580.eefilter)
	e21:SetCondition(c580.ocon)
	c:RegisterEffect(e21)
	local e22=Effect.CreateEffect(c)
	e22:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e22:SetRange(LOCATION_ONFIELD)
	e22:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e22:SetCountLimit(1)
	e22:SetCode(EVENT_PHASE+PHASE_END)
	e22:SetCondition(c580.ocon2)
	e22:SetOperation(c580.ermop)
	c:RegisterEffect(e22) 

	local e400=Effect.CreateEffect(c)
	e400:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e400:SetCode(EVENT_SUMMON_SUCCESS)
	e400:SetOperation(c580.sumsuc)
	c:RegisterEffect(e400)
end

function c580.sumsuc(e,tp,eg,ep,ev,re,r,rp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

function c580.ofilter2(c)
	return c:IsFaceup() and c:IsSetCard(0x900) and c:IsType(TYPE_FIELD)
end
function c580.ocon(e)
	return Duel.IsExistingMatchingCard(c580.ofilter2,e:GetHandlerPlayer(),LOCATION_SZONE,LOCATION_SZONE,1,nil)
end
function c580.eefilter(e,te)
	return te:GetOwner()~=e:GetOwner()
end

function c580.spmfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsReleasableByEffect()
end--
function c580.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	local a=Duel.GetMatchingGroupCount(c580.spmfilter,tp,LOCATION_MZONE,0,nil)
	return a>=3 and (Duel.GetLocationCount(tp,LOCATION_MZONE)>=-3)
end
function c580.spop(e,tp,eg,ep,ev,re,r,rp,c)
	if Duel.GetLocationCount(tp, LOCATION_MZONE)<-3 then return end	
	local g=Duel.SelectMatchingCard(c:GetControler(),c580.spmfilter,c:GetControler(),LOCATION_ONFIELD,0,3,3,nil)
	Duel.Release(g,REASON_COST)
end

function c580.ocon2(e)
	return not Duel.IsExistingMatchingCard(c580.ofilter2,e:GetHandlerPlayer(),LOCATION_SZONE,LOCATION_SZONE,1,nil)
end
function c580.ermop(e,tp,eg,ep,ev,re,r,rp,c)
			Duel.Remove(e:GetHandler(),POS_FACEUP,REASON_RULE)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c580.tgcon(e,tp,eg,ep,ev,re,r,rp)
	return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_SPECIAL)==SUMMON_TYPE_SPECIAL
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c580.atkfilter(c,tp)
	return c:IsControler(tp) and c:IsPosition(POS_FACEUP) 
	--and (not e or c:IsRelateToEffect(e)) 
--and not c:IsRace(RACE_CREATORGOD)
end

function c580.atkcon(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(c580.atkfilter,1,nil,1-tp)
end

function c580.atktg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return eg:IsContains(chkc) and s.atkfilter(chkc,tp) end
	if chk==0 then return e:GetHandler():IsRelateToEffect(e) end
	Duel.SetTargetCard(eg:Filter(s.atkfilter,nil,1-tp))
end

function c580.atkop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	local dg=Group.CreateGroup()
	local c=e:GetHandler()
	if g:GetCount()>0 then
	local tc=g:GetFirst()
	while tc do
		local preatk=tc:GetAttack()
		local predef=tc:GetDefense()
		if tc:GetPosition()==POS_FACEUP_ATTACK and preatk>0 then
		local e1=Effect.CreateEffect(c)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(-3000)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1)
		if tc:GetAttack()==0 then dg:AddCard(tc) end end

		if tc:GetPosition()==POS_FACEUP_DEFENSE and predef>0 then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetCode(EFFECT_UPDATE_DEFENSE)
		e1:SetValue(-3000)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1)
		if tc:GetDefense()==0 then dg:AddCard(tc) end end
		tc=g:GetNext()
	end
	Duel.Destroy(dg,REASON_RULE) end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c580.deffilter(c,e,tp)
	return c:IsControler(tp) and c:IsPosition(POS_FACEUP_DEFENSE) and (not e or c:IsRelateToEffect(e)) 
--and not c:IsRace(RACE_CREATORGOD)
end

function c580.defcon(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(c580.deffilter,1,nil,nil,1-tp)
end

function c580.deftg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsRelateToEffect(e) end
	Duel.SetTargetCard(eg)
			local dg=eg
			local g=dg:Filter(Card.IsDefenseAbove,nil,3000)
	dg:Sub(g)   
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,dg,dg:GetCount(),0,0)
end

function c580.defop(e,tp,eg,ep,ev,re,r,rp)
			local dg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
			local g=dg:Filter(Card.IsDefenseBelow,nil,3000)
	dg:Sub(g)
	local c=e:GetHandler()
	if dg:GetCount()>0 then
	local tc=dg:GetFirst()
	while tc do
		local preatk=tc:GetDefense()
			if preatk>0 then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_DEFENSE)
		e1:SetValue(-3000)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1)
	 end
		tc=dg:GetNext()
	end
	Duel.Destroy(g,REASON_RULE) end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c580.efilterr(e,te)   
	local c=e:GetHandler()  
	  local tc=te:GetOwner()
	return te:IsActiveType(TYPE_TRAP)  
	  or ((te:IsActiveType(TYPE_MONSTER) or te:IsActiveType(TYPE_SPELL)) and tc~=e:GetOwner() )
			--  and (( te:GetType()==EFFECT_TYPE_FIELD or te:GetType()==EFFECT_TYPE_EQUIP)  
			--  and Duel.GetTurnCount()-tc:GetTurnID()>=1 ))
			 --and tc:GetFlagEffect(100000220)~=0 and tc:GetFlagEffectLabel(100000220)==c:GetFieldID())
end

function c580.sdcon2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()  
	local tc=re:GetHandler()	
	return re:GetOwner()~=e:GetOwner() and (tc:GetFlagEffect(100000220)==0 or (c:GetFlagEffect(100000220)~=0 and tc:GetFlagEffectLabel(100000220)~=c:GetFieldID()))
end
function c580.sdop2(e,tp,eg,ep,ev,re,r,rp)   
	local c=e:GetHandler()
	local tc=re:GetHandler()	
			--tc:RegisterFlagEffect(10000022,RESET_EVENT+0x1fe00000,0,1) 
	  if re:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then tc:RegisterFlagEffect(8888,0,0,0) end
	local e83=Effect.CreateEffect(c)
	e83:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e83:SetRange(LOCATION_MZONE)
	e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_CANNOT_DISABLE)
	e83:SetCountLimit(1)
	e83:SetCode(EVENT_PHASE+PHASE_END)
	  e83:SetLabelObject(re)
	e83:SetOperation(c580.setop2)
	--c:RegisterEffect(e83) 
end
function c580.setop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local te=e:GetLabelObject()
	  local tc=te:GetHandler() 
	  if tc:GetFlagEffect(8888)~=0 then return end
	  if tc:GetFlagEffect(10000022)==0 then return end
	tc:RegisterFlagEffect(100000220,RESET_EVENT+0x1fe00000,0,1) 
	  tc:SetFlagEffectLabel(100000220,c:GetFieldID())
end
function c580.setop(e,tp,eg,ep,ev,re,r,rp,c)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(nil,tp,0xff,0xff,c)
			local tc=g:GetFirst() 
			while tc do
			if tc:GetOriginalCode()~=c:GetOriginalCode() and tc:GetFlagEffect(8888)==0 then
			c:ResetEffect(tc:GetOriginalCode(),RESET_CARD) end
			tc=g:GetNext() end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c580.atcon(e)
	return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_SPECIAL)==SUMMON_TYPE_SPECIAL and e:GetHandler():GetTurnID()==Duel.GetTurnCount()
end
function c580.atcon2(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetAttacker()
	local tc2=Duel.GetAttackTarget()
	return bit.band(e:GetHandler():GetSummonType(),SUMMON_TYPE_SPECIAL)==SUMMON_TYPE_SPECIAL and tc:IsFaceup() and tc:IsControler(1-tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2~=e:GetHandler()
end
function c580.atop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetAttacker()
	local tc2=Duel.GetAttackTarget()	
	if tc:IsFaceup() and tc:IsControler(1-tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2~=e:GetHandler() then 
		Duel.ChangeAttackTarget(e:GetHandler())
	end
end

function c580.atlimit(e,c)
	return c~=e:GetHandler()
end

function c580.repfilter(c,tc,tp)
	return c:IsControler(tp) and c~=tc and c:IsLocation(LOCATION_MZONE)  and (c:IsReason(REASON_EFFECT)  or c:IsReason(REASON_RULE) )  and not c:IsReason(REASON_REPLACE) and not c:IsStatus(STATUS_DESTROY_CONFIRMED+STATUS_BATTLE_DESTROYED)
end
function c580.reptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:IsExists(c580.repfilter,1,e:GetHandler(),e:GetHandler(),tp) and not e:GetHandler():IsStatus(STATUS_DESTROY_CONFIRMED+STATUS_BATTLE_DESTROYED) end
	return Duel.SelectYesNo(tp,aux.Stringid(19333131,0))
end
function c580.repval(e,c)
	return c580.repfilter(c,e:GetHandler(),e:GetHandlerPlayer())
end
function c580.repop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Destroy(e:GetHandler(),REASON_RULE+REASON_REPLACE)
end