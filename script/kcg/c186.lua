--ダークネス
function c186.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY)		
	e1:SetProperty(EFFECT_CANNOT_INACTIVATE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetOperation(c186.activate)
	c:RegisterEffect(e1)

	--Cannot look
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetRange(LOCATION_FZONE)
	e2:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e2:SetTargetRange(LOCATION_SZONE,0)
	e2:SetCode(EFFECT_DARKNESS_HIDE)
	e2:SetValue(function(e,c) return c:GetFlagEffect(186)~=0 end)
	c:RegisterEffect(e2)	

	--RandomOpen
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	-- e2:SetDescription(aux.Stringid(100000590,0))
	-- e2:SetType(EFFECT_TYPE_QUICK_O)
	-- e2:SetCode(EVENT_FREE_CHAIN)
	-- e2:SetCountLimit(1)
	-- e2:SetRange(LOCATION_FZONE)
	-- e2:SetHintTiming(0,0x1e0)
	-- e2:SetLabelObject(e1)
	-- e2:SetCondition(c186.condition)
	-- e2:SetOperation(c186.operation)
	-- c:RegisterEffect(e2)

	--reset
	local e3=Effect.CreateEffect(c)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e3:SetCode(EVENT_PHASE+PHASE_END)
	e3:SetRange(LOCATION_FZONE)
	e3:SetCountLimit(1)
	e3:SetTarget(c186.target2)
	e3:SetOperation(c186.activate2)
	c:RegisterEffect(e3)

	--Destroy2
	local e4=Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e4:SetType(EFFECT_TYPE_TRIGGER_F+EFFECT_TYPE_FIELD)
	e4:SetCategory(CATEGORY_DESTROY)	
	e4:SetRange(LOCATION_FZONE)
	e4:SetCode(EVENT_LEAVE_FIELD)
	e4:SetCondition(c186.descon2)
	e4:SetOperation(c186.desop2)
	c:RegisterEffect(e4)
	
	-- local e5=Effect.CreateEffect(c)
	-- e5:SetType(EFFECT_TYPE_FIELD)
	-- e5:SetCode(EFFECT_CANNOT_TRIGGER)
	-- e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_SET_AVAILABLE)
	-- e5:SetRange(LOCATION_FZONE)
	-- e5:SetTargetRange(LOCATION_SZONE,0)
	-- e5:SetTarget(function(e,c) return c:GetFlagEffect(186)~=0 end)
	-- c:RegisterEffect(e5)

	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_FIELD)
	e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_SET_AVAILABLE)
	e8:SetRange(LOCATION_FZONE)
	e8:SetTargetRange(LOCATION_SZONE,0)
	e8:SetCode(EFFECT_IMMUNE_EFFECT)
	e8:SetTarget(c186.eest)
	e8:SetValue(c186.eefilter)
	c:RegisterEffect(e8)	
end

function c186.filter(c,code)
	return c:IsCode(code) and c:IsSSetable()
end
function c186.filter22(c,code)
	return c:IsCode(code) and c:IsFaceup() and not c:IsStaus(STATUS_DISABLED)
end
function c186.defilter(c)
	return c:IsType(TYPE_SPELL+TYPE_TRAP) 
end
function c186.tffilter(c)
	return c:IsSSetable() 
	and c:IsCode(511310101,511310102,511310103,511310104,511310105) and c:IsSSetable()
end
function c186.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	e:SetLabel(Duel.GetTurnCount())
	local g=Duel.GetMatchingGroup(c186.defilter,tp,LOCATION_ONFIELD,0,c)
	if g:GetCount()~=0 then Duel.Destroy(g,REASON_EFFECT) Duel.BreakEffect() end
	local ft=Duel.GetLocationCount(tp,LOCATION_SZONE)
	if ft<5 then return end 
	if ft>5 then ft=5 end	
	--darkness
	if Duel.GetMatchingGroupCount(c186.filter,tp,0x13,0,nil,511310101)==0 then return end 
	if Duel.GetMatchingGroupCount(c186.filter,tp,0x13,0,nil,511310102)==0 then return end 
	if Duel.GetMatchingGroupCount(c186.filter,tp,0x13,0,nil,511310103)==0 then return end 
	if Duel.GetMatchingGroupCount(c186.filter,tp,0x13,0,nil,511310104)==0 then return end 
	if Duel.GetMatchingGroupCount(c186.filter,tp,0x13,0,nil,511310105)==0 then return end 
	local sg=Duel.GetMatchingGroup(c186.tffilter,tp,0x13,0,nil)
	Duel.Hint(HINT_SELECTMSG,tp,HINT_SET)
	local g1=sg:Select(tp,1,1,nil)
	sg:Remove(Card.IsCode,nil,g1:GetFirst():GetCode())
	ft=ft-1
	local g2=Group.CreateGroup()
	while ft>0 do
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SET)
		g2=sg:Select(tp,1,1,nil)
		g1:Merge(g2)
		sg:Remove(Card.IsCode,nil,g2:GetFirst():GetCode())
		ft=ft-1
	end
	Duel.SSet(tp,g1,tp,false)
	Duel.ShuffleSetCard(g1)
	while g1:GetCount()>0 do
	  local tc=g1:GetFirst()
	  g1:RemoveCard(tc)
	  tc:RegisterFlagEffect(186,RESET_EVENT+RESETS_STANDARD-RESET_TURN_SET,0,1)
	end
end

-- function c186.condition(e,tp,eg,ep,ev,re,r,rp)
-- 	local actturn=e:GetLabelObject():GetLabel()
-- 	return Duel.GetTurnCount()~=actturn
-- end
-- function c186.operation(e,tp,eg,ep,ev,re,r,rp)
-- 	local c=e:GetHandler()
-- 	if not e:GetHandler():IsLocation(LOCATION_SZONE) or e:GetHandler():IsFacedown() then return end
-- 	local actturn=e:GetLabelObject():GetLabel()
-- 	local g=Duel.GetMatchingGroup(Card.IsFacedown,tp,LOCATION_SZONE,0,nil)
-- 	if g:GetCount()>0 then
-- 		local sg=Group.CreateGroup()
-- 		if e:GetHandler():IsHasEffect(100000703) 
-- 		then sg=g:Select(tp,1,1,nil)
-- 		else sg=g:RandomSelect(tp,1) end
-- 		local tc=sg:GetFirst()
-- 		if Duel.ChangePosition(tc,POS_FACEUP)<1 then return end
-- 		tc:RegisterFlagEffect(100000594,RESET_EVENT+0x1fe0000+RESET_CHAIN,0,1)
-- 		local te=tc:GetActivateEffect()
-- 		if not (te==nil or tc:CheckActivateEffect(false,false,false)==nil) then
-- 		local tep=tc:GetControler()
-- 		local condition=te:GetCondition()
-- 		local cost=te:GetCost()
-- 		local target=te:GetTarget()
-- 		local operation=te:GetOperation()
-- 		e:SetCategory(te:GetCategory())
-- 		e:SetProperty(te:GetProperty())
-- 		Duel.ClearTargetCard()
-- 		if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
-- 		if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
-- 		local gg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
-- 		if gg then
-- 		  local etc=gg:GetFirst()
-- 		  while etc do
-- 			etc:CreateEffectRelation(te)
-- 			etc=gg:GetNext()
-- 		  end
-- 		end
-- 		if operation then operation(te,tep,eg,ep,ev,re,r,rp) end
-- 		tc:ReleaseEffectRelation(te)
-- 		if gg then  
-- 		  local etc=gg:GetFirst()												 
-- 		  while etc do
-- 			etc:ReleaseEffectRelation(te)
-- 			etc=gg:GetNext()
-- 		  end
-- 		end 
-- 		Duel.RaiseEvent(tc,EVENT_CHAIN_SOLVED,tc:GetActivateEffect(),0,tp,tp,Duel.GetCurrentChain())
-- 	    end
-- 		if not tc:IsType(TYPE_CONTINUOUS) and not tc:IsHasEffect(EFFECT_REMAIN_FIELD) then Duel.SendtoGrave(tc,REASON_RULE) end

-- 	if c:IsHasEffect(740) and Duel.SelectYesNo(tp,aux.Stringid(742,0)) then
-- 	local g=Duel.GetMatchingGroup(Card.IsType,tp,LOCATION_ONFIELD,0,e:GetHandler(),TYPE_TRAP)
-- 	if g:GetCount()>0 and Duel.ChangePosition(g,POS_FACEDOWN)>0 then
-- 		Duel.ShuffleSetCard(g)
-- 		local sg=Group.CreateGroup()
-- 		if e:GetHandler():IsHasEffect(100000703) 
-- 		then sg=g:Select(tp,1,1,nil)
-- 		else sg=g:RandomSelect(tp,1) end
-- 		local tc=sg:GetFirst()
-- 		if Duel.ChangePosition(tc,POS_FACEUP)<1 then return end
-- 		tc:RegisterFlagEffect(100000594,RESET_EVENT+0x1fe0000+RESET_CHAIN,0,1)
-- 		local te=tc:GetActivateEffect()
-- 		if te==nil or tc:CheckActivateEffect(false,false,false)==nil then return end
-- 		local tep=tc:GetControler()
-- 		local condition=te:GetCondition()
-- 		local cost=te:GetCost()
-- 		local target=te:GetTarget()
-- 		local operation=te:GetOperation()
-- 		e:SetProperty(te:GetProperty())
-- 		if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
-- 		Duel.ClearTargetCard()
-- 		if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
-- 		tc:CreateEffectRelation(te)
-- 		local gg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
-- 		if gg then
-- 		  local etc=gg:GetFirst()
-- 		  while etc do
-- 			etc:CreateEffectRelation(te)
-- 			etc=gg:GetNext()
-- 		  end
-- 		end
-- 		if operation then operation(te,tep,eg,ep,ev,re,r,rp) end
-- 		tc:ReleaseEffectRelation(te)
-- 		if gg then  
-- 		  local etc=gg:GetFirst()												 
-- 		  while etc do
-- 			etc:ReleaseEffectRelation(te)
-- 			etc=gg:GetNext()
-- 		  end
-- 		end 
-- 		Duel.RaiseEvent(tc,EVENT_CHAIN_SOLVED,tc:GetActivateEffect(),0,tp,tp,Duel.GetCurrentChain())
-- 		if not tc:IsType(TYPE_CONTINUOUS) and not tc:IsHasEffect(EFFECT_REMAIN_FIELD) then Duel.SendtoGrave(tc,REASON_RULE) end
-- 	end
-- 	end
-- 	end
-- end

function c186.filterset(c)
	return c:IsType(TYPE_TRAP) and c:IsFaceup()
end
function c186.target2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c186.filterset,tp,LOCATION_ONFIELD,0,1,e:GetHandler()) end
end
function c186.activate2(e,tp,eg,ep,ev,re,r,rp)
	if not e:GetHandler():IsRelateToEffect(e) then return end
	local g=Duel.GetMatchingGroup(c186.filterset,tp,LOCATION_ONFIELD,0,nil)
	if #g>0 then
		Duel.ChangePosition(g,POS_FACEDOWN)
		Duel.RaiseEvent(g,EVENT_SSET,e,REASON_EFFECT,tp,tp,0)
		Duel.ShuffleSetCard(Duel.GetFieldGroup(tp,LOCATION_SZONE,0):Filter(function(c) return c:GetSequence()<5 end,nil))
		g:ForEach(function(c) 
			c:SetStatus(STATUS_SET_TURN,true)
		end)
	end	
end

function c186.leavefilter(c,e,tp)
	return c:IsPreviousLocation(LOCATION_ONFIELD) and c:IsType(TYPE_SPELL+TYPE_TRAP)
	and c:GetPreviousControler()==tp 
end
function c186.descon2(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(c186.leavefilter,1,nil,e,tp) 
end
function c186.desop2(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c186.defilter,tp,LOCATION_ONFIELD,0,nil)
	Duel.Destroy(g,REASON_EFFECT)
end

function c186.eest(e,c)
	return c:IsType(TYPE_SPELL) or c:IsType(TYPE_TRAP)
end
function c186.eefilter(e,te)
	return not te:GetOwner():IsSetCard(0x316)
	  and not te:GetOwner():IsCode(511310104,511310105)
end
