--クロス·ソウル
function c26.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(13706,14))
	  e1:SetType(EFFECT_TYPE_ACTIVATE)
	  e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetHintTiming(0,TIMING_END_PHASE)
	e1:SetTarget(c26.target)
	e1:SetOperation(c26.activate)  
	c:RegisterEffect(e1)
end

function c26.filter(c)
	return not (c:IsHasEffect(EFFECT_UNRELEASABLE_SUM) or c:IsHasEffect(EFFECT_UNRELEASABLE_NONSUM)) and not c:IsStatus(STATUS_BATTLE_DESTROYED)
end
function c26.filter1(c,e)
	return (c:IsSummonable(false,nil) or c:IsMSetable(false,nil)) and c:IsSummonableCard()
end
function c26.target(e,tp,eg,ep,ev,re,r,rp,chk)
	  local g=Duel.GetMatchingGroup(c26.filter,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	  if chk==0 then return g:GetCount()>0 end
end
function c26.activate(e,tp,eg,ep,ev,re,r,rp)
	  local c=e:GetHandler()
	  local g=Duel.GetMatchingGroup(c26.filter,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	  if g:GetCount()==0 then return end
	  local tc=g:GetFirst()
	  while tc do
	  if tc then
	  local e1=Effect.CreateEffect(c)
	  e1:SetType(EFFECT_TYPE_SINGLE)
	  e1:SetCode(EFFECT_EXTRA_RELEASE)
	  e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	  tc:RegisterEffect(e1)
	  end
	  tc=g:GetNext() end
	if Duel.IsPlayerCanSummon(tp) and Duel.IsExistingMatchingCard(c26.filter1,tp,LOCATION_HAND,0,1,nil,e) and Duel.SelectYesNo(tp,1) then
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SUMMON)
	local sg=Duel.SelectMatchingCard(tp,c26.filter1,tp,LOCATION_HAND,0,1,1,nil,e)
	local sc=sg:GetFirst()
	local s2=sc:IsSummonable(true,nil)
	local s3=sc:IsMSetable(true,nil)
	if not s2 or Duel.SelectOption(tp,1151,1153)==1 then
	Duel.MSet(tp,sc,false,nil)
	else
	Duel.Summon(tp,sc,false,nil)	
	end end
end
