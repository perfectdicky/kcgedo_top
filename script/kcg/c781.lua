--ゴッドアイズ・ファントム・ドラゴン
function c781.initial_effect(c)
	c:EnableReviveLimit()
	Pendulum.AddProcedure(c)
	--chain attack
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e1:SetCode(EVENT_DAMAGE_STEP_END)
	e1:SetCountLimit(1)
	e1:SetRange(LOCATION_PZONE)
	e1:SetCondition(c781.atcon)
	e1:SetCost(c781.atcost)
	e1:SetOperation(c781.atop)
	c:RegisterEffect(e1)

	--spsummon condition
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e2:SetCode(EFFECT_SPSUMMON_CONDITION)
	e2:SetValue(c781.penlimit)
	c:RegisterEffect(e2)

	--special summon
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(70335319,0))
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_SPSUMMON_PROC)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e3:SetRange(LOCATION_HAND)
	--e3:SetCountLimit(1,70335319)
	e3:SetCondition(c781.hspcon)
	e3:SetOperation(c781.hspop)
	e3:SetValue(SUMMON_TYPE_SPECIAL+781)
	c:RegisterEffect(e3)
	--local e4=e3:Clone()
	--e4:SetRange(LOCATION_EXTRA)
	--c:RegisterEffect(e4)

	--attack up
	local e5=Effect.CreateEffect(c)
	e5:SetCategory(CATEGORY_ATKCHANGE)
	e5:SetType(EFFECT_TYPE_TRIGGER_O+EFFECT_TYPE_SINGLE)
	e5:SetCode(EVENT_ATTACK_ANNOUNCE)
	--e5:SetCountLimit(1)
	e5:SetLabelObject(e3)
	e5:SetCondition(c781.atkcon)
	e5:SetOperation(c781.atkop)
	c:RegisterEffect(e5)

	--negate
	local e6=Effect.CreateEffect(c)
	e6:SetDescription(aux.Stringid(70335319,1))
	e6:SetCategory(CATEGORY_NEGATE)
	e6:SetType(EFFECT_TYPE_QUICK_O)
	e6:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e6:SetCode(EVENT_CHAINING)
	e6:SetRange(LOCATION_MZONE)
	--e6:SetCountLimit(1)
	e6:SetCondition(c781.negcon)
	e6:SetCost(c781.negcost)
	e6:SetTarget(c781.negtg)
	e6:SetOperation(c781.negop)
	c:RegisterEffect(e6)
	--
	local e7=Effect.CreateEffect(c)
	e7:SetCategory(CATEGORY_ATKCHANGE)
	e7:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e7:SetCode(EVENT_ATTACK_DISABLED)
	e7:SetRange(LOCATION_MZONE)
	e7:SetProperty(EFFECT_FLAG_DELAY+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP)
	e7:SetCondition(c781.atknegcon)
	e7:SetOperation(c781.operation)
	c:RegisterEffect(e7)

	if not c781.global_check then
		c781.global_check=true
		c781[0]=0
		c781[1]=0
		local ge1=Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_ATTACK_ANNOUNCE)
		ge1:SetOperation(c781.check)
		Duel.RegisterEffect(ge1,0)
		local ge2=Effect.CreateEffect(c)
		ge2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge2:SetCode(EVENT_PHASE_START+PHASE_DRAW)
		ge2:SetOperation(c781.clear)
		Duel.RegisterEffect(ge2,0)
	end
end

function c781.penlimit(e,se,sp,st)
	return st&SUMMON_TYPE_PENDULUM==SUMMON_TYPE_PENDULUM and not e:GetHandler():IsLocation(LOCATION_HAND)
end

function c781.check(e,tp,eg,ep,ev,re,r,rp)
	local at=Duel.GetAttacker()
	if c781[2] and c781[2]~=at then
		c781[at:GetControler()]=1
		return
	end
	c781[2]=at
end
function c781.clear(e,tp,eg,ep,ev,re,r,rp)
	c781[0]=0
	c781[1]=0
	c781[2]=nil
end

function c781.atcon(e,tp,eg,ep,ev,re,r,rp)
	local at=Duel.GetAttacker()
	return at:IsControler(tp) and Duel.GetAttackTarget()~=nil
		and at:IsRace(RACE_DRAGON) and at:IsType(TYPE_PENDULUM) and at:CanChainAttack(0)
end
function c781.atcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return c781[tp]==0 end
	local at=Duel.GetAttacker()
	at:RegisterFlagEffect(70335319,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END,0,1)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_OATH)
	e1:SetTargetRange(LOCATION_MZONE,0)
	e1:SetTarget(c781.atktg)
	e1:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e1,tp)
end

function c781.atktg(e,c)
	return c:GetFlagEffect(70335319)==0
end
function c781.atop(e,tp,eg,ep,ev,re,r,rp)
	Duel.ChainAttack()
end


function c781.hspfilter(c)
	return c:IsRace(RACE_DRAGON) and c:IsType(TYPE_PENDULUM)
end
function c781.hspcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	local g=Duel.GetReleaseGroup(tp)
	return g:GetCount()>=1 and g:FilterCount(Card.IsReleasable,nil)==g:GetCount()
		and g:IsExists(c781.hspfilter,1,nil)
		and (c:IsLocation(LOCATION_EXTRA) and Duel.GetLocationCountFromEx(tp,tp,g,c)>0
			or c:IsLocation(LOCATION_HAND) and Duel.GetMZoneCount(tp,g,tp)>0)
end
function c781.hspop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=Duel.GetReleaseGroup(tp)
	Duel.Release(g,REASON_COST)
	local g2=Duel.GetOperatedGroup()
	if g2:GetCount()<1 then return end
	e:SetLabel(g2:FilterCount(Card.IsRace,nil,RACE_DRAGON))
end

function c781.atkcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	--local bc=c:GetBattleTarget()
	return bit.band(c:GetSummonType(),SUMMON_TYPE_SPECIAL+781)~=0
--bc and bc:IsControler(1-tp)
end
function c781.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	if c:IsFaceup() and c:IsRelateToEffect(e) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(e:GetLabelObject():GetLabel()*1000)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_DISABLE+RESET_PHASE+PHASE_DAMAGE)
		c:RegisterEffect(e1)
		if bc and bc:IsFaceup() then
		   local e2=Effect.CreateEffect(c)
		   e2:SetType(EFFECT_TYPE_SINGLE)
		   e2:SetCode(EFFECT_UPDATE_ATTACK)
		   e2:SetValue(-e:GetLabelObject():GetLabel()*1000)
		   e2:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_DISABLE+RESET_PHASE+PHASE_DAMAGE)
		   bc:RegisterEffect(e2)
		end
	end
end

function c781.negcon(e,tp,eg,ep,ev,re,r,rp)
	return not e:GetHandler():IsStatus(STATUS_BATTLE_DESTROYED)
		and ep~=tp and re:IsActiveType(TYPE_SPELL+TYPE_TRAP) and Duel.IsChainNegatable(ev)
end
function c781.cfilter(c)
	return c:IsType(TYPE_SPELL+TYPE_TRAP) and c:IsAbleToGraveAsCost()
end
function c781.negcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c781.cfilter,tp,LOCATION_HAND+LOCATION_ONFIELD,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local g=Duel.SelectMatchingCard(tp,c781.cfilter,tp,LOCATION_HAND+LOCATION_ONFIELD,0,1,1,nil)
	Duel.SendtoGrave(g,REASON_COST)
end
function c781.negtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_NEGATE,eg,1,0,0)
end
function c781.cfilter2(c,code)
	return c:IsType(TYPE_SPELL+TYPE_TRAP) and c:IsFaceup() and c:IsCode(code)
end
function c781.negop(e,tp,eg,ep,ev,re,r,rp)
	local ec=re:GetHandler()
	Duel.NegateActivation(ev)
	if re:GetHandler():IsRelateToEffect(re) then
		ec:CancelToGrave()
		local g=Duel.GetMatchingGroup(c781.cfilter2,tp,0,LOCATION_SZONE,ec,ec:GetCode())
		if g:GetCount()<1 then return end
		g:AddCard(ec)
		for tc in aux.Next(g) do
		   Duel.ChangePosition(tc,POS_FACEDOWN)
		   local e1=Effect.CreateEffect(e:GetHandler())
		   e1:SetType(EFFECT_TYPE_SINGLE)
		   e1:SetCode(EFFECT_CANNOT_TRIGGER)
		   e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		   tc:RegisterEffect(e1)
		   Duel.RaiseEvent(tc,EVENT_SSET,e,REASON_EFFECT,tp,tp,0)
		end
	end
end

function c781.atknegcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return eg:GetFirst() and eg:GetFirst()==c
end
function c781.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_SKIP_TURN)
	e1:SetTargetRange(0,1)
	e1:SetReset(RESET_PHASE+PHASE_END+RESET_OPPO_TURN)
	Duel.RegisterEffect(e1,tp)
	Duel.SkipPhase(tp,PHASE_STANDBY,RESET_PHASE+PHASE_BATTLE+RESET_SELF_TURN,2)
	Duel.SkipPhase(tp,PHASE_DRAW,RESET_PHASE+PHASE_BATTLE+RESET_SELF_TURN,2)
	Duel.SkipPhase(tp,PHASE_MAIN1,RESET_PHASE+PHASE_BATTLE+RESET_SELF_TURN,2)
end

