--蛇神凱
function c123106.initial_effect(c)
	c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	--cannot special summon
	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SPSUMMON_CONDITION)
	e0:SetValue(aux.FALSE)
	c:RegisterEffect(e0)

	  --Cannot Lose
	local e18=Effect.CreateEffect(c)
	e18:SetType(EFFECT_TYPE_FIELD)
	e18:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e18:SetCode(EFFECT_CANNOT_LOSE_LP)
	e18:SetRange(LOCATION_MZONE)
	e18:SetTargetRange(1,0)
	e18:SetValue(1)
	c:RegisterEffect(e18)
	local e19=e18:Clone()
	e19:SetCode(EFFECT_CANNOT_LOSE_DECK)
	c:RegisterEffect(e19)
	local e20=e18:Clone()
	e20:SetCode(EFFECT_CANNOT_LOSE_EFFECT)
	c:RegisterEffect(e20)
			
	  --spsummon success
	  local e5=Effect.CreateEffect(c)
	  e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	  e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	  e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	  e5:SetOperation(c123106.sucop)
	  c:RegisterEffect(e5)

	  --attack cost
	  local e6=Effect.CreateEffect(c)
	  e6:SetType(EFFECT_TYPE_SINGLE)
	  e6:SetCode(EFFECT_ATTACK_COST)
	  --e6:SetCondition(c123106.atcon)
	  e6:SetCost(c123106.atcost)
	  e6:SetOperation(c123106.atop)
	  c:RegisterEffect(e6)

	--特殊召唤不会被无效化
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	c:RegisterEffect(e9)

	--不能被各种方式解放
	local e12=Effect.CreateEffect(c)
	e12:SetType(EFFECT_TYPE_SINGLE)
	e12:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e12:SetRange(LOCATION_MZONE)
	e12:SetCode(EFFECT_UNRELEASABLE_SUM)
	e12:SetValue(1)
	c:RegisterEffect(e12)
	local e13=e12:Clone()
	e13:SetCode(EFFECT_UNRELEASABLE_NONSUM)
	c:RegisterEffect(e13)
	local e14=e12:Clone()
	e14:SetCode(EFFECT_UNRELEASABLE_EFFECT)
	c:RegisterEffect(e14)

	--不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变为里侧表示、作为特殊召唤素材
	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e101)
	local e102=e101:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103=e102:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104=e103:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e106=e104:Clone()
	e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e106)
	local e107=e106:Clone()
	e107:SetCode(EFFECT_CANNOT_TURN_SET)
	c:RegisterEffect(e107)
	local e108=e107:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109=e108:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110=e109:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e1102=e109:Clone()
	e1102:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e1102)
	local e111=e109:Clone()
	e111:SetCode(EFFECT_CANNOT_RELEASE)
	c:RegisterEffect(e111)
	local e112=e109:Clone()
	e112:SetCode(EFFECT_CANNOT_USE_AS_COST)
	c:RegisterEffect(e112)

	local e1235=Effect.CreateEffect(c)
	e1235:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1235:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e1235:SetCode(EVENT_LEAVE_FIELD)
	e1235:SetOperation(c123106.lose)
	c:RegisterEffect(e1235)  

	local e1232=Effect.GlobalEffect()  
	e1232:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1232:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e1232:SetCode(EVENT_ADJUST)
	e1232:SetRange(LOCATION_MZONE)  
	e1232:SetCondition(c123106.lpcon)
	e1232:SetOperation(c123106.lpop)
	e1232:SetTargetRange(1,0)
	--c:RegisterEffect(e1232) 
	local e1233=e1232:Clone()
	e1233:SetCode(EVENT_CHAIN_SOLVED)
	--c:RegisterEffect(e1233)

	local e1234=Effect.CreateEffect(c)
	e1234:SetType(EFFECT_TYPE_FIELD)
	e1234:SetCode(EFFECT_DRAW_COUNT)
	e1234:SetRange(LOCATION_MZONE)  
	e1234:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1234:SetTargetRange(1,0)
	e1234:SetCondition(c123106.dc)
	e1234:SetValue(0)
	--c:RegisterEffect(e1234)  
end

function c123106.rdop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local ttp=c:GetControler()
	if Duel.GetBattleDamage(ttp)>=Duel.GetLP(ttp) then
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1,0)
	e3:SetValue(Duel.GetLP(ttp)-1)
	e3:SetReset(RESET_PHASE+PHASE_DAMAGE)
	Duel.RegisterEffect(e3,ttp) end
end
function c123106.rdcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tp=c:GetControler()
	local e1=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_DAMAGE)
	local e2=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_RECOVER)
	local rd=e1 and not e2
	local rr=not e1 and e2
	local ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_DAMAGE)
	if ex and (cp==tp or cp==PLAYER_ALL) and not rd and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) then 
		return true 
	end
	ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_RECOVER)
	return ex and (cp==tp or cp==PLAYER_ALL) and rr and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) 
end
function c123106.rdop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tp=c:GetControler()
	local e1=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_DAMAGE)
	local e2=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_RECOVER)
	local rd=e1 and not e2
	local rr=not e1 and e2
	local ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_DAMAGE)
	if ex and (cp==tp or cp==PLAYER_ALL) and not rd and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) then 
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1,0)
	e3:SetValue(Duel.GetLP(tp)-1)
	e3:SetReset(RESET_EVENT+EVENT_CHAIN_SOLVED)
	Duel.RegisterEffect(e3,tp) 
	end
	ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_RECOVER)
	if ex and (cp==tp or cp==PLAYER_ALL) and rr and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) then
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1,0)
	e3:SetValue(Duel.GetLP(tp)-1)
	e3:SetReset(RESET_EVENT+EVENT_CHAIN_SOLVED)
	Duel.RegisterEffect(e3,tp) 
	end 
end
function c123106.dc(e)
local tp=e:GetOwner():GetControler()
return Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)<1
end

function c123106.sucop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetFieldGroup(tp,LOCATION_HAND,0)
	Duel.SendtoGrave(g,REASON_EFFECT)
	local c=e:GetHandler()
	--Duel.SetLP(e:GetHandlerPlayer(),4)
			local e102=Effect.CreateEffect(c)
			e102:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
			e102:SetCode(EVENT_PRE_DAMAGE_CALCULATE)
			e102:SetOperation(c123106.rdop)
			--Duel.RegisterEffect(e102,tp)
			local e103=e102:Clone()
			e103:SetCode(EVENT_CHAINING)
			e103:SetCondition(c123106.rdcon)
			e103:SetOperation(c123106.rdop2)
			--Duel.RegisterEffect(e103,tp) 
end

function c123106.lpcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetLP(e:GetHandlerPlayer())~=4
end
function c123106.lpop(e,tp,eg,ep,ev,re,r,rp) 
	Duel.SetLP(e:GetHandlerPlayer(),4)
end 

function c123106.atcon(e,tp,eg,ep,ev,re,r,rp)
	  return e:GetHandler():IsCode(82103466)
end
function c123106.atcost(e,c,tp)
	return Duel.GetFieldGroupCount(e:GetHandler():GetControler(),LOCATION_DECK,0)>=10
end
function c123106.atop(e,tp,eg,ep,ev,re,r,rp)
	Duel.DiscardDeck(e:GetHandler():GetControler(),10,REASON_COST)
	Duel.AttackCostPaid()
end

function c123106.damval(e,re,val,r,rp,rc)
	if bit.band(r,REASON_EFFECT)~=0 then return 0 end
	return val
end

function c123106.losecon(e,tp,eg,ep,ev,re,r,rp)
	  local tc=e:GetHandler() 
	  local ttp=tc:GetOwner() 
	return tc:IsFacedown() or tc:IsDisabled() 
end
function c123106.lose(e,tp,eg,ep,ev,re,r,rp)
	  local tc=e:GetHandler() 
	  local ttp=tc:GetOwner() 
	  if Duel.IsExistingMatchingCard(Card.IsCode,ttp,LOCATION_EXTRA,0,1,nil,123108) and Duel.IsExistingMatchingCard(c123106.cfilter,ttp,LOCATION_FZONE,0,1,nil) then return end
	  local WIN_REASON_DIVINE_SERPENT=0x103
	  Duel.Win(1-ttp,WIN_REASON_DIVINE_SERPENT)
end

function c123106.cfilter(c)
	   return c:IsSetCard(0x900) and c:IsType(TYPE_FIELD) and c:IsFaceup()
end
function c123106.spcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return c:IsPreviousPosition(POS_FACEUP) and Duel.IsExistingMatchingCard(c123106.cfilter,tp,LOCATION_SZONE,LOCATION_SZONE,1,nil)
end
function c123106.spfilter(c,e,tp)
	return c:IsCode(123108) and c:IsCanBeSpecialSummoned(e,0,tp,true,true)
end
function c123106.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c123106.spfilter,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED,0,1,nil,e,tp) 
							and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED)
end
function c123106.spop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)==0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c123106.spfilter,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED,0,1,1,nil,e,tp)
	if g:GetCount()~=0 then
		Duel.SpecialSummon(g,1,tp,tp,true,true,POS_FACEUP)
	end
end
