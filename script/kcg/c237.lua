--No.69 紋章神コート·オブ·アームズ
local s, id = GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,4,3)
	c:EnableReviveLimit()

	--cannot destroyed
	  local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e0:SetValue(s.indes)
	c:RegisterEffect(e0)

	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e100:SetValue(1)
	c:RegisterEffect(e100)

	--复制效果
	local e4=Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_ADJUST)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCondition(s.sdcon)
	e4:SetOperation(s.sdop)
	c:RegisterEffect(e4)
	local e7=e4:Clone()
	e7:SetCode(EVENT_CHAIN_SOLVED)
	c:RegisterEffect(e7) 
	local e8=e4:Clone()
	e8:SetCode(EVENT_SUMMON_SUCCESS)
	c:RegisterEffect(e8) 
	local e9=e4:Clone()
	e9:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
	c:RegisterEffect(e9) 
	local e10=e4:Clone()
	e10:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e10) 
	local e6=e4:Clone()
	e6:SetCondition(s.sdcon2)
	e6:SetOperation(s.sdop2)
	c:RegisterEffect(e6)

	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(16037007,1))
	e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e5:SetCode(EVENT_ATTACK_ANNOUNCE)
	e5:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e5:SetRange(LOCATION_MZONE)
	e5:SetCost(s.ocost)
	e5:SetTarget(s.otg)
	e5:SetOperation(s.oop)
	c:RegisterEffect(e5,false,REGISTER_FLAG_DETACH_XMAT)
end
s.xyz_number=69
s.listed_series = {0x48}

function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end

function s.distarget(e,c)
	  return c~=e:GetHandler()
end

function s.sdfilter(c,e,tc)
	return c:IsFaceup() and (c:GetFlagEffect(237)==0 or (c:GetFlagEffect(237)~=0 and c:GetFlagEffectLabel(237)~=tc:GetFieldID()))
			and not c:IsImmuneToEffect(e) and not c:IsDisabled()
end
function s.sdcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()  
	local g=Duel.GetMatchingGroup(s.sdfilter,tp,LOCATION_MZONE,LOCATION_MZONE,c,e,c)
	return g:GetCount()>0
end
function s.sdop(e,tp,eg,ep,ev,re,r,rp)   
	local c=e:GetHandler()
	c:RegisterFlagEffect(462,RESET_EVENT+0x1fe0000,0,1)
	local g=Duel.GetMatchingGroup(s.sdfilter,tp,LOCATION_MZONE,LOCATION_MZONE,c,e,c)
	if g:GetCount()>0 then 
	local tc=g:GetFirst()
	while tc do
	local e1=Effect.CreateEffect(c)
	--e1:SetProperty(EFFECT_FLAG_OWNER_RELATE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_DISABLE)
	e1:SetCondition(s.dcon)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	tc:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EFFECT_DISABLE_EFFECT)
	tc:RegisterEffect(e2)
	tc:RegisterFlagEffect(237,RESET_EVENT+0x1fe0000,0,1) 
	tc:SetFlagEffectLabel(237, c:GetFieldID())
	--c:CopyEffect(tc:GetOriginalCode(),RESET_EVENT+0x1fe0000,1)
	tc=g:GetNext()
	end end
end
function s.sdcon2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()  
	local g=Duel.GetMatchingGroup(s.sdfilter2,tp,LOCATION_MZONE,LOCATION_MZONE,c,c)
	return g:GetCount()>0
end
function s.sdfilter2(c,tc)
	return c:IsFaceup() and c:GetFlagEffect(237)==1 and c:GetFlagEffectLabel(237)==tc:GetFieldID() and c:IsDisabled()
end
function s.sdop2(e,tp,eg,ep,ev,re,r,rp)   
	  local c=e:GetHandler()
	  local g=Duel.GetMatchingGroup(s.sdfilter2,tp,LOCATION_MZONE,LOCATION_MZONE,c,c)
	  if g:GetCount()>0 then 
	  local tc=g:GetFirst()
	  while tc do
	  c:CopyEffect(tc:GetOriginalCode(),RESET_EVENT+0x1fe0000,1)
	  tc:RegisterFlagEffect(237,RESET_EVENT+0x1fe0000,0,1) 
	  tc:SetFlagEffectLabel(237, c:GetFieldID())
	  tc=g:GetNext()
	  end end
end
function s.dcon(e,tp,eg,ep,ev,re,r,rp)
	  local c=e:GetOwner()  
	return c:IsLocation(LOCATION_MZONE) and c:IsFaceup() and not c:IsStatus(STATUS_DISABLED) and c:GetFlagEffect(462)~=0
end

function s.ofilter(c)
	return aux.TRUE
end
function s.ocost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.otg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_ONFIELD) and s.ofilter(chkc) end
	if chk==0 then return Duel.GetAttacker():IsControler(1-tp) and Duel.IsExistingTarget(s.ofilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	local g=Duel.SelectTarget(tp,s.ofilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,1,0,0)   
end
function s.oop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc:IsRelateToEffect(e) then
	   Duel.Destroy(tc,REASON_EFFECT)
	end
end
