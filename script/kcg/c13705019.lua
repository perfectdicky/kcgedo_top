--Ra's Disciple
function c13705019.initial_effect(c)
	--search
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(86547356,1))
	e1:SetCategory(CATEGORY_TOHAND+CATEGORY_SEARCH)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e1:SetType(EFFECT_TYPE_TRIGGER_O+EFFECT_TYPE_SINGLE)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetTarget(c13705019.tg)
	e1:SetOperation(c13705019.op)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
	c:RegisterEffect(e2)
	local e3=e1:Clone()
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e3)

	local e01=Effect.CreateEffect(c)
	e01:SetDescription(aux.Stringid(74875003,0))
	e01:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e01:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e01:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e01:SetCode(EVENT_SUMMON_SUCCESS)
	e01:SetTarget(c13705019.target)
	e01:SetOperation(c13705019.operation)
	c:RegisterEffect(e01)
	local e02=e01:Clone()
	e02:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e02)
	local e03=e01:Clone()
	e03:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
	c:RegisterEffect(e03)	
end
c13705019.listed_names={74875003}

function c13705019.filter2(c,e,tp)
	return c:IsCode(74875003) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function c13705019.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingMatchingCard(c13705019.filter2,tp,LOCATION_DECK+LOCATION_HAND,0,1,nil,e,tp) and e:GetHandler():GetFlagEffect(13705019)==0 end
	e:GetHandler():RegisterFlagEffect(13705019,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END,0,1)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_DECK+LOCATION_HAND)
end
function c13705019.operation(e,tp,eg,ep,ev,re,r,rp)
	local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if ft<=0 then return end
	if ft>2 then ft=2 end
	if Duel.IsPlayerAffectedByEffect(tp,CARD_BLUEEYES_SPIRIT) then ft=1 end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c13705019.filter2,tp,LOCATION_DECK+LOCATION_HAND,0,1,ft,nil,e,tp)
	if #g>0 then
		Duel.SpecialSummon(g,0,tp,tp,false,false,POS_FACEUP)
	end
end

function c13705019.tg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c13705019.filter,tp,LOCATION_DECK,0,1,nil) and e:GetHandler():GetFlagEffect(13705019)==0 end
	e:GetHandler():RegisterFlagEffect(13705019,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END,0,1)
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK)
end
function c13705019.op(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,c13705019.filter,tp,LOCATION_DECK,0,1,2,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,g)
	end
end
function c13705019.filter(c)
	return c:IsCode(74875003) or c:IsCode(10000010) or c:IsCode(708)
end
