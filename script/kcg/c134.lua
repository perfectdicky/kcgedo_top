local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()

	  --special summon
	  local e1=Effect.CreateEffect(c)
	  e1:SetType(EFFECT_TYPE_FIELD)
	  e1:SetCode(EFFECT_SPSUMMON_PROC)
	  e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	  e1:SetRange(LOCATION_HAND)
	  e1:SetCondition(s.spcon)
	  e1:SetOperation(s.spop)
	  c:RegisterEffect(e1)

	  local e04=Effect.CreateEffect(c) 
	e04:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	  e04:SetType(EFFECT_TYPE_SINGLE) 
	  e04:SetCode(EFFECT_CANNOT_SUMMON) 
	  c:RegisterEffect(e04) 
	  local e05=e04:Clone() 
	  e05:SetCode(EFFECT_CANNOT_MSET) 
	  c:RegisterEffect(e05) 

	  local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	  e00:SetType(EFFECT_TYPE_SINGLE)
	  e00:SetCode(EFFECT_SET_ATTACK)
	  e00:SetValue(s.atkvalue)
	  c:RegisterEffect(e00)
	  e01=e00:Clone()
	  e01:SetCode(EFFECT_SET_DEFENSE)
	  e01:SetValue(s.defvalue)
	  c:RegisterEffect(e01)

	--spsummon
	local e30=Effect.CreateEffect(c)
	e30:SetType(EFFECT_TYPE_SINGLE)
	e30:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e30:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e30)

	  local e4=Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	  e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	  e4:SetCode(EVENT_SPSUMMON_SUCCESS)
    e4:SetTarget(s.spcon2)
    e4:SetOperation(s.spop2)
	  c:RegisterEffect(e4)

	--不受对方的陷阱效果以及魔法效果怪兽
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e5:SetRange(LOCATION_MZONE)
	e5:SetCode(EFFECT_IMMUNE_EFFECT)
	e5:SetValue(s.efilter)
	c:RegisterEffect(e5)

	--Search
	local e7=Effect.CreateEffect(c)
	e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e7:SetCategory(CATEGORY_TOHAND)
	e7:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e7:SetCode(EVENT_PREDRAW)
	e7:SetRange(LOCATION_MZONE)
	e7:SetCondition(s.schcon)
	e7:SetTarget(s.schtar)
	e7:SetOperation(s.schop)
	c:RegisterEffect(e7)

	--不能被各种方式解放
	local e12=Effect.CreateEffect(c)
	e12:SetType(EFFECT_TYPE_SINGLE)
	e12:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e12:SetRange(LOCATION_MZONE)
	e12:SetCode(EFFECT_UNRELEASABLE_SUM)
	e12:SetValue(1)
	c:RegisterEffect(e12)
	local e13=e12:Clone()
	e13:SetCode(EFFECT_UNRELEASABLE_NONSUM)
	c:RegisterEffect(e13)
	local e14=e12:Clone()
	e14:SetCode(EFFECT_UNRELEASABLE_EFFECT)
	c:RegisterEffect(e14)

	--不会被卡的效果破坏、除外、返回手牌和卡组、送入墓地、无效化、改变控制权、变为里侧表示、成为特殊召唤素材
	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	  e100:SetValue(s.lffilter)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_REMOVE)
	  e101:SetValue(1)
	c:RegisterEffect(e101)
	local e102=e101:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103=e102:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104=e103:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e105=e104:Clone()
	e105:SetCode(EFFECT_CANNOT_DISABLE)
	--c:RegisterEffect(e105)
	local e106=e105:Clone()
	e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e106)
	local e107=e106:Clone()
	e107:SetCode(EFFECT_CANNOT_TURN_SET)
	c:RegisterEffect(e107)
	local e108=e107:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109=e108:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110=e109:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e1102=e109:Clone()
	e1102:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e1102)
	local e111=e109:Clone()
	e111:SetCode(EFFECT_CANNOT_RELEASE)
	c:RegisterEffect(e111)
	local e112=e109:Clone()
	e112:SetCode(EFFECT_CANNOT_USE_AS_COST)
	c:RegisterEffect(e112)
	local e114=e104:Clone()
	e114:SetCode(EFFECT_CANNOT_DISEFFECT)
	--c:RegisterEffect(e114)

		--selfdes
		local e201=Effect.CreateEffect(c)
		e201:SetType(EFFECT_TYPE_SINGLE)
		e201:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
		e201:SetRange(LOCATION_MZONE)
		e201:SetCode(EFFECT_SELF_DESTROY)
		e201:SetCondition(s.descon)
		c:RegisterEffect(e201)

		--spson
		local e202=Effect.CreateEffect(c)
		e202:SetType(EFFECT_TYPE_SINGLE)
		e202:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e202:SetCode(EFFECT_SPSUMMON_CONDITION)
		e202:SetValue(aux.FALSE)
		c:RegisterEffect(e202)
end
s.listed_names={27564031}

function s.spfilter(c)
		return c:IsCode(90048290) and c:IsAbleToGraveAsCost()
end
function s.spcon(e,c)
		if c==nil then return true end
		return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0
				and Duel.IsExistingMatchingCard(s.spfilter,c:GetControler(),LOCATION_EXTRA,0,1,nil)
				and Duel.IsExistingMatchingCard(Card.IsSetCard,c:GetControler(),LOCATION_GRAVE,0,3,nil,0x23)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp,c)
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
		local tg=Duel.SelectMatchingCard(tp,s.spfilter,tp,LOCATION_EXTRA,0,1,1,nil)
		Duel.SendtoGrave(tg,REASON_COST)
end

function s.dragfilter(c)
    return c:IsType(TYPE_SYNCHRO) and c:IsRace(RACE_DRAGON)
end
function s.atkvalue(e)
    local g = Duel.GetMatchingGroup(s.dragfilter, e:GetHandler():GetControler(), LOCATION_MZONE, 0, nil)
    local tatk = 0
    local tc = g:GetFirst()
    while tc do
        local atk = tc:GetAttack()
        tatk = tatk + atk
        tc = g:GetNext()
    end
    return tatk
end
function s.defvalue(e)
    local g = Duel.GetMatchingGroup(s.dragfilter, e:GetHandler():GetControler(), LOCATION_MZONE, 0, nil)
    local tdef = 0
    local tc = g:GetFirst()
    while tc do
        local def = tc:GetDefense()
        tdef = tdef + def
        tc = g:GetNext()
    end
    return tdef
end

function s.spfilter2(c, e, tp)
    return c:IsType(TYPE_SYNCHRO) and c:IsRace(RACE_DRAGON) and
               c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_SYNCHRO, tp, true, false)
end
function s.spcon2(e, tp, eg, ep, ev, re, r, rp, chk)
    local g = Duel.GetMatchingGroup(s.spfilter2, tp, LOCATION_EXTRA, 0, nil, e, tp)
    if chk == 0 then
        return (#g > 0) and Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO) > 0
    end
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_EXTRA)
end
function s.spop2(e, tp, eg, ep, ev, re, r, rp, c)
    local g = Duel.GetMatchingGroup(s.spfilter2, tp, LOCATION_EXTRA, 0, nil, e, tp)
    if Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO) < 1 then
        return
    end
    local sg = Duel.SelectMatchingCard(tp, s.spfilter2, tp, LOCATION_EXTRA, 0, 1,
                   Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO), nil, e, tp)
    Duel.SpecialSummon(sg, SUMMON_TYPE_SYNCHRO, tp, tp, true, false, POS_FACEUP)
    for sg0 in aux.Next(sg) do
        sg0:CompleteProcedure()
    end
end

function s.schcon(e,tp,eg,ep,ev,re,r,rp)
	return tp==Duel.GetTurnPlayer() and Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)>0
		and Duel.GetDrawCount(tp)>0
end
function s.schfilter(c)
	return c:IsCode(21159309) and c:IsAbleToHand()
end
function s.schtar(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local dt=Duel.GetDrawCount(tp)
	if dt~=0 then
		_replace_count=0
		_replace_max=dt
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1,0)
		e1:SetReset(RESET_PHASE+PHASE_DRAW)
		e1:SetValue(0)
		Duel.RegisterEffect(e1,tp)
	end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK)
end
function s.schop(e,tp,eg,ep,ev,re,r,rp)
	_replace_count=_replace_count+1
	if _replace_count>_replace_max or not e:GetHandler():IsRelateToEffect(e) or e:GetHandler():IsFacedown()then return end
	local code=21159309
	local gg=Group.FromCards(Duel.CreateToken(tp,code))
	Duel.SendtoDeck(gg,tp,nil,REASON_RULE)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,s.schfilter,tp,LOCATION_DECK,0,1,1,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,nil,REASON_RULE)
		Duel.ConfirmCards(1-tp,g)
	end
end

function s.efilter(e,te)
	return te:GetOwnerPlayer()~=e:GetOwnerPlayer()
end

function s.lffilter(e,re,rp)
	return re:GetOwner()~=e:GetHandler() 
end

function s.descon(e)
	local c=e:GetHandler()
	local f1=Duel.GetFieldCard(0,LOCATION_SZONE,5)
	local f2=Duel.GetFieldCard(1,LOCATION_SZONE,5)
	return ((f1==nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and (f2==nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end
