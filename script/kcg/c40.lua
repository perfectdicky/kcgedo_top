--奇跡の方舟
function c40.initial_effect(c)
	  c:EnableReviveLimit()

	--search
	local e0=Effect.CreateEffect(c)
	e0:SetDescription(aux.Stringid(612115,0))
	e0:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	  e0:SetCode(EVENT_PHASE+PHASE_STANDBY)
	e0:SetRange(LOCATION_HAND)
	  e0:SetCountLimit(1)
	e0:SetCost(c40.cost)
	c:RegisterEffect(e0)

	  local e00=Effect.CreateEffect(c)
	  e00:SetDescription(aux.Stringid(1802450,1))
	  e00:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e00:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	  e00:SetCode(EVENT_PHASE+PHASE_STANDBY)
	e00:SetRange(LOCATION_REMOVED)
	  e00:SetCondition(c40.spcon2) 
	  e00:SetCountLimit(1)
	  e00:SetTarget(c40.sptarget2)
	e00:SetOperation(c40.spoperation2)
	c:RegisterEffect(e00)

	  --REMOVE
	local e1=Effect.CreateEffect(c) 
	  e1:SetCategory(CATEGORY_REMOVE) 
	e1:SetType(EFFECT_TYPE_FIELD) 
	e1:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_IGNORE_RANGE) 
	e1:SetCode(EFFECT_TO_GRAVE_REDIRECT) 
	e1:SetRange(LOCATION_REMOVED+LOCATION_MZONE) 
	e1:SetTarget(c40.rmtarget) 
	e1:SetValue(LOCATION_REMOVED) 
	c:RegisterEffect(e1) 

	  --SPECIAL SUMMON
	  local e2=Effect.CreateEffect(c)
	  e2:SetDescription(aux.Stringid(5438492,0))
	  e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	  e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	  e2:SetRange(LOCATION_REMOVED)
	  e2:SetCode(EVENT_ATTACK_ANNOUNCE)
	  e2:SetCountLimit(1)
	  e2:SetCondition(c40.rdcon)
	  e2:SetTarget(c40.rdtar)
	  e2:SetOperation(c40.rdop) 
	  c:RegisterEffect(e2)

	--RECOVER
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(54959865,0))
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetCategory(CATEGORY_TOGRAVE+CATEGORY_RECOVER)
	e3:SetType(EFFECT_TYPE_IGNITION)
	e3:SetRange(LOCATION_REMOVED+LOCATION_MZONE)
	e3:SetCountLimit(1)
	e3:SetTarget(c40.lptarget)
	e3:SetOperation(c40.lpoperation)
	c:RegisterEffect(e3)

	  --special summon
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(5438492,0))
	e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	  e4:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e4:SetCode(EVENT_DESTROYED)
	  --e4:SetCondition(c40.spcon) 
	e4:SetTarget(c40.sptarget)
	e4:SetOperation(c40.spoperation)
	c:RegisterEffect(e4)
end

function c40.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return c:IsAbleToRemoveAsCost() end
	Duel.Remove(c,POS_FACEUP,REASON_EFFECT)
end

function c40.rmtarget(e,c)
	return not c:IsLocation(0x80) and c:IsType(TYPE_MONSTER)
end

function c40.rdcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()~=tp and Duel.GetFieldGroupCount(tp,LOCATION_MZONE,0)==0
end
function c40.rdfilter(c,e,tp)
	return c:IsCanBeSpecialSummoned(e,0,tp,false,false,POS_FACEUP_DEFENSE) and c:IsType(TYPE_MONSTER) 
	and c:GetFieldID()>e:GetHandler():GetFieldID() 
end
function c40.rdtar(e,tp,eg,ep,ev,re,r,rp,chk)
	  if chk==0 then return Duel.IsExistingMatchingCard(c40.rdfilter,tp,LOCATION_REMOVED,LOCATION_REMOVED,1,e:GetHandler(),e,tp) end
	  local g=Duel.GetMatchingGroup(c40.rdfilter,tp,LOCATION_REMOVED,LOCATION_REMOVED,e:GetHandler(),e,tp)
	  local atkc=Duel.GetMatchingGroupCount(Card.CanAttack,tp,0,LOCATION_MZONE,nil)
	  if atkc>g:GetCount() then atkc=g:GetCount() end
	  if Duel.IsPlayerAffectedByEffect(tp,59822133) then atkc=1 end   
	  Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,atkc,tp,LOCATION_REMOVED)
end
function c40.rdop(e,tp,eg,ep,ev,re,r,rp)
	  if Duel.GetFieldGroupCount(tp,LOCATION_MZONE,0)~=0 then return end
	  local g=Duel.GetMatchingGroup(c40.rdfilter,tp,LOCATION_REMOVED,LOCATION_REMOVED,e:GetHandler(),e,tp)
	  local atkc=Duel.GetMatchingGroupCount(Card.CanAttack,tp,0,LOCATION_MZONE,nil)
	  if g:GetCount()~=0 and atkc>0 then
	  if atkc>g:GetCount() then atkc=g:GetCount() end
	  if Duel.IsPlayerAffectedByEffect(tp,59822133) then atkc=1 end
	  local g2=g:RandomSelect(tp,atkc)
	  Duel.SpecialSummon(g2,0,tp,tp,false,false,POS_FACEUP_DEFENSE) end
end

function c40.rdfilter2(c,e)
	  return c:IsAbleToGrave() and c:IsType(TYPE_MONSTER) and c:GetFieldID()>e:GetHandler():GetFieldID() 
end
function c40.lptarget(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c40.rdfilter2,tp,LOCATION_REMOVED,LOCATION_REMOVED,1,e:GetHandler(),e) end
	  local g=Duel.GetMatchingGroup(c40.rdfilter2,tp,LOCATION_REMOVED,LOCATION_REMOVED,e:GetHandler(),e)
	Duel.SetOperationInfo(0,CATEGORY_TOGRAVE,nil,g:GetCount(),tp,rt)
	Duel.SetOperationInfo(0,CATEGORY_RECOVER,nil,0,tp,g:GetCount()*500)
end
function c40.lpoperation(e,tp,eg,ep,ev,re,r,rp)
	  local g=Duel.GetMatchingGroup(c40.rdfilter2,tp,LOCATION_REMOVED,LOCATION_REMOVED,e:GetHandler(),e)
	  if Duel.SendtoGrave(g,nil,REASON_EFFECT)>0 then
	  local g2=Duel.GetOperatedGroup()
	  Duel.BreakEffect()
	  Duel.Recover(tp,g2:GetCount()*500,REASON_EFFECT) end
end

function c40.spcon2(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetTurnID()==Duel.GetTurnCount()-6
end
function c40.sptarget2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_REMOVED)
end
function c40.spoperation2(e,tp,eg,ep,ev,re,r,rp)
	  Duel.SpecialSummon(e:GetHandler(),0,tp,tp,true,true,POS_FACEUP)
	  e:GetHandler():CompleteProcedure()
end

function c40.spcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetTurnID()>=7 and e:GetHandler():IsOnField()
end
function c40.sptarget(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION)>0 and Duel.IsExistingMatchingCard(c40.spfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c40.spfilter(c,e,tp)
	return c:IsCode(86327225) and c:IsCanBeSpecialSummoned(e,0,tp,true,false)
end
function c40.spoperation(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c40.spfilter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	local tc=g:GetFirst()
	if tc then
	Duel.SpecialSummon(tc,0,tp,tp,true,false,POS_FACEUP)
	tc:CompleteProcedure()
	end
end
