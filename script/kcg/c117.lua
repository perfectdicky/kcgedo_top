--Ｄ－フォース
local s, id = GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCategory(CATEGORY_TODECK)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)	

	--Cannot draw
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetCode(EFFECT_CANNOT_DRAW)
	e2:SetRange(LOCATION_DECK)
	e2:SetTargetRange(1,0)
	e2:SetCondition(s.drcon1)
	c:RegisterEffect(e2)
	
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetCode(EFFECT_DRAW_COUNT)
	e3:SetRange(LOCATION_DECK)
	e3:SetTargetRange(1,0)
	e3:SetCondition(s.drcon2)
	e3:SetValue(0)
	c:RegisterEffect(e3)
        
	--不受效果
    local e4=Effect.CreateEffect(c)
    e4:SetType(EFFECT_TYPE_FIELD)
    e4:SetCode(EFFECT_DISABLE)
    e4:SetRange(LOCATION_DECK)
    e4:SetTargetRange(0,LOCATION_MZONE)
    e4:SetCondition(s.sdcon2)
    c:RegisterEffect(e4)

	--disable and destroy
	local e5=Effect.CreateEffect(c) 
	e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS) 
	e5:SetRange(LOCATION_DECK)
	e5:SetCode(EVENT_CHAIN_SOLVING)  
	e5:SetCondition(s.discon) 
	e5:SetOperation(s.disop) 
	c:RegisterEffect(e5) 
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_TODECK,e:GetHandler(),1,0,0)
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) then
		Duel.BreakEffect()
		c:CancelToGrave()
		Duel.SendtoDeck(c,nil,0,REASON_EFFECT)
		c:ReverseInDeck()
	end
end

function s.drcon1(e)
	return s.drcon2(e) and Duel.GetCurrentPhase()==PHASE_DRAW
end
function s.drcon2(e)
	local c=e:GetHandler()
	local g=Duel.GetDecktopGroup(e:GetHandlerPlayer(),1)
	return g:GetFirst()==c and c:IsFaceup() and Duel.GetTurnPlayer()==e:GetHandlerPlayer()
end

function s.sdcon2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()	
	local g=Duel.GetDecktopGroup(c:GetControler(),1)
	return #g>0 and g:GetFirst()==c and c:IsFaceup()
end

function s.dfilter(c,tp)
	return c:IsControler(tp) and c:IsLocation(LOCATION_ONFIELD)
end
function s.discon(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()	
	local dc=Duel.GetDecktopGroup(tp,1)
	local g=Duel.GetChainInfo(ev,CHAININFO_TARGET_CARDS)	
	return #dc>0 and dc:GetFirst()==c and dc:IsFaceup()
		and re:IsActiveType(TYPE_SPELL+TYPE_TRAP) and rp~=tp
		and re:IsHasProperty(EFFECT_FLAG_CARD_TARGET) and g and g:IsExists(s.dfilter,1,nil,tp)
end
function s.disop(e,tp,eg,ep,ev,re,r,rp)
	local rc=re:GetHandler()
	if ((re:IsHasType(EFFECT_TYPE_ACTIVATE) and Duel.NegateActivation(ev))
		or (not re:IsHasType(EFFECT_TYPE_ACTIVATE) and Duel.NegateEffect(ev)))
		and rc:IsRelateToEffect(re) then
		Duel.Destroy(rc,REASON_EFFECT)
	end
end