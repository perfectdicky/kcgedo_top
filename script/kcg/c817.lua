--极神圣帝 奥丁
local s, id = GetID()

function s.initial_effect(c)
	Synchro.AddProcedure(c,nil,1,1,Synchro.NonTuner(nil),2,99)
	c:EnableReviveLimit()
	
	local te = Effect.CreateEffect(c)
	te:SetProperty(EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP)
    te:SetType(EFFECT_TYPE_QUICK_O)
	te:SetRange(LOCATION_MZONE)
	te:SetCode(EVENT_FREE_CHAIN)
	te:SetCondition(s.descondition)
	te:SetOperation(s.desactivate)
	c:RegisterEffect(te)
	
	--被破坏进墓地结束阶段特殊召唤
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_TO_GRAVE)
	e2:SetOperation(s.regop)
	c:RegisterEffect(e2)
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(817,2))
	e3:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e3:SetCode(EVENT_PHASE+PHASE_END)
	e3:SetRange(LOCATION_GRAVE)
	e3:SetCountLimit(1)
	e3:SetCondition(s.spcon)
	e3:SetTarget(s.sptg)
	e3:SetOperation(s.spop)
	c:RegisterEffect(e3)
	local e4=e3:Clone()
	e4:SetCondition(s.spcon2)
	c:RegisterEffect(e4)
	
	--墓地特召时从卡组抽卡
	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(817,2))
	e5:SetCategory(CATEGORY_DRAW)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e5:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	e5:SetCondition(s.drcon)
	e5:SetTarget(s.drtg)
	e5:SetOperation(s.drop)
	c:RegisterEffect(e5)
end
-------------------------------------------------------------------------------
function s.agfilter(c, tp)
    return c:IsFaceup() and c:IsRace(RACE_DIVINE) and c:IsLocation(LOCATION_MZONE) and c:IsControler(tp)
end
function s.descondition(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(s.agfilter, tp, LOCATION_MZONE, 0,  nil, tp)
	local chk=0
	for tc in aux.Next(g) do
		for i = 1, 428 do
			if tc:IsHasEffect(i) then
				local ae = {tc:IsHasEffect(i)}
				for _, te in ipairs(ae) do
					if te:GetOwner() ~= tc and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_TRAP)) then
						chk=1
						break 
					end
				end
				if chk==1 then break end
			end
		end
		if chk==1 then break end
	end
	return chk==1 and not e:GetHandler():IsStatus(STATUS_CHAINING)
end
function s.desactivate(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(s.agfilter, tp, LOCATION_MZONE, 0,  nil, tp)
	local chk=0
	for tc in aux.Next(g) do
		for i = 1, 428 do
			if tc:IsHasEffect(i) then
				local ae = {tc:IsHasEffect(i)}
				for _, te in ipairs(ae) do
					if te:GetOwner() ~= tc and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_TRAP)) then
						chk=1
						break 
					end
				end
				if chk==1 then break end
			end
		end
		if chk==1 then break end
	end
	if chk==0 then return end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_IMMUNE_EFFECT)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTargetRange(LOCATION_MZONE,0)
	e1:SetTarget(s.gfilter)
	e1:SetValue(s.efilter)
	e1:SetReset(RESET_PHASE+PHASE_END)
	e:GetHandler():RegisterEffect(e1)
end
function s.gfilter(e, c)
    return c:IsFaceup() and c:IsRace(RACE_DIVINE)
end
function s.efilter(e, te)
    return te:IsActiveType(TYPE_SPELL + TYPE_TRAP)
end
-------------------------------------------------------------------------------
function s.regop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local pos=c:GetPreviousPosition()
	if c:IsReason(REASON_BATTLE) then pos=c:GetBattlePosition() end
	if rp~=tp and c:GetPreviousControler()==tp and c:IsReason(REASON_DESTROY)
		and c:IsPreviousLocation(LOCATION_ONFIELD) and bit.band(pos,POS_FACEUP)~=0 then
		c:RegisterFlagEffect(817,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1)
	end
end

function s.spcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(817)~=0
end

function s.spcon2(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(817)~=1
end

function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and e:GetHandler():IsCanBeSpecialSummoned(e,1,tp,false,false) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
end

function s.spop(e,tp,eg,ep,ev,re,r,rp)
	if e:GetHandler():IsRelateToEffect(e) then
		Duel.SpecialSummon(e:GetHandler(),1,tp,tp,false,false,POS_FACEUP)
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.drcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetSummonType()==SUMMON_TYPE_SPECIAL+1
end

function s.drtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,1) end
	Duel.SetTargetPlayer(tp)
	Duel.SetTargetParam(1)
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,1)
end

function s.drop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Draw(p,d,REASON_EFFECT)
end
