--orichalcos Ariesteros
function c12398.initial_effect(c)
	c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(aux.FALSE)
	c:RegisterEffect(e1)

	--change battle target
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_BE_BATTLE_TARGET)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCondition(c12398.cbcon)
	e2:SetOperation(c12398.cbop)
	c:RegisterEffect(e2)

	--Def
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetCode(EFFECT_SET_DEFENSE_FINAL)
	e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e4:SetRange(LOCATION_MZONE)
	e4:SetValue(c12398.adval)
	c:RegisterEffect(e4)

	--Shunoros Debuf
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_BATTLED)
	--e3:SetTarget(c12398.target)
	e3:SetOperation(c12398.operation)
	c:RegisterEffect(e3)

	--selfdestroy
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetCode(EFFECT_SELF_DESTROY)
	e5:SetCondition(c12398.descon)
	e5:SetOperation(c12398.desop)
	c:RegisterEffect(e5)
end
c12398.listed_names={12399}

function c12398.desfilter(c)
	return c:IsCode(12399) and c:IsFaceup()
	and (c:IsLocation(LOCATION_MZONE) or c:GetFlagEffect(12)~=0)
end
function c12398.descon(e)
	return not Duel.IsExistingMatchingCard(c12398.desfilter,e:GetHandler():GetControler(),LOCATION_ONFIELD,0,1,nil)
end
function c12398.desop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Destroy(e:GetHandler(),REASON_EFFECT)
end

function c12398.cbcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bt=eg:GetFirst()
	return c~=bt and bt:GetControler()==c:GetControler()
end
function c12398.cbop(e,tp,eg,ep,ev,re,r,rp)
	  local c=e:GetHandler()
	  Duel.ChangeAttackTarget(c)
end

function c12398.adval(e,c)
	local ph=Duel.GetCurrentPhase()
	local c=e:GetHandler()
	local a=Duel.GetAttacker()
	local d=Duel.GetAttackTarget()
	if (ph==PHASE_DAMAGE_CAL or PHASE_DAMAGE or Duel.IsDamageCalculated()) and c:IsRelateToBattle() and c:GetBattleTarget()~=nil then
		if d==c then return a:GetAttack()+300 end
	end
end

function c12398.filter(c)
	return c:IsFaceup() and c:IsCode(12399) 
	and ((c:GetFlagEffectLabel(123104) and c:GetFlagEffectLabel(123104)>0 and c:IsLocation(LOCATION_SZONE)) 
	or (c:GetAttack()>0 and c:IsLocation(LOCATION_MZONE)))
end
function c12398.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c12398.filter,tp,LOCATION_ONFIELD,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
	Duel.SelectTarget(tp,c12398.filter,tp,LOCATION_ONFIELD,0,1,1,nil)
end
function c12398.operation(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
	local tc=Duel.SelectMatchingCard(tp,c12398.filter,tp,LOCATION_ONFIELD,0,1,1,nil):GetFirst()
	if tc then
		if tc:IsLocation(LOCATION_MZONE) then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(-e:GetHandler():GetDefense())
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1) end
		if tc:IsLocation(LOCATION_SZONE) then
local a=tc:GetFlagEffectLabel(123104)
if a<=e:GetHandler():GetDefense() then a=e:GetHandler():GetDefense() end
 tc:ResetFlagEffect(123104)
		tc:RegisterFlagEffect(123104,RESET_EVENT+0x1fe0000-RESET_TOFIELD-RESET_LEAVE,nil,1,a-e:GetHandler():GetDefense()) end
	end
end
function c12398.filter2(c)
	return c:IsFaceup() and c:IsCode(12399) 
end
