--Legendary Knight Critias
function c294.initial_effect(c)
	c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	local e001=Effect.CreateEffect(c)
	e001:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e001:SetType(EFFECT_TYPE_SINGLE)
	e001:SetCode(EFFECT_OVERINFINITE_ATTACK)
	c:RegisterEffect(e001)	
	local e002=Effect.CreateEffect(c)
	e002:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e002:SetType(EFFECT_TYPE_SINGLE)
	e002:SetCode(EFFECT_OVERINFINITE_DEFENSE)
	c:RegisterEffect(e002)	
		
	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(aux.FALSE)
	c:RegisterEffect(e1)

	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e0:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e0:SetCode(EVENT_ADJUST)
	e0:SetRange(LOCATION_MZONE)
	e0:SetOperation(c294.adjustop)
	c:RegisterEffect(e0)
	local g=Group.CreateGroup()
	g:KeepAlive()
	e0:SetLabelObject(g)

	--Activate Traps
	local e2=Effect.CreateEffect(c)
	e2:SetCategory(CATEGORY_REMOVE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e2:SetDescription(aux.Stringid(122600,0))
	e2:SetType(EFFECT_TYPE_QUICK_O)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_MZONE)
	--e2:SetCondition(c294.con)
	e2:SetTarget(c294.tg)
	e2:SetOperation(c294.op)
	e2:SetCountLimit(1)
	c:RegisterEffect(e2)

	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_CANNOT_DISABLE)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	  local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_DISEFFECT)
	c:RegisterEffect(e101)

	--特殊召唤不会被无效化
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e9)
end

function c294.adfilter(c)
return c:IsFaceup() and c:IsSetCard(0x900) and c:IsType(TYPE_FIELD)
end
function c294.adjustop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local pg=e:GetLabelObject()
	if c:GetFlagEffect(293)==0 then
		c:RegisterFlagEffect(293,RESET_EVENT+0x1ff0000,0,1)
		pg:Clear()
	end
	local g=Duel.GetMatchingGroup(c294.adfilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
	local dg=g:Filter(c294.adfilter,nil)
	if dg:GetCount()==0 or Duel.Destroy(dg,REASON_RULE)==0 then
		pg:Clear()
		pg:Merge(g)
	else
		pg:Clear()
		pg:Merge(g)
		Duel.Readjust()
	end
end

function c294.con(e,tp,eg,ep,ev,re,r,rp)
	return (Duel.IsExistingTarget(c294.filter1,tp,LOCATION_GRAVE,LOCATION_GRAVE,1,nil)
	and e:GetLabel()~=1 and e:GetHandler()==Duel.GetAttacker() and e:GetHandler():GetBattleTarget()~=nil)
	or (Duel.IsExistingTarget(c294.filter1,tp,LOCATION_GRAVE,LOCATION_GRAVE,1,nil)
	and e:GetLabel()~=1 and e:GetHandler()==Duel.GetAttackTarget() and e:GetHandler():GetBattleTarget()~=nil)
end
function c294.filter1(c,tp,ev)
   if not c:IsType(TYPE_TRAP) or not c:IsAbleToRemove() then return false end
   local tec=c.trap
   local actchk=0
   if not tec then return false end
   for _,te in ipairs(tec) do 
	  local chk,aeg,aep,aev,are,ar,arp=Duel.CheckEvent(te:GetCode(),true) 
	  if (chk or te:GetCode()==EVENT_FREE_CHAIN) 
	  and (te:GetCost()==nil or te:GetCost()(te,tp,aeg,aep,aev,are,ar,arp,0))
	  and (te:GetCondition()==nil or te:GetCondition()(te,tp,aeg,aep,aev,are,ar,arp)) and (te:GetTarget()==nil or te:GetTarget()(te,tp,aeg,aep,aev,are,ar,arp,0)) then actchk=1 break end 
   end
  return actchk==1
--c:CheckActivateEffect(false,false,false)~=nil 
end
function c294.tg(e,tp,eg,ep,ev,re,r,rp,chk)
	 if chk==0 then return Duel.IsExistingMatchingCard(c294.filter1,tp,LOCATION_GRAVE,LOCATION_GRAVE,1,nil,tp,ev) end 
	 Duel.SetOperationInfo(0,CATEGORY_REMOVE,nil,1,tp,LOCATION_GRAVE)
end
function c294.op(e,tp,eg,ep,ev,re,r,rp)
	 local c=e:GetHandler()
	 Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	 local tc=Duel.SelectMatchingCard(tp,c294.filter1,tp,LOCATION_GRAVE,LOCATION_GRAVE,1,1,nil,tp,ev):GetFirst()
	 if not tc then return end
	 local tec=tc.trap
	 if not tec then return end
	 local te=nil
	 local acd={}
	 local ac={}
	 for _,temp in ipairs(tec) do
		local tcost=temp:GetCost()
		local tg=temp:GetTarget()
		if (not tcost or tcost (temp,tp,Group.CreateGroup(),PLAYER_NONE,0,e,REASON_EFFECT,PLAYER_NONE,0)) 
		  and (not tg or tg(temp,tp,Group.CreateGroup(),PLAYER_NONE,0,teh,REASON_EFFECT,PLAYER_NONE,0)) then
				table.insert(ac,temp)
				table.insert(acd,temp:GetDescription())
		end
	  end
	  Duel.Hint(HINT_CARD,0,tc:GetCode())
	  if #ac==1 then te=ac[1] elseif #ac>1 then
			Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EFFECT)
			local opt=Duel.SelectOption(tp,table.unpack(acd))
			opt=opt+1
			te=ac[opt]
	  end
		if not te then return end
		Duel.BreakEffect()
		if #ac<=0 then return end
		local tcost=te:GetCost()
		if tcost then tcost (te,tp,Group.CreateGroup(),PLAYER_NONE,0,e,REASON_EFFECT,PLAYER_NONE,1) end
		local tg=te:GetTarget()  
		local op=te:GetOperation()
		if tg then tg(te,tp,Group.CreateGroup(),PLAYER_NONE,0,e,REASON_EFFECT,PLAYER_NONE,1) end
		Duel.BreakEffect()
		tc:CreateEffectRelation(te)
		Duel.BreakEffect()
		local g=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
		if g then
			local etc=g:GetFirst()
			while etc do
				etc:CreateEffectRelation(te)
				etc=g:GetNext()
			end
		end
		if op then op(te,tp,Group.CreateGroup(),PLAYER_NONE,0,e,REASON_EFFECT,PLAYER_NONE,1) end
		tc:ReleaseEffectRelation(te)
		if etc then 
			etc=g:GetFirst()
			while etc do
				etc:ReleaseEffectRelation(te)
				etc=g:GetNext()
			end
		end

   if Duel.Remove(tc,POS_FACEUP,REASON_EFFECT)>0 
	and (bit.band(te:GetType(),EFFECT_TYPE_ACTIVATE)~=0 or bit.band(te:GetType(),EFFECT_TYPE_IGNITION)~=0 or bit.band(te:GetType(),EFFECT_TYPE_QUICK_F)~=0 or bit.band(te:GetType(),EFFECT_TYPE_QUICK_O)~=0 or bit.band(te:GetType(),EFFECT_TYPE_TRIGGER_F)~=0 or bit.band(te:GetType(),EFFECT_TYPE_TRIGGER_O)~=0) 
	and te:GetCode()~=EVENT_FREE_CHAIN and op then
	local e2=Effect.CreateEffect(c)
	e2:SetCategory(te:GetCategory())
	e2:SetProperty(te:GetProperty()+EFFECT_FLAG_CANNOT_DISABLE)
	e2:SetDescription(te:GetDescription())
	if (bit.band(te:GetType(),EFFECT_TYPE_ACTIVATE)~=0 or bit.band(te:GetType(),EFFECT_TYPE_QUICK_F)~=0 or bit.band(te:GetType(),EFFECT_TYPE_QUICK_O)~=0) then
	e2:SetType(EFFECT_TYPE_QUICK_O)
	else e2:SetType(EFFECT_TYPE_TRIGGER_O) end
	e2:SetCode(te:GetCode())
	e2:SetRange(LOCATION_MZONE)
	if te:GetCondition() then e2:SetCondition(te:GetCondition()) end
	if te:GetCost() then e2:SetCost(te:GetCost()) end
	e2:SetTarget(c294.traptg)
	e2:SetLabelObject(te)
	e2:SetOperation(c294.trapop)
	e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	c:RegisterEffect(e2)
   end
end
function c294.traptg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	local te=e:GetLabelObject()
	local tg=te:GetTarget()
	if chk==0 then return (tg==nil or tg(te,tp,eg,ep,ev,re,r,rp,0)) and not e:GetHandler():IsStatus(STATUS_CHAINING) and not Duel.IsExistingMatchingCard(c294.trafilter,tp,LOCATION_MZONE,0,1,nil) end
	 if tg then tg(te,tp,eg,ep,ev,re,r,rp) end
end
function c294.trafilter(c)
	return c:GetFlagEffect(296)~=0
end
function c294.trapop(e,tp,eg,ep,ev,re,r,rp)
	local te=e:GetLabelObject()
	local op=te:GetOperation()
	if op then op(te,tp,eg,ep,ev,re,r,rp,1) end
	--e:GetHandler():CancelToGrave()
end