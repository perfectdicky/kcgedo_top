--Number CI1000: Numerronius Numerronia
local s, id = GetID()
function s.initial_effect(c)
	Xyz.AddProcedure(c,nil,13,5)
	c:EnableReviveLimit()

	--cannot destroyed
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e0:SetValue(s.indes)
	c:RegisterEffect(e0)

	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	--c:RegisterEffect(e1)

	  --special summon
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(87997872,0))
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetRange(LOCATION_EXTRA)
	e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	e2:SetCode(EVENT_TO_GRAVE)
	e2:SetCondition(s.spcon)
	e2:SetTarget(s.sptg)
	e2:SetOperation(s.spop)
	c:RegisterEffect(e2)

	--cannot attack
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_CANNOT_ATTACK)
	e3:SetCondition(s.value)
	c:RegisterEffect(e3)

	--indestructible
	local e4=Effect.CreateEffect(c) 
	e4:SetType(EFFECT_TYPE_SINGLE) 
	e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE) 
	e4:SetRange(LOCATION_MZONE) 
	e4:SetCode(EFFECT_INDESTRUCTABLE_EFFECT) 
	e4:SetValue(1) 
	c:RegisterEffect(e4) 

	--End Phase Win
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e5:SetRange(LOCATION_MZONE)
	e5:SetCode(EVENT_PHASE+PHASE_END)
	e5:SetCountLimit(1)
	e5:SetCondition(s.wincon)
	e5:SetOperation(s.winop)
	c:RegisterEffect(e5)

	--disable attack without Numeron Network
	local e6=Effect.CreateEffect(c)
	e6:SetDescription(aux.Stringid(84013237,0))
	e6:SetCategory(CATEGORY_RECOVER)
	e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCode(EVENT_ATTACK_ANNOUNCE)
	e6:SetCost(s.atkcost)
	e6:SetCondition(s.condition)
	e6:SetTarget(s.target)
	e6:SetOperation(s.atkop)
	c:RegisterEffect(e6,false,REGISTER_FLAG_DETACH_XMAT)

	--自己场上怪兽不能成为攻击目标
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_FIELD)
	e7:SetRange(LOCATION_MZONE)
	e7:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
	e7:SetTargetRange(LOCATION_MZONE,0)
	  e7:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e7:SetTarget(s.beatktg)
	e7:SetValue(Auxiliary.imval1)
	c:RegisterEffect(e7)

	--Negate the effect of All other cards you control
	--local e7=Effect.CreateEffect(c) 
	--e7:SetType(EFFECT_TYPE_FIELD) 
	----e7:SetCode(EFFECT_DISABLE) 
	--e7:SetRange(LOCATION_MZONE) 
	--e7:SetTargetRange(LOCATION_SZONE+LOCATION_MZONE,0) 
	--e7:SetTarget(s.distg) 
	--c:RegisterEffect(e7) 
end
s.xyz_number=1000

function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end

function s.value(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsCode(13716)
end

function s.desfilter(c)
	return c:IsFaceup() and (not c:IsSetCard(0x48) or c:IsSetCard(0x1048))
end
function s.descon(e)
	local c=e:GetHandler()
	return Duel.IsExistingMatchingCard(s.desfilter,c:GetControler(),0,LOCATION_MZONE,1,c)
end

function s.cfilter(c,tp,code)
	return c:IsCode(code) and c:GetPreviousControler()==tp and c:IsReason(REASON_DESTROY) and c:IsPreviousLocation(LOCATION_ONFIELD)
end
function s.spcon(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(s.cfilter,1,nil,tp,13715)
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCountFromEx(tp,tp,nil,e:GetHandler()
)>0
		and e:GetHandler():IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,true,false) end
	--if e:GetHandler():IsLocation(LOCATION_EXTRA) then
		--Duel.ConfirmCards(1-tp,e:GetHandler())
	--end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp)
	if e:GetHandler():IsRelateToEffect(e) and eg:IsExists(s.cfilter,1,nil,tp,13715) then
	local g=eg:Filter(s.cfilter,nil,tp,13715)
	e:GetHandler():SetMaterial(g)
	Duel.Overlay(e:GetHandler(),g)
	Duel.SpecialSummon(e:GetHandler(),SUMMON_TYPE_XYZ,tp,tp,true,false,POS_FACEUP)
	--Duel.SelectTarget(tp,s.filter3,tp,LOCATION_MZONE,0,1,1,nil)
	--local tc=Duel.GetFirstTarget()
	--local g2=Duel.SelectMatchingCard(tp,s.filter2,tp,LOCATION_GRAVE,0,1,1,nil)
	  --Duel.SpecialSummonComplete()
	e:GetHandler():CompleteProcedure() end
end
function s.filter2(c)
	return c:IsCode(13715)
end
function s.filter3(c)
	return c:IsCode(13716)
end

function s.wincon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()~=tp and Duel.GetActivityCount(1-tp,ACTIVITY_ATTACK)==0 
end
function s.winop(e,tp,eg,ep,ev,re,r,rp)
	local WIN_REASON_CiNo1000=0x52
	Duel.Win(tp,WIN_REASON_CiNo1000)
end

function s.afilter(c)
	return c:IsFaceup() and c:IsCode(41418852)
end
function s.condition(e,tp,eg,ep,ev,re,r,rp)
	return tp~=Duel.GetTurnPlayer() 
end
function s.atkcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local tg=Duel.GetAttacker()
	if chk==0 then return tg:IsOnField() end
	Duel.SetTargetCard(tg)
	local rec=tg:GetAttack()
	Duel.SetOperationInfo(0,CATEGORY_RECOVER,nil,0,tp,rec)
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc:IsFaceup() and tc:CanAttack() then
		if Duel.NegateAttack(tc) then
			Duel.Recover(tp,tc:GetAttack(),REASON_EFFECT)
		end
	end
end

function s.distg(e,c)
	return not c:IsCode(13716)
end

function s.beatktg(e,c)
	return c~=e:GetHandler()
end
