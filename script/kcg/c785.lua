--Go-DD World (K)
function c785.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c785.con)
	e1:SetOperation(c785.op)
	c:RegisterEffect(e1)   

	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCondition(c785.adcon)
	e2:SetOperation(c785.adop)
	e2:SetCode(EVENT_ADJUST)
	c:RegisterEffect(e2)
end

function c785.con(e,tp,eg,ep,ev,re,r,rp)
   return Duel.IsExistingMatchingCard(c785.usefilter,tp,LOCATION_ONFIELD,0,5,nil) 
end
function c785.usefilter2(c)
   return c:IsFaceup() and c:IsSetCard(0xaf) and not c:IsSetCard(0x20af)
end
function c785.usefilter(c)
   return c:IsFaceup() and c:IsSetCard(0xaf)
end
function c785.usefilter3(e,c)
   return c:IsFaceup() and c:IsSetCard(0xaf) and not c:IsSetCard(0x20af)
end
function c785.op(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if not Duel.IsExistingMatchingCard(c785.usefilter2,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil) then return end
	local g=Duel.GetMatchingGroup(c785.usefilter2,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
	for tc in aux.Next(g) do
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_ADD_SETCODE)
	e2:SetValue(0x20af)
	e2:SetReset(RESET_EVENT+0x1fe0000)
	tc:RegisterEffect(e2)
	end
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetReset(RESET_PHASE+PHASE_END)
	e1:SetTargetRange(0,LOCATION_MZONE)
	Duel.RegisterEffect(e1,tp)  
end

function c785.adcon(e,tp,eg,ep,ev,re,r,rp)
   return Duel.IsExistingMatchingCard(c785.usefilter2,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil) 
end
function c785.adop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if not Duel.IsExistingMatchingCard(c785.usefilter2,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil) then return end
	local g=Duel.GetMatchingGroup(c785.usefilter2,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
	for tc in aux.Next(g) do
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_ADD_SETCODE)
	e2:SetValue(0x20af)
	e2:SetReset(RESET_EVENT+0x1fe0000)
	tc:RegisterEffect(e2)
	end
end