--Orichalcos Shunoros
function c12399.initial_effect(c)
	c:EnableReviveLimit()

	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(aux.FALSE)
	c:RegisterEffect(e1)

	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_REMOVE_TYPE)
	e0:SetValue(TYPE_FUSION)
	c:RegisterEffect(e0)

	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_SET_ATTACK)
	e2:SetValue(c12399.atk)
	c:RegisterEffect(e2)

	--spsummon Divine Sepent
	local e3=Effect.CreateEffect(c)
	e3:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e3:SetDescription(aux.Stringid(12399,0))
	e3:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e3:SetCode(EVENT_DESTROYED)
	e3:SetCondition(c12399.spcon)
	e3:SetTarget(c12399.sptg)
	e3:SetOperation(c12399.spop)
	c:RegisterEffect(e3)

	--Summon
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(12399,0))
	e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e4:SetType(EFFECT_TYPE_TRIGGER_F+EFFECT_TYPE_SINGLE)
	e4:SetCode(EVENT_SPSUMMON_SUCCESS)
	e4:SetTarget(c12399.tg)
	e4:SetOperation(c12399.op)
	c:RegisterEffect(e4)	

	--ATK goes down
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e6:SetCode(EVENT_BATTLED)
	e6:SetCondition(c12399.statcon)
	e6:SetOperation(c12399.statop)
	c:RegisterEffect(e6)

	--DEF goes down
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e7:SetCode(EVENT_BATTLED)
	e7:SetCondition(c12399.statcon2)
	e7:SetOperation(c12399.statop2)
	c:RegisterEffect(e7)

	local e08=Effect.CreateEffect(c)
	e08:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e08:SetCode(EVENT_SPSUMMON_SUCCESS)
	e08:SetOperation(c12399.saveatk)
	c:RegisterEffect(e08)

	--selfdestroy
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e8:SetRange(LOCATION_ONFIELD)
	e8:SetCode(EFFECT_SELF_DESTROY)
	e8:SetLabelObject(e08)
	e8:SetCondition(c12399.descon)
	c:RegisterEffect(e8)

	  --protected card damage effects
	  local e9=Effect.CreateEffect(c)
	  e9:SetType(EFFECT_TYPE_FIELD) 
	  e9:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	  e9:SetRange(LOCATION_ONFIELD)  
	  e9:SetTargetRange(LOCATION_ONFIELD,0) 
	  e9:SetTarget(c12399.indes) 
	  e9:SetValue(1) 
	  c:RegisterEffect(e9)
end
c12399.listed_names={12398,123100,82103466}

function c12399.atk(e,c)
	if e:GetHandler():GetFlagEffectLabel(123104) then return e:GetHandler():GetFlagEffectLabel(123104)
	else return 0 end
end

function c12399.indes(e,c)
		return c:IsFaceup() and c:IsCode(12398,123100)
end

function c12399.spcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return c:IsPreviousPosition(POS_FACEUP) and Duel.GetLP(tp)>=10000 
end

function c12399.spfilter(c,e,tp)
	return c:IsCode(82103466) and c:IsCanBeSpecialSummoned(e,0,tp,true,false)
	and Duel.GetLocationCountFromEx(tp,tp,nil,c)>0 
end
function c12399.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c12399.spfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c12399.spop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c12399.spfilter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	if g:GetCount()~=0 then
		Duel.SpecialSummon(g,0,tp,tp,true,false,POS_FACEUP)
	end
end

function c12399.spfilter2(c,e,tp)
	return c:IsCode(123100) and c:IsCanBeSpecialSummoned(e,0,tp,true,false)
end
function c12399.spfilter3(c,e,tp)
	return c:IsCode(12398) and c:IsCanBeSpecialSummoned(e,0,tp,true,false)
end
function c12399.tg(e,tp,eg,ep,ev,re,r,rp,chk)
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	if chk==0 then return (not ect or ect>=2) and Duel.IsExistingMatchingCard(c12399.spfilter2,tp,LOCATION_EXTRA,0,1,nil,e,tp)
		and Duel.IsExistingMatchingCard(c12399.spfilter3,tp,LOCATION_EXTRA,0,1,nil,e,tp) 
		and Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION)>1 
		and not Duel.IsPlayerAffectedByEffect(tp,59822133) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,2,tp,LOCATION_EXTRA)
end
function c12399.op(e,tp,eg,ep,ev,re,r,rp)
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	if Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION)<2 then return end
	if Duel.IsPlayerAffectedByEffect(tp,59822133) or (ect and ect<2)  then return end   
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g1=Duel.SelectMatchingCard(tp,c12399.spfilter2,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	if g1:GetCount()>0 then Duel.SpecialSummon(g1:GetFirst(),0,tp,tp,true,false,POS_FACEUP) g1:GetFirst():CompleteProcedure() end
	  Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g2=Duel.SelectMatchingCard(tp,c12399.spfilter3,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	if g2:GetCount()>0 then Duel.SpecialSummon(g2:GetFirst(),0,tp,tp,true,false,POS_FACEUP) g2:GetFirst():CompleteProcedure() end
end

function c12399.statcon(e,tp,eg,ep,ev,re,r,rp)
local c=e:GetHandler()
local a=Duel.GetAttacker()
local b=Duel.GetAttackTarget()
if c==nil or a==nil or b==nil then return end
return c==a and (b:GetPosition()==POS_FACEUP_ATTACK or b:GetPosition()==POS_FACEDOWN_ATTACK)
--else if c==b then return a:GetPosition()==POS_FACEUP_ATTACK or a:GetPosition()==POS_FACEDOWN_ATTACK
--end
--end
end
function c12399.statop(e,tp,eg,ep,ev,re,r,rp)
	local bc=e:GetHandler():GetBattleTarget()
	local atk=bc:GetAttack()
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(-atk)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		e:GetHandler():RegisterEffect(e1)
end
function c12399.statcon2(e,tp,eg,ep,ev,re,r,rp)
local c=e:GetHandler()
local a=Duel.GetAttacker()
local b=Duel.GetAttackTarget()
if c==nil or a==nil or b==nil then return end
return c==a and (b:GetPosition()==POS_FACEUP_DEFENSE or b:GetPosition()==POS_FACEDOWN_DEFENSE)
--else if c==b then return a:GetPosition()==POS_FACEUP_DEFENSE or a:GetPosition()==POS_FACEDOWN_DEFENSE
--end
--end
end
function c12399.statop2(e,tp,eg,ep,ev,re,r,rp)
	local bc=e:GetHandler():GetBattleTarget()
	local def=bc:GetDefense()
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(-def)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		e:GetHandler():RegisterEffect(e1)
end

function c12399.desfilter(c)
	return c:IsCode(12399) 
end

function c12399.saveatk(e,tp,eg,ep,ev,re,r,rp)
	e:SetLabel(c123104[1])
	e:GetHandler():RegisterFlagEffect(123104,RESET_EVENT+0x1fe0000,0,1,c123104[1])
end
function c12399.descon(e)
	return ((e:GetHandler():GetFlagEffectLabel(123104) and e:GetHandler():GetFlagEffectLabel(123104)<1 and e:GetHandler():IsLocation(LOCATION_SZONE)) 
	or (e:GetHandler():GetAttack()==0 and e:GetHandler():IsLocation(LOCATION_MZONE)))
	and e:GetLabelObject():GetLabel()>0
end
