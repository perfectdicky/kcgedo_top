--Legendary Knight Hermos
function c295.initial_effect(c)
	c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	local e001=Effect.CreateEffect(c)
	e001:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e001:SetType(EFFECT_TYPE_SINGLE)
	e001:SetCode(EFFECT_OVERINFINITE_ATTACK)
	c:RegisterEffect(e001)	
	local e002=Effect.CreateEffect(c)
	e002:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e002:SetType(EFFECT_TYPE_SINGLE)
	e002:SetCode(EFFECT_OVERINFINITE_DEFENSE)
	c:RegisterEffect(e002)	

	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(aux.FALSE)
	c:RegisterEffect(e1)

	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e0:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e0:SetCode(EVENT_ADJUST)
	e0:SetRange(LOCATION_MZONE)
	e0:SetOperation(c295.adjustop)
	c:RegisterEffect(e0)
	local g=Group.CreateGroup()
	g:KeepAlive()
	e0:SetLabelObject(g)

	  --Absorb Monster effects
	  local e2=Effect.CreateEffect(c)
	  e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	  e2:SetDescription(aux.Stringid(2407234,1))
	  e2:SetType(EFFECT_TYPE_QUICK_O)
	  e2:SetCode(EVENT_FREE_CHAIN)
	  e2:SetRange(LOCATION_MZONE)
	  e2:SetCountLimit(1)
	  e2:SetCost(c295.ccost)
	  e2:SetOperation(c295.cop)
	  c:RegisterEffect(e2)

	--Redirect attack
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(69937550,1))
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e3:SetCode(EVENT_BE_BATTLE_TARGET)
	e3:SetRange(LOCATION_MZONE)
	--e3:SetCountLimit(1)
	e3:SetCondition(c295.cbcon)
	--e3:SetTarget(c295.cbtg)
	e3:SetOperation(c295.cbop)
	c:RegisterEffect(e3)

	--Divert and multiply
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(10000010,0))
	e4:SetCategory(CATEGORY_ATKCHANGE)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e4:SetCode(EVENT_BE_BATTLE_TARGET)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCountLimit(1)
	e4:SetCondition(c295.pbcon)
	e4:SetTarget(c295.pbtg)
	e4:SetCost(c295.pbcost)
	e4:SetOperation(c295.pbop)
	c:RegisterEffect(e4)

	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_CANNOT_DISABLE)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_DISEFFECT)
	c:RegisterEffect(e101)
	--特殊召唤不会被无效化
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e9)
end

function c295.adfilter(c)
return c:IsFaceup() and c:IsSetCard(0x900) and c:IsType(TYPE_FIELD)
end
function c295.adjustop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local pg=e:GetLabelObject()
	if c:GetFlagEffect(293)==0 then
		c:RegisterFlagEffect(293,RESET_EVENT+0x1ff0000,0,1)
		pg:Clear()
	end
	local g=Duel.GetMatchingGroup(c295.adfilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
	local dg=g:Filter(c295.adfilter,nil)
	if dg:GetCount()==0 or Duel.Destroy(dg,REASON_RULE)==0 then
		pg:Clear()
		pg:Merge(g)
	else
		pg:Clear()
		pg:Merge(g)
		Duel.Readjust()
	end
end

function c295.ccost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c295.sfilter,tp,LOCATION_GRAVE,0,1,nil) end
	local tc=Duel.SelectMatchingCard(tp,c295.sfilter,tp,LOCATION_GRAVE,0,1,1,nil)
	e:SetLabelObject(tc:GetFirst())
	Duel.Remove(tc,POS_FACEUP,REASON_COST)
end
function c295.sfilter(c)
	return c:IsType(TYPE_EFFECT) and c:IsAbleToRemoveAsCost()
end
function c295.cop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=e:GetLabelObject()
	if c:IsRelateToEffect(e) and c:IsFaceup() then
		local code=tc:GetOriginalCode()
		local reset_flag=RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END+RESET_SELF_TURN
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_CODE)
		e1:SetValue(tc:GetCode())
		e1:SetReset(reset_flag)
		c:RegisterEffect(e1)		
		c:CopyEffect(code, reset_flag, 1)
	end
end 

function c295.cbcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bt=eg:GetFirst()
	return c~=bt and bt:GetControler()==c:GetControler() 
	and c:GetFlagEffect(294)==0 
	and c:GetFlagEffect(725)==0
end
function c295.cbtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	e:GetHandler():RegisterFlagEffect(294,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1)
end
function c295.cbop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tg2=Duel.GetAttackTarget()
	local tg=Duel.GetAttacker()	
	if not (tg:IsOnField() and not tg:IsStatus(STATUS_ATTACK_CANCELED)) or not tg2:IsOnField() then return end		
	-- Duel.HintSelection(Group.FromCards(c))
	-- Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	-- local g=Duel.SelectMatchingCard(tp,Card.IsFaceup,tp,LOCATION_MZONE,0,1,1,tg2)
	Duel.ChangeAttackTarget(c)
	-- if c:GetControler()==Duel.GetAttacker():GetControler() then 
	--   local e4=Effect.CreateEffect(c)
	--   e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DELAY)
	--   e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	--   e4:SetCode(EVENT_BATTLE_CONFIRM)
	--   e4:SetOperation(function(...) 
	--   if c==Duel.GetAttackTarget() then
	--   local e5=Effect.CreateEffect(c)
	--   e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DELAY+EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	--   e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	--   e5:SetCode(EVENT_BATTLED)
	--   e5:SetCountLimit(1)
	--   e5:SetOperation(function(...) Duel.ChangeBattleDamage(tp,0) Duel.ChangeBattleDamage(1-tp,0) e5:Reset() end)
	--   e5:SetReset(RESET_PHASE+PHASE_BATTLE)
	--   Duel.RegisterEffect(e5,tp)  
	--   Duel.ChangeBattleDamage(tp,0) Duel.ChangeBattleDamage(1-tp,0)
	--   Duel.CalculateDamage(Duel.GetAttacker(),c,false)
	--   end 
	--   e4:Reset() end)
	--   e4:SetCountLimit(1)
	--   e4:SetReset(RESET_PHASE+PHASE_BATTLE)
	--   Duel.RegisterEffect(e4,tp)  
	-- end
end

function c295.pbcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler()==Duel.GetAttackTarget() or (eg~=nil and e:GetHandler()==eg:GetFirst())
end
function c295.cfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsAbleToRemoveAsCost()
end
function c295.pbcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c295.cfilter,tp,LOCATION_GRAVE,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g=Duel.SelectMatchingCard(tp,c295.cfilter,tp,LOCATION_GRAVE,0,1,99,nil)
	e:SetLabel(g:GetCount())
	Duel.Remove(g,POS_FACEUP,REASON_COST)
end
function c295.pbtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_ATKCHANGE,e:GetHandler(),1,0,0)
	e:GetHandler():RegisterFlagEffect(296,RESET_EVENT+0x1fe0000+RESET_CHAIN,0,1)
end
function c295.pbop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	c:ResetFlagEffect(296)
	if c:IsFacedown() or c:GetAttack()<1 then return end
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetValue(c:GetAttack()*e:GetLabel())
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	c:RegisterEffect(e1)

	if c:GetFlagEffect(295)==0 then
		local e4=Effect.CreateEffect(c)
		e4:SetDescription(aux.Stringid(10000010,0))
		e4:SetCategory(CATEGORY_ATKCHANGE)
		e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
		e4:SetCode(EVENT_BE_BATTLE_TARGET)
		e4:SetRange(LOCATION_MZONE)
		e4:SetCondition(c295.pbcon)
		e4:SetTarget(c295.pbtg)
		e4:SetLabel(e:GetLabel())
		e4:SetOperation(c295.pbop)
		e4:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_DAMAGE)
		c:RegisterEffect(e4) 
	end
	c:RegisterFlagEffect(295,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_DAMAGE,0,1)
	if c:GetFlagEffect(295)>2 and e:GetLabel()>1 then
		Duel.Hint(HINT_MESSAGE,tp,aux.Stringid(519,0))
		local e1=Effect.CreateEffect(c)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_SET_ATTACK)
		e1:SetValue(1000000)
		e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e1)
    end

	if c:GetFlagEffect(294)==0 and Duel.SelectYesNo(tp,aux.Stringid(69937550,1)) then 
		c:RegisterFlagEffect(725,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1)
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
		local g=Duel.SelectMatchingCard(tp,Card.IsFaceup,tp,LOCATION_MZONE,0,1,1,c)
		local tc=g:GetFirst()
		if tc then 
			Duel.HintSelection(g) 
			Duel.ChangeAttackTarget(tc)		
	--   if tc:GetControler()==Duel.GetAttacker():GetControler() then 
	-- 	  --Duel.RaiseEvent(tc,EVENT_BE_BATTLE_TARGET,e,REASON_EFFECT,tp,tp,0) 
	--   local e4=Effect.CreateEffect(c)
	--   e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DELAY)
	--   e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	--   e4:SetCode(EVENT_BATTLE_CONFIRM)
	--   e4:SetOperation(function(...) 
	--   if tc==Duel.GetAttackTarget() then
	--   local e5=Effect.CreateEffect(c)
	--   e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DELAY+EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	--   e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	--   e5:SetCode(EVENT_BATTLED)
	--   e5:SetCountLimit(1)
	--   e5:SetOperation(function(...) Duel.ChangeBattleDamage(tp,0) Duel.ChangeBattleDamage(1-tp,0) e5:Reset() end)
	--   e5:SetReset(RESET_PHASE+PHASE_BATTLE)
	--   Duel.RegisterEffect(e5,tp)  
	--   Duel.ChangeBattleDamage(tp,0) Duel.ChangeBattleDamage(1-tp,0)
	--   Duel.CalculateDamage(Duel.GetAttacker(),tc,false)
	--   end 
	--   e4:Reset() end)
	--   e4:SetCountLimit(1)
	--   e4:SetReset(RESET_PHASE+PHASE_BATTLE)
	--   Duel.RegisterEffect(e4,tp)
		 -- end
		 end
	end
end
