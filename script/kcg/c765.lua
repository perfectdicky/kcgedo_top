--Darkness 逆轉命運 (KA)
function c765.initial_effect(c)
	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_SINGLE_RANGE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_CHANGE_TYPE)
	e0:SetRange(LOCATION_SZONE)
	e0:SetValue(TYPE_CONTINUOUS+TYPE_TRAP)
	c:RegisterEffect(e0)	

	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c765.destg)
	e1:SetOperation(c765.desop)
	c:RegisterEffect(e1)
end

function c765.destg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	--if chkc then return true end
	--local g=Duel.GetFieldGroup(1-tp,LOCATION_GRAVE,0)
	if chk==0 then return true end
		--g:GetCount()>0 end
	-- if g:GetCount()>0 and e:GetHandler():GetFlagEffect(765)~=0 then
	-- 	e:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)	
	-- 	Duel.SetTargetCard(g:GetFirst())
	-- end
end
function c765.ofilter(c)
	return c:IsType(TYPE_SPELL) or c:IsType(TYPE_TRAP)
end
function c765.darkness(c)
	return c:IsFaceup() and c:GetFlagEffect(765)~=0 and c:IsType(TYPE_TRAP+TYPE_CONTINUOUS) and c:IsSetCard(0x316)
end
function c765.desop(e,tp,eg,ep,ev,re,r,rp)
	if e:GetHandler():GetFlagEffect(765)==0 then return end
	local sqc=Duel.GetMatchingGroup(c765.darkness,tp,LOCATION_SZONE,0,e:GetHandler())
	for i=1,sqc:GetCount() do
	if Duel.IsExistingMatchingCard(c765.ofilter,tp,LOCATION_DECK+LOCATION_HAND,0,1,nil) and Duel.SelectYesNo(tp,aux.Stringid(12744567,0)) then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
		local og=Duel.SelectMatchingCard(tp,c765.ofilter,tp,LOCATION_DECK+LOCATION_HAND,0,1,1,nil):GetFirst()
		if og and not og:IsImmuneToEffect(e) then
			Duel.Overlay(e:GetHandler(),og)
	    end
	end
	end	
	
	-- local tc=Duel.GetFirstTarget()
	-- if tc and tc:IsRelateToEffect(e) then
	-- 	local e1=Effect.CreateEffect(e:GetHandler())
	-- 	e1:SetType(EFFECT_TYPE_SINGLE)
	-- 	e1:SetCode(EFFECT_FORBIDDEN)
	-- 	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	-- 	tc:RegisterEffect(e1)
	-- end

	-- local g=Duel.GetMatchingGroup(c765.darkness,tp,LOCATION_SZONE,0,e:GetHandler())
	-- if g:GetCount()>0 then
	-- 	while g:GetCount()>0 do
	-- 	   local tc=g:GetFirst()
	-- 	   c765.zero(e:GetHandler(),e,tp)
	-- 	   g:RemoveCard(tc)
	--     end
	-- end	
end

function c765.zero(tc,e,tep)
	local te=tc:GetActivateEffect()
	if te==nil or tc:CheckActivateEffect(true,false,false)==nil then return end
	local condition=te:GetCondition()
	local cost=te:GetCost()
	local target=te:GetTarget()
	local operation=te:GetOperation()
	e:SetProperty(te:GetProperty())
	Duel.ClearTargetCard()
	if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
	if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
	tc:CreateEffectRelation(te)
	local gg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	if gg then  
		local etc=gg:GetFirst()	
		while etc do
			etc:CreateEffectRelation(te)
			etc=gg:GetNext()
		end
	end						
	if operation then 
	local tc=Duel.GetFirstTarget()
	if tc and tc:IsRelateToEffect(e) then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_FORBIDDEN)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		tc:RegisterEffect(e1)
	end
	end
	tc:ReleaseEffectRelation(te)					
	if gg then  
		local etc=gg:GetFirst()												 
		while etc do
			etc:ReleaseEffectRelation(te)
			etc=gg:GetNext()
		end
	end 
end