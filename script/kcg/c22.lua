--ユベル－Das Extremer Traurig Drachen
function c22.initial_effect(c)
	c:EnableReviveLimit()   

	  --battle
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_REFLECT_BATTLE_DAMAGE)
	e1:SetValue(1)
	c:RegisterEffect(e1)

	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_REMOVE_TYPE)
	e0:SetValue(TYPE_FUSION)
	c:RegisterEffect(e0)

	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e2:SetValue(1)
	c:RegisterEffect(e2)

	local e32=Effect.CreateEffect(c)
	  e32:SetDescription(aux.Stringid(31764700,0))
	e32:SetCategory(CATEGORY_DESTROY)
	e32:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e32:SetRange(LOCATION_MZONE)
	e32:SetCode(EVENT_PRE_BATTLE_DAMAGE)
	  e32:SetTarget(c22.rdtg)
	e32:SetOperation(c22.rdop)
	c:RegisterEffect(e32)

	--destroy 
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_DESTROY) 
	e4:SetDescription(aux.Stringid(4779091,1)) 
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F) 
	e4:SetRange(LOCATION_MZONE) 
	e4:SetCountLimit(1) 
	e4:SetProperty(EFFECT_FLAG_REPEAT+EFFECT_FLAG_CARD_TARGET) 
	e4:SetCode(EVENT_PHASE+PHASE_END) 
	e4:SetCondition(c22.descon) 
	e4:SetTarget(c22.destg)
	e4:SetOperation(c22.desop)
	c:RegisterEffect(e4) 
 
	--cannot special summon
	local e5=Effect.CreateEffect(c)
	e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetCode(EFFECT_SPSUMMON_CONDITION)
	e5:SetValue(aux.FALSE)
	c:RegisterEffect(e5)
end

function c22.damop2(e,tp,eg,ep,ev,re,r,rp)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_REFLECT_BATTLE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1,0)
	e1:SetReset(RESET_PHASE+PHASE_DAMAGE_CAL)
	Duel.RegisterEffect(e1,tp)
end

function c22.rdtg(e,tp,eg,ep,ev,re,r,rp,chk)
	  local atker=e:GetHandler():GetBattleTarget()
	if chk==0 then return atker~=nil and atker:IsOnField() and not atker:IsStatus(STATUS_BATTLE_DESTROYED) end
	  Duel.SetOperationInfo(0,CATEGORY_DESTROY,atker,1,0,0) 
end
function c22.rdop(e,tp,eg,ep,ev,re,r,rp)
	  local atker=e:GetHandler():GetBattleTarget()
	  if atker~=nil and atker:IsOnField() and not atker:IsStatus(STATUS_BATTLE_DESTROYED) then
	Duel.Destroy(atker,REASON_EFFECT) end
end

function c22.descon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()==tp
end
function c22.sfilter(c)
	return c:IsOnField() 
end
function c22.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end  
	  local g1count=Duel.GetMatchingGroupCount(c22.sfilter,tp,LOCATION_MZONE,0,e:GetHandler())
		local g2count=Duel.GetMatchingGroupCount(c22.sfilter,1-tp,LOCATION_MZONE,0,e:GetHandler())
	  local count=g1count+g2count
	  if g1count<g2count then count=g1count*2 end
	  Duel.SetOperationInfo(0,CATEGORY_DESTROY,nil,count,0,0) 
end
function c22.desop(e,tp,eg,ep,ev,re,r,rp)
		local g1=Duel.GetMatchingGroup(c22.sfilter,tp,LOCATION_MZONE,0,e:GetHandler())
	  if g1~=nil then
	if Duel.Destroy(g1,REASON_EFFECT)~=0 then
	  g1=Duel.GetOperatedGroup()
	  Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	  local g2=Duel.SelectMatchingCard(tp,c22.sfilter,tp,0,LOCATION_MZONE,g1:GetCount(),g1:GetCount(),nil)
	  if g2~=nil then
	Duel.Destroy(g2,REASON_EFFECT) end end end
end

function c22.batop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	if bc and c:IsAttackPos() then
		e:SetLabel(bc:GetAttack())
		e:SetLabelObject(bc)
	else
		e:SetLabelObject(nil)
	end
end

function c22.damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	local bc=e:GetLabelObject():GetLabelObject()
	if chk==0 then return bc end
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,e:GetLabelObject():GetLabel())
	if baux.TRUE then
		Duel.SetOperationInfo(0,CATEGORY_DESTROY,bc,1,0,0)
	end
end
function c22.damop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Damage(1-tp,e:GetLabelObject():GetLabel(),REASON_EFFECT)
	local bc=e:GetLabelObject():GetLabelObject()
	if bc:IsRelateToBattle() then
		Duel.Destroy(bc,REASON_EFFECT)
	end
end
