--Ritual Refinement
function c373.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c373.target)
	e1:SetOperation(c373.activate)
	c:RegisterEffect(e1)
end
function c373.filter2(c)
	return c:IsSetCard(0x909) and c:IsFaceup()
end
function c373.filter(c,e,tp)
	return c:IsSetCard(0x909) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function c373.target(e,tp,eg,ep,ev,re,r,rp,chk)
      local count=Duel.GetMatchingGroupCount(c373.filter2,tp,LOCATION_MZONE,0,nil)
      local gcount=count*2
      local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if chk==0 then return ft>0 and count>0 and not Duel.IsPlayerAffectedByEffect(tp,59822133)
	and Duel.IsExistingMatchingCard(c373.filter,tp,LOCATION_DECK,0,1,nil,e,tp) end
      if gcount>ft then gcount=ft end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,gcount,tp,LOCATION_DECK)
      e:SetLabel(gcount)
end
function c373.activate(e,tp,eg,ep,ev,re,r,rp)
      local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	if Duel.IsPlayerAffectedByEffect(tp,59822133) then ft=1 end		  			
      local gcount=e:GetLabel()
      if gcount>ft then gcount=ft end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c373.filter,tp,LOCATION_DECK,0,1,gcount,nil,e,tp)
	if g:GetCount()>0 then
		Duel.SpecialSummon(g,0,tp,tp,false,false,POS_FACEUP)
	end
end
