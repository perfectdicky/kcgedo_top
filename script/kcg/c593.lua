--RUM－レイド・フォース
function c593.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c593.target)
	e1:SetOperation(c593.activate)
	c:RegisterEffect(e1)

	--salvage
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(94220427,0))
	e2:SetCategory(CATEGORY_TOHAND)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_SPSUMMON_SUCCESS)
	e2:SetRange(LOCATION_GRAVE)
	e2:SetCondition(c593.thcon)
	e2:SetTarget(c593.thtg)
	e2:SetOperation(c593.thop)
	c:RegisterEffect(e2)
end

function c593.filter1(c,e,tp,mc)
	return c:IsType(TYPE_XYZ) and mc:IsCanBeXyzMaterial(c,tp) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
	and c:GetRank()==mc:GetRank()+10 and c:IsSetCard(0x1048) and c:IsSetCard(0x14b)
	and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)	
	and Duel.GetLocationCountFromEx(tp,tp,mc,c)>0
end
function c593.filter2(c,e,tp)
	return c:GetRank()==1 and c:IsType(TYPE_XYZ) 
	and Duel.IsExistingMatchingCard(c593.filter1,tp,LOCATION_EXTRA,0,1,nil,e,tp,c)
	and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
end

function c593.filter3(c,e,tp,mc)
	return c:IsCanBeFusionMaterial(mc) and c:IsFaceup() and c:IsSetCard(0x914) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL) 
end
function c593.filter4(c,e,tp)
	local loc=LOCATION_MZONE
	local ce=Duel.GetChainMaterial(tp)
	if ce~=nil then
		local fgroup=ce:GetTarget()
		local mg2=fgroup(ce,e,tp)
		local mf=ce:GetValue()
		loc=loc+LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED
	end
	return c:IsCode(602) and Duel.IsExistingMatchingCard(c593.filter3,tp,loc,0,2,nil,e,tp,c)
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,true)
end

function c593.filter5(c,e,tp,mc)
	return c:IsCanBeFusionMaterial(mc) and c:IsFaceup() and c:IsCode(602) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
		   and Duel.IsExistingMatchingCard(c593.filter52,tp,LOCATION_MZONE,0,1,c,e,tp,mc)
end
function c593.filter52(c,e,tp,mc)
	return c:IsCanBeFusionMaterial(mc) and c:IsFaceup() and c:IsSetCard(0x914) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
end
function c593.filter6(c,e,tp)
	local loc=LOCATION_MZONE
	local ce=Duel.GetChainMaterial(tp)
	if ce~=nil then
		local fgroup=ce:GetTarget()
		local mg2=fgroup(ce,e,tp)
		local mf=ce:GetValue()
		loc=loc+LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED
	end
	return c:IsCode(603) and Duel.IsExistingMatchingCard(c593.filter5,tp,loc,0,1,nil,e,tp,c)
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,true)
end

function c593.filter7(c,e,tp,mc)
	return c:IsCanBeFusionMaterial(mc) and c:IsFaceup() and c:IsSetCard(0x914) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
end
function c593.filter8(c,e,tp)
	local loc=LOCATION_MZONE
	local ce=Duel.GetChainMaterial(tp)
	if ce~=nil then
		local fgroup=ce:GetTarget()
		local mg2=fgroup(ce,e,tp)
		local mf=ce:GetValue()
		loc=loc+LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED
	end
	return c:IsCode(612) and Duel.IsExistingMatchingCard(c593.filter7,tp,loc,0,3,nil,e,tp,c)
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,true)
end
function c593.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local res=(Duel.IsExistingMatchingCard(c593.filter2,tp,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,nil,e,tp)
	if chk==0 then return res end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,0)
end
function c593.filter0(c,e,tp)
	return ((c:IsCode(612) and Duel.IsExistingMatchingCard(c593.filter7,tp,LOCATION_MZONE,0,3,nil,e,tp,c))
    or (c:IsCode(603) and Duel.IsExistingMatchingCard(c593.filter5,tp,LOCATION_MZONE,0,1,nil,e,tp,c))
    or (c:IsCode(602) and Duel.IsExistingMatchingCard(c593.filter3,tp,LOCATION_MZONE,0,2,nil,e,tp,c))
    or (c:IsCode(594) and Duel.IsExistingMatchingCard(c593.filter1,tp,LOCATION_MZONE,0,2,nil,e,tp,c)))
	and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,true)
end
function c593.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if not (Duel.IsExistingMatchingCard(c593.filter2,tp,LOCATION_EXTRA,0,1,nil,e,tp)
		-- or Duel.IsExistingMatchingCard(c593.filter4,tp,LOCATION_HAND+LOCATION_DECK,0,1,nil,e,tp)
		-- or Duel.IsExistingMatchingCard(c593.filter6,tp,LOCATION_HAND+LOCATION_DECK,0,1,nil,e,tp)
		-- or Duel.IsExistingMatchingCard(c593.filter8,tp,LOCATION_HAND+LOCATION_DECK,0,1,nil,e,tp)) 
		then return end
	-- local loc=LOCATION_MZONE
	-- local ce=Duel.GetChainMaterial(tp)
	-- if ce~=nil then
	-- 	local fgroup=ce:GetTarget()
	-- 	local mg2=fgroup(ce,e,tp)
	-- 	local mf=ce:GetValue()
	-- 	loc=loc+LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED
	-- end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c593.filter2,tp,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,1,nil,e,tp)	
	-- local g=Duel.SelectMatchingCard(tp,c593.filter0,tp,LOCATION_EXTRA+LOCATION_HAND+LOCATION_DECK,0,1,1,nil,e,tp)
	if g:GetCount()<1 then return end
	local sc=g:GetFirst()
	Duel.SpecialSummon(sc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
	sc:CompleteProcedure()	
	Duel.BreakEffect()
	local xyzg=Duel.GetMatchingGroup(c593.filter1,tp,LOCATION_EXTRA,0,nil,e,tp,sc)
	if xyzg:GetCount()>0 then	
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local xc=xyzg:Select(tp,1,1,nil):GetFirst()
		xc:SetMaterial(g)
		Duel.Overlay(xc,g)
		Duel.SpecialSummon(xc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
		xc:CompleteProcedure() 	
		local de=Effect.CreateEffect(c)
		de:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
		de:SetCategory(CATEGORY_REMOVE)
		de:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
		de:SetRange(LOCATION_MZONE)
		de:SetCode(EVENT_PHASE+PHASE_END)
		de:SetCountLimit(1)
		de:SetTarget(c593.rmtg)
		de:SetOperation(c593.rmop)
		de:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		fc:RegisterEffect(de,true)		
	end
	-- if sc:IsCode(594) then
	-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
	-- 	local sg=Duel.SelectMatchingCard(tp,c593.filter1,tp,loc,0,2,5,nil,e,tp,sc)
	-- 	if not aux.MustMaterialCheck(sg,tp,EFFECT_MUST_BE_XMATERIAL) then return end
	-- 	local ssc=sg:GetFirst()
	-- 	while ssc do
	-- 	local mg=ssc:GetOverlayGroup()
	-- 	if mg:GetCount()~=0 then
	-- 		Duel.Overlay(sc,mg)
	-- 	end
	-- 	ssc=sg:GetNext() end
	-- 	sc:SetMaterial(sg)
	-- 	Duel.Overlay(sc,sg)
	-- 	Duel.SpecialSummon(sc,SUMMON_TYPE_FUSION,tp,tp,true,true,POS_FACEUP)
	-- 	sc:CompleteProcedure()
	-- end

	-- if sc:IsCode(602) then
	-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL) 
	-- 	local sg=Duel.SelectMatchingCard(tp,c593.filter3,tp,loc,0,2,2,nil,e,tp,sc)
	-- 	sc:SetMaterial(sg)
	-- 	Duel.SendtoGrave(sg,REASON_RULE+REASON_FUSION+REASON_MATERIAL)
	-- 	Duel.SpecialSummon(sc,SUMMON_TYPE_FUSION,tp,tp,true,true,POS_FACEUP)
	-- 	sc:CompleteProcedure()
	-- end

	-- if sc:IsCode(603) then
	-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL) 
	-- 	local sg=Duel.SelectMatchingCard(tp,c593.filter5,tp,loc,0,1,1,nil,e,tp,sc)
	-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL) 
	-- 	local sg2=Duel.SelectMatchingCard(tp,c593.filter52,tp,loc,0,1,1,sg:GetFirst(),e,tp,sc)
	-- 				   sg:Merge(sg2)
	-- 	sc:SetMaterial(sg)
	-- 	Duel.SendtoGrave(sg,REASON_RULE+REASON_FUSION+REASON_MATERIAL)
	-- 	Duel.SpecialSummon(sc,SUMMON_TYPE_FUSION,tp,tp,true,true,POS_FACEUP)
	-- 	sc:CompleteProcedure()
	-- end

	-- if sc:IsCode(612) then
	-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL) 
	-- 	local sg=Duel.SelectMatchingCard(tp,c593.filter7,tp,loc,0,3,3,nil,e,tp,sc)
	-- 	sg:Merge(sg)
	-- 	sc:SetMaterial(sg)
	-- 	Duel.SendtoGrave(sg,REASON_RULE+REASON_FUSION+REASON_MATERIAL)
	-- 	Duel.SpecialSummon(sc,SUMMON_TYPE_FUSION,tp,tp,true,true,POS_FACEUP)
	-- 	sc:CompleteProcedure()
	-- end
end
function c593.rmtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsAbleToRemove() end
	local sg=Duel.GetMatchingGroup(Card.IsAbleToRemove,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,sg,sg:GetCount(),0,0)
end
function c593.rmop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tatk=c:GetAttack()
	local g=Duel.GetMatchingGroup(c593.rrfilter,tp,LOCATION_MZONE,LOCATION_MZONE,nil,e)
	if g:GetCount()>0 and c:IsRelateToEffect(e) and Duel.Remove(g,POS_FACEUP,REASON_EFFECT)~=0 then
	local tc=g:GetFirst() 
	local tatk=0
	  while tc do
		local atk=tc:GetPreviousAttackOnField() 
		if atk<0 then atk=0 end 
		tatk=tatk+atk 
		tc=g:GetNext() 
	  end

	--spsummon
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(10449150,1))
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_PHASE+PHASE_STANDBY)
	e4:SetRange(LOCATION_REMOVED)
	e4:SetCountLimit(1)
	e4:SetCondition(c593.spcon)
	e4:SetTarget(c593.sptg)
	e4:SetOperation(c593.spop)
	if Duel.GetCurrentPhase()==PHASE_STANDBY and Duel.GetTurnPlayer()==tp then
		e4:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_STANDBY+RESET_SELF_TURN,2)
	else
		e4:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_STANDBY+RESET_SELF_TURN,1)
	end
	c:RegisterEffect(e4)

	--damage
	local e5=Effect.CreateEffect(c) 
	e5:SetDescription(aux.Stringid(10449150,2)) 
	e5:SetCategory(CATEGORY_DAMAGE) 
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F) 
	e5:SetProperty(EFFECT_FLAG_PLAYER_TARGET) 
	e5:SetCode(EVENT_SPSUMMON_SUCCESS) 
	e5:SetCondition(c593.damcon) 
	e5:SetTarget(c593.damtg) 
	e5:SetOperation(c593.damop)  
	e5:SetLabel(tatk)
	if Duel.GetCurrentPhase()==PHASE_STANDBY and Duel.GetTurnPlayer()==tp then
		e5:SetReset(RESET_EVENT+0x1fe0000-RESET_TOFIELD+RESET_PHASE+PHASE_STANDBY+RESET_SELF_TURN,2)
	else
		e5:SetReset(RESET_EVENT+0x1fe0000-RESET_TOFIELD+RESET_PHASE+PHASE_STANDBY+RESET_SELF_TURN,1)
	end
	c:RegisterEffect(e5) end
end

function c593.spcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()==tp
end
function c593.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	  if chk==0 then return e:GetHandler():IsCanBeSpecialSummoned(e,SUMMON_TYPE_SPECIAL+1,tp,false,false) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
end
function c593.spop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)>0 then
	Duel.SpecialSummon(e:GetHandler(),SUMMON_TYPE_SPECIAL+1,tp,tp,false,false,POS_FACEUP)
	end
end

function c593.damfilter(c)
	return c:IsFaceup() and c:IsCode(41418852)
end
function c593.damcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetSummonType()==SUMMON_TYPE_SPECIAL+1
	  and Duel.IsExistingMatchingCard(c593.damfilter,tp,LOCATION_SZONE,LOCATION_SZONE,1,nil)
end
function c593.damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetTargetPlayer(1-tp)
	Duel.SetTargetParam(e:GetLabel())
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,e:GetLabel()) 
end
function c593.damop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Damage(p,d,REASON_EFFECT)
end


function c593.cfilter(c,tp)
	return c:IsFaceup() and c:IsSetCard(0x14b) and c:IsControler(tp)
end
function c593.thcon(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(c593.cfilter,1,nil,tp)
end
function c593.thtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsAbleToHand() end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,e:GetHandler(),1,0,0)
end
function c593.thop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) then
		Duel.SendtoHand(c,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,c)
	end
end
