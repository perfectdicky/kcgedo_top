--Numeron Direct
function c13707.initial_effect(c)
--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c13707.condition)
	e1:SetTarget(c13707.target)
	e1:SetOperation(c13707.activate)
	c:RegisterEffect(e1)
end
function c13707.filter1(c)
	return c:IsFaceup() and c:IsCode(41418852)
end
function c13707.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c13707.filter1,tp,LOCATION_SZONE,0,1,nil)
end
function c13707.filter(c,e,tp)
	return c:IsType(TYPE_XYZ) and c:IsAttackBelow(1000) and c:IsSetCard(0x14b)
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
end
function c13707.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return 
		(Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_XYZ)>3 
		and not Duel.IsPlayerAffectedByEffect(tp,59822133)
		and Duel.IsExistingMatchingCard(c13707.filter,tp,LOCATION_EXTRA,0,4,nil,e,tp))
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,4,0,LOCATION_EXTRA)
end
function c13707.activate(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
	if Duel.IsPlayerAffectedByEffect(tp,59822133) then return end	
	local count=0
	local ft1=Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_XYZ)
	if ft1>3 then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local g=Duel.SelectMatchingCard(tp,c13707.filter,tp,LOCATION_EXTRA,0,4,4,nil,e,tp)
		if g:GetCount()>0 then
			local tc=g:GetFirst()
			while tc do
				Duel.SpecialSummonStep(tc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
 	            local de=Effect.CreateEffect(c)
 	            de:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
 	            de:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
 	            de:SetRange(LOCATION_MZONE)
 	            de:SetCode(EVENT_PHASE+PHASE_END)
 	            de:SetCountLimit(1)
 	            de:SetOperation(c13707.rmop)
 	            de:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
 	            tc:RegisterEffect(de,true)
 	            Duel.SpecialSummonComplete()
 	            tc:CompleteProcedure()
				tc=g:GetNext()
				end
		end
	end
end
function c13707.rmop(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
	Duel.Remove(c,POS_FACEUP,REASON_EFFECT)
end
