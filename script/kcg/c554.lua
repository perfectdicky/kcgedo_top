--黑暗的奇襲 (K)
function c554.initial_effect(c)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c554.atcon)
	e1:SetOperation(c554.actop)
	c:RegisterEffect(e1)	
end

function c554.atcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetCurrentPhase()<=PHASE_BATTLE
end
function c554.atktarget(e,c)
	return not (c:GetTurnID()==Duel.GetTurnCount() and (c:GetSummonType()==SUMMON_TYPE_SPECIAL or c:GetSummonType()==SUMMON_TYPE_NORMAL)) 
end
function c554.actop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetTurnPlayer()==tp then
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_BP_TWICE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1,0)
	e1:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e1,tp) 

	local e5=Effect.CreateEffect(e:GetHandler())
	e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_PHASE+PHASE_BATTLE)
	e5:SetCountLimit(1)
	e5:SetOperation(c554.atop32)
	e5:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e5,tp) 
	end
end
function c554.atop32(e,tp,eg,ep,ev,re,r,rp)
	--cannot attack
	local e8=Effect.CreateEffect(e:GetHandler())
	e8:SetType(EFFECT_TYPE_FIELD)
	e8:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
	e8:SetTargetRange(LOCATION_MZONE,0)
	e8:SetTarget(c554.atktarget)
	  e8:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e8,tp) 
end

