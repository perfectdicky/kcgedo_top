--No.99 Hope Dragoon
local s, id = GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,10,3,s.ovfilter,aux.Stringid(51543904,0),3,s.xyzop)
	c:EnableReviveLimit()

	--cannot destroyed
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetValue(s.indes)
	c:RegisterEffect(e1)
	--spsummon
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(2067935,1))
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_MZONE)
	e2:SetTarget(s.sptg)
	e2:SetOperation(s.spop)
	e2:SetCountLimit(1)
	c:RegisterEffect(e2)
	--destroy
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(15240238,0))
	e3:SetCategory(CATEGORY_NEGATE+CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_QUICK_O)
	e3:SetCode(EVENT_CHAINING)
	e3:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCost(s.cost)
	e3:SetCondition(s.descondition)
	e3:SetTarget(s.target)
	e3:SetOperation(s.activate)
	c:RegisterEffect(e3,false,REGISTER_FLAG_DETACH_XMAT)
	local e4=e3:Clone()
	e4:SetCondition(s.handcondition)
	c:RegisterEffect(e4,false,REGISTER_FLAG_DETACH_XMAT)
	local e5=e3:Clone()
	e5:SetCondition(s.deckcondition)
	c:RegisterEffect(e5,false,REGISTER_FLAG_DETACH_XMAT)
	local e6=e3:Clone()
	e6:SetCondition(s.rmcondition)
	c:RegisterEffect(e6,false,REGISTER_FLAG_DETACH_XMAT)
	local e7=e3:Clone()
	e7:SetCondition(s.ctcondition)
	c:RegisterEffect(e7,false,REGISTER_FLAG_DETACH_XMAT)
	local e8=e3:Clone()
	e8:SetCondition(s.ctcondition2)
	--e8:SetCountLimit(1) 
	c:RegisterEffect(e8,false,REGISTER_FLAG_DETACH_XMAT)
	local e9=e3:Clone()
	e9:SetCondition(s.togravecondition)
	c:RegisterEffect(e9,false,REGISTER_FLAG_DETACH_XMAT)
	local e10=e3:Clone()
	e10:SetCondition(s.releasecondition)
	c:RegisterEffect(e10,false,REGISTER_FLAG_DETACH_XMAT)
end
s.xyz_number=99
s.listed_series={0x95,0x48}

function s.cfilter(c)
	return c:IsSetCard(0x95) and c:IsType(TYPE_SPELL) and c:IsDiscardable()
end
function s.ovfilter(c,tp,lc)
	return c:IsFaceup() and c:IsSetCard(0x107f,lc,SUMMON_TYPE_XYZ,tp)
end
function s.xyzop(e,tp,chk,mc)
	if chk==0 then return Duel.IsExistingMatchingCard(s.cfilter,tp,LOCATION_HAND,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DISCARD)
	local tc=Duel.GetMatchingGroup(s.cfilter,tp,LOCATION_HAND,0,nil):SelectUnselect(Group.CreateGroup(),tp,false,Xyz.ProcCancellable)
	if tc then
		Duel.SendtoGrave(tc,REASON_DISCARD+REASON_COST)
		return true
	else return false end
end

function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end
function s.filter(c,e,tp)
	return c:IsType(TYPE_XYZ) and (c:IsSetCard(0x48) or c:IsSetCard(0x1048) or c:IsSetCard(0x2048))
		and c:IsCanBeSpecialSummoned(e,0,tp,false,false) and not c:IsHasEffect(EFFECT_NECRO_VALLEY)
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_GRAVE,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_GRAVE)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,s.filter,tp,LOCATION_GRAVE,0,1,1,nil,e,tp)
	if g:GetCount()>0 then
	local c=e:GetHandler()
	local tc=g:GetFirst()
	if tc then
		Duel.SpecialSummonStep(tc,0,tp,tp,false,false,POS_FACEUP)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e2)
		  Duel.SpecialSummonComplete() 
	  end end
end
function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.filter1(c,e)
	return c==e:GetHandler()
end
function s.descondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_DESTROY)
	return ex and tg~=nil and tc+tg:FilterCount(s.filter1,nil,e)-tg:GetCount()>0 
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetFlagEffect(210)==0 end
	Duel.SetOperationInfo(0,CATEGORY_NEGATE,eg,1,0,0)
	if re:GetHandler():IsRelateToEffect(re) then
		  local g=Duel.GetMatchingGroup(s.filter20,tp,LOCATION_MZONE,LOCATION_MZONE,e:GetHandler())
		Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
		  local tc=g:GetFirst()
		  local dam=0
		  while tc do
			 local atk=tc:GetAttack()
			 if atk<0 then atk=0 end
			 dam=dam+atk
			 tc=g:GetNext()
		  end
			Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,dam)
	end
	  e:GetHandler():RegisterFlagEffect(210,RESET_EVENT+0x1ff0000,0,1)
end
function s.filter20(c)
	return aux.TRUE 
end
function s.filter2(c,e)
	return aux.TRUE and not c:IsImmuneToEffect(e)
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
	local tc=re:GetHandler()
	if Duel.NegateActivation(ev) then
	local g=Duel.GetMatchingGroup(s.filter2,tp,LOCATION_MZONE,LOCATION_MZONE,e:GetHandler(),e)
	local tc=g:GetFirst()
	local dam=0
	while tc do
		local atk=tc:GetAttack()
		if atk<0 then atk=0 end
		dam=dam+atk
		tc=g:GetNext()
	end
	  if Duel.Destroy(g,REASON_EFFECT)>0 then
	Duel.Damage(1-tp,dam,REASON_EFFECT) end
	  end
	  e:GetHandler():ResetFlagEffect(210)
end
function s.handcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_TOHAND)
	return ex and tg~=nil and tc+tg:FilterCount(s.filter1,nil,e)-tg:GetCount()>0 
end
function s.deckcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_TODECK)
	return ex and tg~=nil and tc+tg:FilterCount(s.filter1,nil,e)-tg:GetCount()>0 
end
function s.rmcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_REMOVE)
	return ex and tg~=nil and tc+tg:FilterCount(s.filter1,nil,e)-tg:GetCount()>0 
end
function s.ctcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_CONTROL)
	return ex and tg~=nil and tc+tg:FilterCount(s.filter1,nil,e)-tg:GetCount()>0 
end
function s.ctcondition2(e,tp,eg,ep,ev,re,r,rp)
	local ph=Duel.GetCurrentPhase()
	if not e:GetHandler():IsRelateToBattle() then return false end
	local bc=e:GetHandler():GetBattleTarget()
	return bc and bc:IsFaceup() and (bc:IsCode(209) or bc:IsCode(113) 
		or (bc:IsCode(211) and bc:GetOverlayGroup():IsExists(Card.IsCode,1,nil,209)))
end
function s.togravecondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_TOGRAVE)
	return ex and tg~=nil and tc+tg:FilterCount(s.filter1,nil,e)-tg:GetCount()>0 
end
function s.releasecondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_RELEASE)
	return ex and tg~=nil and tc+tg:FilterCount(s.filter1,nil,e)-tg:GetCount()>0 
end
 function s.spcondition(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsContains(e:GetHandler())
end
function s.spactivate(e,tp,eg,ep,ev,re,r,rp)   
	  Duel.NegateSummon(re:GetHandler())
	local g=Duel.GetMatchingGroup(aux.TRUE,tp,LOCATION_MZONE,LOCATION_MZONE,e:GetHandler())
	local tc=g:GetFirst()
	local dam=0
	while tc do
		local atk=tc:GetAttack()
		if atk<0 then atk=0 end
		dam=dam+atk
		tc=g:GetNext()
	end
	  if Duel.Destroy(g,REASON_EFFECT)>0 then
	Duel.Damage(1-tp,dam,REASON_EFFECT) end
end