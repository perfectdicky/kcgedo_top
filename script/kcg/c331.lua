--Strain Nuke (K)
local s,id=GetID()
function s.initial_effect(c)
	local e3=Effect.CreateEffect(c) 
	e3:SetProperty(EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_OVERLAY)
	e3:SetOperation(s.efop)
	c:RegisterEffect(e3)	
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetProperty(EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	-- e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	-- e2:SetCode(EVENT_BE_MATERIAL)
	-- e2:SetCondition(s.efcon)
	-- e2:SetOperation(s.efop)
	-- c:RegisterEffect(e2) 
end

function s.efcon(e,tp,eg,ep,ev,re,r,rp)
	local ec=e:GetHandler():GetReasonCard()
	return r==REASON_XYZ
end
function s.efop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local eqc=eg:GetFirst()
	--if not eqc then eqc=c:GetReasonCard() end
	Duel.Hint(HINT_CARD,ep,331)
	if not eqc then return end
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_ATTACK_ALL)
	e4:SetValue(1)
	e4:SetRange(LOCATION_MZONE)
	e4:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
	e4:SetCondition(s.condition2)
    e4:SetReset(RESET_EVENT+RESETS_STANDARD)
	eqc:RegisterEffect(e4)
end

function s.filter4(c)
	return c:GetOriginalCode()==331
end
function s.filter5(c,code)
	return c:IsCode(code)
end
function s.condition2(e,tp,eg,ep,ev,re,r,rp) 
	local c=e:GetHandler()
	local gg=Duel.GetMatchingGroup(s.filter5,0,LOCATION_MZONE,LOCATION_MZONE,nil,c:GetCode())
	return gg:GetCount()>0 and c:GetOverlayCount()>0 and c:GetOverlayGroup():IsContains(e:GetOwner())
end