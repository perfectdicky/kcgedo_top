--���L����������
function c50949586.initial_effect(c)
	--fusion material
	c:EnableReviveLimit()
	--Fusion.AddProcFunRep(c,aux.FilterBoolFunction(Card.IsSetCard,0x900),2,true)
	--spsummon condition
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(c50949586.splimit)
	c:RegisterEffect(e1)
	--special summon rule
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SPSUMMON_PROC)
	e2:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e2:SetRange(LOCATION_EXTRA)
	e2:SetCondition(c50949586.sprcon)
	e2:SetOperation(c50949586.sprop)
	c:RegisterEffect(e2)
	--immune
	--local e3=Effect.CreateEffect(c) 
	--e3:SetType(EFFECT_TYPE_SINGLE) 
	--e3:SetProperty(EFFECT_FLAG_IGNORE_RANGE) 
	--e3:SetRange(LOCATION_MZONE) 
	--e3:SetCode(EFFECT_IMMUNE_EFFECT) 
	--e3:SetValue(c50949586.efilter) 
	--c:RegisterEffect(e3) 
	--battle indestructable
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e4:SetRange(LOCATION_ONFIELD)
	e4:SetTargetRange(LOCATION_ONFIELD,0)
	e4:SetTarget(c50949586.indtg)
	e4:SetValue(1)
	c:RegisterEffect(e4)
end
c50949586.listed_series={0x900}
c50949586.material_setcode={0x900}

function c50949586.splimit(e,se,sp,st)
	return e:GetHandler():GetLocation()~=LOCATION_EXTRA
end
function c50949586.spfilter(c,sc)
	return c:IsSetCard(0x900) and c:IsCanBeFusionMaterial() and (c:IsAbleToDeckAsCost() or c:IsAbleToExtraAsCost())
	and Duel.IsExistingMatchingCard(c50949586.sp3filter,c:GetControler(),LOCATION_MZONE,0,1,c,c,sc)
end
function c50949586.sp3filter(c,tc,sc)
	return c:IsSetCard(0x900) and c:IsCanBeFusionMaterial() and (c:IsAbleToDeckAsCost() or c:IsAbleToExtraAsCost())
	and Duel.GetLocationCountFromEx(c:GetControler(),c:GetControler(),Group.FromCards(c,tc),sc)>0
end
function c50949586.sprcon(e,c)
	if c==nil then return true end 
	local tp=c:GetControler()
	return Duel.IsExistingMatchingCard(c50949586.spfilter,tp,LOCATION_MZONE,0,1,nil,e:GetHandler())
end
function c50949586.sprop(e,tp,eg,ep,ev,re,r,rp,c)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(tp,c50949586.spfilter,tp,LOCATION_MZONE,0,2,2,nil,e:GetHandler())
	local tc=g:GetFirst()
	while tc do
		if not tc:IsFaceup() then Duel.ConfirmCards(1-tp,tc) end
		tc=g:GetNext()
	end
	e:GetHandler():SetMaterial(g)   
	Duel.SendtoDeck(g,nil,2,REASON_COST)
end

function c50949586.efilter(e,te)
	return (te:IsActiveType(TYPE_SPELL)or te:IsActiveType(TYPE_TRAP) or te:IsActiveType(TYPE_MONSTER)) and te:GetHandler():GetControler()~=e:GetHandler():GetControler()
end

function c50949586.indtg(e,c)
	return c:IsSetCard(0x900) and c~=e:GetHandler()
end
