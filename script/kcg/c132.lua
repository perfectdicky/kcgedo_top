-- а�� Ĩ���ߣ�AC��
local s, id = GetID()
function s.initial_effect(c)
    -- c:SetUniqueOnField(1,1,132)

    local e04 = Effect.CreateEffect(c)
    e04:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e04:SetType(EFFECT_TYPE_SINGLE)
    e04:SetCode(EFFECT_CANNOT_SUMMON)
    c:RegisterEffect(e04)
    local e05 = e04:Clone()
    e05:SetCode(EFFECT_CANNOT_MSET)
    c:RegisterEffect(e05)

    -- special summon
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_SPSUMMON_PROC)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e1:SetRange(LOCATION_HAND)
    e1:SetCondition(s.spcon)
    e1:SetOperation(s.spop)
    c:RegisterEffect(e1)

    -- selfdes
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE)
    e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCode(EFFECT_SELF_DESTROY)
    e2:SetCondition(s.descon)
    c:RegisterEffect(e2)

    -- spson
    local e11 = Effect.CreateEffect(c)
    e11:SetType(EFFECT_TYPE_SINGLE)
    e11:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e11:SetCode(EFFECT_SPSUMMON_CONDITION)
    e11:SetValue(aux.FALSE)
    c:RegisterEffect(e11)

    -- ����Ϊ�Է����Ͽ���
    local e3 = Effect.CreateEffect(c)
    e3:SetType(EFFECT_TYPE_SINGLE)
    e3:SetCode(EFFECT_SET_ATTACK)
    e3:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e3:SetRange(LOCATION_MZONE)
    e3:SetValue(s.adval)
    c:RegisterEffect(e3)
    local e4 = e3:Clone()
    e4:SetCode(EFFECT_SET_DEFENSE)
    c:RegisterEffect(e4)

    -- �볡ʱ�ƻ��������п���
    local e5 = Effect.CreateEffect(c)
    e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e5:SetDescription(aux.Stringid(800, 2))
    e5:SetCategory(CATEGORY_DESTROY)
    e5:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_F)
    e5:SetCode(EVENT_TO_GRAVE)
    e5:SetCondition(s.erascon)
    e5:SetTarget(s.erastg)
    e5:SetOperation(s.erasop)
    c:RegisterEffect(e5)

    -- ���ٻغϽ�����ȥ����ǰ����
    -- local e7=Effect.CreateEffect(c)
    -- e7:SetDescription(aux.Stringid(10000023,2))
    -- e7:SetCategory(CATEGORY_TOGRAVE+CATEGORY_TODECK+CATEGORY_REMOVE)
    -- e7:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
    -- e7:SetRange(LOCATION_MZONE)
    -- e7:SetProperty(EFFECT_FLAG_REPEAT)
    -- e7:SetCountLimit(1)
    -- e7:SetCode(EVENT_PHASE+PHASE_END)
    -- e7:SetCondition(s.tgcon)
    -- e7:SetTarget(s.tgtg)
    -- e7:SetOperation(s.tgop)
    -- c:RegisterEffect(e5)

    local te = Effect.CreateEffect(c)
    te:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    te:SetRange(LOCATION_MZONE)
    te:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    te:SetCountLimit(1)
    te:SetCode(EVENT_PHASE + PHASE_END)
    te:SetOperation(s.reop2)
    c:RegisterEffect(te)

    local te = Effect.CreateEffect(c)
    te:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    te:SetRange(LOCATION_MZONE)
    te:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    te:SetCountLimit(1)
    te:SetCode(EVENT_PHASE + PHASE_END)
    te:SetOperation(s.reop2)
    c:RegisterEffect(te)

    local e82 = Effect.CreateEffect(c)
    e82:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e82:SetCode(EVENT_CHAIN_SOLVED)
    e82:SetRange(LOCATION_MZONE)
    e82:SetCondition(s.sdcon2)
    e82:SetOperation(s.sdop2)
    c:RegisterEffect(e82)
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e8:SetRange(LOCATION_MZONE)
    e8:SetCode(EFFECT_IMMUNE_EFFECT)
    e8:SetValue(s.efilter)
    c:RegisterEffect(e8)

    local e13 = Effect.CreateEffect(c)
    e13:SetType(EFFECT_TYPE_SINGLE)
    e13:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
    e13:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e13:SetRange(LOCATION_MZONE)
    e13:SetValue(s.tgvalue)
    -- c:RegisterEffect(e13)

    -- ���ᱻ����Ч���ƻ������⡢�������ƺͿ��顢��ȥĹ�ء���Ч�����ı����Ȩ����Ϊ����ʾ����Ϊ�����ٻ��ز�
    local e100 = Effect.CreateEffect(c)
    e100:SetType(EFFECT_TYPE_SINGLE)
    e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e100:SetRange(LOCATION_MZONE)
    e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
    e100:SetValue(s.lffilter)
    c:RegisterEffect(e100)
    local e101 = e100:Clone()
    e101:SetCode(EFFECT_CANNOT_REMOVE)
    e100:SetValue(1)
    c:RegisterEffect(e101)
    local e102 = e101:Clone()
    e102:SetCode(EFFECT_CANNOT_TO_HAND)
    c:RegisterEffect(e102)
    local e103 = e102:Clone()
    e103:SetCode(EFFECT_CANNOT_TO_DECK)
    c:RegisterEffect(e103)
    local e104 = e103:Clone()
    e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
    c:RegisterEffect(e104)
    local e105 = e104:Clone()
    e105:SetCode(EFFECT_CANNOT_DISABLE)
    c:RegisterEffect(e105)
    local e106 = e105:Clone()
    e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
    c:RegisterEffect(e106)
    local e107 = e106:Clone()
    e107:SetCode(EFFECT_CANNOT_TURN_SET)
    c:RegisterEffect(e107)
    local e108 = e107:Clone()
    e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
    c:RegisterEffect(e108)
    local e109 = e108:Clone()
    e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
    c:RegisterEffect(e109)
    local e110 = e109:Clone()
    e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
    c:RegisterEffect(e110)
    local e111 = e109:Clone()
    e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
    c:RegisterEffect(e111)
end
s.listed_names = {27564031}

function s.spfilter(c)
    return c:IsCode(57793869) and c:IsAbleToGraveAsCost()
end
function s.spcon(e, c)
    if c == nil then
        return true
    end
    return Duel.GetLocationCount(c:GetControler(), LOCATION_MZONE) > 0 and
               Duel.IsExistingMatchingCard(s.spfilter, c:GetControler(), LOCATION_HAND + LOCATION_DECK, 0, 1, nil) and
               Duel.IsExistingMatchingCard(Card.IsSetCard, c:GetControler(), LOCATION_GRAVE, 0, 3, nil, 0x23)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
    local tg = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_HAND + LOCATION_DECK, 0, 1, 1, nil)
    Duel.SendtoGrave(tg, REASON_COST)
end

function s.descon(e)
    local c = e:GetHandler()
    local f1 = Duel.GetFieldCard(0, LOCATION_SZONE, 5)
    local f2 = Duel.GetFieldCard(1, LOCATION_SZONE, 5)
    return ((f1 == nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and
               (f2 == nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end

function s.adval(e, c)
    return Duel.GetFieldGroupCount(e:GetHandler():GetControler(), 0, LOCATION_ONFIELD) * 1000
end

function s.erascon(e)
    return e:GetHandler():IsReason(REASON_DESTROY)
end
function s.filter(c)
    return not c:IsRace(RACE_CREATORGOD) and c:IsFaceup()
end
function s.erastg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    local dg = Duel.GetMatchingGroup(nil, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, nil)
    Duel.SetOperationInfo(0, CATEGORY_DESTROY, dg, dg:GetCount(), 0, 0)
end
function s.erasop(e, tp, eg, ep, ev, re, r, rp)
    local dg = Duel.GetMatchingGroup(nil, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, nil)
    Duel.Destroy(dg, REASON_EFFECT)
end

function s.tgcon(e, tp, eg, ep, ev, re, r, rp)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL and
               not e:GetHandler():GetPreviousLocation() == LOCATION_HAND
end

function s.tgtg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
        Duel.SetOperationInfo(0, CATEGORY_TOGRAVE, e:GetHandler(), 1, 0, 0)
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
        Duel.SetOperationInfo(0, CATEGORY_REMOVE, e:GetHandler(), 1, 0, 0)
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
        Duel.SetOperationInfo(0, CATEGORY_TODECK, e:GetHandler(), 1, 0, 0)
    end
end

function s.tgop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsRelateToEffect(e) and c:IsFaceup() then
        if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
            Duel.SendtoGrave(c, REASON_RULE)
        end
        if e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
            Duel.Remove(c, 0, REASON_RULE)
        end
        if e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
            Duel.SendtoDeck(c, nil, 2, REASON_RULE)
        end
    end
end

function s.efilter(e, te)
    local c = e:GetHandler()
    local tc = te:GetOwner()
    local turncount = Duel.GetTurnCount() - tc:GetTurnID()
    if c:GetTurnID() == Duel.GetTurnCount() then
        turncount = 0
    end
    if tc == e:GetOwner() or te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then
        return false
    else
        return te:GetActiveType() == TYPE_TRAP
    end
end
function s.sdcon2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=1
                    break 
                end
            end
        end
    end
    return eff==1
end
function s.sdop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=te
                    break 
                end
            end
        end
    end
    if eff==0 then return end
    local e83 = Effect.CreateEffect(c)
    e83:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e83:SetRange(LOCATION_MZONE)
    e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
    e83:SetCountLimit(1)
    e83:SetCode(EVENT_PHASE + PHASE_END)
    e83:SetLabelObject(eff)
    e83:SetOperation(s.setop2)
    e83:SetReset(RESET_PHASE + PHASE_END)
    c:RegisterEffect(e83) 
end
function s.setop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local te = e:GetLabelObject()
    if not te or te==0 then return end
    te:Reset()
end   

function s.atcon(e)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL
end
function s.atlimit(e, c)
    return c ~= e:GetHandler()
end

function s.tgvalue(e, re, rp)
    return rp ~= e:GetHandlerPlayer() and re:GetHandler():GetTurnID() ~= Duel.GetTurnCount()
end

function s.lffilter(e, re, rp)
    return re:GetOwner() ~= e:GetHandler()
end

function s.reop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if te:GetOwner() ~= e:GetOwner() and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then
                    local e80 = Effect.CreateEffect(c)
                    e80:SetType(EFFECT_TYPE_SINGLE)
                    e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
                    e80:SetRange(LOCATION_MZONE)
                    e80:SetCode(EFFECT_IMMUNE_EFFECT)
                    e80:SetValue(function(e, te2)
                        return te2 == te
                    end)
                    c:RegisterEffect(e80)
                end
            end
        end
    end
end
