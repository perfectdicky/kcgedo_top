-- Sin サイバー·エンド·ドラゴン
local s, id = GetID()
function s.initial_effect(c)
    c:EnableReviveLimit()
    -- special summon
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_SPSUMMON_PROC)
    e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
    e1:SetRange(LOCATION_HAND)
    e1:SetCondition(s.spcon)
    e1:SetOperation(s.spop)
    c:RegisterEffect(e1)

    local e4 = Effect.CreateEffect(c)
    e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e4:SetType(EFFECT_TYPE_SINGLE)
    e4:SetCode(EFFECT_CANNOT_SUMMON)
    c:RegisterEffect(e4)
    local e5 = e4:Clone()
    e5:SetCode(EFFECT_CANNOT_MSET)
    c:RegisterEffect(e5)

    -- only 1 can exists
    -- local e2=Effect.CreateEffect(c) 
    -- e2:SetType(EFFECT_TYPE_SINGLE) 
    -- e2:SetCode(EFFECT_CANNOT_FLIP_SUMMON) 
    -- e2:SetCondition(c1710476.excon) 
    -- c:RegisterEffect(e2) 
    -- local e3=e2:Clone() 
    -- e3:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON) 
    -- c:RegisterEffect(e3) 
    -- local e4=Effect.CreateEffect(c) 
    -- e4:SetType(EFFECT_TYPE_FIELD) 
    -- e4:SetRange(LOCATION_MZONE) 
    -- e4:SetProperty(EFFECT_FLAG_PLAYER_TARGET) 
    -- e4:SetTargetRange(1,1) 
    -- e4:SetCode(EFFECT_CANNOT_SUMMON) 
    -- e4:SetTarget(s.sumlimit) 
    -- c:RegisterEffect(e4) 
    -- local e5=e4:Clone() 
    -- e5:SetCode(EFFECT_CANNOT_FLIP_SUMMON) 
    -- c:RegisterEffect(e5) 
    -- local e6=e4:Clone() 
    -- e6:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON) 
    -- c:RegisterEffect(e6) 

    -- selfdes
    local e7 = Effect.CreateEffect(c)
    e7:SetType(EFFECT_TYPE_SINGLE)
    e7:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e7:SetRange(LOCATION_MZONE)
    e7:SetCode(EFFECT_SELF_DESTROY)
    e7:SetCondition(s.descon)
    c:RegisterEffect(e7)
    -- local e8=Effect.CreateEffect(c)
    -- e8:SetType(EFFECT_TYPE_FIELD) 
    -- e8:SetCode(EFFECT_SELF_DESTROY) 
    -- e8:SetRange(LOCATION_MZONE) 
    -- e8:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE) 
    -- e8:SetTarget(c1710476.destarget) 
    -- c:RegisterEffect(e8) 

    -- cannot announce
    -- local e8=Effect.CreateEffect(c) 
    -- e8:SetType(EFFECT_TYPE_FIELD) 
    -- e8:SetRange(LOCATION_MZONE) 
    -- e8:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE) 
    -- e8:SetTargetRange(LOCATION_MZONE,0) 
    -- e8:SetTarget(c1710476.antarget) 
    -- c:RegisterEffect(e8) 

    -- spson
    local e9 = Effect.CreateEffect(c)
    e9:SetType(EFFECT_TYPE_SINGLE)
    e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e9:SetCode(EFFECT_SPSUMMON_CONDITION)
    e9:SetValue(aux.FALSE)
    c:RegisterEffect(e9)

    -- pierce
    local e10 = Effect.CreateEffect(c)
    e10:SetType(EFFECT_TYPE_SINGLE)
    e10:SetCode(EFFECT_PIERCE)
    c:RegisterEffect(e10)
end
s.listed_names = {27564031}

function s.sumlimit(e, c)
    return c:IsSetCard(0x23)
end
function s.exfilter(c)
    return c:IsFaceup() and c:IsSetCard(0x23)
end
function s.excon(e)
    return Duel.IsExistingMatchingCard(c1710476.exfilter, 0, LOCATION_MZONE, LOCATION_MZONE, 1, nil)
end

function s.spfilter(c)
    return c:IsCode(1546123) and c:IsAbleToGraveAsCost()
end
function s.spcon(e, c)
    if c == nil then
        return true
    end
    return Duel.GetLocationCount(c:GetControler(), LOCATION_MZONE) > 0 and
               Duel.IsExistingMatchingCard(s.spfilter, c:GetControler(), LOCATION_EXTRA, 0, 1, nil)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
    local tg = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_EXTRA, 0, 1, 1, nil)
    Duel.SendtoGrave(tg, REASON_COST)
end

function s.descon(e)
    local c = e:GetHandler()
    local f1 = Duel.GetFieldCard(0, LOCATION_SZONE, 5)
    local f2 = Duel.GetFieldCard(1, LOCATION_SZONE, 5)
    return ((f1 == nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and
               (f2 == nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end
function s.destarget(e, c)
    return c:IsSetCard(0x23) and c:GetFieldID() > e:GetHandler():GetFieldID()
end

function s.antarget(e, c)
    return c ~= e:GetHandler()
end
