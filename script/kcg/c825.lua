local s, id = GetID()
function s.initial_effect(c)
	--解放3只祭品通召
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetDescription(aux.Stringid(10000080,0))	
	-- e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	-- e1:SetType(EFFECT_TYPE_SINGLE)
	-- e1:SetCode(EFFECT_LIMIT_SUMMON_PROC)
	-- e1:SetCondition(s.ttcon)
	-- e1:SetOperation(s.ttop)
	-- e1:SetValue(SUMMON_TYPE_TRIBUTE)
	-- c:RegisterEffect(e1)
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	-- e2:SetType(EFFECT_TYPE_SINGLE)
	-- e2:SetCode(EFFECT_LIMIT_SET_PROC)
	-- e2:SetCondition(s.ttcon)
	-- e2:SetOperation(s.ttop)
	-- c:RegisterEffect(e2)
	local e1=aux.AddNormalSummonProcedure(c,true,false,3,3,SUMMON_TYPE_TRIBUTE,aux.Stringid(10000080,0))
	local e2=aux.AddNormalSetProcedure(c,true,false,3,3,SUMMON_TYPE_TRIBUTE,aux.Stringid(10000080,0))
	
	local e02=Effect.CreateEffect(c)
	e02:SetDescription(aux.Stringid(10000080,1))
	e02:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_SPSUM_PARAM)
	e02:SetType(EFFECT_TYPE_SINGLE)
	e02:SetCode(EFFECT_LIMIT_SUMMON_PROC)
	e02:SetTargetRange(POS_FACEUP_ATTACK,1)
	e02:SetCondition(s.ttcon2)
	e02:SetTarget(s.tttg2)
	e02:SetOperation(s.ttop2)
	e02:SetValue(SUMMON_TYPE_TRIBUTE)
	c:RegisterEffect(e02)	

	local e200=Effect.CreateEffect(c)
	e200:SetType(EFFECT_TYPE_SINGLE)
	e200:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e200:SetCode(EFFECT_ADD_RACE)
	e200:SetValue(RACE_DRAGON+RACE_CREATORGOD)
	c:RegisterEffect(e200)	

	--control return
	local e00=Effect.CreateEffect(c)
	e00:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e00:SetCode(EVENT_ADJUST)
	e00:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_CANNOT_DISABLE)
	e00:SetRange(LOCATION_MZONE)
	e00:SetCondition(s.retcon)
	e00:SetOperation(s.retop)
	c:RegisterEffect(e00)

 	--通常召唤、特殊召唤、反转召唤不会被无效化
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
	e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e8)
	local e9=e8:Clone()
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	c:RegisterEffect(e9)
	local e10=e9:Clone()
	e10:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
    c:RegisterEffect(e10)
       
	local e400=Effect.CreateEffect(c)
	e400:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e400:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e400:SetCode(EVENT_SUMMON_SUCCESS)
	e400:SetOperation(s.sumsuc)
    c:RegisterEffect(e400)
    local e401=e400:Clone()
    e401:SetCode(EVENT_SPSUMMON_SUCCESS)	
    c:RegisterEffect(e401)
    local e402=e400:Clone()
    e402:SetCode(EVENT_FLIP_SUMMON_SUCCESS)	
	c:RegisterEffect(e402)   
	
	--attack limit
	local e06=Effect.CreateEffect(c)
	e06:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e06:SetType(EFFECT_TYPE_SINGLE)
	e06:SetCode(EFFECT_CANNOT_ATTACK)
	e06:SetCondition(s.retcon3)
	c:RegisterEffect(e06)
	local e111=e06:Clone()
	e111:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e111:SetRange(LOCATION_MZONE)
	e111:SetCode(EFFECT_CANNOT_TRIGGER)
	c:RegisterEffect(e111)
	local e112=e06:Clone()
	e112:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e112:SetCode(EFFECT_CANNOT_CHANGE_POSITION)
	e112:SetRange(LOCATION_MZONE)
	c:RegisterEffect(e112)
	
	--cannot be target
	local e07=Effect.CreateEffect(c)
	e07:SetType(EFFECT_TYPE_SINGLE)
	e07:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e07:SetCode(EFFECT_IGNORE_BATTLE_TARGET)
	e07:SetRange(LOCATION_MZONE)
	e07:SetCondition(s.retcon3)
	e07:SetValue(aux.imval1)
	c:RegisterEffect(e07)
	local e08=e07:Clone()
	e08:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
	e08:SetValue(aux.tgoval)
	c:RegisterEffect(e08)
	
    --通常召唤后攻守数值
	local e4=Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetCode(EFFECT_MATERIAL_CHECK)
	e4:SetValue(s.valcheck)
	c:RegisterEffect(e4)
	local e5=Effect.CreateEffect(c)
	e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_SUMMON_SUCCESS)
	e5:SetLabelObject(e4)
	e5:SetOperation(s.atkop)
	c:RegisterEffect(e5)
end

function s.ttcon(e,c,minc)
	if c==nil then return true end
	return minc<=3 and Duel.CheckTribute(c,3)
end
function s.ttop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=Duel.SelectTribute(tp,c,3,3)
	c:SetMaterial(g)
	Duel.Release(g,REASON_SUMMON+REASON_MATERIAL)
end

function s.ttcon2(e,c,minc,zone,relzone,exeff)
	if c==nil then return true end
	if exeff then
		local ret=exeff:GetValue()
		if type(ret)=="function" then
			ret={ret(exeff,c)}
			if #ret>1 then
				zone=(ret[2]>>16)&0x7f
			end
		end
	end
	local tp=c:GetControler()
	local mg=Duel.GetFieldGroup(tp,0,LOCATION_MZONE)
	mg=mg:Filter(Auxiliary.IsZone,nil,relzone,tp)
	return minc<=3 and Duel.CheckTribute(c,3,3,mg,1-tp,zone)
end
function s.tttg2(e,tp,eg,ep,ev,re,r,rp,chk,c,minc,zone,relzone,exeff)
	if exeff then
		local ret=exeff:GetValue()
		if type(ret)=="function" then
			ret={ret(exeff,c)}
			if #ret>1 then
				zone=(ret[2]>>16)&0x7f
			end
		end
	end
	local mg=Duel.GetFieldGroup(tp,0,LOCATION_MZONE)
	mg=mg:Filter(Auxiliary.IsZone,nil,relzone,tp)
	local g=Duel.SelectTribute(tp,c,3,3,mg,1-tp,zone,true)
	if g and #g>0 then
		g:KeepAlive()
		e:SetLabelObject(g)
		return true
	end
	return false
end
function s.ttop2(e,tp,eg,ep,ev,re,r,rp,c,minc,zone,relzone,exeff)
	local g=e:GetLabelObject()
	c:SetMaterial(g)
	Duel.Release(g,REASON_SUMMON+REASON_MATERIAL)
	g:DeleteGroup()
end

function s.retcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetFlagEffect(e:GetHandler():GetControler(),7082)==0
end
function s.retop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetOwner()
	local type=c:GetType()
	local opt=0
	if Duel.IsPlayerAffectedByEffect(e:GetHandler():GetControler(),10) or Duel.IsEnvironment(269012,e:GetHandler():GetControler()) then return end
	if Duel.GetFlagEffect(e:GetHandler():GetControler(),7082)~=0 then return end
	if (Duel.IsPlayerAffectedByEffect(1-e:GetHandler():GetControler(),10) or Duel.IsEnvironment(269012,1-e:GetHandler():GetControler())) 
	and Duel.GetLocationCount(1-e:GetHandler():GetControler(),LOCATION_MZONE)>0 and Duel.SelectYesNo(1-e:GetHandler():GetControler(),aux.Stringid(11508758,0)) then
	Duel.GetControl(c,1-e:GetHandler():GetControler()) 
	if e:GetHandler():GetOriginalCode()==10000048 then e:GetHandler():SetEntityCode(825,nil) end 
	return end
		
	if Duel.GetFlagEffect(1-e:GetHandler():GetControler(),7081)==0 and not (Duel.IsPlayerAffectedByEffect(1-e:GetHandler():GetControler(),10) or Duel.IsEnvironment(269012,1-e:GetHandler():GetControler())) then
	Duel.RegisterFlagEffect(1-e:GetHandler():GetControler(),7081,0,0,1)
	Duel.Hint(HINT_MESSAGE,1-e:GetHandlerPlayer(),aux.Stringid(708,6))
	local opt=Duel.SelectOption(1-e:GetHandlerPlayer(),aux.Stringid(708,3),aux.Stringid(708,4),aux.Stringid(708,7))
	if opt==0 then Duel.RegisterFlagEffect(1-e:GetHandler():GetControler(),7082,0,0,1)	
	else if not e:GetHandler():GetOriginalCode()==10000048 then 
		 e:GetHandler():SetEntityCode(10000048,nil) 
		 e:GetHandler():SetCardData(CARDDATA_TYPE,type) end
	end
	end

	if Duel.GetFlagEffect(1-e:GetHandler():GetControler(),7082)~=0 and Duel.GetLocationCount(1-e:GetHandler():GetControler(),LOCATION_MZONE)>0 and Duel.SelectYesNo(1-e:GetHandler():GetControler(),aux.Stringid(11508758,0)) then
	Duel.GetControl(c,1-e:GetHandler():GetControler())
	if e:GetHandler():GetOriginalCode()==10000048 then e:GetHandler():SetEntityCode(825,nil)
		local opt=Duel.SelectOption(e:GetHandlerPlayer(),aux.Stringid(825,0),aux.Stringid(825,1))
		if opt==0 then 
			e:GetHandler():SetEntityCode(708,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
		else e:GetHandler():SetEntityCode(824,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true)  end	
	end end

	if Duel.GetFlagEffect(e:GetHandler():GetControler(),7081)==0 and Duel.GetFlagEffect(1-e:GetHandler():GetControler(),7082)==0 then
	local opt=Duel.SelectOption(e:GetHandlerPlayer(),aux.Stringid(708,3),aux.Stringid(708,4),aux.Stringid(708,7))
	if opt==0 then Duel.RegisterFlagEffect(e:GetHandler():GetControler(),7082,0,0,1) 
	  if e:GetHandler():GetOriginalCode()==10000048 then e:GetHandler():SetEntityCode(825,nil) 
		local opt=Duel.SelectOption(e:GetHandlerPlayer(),aux.Stringid(825,0),aux.Stringid(825,1))
		if opt==0 then 
			e:GetHandler():SetEntityCode(708,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
		else e:GetHandler():SetEntityCode(824,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true)  end	
	end 
	else if not e:GetHandler():GetOriginalCode()==10000048 then 
		 e:GetHandler():SetEntityCode(10000048,nil) 
		 e:GetHandler():SetCardData(CARDDATA_TYPE,type) end
	end end
end

function s.sumsuc(e,tp,eg,ep,ev,re,r,rp)
	local type=e:GetHandler():GetType()
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
	if (Duel.IsPlayerAffectedByEffect(e:GetHandlerPlayer(),10) or Duel.IsEnvironment(269012,e:GetHandlerPlayer())) or Duel.GetFlagEffect(e:GetHandlerPlayer(),7082)~=0 then
	local opt=Duel.SelectOption(e:GetHandlerPlayer(),aux.Stringid(825,0),aux.Stringid(825,1))
	if opt==0 then 
		e:GetHandler():SetEntityCode(708,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
	else e:GetHandler():SetEntityCode(824,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) end	
	return end
	if Duel.GetFlagEffect(e:GetHandlerPlayer(),7081)~=0 then return end
	Duel.RegisterFlagEffect(e:GetHandlerPlayer(),7081,0,0,1) 
	Duel.Hint(HINT_MESSAGE,e:GetHandlerPlayer(),aux.Stringid(708,6))
	local opt=Duel.SelectOption(e:GetHandlerPlayer(),aux.Stringid(708,4),aux.Stringid(708,3),aux.Stringid(708,7))
	if opt==1 then 
		Duel.RegisterFlagEffect(e:GetHandlerPlayer(),7082,0,0,1)
		local opt=Duel.SelectOption(e:GetHandlerPlayer(),aux.Stringid(825,0),aux.Stringid(825,1))
		if opt==0 then 
			e:GetHandler():SetEntityCode(708,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
		else e:GetHandler():SetEntityCode(824,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true)  end		
	else e:GetHandler():SetEntityCode(10000048,nil) 
		 e:GetHandler():SetCardData(CARDDATA_TYPE,type)
	end	
end

function s.retcon2(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetOriginalCode()~=10000048
end
function s.retcon3(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetOriginalCode()==10000048
end
function s.valcheck(e,c)
	local g=c:GetMaterial()
	  local tc=g:GetFirst()
	  local tatk=0
	  local tdef=0
	  while tc do
	  local atk=tc:GetAttack()
	  local def=tc:GetDefense()
	  if atk<0 then atk=0 end
	  if def<0 then def=0 end
	  tatk=tatk+atk 
	  tdef=tdef+def 
	  tc=g:GetNext() end
	Original_ATK=tatk
	Original_DEF=tdef
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:GetMaterialCount()==0 then return end
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetValue(Original_ATK)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EFFECT_SET_DEFENSE)
	e2:SetValue(Original_DEF)
	c:RegisterEffect(e2)
end