--罪龍 (K)
function c102.initial_effect(c)
		c:EnableReviveLimit()

		--special summon
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_SPSUMMON_PROC)
		e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
		e1:SetRange(LOCATION_HAND)
		e1:SetCondition(c102.spcon)
		e1:SetOperation(c102.spop)
		c:RegisterEffect(e1)

		--selfdes
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e2:SetRange(LOCATION_MZONE)
		e2:SetCode(EFFECT_SELF_DESTROY)
		e2:SetCondition(c102.descon)
		c:RegisterEffect(e2)

		--spson
		local e3=Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e3:SetCode(EFFECT_SPSUMMON_CONDITION)
		e3:SetValue(aux.FALSE)
		c:RegisterEffect(e3)

		local e4=Effect.CreateEffect(c) 
	  e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e4:SetType(EFFECT_TYPE_SINGLE) 
		e4:SetCode(EFFECT_CANNOT_SUMMON) 
		c:RegisterEffect(e4) 
		local e5=e4:Clone() 
		e5:SetCode(EFFECT_CANNOT_MSET) 
		c:RegisterEffect(e5) 

	--spsummon success
	local e5=Effect.CreateEffect(c)
	e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)	  
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	e5:SetLabelObject(e1)
	e5:SetOperation(c102.sucop)
	c:RegisterEffect(e5)
end
c102.listed_names={27564031}

function c102.spfilter(c)
		return (c:GetLevel()>=7 or c:GetRank()>=7 or c:GetLink()>=7) and c:IsRace(RACE_DRAGON) and c:IsAbleToGraveAsCost() 
		and not c:IsCode(35952884) and not c:IsCode(79856792) and not c:IsCode(1546123) and not c:IsCode(89631139) and not c:IsCode(44508094) and not c:IsCode(74677422) and not c:IsCode(10000010) and not c:IsCode(10000020)  and not c:IsCode(57793869) and not c:IsCode(6007213) and not c:IsCode(90048290)
		and not c:IsSetCard(0x23)
end
function c102.spcon(e,c)
		if c==nil then return true end
		return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0
			   and Duel.IsExistingMatchingCard(c102.spfilter,c:GetControler(),LOCATION_HAND+LOCATION_DECK+LOCATION_EXTRA,0,1,nil)
end
function c102.spop(e,tp,eg,ep,ev,re,r,rp,c)
		local c=e:GetHandler()
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
		local tg=Duel.SelectMatchingCard(tp,c102.spfilter,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_EXTRA,0,1,1,nil)
		if Duel.SendtoGrave(tg,REASON_COST)>0 then 
		tg:KeepAlive()
		e:SetLabelObject(tg) end
		--tg:GetFirst():RegisterFlagEffect(102,0,0,0) 
		--c:RegisterFlagEffect(102,0,0,0) end
end

function c102.descon(e)
	local c=e:GetHandler()
	local f1=Duel.GetFieldCard(0,LOCATION_SZONE,5)
	local f2=Duel.GetFieldCard(1,LOCATION_SZONE,5)
	return ((f1==nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and (f2==nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end

function c102.sumlimit(e,c)
		return c:IsSetCard(0x23)
end

function c102.efilter(e,te)
	return te:GetHandler()==e:GetHandler()
end

function c102.sucfilter(c)
	return not c:IsCode(35952884) and not c:IsCode(79856792) and not c:IsCode(1546123) and not c:IsCode(89631139) and not c:IsCode(44508094) and not c:IsCode(74677422) and not c:IsCode(10000010) and not c:IsCode(10000020)  and not c:IsCode(57793869) and not c:IsCode(6007213) and not c:IsCode(90048290)
		and not c:IsSetCard(0x23)
end
function c102.sucop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if e:GetLabelObject()==nil then return end
	local g=e:GetLabelObject():GetLabelObject():GetFirst()
	c:SetEntityCode(g:GetOriginalCode(),102,{g:GetOriginalSetCard()}, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
end
function c102.mtfilter(c)
	return c:IsSetCard(0x23) and c:IsType(TYPE_MONSTER)
end
