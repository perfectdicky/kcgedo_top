--罪龍帝 (K)
function c103.initial_effect(c)
		c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

		--special summon
		local e1=Effect.CreateEffect(c)
	  e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_SPSUMMON_PROC)
		e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
		e1:SetRange(LOCATION_EXTRA)
		e1:SetCondition(c103.spcon)
		--e1:SetOperation(c103.spop)  
		c:RegisterEffect(e1)

	  local e100=Effect.CreateEffect(c)
	  e100:SetType(EFFECT_TYPE_SINGLE)
	  e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	  e100:SetRange(LOCATION_MZONE)
	  e100:SetCode(EFFECT_CANNOT_DISABLE)
	  e100:SetValue(1)
	  c:RegisterEffect(e100)
	  --特殊召唤不会被无效化
	  local e9=Effect.CreateEffect(c)
	  e9:SetType(EFFECT_TYPE_SINGLE)
	  e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	  e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	  c:RegisterEffect(e9)

		--selfdes
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e2:SetRange(LOCATION_MZONE)
		e2:SetCode(EFFECT_SELF_DESTROY)
		e2:SetCondition(c103.descon)
		c:RegisterEffect(e2)

		--spson
		local e3=Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e3:SetCode(EFFECT_SPSUMMON_CONDITION)
		e3:SetValue(c103.efilter)
		c:RegisterEffect(e3)

		local e4=Effect.CreateEffect(c)
		e4:SetType(EFFECT_TYPE_SINGLE)
		e4:SetCode(EFFECT_SET_ATTACK)
		e4:SetValue(c103.atkvalue)
		c:RegisterEffect(e4)
		local e5=Effect.CreateEffect(c)
		e5=e4:Clone()
		e5:SetCode(EFFECT_SET_DEFENSE)
		c:RegisterEffect(e5)

	--spsummon success
	local e6=Effect.CreateEffect(c)
	e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)	  
	e6:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e6:SetCode(EVENT_SPSUMMON_SUCCESS)
	e6:SetOperation(c103.sucop)
	c:RegisterEffect(e6)
end
c103.listed_names={27564031,104}

function c103.spfilter(c)
		return c:GetLevel()>=7 and c:IsRace(RACE_DRAGON) and c:IsAbleToGraveAsCost() 
		and not c:IsCode(79856792) and not c:IsCode(1546123) and not c:IsCode(89631139) and not c:IsCode(44508094) and not c:IsCode(74677422)
		and not c:IsSetCard(0x23)
end
function c103.spfilter2(c)
		return c:GetOriginalCode()==104
end
function c103.spcon(e,c)
		if c==nil then return true end
		return Duel.IsExistingMatchingCard(Card.IsSetCard,e:GetHandlerPlayer(),LOCATION_GRAVE,0,5,nil,0x23)
			and Duel.IsExistingMatchingCard(c103.spfilter2,e:GetHandlerPlayer(),LOCATION_SZONE,0,1,nil)
			and Duel.GetLocationCountFromEx(tp,tp,nil,e:GetHandler())>0
end
function c103.spop(e,tp,eg,ep,ev,re,r,rp,c)
		 if Duel.IsExistingMatchingCard(Card.IsRace,e:GetHandlerPlayer(),LOCATION_GRAVE,0,1,nil,RACE_DRAGON) then
		 local gg=Duel.GetMatchingGroup(Card.IsRace,e:GetHandlerPlayer(),LOCATION_GRAVE,0,nil,RACE_DRAGON)
		 if gg==nil then return end
		 Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EFFECT)
		 local g=gg:FilterSelect(e:GetHandlerPlayer(),Card.IsType,0,99,nil,TYPE_EFFECT)
		 local g1=g:GetFirst()
		 while g1 do
		 e:GetHandler():CopyEffect(g1:GetOriginalCode(),RESET_EVENT+EVENT_TO_DECK,1)
		 g1=g:GetNext()
		 end end
end

function c103.descon(e)
	local c=e:GetHandler()
	local f1=Duel.GetFieldCard(0,LOCATION_SZONE,5)
	local f2=Duel.GetFieldCard(1,LOCATION_SZONE,5)
	return ((f1==nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and (f2==nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end
function c103.atkvalue(e)
		return Duel.GetMatchingGroupCount(Card.IsRace,0,LOCATION_GRAVE,LOCATION_GRAVE,nil,RACE_DRAGON)*1000
end

function c103.efilter(e,te)
	return te:GetHandler()==e:GetHandler()
end

function c103.sucop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	  if Duel.IsExistingMatchingCard(Card.IsRace,e:GetHandlerPlayer(),LOCATION_GRAVE,0,1,nil,RACE_DRAGON) then
	  local gg=Duel.GetMatchingGroup(Card.IsRace,e:GetHandlerPlayer(),LOCATION_GRAVE,0,nil,RACE_DRAGON)
	  if gg==nil then return end
	  Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EFFECT)
	  local g=gg:FilterSelect(e:GetHandlerPlayer(),Card.IsType,0,99,nil,TYPE_EFFECT)
	  local g1=g:GetFirst()
	  while g1 do
	  c:CopyEffect(g1:GetOriginalCode(),RESET_EVENT+EVENT_TO_DECK,1)
	  g1=g:GetNext()
	  end end
end
