--Over-Hundred Call
function c13725.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c13725.target)
	e1:SetOperation(c13725.activate)
	c:RegisterEffect(e1)
end
function c13725.filter1(c)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsFaceup() 
end
function c13725.filter2(c,e,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
end
function c13725.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0  
            and Duel.IsExistingMatchingCard(c13725.filter1,tp,LOCATION_MZONE,0,3,nil)   
		and Duel.IsExistingMatchingCard(c13725.filter2,tp,LOCATION_EXTRA,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end 
function c13725.activate(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 or not Duel.IsExistingMatchingCard(c13725.filter1,tp,LOCATION_MZONE,0,3,nil) then return end
	Duel.Hint(HINT_SELECTMSG,e:GetHandler():GetControler(),HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c13725.filter2,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	local tc=g:GetFirst()
	if tc then 
            Duel.SpecialSummonStep(tc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
		--local e1=Effect.CreateEffect(e:GetHandler())
		--e1:SetType(EFFECT_TYPE_SINGLE)
		--e1:SetCode(EFFECT_CANNOT_ATTACK)
		--e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
		--e1:SetReset(RESET_EVENT+0x1fe0000)
		--tc:RegisterEffect(e1,true)
		--local e3=Effect.CreateEffect(e:GetHandler())
		--e3:SetType(EFFECT_TYPE_SINGLE)
		--e3:SetCode(EFFECT_DISABLE)
		--e3:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
		--e3:SetReset(RESET_EVENT+0x1fe0000)
		--tc:RegisterEffect(e3,true)
		--local e2=Effect.CreateEffect(e:GetHandler())
		--e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		--e2:SetCode(EVENT_PHASE+PHASE_END)
		--e2:SetCountLimit(1)
		--e2:SetRange(LOCATION_MZONE)
		--e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		--e2:SetOperation(c13725.desop)
		--tc:RegisterEffect(e2)
            Duel.SpecialSummonComplete()
		tc:CompleteProcedure()
	end
end
function c13725.desop(e,tp,eg,ep,ev,re,r,rp)
	Duel.SendtoDeck(e:GetHandler(),nil,2,REASON_EFFECT)
end
