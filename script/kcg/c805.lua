--光之创造神 哈拉克提
local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()

	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_REMOVE_TYPE)
	e0:SetValue(TYPE_FUSION)
	c:RegisterEffect(e0)	
	--融合特召限制
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(aux.FALSE)
	c:RegisterEffect(e1)
	
	--融合特召规则
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SPSUMMON_PROC)
	e2:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE)
	e2:SetRange(LOCATION_EXTRA)
	e2:SetCondition(s.spcon)
	e2:SetOperation(s.spop)
	e2:SetValue(805)
	c:RegisterEffect(e2)
	
	--特殊召唤不会无效化
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e3)
	local e10=e3:Clone()
	e10:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
	c:RegisterEffect(e10)	

	local e400=Effect.CreateEffect(c)
	e400:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e400:SetCode(EVENT_SUMMON_SUCCESS)
	e400:SetOperation(s.sumsuc)
	c:RegisterEffect(e400)
    local e401=e400:Clone()
    e401:SetCode(EVENT_SPSUMMON_SUCCESS)	
    c:RegisterEffect(e401)
    local e402=e400:Clone()
    e402:SetCode(EVENT_FLIP_SUMMON_SUCCESS)	
    c:RegisterEffect(e402) 	
	
	local e80 = Effect.CreateEffect(c)
    e80:SetType(EFFECT_TYPE_SINGLE)
    e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e80:SetRange(LOCATION_MZONE)
    e80:SetCode(EFFECT_IMMUNE_EFFECT)
    e80:SetValue(s.efilterr)
    c:RegisterEffect(e80)
    local e118 = e80:Clone()
    e118:SetCode(EFFECT_GOD_IMMUNE)
    c:RegisterEffect(e118)	
	
	local e17 = Effect.CreateEffect(c)
    e17:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
    e17:SetRange(LOCATION_MZONE)
    e17:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e17:SetCode(EVENT_ATTACK_ANNOUNCE)
    e17:SetCondition(s.atcon2)
    e17:SetOperation(s.atop)
    c:RegisterEffect(e17)
	
	-- destroy replace
    local e72 = Effect.CreateEffect(c)
    e72:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
    e72:SetCode(EFFECT_DESTROY_REPLACE)
    e72:SetRange(LOCATION_MZONE)
    e72:SetTarget(s.reptg)
    e72:SetValue(s.repval)
    e72:SetOperation(s.repop)
    c:RegisterEffect(e72)
	
	local e100 = Effect.CreateEffect(c)
    e100:SetType(EFFECT_TYPE_SINGLE)
    e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e100:SetRange(LOCATION_MZONE)
    e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
    e100:SetValue(1)
    c:RegisterEffect(e100)
    local e101 = e100:Clone()
    e101:SetCode(EFFECT_CANNOT_REMOVE)
    c:RegisterEffect(e101)
    local e102 = e100:Clone()
    e102:SetCode(EFFECT_CANNOT_TO_HAND)
    c:RegisterEffect(e102)
    local e103 = e100:Clone()
    e103:SetCode(EFFECT_CANNOT_TO_DECK)
    c:RegisterEffect(e103)
    local e104 = e100:Clone()
    e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
    c:RegisterEffect(e104)
    local e105 = e100:Clone()
    e105:SetCode(EFFECT_CANNOT_DISABLE)
    c:RegisterEffect(e105)
    local e106 = e100:Clone()
    e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
    c:RegisterEffect(e106)
    local e107 = e100:Clone()
    e107:SetCode(EFFECT_CANNOT_TURN_SET)
    c:RegisterEffect(e107)
    local e108 = e100:Clone()
    e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
    c:RegisterEffect(e108)
    local e109 = e100:Clone()
    e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
    c:RegisterEffect(e109)
    local e110 = e100:Clone()
    e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
    c:RegisterEffect(e110)
    local e111 = e100:Clone()
    e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
    c:RegisterEffect(e111)

    local e117 = Effect.CreateEffect(c)
    e117:SetType(EFFECT_TYPE_FIELD)
    e117:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
    e117:SetCode(EFFECT_CANNOT_RELEASE)
    e117:SetRange(LOCATION_MZONE)
    e117:SetTargetRange(0, 1)
    e117:SetTarget(s.rellimit)
    c:RegisterEffect(e117)

    local e115 = Effect.CreateEffect(c)
    e115:SetType(EFFECT_TYPE_SINGLE)
    e115:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e115:SetValue(function(e, c)
        if not c then
            return false
        end
        return not c:IsRace(RACE_CREATORGOD)
    end)
    c:RegisterEffect(e115)
    local e116 = e115:Clone()
    e116:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
    c:RegisterEffect(e116)
end
s.listed_names = {10000000, CARD_RA, 10000020}
-------------------------------------------------------------------------------------------------------------------------------------------
function s.splimit(e,se,sp,st)
	return e:GetHandler():GetLocation()~=LOCATION_EXTRA
end
-----------------------------------------------------------------------------------------
function s.sumsuc(e,tp,eg,ep,ev,re,r,rp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
	if e:GetHandler():IsSummonType(805) then
	local e8=Effect.CreateEffect(e:GetHandler())
	e8:SetType(EFFECT_TYPE_FIELD)
	e8:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e8:SetCode(10000042)
	e8:SetTargetRange(0,1)
	e8:SetValue(1)
	e8:SetReset(RESET_CHAIN)
	Duel.RegisterEffect(e8,e:GetHandler():GetSummonPlayer())
	Duel.Win(e:GetHandler():GetSummonPlayer(),WIN_REASON_CREATORGOD) end
end
--------------------------------------------------
function s.DGDfiler(c,e,tp)
	return c:IsCode(10000020) and c:IsFaceup() and c:IsRace(RACE_CREATORGOD)
	 and Duel.CheckReleaseGroup(tp,s.DGDfiler2,1,nil,c,e,tp)
end
function s.DGDfiler2(c,tc1,e,tp)
	local g=Group.FromCards(tc1,c)
	return c:IsCode(10000000) and c:IsFaceup() and c~=tc1 and c:IsRace(RACE_CREATORGOD)
	 and Duel.CheckReleaseGroup(tp,s.DGDfiler3,1,nil,g,e,tp)
end
function s.DGDfiler3(c,g,e,tp)
	local ag=g
	if ag:IsContains(c)	then return false end
	ag:AddCard(c)
	return c:IsCode(10000010) and c:IsFaceup() and c:IsRace(RACE_CREATORGOD)
	and Duel.GetLocationCountFromEx(tp,tp,ag,e:GetHandler())>0
end

function s.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	return Duel.CheckReleaseGroup(tp,s.DGDfiler,1,nil,e,tp)
end

function s.spop(e,tp,eg,ep,ev,re,r,rp,c)
	if Duel.CheckReleaseGroup(tp,s.DGDfiler,1,nil,e,tp) then
	local gt1=Duel.SelectMatchingCard(tp,s.DGDfiler,tp,LOCATION_MZONE,0,1,1,nil,e,tp):GetFirst()
	local gt2=Duel.SelectMatchingCard(tp,s.DGDfiler2,tp,LOCATION_MZONE,0,1,1,nil,gt1,e,tp):GetFirst()
	local gt3=Duel.SelectMatchingCard(tp,s.DGDfiler3,tp,LOCATION_MZONE,0,1,1,nil,Group.FromCards(gt1,gt2),e,tp):GetFirst()
	local agt=Group.FromCards(gt1,gt2,gt3)
	Duel.Release(agt,REASON_COST+REASON_RULE)
	end
end

function s.efilterr(e, te)
    return te:GetOwner()~=e:GetOwner()
end

function s.atcon2(e, tp, eg, ep, ev, re, r, rp)
    local tc = Duel.GetAttacker()
    local tc2 = Duel.GetAttackTarget()
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL and tc:IsFaceup() and
               tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler()
end
function s.atop(e, tp, eg, ep, ev, re, r, rp)
    local tc = Duel.GetAttacker()
    local tc2 = Duel.GetAttackTarget()
    if tc:IsFaceup() and tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler() then
        Duel.ChangeAttackTarget(e:GetHandler())
    end
end

function s.repfilter(c, tc, tp)
    return c:IsControler(tp) and c ~= tc and c:IsLocation(LOCATION_MZONE) and
               (c:IsReason(REASON_EFFECT) or c:IsReason(REASON_RULE)) and not c:IsReason(REASON_REPLACE)
end
function s.reptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return eg:IsExists(s.repfilter, 1, e:GetHandler(), e:GetHandler(), tp) and
                   not e:GetHandler():IsStatus(STATUS_DESTROY_CONFIRMED + STATUS_BATTLE_DESTROYED)
    end
    return Duel.SelectYesNo(tp, aux.Stringid(19333131, 0))
end
function s.repval(e, c)
    return s.repfilter(c, e:GetHandler(), e:GetHandlerPlayer())
end
function s.repop(e, tp, eg, ep, ev, re, r, rp)
    Duel.Destroy(e:GetHandler(), REASON_RULE + REASON_REPLACE)
end

function s.rellimit(e, c, tp, sumtp)
    return c == e:GetHandler()
end