--不滅階段 (KA)
function c577.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCost(c577.cost1)
	e1:SetTarget(c577.target1)
	e1:SetOperation(c577.activate1)
	c:RegisterEffect(e1)   
	--spsummon
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(577,1))
            e2:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e2:SetType(EFFECT_TYPE_QUICK_O)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCost(c577.cost2)
	e2:SetTarget(c577.target2)
	e2:SetOperation(c577.activate2)
	c:RegisterEffect(e2) 
end

function c577.cost2(e,tp,eg,ep,ev,re,r,rp,chk)
	local g1=Duel.GetMatchingGroup(Card.IsAbleToGraveAsCost,tp,LOCATION_MZONE,0,nil)
	if chk==0 then return g1:GetCount()>0 end
	local sg=g1:Select(tp,1,1,nil)
	Duel.SendtoGrave(sg,REASON_COST)
end

function c577.target2(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_GRAVE) and c577.spfilter(chkc,e,tp) and chkc:IsControler(tp) end
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingTarget(c577.spfilter,tp,LOCATION_GRAVE,0,1,nil,e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectTarget(tp,c577.spfilter,tp,LOCATION_GRAVE,0,1,1,nil,e,tp)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,1,0,0)
end

function c577.activate2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if not c:IsRelateToEffect(e) or not tc:IsRelateToEffect(e) then return end
	 Duel.SpecialSummon(tc,0,tp,tp,false,false,POS_FACEUP)
end

function c577.spfilter(c,e,tp)
	return c:IsCanBeSpecialSummoned(e,0,tp,false,false) and c:GetLevel()>6
end

function c577.cost1(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local g1=Duel.GetMatchingGroup(Card.IsAbleToGraveAsCost,tp,LOCATION_MZONE,0,nil)
	local g2=Duel.GetTargetCount(c577.spfilter,tp,LOCATION_GRAVE,0,nil,e,tp)
	 if g1:GetCount()>0 and g2>0 and Duel.SelectYesNo(tp,aux.Stringid(10028593,0)) then
		local sg=g1:Select(tp,1,1,nil)
		Duel.SendtoGrave(sg,REASON_COST)
		e:SetLabel(1)
                        e:SetCategory(CATEGORY_SPECIAL_SUMMON)
                        e:SetProperty(EFFECT_FLAG_CARD_TARGET)
	 end
end

function c577.target1(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_GRAVE) and c577.spfilter(chkc,e,tp) and chkc:IsControler(tp) end
	if chk==0 then return true end
            if e:GetLabel()==1 and Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingTarget(c577.spfilter,tp,LOCATION_GRAVE,0,1,nil,e,tp) then      
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectTarget(tp,c577.spfilter,tp,LOCATION_GRAVE,0,1,1,nil,e,tp)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,1,0,0) end
end

function c577.activate1(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if e:GetLabel()~=1 then return end
	local tc=Duel.GetFirstTarget()
	if not c:IsRelateToEffect(e) or not tc:IsRelateToEffect(e) then return end
            Duel.SpecialSummon(tc,0,tp,tp,false,false,POS_FACEUP)
end


