function aux.procAI(ttp)
	local e1=Effect.GlobalEffect()	
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP)
	e1:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e1:SetCode(EVENT_CHAINING)
   e1:SetCondition(function(e,tp) return Duel.GetFlagEffect(tp,1000)==0 end)
	e1:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_chain(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e1,ttp) 
   local e3=e1:Clone()	
   e3:SetCode(EVENT_ATTACK_ANNOUNCE)
   e3:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_attack(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e3,ttp)
   local e4=e3:Clone()	
	e4:SetCode(EVENT_BATTLE_DAMAGE)
	e4:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_batdam(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e4,ttp)
   local e5=e3:Clone()	
	e5:SetCode(EVENT_BE_BATTLE_TARGET)
	e5:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_battg(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e5,ttp)	
   local e6=e3:Clone()	
	e6:SetCode(EVENT_PRE_DAMAGE_CALCULATE)
	e6:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_damcal(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e6,ttp)	
   local e7=e3:Clone()	
	e7:SetCode(EVENT_BATTLE_CONFIRM)
	e7:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_batconfirm(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e7,ttp)	
   local e8=e1:Clone()	
   e8:SetCode(EVENT_SPSUMMON_SUCCESS)
	e8:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_sp(e,tp,eg,ep,ev,re,r,rp) end)	
	Duel.RegisterEffect(e8,ttp)		
   local e9=e1:Clone()	
   e9:SetCode(EVENT_SUMMON_SUCCESS)
	e9:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_sum(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e9,ttp)		
   local e10=e1:Clone()	
   e10:SetCode(EVENT_CUSTOM+94145021)
	e10:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse_draw(e,tp,eg,ep,ev,re,r,rp) end)
   Duel.RegisterEffect(e10,ttp)	

	local e2=Effect.GlobalEffect()	
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
	e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
   e2:SetCode(EVENT_PHASE_START+PHASE_MAIN1)
   e2:SetCountLimit(1)
	e2:SetCondition(function(e,tp) return Duel.GetFlagEffect(tp,1001)==0 end)
	e2:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse2(e,tp,eg,ep,ev,re,r,rp) end)
	Duel.RegisterEffect(e2,ttp) 	
	local e11=e2:Clone()
   e11:SetCode(EVENT_PHASE_START+PHASE_MAIN2)
	Duel.RegisterEffect(e11,ttp)
	local e12=Effect.GlobalEffect()	
	e12:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DELAY)
	e12:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
   e12:SetCode(EVENT_CHAIN_END)
	e12:SetCondition(function(e,tp) return Duel.GetFlagEffect(tp,1001)==0 end)
	e12:SetOperation(function(e,tp,eg,ep,ev,re,r,rp) 
	aux.reverse2(e,tp,eg,ep,ev,re,r,rp) end)      
   Duel.RegisterEffect(e12,ttp)
end

function aux.reverse_chain(e,tp,eg,ep,ev,re,r,rp)
   local code={58851034,14558127,59438930,67750322,73642296,13705015,77538567,10045474,97268402,511001766}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_attack(e,tp,eg,ep,ev,re,r,rp)
   local code={158,18964575,19665973}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_batdam(e,tp,eg,ep,ev,re,r,rp)
   local code={13724}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_damcal(e,tp,eg,ep,ev,re,r,rp)
   local code={11370082}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_battg(e,tp,eg,ep,ev,re,r,rp)
   local code={11370083}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_batconfirm(e,tp,eg,ep,ev,re,r,rp)
   local code={13720}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_sum(e,tp,eg,ep,ev,re,r,rp)
   local code={900000064,155,511002838}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_sp(e,tp,eg,ep,ev,re,r,rp)
   local code={13734,214,325,511002838,13705015,7850740,43138260,74003290,60643553}
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end
function aux.reverse_draw(e,tp,eg,ep,ev,re,r,rp)
   local code={11370078,10045474}   
   aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
end

function aux.treverse(code,e,tp,eg,ep,ev,re,r,rp)
   local g,g2   
   local gcode=0
   local reset=RESET_CHAIN
   local event=EVENT_CHAINING
   for _,code1 in ipairs(code) do
      if code1==7850740 then event=EVENT_SPSUMMON_SUCCESS end
      if code1==900000064 then event=EVENT_SUMMON_SUCCESS end
      if code1==60643553 then event=EVENT_SPSUMMON_SUCCESS end
      if code1==18964575 then event=EVENT_ATTACK_ANNOUNCE reset=reset+RESET_PHASE+PHASE_DAMAGE end
      if code1==19665973 then event=EVENT_ATTACK_ANNOUNCE reset=reset+RESET_PHASE+PHASE_DAMAGE end
      if code1==158 then event=EVENT_ATTACK_ANNOUNCE reset=reset+RESET_PHASE+PHASE_DAMAGE end
      if code1==13724 then event=EVENT_BATTLE_DAMAGE reset=reset+RESET_PHASE+PHASE_DAMAGE end
      if code1==11370082 then event=EVENT_PRE_DAMAGE_CALCULATE reset=reset+RESET_PHASE+PHASE_DAMAGE end
      if code1==11370083 then event=EVENT_BE_BATTLE_TARGET reset=reset+RESET_PHASE+PHASE_DAMAGE end
      if code1==13720 then event=EVENT_BATTLE_CONFIRM reset=reset+RESET_PHASE+PHASE_DAMAGE end
      if code1==74003290 then event=EVENT_SPSUMMON_SUCCESS end
      
      local loc=LOCATION_SZONE
      g=Duel.GetFirstMatchingCard(aux.AIcounter,tp,LOCATION_DECK,0,nil,code1)
      if not Duel.IsExistingMatchingCard(aux.AIcounter3,tp,LOCATION_HAND,0,1,nil,code1) or code1==13726 then loc=LOCATION_SZONE end
      if g and g:IsType(TYPE_MONSTER) then loc=LOCATION_HAND end
      if Duel.IsExistingMatchingCard(aux.AIcounter2,tp,loc,0,1,nil,code) and Duel.IsExistingMatchingCard(aux.AIcounter,tp,LOCATION_DECK,0,1,nil,code1) and not Duel.IsExistingMatchingCard(aux.AIcounter,tp,loc,0,1,nil,code1) then
         g=Duel.GetFirstMatchingCard(aux.AIcounter,tp,LOCATION_DECK,0,nil,code1)
         g2=Duel.GetFirstMatchingCard(aux.AIcounter2,tp,loc,0,nil,code)
         gcode=g2:GetOriginalCode()
         local te=g:GetActivateEffect()
         if g:IsType(TYPE_SPELL+TYPE_TRAP) then
            if g:CheckActivateEffect(false,false,false)==nil or te==nil then goto continue end
            local con=te:GetCondition()
            local cost=te:GetCost()
            local tg=te:GetTarget()
            if not ((con==nil or con(e,tp,eg,ep,ev,re,r,rp)) and (cost==nil or cost(e,tp,eg,ep,ev,re,r,rp)) and (tg==nil or tg(e,tp,eg,ep,ev,re,r,rp,0))) then goto continue end
            if g:IsType(TYPE_TRAP+TYPE_QUICKPLAY) and g2:IsLocation(LOCATION_SZONE) and g2:GetTurnID()==Duel.GetTurnCount() then goto continue end

            if code1==13705015 then 
               local cg=Duel.GetChainInfo(ev,CHAININFO_TARGET_CARDS)
               if not cg or cg:GetCount()<1 then goto continue end
               local cgcount=cg:GetCount()
               if cg:FilterCount(function(c) return c13705015.filter1(c,e,tp) and c:IsLocation(LOCATION_MZONE) and c:IsControler(tp) end,nil)==0 and cg:FilterCount(function(c) return c13705015.filter12(c,e,tp) and c:IsLocation(LOCATION_MZONE) and c:IsControler(tp) end,nil)==0 then goto continue end
            end  
   
            if code1==13720 then 
               if not (Duel.GetBattleDamage(tp)>=Duel.GetLP(tp)) then goto continue end
            end

            if code1==74003290 then 
               if eg:GetFirst():IsDisabled() then goto continue end
            end

         else  
            if not Duel.CheckEvent(event) then goto continue end
            local chkcon=true
            if code1==7850740 then chkcon=c7850740.spcon(e,tp,eg,ep,ev,re,r,rp) and c7850740.sptg(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==900000064 then chkcon=c900000064.spcon(e,tp,eg,ep,ev,re,r,rp) and c900000064.sptg(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==511002838 then chkcon=c511002838.spcon(e,g2) end
            if code1==14558127 then chkcon=c14558127.discon(e,tp,eg,ep,ev,re,r,rp) and c14558127.discost(e,tp,eg,ep,ev,re,r,rp,0) and c14558127.distg(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==59438930 then chkcon=c59438930.condition(e,tp,eg,ep,ev,re,r,rp) and c59438930.cost(e,tp,eg,ep,ev,re,r,rp,0) and c59438930.target(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==60643553 then chkcon=c60643553.condition(e,tp,eg,ep,ev,re,r,rp) and c60643553.cost(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==73642296 then chkcon=c73642296.discon(e,tp,eg,ep,ev,re,r,rp) and c73642296.discost(e,tp,eg,ep,ev,re,r,rp,0) and c73642296.distg(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==18964575 then chkcon=c18964575.condition(e,tp,eg,ep,ev,re,r,rp) and c18964575.cost(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==19665973 then chkcon=c19665973.condition(e,tp,eg,ep,ev,re,r,rp) and c19665973.target(e,tp,eg,ep,ev,re,r,rp,0) end
            if code1==97268402 then chkcon=c97268402.condition(e,tp,eg,ep,ev,re,r,rp) and c97268402.cost(e,tp,eg,ep,ev,re,r,rp,0) and c97268402.target(e,tp,eg,ep,ev,re,r,rp,0) end 
            if not chkcon then goto continue end
         end
   
         if event==EVENT_CHAINING then
            if code1==59438930 then 
               if rp==tp or not re:GetHandler():IsDestructable() then goto continue end
            elseif code1==10045474 then if rp==tp or re:GetHandler():IsImmuneToEffect(e) then goto continue end
            else if (rp==tp or re:GetHandler():IsDisabled()) then goto continue end 
            end 
         end
         
         if event==EVENT_ATTACK_ANNOUNCE and (code1==18964575 or code1==158) and eg:GetFirst():IsImmuneToEffect(e) then goto continue end
         if code1==158 and not ((Duel.IsExistingMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,1,nil,10045474) and c10045474.target(e,tp,eg,ep,ev,re,r,rp,0)) or (Duel.IsExistingMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,1,nil,11370078) and c11370078.condition(e,tp,eg,ep,ev,re,r,rp) and c11370078.target(e,tp,eg,ep,ev,re,r,rp,0)) or Duel.IsExistingMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,1,nil,94145021)) then goto continue end

         aux.change(g,g2,code1,gcode)

         if code1==158 then
            local g3=Duel.GetDecktopGroup(tp,1):GetFirst()
            local gcode3=94145021
            if Duel.IsExistingMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,1,nil,11370078) and not Duel.IsExistingMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,1,nil,94145021) and c11370078.condition(e,tp,eg,ep,ev,re,r,rp) and c11370078.target(e,tp,eg,ep,ev,re,r,rp,0) then gcode3=11370078 end
            if Duel.IsExistingMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,1,nil,10045474) and not Duel.IsExistingMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,1,nil,94145021) and c10045474.target(e,tp,eg,ep,ev,re,r,rp,0) then gcode3=10045474 end
            local g4=Duel.GetFirstMatchingCard(Card.IsCode,tp,LOCATION_DECK,0,nil,gcode3)
            if g3:GetOriginalCode()~=gcode3 then Duel.MoveSequence(g4,0) end 
         end

         Duel.RegisterFlagEffect(tp,1000,reset,0,1)   
         if eg==nil then eg=Group.CreateGroup() end
         if re==nil then re=Effect.GlobalEffect() end
         Duel.BreakEffect()
         Duel.RaiseEvent(eg,event,re,r,rp,ep,ev) 
         if eg:GetFirst()~=nil then Duel.RaiseSingleEvent(eg:GetFirst(),event,re,r,rp,ep,ev) end 
         break 
      end 
      ::continue::
   end
end

function aux.reverse2(e,tp,eg,ep,ev,re,r,rp)
   local g,g2   
   local gcode=0
   if Duel.GetTurnPlayer()~=tp then return end
   local code={511001766,7500772,70101178,392,11370078,83764719,12580477,67169062,115,511001421,13726,53129443,13732,155,511001632,511001765,43422537,513000011,355,100000582,13,13725,13705015,43138260,11370100,511002759,18144506}

   for _,code1 in ipairs(code) do
      local loc=LOCATION_HAND
      g=Duel.GetFirstMatchingCard(aux.AIcounter,tp,LOCATION_DECK,0,nil,code1)
      if not Duel.IsExistingMatchingCard(aux.AIcounter3,tp,LOCATION_HAND,0,1,nil,code1) or code1==13726 then loc=LOCATION_SZONE end
      if g and g:IsType(TYPE_MONSTER) then loc=LOCATION_HAND end
      if Duel.IsExistingMatchingCard(aux.AIcounter3,tp,loc,0,1,nil,code1) and not Duel.IsExistingMatchingCard(aux.AIcounter,tp,loc,0,1,nil,code1) and Duel.IsExistingMatchingCard(aux.AIcounter,tp,LOCATION_DECK,0,1,nil,code1) then
         g2=Duel.GetFirstMatchingCard(aux.AIcounter3,tp,loc,0,nil,code1)
         gcode=g2:GetOriginalCode()

         if not g:IsType(TYPE_MONSTER) then
            local te=g:GetActivateEffect()
            if te==nil then goto continue end
            local con=te:GetCondition()
            local cost=te:GetCost()
            local tg=te:GetTarget()
            if not ((con==nil or con(e,tp,eg,ep,ev,re,r,rp)) and (cost==nil or cost(e,tp,eg,ep,ev,re,r,rp)) and (tg==nil or tg(e,tp,eg,ep,ev,re,r,rp,0))) then goto continue end
            
            if code1==13732 then
               local enemyhighest=Duel.GetFieldGroup(1-tp,LOCATION_MZONE)
               local highatk=0 local enemyhighestc
               for atker in aux.Next(enemyhighest) do
                  if atker:GetAttack()>highatk then highatk=atker:GetAttack() enemyhighestc=atker end
               end
               local xz=Duel.GetMatchingGroupCount(c13732.filter1,tp,LOCATION_GRAVE+LOCATION_MZONE,LOCATION_GRAVE+LOCATION_MZONE,nil,e,tp)  
               if not (Duel.IsExistingTarget(c13732.filter1,tp,LOCATION_GRAVE+LOCATION_MZONE,LOCATION_GRAVE+LOCATION_MZONE,3,nil,e,tp) and not Duel.IsExistingMatchingCard(function(c) return c:IsCode(67926903) end,tp,LOCATION_GRAVE+LOCATION_REMOVED,0,1,nil) and highatk<xz*1000 and not enemyhighestc:IsHasEffect(EFFECT_INDESTRUCTABLE_BATTLE)) then goto continue end 
            end  
   
            if code1==53129443 and Duel.IsExistingMatchingCard(function(c) return c:IsCode(13726) and c:IsFacedown() and c:GetTurnID()~=Duel.GetTurnCount() end,tp,LOCATION_SZONE,0,1,nil) then
               if not Duel.IsExistingMatchingCard(function(c) return c13726.cfilter(c,tp) and c:IsDestructable() and Duel.IsExistingMatchingCard(c13726.opfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp) end,tp,LOCATION_MZONE,0,1,nil) then goto continue end   
            end   
   
            if code1==11370078 then
               if not Duel.IsExistingMatchingCard(function(c) return not c:IsDisabled() and c:IsCode(67926903) end,tp,LOCATION_MZONE,0,1,nil) then goto continue end 
            end   
   
            if code1==111011802 then
               if not Duel.IsExistingMatchingCard(function(c) return c:IsSetCard(0x95) end,tp,LOCATION_SZONE+LOCATION_HAND,0,1,nil) then goto continue end 
            end    
   
            if code1==10045474 then
               if not c10045474.handcon(e) then goto continue end 
            end  
   
            if code1==511002759 then
               if not Duel.IsExistingMatchingCard(function(c) return c:IsAttribute(ATTRIBUTE_WATER) and c:IsFacedown() end,tp,LOCATION_EXTRA,0,1,nil) then goto continue end 
            end 

         else
            if code1==392 then
               if not (Duel.IsPlayerCanSummon(tp) and g:IsSummonable(false,nil) and c392.target(e,tp,eg,ep,ev,re,r,rp,0)) then goto continue end 
            end 

            if code1==70101178 then
               if not ((c70101178.ntcon(e,g,0) and Duel.IsPlayerCanSummon(tp) and g:IsSummonable(false,nil)) or (c70101178.spcon(e,g) and Duel.IsPlayerCanSpecialSummon(tp))) then goto continue end 
            end 
   
            if code1==7500772 then
               if not ((c7500772.ntcon(e,g,0) and Duel.IsPlayerCanSummon(tp) and g:IsSummonable(false,nil)) or (c7500772.spcon(e,g) and Duel.IsPlayerCanSpecialSummon(tp))) then goto continue end 
            end  

            if code1~=392 and code1~=70101178 and code1~=7500772 and not (Duel.IsPlayerCanSummon(tp) and g:IsSummonable(false,nil)) then goto continue end
         end

         aux.change(g,g2,code1,gcode)
      end 
       ::continue::
   end
   
   Duel.RegisterFlagEffect(tp,1001,RESET_PHASE+PHASE_MAIN1+PHASE_MAIN2+RESET_CHAIN+RESET_EVENT+RESET_TOFIELD,0,1)   
end

function aux.AIdeckmonster(c,code)
   local chk=0
   for _,code1 in ipairs(code) do
     if c:GetOriginalCode()==code1 then chk=1 end
   end
   return chk==0 and c:IsType(TYPE_MONSTER) and c:IsFacedown()
end
function aux.AIcounter(c,code1)
   return c:GetOriginalCode()==code1 and c:IsFacedown()
end
function aux.AIcounter2(c,code)
   local chk=0
   for _,code1 in ipairs(code) do
     if c:GetOriginalCode()==code1 then chk=1 end
   end
   return chk==0 and (c:IsLocation(LOCATION_HAND) or (c:IsFacedown() and c:GetSequence()<5))
end
function aux.AIcounter3(c,code1)
   return c:GetOriginalCode()~=code1 and (c:IsLocation(LOCATION_HAND) or (c:IsFacedown() and c:GetSequence()<5))
end
function aux.AIcounter4(c)
   return (c:IsLocation(LOCATION_HAND) or (c:IsFacedown() and c:GetSequence()<5))
end

function aux.change(g,g2,code1,gcode)
   local seq2=g2:GetSequence() local seq=g:GetSequence()
   local tp=g:GetControler()
   if (g:IsType(TYPE_MONSTER) or g2:IsType(TYPE_MONSTER)) and g2:IsLocation(LOCATION_HAND) then
   Duel.Exile(g2,REASON_RULE) Duel.Exile(g,REASON_RULE)
   g2=Duel.CreateToken(tp,gcode) g=Duel.CreateToken(tp,code1)
   Duel.DisableShuffleCheck()
   Duel.SendtoHand(g,tp,REASON_RULE)
   Duel.BreakEffect()
   Duel.SendtoDeck(g2,tp,0,REASON_RULE)
   Duel.BreakEffect() 
   Duel.MoveSequence(g2,seq)
   else
   local ocode1=g:GetOriginalCodeRule() local ocode2=g2:GetOriginalCodeRule()   
   g2:SetEntityCode(code1,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
   g:SetEntityCode(gcode,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
   end
end