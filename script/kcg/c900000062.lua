--��Ĭħ��ʦLV0
function c900000062.initial_effect(c)
	
	--�Է��鿨ʱ���������͵ȼ�
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCategory(CATEGORY_ATKCHANGE)
	e1:SetCode(EVENT_DRAW)
	e1:SetCondition(c900000062.rcon)
	e1:SetOperation(c900000062.rop)
	c:RegisterEffect(e1)
	
	--���ϵ����ȼ�1�Ĺ���
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	--e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	--e2:SetRange(LOCATION_MZONE)
	e2:SetCode(EFFECT_CHANGE_LEVEL)
	e2:SetValue(c900000062.lv)
	--c:RegisterEffect(e2)

	--immune spell
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_IMMUNE_EFFECT)
	e3:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCondition(c900000062.econ)
	e3:SetValue(c900000062.efilter)
	c:RegisterEffect(e3)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000062.rcon(e,tp,eg,ep,ev,re,r,rp)
	return ep~=tp and e:GetHandler():GetLevel()<12
end

function c900000062.rop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local ct=eg:GetCount()
	local lv=ct+math.min(12-c:GetLevel()-ct,0)
	if c:IsFacedown() or not c:IsRelateToEffect(e) or c:GetLevel()>11 then return end
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(lv*500)
	c:RegisterEffect(e1)
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_UPDATE_LEVEL)
	e2:SetValue(lv)
	c:RegisterEffect(e2)
	--c:RegisterFlagEffect(900000062,0,0,0,c:GetLevel())
	Duel.RaiseSingleEvent(c,EVENT_LEVEL_UP,e,0,0,0,0,0)
end

function c900000062.lv(e)
	return e:GetHandler():GetFlagEffectLabel(900000062)
end

function c900000062.econ(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetLevel()>7
end
function c900000062.efilter(e,te)
	return te:IsActiveType(TYPE_SPELL) and te:GetOwnerPlayer()~=e:GetHandlerPlayer()
end
