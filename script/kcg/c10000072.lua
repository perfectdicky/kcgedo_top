--守护之神官
function c10000072.initial_effect(c)
	--在场上时幻神不会被战斗破坏
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTargetRange(LOCATION_MZONE,0)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsRACE_DIVINE))
	e1:SetValue(1)
	c:RegisterEffect(e1)
end
