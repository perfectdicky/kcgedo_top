--Numeron typhoon
function c13713.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	  e1:SetCondition(c13713.con)
	e1:SetTarget(c13713.target)
	e1:SetOperation(c13713.activate)
	c:RegisterEffect(e1)
end

function c13713.filter1(c)
	return c:IsFaceup() and c:IsSetCard(0x14b)
end
function c13713.con(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c13713.filter1,tp,LOCATION_MZONE,0,1,nil) 
end
function c13713.filter(c)
	return c:IsType(TYPE_SPELL+TYPE_TRAP) and aux.TRUE
end
function c13713.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return Duel.IsExistingMatchingCard(c13713.filter,tp,0,LOCATION_ONFIELD,1,c) end
	local sg=Duel.GetMatchingGroup(c13713.filter,tp,0,LOCATION_ONFIELD,c)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,sg,sg:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,1000)
end
function c13713.activate(e,tp,eg,ep,ev,re,r,rp)
	local sg=Duel.GetMatchingGroup(c13713.filter,tp,0,LOCATION_ONFIELD,e:GetHandler())
	if Duel.Destroy(sg,REASON_EFFECT)~=0 then
	  Duel.Damage(1-tp,1000,REASON_EFFECT) end
end
