--覇王龍ズァーク
local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	
	-- Level/Rank
	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_SET_AVAILABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_RANK_LEVEL)
	c:RegisterEffect(e0)
	
	--spsummon condition
	local e15=Effect.CreateEffect(c)
	e15:SetType(EFFECT_TYPE_SINGLE)
	e15:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e15:SetCode(EFFECT_SPSUMMON_CONDITION)
	e15:SetValue(s.splimit)
	c:RegisterEffect(e15)
	
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCode(EFFECT_CHANGE_CODE)
	e2:SetValue(13331639)
	c:RegisterEffect(e2)
    
	--spsummon immune
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE)
	e6:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e6)
	
	--destroy all
	local e7=Effect.CreateEffect(c)
	e7:SetDescription(aux.Stringid(13331639,1))
	e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)   
	e7:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e7:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e7:SetCode(EVENT_SPSUMMON_SUCCESS)
	e7:SetTarget(s.destg)
	e7:SetOperation(s.desop)
	c:RegisterEffect(e7)

	--immune
	local e16=Effect.CreateEffect(c)
	e16:SetType(EFFECT_TYPE_FIELD)
	e16:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e16:SetCode(EFFECT_IMMUNE_EFFECT)
	e16:SetRange(LOCATION_MZONE)
	e16:SetTargetRange(LOCATION_MZONE,0)
	e16:SetValue(s.efilter)
	c:RegisterEffect(e16)		
	
	local e22=Effect.CreateEffect(c)
	e22:SetType(EFFECT_TYPE_SINGLE)
	e22:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e22:SetRange(LOCATION_MZONE)
	e22:SetCode(EFFECT_UNRELEASABLE_EFFECT)
	e22:SetCondition(s.indcon)   
	e22:SetValue(s.refilter)
	c:RegisterEffect(e22)	
	
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e9:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e9:SetRange(LOCATION_MZONE)
	e9:SetCondition(s.indcon)   
	e9:SetValue(1)
	c:RegisterEffect(e9)
	local e6=e9:Clone()
	e6:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	c:RegisterEffect(e6)	
	local e11=e9:Clone()
	e11:SetCode(EFFECT_CANNOT_USE_AS_COST)
	--c:RegisterEffect(e11)   
	local e12=e9:Clone()
	e12:SetValue(s.imfilter2)   
	e12:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e12)
	local e13=e12:Clone()
	e13:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e13)
	local e14=e12:Clone()
	e14:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e14)   
	local e15=e12:Clone()
	e15:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e15)
	-- local e16=e9:Clone() 
	-- e16:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	-- c:RegisterEffect(e16)
	-- local e17=e9:Clone()
	-- e17:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	-- c:RegisterEffect(e17)   
	-- local e18=e17:Clone()
	-- e18:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	-- c:RegisterEffect(e18) 
	-- local e23=e17:Clone()
	-- e23:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	-- c:RegisterEffect(e23) 
	local e19=e9:Clone()
	e19:SetCode(EFFECT_IMMUNE_EFFECT)
	e19:SetValue(s.imfilter) 
	c:RegisterEffect(e19)
	
	--special summon
	local e10=Effect.CreateEffect(c)
	e10:SetDescription(aux.Stringid(13331639,2))
	e10:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DELAY)   
	e10:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e10:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e10:SetCode(EVENT_BATTLE_DESTROYING)
	e10:SetCondition(aux.bdocon)
	e10:SetTarget(s.sptg)
	e10:SetOperation(s.spop)
	c:RegisterEffect(e10)
    
 	--handes
	local e11=Effect.CreateEffect(c)
	e11:SetDescription(aux.Stringid(48739166,0))
	e11:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e11:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	e11:SetCode(EVENT_TO_HAND)
	e11:SetRange(LOCATION_MZONE)
	e11:SetCondition(s.hdcon)
	e11:SetTarget(s.hdtg)
	e11:SetOperation(s.hdop)
	c:RegisterEffect(e11)   
    
	local e8=Effect.CreateEffect(c)
	e8:SetDescription(aux.Stringid(27439792,1))
	e8:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e8:SetCode(EVENT_DESTROYED)
    e8:SetCategory(CATEGORY_TODECK+CATEGORY_TOHAND+CATEGORY_SPECIAL_SUMMON)
	e8:SetProperty(EFFECT_FLAG_DELAY)
	e8:SetCondition(s.pencon)
	e8:SetTarget(s.pentg)
	e8:SetOperation(s.penop)
	c:RegisterEffect(e8)    
end
s.listed_series={0x20f8}
s.listed_names={56}

function s.splimit(e, se, sp, st)
	return se:GetHandler():IsCode(56,57)
end

function s.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=Duel.GetFieldGroup(tp,0,LOCATION_MZONE)
	if chk==0 then return g:GetCount()>0 end
	local tc=g:GetFirst()
	local tatk=0
	while tc do
	local atk=tc:GetAttack()
	if atk<0 then atk=0 end
	tatk=tatk+atk
	tc=g:GetNext() end  
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,tatk)	
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetFieldGroup(tp,0,LOCATION_MZONE)
	if g:GetCount()>0 then
		Duel.Destroy(g,REASON_EFFECT)
		local g2=Duel.GetOperatedGroup()
		Duel.BreakEffect()
		local tc=g2:GetFirst()
		local tatk=0
		while tc do
		local atk=tc:GetPreviousAttackOnField()
		if atk<0 then atk=0 end
		tatk=tatk+atk
		tc=g2:GetNext() end 
		Duel.Damage(1-tp,tatk,REASON_EFFECT)
	end
end

function s.efilter(e,te)
	--local tc=te:GetHandler()
	return te:GetOwnerPlayer()~=e:GetHandlerPlayer() 
	and (te:IsActiveType(TYPE_XYZ+TYPE_FUSION+TYPE_SYNCHRO+TYPE_RITUAL) and te:IsActiveType(TYPE_MONSTER))
end

function s.ndcfilter(c)
	return c:IsFaceup() 
	and (c:IsType(TYPE_XYZ+TYPE_FUSION+TYPE_SYNCHRO+TYPE_RITUAL) and c:IsType(TYPE_MONSTER))
end

function s.indfilter(c,tpe)
	return (c:IsLocation(LOCATION_GRAVE) or c:IsFaceup()) and c:IsType(tpe) and c:IsType(TYPE_MONSTER)
end
function s.indcon(e)
	return Duel.IsExistingMatchingCard(s.indfilter,0,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,nil,TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ+TYPE_PENDULUM+TYPE_RITUAL+TYPE_LINK)
end

function s.refilter(e,te)
	return not (te:GetOwner():IsCode(57) and te:GetOwnerPlayer()==e:GetOwnerPlayer())
end

function s.imfilter(e,re)
    return e:GetHandlerPlayer()~=re:GetHandlerPlayer()
end

function s.imfilter2(e,te)
	if not te then return false end
	return not (te:GetHandler():IsLocation(LOCATION_EXTRA) and te:GetCode()==EFFECT_SPSUMMON_PROC) and e:GetOwner()~=te:GetOwner()
end

function s.spfilter(c,e,tp)
	if not c:IsCanBeSpecialSummoned(e,0,tp,false,false) or not c:IsSetCard(0x20f8) or not c:IsType(TYPE_MONSTER) then return false end
	local type=TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ
	if c:IsType(TYPE_PENDULUM+TYPE_LINK) then type=TYPE_PENDULUM+TYPE_LINK end
	if not c:IsLocation(LOCATION_EXTRA) then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
	else return Duel.GetLocationCountFromEx(tp,tp,nil,type)>0 end
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=Duel.GetMatchingGroup(s.spfilter,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA,0,nil,e,tp)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,math.min(2,#g),tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
	local g=Duel.GetMatchingGroup(s.spfilter, tp, LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA, 0, nil, e, tp)
	local ft=math.min(2,Duel.GetUsableMZoneCount(tp))
	if #g>0 then
		--local rg=Group.CreateGroup()
		--for i=1,ft do
			local ft1=Duel.GetLocationCountFromEx(tp)
			local ft2=Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ)
			local ft3=Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_PENDULUM+TYPE_LINK)
			local ft4=Duel.GetLocationCount(tp,LOCATION_MZONE)
			if Duel.IsPlayerAffectedByEffect(tp,CARD_BLUEEYES_SPIRIT) then
				if ft1>0 then ft1=1 end
				if ft2>0 then ft2=1 end
				if ft3>0 then ft3=1 end
				if ft4>0 then ft4=1 end
				ft=1
			end
			--local rg2=g:FilterSelect(tp,s.rescon,1,1,nil,ft1,ft2,ft3)
			--if #rg2<1 then break end
			--rg:Merge(rg2)
			--Duel.SpecialSummon(rg2,0,tp,tp,false,false,POS_FACEUP)
			local rg2=aux.SelectUnselectGroup(g,e,tp,1,ft,s.rescon(ft1,ft2,ft3,ft4,ft),1,tp,HINTMSG_SPSUMMON)
			if #rg2<1 then return end
			Duel.SpecialSummon(rg2,0,tp,tp,false,false,POS_FACEUP)
			--g:Sub(rg2)
	    --end
		--rg:ForEach(function(c) c:CompleteProcedure() end)
	end
end
function s.exfilter1(c)
	return c:IsFacedown() and c:IsType(TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ) and c:IsLocation(LOCATION_EXTRA)
end
function s.exfilter2(c)
	return c:IsType(TYPE_LINK) or (c:IsFaceup() and c:IsType(TYPE_PENDULUM)) and c:IsLocation(LOCATION_EXTRA)
end
function s.exfilter3(c)
	return not c:IsLocation(LOCATION_EXTRA)
end
function s.rescon(ft1,ft2,ft3,ft4,ft)
	return	function(sg,e,tp,mg)
				local exnpct=sg:FilterCount(s.exfilter1,nil)
				local expct=sg:FilterCount(s.exfilter2,nil)
				local nexpct=sg:FilterCount(s.exfilter3,nil)
				local groupcount=#sg
				local res=ft2>=exnpct and ft3>=expct and ft4>=nexpct and ft>=groupcount
				return res, not res
			end
end

function s.cfilter(c,tp)
	return c:IsControler(tp) and c:IsPreviousLocation(LOCATION_DECK)
end
function s.hdcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetCurrentPhase()~=PHASE_DRAW and eg:IsExists(s.cfilter,1,nil,1-tp)
end
function s.hdtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsRelateToEffect(e) and e:GetHandler():IsFaceup() end
	--Duel.SetOperationInfo(0,CATEGORY_DESTROY,eg,eg:GetCount(),tp,LOCATION_HAND)
end
function s.hdop(e,tp,eg,ep,ev,re,r,rp)
	local zg=eg:Filter(function(c) return not c:IsType(TYPE_TOKEN) end,nil)
	if zg:GetCount()<1 then return end
	Duel.Overlay(e:GetHandler(), zg)
end

function s.pencon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return c:IsPreviousLocation(LOCATION_MZONE) and c:IsFaceup()
end
function s.sppfilter2(c,e,tp)
	return c:IsCode(76794549) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function s.pentg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(s.sppfilter2,LOCATION_DECK+LOCATION_GRAVE,0,1,nil,e,tp) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 end
    Duel.SetOperationInfo(0,CATEGORY_TODECK,e:GetHandler(),1,tp,LOCATION_DECK)
    Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_DECK+LOCATION_GRAVE)
    Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_HAND)
end
function s.sppfilter(c)
	return c:IsCode(57) and c:IsAbleToHand()
end
function s.penop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 then
		Duel.SendtoDeck(c, tp, 2, REASON_EFFECT)
        local sg=Duel.SelectMatchingCard(tp,s.sppfilter2,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil,e,tp)
        if sg:GetCount()<1 then return end
        Duel.SpecialSummon(sg,0,tp,tp,false,false,POS_FACEUP)
        Duel.BreakEffect()
        Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOHAND)
        local g=Duel.SelectMatchingCard(tp,s.sppfilter,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil)
        if g:GetCount()<1 then return end
        Duel.SendtoHand(g, tp, REASON_EFFECT)
	end
end