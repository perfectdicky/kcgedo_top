--Sin 青眼の白龍
function c90.initial_effect(c)
		c:EnableReviveLimit()

		--special summon
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_SPSUMMON_PROC)
		e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
		e1:SetRange(LOCATION_HAND)
		e1:SetCondition(c90.spcon)
		e1:SetOperation(c90.spop)
		c:RegisterEffect(e1)

		--only 1 can exists
		--local e2=Effect.CreateEffect(c) 
		--e2:SetType(EFFECT_TYPE_SINGLE) 
		--e2:SetCode(EFFECT_CANNOT_FLIP_SUMMON) 
		--e2:SetCondition(c9433350.excon) 
		--c:RegisterEffect(e2) 
		--local e3=e2:Clone() 
		--e3:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON) 
		--c:RegisterEffect(e3) 

		local e4=Effect.CreateEffect(c) 
	  e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e4:SetType(EFFECT_TYPE_SINGLE) 
		e4:SetCode(EFFECT_CANNOT_SUMMON) 
		c:RegisterEffect(e4) 
		local e5=e4:Clone() 
		e5:SetCode(EFFECT_CANNOT_MSET) 
		c:RegisterEffect(e5) 

		--selfdes
		local e7=Effect.CreateEffect(c)
		e7:SetType(EFFECT_TYPE_SINGLE)
		e7:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e7:SetRange(LOCATION_MZONE)
		e7:SetCode(EFFECT_SELF_DESTROY)
		e7:SetCondition(c90.descon)
		c:RegisterEffect(e7)
		--local e8=Effect.CreateEffect(c) 
		--e8:SetType(EFFECT_TYPE_FIELD) 
		--e8:SetCode(EFFECT_SELF_DESTROY) 
		--e8:SetRange(LOCATION_MZONE) 
		--e8:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE) 
		--e8:SetTarget(c9433350.destarget) 
		--c:RegisterEffect(e8) 

		--cannot announce
		--local e8=Effect.CreateEffect(c) 
		--e8:SetType(EFFECT_TYPE_FIELD) 
		--e8:SetRange(LOCATION_MZONE) 
		--e8:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE) 
		--e8:SetTargetRange(LOCATION_MZONE,0) 
		--e8:SetTarget(c9433350.antarget) 
		--c:RegisterEffect(e8) 
end
c90.listed_names={27564031}

function c90.sumlimit(e,c)
		return c:IsSetCard(0x23)
end
function c90.exfilter(c)
		return c:IsFaceup() and c:IsSetCard(0x23)
end
function c90.excon(e)
		return Duel.IsExistingMatchingCard(c90.exfilter,0,LOCATION_MZONE,LOCATION_MZONE,1,nil)
end
function c90.spfilter(c)
		return c:IsCode(89631139) and c:IsAbleToGraveAsCost()
end
function c90.spcon(e,c)
		if c==nil then return true end
		return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0
			   and Duel.IsExistingMatchingCard(c90.spfilter,c:GetControler(),LOCATION_HAND+LOCATION_DECK,0,1,nil)
end
function c90.spop(e,tp,eg,ep,ev,re,r,rp,c)
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
		local tg=Duel.SelectMatchingCard(tp,c90.spfilter,tp,LOCATION_HAND+LOCATION_DECK,0,1,1,nil)
		Duel.SendtoGrave(tg,REASON_COST)
end

function c90.descon(e)
	local c=e:GetHandler()
	local f1=Duel.GetFieldCard(0,LOCATION_SZONE,5)
	local f2=Duel.GetFieldCard(1,LOCATION_SZONE,5)
	return ((f1==nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and (f2==nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end
function c90.destarget(e,c)
		return c:IsSetCard(0x23) and c:GetFieldID()>e:GetHandler():GetFieldID()
end

function c90.antarget(e,c)
		return c~=e:GetHandler()
end
