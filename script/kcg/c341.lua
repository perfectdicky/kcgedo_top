--Strain Bomb (K)
function c341.initial_effect(c)
	local e2=Effect.CreateEffect(c)
	e2:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_PLAYER_TARGET)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e2:SetCode(EVENT_DESTROYED)
	e2:SetTarget(c341.tg)
	e2:SetOperation(c341.op)
	c:RegisterEffect(e2)
end

function c341.filter(c)
	return aux.TRUE and c:IsSetCard(0x905) and c:IsFaceup()
end
function c341.tg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingTarget(c341.filter,tp,LOCATION_MZONE,0,1,nil) end
	local g=Duel.GetMatchingGroup(c341.filter,tp,LOCATION_MZONE,0,nil)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,2000)
end
function c341.op(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c341.filter,tp,LOCATION_MZONE,0,nil)
      if Duel.Destroy(g,REASON_EFFECT)~=0 then
	Duel.Damage(1-p,2000,REASON_EFFECT) end      
end
