--Numeron typhoon
function c610.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY+CATEGORY_TOHAND)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	  e1:SetCondition(c610.con)
	e1:SetTarget(c610.target)
	e1:SetOperation(c610.activate)
	c:RegisterEffect(e1)
end

function c610.filter1(c)
	return c:IsFaceup() and c:IsSetCard(0x14b)
end
function c610.con(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c610.filter1,tp,LOCATION_MZONE,0,1,nil) 
end
function c610.filter(c)
	return aux.TRUE
end
function c610.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return Duel.IsExistingMatchingCard(c610.filter,tp,0,LOCATION_MZONE,1,c) end
	local sg=Duel.GetMatchingGroup(c610.filter,tp,0,LOCATION_MZONE,c)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,sg,sg:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK)
end
function c610.filter2(c)
	return c:IsCode(41418852) and c:IsAbleToHand()
end
function c610.activate(e,tp,eg,ep,ev,re,r,rp)
	local sg=Duel.GetMatchingGroup(c610.filter,tp,0,LOCATION_MZONE,e:GetHandler())
	if Duel.Destroy(sg,REASON_EFFECT)~=0 then
	  Duel.BreakEffect()
	  if Duel.IsExistingMatchingCard(c610.filter2,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,nil) then
	    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	    local g=Duel.SelectMatchingCard(tp,c610.filter2,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil)
	    Duel.SendtoHand(g,nil,REASON_EFFECT) end end
end
