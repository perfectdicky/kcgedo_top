-- 太阳神之翼神龙（AC）
local s, id = GetID()
function s.initial_effect(c)
    -- c:SetUniqueOnField(1,1,129)

    local e04 = Effect.CreateEffect(c)
    e04:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e04:SetType(EFFECT_TYPE_SINGLE)
    e04:SetCode(EFFECT_CANNOT_SUMMON)
    c:RegisterEffect(e04)
    local e05 = e04:Clone()
    e05:SetCode(EFFECT_CANNOT_MSET)
    c:RegisterEffect(e05)

    -- special summon
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_SPSUMMON_PROC)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e1:SetRange(LOCATION_HAND)
    e1:SetCondition(s.spcon)
    e1:SetOperation(s.spop)
    c:RegisterEffect(e1)

    -- selfdes
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE)
    e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCode(EFFECT_SELF_DESTROY)
    e2:SetCondition(s.descon)
    c:RegisterEffect(e2)

    -- spson
    local e11 = Effect.CreateEffect(c)
    e11:SetType(EFFECT_TYPE_SINGLE)
    e11:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e11:SetCode(EFFECT_SPSUMMON_CONDITION)
    e11:SetValue(aux.FALSE)
    c:RegisterEffect(e11)

    local te = Effect.CreateEffect(c)
    te:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    te:SetRange(LOCATION_MZONE)
    te:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    te:SetCountLimit(1)
    te:SetCode(EVENT_PHASE + PHASE_END)
    te:SetOperation(s.reop2)
    c:RegisterEffect(te)

    -- 支付LP1000除外对方场上所有怪兽
    local e6 = Effect.CreateEffect(c)
    e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e6:SetDescription(aux.Stringid(10000011, 2))
    e6:SetCategory(CATEGORY_DESTROY)
    e6:SetType(EFFECT_TYPE_IGNITION)
    e6:SetRange(LOCATION_MZONE)
    e6:SetCountLimit(1)
    e6:SetCost(s.descost)
    e6:SetTarget(s.destg)
    e6:SetOperation(s.desop)
    c:RegisterEffect(e6)

    -- 解放怪兽增加攻击
    local e70 = Effect.CreateEffect(c)
    e70:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e70:SetDescription(aux.Stringid(10000011, 0))
    e70:SetCategory(CATEGORY_ATKCHANGE)
    e70:SetType(EFFECT_TYPE_IGNITION)
    e70:SetRange(LOCATION_MZONE)
    e70:SetCountLimit(1)
    e70:SetCost(s.otkcost2)
    e70:SetOperation(s.otkop2)
    c:RegisterEffect(e70)

    -- LP减至1增加攻击力和守备力
    local e7 = Effect.CreateEffect(c)
    e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e7:SetDescription(aux.Stringid(110000010, 2))
    e7:SetCategory(CATEGORY_ATKCHANGE)
    e7:SetType(EFFECT_TYPE_IGNITION)
    e7:SetRange(LOCATION_MZONE)
    e7:SetCountLimit(1)
    e7:SetCost(s.otkcost)
    e7:SetCondition(s.otkcon)
    e7:SetOperation(s.otkop)
    -- c:RegisterEffect(e7)

    -- 不受陷阱效果以及魔法效果怪兽生效一回合
    local e82 = Effect.CreateEffect(c)
    e82:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e82:SetCode(EVENT_CHAIN_SOLVED)
    e82:SetRange(LOCATION_MZONE)
    e82:SetCondition(s.sdcon2)
    e82:SetOperation(s.sdop2)
    c:RegisterEffect(e82)
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e8:SetRange(LOCATION_MZONE)
    e8:SetCode(EFFECT_IMMUNE_EFFECT)
    e8:SetValue(s.efilter)
    c:RegisterEffect(e8)

    -- 不会被卡的效果破坏、除外、返回手牌和卡组、送入墓地、无效化、改变控制权、变为里侧表示、成为特殊召唤素材
    local e100 = Effect.CreateEffect(c)
    e100:SetType(EFFECT_TYPE_SINGLE)
    e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e100:SetRange(LOCATION_MZONE)
    e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
    e100:SetValue(s.lffilter)
    c:RegisterEffect(e100)
    local e101 = e100:Clone()
    e101:SetCode(EFFECT_CANNOT_REMOVE)
    e101:SetValue(1)
    c:RegisterEffect(e101)
    local e102 = e101:Clone()
    e102:SetCode(EFFECT_CANNOT_TO_HAND)
    c:RegisterEffect(e102)
    local e103 = e102:Clone()
    e103:SetCode(EFFECT_CANNOT_TO_DECK)
    c:RegisterEffect(e103)
    local e104 = e103:Clone()
    e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
    c:RegisterEffect(e104)
    local e105 = e104:Clone()
    e105:SetCode(EFFECT_CANNOT_DISABLE)
    c:RegisterEffect(e105)
    local e106 = e105:Clone()
    e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
    c:RegisterEffect(e106)
    local e107 = e106:Clone()
    e107:SetCode(EFFECT_CANNOT_TURN_SET)
    c:RegisterEffect(e107)
    local e108 = e107:Clone()
    e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
    c:RegisterEffect(e108)
    local e109 = e108:Clone()
    e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
    c:RegisterEffect(e109)
    local e110 = e109:Clone()
    e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
    c:RegisterEffect(e110)
    local e111 = e109:Clone()
    e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
    c:RegisterEffect(e111)
end
s.listed_names = {27564031}

function s.spfilter(c)
    return (c:IsCode(10000010) or c:IsCode(708)) and c:IsAbleToGraveAsCost()
end
function s.spcon(e, c)
    if c == nil then
        return true
    end
    return Duel.GetLocationCount(c:GetControler(), LOCATION_MZONE) > 0 and
               Duel.IsExistingMatchingCard(s.spfilter, c:GetControler(), LOCATION_HAND + LOCATION_DECK, 0, 1, nil) and
               Duel.IsExistingMatchingCard(Card.IsSetCard, c:GetControler(), LOCATION_GRAVE, 0, 3, nil, 0x23)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
    local tg = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_HAND + LOCATION_DECK, 0, 1, 1, nil)
    Duel.SendtoGrave(tg, REASON_COST)
end

function s.descon(e)
    local c = e:GetHandler()
    local f1 = Duel.GetFieldCard(0, LOCATION_SZONE, 5)
    local f2 = Duel.GetFieldCard(1, LOCATION_SZONE, 5)
    return ((f1 == nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and
               (f2 == nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end

function s.tgcon(e, tp, eg, ep, ev, re, r, rp)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL and
               not e:GetHandler():GetPreviousLocation() == LOCATION_HAND
end
function s.tgtg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
        Duel.SetOperationInfo(0, CATEGORY_TOGRAVE, e:GetHandler(), 1, 0, 0)
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
        Duel.SetOperationInfo(0, CATEGORY_REMOVE, e:GetHandler(), 1, 0, 0)
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
        Duel.SetOperationInfo(0, CATEGORY_TODECK, e:GetHandler(), 1, 0, 0)
    end
end
function s.tgop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsRelateToEffect(e) and c:IsFaceup() then
        if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
            Duel.SendtoGrave(c, REASON_RULE)
        end
        if e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
            Duel.Remove(c, 0, REASON_RULE)
        end
        if e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
            Duel.SendtoDeck(c, nil, 2, REASON_RULE)
        end
    end
end

function s.descost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.CheckLPCost(tp, 1000)
    end
    Duel.PayLPCost(tp, 1000)
end
function s.destg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsExistingMatchingCard(nil, tp, 0, LOCATION_MZONE, 1, nil)
    end
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetProperty(EFFECT_FLAG_OATH)
    e1:SetCode(EFFECT_CHANGE_CODE)
    e1:SetValue(10000049)
    e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
    -- e:GetHandler():RegisterEffect(e1)
    local g = Duel.GetMatchingGroup(nil, tp, 0, LOCATION_MZONE, nil)
    Duel.SetOperationInfo(0, CATEGORY_DESTROY, g, g:GetCount(), 0, 0)
end

function s.desop(e, tp, eg, ep, ev, re, r, rp)
    local g = Duel.GetMatchingGroup(nil, tp, 0, LOCATION_MZONE, nil)
    Duel.Destroy(g, REASON_EFFECT)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_OATH)
    e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e1:SetRange(LOCATION_MZONE)
    e1:SetValue(1)
    e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
    e:GetHandler():RegisterEffect(e1)
    local e2 = Effect.CreateEffect(e:GetHandler())
    e2:SetType(EFFECT_TYPE_FIELD)
    e2:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
    e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_OATH)
    e2:SetRange(LOCATION_MZONE)
    e2:SetTargetRange(1, 0)
    e2:SetValue(1)
    e2:SetReset(RESET_EVENT + 0x1ff0000 + RESET_PHASE + PHASE_END)
    e:GetHandler():RegisterEffect(e2)
    local e3 = e2:Clone()
    e3:SetCode(EFFECT_IMMUNE_EFFECT)
    e3:SetValue(s.eefilter)
    e:GetHandler():RegisterEffect(e3)
end
function s.eefilter(e, te)
    return te:GetOwner() ~= e:GetOwner()
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.otkcost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.GetLP(tp) > 1
    end
    local lp = Duel.GetLP(tp)
    Duel.SetLP(tp, 1)
    e:SetLabel(lp - 1)
end
function s.otkcon(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    return not c:IsType(TYPE_FUSION)
end
function s.otkop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsFaceup() and c:IsRelateToEffect(e) then
        c:RegisterFlagEffect(10000012, RESET_EVENT + 0x1ff0000, nil, 1)
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
        e1:SetRange(LOCATION_MZONE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(e:GetLabel())
        e1:SetReset(RESET_EVENT + 0x1ff0000)
        c:RegisterEffect(e1)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_UPDATE_DEFENSE)
        c:RegisterEffect(e2)
    end
end

function s.otkcost2(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.CheckReleaseGroup(tp, nil, 1, e:GetHandler())
    end
    local g = Duel.SelectReleaseGroup(tp, nil, 1, 99, e:GetHandler())
    local tc = g:GetFirst()
    local tatk = 0
    local tdef = 0
    while tc do
        local atk = tc:GetAttack()
        local def = tc:GetDefense()
        if atk < 0 then
            atk = 0
        end
        if def < 0 then
            def = 0
        end
        tatk = tatk + atk
        tdef = tdef + def
        tc = g:GetNext()
    end
    e:SetLabelObject({tatk,tdef})
    Duel.Release(g, REASON_COST)
end
function s.otkop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tatk=e:GetLabelObject()[1]
    local tdef=e:GetLabelObject()[2]
    if c:IsFaceup() and c:IsRelateToEffect(e) then
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
        e1:SetRange(LOCATION_MZONE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(tatk)
        e1:SetReset(RESET_EVENT + 0x1fe0000)
        c:RegisterEffect(e1)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_UPDATE_DEFENSE)
        e2:SetValue(tdef)
        c:RegisterEffect(e2)
    end
end

function s.efilter(e, te)
    local c = e:GetHandler()
    local tc = te:GetOwner()
    local turncount = Duel.GetTurnCount() - tc:GetTurnID()
    if c:GetTurnID() == Duel.GetTurnCount() then
        turncount = 0
    end
    if tc == e:GetOwner() or te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then
        return false
    else
        return te:GetActiveType() == TYPE_TRAP
    end
end

function s.sdcon2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=1
                    break 
                end
            end
        end
    end
    return eff==1
end
function s.sdop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=te
                    break 
                end
            end
        end
    end
    if eff==0 then return end
    local e83 = Effect.CreateEffect(c)
    e83:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e83:SetRange(LOCATION_MZONE)
    e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
    e83:SetCountLimit(1)
    e83:SetCode(EVENT_PHASE + PHASE_END)
    e83:SetLabelObject(eff)
    e83:SetOperation(s.setop2)
    e83:SetReset(RESET_PHASE + PHASE_END)
    c:RegisterEffect(e83) 
end
function s.setop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local te = e:GetLabelObject()
    if not te or te==0 then return end
    te:Reset()
end   

function s.atcon(e)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL
end

function s.atlimit(e, c)
    return c ~= e:GetHandler()
end

function s.tgvalue(e, re, rp)
    return rp ~= e:GetHandlerPlayer() and re:GetHandler():GetTurnID() ~= Duel.GetTurnCount()
end

function s.lffilter(e, re, rp)
    return re:GetOwner() ~= e:GetHandler()
end

function s.reop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if te:GetOwner() ~= e:GetOwner() and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then
                    local e80 = Effect.CreateEffect(c)
                    e80:SetType(EFFECT_TYPE_SINGLE)
                    e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
                    e80:SetRange(LOCATION_MZONE)
                    e80:SetCode(EFFECT_IMMUNE_EFFECT)
                    e80:SetValue(function(e, te2)
                        return te2 == te
                    end)
                    c:RegisterEffect(e80)
                end
            end
        end
    end
end
