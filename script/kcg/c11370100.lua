--Rank-Up-Magic The Seventh One
function c11370100.initial_effect(c)
	aux.addsetcard(c,0x912)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(13717,7))
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c11370100.target)
	e1:SetOperation(c11370100.activate)
	c:RegisterEffect(e1)
end

function c11370100.filter1(c,e,tp)
	local no=c.xyz_number
	return Duel.IsPlayerCanSpecialSummonCount(tp,2) 
		and no and no>=101 and no<=107 and c:IsSetCard(0x48) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x2048) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false) and not c:IsHasEffect(EFFECT_NECRO_VALLEY)
		and Duel.IsExistingMatchingCard(c11370100.filter2,tp,LOCATION_EXTRA,0,1,nil,e,tp,no,c)
end
function c11370100.filter2(c,e,tp,no,tc)
	return c.xyz_number==no and c:IsSetCard(0x1048) and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false) and tc:IsCanBeXyzMaterial(c) and Duel.GetLocationCountFromEx(tp,tp,tc,c)>0
end
function c11370100.filter3(c,e,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x48) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x2048) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
	and Duel.IsExistingMatchingCard(c11370100.filter2,tp,LOCATION_EXTRA,0,1,nil,e,tp,no,c)
end
function c11370100.filter5(c,e,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x48) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x2048) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
		and ((not c:IsLocation(LOCATION_MZONE) and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false) and not c:IsHasEffect(EFFECT_NECRO_VALLEY) and Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_XYZ)>0) 
			or (c:IsLocation(LOCATION_MZONE) and Duel.GetLocationCountFromEx(tp,tp,c)>0)) 
		and Duel.IsExistingMatchingCard(c11370100.filter2,tp,LOCATION_EXTRA,0,1,nil,e,tp,no,c)
end
function c11370100.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local loc=0
	local ect=c92345028 and Duel.IsPlayerAffectedByEffect(tp,92345028) and c92345028[tp]
	if Duel.GetLocationCount(tp,LOCATION_MZONE)>0 then loc=loc+LOCATION_GRAVE+LOCATION_REMOVED end
	if Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_XYZ)>0 and (ect==nil or ect>1) then loc=loc+LOCATION_EXTRA end
	if chk==0 then return (loc~=0 and Duel.IsExistingMatchingCard(c11370100.filter1,tp,loc,0,1,nil,e,tp))
			or Duel.IsExistingMatchingCard(c11370100.filter3,tp,LOCATION_MZONE,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA)
end
function c11370100.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local loc=0
	local ect=c92345028 and Duel.IsPlayerAffectedByEffect(tp,92345028) and c92345028[tp]
	if Duel.GetLocationCount(tp,LOCATION_MZONE)>0 then loc=loc+LOCATION_GRAVE end
	if Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_XYZ)>0 and (ect==nil or ect>1) then loc=loc+LOCATION_EXTRA end
	local g1=Group.CreateGroup()
	if not Duel.IsExistingMatchingCard(c11370100.filter3,tp,LOCATION_MZONE,0,1,nil,e,tp) then
	  if loc==0 then return end
	  Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	  g1=Duel.SelectMatchingCard(tp,c11370100.filter1,tp,loc,0,1,1,nil,e,tp)
	else g1=Duel.SelectMatchingCard(tp,c11370100.filter5,tp,loc+LOCATION_MZONE,0,1,1,nil,e,tp) end
	local tc1=g1:GetFirst()
	if tc1 then
		if not tc1:IsLocation(LOCATION_MZONE) then
		if Duel.SpecialSummonStep(tc1,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc1:RegisterEffect(e1,true)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		tc1:RegisterEffect(e2,true)
		Duel.SpecialSummonComplete()
		tc1:CompleteProcedure() end end
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local g2=Duel.SelectMatchingCard(tp,c11370100.filter2,tp,LOCATION_EXTRA,0,1,1,nil,e,tp,tc1.xyz_number,tc1)
		local tc2=g2:GetFirst()
		if tc2 then
				  local mg=tc1:GetOverlayGroup()
				  Duel.BreakEffect()
			  if mg:GetCount()~=0 then
			   Duel.Overlay(tc2,mg)
			  end 
			  tc2:SetMaterial(g1)
			Duel.Overlay(tc2,g1)
			Duel.SpecialSummon(tc2,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
			tc2:CompleteProcedure()
		end
	end
end