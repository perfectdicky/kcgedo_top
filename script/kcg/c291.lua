--Rocket Hermos Cannon
function c291.initial_effect(c)
	--fusion material
	c:EnableReviveLimit()

	
	--Activate
	local e0=Effect.CreateEffect(c)
	e0:SetDescription(aux.Stringid(13708,6))
	e0:SetCategory(CATEGORY_EQUIP)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e0:SetTarget(c291.target)
	e0:SetOperation(c291.operation)
	c:RegisterEffect(e0)

	--Hermos Cannon Blast!
	local e7=Effect.CreateEffect(c)
	e7:SetDescription(aux.Stringid(13708,8))
	  e7:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE) 
	e7:SetType(EFFECT_TYPE_IGNITION)
	e7:SetRange(LOCATION_SZONE)
	  e7:SetCondition(c291.cond2)
	e7:SetTarget(c291.tar)
	e7:SetOperation(c291.act)
	c:RegisterEffect(e7)

	--Equip Limit
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_EQUIP_LIMIT)
	e3:SetValue(c291.eqlimit)
	c:RegisterEffect(e3)
end

function c291.filter(c)
	return c:IsFaceup() 
end
function c291.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:GetLocation()==LOCATION_MZONE and c291.filter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(c291.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EQUIP)
	Duel.SelectTarget(tp,c291.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_EQUIP,e:GetHandler(),1,0,0)
end
function c291.operation(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if e:GetHandler():IsRelateToEffect(e) and tc:IsRelateToEffect(e) and tc:IsFaceup() then
		Duel.Equip(tp,e:GetHandler(),tc)
	end
end

function c291.cond(e)
	return Duel.GetAttacker()==e:GetHandler():GetEquipTarget() 
end

function c291.cond2(e)
	return Duel.GetCurrentPhase()==PHASE_MAIN1
end
function c291.eqlimit(e,c)
	return c:IsFaceup()
end

function c291.tar(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetMatchingGroup(aux.TRUE,tp,0,LOCATION_MZONE,nil) end
	local g=Duel.GetMatchingGroup(aux.TRUE,tp,0,LOCATION_MZONE,nil)
	local tc=g:GetFirst() 
	  local dam=0
	  while tc do
	local atk=tc:GetAttack() 
		if atk<0 then atk=0 end 
		dam=dam+atk 
		tc=g:GetNext() 
	  end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0,nil)
	  Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,dam) 
end
function c291.dfilter(c,e)
	return aux.TRUE and not c:IsImmuneToEffect(e)
end
function c291.act(e,tp,eg,ep,ev,re,r,rp)
   if not e:GetHandler():IsFaceup() then return end
   local g=Duel.GetMatchingGroup(c291.dfilter,tp,0,LOCATION_MZONE,nil,e)
   --local sum=g:GetSum(Card.GetAttack)  
	local tc=g:GetFirst() 
	  local dam=0
	  while tc do
	local atk=tc:GetAttack() 
		if atk<0 then atk=0 end 
		dam=dam+atk 
		tc=g:GetNext() 
	  end
   if Duel.Destroy(g,REASON_EFFECT)>0 then
   Duel.Damage(1-tp,dam,REASON_EFFECT)

	   local e1=Effect.CreateEffect(e:GetHandler())
	   e1:SetType(EFFECT_TYPE_FIELD)
	   e1:SetCode(EFFECT_CANNOT_ATTACK)
	   e1:SetRange(LOCATION_SZONE)
	   e1:SetTargetRange(LOCATION_MZONE,0)
	   e1:SetReset(RESET_EVENT+0x1fc0000+RESET_PHASE+PHASE_END)
	   e:GetHandler():RegisterEffect(e1) end
end
