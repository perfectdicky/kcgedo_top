--Legend of Heart
function c292.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_POSITION+CATEGORY_TOHAND)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	--e1:SetCountLimit(1,292)
	e1:SetCost(c292.cost)
	e1:SetTarget(c292.target)
	e1:SetOperation(c292.operation)
	c:RegisterEffect(e1)
	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_SZONE)
	e100:SetCode(EFFECT_CANNOT_DISABLE)
	e100:SetValue(1)
	c:RegisterEffect(e100)
end
function c292.costfilter(c,code)
	return c:IsCode(code) and c:IsAbleToRemoveAsCost()
end
function c292.cost2filter(c,tp)
	return c:IsRace(RACE_WARRIOR) 
	and Duel.GetLocationCountFromEx(tp,tp,c,TYPE_FUSION)>2
end
function c292.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckLPCost(tp,1000) and Duel.CheckReleaseGroup(tp,c292.cost2filter,1,nil,tp) 
	  and Duel.IsExistingMatchingCard(c292.costfilter,tp,LOCATION_DECK+LOCATION_HAND+LOCATION_ONFIELD+LOCATION_GRAVE,0,1,nil,1784686)
	  and Duel.IsExistingMatchingCard(c292.costfilter,tp,LOCATION_DECK+LOCATION_HAND+LOCATION_ONFIELD+LOCATION_GRAVE,0,1,nil,11082056)
	  and Duel.IsExistingMatchingCard(c292.costfilter,tp,LOCATION_DECK+LOCATION_HAND+LOCATION_ONFIELD+LOCATION_GRAVE,0,1,nil,46232525) 
    end
	Duel.PayLPCost(tp,1000)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_RELEASE)
	local rg=Duel.SelectReleaseGroup(tp,Card.IsRace,1,1,nil,RACE_WARRIOR)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g1=Duel.SelectMatchingCard(tp,c292.costfilter,tp,LOCATION_DECK+LOCATION_ONFIELD+LOCATION_HAND+LOCATION_GRAVE,0,1,1,nil,1784686)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g2=Duel.SelectMatchingCard(tp,c292.costfilter,tp,LOCATION_DECK+LOCATION_ONFIELD+LOCATION_HAND+LOCATION_GRAVE,0,1,1,nil,11082056)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g3=Duel.SelectMatchingCard(tp,c292.costfilter,tp,LOCATION_DECK+LOCATION_ONFIELD+LOCATION_HAND+LOCATION_GRAVE,0,1,1,nil,46232525)
	g1:Merge(g2)
	g1:Merge(g3)
	Duel.DisableShuffleCheck()
	Duel.Remove(g1,POS_FACEUP,REASON_COST)
	Duel.DisableShuffleCheck()
	Duel.Release(rg,REASON_COST)
end
function c292.spfilter(c,e,tp,code)
	return c:IsCode(code) and c:IsCanBeSpecialSummoned(e,0,tp,true,true)
end
function c292.filter3(c,e,tp,code)
	return c:IsFaceup() and (c:IsCode(1784686) or c:IsCode(11082056) or c:IsCode(46232525))
end
function c292.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chk==0 then return (not ect or ect>=3) and Duel.CheckReleaseGroup(tp,c292.cost2filter,1,nil,tp) 
	  and not Duel.IsPlayerAffectedByEffect(tp,59822133)
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,3,0,0)
end
function c292.operation(e,tp,eg,ep,ev,re,r,rp)
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	if Duel.IsPlayerAffectedByEffect(tp,59822133) or (ect and ect<3) or not Duel.IsPlayerCanSpecialSummonCount(tp,3) then return end	   
	  local tc1=Duel.CreateToken(tp,293,nil,nil,nil,nil,nil,nil)
	  local tc2=Duel.CreateToken(tp,294,nil,nil,nil,nil,nil,nil)
	  local tc3=Duel.CreateToken(tp,295,nil,nil,nil,nil,nil,nil)
	local ft=Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION)
	if ft<3 then return end
	Duel.SendtoDeck(tc1,tp,0,REASON_RULE)
	Duel.SendtoDeck(tc2,tp,0,REASON_RULE)
	Duel.SendtoDeck(tc3,tp,0,REASON_RULE)
	Duel.SpecialSummonStep(tc1,0,tp,tp,true,true,POS_FACEUP)
	Duel.SpecialSummonStep(tc2,0,tp,tp,true,true,POS_FACEUP)
	Duel.SpecialSummonStep(tc3,0,tp,tp,true,true,POS_FACEUP)
	Duel.SpecialSummonComplete()
	tc1:CompleteProcedure()
	tc2:CompleteProcedure()
	tc3:CompleteProcedure()
end
