--CNo.104 仮面魔踏士アンブラル
local s, id = GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,5,4)
	c:EnableReviveLimit()

	  --cannot destroyed
		local e0=Effect.CreateEffect(c)
	  e0:SetType(EFFECT_TYPE_SINGLE)
	  e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	  e0:SetValue(s.indes)
	  c:RegisterEffect(e0)

	--destroy
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(49456901,0))
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetCode(EVENT_SPSUMMON_SUCCESS)
	e1:SetTarget(s.destg)
	e1:SetOperation(s.desop)
	c:RegisterEffect(e1)

	local e223=Effect.CreateEffect(c)
	e223:SetDescription(aux.Stringid(49456901,1))
	e223:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e223:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e223:SetRange(LOCATION_MZONE)
	e223:SetCode(EVENT_CHAINING)
	e223:SetCondition(s.condition23)
	e223:SetOperation(s.operation23)
	c:RegisterEffect(e223)
	--negate activate
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(49456901,1))
	e2:SetCategory(CATEGORY_HANDES)
	--e2:SetProperty(0)   
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCondition(s.condition)
	e2:SetCost(s.cost)
	e2:SetTarget(s.target)
	e2:SetOperation(s.operation)
	c:RegisterEffect(e2,false,REGISTER_FLAG_DETACH_XMAT)
	local e224=Effect.CreateEffect(c)
	e224:SetDescription(aux.Stringid(49456901,1))
	e224:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DELAY)
	e224:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e224:SetRange(LOCATION_MZONE)
	e224:SetCode(EVENT_TURN_END)
	e224:SetCountLimit(1)
	e224:SetOperation(s.operation24)
	c:RegisterEffect(e224)

	--negate activate
	local e22=Effect.CreateEffect(c)
	e22:SetDescription(aux.Stringid(49456901,1))
	e22:SetCategory(CATEGORY_NEGATE+CATEGORY_HANDES)
	e22:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e22:SetType(EFFECT_TYPE_QUICK_O)
	e22:SetRange(LOCATION_MZONE)
	e22:SetCode(EVENT_CHAINING)
	e22:SetCondition(s.condition2)
	e22:SetCost(s.cost)
	e22:SetTarget(s.target2)
	e22:SetOperation(s.operation2)
	c:RegisterEffect(e22,false,REGISTER_FLAG_DETACH_XMAT)

	  local e3=Effect.CreateEffect(c)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)	  
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	  e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	  e3:SetCondition(s.con)
	  e3:SetOperation(s.op)
	  c:RegisterEffect(e3)
end
s.xyz_number=104
s.listed_series = {0x48}

function s.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) 
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048) and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end

function s.desfilter(c)
	return c:IsType(TYPE_SPELL+TYPE_TRAP) and aux.TRUE
end
function s.destg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsOnField() and s.desfilter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(s.desfilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	local g=Duel.SelectTarget(tp,s.desfilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,1,0,0)
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc:IsRelateToEffect(e) then
		Duel.Destroy(tc,REASON_EFFECT)
	end
end

function s.condition(e,tp,eg,ep,ev,re,r,rp,chk)
	  return e:GetHandler():GetFlagEffect(257)~=0 and e:GetHandler():GetFlagEffect(256)~=0
end
function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetFieldGroupCount(tp,0,LOCATION_HAND)>0 end
end
function s.operation(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetFieldGroupCount(1-tp,LOCATION_HAND,0)>0 then
		local g=Duel.GetFieldGroup(1-tp,LOCATION_HAND,0):RandomSelect(1-tp,1)
		Duel.SendtoGrave(g,REASON_EFFECT)
		Duel.SetLP(1-tp,Duel.GetLP(1-tp)/2)
	end
end

function s.condition2(e,tp,eg,ep,ev,re,r,rp,chk)
	local loc=Duel.GetChainInfo(ev,CHAININFO_TRIGGERING_LOCATION)
	return not e:GetHandler():IsStatus(STATUS_BATTLE_DESTROYED) and ep~=tp and (loc==LOCATION_HAND or loc==LOCATION_MZONE)
		and re:IsActiveType(TYPE_MONSTER) and Duel.IsChainNegatable(ev) 
		and e:GetHandler():GetFlagEffect(257)~=0
end
function s.target2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_NEGATE,eg,1,0,0)
end
function s.operation2(e,tp,eg,ep,ev,re,r,rp)
	if Duel.NegateActivation(ev) and Duel.GetFieldGroupCount(1-tp,LOCATION_HAND,0)>0 and Duel.SelectYesNo(tp,aux.Stringid(49456901,2)) then
		local g=Duel.GetFieldGroup(1-tp,LOCATION_HAND,0):RandomSelect(1-tp,1)
		Duel.SendtoGrave(g,REASON_EFFECT)
		Duel.SetLP(1-tp,Duel.GetLP(1-tp)/2)
	end
end

function s.condition23(e,tp,eg,ep,ev,re,r,rp,chk)
	local loc=Duel.GetChainInfo(ev,CHAININFO_TRIGGERING_LOCATION)
	return not e:GetHandler():IsStatus(STATUS_BATTLE_DESTROYED) and ep~=tp and (loc==LOCATION_HAND or loc==LOCATION_MZONE)
		and re:IsActiveType(TYPE_MONSTER) and e:GetHandler():GetFlagEffect(257)~=0
end
function s.operation23(e,tp,eg,ep,ev,re,r,rp)
	e:GetHandler():RegisterFlagEffect(256,RESET_EVENT+0x1ff0000,0,1)
end
function s.operation24(e,tp,eg,ep,ev,re,r,rp)
	e:GetHandler():ResetFlagEffect(256)
end

function s.con(e,tp,eg,ep,ev,re,r,rp) 
	  return e:GetHandler():GetSummonType()==SUMMON_TYPE_XYZ and
	  (e:GetHandler():GetOverlayGroup():IsExists(Card.IsCode,1,nil,2061963))
end
function s.op(e,tp,eg,ep,ev,re,r,rp)
	  e:GetHandler():RegisterFlagEffect(257,RESET_EVENT+0x1ff0000,0,1)
end
