--奧雷卡爾克斯聖母EX (KA)
function c556.initial_effect(c)
	--fusion material
	c:EnableReviveLimit()
	--Fusion.AddProcFunRep(c,aux.FilterBoolFunction(c556.splimit2),2,true)

	--spsummon condition
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(c556.splimit)
	c:RegisterEffect(e1)	
	
	--special summon rule
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SPSUMMON_PROC)
	e2:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e2:SetRange(LOCATION_EXTRA)
	e2:SetCondition(c556.sprcon)
	e2:SetOperation(c556.sprop)
	c:RegisterEffect(e2)
	
	--atkup
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e3:SetRange(LOCATION_MZONE)
	e3:SetValue(c556.atkval)
	c:RegisterEffect(e3)	
	
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(2407234,1))
	e4:SetType(EFFECT_TYPE_IGNITION)
	e4:SetRange(LOCATION_ONFIELD)
	e4:SetTarget(c556.sptg)
	e4:SetOperation(c556.spop)
	c:RegisterEffect(e4)	

	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(31986289,0))
	e5:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_TOKEN)
	e5:SetType(EFFECT_TYPE_IGNITION)
	e5:SetRange(LOCATION_ONFIELD)
	e5:SetCountLimit(1)
	e5:SetTarget(c556.sptg2)
	e5:SetOperation(c556.spop2)
	c:RegisterEffect(e5)	
end
c556.listed_series={0x900}
c556.material_setcode={0x900}

function c556.splimit2(c)
	return c:IsSetCard(0x900) and c:GetLevel()>4
end

function c556.splimit(e,se,sp,st)
	return e:GetHandler():GetLocation()~=LOCATION_EXTRA
end

function c556.spfilter(c)
	return c:IsSetCard(0x900) and c:GetLevel()>4 and c:IsCanBeFusionMaterial() 
	and (c:IsAbleToDeckAsCost() or c:IsAbleToExtraAsCost()) 
end
function c556.spfilter1(c,tp,g,sc)
	return g:IsExists(c556.spfilter21,1,c,tp,c,sc)
end
function c556.spfilter21(c,tp,tc,sc)
	return Duel.GetLocationCountFromEx(tp,tp,Group.FromCards(c,tc),sc)>0
end
function c556.sprcon(e,c)
	if c==nil then return true end 
	local tp=c:GetControler()
	local g=Duel.GetMatchingGroup(c556.spfilter,tp,LOCATION_MZONE,0,nil)
	return g:IsExists(c556.spfilter1,1,nil,tp,g,e:GetHandler())
end
function c556.sprop(e,tp,eg,ep,ev,re,r,rp,c)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(tp,c556.spfilter,tp,LOCATION_MZONE,0,2,2,nil)
	local tc=g:GetFirst()
	while tc do
		if not tc:IsFaceup() then Duel.ConfirmCards(1-tp,tc) end
		tc=g:GetNext()
	end
	Duel.SendtoDeck(g,nil,2,REASON_COST)
end

function c556.vfilter(c)
	return c:IsSetCard(0x900) and c:IsFaceup() and c:IsType(TYPE_MONSTER) 
	and (c:IsLocation(LOCATION_MZONE) or c:GetFlagEffect(12)~=0)
end
function c556.atkval(e,c)
	return Duel.GetMatchingGroupCount(c556.vfilter,c:GetControler(),LOCATION_ONFIELD,0,nil)*500
end

function c556.cfilter(c)
	return c:IsType(TYPE_EFFECT) and c:IsSetCard(0x900)
end
function c556.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c556.cfilter,tp,LOCATION_DECK,0,1,nil) and e:GetHandler():GetFlagEffect(556)==0 end
	e:GetHandler():RegisterFlagEffect(556,RESET_EVENT+0x1ff0000,0,1)
end
function c556.spop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	local g=Duel.SelectMatchingCard(tp,c556.cfilter,tp,LOCATION_DECK,0,1,1,nilp)
	if g:GetCount()>0 then
		local code=g:GetFirst():GetOriginalCode()
		e:GetHandler():CopyEffect(code, RESET_EVENT+0x1fe0000,1)
	end
end

function c556.sptg2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and Duel.IsPlayerCanSpecialSummonMonster(tp,557,0x900,0,0,2500,4,RACE_DIVINE,ATTRIBUTE_LIGHT) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,0)
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,1,tp,0)
end
function c556.spop2(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)==0 then return end
	if not Duel.IsPlayerCanSpecialSummonMonster(tp,557,0x900,0,0,2500,4,RACE_DIVINE,ATTRIBUTE_LIGHT) then return end
	local token=Duel.CreateToken(tp,557)
	Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
		local e1=Effect.CreateEffect(token)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
		e1:SetRange(LOCATION_MZONE)
		e1:SetTargetRange(LOCATION_MZONE,0)
		e1:SetTarget(c556.atktg) 
		e1:SetValue(aux.imval1)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		token:RegisterEffect(e1)
	Duel.SpecialSummonComplete()
end
function c556.atktg(e,c)
	return not (c:IsFaceup() and c:IsCode(557))
end
