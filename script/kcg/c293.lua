--Legendary Knight Timaeus
function c293.initial_effect(c)
	c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	local e001=Effect.CreateEffect(c)
	e001:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e001:SetType(EFFECT_TYPE_SINGLE)
	e001:SetCode(EFFECT_OVERINFINITE_ATTACK)
	c:RegisterEffect(e001)	
	local e002=Effect.CreateEffect(c)
	e002:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e002:SetType(EFFECT_TYPE_SINGLE)
	e002:SetCode(EFFECT_OVERINFINITE_DEFENSE)
	c:RegisterEffect(e002)			

	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(aux.FALSE)
	c:RegisterEffect(e1)

	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e0:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e0:SetCode(EVENT_ADJUST)
	e0:SetRange(LOCATION_MZONE)
	e0:SetOperation(c293.adjustop)
	c:RegisterEffect(e0)
	local g=Group.CreateGroup()
	g:KeepAlive()
	e0:SetLabelObject(g)

local e2=Effect.CreateEffect(c)
e2:SetDescription(aux.Stringid(2978414,0))
e2:SetType(EFFECT_TYPE_QUICK_O)
e2:SetRange(LOCATION_MZONE)
e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_CANNOT_DISABLE)
e2:SetCode(EVENT_FREE_CHAIN)
--e2:SetCost(c293.con)
e2:SetTarget(c293.tar) 
e2:SetOperation(c293.op)
e2:SetCountLimit(1)
c:RegisterEffect(e2)

	--if not c293.global_check then
		--c293.global_check=true
		--c293[0]=0
		--c293[1]=0
		local ge1=Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_CHAINING)
		ge1:SetOperation(c293.checkop1)
		--Duel.RegisterEffect(ge1,0)
		local ge2=Effect.CreateEffect(c)
		ge2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge2:SetCode(EVENT_CHAIN_NEGATED)
		ge2:SetOperation(c293.checkop2)
		--Duel.RegisterEffect(ge2,0)
		local ge3=Effect.CreateEffect(c)
		ge3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge3:SetCode(EVENT_PHASE_START+PHASE_DRAW)
		ge3:SetOperation(c293.clear)
		--Duel.RegisterEffect(ge3,0)
		local ge4=Effect.CreateEffect(c)
		ge4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge4:SetCode(EVENT_CHAINING)
		ge4:SetOperation(c293.checkop3)
		--Duel.RegisterEffect(ge4,0)
	--end

	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_CANNOT_DISABLE)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_DISEFFECT)
	c:RegisterEffect(e101)
	--特殊召唤不会被无效化
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e9)

	--set
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(80019195,1))
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e3:SetCode(EVENT_BE_BATTLE_TARGET)
	e3:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e3:SetTarget(c293.settg)
	e3:SetOperation(c293.setop)
	c:RegisterEffect(e3)	
end

function c293.adfilter(c)
return c:IsFaceup() and c:IsSetCard(0x900) and c:IsType(TYPE_FIELD)
end
function c293.adjustop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local pg=e:GetLabelObject()
	if c:GetFlagEffect(293)==0 then
		c:RegisterFlagEffect(293,RESET_EVENT+0x1ff0000,0,1)
		pg:Clear()
	end
	local g=Duel.GetMatchingGroup(c293.adfilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
	local dg=g:Filter(c293.adfilter,nil)
	if dg:GetCount()==0 or Duel.Destroy(dg,REASON_RULE)==0 then
		pg:Clear()
		pg:Merge(g)
	else
		pg:Clear()
		pg:Merge(g)
		Duel.Readjust()
	end
end

function c293.spfilter(c,tc1)
	return c:IsAbleToRemove() and c:IsFaceup() and c~=tc1
end
function c293.spfilter1(c)
	local tp=c:GetControler()
	return c:IsCode(293) and c:IsAbleToRemove() and c:IsFaceup()
	and Duel.IsExistingMatchingCard(c293.spfilter2,tp,LOCATION_MZONE,0,1,c,c)
end
function c293.spfilter2(c,tc1)
	local tp=c:GetControler()
	return c:IsAbleToRemove() and c:IsFaceup()
	and Duel.GetLocationCountFromEx(tp,tp,Group.FromCards(c,tc1),TYPE_FUSION)>0
end
function c293.con(e,tp,eg,ep,ev,re,r,rp)
	return c293[0]>0 and c293[1]>0
end
function c293.tar(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c293.spfilter1,tp,LOCATION_MZONE,0,1,nil) end
end
function c293.op(e,tp,eg,ep,ev,re,r,rp,c)
	if Duel.IsExistingMatchingCard(c293.spfilter1,tp,LOCATION_MZONE,0,1,nil) then
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g1=Duel.SelectMatchingCard(tp,c293.spfilter1,tp,LOCATION_MZONE,0,1,1,nil)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g2=Duel.SelectMatchingCard(tp,c293.spfilter2,tp,LOCATION_MZONE,0,1,99,g1:GetFirst(),g1:GetFirst())
	-- local g3=Group.CreateGroup()
	-- if Duel.IsExistingMatchingCard(c293.spfilter,tp,LOCATION_MZONE,0,1,g2:GetFirst(),g1:GetFirst()) and Duel.SelectYesNo(tp,aux.Stringid(82734805,0)) then
	-- Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	-- g3=Duel.SelectMatchingCard(tp,c293.spfilter,tp,LOCATION_MZONE,0,1,1,g2:GetFirst(),g1:GetFirst())
	-- g1:Merge(g3) end
	g1:Merge(g2)
	local code=296
	-- if (g2:GetFirst():IsType(TYPE_FUSION) and g3 and g3:GetFirst():IsType(TYPE_SYNCHRO)) or (g2:GetFirst():IsType(TYPE_SYNCHRO) and g3 and g3:GetFirst():IsType(TYPE_FUSION)) then code=806 end
	-- if (g2:GetFirst():IsType(TYPE_XYZ) and g3 and g3:GetFirst():IsType(TYPE_LINK)) or (g2:GetFirst():IsType(TYPE_LINK) and g3 and g3:GetFirst():IsType(TYPE_XYZ)) then code=807 end
	local g4=Duel.CreateToken(tp,code,nil,nil,nil,nil,nil,nil)
	--if g1:FilterCount(c293.checkatk,nil)>0 then g4:RegisterFlagEffect(999999,0,nil,0) end
	Duel.Remove(g1,POS_FACEUP,REASON_EFFECT+REASON_MATERIAL+REASON_FUSION)
	Duel.SendtoDeck(g4,tp,0,REASON_RULE)
	g4:SetMaterial(g1)
	Duel.SpecialSummon(g4,SUMMON_TYPE_FUSION,tp,tp,true,true,POS_FACEUP)
	g4:CompleteProcedure() end
end
function c293.checkatk(c)
	return c:GetFlagEffect(999999)~=0
end

function c293.checkop1(e,tp,eg,ep,ev,re,r,rp)
	if re:GetHandler():GetCode()==294 and re:GetHandler():GetControler()==e:GetHandler():GetControler() then
		c293[0]=c293[0]+1
	end
end
function c293.checkop2(e,tp,eg,ep,ev,re,r,rp)
	if re:GetHandler():GetCode()==294 and re:GetHandler():GetControler()==e:GetHandler():GetControler() then
		c293[0]=c293[0]-1
	end
	if re:GetHandler():GetCode()==295 and re:GetHandler():GetControler()==e:GetHandler():GetControler() then
		c293[1]=c293[1]-1
	end
end
function c293.checkop3(e,tp,eg,ep,ev,re,r,rp)
	if re:GetHandler():GetCode()==295 and re:GetHandler():GetControler()==e:GetHandler():GetControler() then
		c293[1]=c293[1]+1
	end
end
function c293.clear(e,tp,eg,ep,ev,re,r,rp)
	c293[0]=0
	c293[1]=0
end

function c293.filter2(c,tcode,e,tp)
	return c:IsCode(tcode) and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,false)
end

function c293.setfilter(c)
	return c:IsType(TYPE_SPELL) and c:IsSSetable()
end
function c293.settg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsControler(tp) and chkc:IsLocation(LOCATION_GRAVE) and s.setfilter(chkc) end
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_SZONE)>0
		and Duel.IsExistingTarget(c293.setfilter,tp,LOCATION_GRAVE,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SET)
	local g=Duel.SelectTarget(tp,c293.setfilter,tp,LOCATION_GRAVE,0,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_LEAVE_GRAVE,g,1,0,0)
end
function c293.setop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc:IsRelateToEffect(e) and Duel.GetLocationCount(tp,LOCATION_SZONE)>0 then
		Duel.SSet(tp,tc)
	end
end
