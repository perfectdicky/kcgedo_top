--奧雷卡爾克斯邪魔導 (KA)
function c558.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,aux.FilterBoolFunction(Card.IsSetCard,0x900),4,4)
	c:EnableReviveLimit()
	-- immue s
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_ONFIELD)
	e1:SetCode(EFFECT_IMMUNE_EFFECT)
	e1:SetValue(c558.efilter)
	c:RegisterEffect(e1)
	--lp set
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(558,0))
	e2:SetType(EFFECT_TYPE_TRIGGER_F+EFFECT_TYPE_SINGLE)
	e2:SetCode(EVENT_LEAVE_FIELD)
	e2:SetCondition(c558.slcon)
	e2:SetOperation(c558.slop)
	c:RegisterEffect(e2)
end
c558.listed_series={0x900}
c558.material_setcode={0x900}

function c558.efilter(e,te)
	return te:GetOwnerPlayer()==e:GetHandlerPlayer() 
end

function c558.slcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsReason(REASON_DESTROY)
end

function c558.slop(e,tp,eg,ep,ev,re,r,rp)
	Duel.SetLP(1-tp,1)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CHANGE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1,0)
	e1:SetValue(0)
	e1:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e1,tp)
	local e2=e1:Clone()
	e2:SetCode(EFFECT_NO_EFFECT_DAMAGE)
	e2:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e2,tp)
end

