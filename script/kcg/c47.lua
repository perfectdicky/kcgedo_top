--E·HERO ゴッド·ネオス
function c47.initial_effect(c)
	c:EnableReviveLimit()
	Fusion.AddProcMixRep(c,true,true,c47.ffilter,2,2,c47.ffilter1,c47.ffilter2,c47.ffilter3)

	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_FUSION_MATERIAL)
	e1:SetCondition(c47.fuscon)
	e1:SetOperation(c47.fusop)
	--c:RegisterEffect(e1)

	--copy
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(31111109,0))
	e2:SetCategory(CATEGORY_ATKCHANGE)
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCountLimit(1)
	e2:SetTarget(c47.copytg)
	e2:SetOperation(c47.copyop) 
	c:RegisterEffect(e2)

	--spsummon condition
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e3:SetCode(EFFECT_SPSUMMON_CONDITION)
	e3:SetValue(aux.fuslimit)
	c:RegisterEffect(e3)
end
c47.material_setcode={0x8,0x9,0x1f}
c47.listed_series={0x1f}

function c47.ffilter(c,fc,sumtype,tp)
	return c:IsSetCard(0x9,fc,sumtype,tp) or c:IsSetCard(0x1f,fc,sumtype,tp) or c:IsSetCard(0x8,fc,sumtype,tp)
end
function c47.ffilter1(c,fc,sumtype,tp)
	return c:IsSetCard(0x9,fc,sumtype,tp)
end
function c47.ffilter2(c,fc,sumtype,tp)
	return c:IsSetCard(0x1f,fc,sumtype,tp)
end
function c47.ffilter3(c,fc,sumtype,tp)
	return c:IsSetCard(0x8,fc,sumtype,tp)
end

function c47.fuscon(e,g,gc,chkf)
	if g==nil then return false end
	if gc then return false end
	local g1=g:Filter(Card.IsSetCard,nil,0x9)
	local c1=g1:GetCount()
	local g2=g:Filter(Card.IsSetCard,nil,0x1f)
	local c2=g2:GetCount()
	local g3=g:Filter(Card.IsSetCard,nil,0x8)
	local c3=g3:GetCount()
	local ag=g1:Clone()
	ag:Merge(g2)
	ag:Merge(g3)
	if chkf~=PLAYER_NONE and not ag:IsExists(aux.FConditionCheckF,1,nil,chkf) then return false end
	return c1>0 and c2>0 and c3>0 and ag:GetCount()>=5 and (c1>1 or c3>1 or g1:GetFirst()~=g3:GetFirst())
end
function c47.fusop(e,tp,eg,ep,ev,re,r,rp,gc,chkf)
	if gc then return end
	local g1=eg:Filter(Card.IsSetCard,nil,0x9)
	local g2=eg:Filter(Card.IsSetCard,nil,0x1f)
	local g3=eg:Filter(Card.IsSetCard,nil,0x8)
	local ag=g1:Clone()
	ag:Merge(g2)
	ag:Merge(g3)
	local tc=nil local f1=0 local f2=0 local f3=0
	local mg=Group.CreateGroup()
	for i=1,5 do
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
		if i==1 and chkf~=PLAYER_NONE then
			tc=ag:FilterSelect(tp,aux.FConditionCheckF,1,1,nil,chkf):GetFirst()
		else tc=ag:Select(tp,1,1,nil):GetFirst() end
		if g1:IsContains(tc) then
			g1:RemoveCard(tc)
			if g3:IsContains(tc) then g3:RemoveCard(tc) f1=f1+1 f3=f3+1
			else f1=f1+2 end
		elseif g2:IsContains(tc) then g2:RemoveCard(tc) f2=f2+1
		else g3:RemoveCard(tc) f3=f3+2 end
		ag:RemoveCard(tc)
		mg:AddCard(tc)
		if i==3 then
			if f1==0 and f2==0 then ag:Sub(g3)
			elseif f1==0 and f3==0 then ag:Sub(g2)
			elseif f2==0 and f3==0 then ag:Sub(g1) end
		end
		if i==4 then
			if f1==0 then ag=g1
			elseif f2==0 then ag=g2
			elseif f3==0 then ag=g3
			elseif f1==1 and f3==1 then ag:Sub(g2) end
		end
	end
	Duel.SetFusionMaterial(mg)
end

function c47.spfilter(c,code)
	if c:GetCode()~=code then return false end
	if c:IsType(TYPE_FUSION) then return c:IsAbleToExtraAsCost() 
	else return c:IsAbleToDeckAsCost() end
end
function c47.spcon(e,c)
	if c==nil then return true end 
	local tp=c:GetControler()
	local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if ft<-4 then return false end
	local g1=Duel.GetMatchingGroup(Card.IsCode,tp,LOCATION_MZONE,0,nil,89943723)
	local g2=Duel.GetMatchingGroup(Card.IsSetCard,tp,LOCATION_MZONE+LOCATION_GRAVE,0,nil,0x1f)
	if g1:GetCount()==0 or g2:GetCount()<6 then return false end
	  if ft>0 then return true end
	local f1=g1:FilterCount(Card.IsLocation,nil,LOCATION_MZONE)
	local f2=g2:FilterCount(Card.IsLocation,nil,LOCATION_MZONE+LOCATION_GRAVE)
	--if ft==-4 then return f1+f2+f3==3 
	--elseif ft==-3 then return f1+f2+f3>=2 
	--else return f1+f2+f3>=1 end  

	  return f1>0 and f2>5 and g2:IsExists(Card.IsRace,1,nil,RACE_PLANT) and g2:IsExists(Card.IsRace,1,nil,RACE_WARRIOR) and g2:IsExists(Card.IsRace,1,nil,RACE_BEAST)  
	  and g2:IsExists(Card.IsRace,1,nil,RACE_ROCK)and g2:IsExists(Card.IsRace,1,nil,RACE_INSECT) and g2:IsExists(Card.IsCode,1,nil,54959865)
--and g2:GetSortCount(Card.IsFaceup)>5  
end
function c47.spop(e,tp,eg,ep,ev,re,r,rp,c)
	local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
	  Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
	  local g1=Duel.GetMatchingGroup(Card.IsCode,tp,LOCATION_MZONE,0,nil,89943723)
	local g2=Duel.GetMatchingGroup(Card.IsSetCard,tp,LOCATION_MZONE+LOCATION_GRAVE,0,nil,0x1f)
	g1:Merge(g2) 
	local g=Group.CreateGroup()
	local tc=nil
	for i=1,7 do
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
		--if ft<=0 then 
			tc=g1:Select(tp,1,1,nil):GetFirst() 
		--else 
			--tc=g1:Select(tp,1,1,nil):GetFirst() 
		--end 
		g:AddCard(tc)
		g1:Remove(Card.IsCode,nil,tc:GetCode())
		--ft=ft+1 
	end
	local cg=g:Filter(Card.IsFacedown,nil)
	if cg:GetCount()>0 then
		Duel.ConfirmCards(1-tp,cg)
	end
	  --g:Merge(g1)   
	Duel.SendtoDeck(g,nil,7,REASON_COST)
end

function c47.fuscon(e,g,gc,chkf)
	if g==nil then return false end
	if gc then return false end
	local g1=g:Filter(Card.IsSetCard,nil,0x9)
	local c1=g1:GetCount()
	local g2=g:Filter(Card.IsSetCard,nil,0x1f)
	local c2=g2:GetCount()
	local g3=g:Filter(Card.IsSetCard,nil,0x8)
	local c3=g3:GetCount()
	local ag=g1:Clone()
	ag:Merge(g2)
	ag:Merge(g3)
	if chkf~=PLAYER_NONE and not ag:IsExists(aux.FConditionCheckF,1,nil,chkf) then return false end
	return c1>0 and c2>0 and c3>0 and ag:GetCount()>=5 and (c1>1 or c3>1 or g1:GetFirst()~=g3:GetFirst())
end
function c47.fusop(e,tp,eg,ep,ev,re,r,rp,gc,chkf)
	if gc then return end
	local g1=eg:Filter(Card.IsSetCard,nil,0x9)
	local g2=eg:Filter(Card.IsSetCard,nil,0x1f)
	local g3=eg:Filter(Card.IsSetCard,nil,0x8)
	local ag=g1:Clone()
	ag:Merge(g2)
	ag:Merge(g3)
	local tc=nil local f1=0 local f2=0 local f3=0
	local mg=Group.CreateGroup()
	for i=1,5 do
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
		if i==1 and chkf~=PLAYER_NONE then
			tc=ag:FilterSelect(tp,aux.FConditionCheckF,1,1,nil,chkf):GetFirst()
		else tc=ag:Select(tp,1,1,nil):GetFirst() end
		if g1:IsContains(tc) then
			g1:RemoveCard(tc)
			if g3:IsContains(tc) then g3:RemoveCard(tc) f1=f1+1 f3=f3+1
			else f1=f1+2 end
		elseif g2:IsContains(tc) then g2:RemoveCard(tc) f2=f2+1
		else g3:RemoveCard(tc) f3=f3+2 end
		ag:RemoveCard(tc)
		mg:AddCard(tc)
		if i==3 then
			if f1==0 and f2==0 then ag:Sub(g3)
			elseif f1==0 and f3==0 then ag:Sub(g2)
			elseif f2==0 and f3==0 then ag:Sub(g1) end
		end
		if i==4 then
			if f1==0 then ag=g1
			elseif f2==0 then ag=g2
			elseif f3==0 then ag=g3
			elseif f1==1 and f3==1 then ag:Sub(g2) end
		end
	end
	Duel.SetFusionMaterial(mg)
end

function c47.splimit(e,se,sp,st)
	return bit.band(st,SUMMON_TYPE_FUSION)==SUMMON_TYPE_FUSION
end

function c47.filter(c)
	return (c:IsSetCard(0x1f)) and c:IsType(TYPE_MONSTER)
		and not c:IsForbidden() and c:IsAbleToRemove()
end
function c47.copytg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chk==0 then return Duel.IsExistingMatchingCard(c47.filter,tp,LOCATION_DECK,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g=Duel.SelectTarget(tp,c47.filter,tp,LOCATION_DECK,0,1,99,nil)
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,g,1,0,0)
end
function c47.copyop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.SelectMatchingCard(tp,c47.filter,tp,LOCATION_DECK,0,1,Duel.GetMatchingGroupCount(c47.filter,tp,LOCATION_DECK,0,nil),nil)
	if Duel.Remove(g,POS_FACEUP,REASON_EFFECT)>0 and c:IsRelateToEffect(e) and c:IsFaceup() then
	local g=Duel.GetOperatedGroup()
		for tc in aux.Next(g) do
			local code=tc:GetOriginalCode()
			local cid=c:CopyEffect(code,RESET_EVENT+RESETS_STANDARD,1)
			local e1=Effect.CreateEffect(c)
			e1:SetDescription(aux.Stringid(31111109,1))
			e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
			e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
			e1:SetCountLimit(1)
			e1:SetRange(LOCATION_MZONE)
			e1:SetReset(RESET_EVENT+RESETS_STANDARD)
			e1:SetLabel(cid)
			e1:SetOperation(c47.rstop)
			c:RegisterEffect(e1)
		end
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetProperty(EFFECT_FLAG_COPY_INHERIT)
		e2:SetCode(EFFECT_UPDATE_ATTACK)
		e2:SetValue(#g*500)
		e2:SetReset(RESET_EVENT+RESETS_STANDARD_DISABLE)
		c:RegisterEffect(e2)
	end
end
function c47.rstop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local cid=e:GetLabel()
	c:ResetEffect(cid,RESET_COPY)
	Duel.HintSelection(Group.FromCards(c))
	Duel.Hint(HINT_OPSELECTED,1-tp,e:GetDescription())
end