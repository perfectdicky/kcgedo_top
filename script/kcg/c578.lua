--奧雷卡爾克斯之怒 (KA)
function c578.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY+CATEGORY_RECOVER)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_BE_BATTLE_TARGET)
	e1:SetCondition(c578.condition)
	e1:SetTarget(c578.target)
	e1:SetOperation(c578.activate)
	c:RegisterEffect(e1)	
end

function c578.condition(e,tp,eg,ep,ev,re,r,rp)
	local d=Duel.GetAttackTarget()
	return d and d:IsFaceup() and d:IsControler(tp) and d:IsSetCard(0x900)
end

function c578.filter(c)
	return c:IsAttackPos() and aux.TRUE
end

function c578.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c578.filter,tp,0,LOCATION_MZONE,1,nil) end
	local g=Duel.GetMatchingGroup(c578.filter,tp,0,LOCATION_MZONE,nil)
			local tc=g:GetFirst()
			local tatk=0
			while tc do
			local atk=tc:GetAttack()
			if atk<0 then atk=0 end
			tatk=tatk+atk
			tc=g:GetNext() end
	Duel.SetOperationInfo(0,CATEGORY_RECOVER,nil,0,tp,tatk)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
end

function c578.activate(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c578.filter,tp,0,LOCATION_MZONE,nil)
	if g:GetCount()>0 then
	Duel.Destroy(g,REASON_EFFECT)
			local sg=Duel.GetOperatedGroup()
			local tc=sg:GetFirst()
			local tatk=0
			while tc do
			local atk=tc:GetPreviousAttackOnField()
			if atk<0 then atk=0 end
			tatk=tatk+atk
			tc=sg:GetNext() end
			Duel.BreakEffect()
	Duel.Recover(tp,tatk,REASON_EFFECT)
	end
end
