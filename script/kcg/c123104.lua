--Orichlachos Kytora
function c123104.initial_effect(c)

	  local e00=Effect.CreateEffect(c) 
	  e00:SetType(EFFECT_TYPE_SINGLE) 
	  e00:SetCode(EFFECT_CANNOT_SUMMON) 
	  c:RegisterEffect(e00) 
	  local e01=e00:Clone() 
	  e01:SetCode(EFFECT_CANNOT_MSET) 
	  c:RegisterEffect(e01) 

	--special summon
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(c123104.spcon)
	e1:SetOperation(c123104.spop)
	c:RegisterEffect(e1)

	--Negates Battle Damage
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e2:SetRange(LOCATION_ONFIELD)
	e2:SetCode(EVENT_PRE_BATTLE_DAMAGE)
	e2:SetCondition(c123104.rdcon)
	e2:SetOperation(c123104.rdop)
	c:RegisterEffect(e2)

			--special summon
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(5398,0))
	e3:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e3:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e3:SetCode(EVENT_DESTROYED)
	e3:SetTarget(c123104.target)
	e3:SetOperation(c123104.operation)
	c:RegisterEffect(e3)

	--suicide
	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(123104,1))
	e5:SetType(EFFECT_TYPE_IGNITION)
	e5:SetCategory(CATEGORY_DESTROY)
	e5:SetRange(LOCATION_ONFIELD)
	e5:SetTarget(c123104.destg)
	e5:SetOperation(c123104.desop)
	c:RegisterEffect(e5)

	--not destroyed by card effects
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e4:SetRange(LOCATION_ONFIELD)
	e4:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e4:SetValue(c123104.except)
	c:RegisterEffect(e4)
end
c123104[1]=0
c123104.listed_names={12399}

function c123104.filter(c,e,tp)
	return c:IsCode(12399) and c:IsCanBeSpecialSummoned(e,0,tp,true,false)
	and Duel.GetLocationCountFromEx(tp,tp,nil,c)>0
end
function c123104.spcon(e,c)
	if c==nil then return true end
	return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0 and
	Duel.CheckLPCost(c:GetControler(),500)
end
function c123104.spop(e,tp,eg,ep,ev,re,r,rp,c,chk)
	if chk==0 then return Duel.CheckLPCost(tp,500)
	else Duel.PayLPCost(tp,500) end
end

function c123104.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c123104.filter,tp,LOCATION_EXTRA,0,1,nil,e,tp) and c123104[1]>0 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c123104.operation(e,tp,eg,ep,ev,re,r,rp)
	if c123104[1]<1 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c123104.filter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	local tc=g:GetFirst()
	if tc then
	if Duel.SpecialSummon(tc,0,tp,tp,true,false,POS_FACEUP)==0 then return end
	tc:CompleteProcedure() 
	end
end

function c123104.rdcon(e,tp,eg,ep,ev,re,r,rp)
	 return Duel.GetAttacker()~=nil and Duel.GetAttackTarget()~=nil and ep==tp and ev>0
end
function c123104.rdop(e,tp,eg,ep,ev,re,r,rp)
	 if Duel.GetAttacker()~=nil and Duel.GetAttackTarget()~=nil and ep==tp and ev>0 then
	 --local dg=Duel.GetMatchingGroup(c123104.filter1,tp,0,LOCATION_MZONE,nil)
	 --if dg:GetCount()==1 then
	 --c123104[1]=c123104[1]+dg:GetFirst():GetFlagEffectLabel(283)
	 --else c123104[1]=c123104[1]+ev end
	 local val=ev
	 if Duel.GetAttacker():GetAttack()==888888 or Duel.GetAttackTarget():GetAttack()==888888 then
	 val=888888  end
	 c123104[1]=c123104[1]+val
	 Duel.ChangeBattleDamage(tp,0)
	 end
end
function c123104.filter1(c)
	  return c:IsFaceup() and c:GetFlagEffect(283)~=0
end

function c123104.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsDestructable() end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,e:GetHandler(),1,0,0)
end
function c123104.desop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Destroy(e:GetHandler(),REASON_EFFECT)
end

function c123104.damval(e,re,val,r,rp,rc)
	if bit.band(r,REASON_EFFECT)~=0 then return 0 end
	return val
end

function c123104.except(e,te)
	return te:GetOwner()~=e:GetOwner()
end