--Numeron Chaos Ritual
function c13717.initial_effect(c)
--Activate
	if not c13717.global_check then
		c13717.global_check=true
		c13717[0]=false
		c13717[1]=false
		local ge1=Effect.CreateEffect(c)
		ge1:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
		ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_DESTROYED)
		ge1:SetOperation(c13717.checkop)
		Duel.RegisterEffect(ge1,0)
		local ge2=Effect.CreateEffect(c)
		ge2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge2:SetCode(EVENT_PHASE+PHASE_END)
		ge2:SetCountLimit(1)
		ge2:SetOperation(c13717.clear)
		Duel.RegisterEffect(ge2,0)
	end

	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetHintTiming(0,TIMING_END_PHASE)
	e1:SetTarget(c13717.target)
	e1:SetOperation(c13717.activate)
	c:RegisterEffect(e1)
end
function c13717.checkop(e,tp,eg,ep,ev,re,r,rp)
	local tc=eg:GetFirst()
	while tc do
		if tc:IsSetCard(0x14b) and tc:IsSetCard(0x48) and tc:IsType(TYPE_MONSTER) then
			c13717[0]=true
		end
		tc=eg:GetNext()
	end
end
function c13717.clear(e,tp,eg,ep,ev,re,r,rp)
	c13717[0]=false
	c13717[1]=false
end

function c13717.filter(c,e,tp)
	local g=Duel.GetMatchingGroup(c13717.filter2,tp,LOCATION_GRAVE+LOCATION_REMOVED,0,nil,c,tp)
	return c:GetRank()==12 and c.minxyzct
	and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false)
	and g:GetCount()>=c.minxyzct
	and Duel.GetLocationCountFromEx(tp,tp,nil,c)>0 
end
function c13717.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return c13717[0]
		and Duel.IsExistingMatchingCard(c13717.filter,tp,LOCATION_EXTRA,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c13717.activate(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsExistingMatchingCard(c13717.filter,tp,LOCATION_EXTRA,0,1,nil,e,tp) then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c13717.filter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	local sc=g:GetFirst()
	if g:GetCount()>0 then
		if not sc.minxyzct then return end
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
		local m=Duel.SelectMatchingCard(tp,c13717.filter2,tp,LOCATION_GRAVE+LOCATION_REMOVED,0,sc.minxyzct,sc.maxxyzct,nil,sc,tp)
		if m:GetCount()<sc.minxyzct or not aux.MustMaterialCheck(m,tp,EFFECT_MUST_BE_XMATERIAL) then return end
		sc:SetMaterial(m)
		Duel.Overlay(sc,m)
		Duel.SpecialSummon(sc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
		sc:CompleteProcedure() 
	end
end
function c13717.filter2(c,tc,tp)
	return not c:IsHasEffect(EFFECT_NECRO_VALLEY) 
	and ((c:IsXyzLevel(tc,12) and c:IsCanBeXyzMaterial(tc) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL))
	 or (c:IsSetCard(0x48) or c:IsCode(41418852)))
end
function c13717.filter3(c,tc)
	return c:IsCode(41418852) and not c:IsHasEffect(EFFECT_NECRO_VALLEY) 
end
