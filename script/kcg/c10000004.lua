--幻魔皇 拉比艾尔
local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	--c:SetUniqueOnField(1,1,10000004)
	
	--特殊召唤方式
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(s.spcon)
	e1:SetOperation(s.spop)
	c:RegisterEffect(e1)
	
	--对方召唤特召时特召衍生物
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(10000004,2))
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_TOKEN)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCode(EVENT_SUMMON_SUCCESS)
	e2:SetCondition(s.tkcon)
	e2:SetTarget(s.tktg)
	e2:SetOperation(s.tkop)
	c:RegisterEffect(e2)
	local e3=e2:Clone()
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e3)
	
	--解放怪兽提升攻击
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(10000004,2))
	e4:SetCategory(CATEGORY_ATKCHANGE)
	e4:SetType(EFFECT_TYPE_IGNITION)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCountLimit(1)
	e4:SetCost(s.atcost)
	e4:SetOperation(s.atop)
	c:RegisterEffect(e4)
	
	--不受陷阱效果以及魔法效果怪兽生效一回合
	local e82 = Effect.CreateEffect(c)
    e82:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e82:SetCode(EVENT_CHAIN_SOLVED)
    e82:SetRange(LOCATION_MZONE)
    e82:SetCondition(s.sdcon2)
    e82:SetOperation(s.sdop2)
    c:RegisterEffect(e82)
    local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e8:SetRange(LOCATION_MZONE)
	e8:SetCode(EFFECT_IMMUNE_EFFECT)
	e8:SetValue(s.efilterr)
	c:RegisterEffect(e8)

	local te=Effect.CreateEffect(c)
	te:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	te:SetRange(LOCATION_MZONE)
	te:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	te:SetCountLimit(1)
	te:SetCode(EVENT_PHASE+PHASE_END)	
	te:SetOperation(s.reop2)	
	c:RegisterEffect(te)	
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.spcon(e,c)
	if c==nil then return true end
	return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>-3
		and Duel.CheckReleaseGroup(c:GetControler(),nil,3,nil)
end

function s.spop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=Duel.SelectReleaseGroup(c:GetControler(),nil,3,3,nil)
	Duel.Release(g,REASON_COST)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.tkfilter(c,e,tp)
	return c:IsControler(tp) and (not e or c:IsRelateToEffect(e))
end

function s.tkcon(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(s.tkfilter,1,nil,nil,1-tp)
end

function s.tktg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,1,0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,0,0)
end

function s.tkop(e,tp,eg,ep,ev,re,r,rp)
	  local tc=eg:GetFirst()
	  while tc do
	local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if ft<=0 or not Duel.IsPlayerCanSpecialSummonMonster(tp,69890968,0,0x4011,1000,1000,1,RACE_FIEND,ATTRIBUTE_DARK) then return end
	local token=Duel.CreateToken(tp,69890968)
	Duel.SpecialSummon(token,0,tp,tp,false,false,POS_FACEUP)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	token:RegisterEffect(e1,true)
	  tc=eg:GetNext() end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.atcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckReleaseGroup(tp,nil,1,e:GetHandler()) end
	local g=Duel.SelectReleaseGroup(tp,nil,1,2,e:GetHandler())
	local tatk=0
	for tc in aux.Next(g) do
	   local atk=tc:GetAttack()
	   if atk<0 then atk=0 end
	   tatk=tatk+atk
	end
	e:SetLabel(tatk)
	Duel.Release(g,REASON_COST)
end

function s.atop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsFaceup() and c:IsRelateToEffect(e) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(e:GetLabel())
		e1:SetReset(RESET_EVENT+0x1ff0000+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e1)
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.efilterr(e,te)   
	local c=e:GetHandler()  
	local tc=te:GetOwner()
	local turncount=Duel.GetTurnCount()-tc:GetTurnID()
	if c:GetTurnID()==Duel.GetTurnCount() then turncount=0 end
	if tc==e:GetOwner() or te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then return false
	else return te:GetActiveType()==TYPE_TRAP  
	end
end

function s.sdcon2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=1
                    break 
                end
            end
        end
    end
    return eff==1
end
function s.sdop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=te
                    break 
                end
            end
        end
    end
    if eff==0 then return end
    local e83 = Effect.CreateEffect(c)
    e83:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e83:SetRange(LOCATION_MZONE)
    e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
    e83:SetCountLimit(1)
    e83:SetCode(EVENT_PHASE + PHASE_END)
    e83:SetLabelObject(eff)
    e83:SetOperation(s.setop2)
    e83:SetReset(RESET_PHASE + PHASE_END)
    c:RegisterEffect(e83) 
end
function s.setop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local te = e:GetLabelObject()
    if not te or te==0 then return end
    te:Reset()
end   

function s.reop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	for i=1,428 do
	if c:IsHasEffect(i) then 
		local ae={c:IsHasEffect(i)}
		for _,te in ipairs(ae) do
			if te:GetOwner()~=e:GetOwner() and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then
			local e80=Effect.CreateEffect(c)
			e80:SetType(EFFECT_TYPE_SINGLE)
			e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
			e80:SetRange(LOCATION_MZONE)
			e80:SetCode(EFFECT_IMMUNE_EFFECT)
			e80:SetValue(function(e,te2) return te2==te end)
			c:RegisterEffect(e80) end
		end
	end
	end
end