-- CXyz Barian, the King of Wishes
local s, id = GetID()
function s.initial_effect(c)
    -- xyz summon
    c:EnableReviveLimit()

    -- special summon
    local e4 = Effect.CreateEffect(c)
    e4:SetDescription(aux.Stringid(87997872, 0))
    e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
    e4:SetRange(LOCATION_EXTRA)
    e4:SetProperty(EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DAMAGE_CAL + EFFECT_FLAG_DELAY)
    e4:SetCode(EVENT_TO_GRAVE)
    e4:SetCondition(s.spcon)
    e4:SetTarget(s.sptg)
    e4:SetOperation(s.spop)
    c:RegisterEffect(e4)

    local e5 = Effect.CreateEffect(c)
    e5:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_F)
    e5:SetProperty(EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DAMAGE_CAL + EFFECT_FLAG_DELAY)
    e5:SetCode(EVENT_SPSUMMON_SUCCESS)
    e5:SetTarget(s.sptg2)
    e5:SetOperation(s.spop2)
    c:RegisterEffect(e5)

    -- atk
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE)
    e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e2:SetRange(LOCATION_MZONE)
    e2:SetCode(EFFECT_SET_ATTACK)
    e2:SetValue(s.atkval)
    c:RegisterEffect(e2)

    local e1 = Effect.CreateEffect(c)
    e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetRange(LOCATION_MZONE)
    e1:SetCode(511001363)
    c:RegisterEffect(e1)
    aux.GlobalCheck(s, function()
        local ge = Effect.CreateEffect(c)
        ge:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        ge:SetCode(EVENT_ADJUST)
        ge:SetOperation(s.op)
        Duel.RegisterEffect(ge, 0)
    end)

    local e3 = Effect.CreateEffect(c)
    e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_DAMAGE_STEP +
                       EFFECT_FLAG_DAMAGE_CAL)
    e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e3:SetCode(EVENT_ADJUST)
    e3:SetRange(LOCATION_MZONE)
    e3:SetOperation(s.op2)
    c:RegisterEffect(e3)

 	local e6=Effect.CreateEffect(c)
	e6:SetDescription(aux.Stringid(12744567,1))
	e6:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_RECOVER)
	e6:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e6:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e6:SetCode(EVENT_LEAVE_FIELD)
	e6:SetCondition(s.lspcon)
	e6:SetTarget(s.lsptg)
	e6:SetOperation(s.lspop)
	c:RegisterEffect(e6)   
end
s.listed_names = {38, 67926903}

function s.sfilter(c, tp)
    return c:IsCode(38) and c:IsPreviousControler(tp) and c:IsReason(REASON_DESTROY) and c:IsPreviousLocation(LOCATION_ONFIELD)
end
function s.spcon(e, tp, eg, ep, ev, re, r, rp)
    return eg:IsExists(s.sfilter, 1, nil, tp)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():IsCanBeSpecialSummoned(e, SUMMON_TYPE_XYZ, tp, true, false) and
                   Duel.IsExistingMatchingCard(s.cfilter2, tp,
                       LOCATION_GRAVE + LOCATION_EXTRA + LOCATION_REMOVED + LOCATION_MZONE, 0, 1, nil, tp,
                       e:GetHandler())
    end
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, e:GetHandler(), 1, 0, LOCATION_EXTRA)
end
function s.cfilter2(c, tp, tc)
    return c:IsCode(67926903) and Duel.GetLocationCountFromEx(tp, tp, Group.FromCards(c), tc) > 0
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
    if e:GetHandler():IsRelateToEffect(e) and eg:IsExists(s.sfilter, 1, nil, tp) then
        local g = Duel.SelectMatchingCard(tp, s.cfilter2, tp,
                      LOCATION_GRAVE + LOCATION_EXTRA + LOCATION_REMOVED + LOCATION_MZONE, 0, 1, 1, nil, tp,
                      e:GetHandler())
        if not g then
            return
        end
        local ag = g:GetFirst()
        local og = ag:GetOverlayGroup()
        if #og > 0 then
            Duel.Overlay(e:GetHandler(), og)
        end
        e:GetHandler():SetMaterial(g)
        Duel.Overlay(e:GetHandler(), g)
        Duel.SpecialSummon(e:GetHandler(), SUMMON_TYPE_XYZ, tp, tp, true, false, POS_FACEUP)
        e:GetHandler():CompleteProcedure()
    end
end

function s.sptg2(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsExistingMatchingCard(s.ofilter, 0, LOCATION_GRAVE + LOCATION_MZONE + LOCATION_REMOVED,
                   LOCATION_GRAVE + LOCATION_MZONE + LOCATION_REMOVED, 1, nil, tp)
    end
end
function s.ofilter(c, tp)
    local no=c.xyz_number
    return (c:IsControlerCanBeChanged() or c:IsControler(tp)) and not c:IsType(TYPE_TOKEN) and
               (no and no >= 101 and no <= 107 and c:IsSetCard(0x48))
end
function s.spop2(e, tp, eg, ep, ev, re, r, rp)
    if e:GetHandler():IsRelateToEffect(e) and
        Duel.IsExistingMatchingCard(s.ofilter, 0, LOCATION_GRAVE + LOCATION_MZONE + LOCATION_REMOVED,
            LOCATION_GRAVE + LOCATION_MZONE + LOCATION_REMOVED, 1, nil, tp) then
        local g = Duel.GetMatchingGroup(s.ofilter, 0, LOCATION_GRAVE + LOCATION_MZONE + LOCATION_REMOVED,
                      LOCATION_GRAVE + LOCATION_MZONE + LOCATION_REMOVED, nil, tp)
        if not g then
            return
        end
        for tc in aux.Next(g) do
            local og = tc:GetOverlayGroup()
            if #og > 0 then
                Duel.Overlay(e:GetHandler(), og)
            end
        end
        Duel.Overlay(e:GetHandler(), g)
    end
end

function s.op2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local g = c:GetOverlayGroup()
    for tc in aux.Next(g) do
        local code = tc:GetOriginalCode()
        if c:GetFlagEffect(code) == 0 then
            c:RegisterFlagEffect(code, RESET_EVENT + RESETS_STANDARD, 0, 1)
        end
    end
end

function s.cfilter(c)
    return c:IsHasEffect(511002571) and #{c:GetCardEffect(5110013630)} == 0
end
function s.op(e)
    local g = Duel.GetMatchingGroup(s.cfilter, 0, 0xff, 0xff, nil)
    for c in aux.Next(g) do
        local effs = {c:GetCardEffect(511002571)}
        for _, eff in ipairs(effs) do
            local te = eff:GetLabelObject()
            local resetflag, resetcount = te:GetReset()
            local max, code = te:GetCountLimit()
            local category = te:GetCategory()
            local prop1, prop2 = te:GetProperty()
            local range = te:GetRange()
            local targetrange1, targetrange2 = te:GetTargetRange()
            if not targetrange1 then
                targetrange1 = 0
            end
            if not targetrange2 then
                targetrange2 = 0
            end
            local label = te:GetLabel()
            local labelob = te:GetLabelObject()
            if labelob and labelob:GetCode() == EFFECT_RANKUP_EFFECT then
                local te = labelob:GetLabelObject()
                local max, code = te:GetCountLimit()
                local prop1, prop2 = te:GetProperty()
                local reset = labelob:GetLabel()
                local label = te:GetLabel()
                local labelob = te:GetLabelObject()
                local e1 = Effect.CreateEffect(c)
                if te:GetDescription() then
                    e1:SetDescription(te:GetDescription())
                end
                if bit.band(te:GetType(), EFFECT_TYPE_XMATERIAL) == 0 then
                    e1:SetType(EFFECT_TYPE_XMATERIAL + te:GetType())
                else
                    e1:SetType(te:GetType())
                end
                if range then
                    e1:SetRange(range)
                end
                if targetrange1 ~= 0 or targetrange2 ~= 0 then
                    e1:SetTargetRange(targetrange1, targetrange2)
                end
                if te:GetCode() then
                    e1:SetCode(te:GetCode())
                end
                e1:SetProperty(prop1, prop2)
                if label and label ~= 0 then
                    e1:SetLabel(label)
                end
                e1:SetLabelObject(te)
                e1:SetCondition(s.copycon)
                e1:SetCost(s.copycost)
                if max and max > 0 then
                    e1:SetCountLimit(max, code)
                end
                if te:GetTarget() then
                    e1:SetTarget(te:GetTarget())
                end
                if te:GetOperation() then
                    e1:SetOperation(te:GetOperation())
                end
                e1:SetReset(te:GetLabel())
                c:RegisterEffect(e1, true)
                local e2 = Effect.CreateEffect(c)
                e2:SetType(EFFECT_TYPE_SINGLE)
                e2:SetCode(5110013630)
                e2:SetProperty(prop1, prop2)
                e2:SetReset(te:GetLabel())
                c:RegisterEffect(e2, true)
                goto exit
            end
            local e1 = Effect.CreateEffect(c)
            if te:GetDescription() then
                e1:SetDescription(te:GetDescription())
            end
            if bit.band(te:GetType(), EFFECT_TYPE_XMATERIAL) == 0 then
                e1:SetType(EFFECT_TYPE_XMATERIAL + te:GetType())
            else
                e1:SetType(te:GetType())
            end
            if range then
                e1:SetRange(range)
            end
            if targetrange1 ~= 0 or targetrange2 ~= 0 then
                e1:SetTargetRange(targetrange1, targetrange2)
            end
            if te:GetCode() then
                e1:SetCode(te:GetCode())
            end
            e1:SetCategory(category)
            e1:SetProperty(prop1, prop2)
            if label and label ~= 0 then
                e1:SetLabel(label)
            end
            e1:SetLabelObject(te)
            e1:SetCondition(s.copycon)
            e1:SetCost(s.copycost)
            if max and max > 0 then
                e1:SetCountLimit(max, code)
            end
            if te:GetTarget() then
                e1:SetTarget(te:GetTarget())
            end
            if te:GetOperation() then
                e1:SetOperation(te:GetOperation())
            end
            if resetflag and resetcount then
                e1:SetReset(resetflag, resetcount)
            elseif resetflag then
                e1:SetReset(resetflag)
            end
            c:RegisterEffect(e1, true)
            local e2 = Effect.CreateEffect(c)
            e2:SetType(EFFECT_TYPE_SINGLE)
            e2:SetCode(5110013630)
            e2:SetProperty(prop1, prop2)
            if resetflag and resetcount then
                e2:SetReset(resetflag, resetcount)
            elseif resetflag then
                e2:SetReset(resetflag)
            end
            c:RegisterEffect(e2, true)
            ::exit::
        end
    end
end
function s.copycon(e, tp, eg, ep, ev, re, r, rp)
    local con = e:GetLabelObject():GetCondition()
    return e:GetHandler():IsHasEffect(511001363) and e:GetOwner():GetFlagEffect(511001363) == 0 and
               (not con or con(e, tp, eg, ep, ev, re, r, rp))
end
function s.copycost(e, tp, eg, ep, ev, re, r, rp, chk)
    local c = e:GetHandler()
    local tc = e:GetOwner()
    local te = e:GetLabelObject()
    local cost = te:GetCost()
    local a = c:CheckRemoveOverlayCard(tp, 1, REASON_COST)
    local b = Duel.CheckLPCost(tp, 400)
    local ov = c:GetOverlayGroup()
    if chk == 0 then
        return (a or b) and (cost == nil or cost(e, tp, eg, ep, ev, re, r, rp, 0))
    end
    Duel.Hint(HINT_CARD, 0, tc:GetOriginalCode())
    -- Duel.SetTargetCard(tc)
    local op = 0
    Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(76922029, 0))
    if a and b then
        op = Duel.SelectOption(tp, aux.Stringid(81330115, 0), aux.Stringid(21454943, 1))
    elseif a and not b then
        Duel.SelectOption(tp, aux.Stringid(81330115, 0))
        op = 0
    else
        Duel.SelectOption(tp, aux.Stringid(21454943, 1))
        op = 1
    end
    if op == 0 then
        Duel.SendtoGrave(tc, REASON_COST)
    else
        Duel.PayLPCost(tp, 400)
    end
    tc:RegisterFlagEffect(511001363, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, 0, 1)
    if te:GetLabelObject() then
        te:SetLabelObject(te:GetLabelObject())
    end
end

function s.atkval(e, c)
    return c:GetOverlayCount() * 1000
end

function s.lspcon(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    return c:IsPreviousLocation(LOCATION_MZONE) and c:GetOverlayCount() > 0
end
function s.lsptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, e:GetHandler(), 1, 0, 0)
    local rec = e:GetHandler():GetPreviousAttackOnField()
    Duel.SetTargetPlayer(tp)
    Duel.SetTargetParam(rec)
    Duel.SetOperationInfo(0, CATEGORY_RECOVER, nil, 0, tp, rec)
end
function s.lspop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsRelateToEffect(e) and Duel.SpecialSummon(c, 0, tp, tp, false, false, POS_FACEUP) > 0 then
        local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
        Duel.Recover(p, d, REASON_EFFECT)
    end
end