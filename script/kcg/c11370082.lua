--Seventh Around
function c11370082.initial_effect(c)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PRE_DAMAGE_CALCULATE)
	e1:SetCondition(c11370082.condition)
	e1:SetOperation(c11370082.operation)
	c:RegisterEffect(e1)
end

function c11370082.xyzfilter(c)
	local no=c.xyz_number
	return no and no>=101 and no<=107 
end
function c11370082.condition(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetAttacker()
	local bc=Duel.GetAttackTarget()
	if tc:IsControler(1-tp) then
		tc=Duel.GetAttackTarget()
		bc=Duel.GetAttacker()
	end
	return tc and bc and tc:IsType(TYPE_XYZ) 
	and tc:GetOverlayGroup():IsExists(c11370082.xyzfilter,1,nil) 
	--and tc:IsStatus(STATUS_BATTLE_DESTROYED)
	and (not tc:IsHasEffect(EFFECT_INDESTRUCTABLE_BATTLE) or not tc:IsHasEffect(EFFECT_INDESTRUCTABLE))
	and tc:GetAttack()<=bc:GetAttack()
end
function c11370082.operation(e,tp,eg,ep,ev,re,r,rp)
	Duel.ChangeBattleDamage(ep,0)
	local tc=Duel.GetAttacker()
	local bc=tc:GetBattleTarget()
	if tc:IsControler(1-tp) then
		tc=Duel.GetAttackTarget()
		bc=Duel.GetAttacker()
	end
	local og=tc:GetOverlayGroup()
	if og:GetCount()==0 then return end
	if Duel.Remove(og,POS_FACEUP,REASON_EFFECT)>0 and Duel.Remove(tc,POS_FACEUP,REASON_EFFECT)>0 then
	tc:RegisterFlagEffect(11370082,RESET_EVENT+0x1fe0000,0,1)
	local tc2=og:GetFirst()
	while tc2 do
	tc2:RegisterFlagEffect(11370082,RESET_EVENT+0x1fe0000,0,1)	  
	tc2=og:GetNext() end
	local c=e:GetHandler()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c11370082.filter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp)
	local g1=g:GetFirst()
	if g:GetCount()>0 then
		Duel.SpecialSummon(g1,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
		g1:CompleteProcedure()
		local e1=Effect.CreateEffect(c)
		e1:SetCategory(CATEGORY_DAMAGE)
		e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		--e1:SetProperty(EFFECT_FLAG_REPEAT)
		e1:SetCountLimit(1)
		e1:SetLabel(g1:GetBaseAttack())
		e1:SetCode(EVENT_PHASE+PHASE_BATTLE)
		e1:SetOperation(c11370082.desop)
			e1:SetReset(RESET_PHASE+PHASE_END) 
		Duel.RegisterEffect(e1,tp) end end
end

function c11370082.filter(c,e,tp)
	return c:IsRankBelow(3) and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false,POS_FACEUP,tp)
	and Duel.GetLocationCountFromEx(tp,tp,nil,c)>0
end

function c11370082.desop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Damage(1-tp,e:GetLabel(),REASON_EFFECT)
end
