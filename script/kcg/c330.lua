--壓力胞子 (K)
local s,id=GetID()
function s.initial_effect(c)
	local e3=Effect.CreateEffect(c) 
	e3:SetProperty(EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_OVERLAY)
	e3:SetOperation(s.efop)
	c:RegisterEffect(e3)	   
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetProperty(EFFECT_FLAG_DAMAGE_CAL+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	-- e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	-- e2:SetCode(EVENT_BE_MATERIAL)
	-- e2:SetCondition(s.efcon)
	-- e2:SetOperation(s.efop)
	-- c:RegisterEffect(e2)
end

function s.efcon(e,tp,eg,ep,ev,re,r,rp)
	local ec=e:GetHandler():GetReasonCard()
	return r==REASON_XYZ
end
function s.efop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local eqc=eg:GetFirst()
	--if not eg then eqc=c:GetReasonCard() end
	Duel.Hint(HINT_CARD,ep,330)
	if not (Duel.GetLocationCount(ep,LOCATION_MZONE)>0 and eqc
	  and Duel.IsPlayerCanSpecialSummonMonster(ep,347,0,eqc:GetOriginalType(),eqc:GetBaseAttack()+100,eqc:GetBaseDefense(),eqc:GetOriginalLevel(),eqc:GetOriginalRace(),eqc:GetOriginalAttribute())) then return end
	local tc2=Duel.CreateToken(ep,347)
	local ocode=eqc:GetOriginalCodeRule()
	if Duel.SpecialSummonStep(tc2,0,ep,ep,true,false,POS_FACEUP) then
	tc2:SetEntityCode(eqc:GetOriginalCode(),347,{eqc:GetOriginalSetCard(),tc2:GetOriginalSetCard()	},nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
	-- if eqc:IsSetCard(0x48) then
	-- local mt=_G["c" .. 347]
	-- mt.xyz_number=eqc.xyz_number end
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SET_BASE_ATTACK)
	e0:SetValue(math.max(eqc:GetBaseAttack(),0)+100)
	e0:SetReset(RESET_EVENT+RESETS_STANDARD)
	tc2:RegisterEffect(e0,true)  
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e7:SetCode(EFFECT_OVERLAY_REMOVE_REPLACE)
	e7:SetRange(LOCATION_MZONE)
	--e7:SetCondition(s.rcon)
	e7:SetTarget(s.atktg) 
	tc2:RegisterEffect(e7,true) 
	Duel.SpecialSummonComplete()
	tc2:CompleteProcedure() end  
end

function s.rcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return bit.band(r,REASON_COST)~=0 and re:GetHandler()==e:GetHandler()
	and c:GetFlagEffect(330)==0
end
function s.atktg(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	c:RegisterFlagEffect(330,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END,0,1)
end