--Orichalcos Dexia
function c123100.initial_effect(c)
	c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(aux.FALSE)
	c:RegisterEffect(e1)

	--ATK Increase
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_SET_ATTACK_FINAL)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetValue(c123100.adval)
	c:RegisterEffect(e2)

	--selfdestroy
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_SELF_DESTROY)
	e3:SetCondition(c123100.descon)
	e3:SetOperation(c123100.desop)
	c:RegisterEffect(e3)

	--Shunoros Debuf
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_BATTLED)
	--e5:SetTarget(c123100.target)
	e5:SetOperation(c123100.operation)
	c:RegisterEffect(e5)
end
c123100.listed_names={12399}

function c123100.adval(e,c)
	local ph=Duel.GetCurrentPhase()
	local c=e:GetHandler()
	local a=Duel.GetAttacker()
	local d=Duel.GetAttackTarget()
	if (ph==PHASE_DAMAGE_CAL or PHASE_DAMAGE or Duel.IsDamageCalculated()) and c:IsRelateToBattle() and c:GetBattleTarget()~=nil then
		if a==c and d:IsAttackPos() then return d:GetAttack()+300 end
		if a==c and d:IsDefensePos() then return d:GetDefense()+300 end
		if d==c then return a:GetAttack()+300 end
	end
end

function c123100.cfilter(c)
	return c:IsFaceup() and c:IsCode(12399)
end

function c123100.spcon(e,c)
	if c==nil then return true end
	return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0 
		and Duel.IsExistingMatchingCard(c123100.cfilter,0,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil)
end

function c123100.desfilter(c)
	return c:IsFaceup() and c:IsCode(12399) 
	and (c:IsLocation(LOCATION_MZONE) or c:GetFlagEffect(12)~=0)
end

function c123100.descon(e)
	return not Duel.IsExistingMatchingCard(c123100.desfilter,e:GetHandler():GetControler(),LOCATION_ONFIELD,0,1,nil)
end

function c123100.desop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Destroy(e:GetHandler(),REASON_EFFECT)
end
function c123100.filter(c)
	return c:IsFaceup() and c:IsCode(12399) 
	and ((c:GetFlagEffectLabel(123104) and c:GetFlagEffectLabel(123104)>0 and c:IsLocation(LOCATION_SZONE)) 
	or (c:GetAttack()>0 and c:IsLocation(LOCATION_MZONE)))
end
function c123100.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_ONFIELD) and chkc:IsControler(tp) and c123100.filter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(c123100.filter,tp,LOCATION_ONFIELD,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	Duel.SelectTarget(tp,c123100.filter,tp,LOCATION_ONFIELD,0,1,1,nil)
end
function c123100.operation(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
	local tc=Duel.SelectMatchingCard(tp,c123100.filter,tp,LOCATION_ONFIELD,0,1,1,nil):GetFirst()
	if tc then
		if tc:IsLocation(LOCATION_MZONE) then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(-e:GetHandler():GetAttack())
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1) end
		if tc:IsLocation(LOCATION_SZONE) then
local a=tc:GetFlagEffectLabel(123104)
 tc:ResetFlagEffect(123104)
	if a<=e:GetHandler():GetAttack() then a=e:GetHandler():GetAttack() end	tc:RegisterFlagEffect(123104,RESET_EVENT+0x1fe0000-RESET_TOFIELD-RESET_LEAVE,nil,1,a-e:GetHandler():GetAttack()) end
	end
end
