--罪龍 (K)
function c495.initial_effect(c)
		c:EnableReviveLimit()

		--special summon
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_SPSUMMON_PROC)
		e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
		e1:SetRange(LOCATION_HAND)
		e1:SetCondition(c495.spcon)
		e1:SetOperation(c495.spop)
		c:RegisterEffect(e1)

		--selfdes
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e2:SetRange(LOCATION_MZONE)
		e2:SetCode(EFFECT_SELF_DESTROY)
		e2:SetCondition(c495.descon)
		c:RegisterEffect(e2)

		--spson
		local e3=Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e3:SetCode(EFFECT_SPSUMMON_CONDITION)
		e3:SetValue(aux.FALSE)
		c:RegisterEffect(e3)

		local e4=Effect.CreateEffect(c) 
	  e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
		e4:SetType(EFFECT_TYPE_SINGLE) 
		e4:SetCode(EFFECT_CANNOT_SUMMON) 
		c:RegisterEffect(e4) 
		local e5=e4:Clone() 
		e5:SetCode(EFFECT_CANNOT_MSET) 
		c:RegisterEffect(e5) 

	--multi attack
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	e5:SetOperation(c495.mtop)
	c:RegisterEffect(e5)

	--damage reduce
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_FIELD)
	e7:SetCode(EFFECT_CHANGE_DAMAGE)
	e7:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e7:SetRange(LOCATION_MZONE)
	e7:SetTargetRange(1,1)
	e7:SetValue(c495.damval)
	c:RegisterEffect(e7)

	--Special Summon
	local e8=Effect.CreateEffect(c)
	e8:SetDescription(aux.Stringid(35952884,1))
	e8:SetType(EFFECT_TYPE_TRIGGER_F+EFFECT_TYPE_SINGLE)
	e8:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e8:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_DESTROY)
	e8:SetCode(EVENT_LEAVE_FIELD)
	e8:SetCondition(c495.sumcon)
	e8:SetTarget(c495.sumtg)
	e8:SetOperation(c495.sumop)
	c:RegisterEffect(e8)

	local e9=Effect.CreateEffect(c)
	e9:SetProperty(EFFECT_FLAG_INITIAL)
	e9:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e9:SetCode(EVENT_BATTLED)
	e9:SetRange(LOCATION_MZONE)
	e9:SetCondition(c495.con)
	e9:SetOperation(c495.op)
	c:RegisterEffect(e9)

	--immune
	local e10=Effect.CreateEffect(c)
	e10:SetType(EFFECT_TYPE_SINGLE)
	e10:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e10:SetRange(LOCATION_MZONE)
	e10:SetCode(EFFECT_IMMUNE_EFFECT)
	e10:SetValue(c495.efilter)
	c:RegisterEffect(e10)
end

function c495.spfilter(c)
		return c:IsCode(35952884) and c:IsAbleToGraveAsCost() 
end
function c495.spcon(e,c)
		if c==nil then return true end
		return Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0
			   and Duel.IsExistingMatchingCard(c495.spfilter,c:GetControler(),LOCATION_EXTRA,0,1,nil)
end
function c495.spop(e,tp,eg,ep,ev,re,r,rp,c)
		local c=e:GetHandler()
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
		local tg=Duel.SelectMatchingCard(tp,c495.spfilter,tp,LOCATION_EXTRA,0,1,1,nil)
		Duel.SendtoGrave(tg,REASON_COST)
end

function c495.descon(e)
	local c=e:GetHandler()
	local f1=Duel.GetFieldCard(0,LOCATION_SZONE,5)
	local f2=Duel.GetFieldCard(1,LOCATION_SZONE,5)
	return ((f1==nil or not f1:IsFaceup() or not f1:IsCode(27564031)) and (f2==nil or not f2:IsFaceup() or not f2:IsCode(27564031)))
end

function c495.sumlimit(e,c)
		return c:IsSetCard(0x23)
end

function c495.mtfilter(c)
	return c:IsSetCard(0x23) and c:IsType(TYPE_MONSTER)
end
function c495.mtop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	  local ttp=c:GetControler()
	local ct=1
	  local count=Duel.GetMatchingGroupCount(c495.mtfilter,ttp,LOCATION_GRAVE,0,nil)
	  if count>1 then ct=count end
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_EXTRA_ATTACK)
	e1:SetReset(RESET_EVENT+0x1ff0000)
	e1:SetValue(ct-1)
	c:RegisterEffect(e1)
end

function c495.damval(e,re,val,r,rp,rc)
	if bit.band(r,REASON_EFFECT)~=0 then return 0 end
	return val
end

function c495.discon(e,tp,eg,ep,ev,re,r,rp)
	  local ph=Duel.GetCurrentPhase()
	return not e:GetHandler():IsStatus(STATUS_BATTLE_DESTROYED) and Duel.IsChainNegatable(ev)
	  and Duel.GetAttacker()==e:GetHandler() and (ph>=PHASE_BATTLE and ph<=PHASE_DAMAGE_CAL)
end
function c495.distg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_NEGATE,eg,1,0,0)
	if re:GetHandler():IsDestructable() and re:GetHandler():IsRelateToEffect(re) then
		Duel.SetOperationInfo(0,CATEGORY_DESTROY,eg,1,0,0)
	end
end
function c495.disop(e,tp,eg,ep,ev,re,r,rp)
	Duel.NegateActivation(ev)
	if re:GetHandler():IsRelateToEffect(re) then
		Duel.Destroy(eg,REASON_EFFECT)
	end
end

function c495.sumcon(e,tp,eg,ep,ev,re,r,rp,chk)
	return e:GetHandler():IsPreviousPosition(POS_FACEUP) and e:GetHandler():IsPreviousLocation(LOCATION_ONFIELD)
end
function c495.filter(c,e,tp)
	return c:GetCode()==24696097 and c:IsCanBeSpecialSummoned(e,0,tp,true,false)
end
function c495.sumtg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	  local g=Duel.GetMatchingGroup(aux.TRUE,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c495.sumop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tg=Duel.GetFirstMatchingCard(c495.filter,tp,LOCATION_EXTRA,0,nil,e,tp)
	  local g=Duel.GetMatchingGroup(aux.TRUE,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	  if g:GetCount()>0 then Duel.Destroy(g,REASON_EFFECT) end
	if tg~=nil and Duel.SelectYesNo(tp,aux.Stringid(35952884,1)) then
		Duel.SpecialSummonStep(tg,0,tp,tp,true,false,POS_FACEUP)
			local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tg:RegisterEffect(e1,true)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		tg:RegisterEffect(e2,true)
			Duel.SpecialSummonComplete()
	end
end

function c495.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	  if Duel.GetAttacker()~=nil and Duel.GetAttackTarget()~=nil then
	  local bc=c
	  if Duel.GetAttacker()==c then
	bc=Duel.GetAttackTarget() end
	  if Duel.GetAttackTarget()==c then
	bc=Duel.GetAttacker() end
	if bc then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
		e1:SetValue(1)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		bc:RegisterEffect(e1)
			local e11=e1:Clone()
		e11:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
		bc:RegisterEffect(e11)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_DAMAGE_STEP_END)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		e2:SetOperation(c495.resetop)
		e2:SetLabelObject(e1)
		bc:RegisterEffect(e2)
			local e21=e2:Clone()
		e21:SetLabelObject(e11)
		bc:RegisterEffect(e21) end end  
end
c495.listed_names={27564031}

--Immune
function c495.econ(e,tp,eg,ep,ev,re,r,rp)
	local d=Duel.GetAttacker()
	  return d~=nil and d==e:GetHandler()
end
function c495.efilter(e,te)
	return te:GetOwnerPlayer()~=e:GetHandlerPlayer() and te:IsActiveType(TYPE_EFFECT)
end
function c495.tgvalue(e,re,rp)
	return rp~=e:GetHandlerPlayer() and re:IsActiveType(TYPE_MONSTER)
end

function c495.con(e)
	local c=e:GetHandler()
	local atk=c:GetAttack()
	local bc=c:GetBattleTarget()
	if not bc then return end
	local bct=0
	if bc:GetPosition()==POS_FACEUP_ATTACK then
		bct=bc:GetAttack()
	else bct=bc:GetDefense()+1 end
	return c:IsRelateToBattle() and c:GetPosition()==POS_FACEUP_ATTACK 
	 and atk>=bct and not bc:IsStatus(STATUS_DESTROY_CONFIRMED)
end 
function c495.op(e,tp,eg,ep,ev,re,r,rp)
	local bc=e:GetHandler():GetBattleTarget()
	Duel.Destroy(bc,REASON_RULE)
	bc:SetStatus(STATUS_DESTROY_CONFIRMED,true)
end

function c495.oop(e,tp,eg,ep,ev,re,r,rp)
	local bc=e:GetHandler():GetBattleTarget()
	if bc then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
		e1:SetValue(1)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		bc:RegisterEffect(e1)
		local e2=Effect.CreateEffect(e:GetHandler())
		e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_DAMAGE_STEP_END)
		e2:SetReset(RESET_EVENT+0x1fe0000)
		e2:SetOperation(c495.resetop)
		e2:SetLabelObject(e1)
		bc:RegisterEffect(e2)
	end
end
function c495.resetop(e,tp,eg,ep,ev,re,r,rp)
	e:GetLabelObject():Reset()
	e:Reset()
end