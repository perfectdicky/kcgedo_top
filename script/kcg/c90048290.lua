local s, id = GetID()
function s.initial_effect(c)
    c:EnableReviveLimit()

    local e000 = Effect.CreateEffect(c)
    e000:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e000:SetType(EFFECT_TYPE_SINGLE)
    e000:SetCode(EFFECT_REMOVE_TYPE)
    e000:SetValue(TYPE_FUSION)
    c:RegisterEffect(e000)

    local e00 = Effect.CreateEffect(c)
    e00:SetType(EFFECT_TYPE_SINGLE)
    e00:SetCode(EFFECT_SET_ATTACK)
    e00:SetValue(s.atkvalue)
    c:RegisterEffect(e00)
    e01 = e00:Clone()
    e01:SetCode(EFFECT_SET_DEFENSE)
    e01:SetValue(s.defvalue)
    c:RegisterEffect(e01)
    -- special summon
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_SPSUMMON_PROC)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e1:SetRange(LOCATION_EXTRA)
    e1:SetCondition(s.spcon)
    e1:SetOperation(s.spop)
    c:RegisterEffect(e1)
    -- spson
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE)
    e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e2:SetCode(EFFECT_SPSUMMON_CONDITION)
    e2:SetValue(aux.FALSE)
    c:RegisterEffect(e2)
    -- spsummon
    local e30 = Effect.CreateEffect(c)
    e30:SetType(EFFECT_TYPE_SINGLE)
    e30:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
    e30:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    c:RegisterEffect(e30)
    local e31 = e30:Clone()
    e31:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
    c:RegisterEffect(e31)
	local e4 = Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_DAMAGE_CAL + EFFECT_FLAG_DAMAGE_STEP)
    e4:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_O)
	e4:SetCode(EVENT_SPSUMMON_SUCCESS)
	e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
    e4:SetTarget(s.spcon2)
    e4:SetOperation(s.spop2)
    c:RegisterEffect(e4)
    -- 不受对方的陷阱效果以及魔法效果怪兽
    local e5 = Effect.CreateEffect(c)
    e5:SetType(EFFECT_TYPE_SINGLE)
    e5:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e5:SetRange(LOCATION_MZONE)
    e5:SetCode(EFFECT_IMMUNE_EFFECT)
    e5:SetValue(s.efilter)
    c:RegisterEffect(e5)
    -- Search
    local e7 = Effect.CreateEffect(c)
    e7:SetDescription(aux.Stringid(10406322, 2))
    e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e7:SetCategory(CATEGORY_TOHAND)
    e7:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
    e7:SetCode(EVENT_PREDRAW)
    e7:SetRange(LOCATION_MZONE)
    e7:SetCondition(s.schcon)
    e7:SetTarget(s.schtar)
    e7:SetOperation(s.schop)
    c:RegisterEffect(e7)
    -- 不能被各种方式解放
    local e12 = Effect.CreateEffect(c)
    e12:SetType(EFFECT_TYPE_SINGLE)
    e12:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e12:SetRange(LOCATION_MZONE)
    e12:SetCode(EFFECT_UNRELEASABLE_SUM)
    e12:SetValue(1)
    c:RegisterEffect(e12)
    local e13 = e12:Clone()
    e13:SetCode(EFFECT_UNRELEASABLE_NONSUM)
    c:RegisterEffect(e13)
    local e14 = e12:Clone()
    e14:SetCode(EFFECT_UNRELEASABLE_EFFECT)
    c:RegisterEffect(e14)
    -- 不会被卡的效果破坏、除外、返回手牌和卡组、送入墓地、无效化、改变控制权、变为里侧表示、成为特殊召唤素材
    local e100 = Effect.CreateEffect(c)
    e100:SetType(EFFECT_TYPE_SINGLE)
    e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e100:SetRange(LOCATION_MZONE)
    e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
    e100:SetValue(1)
    c:RegisterEffect(e100)
    local e101 = e100:Clone()
    e101:SetCode(EFFECT_CANNOT_REMOVE)
    c:RegisterEffect(e101)
    local e102 = e101:Clone()
    e102:SetCode(EFFECT_CANNOT_TO_HAND)
    c:RegisterEffect(e102)
    local e103 = e102:Clone()
    e103:SetCode(EFFECT_CANNOT_TO_DECK)
    c:RegisterEffect(e103)
    local e104 = e103:Clone()
    e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
    c:RegisterEffect(e104)
    local e105 = e104:Clone()
    e105:SetCode(EFFECT_CANNOT_DISABLE)
    c:RegisterEffect(e105)
    local e106 = e105:Clone()
    e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
    c:RegisterEffect(e106)
    local e107 = e106:Clone()
    e107:SetCode(EFFECT_CANNOT_TURN_SET)
    c:RegisterEffect(e107)
    local e108 = e107:Clone()
    e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
    c:RegisterEffect(e108)
    local e109 = e108:Clone()
    e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
    c:RegisterEffect(e109)
    local e110 = e109:Clone()
    e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
    c:RegisterEffect(e110)
    local e1102 = e109:Clone()
    e1102:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
    c:RegisterEffect(e1102)
    local e111 = e109:Clone()
    e111:SetCode(EFFECT_CANNOT_RELEASE)
    c:RegisterEffect(e111)
    local e112 = e109:Clone()
    e112:SetCode(EFFECT_CANNOT_USE_AS_COST)
    c:RegisterEffect(e112)
    local e114 = e104:Clone()
    e114:SetCode(EFFECT_CANNOT_DISEFFECT)
    c:RegisterEffect(e114)
end

function s.dragfilter(c)
    return c:IsType(TYPE_SYNCHRO) and c:IsRace(RACE_DRAGON)
end
function s.atkvalue(e)
    local g = Duel.GetMatchingGroup(s.dragfilter, e:GetHandler():GetControler(), LOCATION_MZONE, 0, nil)
    local tatk = 0
    local tc = g:GetFirst()
    while tc do
        local atk = tc:GetAttack()
        tatk = tatk + atk
        tc = g:GetNext()
    end
    return tatk
end
function s.defvalue(e)
    local g = Duel.GetMatchingGroup(s.dragfilter, e:GetHandler():GetControler(), LOCATION_MZONE, 0, nil)
    local tdef = 0
    local tc = g:GetFirst()
    while tc do
        local def = tc:GetDefense()
        tdef = tdef + def
        tc = g:GetNext()
    end
    return tdef
end

function s.spfilter(c, code)
    return c:GetOriginalCode() == code
end
function s.spcon(e, c)
    if c == nil then
        return true
    end
    local tp = c:GetControler()
    local g1 = Duel.GetReleaseGroup(tp, s.spfilter, 1, 1, nil, 44508094)
    local g2 = Duel.GetReleaseGroup(tp, s.spfilter, 1, 1, nil, 70902743)
    local g3 = Duel.GetReleaseGroup(tp, s.spfilter, 1, 1, nil, 73580471)
    local g4 = Duel.GetReleaseGroup(tp, s.spfilter, 1, 1, nil, 25862681)
    local g5 = Duel.GetReleaseGroup(tp, s.spfilter, 1, 1, nil, 9012916)
    g1:Merge(g2)
    g1:Merge(g3)
    g1:Merge(g4)
    g1:Merge(g5)
    return Duel.GetLocationCountFromEx(tp, tp, g1, c) > 0 and
               Duel.CheckReleaseGroup(tp, s.spfilter, 1, nil, 44508094) and
               Duel.CheckReleaseGroup(tp, s.spfilter, 1, nil, 73580471) and
               Duel.CheckReleaseGroup(tp, s.spfilter, 1, nil, 70902743) and
               Duel.CheckReleaseGroup(tp, s.spfilter, 1, nil, 25862681) and
               Duel.CheckReleaseGroup(tp, s.spfilter, 1, nil, 9012916)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
    local g1 = Duel.SelectReleaseGroup(tp, s.spfilter, 1, 1, nil, 44508094)
    local g2 = Duel.SelectReleaseGroup(tp, s.spfilter, 1, 1, nil, 70902743)
    local g3 = Duel.SelectReleaseGroup(tp, s.spfilter, 1, 1, nil, 73580471)
    local g4 = Duel.SelectReleaseGroup(tp, s.spfilter, 1, 1, nil, 25862681)
    local g5 = Duel.SelectReleaseGroup(tp, s.spfilter, 1, 1, nil, 9012916)
    g1:Merge(g2)
    g1:Merge(g3)
    g1:Merge(g4)
    g1:Merge(g5)
    Duel.Release(g1, REASON_COST)
end

function s.spfilter2(c, e, tp)
	return c:IsType(TYPE_SYNCHRO) and c:IsRace(RACE_DRAGON) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_SYNCHRO, tp, true, false)
end
function s.spcon2(e, tp, eg, ep, ev, re, r, rp, chk)
	local g = Duel.GetMatchingGroup(s.spfilter2, tp, LOCATION_EXTRA, 0, nil, e, tp)
	if chk == 0 then return (#g>0)
	  and Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO)>0 end
	Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_EXTRA)
end
function s.spop2(e, tp, eg, ep, ev, re, r, rp, c)
	local g = Duel.GetMatchingGroup(s.spfilter2, tp, LOCATION_EXTRA, 0, nil, e, tp)
	if Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO)<1 then return end
	local sg=Duel.SelectMatchingCard(tp, s.spfilter2, tp, LOCATION_EXTRA, 0, 1, Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO), nil, e, tp)
	Duel.SpecialSummon(sg, SUMMON_TYPE_SYNCHRO, tp, tp, true, false, POS_FACEUP)
	for sg0 in aux.Next(sg) do
		sg0:CompleteProcedure()
	end
end

function s.schcon(e, tp, eg, ep, ev, re, r, rp)
    return tp == Duel.GetTurnPlayer() and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDrawCount(tp) > 0
end
function s.schfilter(c)
    return c:IsCode(21159309) and c:IsAbleToHand()
end
function s.schtar(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then return true end
    local dt = Duel.GetDrawCount(tp)
    if dt ~= 0 then
        _replace_count = 0
        _replace_max = dt
        local e1 = Effect.CreateEffect(e:GetHandler())
        e1:SetType(EFFECT_TYPE_FIELD)
        e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
        e1:SetCode(EFFECT_DRAW_COUNT)
        e1:SetTargetRange(1, 0)
        e1:SetReset(RESET_PHASE + PHASE_DRAW)
        e1:SetValue(0)
        Duel.RegisterEffect(e1, tp)
    end
    Duel.SetOperationInfo(0, CATEGORY_TOHAND, nil, 1, tp, LOCATION_DECK)
end
function s.schop(e, tp, eg, ep, ev, re, r, rp)
    _replace_count = _replace_count + 1
    if _replace_count > _replace_max or not e:GetHandler():IsRelateToEffect(e) or e:GetHandler():IsFacedown() then
        return
    end
    local code = 21159309
    local gg = Group.FromCards(Duel.CreateToken(tp, code))
    Duel.SendtoDeck(gg, tp, nil, REASON_RULE)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
    local g = Duel.SelectMatchingCard(tp, s.schfilter, tp, LOCATION_DECK, 0, 1, 1, nil)
    if g:GetCount() > 0 then
        Duel.SendtoHand(g, nil, REASON_RULE)
        Duel.ConfirmCards(1 - tp, g)
    end
end

function s.efilter(e, te)
    return te:GetOwnerPlayer() ~= e:GetOwnerPlayer()
end

