--奧雷卡爾克斯克麗絲 (KA)
function c566.initial_effect(c)
	--act limit
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetCondition(c566.con)
	e1:SetRange(LOCATION_ONFIELD)
	e1:SetTargetRange(0,1)
	e1:SetValue(c566.aclimit)
	c:RegisterEffect(e1) 
	
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(17706537,0))
	e2:SetCategory(CATEGORY_DAMAGE)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetCode(EVENT_BATTLE_DAMAGE)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e2:SetCondition(c566.damcon)
	e2:SetTarget(c566.damtg)
	e2:SetOperation(c566.damop)
	c:RegisterEffect(e2)   
end

function c566.con(e)
	local ph=Duel.GetCurrentPhase()
	return ph>=PHASE_BATTLE_START and ph<=PHASE_BATTLE
end

function c566.aclimit(e,re,tp)
	local rc=re:GetHandler()
	return (rc:GetType()==TYPE_SPELL or rc:GetType()==TYPE_TRAP) and re:IsHasType(EFFECT_TYPE_ACTIVATE)
end

function c566.damcon(e,tp,eg,ep,ev,re,r,rp)
	return ep~=tp
end
function c566.damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetTargetPlayer(1-tp)
	local dam=Duel.GetFieldGroupCount(tp,0,LOCATION_HAND)*600
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,dam)
end

function c566.damop(e,tp,eg,ep,ev,re,r,rp)
	local p=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER)
	local dam=Duel.GetFieldGroupCount(p,LOCATION_HAND,0)*600
	Duel.Damage(p,dam,REASON_EFFECT)
end