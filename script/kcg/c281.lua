--The Eye of Timaeus
function c281.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_FUSION_SUMMON)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	e1:SetDescription(aux.Stringid(281,0))
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	--e1:SetCountLimit(1, 281)
	e1:SetTarget(c281.target)
	e1:SetOperation(c281.activate)
	c:RegisterEffect(e1)
	local e2=Effect.CreateEffect(c)
	e2=e1:Clone()
	e2:SetDescription(aux.Stringid(281,1))
	--e2:SetCountLimit(1, 281)
	e2:SetTarget(c281.target2)
	e2:SetOperation(c281.activate2)   
	c:RegisterEffect(e2) 
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(281,2))
	e3:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_FUSION_SUMMON)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCountLimit(1, 281)	
	e3:SetTarget(c281.atarget)
	e3:SetOperation(c281.aactivate)
	c:RegisterEffect(e3)

	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_SZONE)
	e100:SetCode(EFFECT_CANNOT_DISABLE)
	e100:SetValue(1)
	c:RegisterEffect(e100)
end
c281.list={[38033121]=286,[46986414]=287}

function c281.filter1(c,e,tp)
	local code=c:GetOriginalCode()
	local tcode=c281.list[code]
	return Duel.GetLocationCountFromEx(tp,tp,c,TYPE_FUSION)>0 and (c:IsControler(tp) or c:IsFaceup()) and tcode and c:IsCanBeFusionMaterial() and not c:IsImmuneToEffect(e) and not c:IsSetCard(0xa1)
end
function c281.filter2(c,tcode,e,tp)
	return c:IsCode(tcode) and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,false)
end
function c281.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then
		return Duel.IsExistingMatchingCard(c281.filter1,tp,LOCATION_MZONE,0,1,nil,e,tp)
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c281.activate(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsExistingMatchingCard(c281.filter1,tp,LOCATION_MZONE,0,1,nil,e,tp) then return end
	  Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
	local rg=Duel.SelectMatchingCard(tp,c281.filter1,tp,LOCATION_MZONE,0,1,1,nil,e,tp)
	  if rg:GetFirst():IsFacedown() then Duel.ConfirmCards(tp,rg:GetFirst()) end
	  local code=rg:GetFirst():GetOriginalCode()
	local tcode=c281.list[code]
	  --Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	--local tc=Duel.SelectMatchingCard(tp,c281.filter2,tp,LOCATION_EXTRA,0,1,1,nil,tcode,e,tp):GetFirst()
	  local tc=Duel.CreateToken(tp,tcode,nil,nil,nil,nil,nil,nil)
	  tc:SetMaterial(rg)
	Duel.SendtoGrave(rg,REASON_EFFECT+REASON_MATERIAL+REASON_FUSION)
	  Duel.BreakEffect()
	if tc and Duel.SendtoDeck(tc,tp,0,REASON_RULE)>0 and Duel.SpecialSummon(tc,SUMMON_TYPE_FUSION,tp,tp,true,false,POS_FACEUP)~=0 then
		tc:CompleteProcedure()
	end
end

function c281.filter11(c,e,tp)
	local code=c:GetOriginalCode()
	local tcode=c281.list[code]
	return Duel.GetLocationCountFromEx(tp,tp,c,TYPE_FUSION)>0 and (c:IsControler(tp) or c:IsFaceup()) and not tcode
			 and c:IsCanBeFusionMaterial() and not c:IsImmuneToEffect(e) and not c:IsSetCard(0xa1)
			 --and Duel.IsExistingMatchingCard(c281.filter22,tp,LOCATION_EXTRA,0,1,nil,e,tp)
end
function c281.filter22(c,e,tp)
	return c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,false) and c:IsCode(42)
end
function c281.target2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then
		return Duel.IsExistingMatchingCard(c281.filter11,tp,LOCATION_MZONE,0,1,nil,e,tp)
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c281.activate2(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsExistingMatchingCard(c281.filter11,tp,LOCATION_MZONE,0,1,nil,e,tp) then return end
	  Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
	  local g=Duel.SelectMatchingCard(tp,c281.filter11,tp,LOCATION_MZONE,0,1,1,nil,e,tp)
	  if g:GetFirst():IsFacedown() then Duel.ConfirmCards(tp,g:GetFirst()) end
	  --Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	  --local tc=Duel.SelectMatchingCard(tp,c281.filter22,tp,LOCATION_EXTRA,0,1,1,nil,e,tp):GetFirst()
	  local tc=Duel.CreateToken(tp,42,nil,nil,nil,nil,nil,nil)
	  tc:SetMaterial(g)
	  Duel.SendtoGrave(g,REASON_EFFECT+REASON_MATERIAL+REASON_FUSION)
	  Duel.BreakEffect()
	  local atk=g:GetFirst():GetBaseAttack()
	  if atk<0 then atk=0 end
	  if g and tc and Duel.SendtoDeck(tc,tp,0,REASON_RULE)>0 then 
		local code=g:GetFirst():GetOriginalCode()	
		Duel.SpecialSummonStep(tc,SUMMON_TYPE_FUSION,tp,tp,true,false,POS_FACEUP)
		tc:SetEntityCode(code,42,{g:GetFirst():GetOriginalSetCard(),tc:GetOriginalSetCard()},nil,nil,nil,nil,atk+500,atk-300,nil,nil,nil,true)
		-- if g:GetFirst():IsSetCard(0x48) then
		-- local mt=_G["c" .. 42]
		-- mt.xyz_number=tc.xyz_number end
		Duel.SpecialSummonComplete()
		tc:CompleteProcedure()
	  end
end

function c281.tgfilter(c,e,tp)
	return c:IsFaceup() and c:IsCanBeFusionMaterial()
		and Duel.IsExistingMatchingCard(c281.spfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp,c)
end
function c281.spfilter(c,e,tp,mc)
	if Duel.GetLocationCountFromEx(tp,tp,mc,c)<=0 then return false end
	local mustg=aux.GetMustBeMaterialGroup(tp,nil,tp,c,nil,REASON_FUSION)
	return aux.IsMaterialListCode(c,mc:GetCode()) and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,false,false)
	and (#mustg==0 or (#mustg==1 and mustg:IsContains(mc)))
end
function c281.atarget(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc==0 then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(tp) and c281.tgfilter(chkc,e,tp) end
	if chk==0 then return Duel.IsExistingTarget(c281.tgfilter,tp,LOCATION_MZONE,0,1,nil,e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	Duel.SelectTarget(tp,c281.tgfilter,tp,LOCATION_MZONE,0,1,1,nil,e,tp)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c281.aactivate(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc:IsRelateToEffect(e) and tc:IsFaceup() and tc:IsCanBeFusionMaterial() and not tc:IsImmuneToEffect(e) then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local sg=Duel.SelectMatchingCard(tp,c281.spfilter,tp,LOCATION_EXTRA,0,1,1,nil,e,tp,tc)
		local sc=sg:GetFirst()
		if sc then
			sc:SetMaterial(Group.FromCards(tc))
			Duel.SendtoGrave(tc,REASON_EFFECT+REASON_MATERIAL+REASON_FUSION)
			Duel.BreakEffect()
			Duel.SpecialSummon(sc,SUMMON_TYPE_FUSION,tp,tp,false,false,POS_FACEUP)
			sc:CompleteProcedure()
		end
	end
end