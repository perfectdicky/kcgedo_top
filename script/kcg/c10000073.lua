--侵攻之神官
function c10000073.initial_effect(c)
	--场上有幻神时破坏效果怪兽效果无效
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_BATTLE_END)
	e1:SetCondition(c10000073.discon)
	e1:SetOperation(c10000073.disop)
	c:RegisterEffect(e1)
	
	--场上有幻神时可以连锁攻击
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(10000073,0))
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_BATTLE_END)
	e2:SetCondition(c10000073.atcon)
	e2:SetOperation(c10000073.atop)
	c:RegisterEffect(e2)
end

function c10000073.filter(c,att)
	return c:IsFaceup() and c:IsRace(att)
end

function c10000073.discon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	return bc and bc:IsStatus(STATUS_BATTLE_DESTROYED) and not c:IsStatus(STATUS_BATTLE_DESTROYED)
		and Duel.IsExistingMatchingCard(c10000073.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil,RACE_DIVINE)
end

function c10000073.disop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_DISABLE)
	e1:SetReset(RESET_EVENT+0x17a0000)
	bc:RegisterEffect(e1)
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_DISABLE_EFFECT)
	e2:SetReset(RESET_EVENT+0x17a0000)
	bc:RegisterEffect(e2)
end

function c10000073.atcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	return bc and bc:IsStatus(STATUS_BATTLE_DESTROYED) and c:CanChainAttack()
		and not c:IsDisabled() and Duel.IsExistingMatchingCard(c10000073.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil,RACE_DIVINE)
end

function c10000073.atop(e,tp,eg,ep,ev,re,r,rp)
	Duel.ChainAttack()
end
