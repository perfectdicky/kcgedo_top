--エクゾディア·ネクロス
function c149.initial_effect(c)
	c:EnableReviveLimit()
	--cannot destroy
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	  e1:SetCondition(c149.batdescon)
	e1:SetValue(1)
	c:RegisterEffect(e1)

	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	  e2:SetCondition(c149.descon1)
	e2:SetValue(c149.efdes1)
	c:RegisterEffect(e2)
	  e21=e2:Clone()
	  e21:SetCondition(c149.descon2)
	e21:SetValue(c149.efdes2)
	c:RegisterEffect(e21)  
	  e22=e2:Clone()
	  e22:SetCondition(c149.descon3)
	e22:SetValue(c149.efdes3)
	c:RegisterEffect(e22) 
	local e23=Effect.CreateEffect(c)
	e23:SetCategory(CATEGORY_ATKCHANGE)
	e23:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e23:SetCode(EVENT_BATTLED)
	  e23:SetRange(LOCATION_MZONE)
	  e23:SetCondition(c149.descon4)
	e23:SetOperation(c149.operation)
	c:RegisterEffect(e23)

	--spsummon limit
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e3:SetCode(EFFECT_SPSUMMON_CONDITION)
	e3:SetValue(c149.splimit)
	c:RegisterEffect(e3)

	--atkup
	--local e4=Effect.CreateEffect(c)
	--e4:SetDescription(aux.Stringid(12600382,0))
	--e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)   
	--e4:SetCategory(CATEGORY_ATKCHANGE)
	--e4:SetCode(EVENT_PHASE+PHASE_STANDBY)
	--e4:SetRange(LOCATION_MZONE)
	--e4:SetCountLimit(1)
	--e4:SetProperty(EFFECT_FLAG_REPEAT)
	--e4:SetCondition(c149.atkcon)
	--e4:SetOperation(c149.atkop)
	--c:RegisterEffect(e4)

	--selfdes
	--local e5=Effect.CreateEffect(c)
	--e5:SetType(EFFECT_TYPE_SINGLE)
	--e5:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	--e5:SetRange(LOCATION_MZONE)
	--e5:SetCode(EFFECT_SELF_DESTROY)
	--e5:SetCondition(c12600382.descon)
	--c:RegisterEffect(e5)
end

function c149.batdesfilter(c)
	return c:IsCode(33396948)
end
function c149.batdescon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c149.batdesfilter,e:GetHandlerPlayer(),LOCATION_GRAVE,0,1,nil)
end

function c149.desfilter1(c)
	return c:IsCode(44519536)
end
function c149.descon1(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c149.desfilter1,e:GetHandlerPlayer(),LOCATION_GRAVE,0,1,nil)
end
function c149.efdes1(e,te)
	return te:IsActiveType(TYPE_SPELL)
end
function c149.desfilter2(c)
	return c:IsCode(7902349)
end
function c149.descon2(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c149.desfilter2,e:GetHandlerPlayer(),LOCATION_GRAVE,0,1,nil)
end
function c149.efdes2(e,te)
	return te:IsActiveType(TYPE_MONSTER)
end
function c149.desfilter3(c)
	return c:IsCode(8124921)
end
function c149.descon3(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c149.desfilter3,e:GetHandlerPlayer(),LOCATION_GRAVE,0,1,nil)
end
function c149.efdes3(e,te)
	return te:IsActiveType(TYPE_TRAP)
end
function c149.desfilter4(c)
	return c:IsCode(70903634)
end
function c149.descon4(e,tp,eg,ep,ev,re,r,rp)
	  if Duel.GetAttacker()~=nil or Duel.GetAttackTarget()~=nil then
	  return e:GetHandler()==Duel.GetAttacker() or e:GetHandler()==Duel.GetAttackTarget() end
end
function c149.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) and c:IsFaceup() then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetProperty(EFFECT_FLAG_COPY_INHERIT)
		e1:SetValue(1000)
		e1:SetReset(RESET_EVENT+0x1ff0000)
		c:RegisterEffect(e1)
	end
end

function c149.splimit(e,se,sp,st)
	return st==(SUMMON_TYPE_SPECIAL+332449)
end

function c149.atkcon(e,tp,eg,ep,ev,re,r,rp)
	return tp==Duel.GetTurnPlayer()
end
function c149.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_COPY_INHERIT)
	e1:SetReset(RESET_EVENT+0x1ff0000)
	e1:SetValue(500)
	c:RegisterEffect(e1)
end
function c149.descon(e)
	local p=e:GetHandlerPlayer()
	return not Duel.IsExistingMatchingCard(Card.IsCode,p,LOCATION_GRAVE,0,1,nil,8124921)
		or not Duel.IsExistingMatchingCard(Card.IsCode,p,LOCATION_GRAVE,0,1,nil,44519536)
		or not Duel.IsExistingMatchingCard(Card.IsCode,p,LOCATION_GRAVE,0,1,nil,70903634)
		or not Duel.IsExistingMatchingCard(Card.IsCode,p,LOCATION_GRAVE,0,1,nil,7902349)
		or not Duel.IsExistingMatchingCard(Card.IsCode,p,LOCATION_GRAVE,0,1,nil,33396948)
end
