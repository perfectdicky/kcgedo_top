--混沌幻魔 阿米泰尔
local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	Fusion.AddProcMix(c,true,true,6007213,32491822,69890967)
	Fusion.AddContactProc(c,s.contactfil,s.contactop)
	
	--融合特召限制
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetType(EFFECT_TYPE_SINGLE)
	-- e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	-- e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	-- e1:SetValue(s.splimit)
	-- c:RegisterEffect(e1)

	--special summon rule
	-- local e0=Effect.CreateEffect(c)
	-- e0:SetType(EFFECT_TYPE_FIELD)
	-- e0:SetCode(EFFECT_SPSUMMON_PROC)
	-- e0:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	-- e0:SetRange(LOCATION_EXTRA)
	-- e0:SetCondition(s.spcon)
	-- e0:SetOperation(s.spop)
	-- c:RegisterEffect(e0)
	
	--不会被战斗破坏
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e2:SetValue(1)
	c:RegisterEffect(e2)
	
	--攻击对方怪兽伤害阶段攻击提升
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(10000044,0))
	e3:SetCategory(CATEGORY_ATKCHANGE)
	e3:SetType(EFFECT_TYPE_IGNITION)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCountLimit(1)
	e3:SetTarget(s.atkcon)
	e3:SetOperation(s.atkop)
	c:RegisterEffect(e3)
	
	--不受陷阱效果以及魔法效果怪兽生效一回合
	local e82 = Effect.CreateEffect(c)
    e82:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e82:SetCode(EVENT_CHAIN_SOLVED)
    e82:SetRange(LOCATION_MZONE)
    e82:SetCondition(s.sdcon2)
    e82:SetOperation(s.sdop2)
    c:RegisterEffect(e82)
    local e80=Effect.CreateEffect(c)
	e80:SetType(EFFECT_TYPE_SINGLE)
	e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e80:SetRange(LOCATION_MZONE)
	e80:SetCode(EFFECT_IMMUNE_EFFECT)
	e80:SetValue(s.efilterr)
	c:RegisterEffect(e80)

	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(10000044,1))
	e5:SetCategory(CATEGORY_CONTROL)
	e5:SetType(EFFECT_TYPE_IGNITION)
	e5:SetRange(LOCATION_MZONE)
	e5:SetCountLimit(1)
	e5:SetCondition(s.copycon)
	e5:SetOperation(s.copyop)
	c:RegisterEffect(e5)

	local e6=Effect.CreateEffect(c)
	e6:SetDescription(aux.Stringid(10000044,2))
	e6:SetCategory(CATEGORY_REMOVE)
	e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e6:SetCode(EVENT_PHASE+PHASE_END)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCountLimit(1)
	e6:SetTarget(s.destg)
	e6:SetOperation(s.desop)
	c:RegisterEffect(e6)

	local te=Effect.CreateEffect(c)
	te:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	te:SetRange(LOCATION_MZONE)
	te:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	te:SetCountLimit(1)
	te:SetCode(EVENT_PHASE+PHASE_END)	
	te:SetOperation(s.reop2)	
	c:RegisterEffect(te)	
end

s.listed_names={6007213,32491822,69890967}
function s.splimit(e,se,sp,st)
	return e:GetHandler():GetLocation()~=LOCATION_EXTRA
end
function s.contactfil(tp)
	return Duel.GetMatchingGroup(Card.IsAbleToRemoveAsCost,tp,LOCATION_ONFIELD,0,nil)
end
function s.contactop(g)
	Duel.Remove(g,POS_FACEUP,REASON_COST+REASON_MATERIAL)
end

function s.spfilter(c,code)
	return c:IsFaceup() and c:IsCode(code) and c:IsAbleToRemoveAsCost() 
end
function s.spcon(e,c)
	if c==nil then return true end 
	local tp=c:GetControler()
	return Duel.GetLocationCountFromEx(tp,tp,nil,c)>0
		and Duel.IsExistingMatchingCard(s.spfilter,tp,LOCATION_ONFIELD,0,1,nil,6007213)
		and Duel.IsExistingMatchingCard(s.spfilter,tp,LOCATION_ONFIELD,0,1,nil,32491822)
		and Duel.IsExistingMatchingCard(s.spfilter,tp,LOCATION_ONFIELD,0,1,nil,69890967)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp,c)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g1=Duel.SelectMatchingCard(tp,s.spfilter,tp,LOCATION_ONFIELD,0,1,1,nil,6007213)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g2=Duel.SelectMatchingCard(tp,s.spfilter,tp,LOCATION_ONFIELD,0,1,1,nil,32491822)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g3=Duel.SelectMatchingCard(tp,s.spfilter,tp,LOCATION_ONFIELD,0,1,1,nil,69890967)
	g1:Merge(g2)
	g1:Merge(g3)
	Duel.Remove(g1,POS_FACEUP,REASON_COST)
end
------------------------------------------------------------------------------
function s.atfilter(c)
	return not c:IsHasEffect(EFFECT_CANNOT_BE_BATTLE_TARGET) and not c:IsHasEffect(EFFECT_IGNORE_BATTLE_TARGET)
end
function s.atkcon(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()  
	local tg=Duel.GetMatchingGroup(s.atfilter,tp,0,LOCATION_MZONE,nil)
	if chk==0 then return tg:GetCount()>0 and c:CanAttack() and c:IsAttackPos() end
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp) 
	local c=e:GetHandler() 
	local tg=Duel.GetMatchingGroup(s.atfilter,tp,0,LOCATION_MZONE,nil)
	if tg:GetCount()<1 then return end	
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATTACKTARGET)  
	local g=Duel.SelectMatchingCard(tp,s.atfilter,tp,0,LOCATION_MZONE,1,1,nil)
	if g:GetCount()<1 then return end	
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetReset(RESET_EVENT+RESETS_STANDARD)
	e3:SetValue(10000)
	c:RegisterEffect(e3)	
	local tc=g:GetFirst()
	if tc then Duel.ForceAttack(c,tc) end
end
------------------------------------------------------------------------------
function s.condtion(e)
	local ph=Duel.GetCurrentPhase()
	return (ph==PHASE_DAMAGE or ph==PHASE_DAMAGE_CAL)
		and (Duel.GetAttacker()==e:GetHandler() or Duel.GetAttackTarget()==e:GetHandler())
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.efilterr(e,te)   
	local c=e:GetHandler()  
	local tc=te:GetOwner()
	local turncount=Duel.GetTurnCount()-tc:GetTurnID()
	if c:GetTurnID()==Duel.GetTurnCount() then turncount=0 end
	if tc==e:GetOwner() or te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then return false
	else return te:GetActiveType()==TYPE_TRAP 
	end
end

function s.sdcon2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=1
                    break 
                end
            end
        end
    end
    return eff==1
end
function s.sdop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=te
                    break 
                end
            end
        end
    end
    if eff==0 then return end
    local e83 = Effect.CreateEffect(c)
    e83:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e83:SetRange(LOCATION_MZONE)
    e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
    e83:SetCountLimit(1)
    e83:SetCode(EVENT_PHASE + PHASE_END)
    e83:SetLabelObject(eff)
    e83:SetOperation(s.setop2)
    e83:SetReset(RESET_PHASE + PHASE_END)
    c:RegisterEffect(e83) 
end
function s.setop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local te = e:GetLabelObject()
    if not te or te==0 then return end
    te:Reset()
end   

function s.copycon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetLocationCount(1-tp,LOCATION_MZONE)>0
end
function s.copyop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if Duel.GetLocationCount(1-tp,LOCATION_MZONE)>0 and Duel.GetControl(c,1-tp) then
	c:RegisterFlagEffect(100000441,RESET_EVENT+0x1ff0000+RESET_PHASE+PHASE_END,0,1) end
end

function s.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetFlagEffect(100000441)~=0 end
	local g=Duel.GetMatchingGroup(Card.IsAbleToRemove,tp,LOCATION_ONFIELD,0,e:GetHandler())
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,g,g:GetCount(),0,0)
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(Card.IsAbleToRemove,tp,LOCATION_ONFIELD,0,e:GetHandler())
	Duel.Remove(g,POS_FACEUP,REASON_EFFECT)
	Duel.GetControl(e:GetHandler(),1-tp)  
end

function s.reop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	for i=1,428 do
	if c:IsHasEffect(i) then 
		local ae={c:IsHasEffect(i)}
		for _,te in ipairs(ae) do
			if te:GetOwner()~=e:GetOwner() and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then
			local e80=Effect.CreateEffect(c)
			e80:SetType(EFFECT_TYPE_SINGLE)
			e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
			e80:SetRange(LOCATION_MZONE)
			e80:SetCode(EFFECT_IMMUNE_EFFECT)
			e80:SetValue(function(e,te2) return te2==te end)
			c:RegisterEffect(e80) end
		end
	end
	end
end