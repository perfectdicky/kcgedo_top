--Number S39 - Utopia ONE
local s, id = GetID()
function c65.initial_effect(c)
	--xyz summon
	c:EnableReviveLimit()
	Xyz.AddProcedure(c,aux.FilterBoolFunctionEx(Card.IsAttribute,ATTRIBUTE_LIGHT),4,3,c65.ovfilter,aux.Stringid(86532744,1))

	  --special summon 
	--   local e00=Effect.CreateEffect(c)  
	--   e00:SetType(EFFECT_TYPE_FIELD) 
	--   e00:SetCode(EFFECT_SPSUMMON_PROC)  
	--   e00:SetProperty(EFFECT_FLAG_UNCOPYABLE)  
	--   e00:SetRange(LOCATION_EXTRA)  
	--   e00:SetCondition(c65.spcon)  
	--   e00:SetOperation(c65.spop)  
	--   e00:SetValue(SUMMON_TYPE_XYZ)
	--   c:RegisterEffect(e00)  
	--attack up
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_REMOVE+CATEGORY_DAMAGE)
	e1:SetDescription(aux.Stringid(86532744,0))
	--e1:SetProperty(0)   
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(c65.cost)
	e1:SetTarget(c65.target)
	e1:SetOperation(c65.operation)
	c:RegisterEffect(e1,false,REGISTER_FLAG_DETACH_XMAT)
	  --cannot destroyed
	  local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e0:SetValue(c65.indes)
	c:RegisterEffect(e0)
end
c65.xyz_number=39
c65.listed_names={84013237}

 function c65.mfilter(c,xyzc)
	return c:IsFaceup() and c:IsCode(84013237) and c:IsCanBeXyzMaterial(xyzc)
end
function c65.spcon(e,c,og)
	if c==nil then return true end
	local tp=c:GetControler()
	local mg=nil
	if og then
		mg=og:Filter(c65.mfilter,nil,c)
	else
		mg=Duel.GetMatchingGroup(c65.mfilter,tp,LOCATION_MZONE,0,nil,c)
	end
	return Duel.GetLocationCount(tp,LOCATION_MZONE)>-1 and mg:GetCount()>0
		and Duel.GetLP(tp)<=Duel.GetLP(1-tp)-3000 
end
function c65.spop(e,tp,eg,ep,ev,re,r,rp,c,og)
	local g=nil
	local sg=Group.CreateGroup()
	if og then
		g=og
		local tc=og:GetFirst()
		while tc do
			sg:Merge(tc:GetOverlayGroup())
			tc=og:GetNext()
		end
	else
		local mg=Duel.GetMatchingGroup(c65.mfilter,tp,LOCATION_MZONE,0,nil)
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
		g=mg:FilterSelect(tp,c65.mfilter,1,1,nil)
			local tc1=g:GetFirst()
		sg:Merge(tc1:GetOverlayGroup())
	end
	Duel.Overlay(c,sg)
	c:SetMaterial(g)
	Duel.Overlay(c,g)
end
 function c65.indes(e,c)
	return not e:GetHandler():GetBattleTarget():IsSetCard(0x48) and not e:GetHandler():GetBattleTarget():IsSetCard(0x1048)
	  and not e:GetHandler():GetBattleTarget():IsSetCard(0x2048)
end
function c65.desfilter2(c)
	return c:IsFaceup() and c:IsType(TYPE_MONSTER) and (not c:IsSetCard(0x48) or c:IsSetCard(0x1048))
end
function c65.descon2(e)
	local c=e:GetHandler()
	return Duel.IsExistingMatchingCard(c65.desfilter2,c:GetControler(),0,LOCATION_MZONE,1,c)
end
 function c65.ovfilter(c,tp,lc)
	return c:IsFaceup() and c:IsSummonCode(lc,SUMMON_TYPE_XYZ,tp,84013237)
end
function c65.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=e:GetHandler():GetOverlayGroup():GetCount()
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) and Duel.GetLP(tp)>1 end
	e:GetHandler():RemoveOverlayCard(tp,g,g,REASON_COST)
	  Duel.SetLP(tp,1)
end
function c65.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(Card.IsAbleToRemove,tp,0,LOCATION_MZONE,1,nil) end
	local sg=Duel.GetMatchingGroup(Card.IsAbleToRemove,tp,0,LOCATION_MZONE,nil)
	local sg2=Duel.GetMatchingGroup(c65.ovfilter2,tp,0,LOCATION_MZONE,nil,e)
	  local dam=0
	  local tc=sg2:GetFirst()
	  while tc do
	  local atk=tc:GetAttack()
	  dam=dam+atk
	  tc=sg2:GetNext() end
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,nil,sg:GetCount(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,dam)
end
function c65.ovfilter2(c,e)
	return c:IsAbleToRemove() and not c:IsImmuneToEffect(e)
end
function c65.operation(e,tp,eg,ep,ev,re,r,rp)
local sg=Duel.GetMatchingGroup(c65.ovfilter2,tp,0,LOCATION_MZONE,nil,e)
local dam=0
local tc=sg:GetFirst()
while tc do
local atk=tc:GetAttack()
dam=dam+atk
tc=sg:GetNext() end
if Duel.Remove(sg,POS_FACEUP,REASON_EFFECT)>0 then
Duel.BreakEffect()
Duel.Damage(1-tp,dam,REASON_EFFECT) end
end
 function c65.infilter(e,c)
	return c:IsSetCard(0x48) and c:IsType(TYPE_MONSTER)
end