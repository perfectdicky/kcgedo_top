--E'Rah
function c347.initial_effect(c)
	-- local e4=Effect.CreateEffect(c)
	-- e4:SetType(EFFECT_TYPE_SINGLE)
	-- e4:SetCode(EFFECT_ATTACK_ALL)
	-- e4:SetValue(1)
	-- e4:SetCondition(c347.condition2)
	-- c:RegisterEffect(e4)
end

function c347.filter4(c)
	return c:GetOriginalCode()==331
end
function c347.filter5(c,code)
	return c:GetOverlayCount()>0 and c:GetOverlayGroup():FilterCount(c347.filter4,nil)>0 
	 and c:IsCode(code)
end
function c347.condition2(e,tp,eg,ep,ev,re,r,rp) 
	local c=e:GetHandler()
	local gg=Duel.GetMatchingGroup(c347.filter5,tp,LOCATION_MZONE,LOCATION_MZONE,nil,c:GetCode())
	return gg:GetCount()>0
end
