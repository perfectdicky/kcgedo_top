function c122800.initial_effect(c)
	--fusion material
	c:EnableReviveLimit()
	--Fusion.AddProcFun2(c,aux.FilterBoolFunction(Card.IsSetCard,0x900),aux.FilterBoolFunction(c122800.spfilter),true)
	--spsummon condition
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(c122800.splimit)
	c:RegisterEffect(e1)
	--special summon rule
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SPSUMMON_PROC)
	e2:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e2:SetRange(LOCATION_EXTRA)
	e2:SetCondition(c122800.sprcon)
	e2:SetOperation(c122800.sprop)
	c:RegisterEffect(e2)
	--Battle
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_ATTACK_ANNOUNCE)
	e3:SetOperation(c122800.actop)
	c:RegisterEffect(e3)
	--local e4=Effect.CreateEffect(c)
	--e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	--e4:SetCode(EVENT_BE_BATTLE_TARGET)
	--e4:SetOperation(c122800.actop)
	--c:RegisterEffect(e4)
	--damage
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e6:SetDescription(aux.Stringid(122800,1))
	e6:SetCategory(CATEGORY_DAMAGE)
	e6:SetCode(EVENT_PHASE+PHASE_STANDBY)
	e6:SetRange(LOCATION_ONFIELD)
	e6:SetCountLimit(1)
	e6:SetCondition(c122800.damcon)
	e6:SetTarget(c122800.damtg)
	e6:SetOperation(c122800.damop)
	c:RegisterEffect(e6)
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_SINGLE)
	e7:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e7:SetRange(LOCATION_ONFIELD)
	e7:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e7:SetValue(1)
	c:RegisterEffect(e7)
	--battle indes
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e8:SetRange(LOCATION_ONFIELD)
	e8:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
	e8:SetCountLimit(1)
	e8:SetValue(c122800.valcon)
	c:RegisterEffect(e8)
end
c122800.listed_series={0x900}
c122800.material_setcode={0x900}

function c122800.valcon(e,re,r,rp)
	return bit.band(r,REASON_BATTLE)~=0
end
function c122800.actop(e,tp,eg,ep,ev,re,r,rp)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetTargetRange(0,1)
	e1:SetValue(aux.TRUE)
	e1:SetReset(RESET_PHASE+PHASE_DAMAGE)
	Duel.RegisterEffect(e1,tp)

	if Duel.GetAttackTarget()~=nil and Duel.GetAttacker()~=nil and Duel.GetAttacker()==e:GetHandler() then
	local tc=Duel.GetAttackTarget()
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetReset(RESET_PHASE+PHASE_DAMAGE_CAL)
	e1:SetValue(tc:GetAttack())
	e:GetHandler():RegisterEffect(e1)
	end
end

function c122800.damcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()==tp
end
function c122800.damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetTargetPlayer(e:GetHandler():GetControler())
	Duel.SetTargetParam(1500)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,0,0,tp,1500)
end
function c122800.damop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Damage(p,d,REASON_EFFECT)
end

function c122800.splimit(e,se,sp,st)
	return e:GetHandler():GetLocation()~=LOCATION_EXTRA
end

function c122800.spfilter(c,tp,sc)
	return c:IsSetCard(0x900) and c:IsType(TYPE_FUSION) and c:IsCanBeFusionMaterial() and (c:IsAbleToDeckAsCost() or c:IsAbleToExtraAsCost())
	and Duel.IsExistingMatchingCard(c122800.spfilter2,tp,LOCATION_ONFIELD,0,1,c,c,tp,sc)
end
function c122800.spfilter2(c,tc,tp,sc)
	return c:IsSetCard(0x900) and c:IsCanBeFusionMaterial() and (c:IsAbleToDeckAsCost() or c:IsAbleToExtraAsCost())
	and Duel.GetLocationCountFromEx(tp,tp,Group.FromCards(c,tc),sc)>0
end
function c122800.sprcon(e,c)
	if c==nil then return true end 
	local tp=c:GetControler()
	return Duel.IsExistingMatchingCard(c122800.spfilter,tp,LOCATION_MZONE,0,1,nil,tp,e:GetHandler())
end
function c122800.sprop(e,tp,eg,ep,ev,re,r,rp,c)
	if not Duel.IsExistingMatchingCard(c122800.spfilter,tp,LOCATION_MZONE,0,1,nil,tp,e:GetHandler()) then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(tp,c122800.spfilter,tp,LOCATION_MZONE,0,1,1,nil,tp,e:GetHandler())
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
	local g2=Duel.SelectMatchingCard(tp,c122800.spfilter2,tp,LOCATION_MZONE,0,1,1,g:GetFirst(),g:GetFirst(),tp,e:GetHandler())
	g:Merge(g2)
	local tc=g:GetFirst()
	while tc do
		if not tc:IsFaceup() then Duel.ConfirmCards(1-tp,tc) end
		tc=g:GetNext()
	end
	e:GetHandler():SetMaterial(g) 
	Duel.SendtoDeck(g,nil,2,REASON_COST+REASON_MATERIAL+REASON_FUSION)
end
