--暗黑方界邪神　深藍新星三位一體 (KA)
function c759.initial_effect(c)
	 --fusion material
	c:EnableReviveLimit()
	Fusion.AddProcFunRep(c,c759.matfilter,3,true)

	--spsummon condition
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetCode(EFFECT_SPSUMMON_CONDITION)
	e0:SetValue(aux.fuslimit)
	c:RegisterEffect(e0)

	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e8:SetCode(EVENT_SPSUMMON_SUCCESS)
	e8:SetOperation(c759.tgop)
	c:RegisterEffect(e8)

	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_SET_ATTACK)
	e3:SetValue(c759.atkval)
	c:RegisterEffect(e3)

   local e4=Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DELAY)
	e4:SetCategory(CATEGORY_TOHAND+CATEGORY_SPECIAL_SUMMON)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e4:SetCode(EVENT_DESTROYED)
	e4:SetTarget(c759.destg)
	e4:SetOperation(c759.desop)
	c:RegisterEffect(e4)
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_LEAVE_FIELD)
	e5:SetOperation(c759.op)
	e5:SetLabelObject(e4)
	c:RegisterEffect(e5)

	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(12744567,0))
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1)
	e1:SetTarget(c759.target)
	e1:SetOperation(c759.operation)
	c:RegisterEffect(e1)

	local e20=Effect.CreateEffect(c)
	e20:SetType(EFFECT_TYPE_FIELD)
	e20:SetRange(LOCATION_MZONE)
	e20:SetTargetRange(0,0x16)
	e20:SetTarget(c759.disable)
	e20:SetCode(EFFECT_DISABLE)
	c:RegisterEffect(e20)
	local e21=e20:Clone()
	e21:SetCode(EFFECT_CANNOT_ATTACK)
	c:RegisterEffect(e21)

	--if not c759.global_check then
		--c759.global_check=true
		--local ge2=Effect.CreateEffect(c)
		--ge2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		--ge2:SetCode(EVENT_ADJUST)
		--ge2:SetCountLimit(1)
		--ge2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
		--ge2:SetOperation(c759.archchk)
		--Duel.RegisterEffect(ge2,0)
	--end
end

function c759.matfilter(c)
	return c:IsType(TYPE_FUSION) and c:IsSetCard(0xe3)
end

function c759.archchk(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetFlagEffect(0,420)==0 then 
		Duel.CreateToken(tp,420)
		Duel.CreateToken(1-tp,420)
		Duel.RegisterFlagEffect(0,420,0,0,0)
	end
end

function c759.filter(c)
	return c:IsFaceup() and c:IsSetCard(0xe3) and c:IsType(TYPE_MONSTER)
end
function c759.tgop(e,tp,eg,ep,ev,re,r,rp)
	local g=e:GetHandler():GetMaterialCount()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
	local ag=Duel.SelectMatchingCard(tp,c759.filter,tp,LOCATION_DECK+LOCATION_GRAVE,LOCATION_GRAVE,1,g,nil) 
	Duel.Overlay(e:GetHandler(),ag)
end

function c759.atkval(e)
	return e:GetHandler():GetOverlayGroup():FilterCount(Card.IsType,nil,TYPE_MONSTER)*1200
end

function c759.spfilter(c,e,tp)
	return c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_SPECIAL,tp,true,false) and c:IsType(TYPE_MONSTER) and c:IsSetCard(0xe3)
end
function c759.destg(e,tp,eg,ev,ep,re,r,rp,chk)
	local c=e:GetHandler()
	local g=e:GetLabelObject()
	if chk==0 then return e:GetHandler():IsAbleToDeck() and (g and g:GetCount()>0 and Duel.GetLocationCount(tp,LOCATION_MZONE)>=g:GetCount()-1 and not (Duel.IsPlayerAffectedByEffect(tp,59822133) and g:GetCount()>1)) end
	Duel.SetOperationInfo(0,CATEGORY_TODECK,c,1,0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,g:GetCount(),0,0)
end
function c759.desop(e,tp,eg,ev,ep,re,r,rp)
	local c=e:GetHandler()
	local mg=e:GetLabelObject()
	if Duel.SendtoDeck(c,nil,2,REASON_EFFECT)~=0 then
	if mg:FilterCount(c759.spfilter,nil,e,tp)<1 or Duel.GetLocationCount(tp,LOCATION_MZONE)<mg:GetCount() 
	   or (Duel.IsPlayerAffectedByEffect(tp,59822133) and mg:GetCount()>1) then return end
	Duel.SpecialSummon(mg,SUMMON_TYPE_SPECIAL,tp,tp,true,false,POS_FACEUP)
	mg:DeleteGroup()
	end
end

function c759.op(e,tp,eg,ep,ev,re,r,rp)
	local g=e:GetHandler():GetOverlayGroup()
	if g:GetCount()>0 then
	local mg=g:Filter(c759.spfilter,nil,e,tp)
	mg:KeepAlive()
	e:GetLabelObject():SetLabelObject(mg) end
end

function c759.zfilter(c)
	return not c:IsType(TYPE_TOKEN) and c:IsAbleToChangeControler()
end
function c759.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(1-tp) and c759.zfilter(chkc) end
	if chk==0 then return e:GetHandler():IsType(TYPE_XYZ) and Duel.IsExistingTarget(c759.zfilter,tp,0,LOCATION_MZONE,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
	Duel.SelectTarget(tp,c759.zfilter,tp,0,LOCATION_MZONE,1,1,nil)
end
function c759.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if c:IsRelateToEffect(e) and tc:IsRelateToEffect(e) and not tc:IsImmuneToEffect(e) then
		local og=tc:GetOverlayGroup()
		if og:GetCount()>0 then
			Duel.SendtoGrave(og,REASON_RULE)
		end
		Duel.Overlay(c,Group.FromCards(tc))
		Duel.SetLP(1-tp,Duel.GetLP(1-tp)/2)
	end
end

function c759.disable(e,c)
	return c:IsType(TYPE_MONSTER)
end
