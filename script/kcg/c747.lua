--闇黑死亡眼 (KA)
function c747.initial_effect(c)
	Fusion.AddProcFunRep(c,c747.mat_filter2,2,true)
	c:EnableReviveLimit()

	--spsummon condition
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetCode(EFFECT_SPSUMMON_CONDITION)
	e0:SetValue(aux.fuslimit)
	c:RegisterEffect(e0)

	--immune
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e1:SetCode(EFFECT_IMMUNE_EFFECT)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTargetRange(LOCATION_SZONE,0)
	e1:SetCondition(c747.condition)
	e1:SetValue(c747.efilter)
	--c:RegisterEffect(e1)

	-- local e41=Effect.CreateEffect(c)
	-- e41:SetType(EFFECT_TYPE_FIELD)
	-- e41:SetCode(740)
	-- e41:SetRange(LOCATION_MZONE)
	-- e41:SetTargetRange(LOCATION_HAND,0)
	-- e41:SetCondition(c747.condition)
	-- e41:SetTarget(c747.tfilter)
	--c:RegisterEffect(e41)   

	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(100000703,0))
	e2:SetType(EFFECT_TYPE_QUICK_O)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCountLimit(1)
	e2:SetCondition(c747.ddcondition)
	e2:SetOperation(c747.operation)
	--c:RegisterEffect(e2)

	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(15240238,0))
	e3:SetCategory(CATEGORY_NEGATE+CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_QUICK_O)
	e3:SetCode(EVENT_CHAINING)
	e3:SetProperty(0+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCondition(c747.descondition)
	e3:SetTarget(c747.target)
	e3:SetOperation(c747.activate)
	c:RegisterEffect(e3,false,REGISTER_FLAG_DETACH_XMAT)
	local e4=e3:Clone()
	e4:SetCondition(c747.handcondition)
	c:RegisterEffect(e4,false,REGISTER_FLAG_DETACH_XMAT)
	local e5=e3:Clone()
	e5:SetCondition(c747.deckcondition)
	c:RegisterEffect(e5,false,REGISTER_FLAG_DETACH_XMAT)
	local e6=e3:Clone()
	e6:SetCondition(c747.rmcondition)
	c:RegisterEffect(e6,false,REGISTER_FLAG_DETACH_XMAT)
	local e7=e3:Clone()
	e7:SetCondition(c747.ctcondition)
	c:RegisterEffect(e7,false,REGISTER_FLAG_DETACH_XMAT)
	local e8=e3:Clone()
	e8:SetCondition(c747.ctcondition2)
	--e8:SetCountLimit(1) 
	c:RegisterEffect(e8,false,REGISTER_FLAG_DETACH_XMAT)
	local e9=e3:Clone()
	e9:SetCondition(c747.togravecondition)
	c:RegisterEffect(e9,false,REGISTER_FLAG_DETACH_XMAT)
	local e10=e3:Clone()
	e10:SetCondition(c747.releasecondition)
	c:RegisterEffect(e10,false,REGISTER_FLAG_DETACH_XMAT)

	-- local e40=Effect.CreateEffect(c)
	-- e40:SetType(EFFECT_TYPE_FIELD)
	-- e40:SetCode(740)
	-- e40:SetRange(LOCATION_MZONE)
	-- e40:SetTargetRange(LOCATION_SZONE,0)
	-- e40:SetTarget(c747.tfilter2)
	-- c:RegisterEffect(e40)

	-- local e06=Effect.CreateEffect(c)
	-- e06:SetType(EFFECT_TYPE_FIELD)
	-- e06:SetCode(100000703)
	-- e06:SetRange(LOCATION_MZONE)
	-- e06:SetTargetRange(LOCATION_ONFIELD,0)
	-- c:RegisterEffect(e06)	

	local e14=Effect.CreateEffect(c)
	e14:SetDescription(aux.Stringid(185,0))
	e14:SetType(EFFECT_TYPE_QUICK_O)
	e14:SetCode(EVENT_FREE_CHAIN)
	e14:SetRange(LOCATION_MZONE)
	e14:SetCountLimit(1)
	e14:SetTarget(c747.rvtg)	
	e14:SetOperation(c747.operation)
	c:RegisterEffect(e14)

	local e17=Effect.CreateEffect(c)
	e17:SetDescription(aux.Stringid(185,1))
	e17:SetType(EFFECT_TYPE_QUICK_O)
	e17:SetCode(EVENT_FREE_CHAIN)
	e17:SetRange(LOCATION_MZONE)
	e17:SetCountLimit(1)
	e17:SetTarget(c747.retg)
	e17:SetOperation(c747.reop)
	c:RegisterEffect(e17)
	
	local e18=Effect.CreateEffect(c)
	e18:SetDescription(aux.Stringid(185,2))
	e18:SetType(EFFECT_TYPE_QUICK_O)
	e18:SetCode(EVENT_FREE_CHAIN)
	e18:SetRange(LOCATION_MZONE)
	e18:SetCountLimit(1)
	e18:SetTarget(c747.retg2)
	e18:SetOperation(c747.reop2)
	c:RegisterEffect(e18)	
end

function c747.mat_filter2(c)
  return c:IsSetCard(0x316) and c:GetLevel()>=7
end

function c747.condition(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsAttackPos()
end

function c747.efilter(e,te)
	return te:GetOwnerPlayer()~=e:GetHandlerPlayer()
end

function c747.tfilter(e,c)
	return not c:IsType(TYPE_CONTINUOUS) and c:IsSetCard(0x316)
end

function c747.ddfilter(c)
	return not c:IsHasEffect(100000703) and c:IsFaceup() and c:IsCode(100000590)
end
function c747.ddcondition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c747.ddfilter,tp,LOCATION_SZONE,0,1,nil)
end

function c747.rvfilterset(c)
	return c:GetFlagEffect(186)~=0 and c:IsFacedown() 
end
function c747.rvtg(e,tp,ev,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c747.rvfilterset,tp,LOCATION_SZONE,0,1,nil) end
end
function c747.operation(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c747.rvfilterset,tp,LOCATION_SZONE,0,nil)
	if #g>0 then Duel.ConfirmCards(tp, g) end
	-- local e3=Effect.CreateEffect(e:GetHandler())
	-- e3:SetType(EFFECT_TYPE_FIELD)
	-- e3:SetCode(100000703)
	-- e3:SetRange(LOCATION_MZONE)
	-- e3:SetTargetRange(LOCATION_ONFIELD,0)
	-- e3:SetReset(RESET_PHASE+PHASE_END)
	-- e:GetHandler():RegisterEffect(e3)
end

function c747.filter1(c,e)
	return c==e:GetHandler()
end
function c747.descondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_DESTROY)
	return ex and tg~=nil and tc+tg:FilterCount(c747.filter1,nil,e)-tg:GetCount()>0 
end
function c747.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetFlagEffect(210)==0 end
	Duel.SetOperationInfo(0,CATEGORY_NEGATE,eg,1,0,0)
	if re:GetHandler():IsRelateToEffect(re) then
		  local g=Duel.GetMatchingGroup(c747.filter20,tp,LOCATION_MZONE,LOCATION_MZONE,e:GetHandler())
		Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
		  local tc=g:GetFirst()
		  local dam=0
		  while tc do
			 local atk=tc:GetAttack()
			 if atk<0 then atk=0 end
			 dam=dam+atk
			 tc=g:GetNext()
		  end
			Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,dam)
	end
	  e:GetHandler():RegisterFlagEffect(210,RESET_EVENT+0x1ff0000,0,1)
end
function c747.filter20(c)
	return aux.TRUE 
end
function c747.filter2(c,e)
	return aux.TRUE and not c:IsImmuneToEffect(e)
end
function c747.activate(e,tp,eg,ep,ev,re,r,rp)
	local tc=re:GetHandler()
	if Duel.NegateActivation(ev) then
	local g=Duel.GetMatchingGroup(c747.filter2,tp,LOCATION_MZONE,LOCATION_MZONE,e:GetHandler(),e)
	local tc=g:GetFirst()
	local dam=0
	while tc do
		local atk=tc:GetAttack()
		if atk<0 then atk=0 end
		dam=dam+atk
		tc=g:GetNext()
	end
	  if Duel.Destroy(g,REASON_EFFECT)>0 then
	Duel.Damage(1-tp,dam,REASON_EFFECT) end
	  end
	  e:GetHandler():ResetFlagEffect(210)
end
function c747.handcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_TOHAND)
	return ex and tg~=nil and tc+tg:FilterCount(c747.filter1,nil,e)-tg:GetCount()>0 
end
function c747.deckcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_TODECK)
	return ex and tg~=nil and tc+tg:FilterCount(c747.filter1,nil,e)-tg:GetCount()>0 
end
function c747.rmcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_REMOVE)
	return ex and tg~=nil and tc+tg:FilterCount(c747.filter1,nil,e)-tg:GetCount()>0 
end
function c747.ctcondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_CONTROL)
	return ex and tg~=nil and tc+tg:FilterCount(c747.filter1,nil,e)-tg:GetCount()>0 
end
function c747.ctcondition2(e,tp,eg,ep,ev,re,r,rp)
	local ph=Duel.GetCurrentPhase()
	if not e:GetHandler():IsRelateToBattle() then return false end
	local bc=e:GetHandler():GetBattleTarget()
	return bc and bc:IsFaceup() and (bc:IsCode(209) or bc:IsCode(113) 
		or (bc:IsCode(211) and bc:GetOverlayGroup():IsExists(Card.IsCode,1,nil,209)))
end
function c747.togravecondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_TOGRAVE)
	return ex and tg~=nil and tc+tg:FilterCount(c747.filter1,nil,e)-tg:GetCount()>0 
end
function c747.releasecondition(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.IsChainNegatable(ev) then return false end
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_RELEASE)
	return ex and tg~=nil and tc+tg:FilterCount(c747.filter1,nil,e)-tg:GetCount()>0 
end

function c747.tfilter2(e,c)
	return c:IsType(TYPE_FIELD) and c:IsSetCard(0x316)
end


function c747.refilter(c)
	return c:GetSequence()<5
end
function c747.retg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c747.refilter,tp,LOCATION_SZONE,0,1,nil) or Duel.IsExistingMatchingCard(c747.refilter,1-tp,LOCATION_SZONE,0,1,nil) end
end
function c747.getflag(g,tp)
	local flag = 0
	for c in aux.Next(g) do
		flag = flag|((1<<c:GetSequence())<<(8+(16*c:GetControler())))
	end
	if tp~=0 then
		flag=((flag<<16)&0xffff)|((flag>>16)&0xffff)
	end
	return ~flag
end
function c747.reop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(c747.refilter,tp,LOCATION_SZONE,0,nil)
	local g2=Duel.GetMatchingGroup(c747.refilter,1-tp,LOCATION_SZONE,0,nil)	
	if #g<1 and #g2<1 then return end
	local ag=g
	ag:Merge(g2)
	local try=1
	local filter=0 local filter2=0
	while #ag>0 do
		if try==0 and not Duel.SelectYesNo(tp, aux.Stringid(185,1)) then break end
		try=0
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SELECT)
		local p=ag:Select(tp,1,1,nil):GetFirst()
		local ttp=p:GetControler()
		--c747.getflag(ag,ttp)
		local afilter=0	
		ag:RemoveCard(p)	
		if ttp==tp then 
			g:RemoveCard(p)
			afilter=filter|(0x100<<p:GetSequence())|0xffffe0ff
		else 
			g2:RemoveCard(p) 
			afilter=filter2|(0x100<<p:GetSequence()<<16)|0xe0ffffff
		end				
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOZONE)
		local zone
		if ttp==tp then 
			zone=Duel.SelectFieldZone(tp,1,LOCATION_SZONE,0,afilter)
			--filter=filter|zone
		else 
			zone=Duel.SelectFieldZone(tp,1,0,LOCATION_SZONE,afilter)
			--filter2=filter2|zone 
			zone=zone>>16
		end
		local seq=math.log(zone>>8,2)
		local oc=Duel.GetFieldCard(ttp,LOCATION_SZONE,seq)
		if oc then
			Duel.SwapSequence(p,oc)
		else
			Duel.MoveSequence(p,seq)
		end	
	end
end

function c747.setfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_SPELL+TYPE_TRAP) and c:IsSSetable(true)
end
function c747.retg2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c747.setfilter,tp,LOCATION_SZONE,0,1,nil) or Duel.IsExistingMatchingCard(c747.setfilter,1-tp,LOCATION_SZONE,0,1,nil) end
end
function c747.reop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(c747.setfilter,tp,LOCATION_SZONE,LOCATION_SZONE,nil)
	if #g<1 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SET)
	local p=g:Select(tp,1,99,nil)
	for ap in aux.Next(p) do
	    Duel.ChangePosition(ap, POS_FACEDOWN)
		Duel.RaiseEvent(ap,EVENT_SSET,e,REASON_EFFECT,tp,tp,0)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
		e1:SetCode(EFFECT_TRAP_ACT_IN_SET_TURN)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		ap:RegisterEffect(e1)
	end	
end