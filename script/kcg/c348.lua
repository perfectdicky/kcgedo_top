--陰陽合祀 (K)
function c348.initial_effect(c)
	--Activate
	local e2=Effect.CreateEffect(c)
	e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	  e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_DESTROYED)
	e2:SetRange(LOCATION_SZONE)
	e2:SetTarget(c348.tg)
	e2:SetOperation(c348.op)
	c:RegisterEffect(e2)
end

function c348.spfilter(c,e,tp)
	return c:IsCanBeSpecialSummoned(e,0,tp,false,false) and c:GetLevel()==10 and c:IsType(TYPE_SYNCHRO) and c:IsRace(RACE_DRAGON)
end
function c348.tg(e,tp,eg,ep,ev,re,r,rp,chk)
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	local g=Duel.GetMatchingGroup(c348.spfilter,tp,LOCATION_EXTRA,0,nil,e,tp)
	if chk==0 then 
		local rc=eg:Filter(Card.IsCode,nil,1686814)
		return (not ect or ect>=2) and Duel.GetLocationCountFromEx(tp,tp,nil,SUMMON_TYPE_SYNCHRO)>1 and Duel.GetFieldGroupCount(tp,LOCATION_MZONE,0)==0
				and not Duel.IsPlayerAffectedByEffect(tp,59822133)   
				   and rc:GetCount()>0 and g:GetCount()>0 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,2,0,0)
end
function c348.op(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	if Duel.IsPlayerAffectedByEffect(tp,59822133) or (ect and ect<2) then return end 
	local ft=Duel.GetLocationCountFromEx(tp,tp,nil,SUMMON_TYPE_SYNCHRO)
	  if ft<=1 or Duel.GetFieldGroupCount(tp,LOCATION_MZONE,0)>0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectTarget(tp,c348.spfilter,tp,LOCATION_EXTRA,0,2,2,nil,e,tp)
	  local tc=g:GetFirst()
	  while tc do
	if tc:IsRelateToEffect(e) and Duel.SpecialSummonStep(tc,0,tp,tp,false,false,POS_FACEUP) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e1:SetRange(LOCATION_ONFIELD)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1,true)
			local e2=e1:Clone()
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		tc:RegisterEffect(e2,true)
			local e3=e1:Clone()
		e3:SetCode(EFFECT_SET_ATTACK)
			e3:SetValue(0)
		tc:RegisterEffect(e3)
		Duel.SpecialSummonComplete() end
			tc=g:GetNext()
	end
end
