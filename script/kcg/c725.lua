--Attack Guidance Armor
function c725.initial_effect(c)
	--change target
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_BE_BATTLE_TARGET)
	e1:SetTarget(c725.atktg)
	e1:SetOperation(c725.atkop)
	c:RegisterEffect(e1)
end
function c725.filter(c)
	return (not Duel.GetAttacker() or c~=Duel.GetAttacker()) and c~=Duel.GetAttackTarget()
end
function c725.atktg(e,tp,eg,ep,ev,re,r,rp,chk)
	local tg=Duel.GetAttacker()
	if chk==0 then return tg:IsOnField() and not tg:IsStatus(STATUS_ATTACK_CANCELED) end
end
function c725.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tg=Duel.GetAttacker()
	if not (tg:IsOnField() and not tg:IsStatus(STATUS_ATTACK_CANCELED)) then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	local g=Duel.SelectMatchingCard(tp,c725.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,eg:GetFirst())
	local tc=g:GetFirst()
	if tc then 
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetRange(LOCATION_SZONE)
		e1:SetCode(EFFECT_SELF_ATTACK)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetReset(RESET_PHASE+PHASE_DAMAGE)
		e1:SetTargetRange(1,1)
		Duel.RegisterEffect(e1,tp)
		Duel.ChangeAttackTarget(tc,true)			
	-- 	Duel.HintSelection(g)  
	-- 	Duel.ChangeAttackTarget(tc)
	-- 	if tc:GetControler()==Duel.GetAttacker():GetControler() then 
	--   local e4=Effect.CreateEffect(c)
	--   e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DELAY)
	--   e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	--   e4:SetCode(EVENT_BATTLE_CONFIRM)
	--   e4:SetOperation(function(...) 
	--   if tc==Duel.GetAttackTarget() then
	--   local e5=Effect.CreateEffect(c)
	--   e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DELAY+EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	--   e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	--   e5:SetCode(EVENT_BATTLED)
	--   e5:SetCountLimit(1)
	--   e5:SetOperation(function(...) Duel.ChangeBattleDamage(tp,0) Duel.ChangeBattleDamage(1-tp,0) e5:Reset() end)
	--   e5:SetReset(RESET_PHASE+PHASE_BATTLE)
	--   Duel.RegisterEffect(e5,tp) 
	--   Duel.ChangeBattleDamage(tp,0) Duel.ChangeBattleDamage(1-tp,0)
	--   Duel.CalculateDamage(Duel.GetAttacker(),tc,true) 
	--   end 
	--   e4:Reset() end)
	--   e4:SetCountLimit(1)
	--   e4:SetReset(RESET_PHASE+PHASE_BATTLE)
	--   Duel.RegisterEffect(e4,tp)	
	-- 	end
    end
end