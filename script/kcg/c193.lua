--ダークネス １
function c193.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	--e1:SetCondition(c193.condition)
	e1:SetTarget(c193.destg)
	e1:SetOperation(c193.desop)
	c:RegisterEffect(e1)
end

function c193.condition(e,tp,eg,ep,ev,re,r,rp)
    return e:GetHandler():GetFlagEffect(193)~=0
end
function c193.filter(c)
	return c:IsFaceup() and c:IsDestructable()
end
function c193.filter2(c)
	return c:IsFaceup() and c:IsCode(511310100) and not c:IsDisabled()
end
function c193.destg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	--if chkc then return chkc:GetControler()~=tp and chkc:IsOnField() and c193.filter(chkc) end
	if chk==0 then return true end
		--Duel.IsExistingTarget(c193.filter,tp,0,LOCATION_ONFIELD,1,nil) end
	if Duel.IsExistingTarget(c193.filter,tp,0,LOCATION_ONFIELD,1,nil) and e:GetHandler():GetFlagEffect(193)~=0 then
		e:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
		local g=Duel.SelectTarget(tp,c193.filter,tp,0,LOCATION_ONFIELD,1,1,nil)
		Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
	end	
end
function c193.darkness(c)
	return c:IsFaceup() and c:GetFlagEffect(193)~=0 and c:IsType(TYPE_TRAP+TYPE_CONTINUOUS) and c:IsSetCard(0x316)
end
function c193.desop(e,tp,eg,ep,ev,re,r,rp)
	if e:GetHandler():GetFlagEffect(193)==0 then return end	
	local tc=Duel.GetFirstTarget()
	if tc and tc:IsRelateToEffect(e) then
		Duel.Destroy(tc,REASON_EFFECT)
	end
	local g=Duel.GetMatchingGroup(c193.darkness,tp,LOCATION_SZONE,0,e:GetHandler())
	if g:GetCount()>0 then
		while g:GetCount()>0 do
		   local tc=g:GetFirst()
		   c193.zero(e:GetHandler(),e,tp)
		   g:RemoveCard(tc)
	    end
	end	
end

function c193.zero(tc,e,tep)
	local te=tc:GetActivateEffect()
	if te==nil or tc:CheckActivateEffect(true,false,false)==nil then return end
	local condition=te:GetCondition()
	local cost=te:GetCost()
	local target=te:GetTarget()
	local operation=te:GetOperation()
	e:SetProperty(te:GetProperty())
	Duel.ClearTargetCard()
	if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
	if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
	tc:CreateEffectRelation(te)
	local gg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	if gg then  
		local etc=gg:GetFirst()	
		while etc do
			etc:CreateEffectRelation(te)
			etc=gg:GetNext()
		end
	end						
	if operation then 
	 	local tc2=Duel.GetFirstTarget()
        if tc2 and tc2:IsRelateToEffect(te) then
		Duel.Destroy(tc2,REASON_EFFECT) end
	end
	tc:ReleaseEffectRelation(te)					
	if gg then  
		local etc=gg:GetFirst()												 
		while etc do
			etc:ReleaseEffectRelation(te)
			etc=gg:GetNext()
		end
	end 
end