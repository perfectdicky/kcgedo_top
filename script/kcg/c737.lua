--異界共鳴-同調融合 (K)
function c737.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_FUSION_SUMMON)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)	
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c737.target)
	e1:SetOperation(c737.tactivate)
	c:RegisterEffect(e1)
end

function c737.darkness(c)
	return c:IsFaceup() and c:IsType(TYPE_TRAP) and c:IsSetCard(0x316)
	and c:GetOverlayCount()>0
end
function c737.ofilter(c)
	return c:IsType(TYPE_SPELL) or c:IsType(TYPE_TRAP)
end
function c737.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return true end	
	if chk==0 then return true end
	if Duel.IsExistingTarget(c737.darkness,tp,LOCATION_ONFIELD,0,1,nil) and e:GetHandler():GetFlagEffect(737)~=0 then		
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
		Duel.SelectTarget(tp,c737.darkness,tp,LOCATION_ONFIELD,0,1,1,nil)	
		e:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)	
	end
end
function c737.tactivate(e,tp,eg,ep,ev,re,r,rp)
	if e:GetHandler():GetFlagEffect(737)==0 then return end

	-- local sqc=Duel.GetMatchingGroup(c737.darkness,tp,LOCATION_SZONE,0,e:GetHandler())
	-- for i=1,sqc:GetCount() do
	-- if e:GetHandler():GetFlagEffect(737)~=0 and Duel.IsExistingMatchingCard(c737.ofilter,tp,LOCATION_DECK+LOCATION_HAND,0,1,nil) and Duel.SelectYesNo(tp,aux.Stringid(12744567,0)) then
	-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
	-- 	local og=Duel.SelectMatchingCard(tp,c737.ofilter,tp,LOCATION_DECK+LOCATION_HAND,0,1,1,nil):GetFirst()
	-- 	if og and not og:IsImmuneToEffect(e) then
	-- 		Duel.Overlay(e:GetHandler(),og)
	--     end
	-- end
	-- end

	--c737.activate(e,tp,eg,ep,ev,re,r,rp)

	local sc=Duel.GetFirstTarget()
	if not sc or sc:IsFacedown() or not sc:IsRelateToEffect(e) or sc:IsControler(1-tp) or sc:IsImmuneToEffect(e) then return end
	local og2=sc:GetOverlayGroup():Filter(c737.ofilter,nil)
	if og2:GetCount()<1 then return end
	Duel.SetLP(0,10)
	for tc in aux.Next(og2) do
		-- for sqtc in aux.Next(sqc) do
		-- 	sqtc:RegisterFlagEffect(tc:GetOriginalCode(),RESET_EVENT+0x1fe0000+RESET_CHAIN+RESET_PHASE+PHASE_END,0,1)
	    -- end	
		c737.zero(tc,e,tp)
	end	
end	

-- function c737.activate(e,tp,eg,ep,ev,re,r,rp)
-- 	local c=e:GetHandler()
-- 	local sg=Duel.GetMatchingGroup(c737.exxfilter,tp,LOCATION_EXTRA,0,nil,e,tp)
-- 	if sg:GetCount()>0 then	
-- 		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
-- 		local tg=sg:Select(tp,1,1,nil)
-- 		local fc=tg:GetFirst()
-- 		Duel.SpecialSummon(fc,0,tp,tp,false,false,POS_FACEUP)	
-- 		local de=Effect.CreateEffect(c)
-- 		de:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
-- 		de:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
-- 		de:SetRange(LOCATION_MZONE)
-- 		de:SetCode(EVENT_PHASE+PHASE_END)
-- 		de:SetCountLimit(1)
-- 		de:SetOperation(c737.rmop)
-- 		de:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
-- 		fc:RegisterEffect(de,true)	
-- 	end
-- end
function c737.rmop(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
	Duel.SendtoDeck(c, nil, 0, REASON_EFFECT)
end

function c737.zero(tc,e,tep)
	local te=tc:GetActivateEffect()
	if te==nil or tc:CheckActivateEffect(true,false,false)==nil then return end
	local condition=te:GetCondition()
	local cost=te:GetCost()
	local target=te:GetTarget()
	local operation=te:GetOperation()
	e:SetProperty(te:GetProperty())
	Duel.ClearTargetCard()
	if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
	if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
	tc:CreateEffectRelation(te)
	local gg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	if gg then  
		local etc=gg:GetFirst()	
		while etc do
			etc:CreateEffectRelation(te)
			etc=gg:GetNext()
		end
	end						
	if operation then operation(te,tep,eg,ep,ev,re,r,rp) end
	tc:ReleaseEffectRelation(te)					
	if gg then  
		local etc=gg:GetFirst()												 
		while etc do
			etc:ReleaseEffectRelation(te)
			etc=gg:GetNext()
		end
	end 
end
