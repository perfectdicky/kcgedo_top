--ガガガマジシャン
function c374.initial_effect(c)
	c:SetUniqueOnField(1,0,374)

	--lv change
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(26082117,0))
	e1:SetCategory(CATEGORY_LVCHANGE)	
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetCountLimit(1)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTarget(c374.tg)
	e1:SetOperation(c374.op)
	c:RegisterEffect(e1)
end

function c374.filter(c)
	return c:IsFaceup() and not c:IsType(TYPE_XYZ) and c:GetLevel()<7 and c:IsSetCard(0x909)
end
function c374.tg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(tp) and c374.filter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(c374.filter,tp,LOCATION_MZONE,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,aux.Stringid(26082117,1))
	Duel.SelectTarget(tp,c374.filter,tp,LOCATION_MZONE,0,1,1,nil)
end
function c374.op(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
      local tc=Duel.GetFirstTarget()
      local lv=tc:GetLevel()
	if tc:IsRelateToEffect(e) and tc:IsFaceup() and c:IsFaceup() and c:IsRelateToEffect(e) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_LEVEL)
		e1:SetValue(lv*2)
		e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e1)
	      --unsynchroable
	      local e2=Effect.CreateEffect(c)
	      e2:SetType(EFFECT_TYPE_SINGLE)
	      e2:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	      e2:SetValue(1)
		e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	      tc:RegisterEffect(e2)
	end
end
