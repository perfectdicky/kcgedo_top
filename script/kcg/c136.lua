--神泥史莱姆（AC）
function c136.initial_effect(c)
	c:EnableReviveLimit()
	-- Fusion.AddProcCodeFunRep(c,26905245,function(tc) return tc:IsType(TYPE_MONSTER+TYPE_EFFECT) and tc:IsLocation(LOCATION_ONFIELD) and not tc:IsCode(900000082) end,1,4,false,false)
	Fusion.AddProcMix(c,true,true,aux.FilterBoolFunctionEx(Card.IsAttribute,ATTRIBUTE_DIVINE),aux.FilterBoolFunctionEx(Card.IsRace,RACE_AQUA))	

	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetRange(LOCATION_EXTRA)
	e1:SetCondition(c136.hspcon)
	e1:SetTarget(c136.hsptg)
	e1:SetOperation(c136.hspop)
	c:RegisterEffect(e1)	

	--破坏后守备表示特殊召唤
	local e4=Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetCode(EFFECT_MATERIAL_CHECK)
	e4:SetValue(c136.valcheck)
	c:RegisterEffect(e4)
	local e5=Effect.CreateEffect(c)
	--e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	e5:SetLabelObject(e4)
	e5:SetOperation(c136.atkop)
	c:RegisterEffect(e5)

	--上级召唤时可以当作两只祭品
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_TRIPLE_TRIBUTE)
	e2:SetValue(1)
	c:RegisterEffect(e2)
end

function c136.hspfilter(c,tp,sc)
	return c:IsAttribute(ATTRIBUTE_DIVINE) and c:GetLevel()==10 and c:GetAttack()==0 and Duel.GetLocationCountFromEx(tp,tp,c,sc)>0
end
function c136.hspcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	return Duel.CheckReleaseGroup(tp,c136.hspfilter,1,false,1,true,c,tp,nil,nil,nil,tp,c)
end
function c136.hsptg(e,tp,eg,ep,ev,re,r,rp,chk,c)
	local g=Duel.SelectReleaseGroup(tp,c136.hspfilter,1,1,false,true,true,c,tp,nil,false,nil,tp,c)
	if g then
		g:KeepAlive()
		e:SetLabelObject(g)
	return true
	end
	return false
end
function c136.hspop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=e:GetLabelObject()
	if not g then return end
	Duel.Release(g,REASON_COST+REASON_MATERIAL)
	g:DeleteGroup()
end

function c136.valcheck(e,c)
	local g=c:GetMaterial()
	local tg=g:Filter(Card.IsAttribute,nil,ATTRIBUTE_DIVINE)
	local tc=tg:GetFirst()
	Original_code=tc:GetCode()
	Original_att=tc:GetAttribute()
	Original_race=tc:GetRace()
	Original_lev=tc:GetLevel()
	Original_att=tc:GetAttack()
	Original_def=tc:GetDefense()
	Original_typ=tc:GetType()|TYPE_FUSION|TYPE_EFFECT
	--g:RemoveCard(tc)
	e:SetLabelObject(g)
end
function c136.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=e:GetLabelObject():GetLabelObject()
	if not g or g:GetCount()<1 or not c:IsSummonType(SUMMON_TYPE_FUSION) then return end
	--c:CopyEffect(900000082,RESET_EVENT+EVENT_TO_DECK)
	for tc in aux.Next(g) do
	c:CopyEffect(tc:GetOriginalCode(),RESET_EVENT+EVENT_TO_DECK) end
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SET_BASE_DEFENSE)
	e0:SetReset(RESET_EVENT+EVENT_TO_DECK)
	e0:SetValue(Original_def)
	c:RegisterEffect(e0,true)   
	local e1=Effect.CreateEffect(c)
	e1=e0:Clone()
	e1:SetCode(EFFECT_CHANGE_CODE)
	e1:SetValue(Original_code)
	c:RegisterEffect(e1,true) 
	local e2=Effect.CreateEffect(c)
	e2=e0:Clone()
	e2:SetCode(EFFECT_CHANGE_TYPE)
	e2:SetValue(Original_typ)
	c:RegisterEffect(e2,true) 
	local e3=Effect.CreateEffect(c)
	e3=e0:Clone()
	e3:SetCode(EFFECT_CHANGE_RACE)
	e3:SetValue(Original_race)
	c:RegisterEffect(e3,true) 
	local e4=Effect.CreateEffect(c)
	e4=e0:Clone()
	e4:SetCode(EFFECT_CHANGE_ATTRIBUTE)
	e4:SetValue(Original_att)
	c:RegisterEffect(e4,true) 
	local e5=Effect.CreateEffect(c)
	e5=e0:Clone()
	e5:SetCode(EFFECT_CHANGE_LEVEL)
	e5:SetValue(Original_lev)
	c:RegisterEffect(e5,true)   
	local e6=Effect.CreateEffect(c)
	e6=e0:Clone()
	e6:SetCode(EFFECT_SET_BASE_ATTACK)
	e6:SetValue(Original_att)
	c:RegisterEffect(e6,true)   
end

function c136.condition(e,c)
	return c:IsType(TYPE_MONSTER)
end
