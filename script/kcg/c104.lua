--罪世界2 (K)
function c104.initial_effect(c)

	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_ADD_CODE)
	e0:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e0:SetRange(LOCATION_FZONE)
	e0:SetValue(27564031)
	c:RegisterEffect(e0)

	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c104.atcon)
	  --e1:SetOperation(c104.actop) 
	c:RegisterEffect(e1)

	--search
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(27564031,0))
	e2:SetCategory(CATEGORY_TOHAND+CATEGORY_SEARCH)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetRange(LOCATION_FZONE)
	e2:SetCondition(c104.condition)
	e2:SetTarget(c104.target)
	e2:SetOperation(c104.operation)
	c:RegisterEffect(e2)

	--
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CANNOT_REMOVE)
	e3:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e3:SetRange(LOCATION_FZONE)
	  e3:SetTargetRange(LOCATION_MZONE,0)
	e3:SetValue(1)
	  e3:SetTarget(c104.rmtarget)
	c:RegisterEffect(e3)
	  local e4=e3:Clone()
	e4:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e4)
	  local e6=e3:Clone()
	e6:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e6)
	  local e7=e3:Clone()
	e7:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	c:RegisterEffect(e7)

	--攻击提升
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetRange(LOCATION_FZONE)
	e5:SetCode(EFFECT_UPDATE_ATTACK)
	e5:SetTargetRange(LOCATION_MZONE,0)
	  e5:SetTarget(c104.rmtarget)
	e5:SetValue(1000)
	c:RegisterEffect(e5)
	
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e8:SetRange(LOCATION_FZONE)
	e8:SetCode(EFFECT_IMMUNE_EFFECT)
	e8:SetValue(c104.immu)
	c:RegisterEffect(e8)	
end
c104.listed_names={27564031}

function c104.immu(e,te)
   return te:GetOwner()~=e:GetOwner()
end

function c104.atcon(e)
	local tc=Duel.GetFieldCard(e:GetHandler():GetControler(),LOCATION_SZONE,5)
	  local tc2=Duel.GetFieldCard(1-e:GetHandler():GetControler(),LOCATION_SZONE,5)
	return (tc~=nil and tc:IsFaceup() and tc:IsCode(27564031))
			  or (tc2~=nil and tc2:IsFaceup() and tc2:IsCode(27564031))
end
function c104.actop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetHandler()
	local oppfield=Duel.GetFieldCard(1-tp,LOCATION_SZONE,5)
	if (oppfield~=nil and not (oppfield:IsCode(10) or oppfield:IsCode(11) or oppfield:IsCode(12) or oppfield:IsCode(12201) or oppfield:IsCode(190))) 
	or (oppfield~=nil and (oppfield:IsCode(10) or oppfield:IsCode(11) or oppfield:IsCode(12) or oppfield:IsCode(12201) or oppfield:IsCode(190)) and oppfield:IsFacedown()) then
	Duel.Destroy(oppfield,REASON_RULE) end
end

function c104.condition(e,tp,eg,ep,ev,re,r,rp)
	return tp==Duel.GetTurnPlayer() and Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)>0
		and Duel.GetDrawCount(tp)>0
end
function c104.filter(c)
	return c:IsSetCard(0x23) and c:IsAbleToHand()
end
function c104.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c104.filter,tp,LOCATION_DECK,0,1,nil) end
	local dt=Duel.GetDrawCount(tp)
	if dt~=0 then
		_replace_count=0
		_replace_max=dt
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1,0)
		e1:SetReset(RESET_PHASE+PHASE_DRAW)
		e1:SetValue(0)
		Duel.RegisterEffect(e1,tp)
	end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,0,LOCATION_DECK)
end
function c104.operation(e,tp,eg,ep,ev,re,r,rp)
	_replace_count=_replace_count+1
	if _replace_count>_replace_max or not e:GetHandler():IsRelateToEffect(e) then return end
	local g=Duel.GetMatchingGroup(Card.IsSetCard,tp,LOCATION_DECK,0,nil,0x23)
	  --local g2=Duel.GetMatchingGroup(Card.IsRace,tp,LOCATION_DECK,0,nil,RACE_DRAGON)
	if g:GetCount()>=1 then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
		local sg=g:RandomSelect(tp,1)
			local sg1=sg:GetFirst()
		--local sgg=g2:RandomSelect(tp,1)
			--local sgg1=sgg:GetFirst()
			if sg1:IsAbleToHand() then Duel.SendtoHand(sg1,nil,REASON_EFFECT) else Duel.SendtoGrave(sg1,REASON_EFFECT) end
			--if sgg1:IsAbleToHand() then Duel.SendtoHand(sgg1,nil,REASON_EFFECT) else Duel.SendtoGrave(sgg1,REASON_EFFECT) end
			--sg:Merge(sgg)
		Duel.ConfirmCards(1-tp,sg)
		Duel.ShuffleDeck(tp)
	end
end

function c104.rmtarget(e,c)
	  return c:IsSetCard(0x23)
end
