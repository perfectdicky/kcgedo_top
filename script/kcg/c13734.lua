--Double-Rank-Up-Magic Hope Force
function c13734.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(13717,7))
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetTarget(c13734.target)
	e1:SetOperation(c13734.operation)
	c:RegisterEffect(e1)
end
function c13734.filter1(c,e,tp)
	local rk=c:GetRank()
	return c:IsFaceup() and c:IsCode(84013237) and c:IsType(TYPE_XYZ) and c:GetOverlayCount()==2
	and Duel.IsExistingMatchingCard(c13734.filter2,tp,LOCATION_EXTRA,0,1,nil,rk,e,tp,c)
end
function c13734.filter2(c,rk,e,tp,tc)
	return (c:GetRank()==(rk+1) or c:GetRank()==(rk+2)) 
	  and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false) 
	  and Duel.GetLocationCountFromEx(tp,tp,tc,c)>1
end
function c13734.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsControler(tp) and chkc:IsLocation(LOCATION_MZONE) and c13734.filter1(chkc,e,tp) end
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	if chk==0 then return (not ect or ect>=2)
		and not Duel.IsPlayerAffectedByEffect(tp,59822133)
		and Duel.IsExistingTarget(c13734.filter1,tp,LOCATION_MZONE,0,1,nil,e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	local g=Duel.SelectTarget(tp,c13734.filter1,tp,LOCATION_MZONE,0,1,1,nil,e,tp)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c13734.operation(e,tp,eg,ep,ev,re,r,rp)
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	if Duel.IsPlayerAffectedByEffect(tp,59822133) or (ect and ect<2)  then return end	  
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if tc:IsFacedown() or not tc:IsRelateToEffect(e) or tc:GetOverlayCount()~=2 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c13734.filter2,tp,LOCATION_EXTRA,0,1,1,nil,tc:GetRank(),e,tp,tc)
	local sc=g:GetFirst()
	if sc then
	Duel.SpecialSummonStep(sc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
		local g1=tc:GetOverlayGroup()
		Duel.Hint(HINT_SELECTMSG,tp,aux.Stringid(47660516,0))
		local mg2=g1:Select(tp,1,1,nil)
		local oc=mg2:GetFirst()
		sc:SetMaterial(mg2)
		Duel.Overlay(sc,mg2)
		Duel.RaiseSingleEvent(tc,EVENT_DETACH_MATERIAL,e,0,0,0,0)
	end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g2=Duel.SelectMatchingCard(tp,c13734.filter2,tp,LOCATION_EXTRA,0,1,1,nil,tc:GetRank(),e,tp,tc)
	local sc2=g2:GetFirst()
	if sc2 then
		Duel.SpecialSummonStep(sc2,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
		local g1=tc:GetOverlayGroup()
		Duel.Hint(HINT_SELECTMSG,tp,aux.Stringid(47660516,0))
		local mg2=g1:Select(tp,1,1,nil)
		local oc=mg2:GetFirst()
		sc2:SetMaterial(mg2)
		Duel.Overlay(sc2,mg2)
		Duel.RaiseSingleEvent(tc,EVENT_DETACH_MATERIAL,e,0,0,0,0)
	end
   Duel.SpecialSummonComplete()
   if sc then sc:CompleteProcedure() end
   if sc2 then sc2:CompleteProcedure() end
end