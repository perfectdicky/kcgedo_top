--Over-Hundred Chaos Universe
function c13726.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	--e1:SetCondition(c13726.condition)
	e1:SetHintTiming(0,TIMING_END_PHASE)
	e1:SetTarget(c13726.target)
	e1:SetOperation(c13726.operation)
	c:RegisterEffect(e1)
end
function c13726.cfilter(c,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x1048)
end
function c13726.condition(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(c13726.cfilter,1,nil,tp)
end
function c13726.spfilter(c,e,tp)
	return c:IsSetCard(0x1048) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function c13726.spfilter2(c,e,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x1048) 
	  and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
	  and c:GetTurnID()==Duel.GetTurnCount() and not c:IsHasEffect(EFFECT_NECRO_VALLEY) and c:IsReason(REASON_DESTROY) 
	  and Duel.IsExistingMatchingCard(c13726.opfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp)
end
function c13726.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then
		local g=Duel.GetMatchingGroup(c13726.spfilter2,tp,LOCATION_GRAVE,0,nil,e,tp)
		return (not ect or ect>=2) 
	   and g:GetCount()>0 
	   and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 
	   and Duel.GetLocationCountFromEx(1-tp,tp,nil,TYPE_XYZ)>0
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,0,0,0)
end
function c13726.opfilter(c,e,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x1048)
	  and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false,POS_FACEUP,1-tp)
end
function c13726.operation(e,tp,eg,ep,ev,re,r,rp)
	local ct=Duel.GetLocationCount(tp,LOCATION_MZONE)
	local ct2=Duel.GetLocationCountFromEx(1-tp,tp,nil,TYPE_XYZ)
	if ct<1 then return end
	if Duel.IsPlayerAffectedByEffect(tp,59822133) then ct=1 end 
	local ect=c29724053 and Duel.IsPlayerAffectedByEffect(tp,29724053) and c29724053[tp]
	if (ect and ect<ct) then ct=ect end 
	if ct>ct2 then ct=ct2 end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c13726.spfilter2,tp,LOCATION_GRAVE,0,1,ct,nil,e,tp)
	local count=0
	local tc2=g:GetFirst()
	while tc2 do
	if tc2~=nil and Duel.SpecialSummonStep(tc2,0,tp,tp,false,false,POS_FACEUP)~=0 then
	count=count+1
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_DISABLE)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	tc2:RegisterEffect(e1,true)
	local e2=e1:Clone()
	e2:SetCode(EFFECT_DISABLE_EFFECT)
	tc2:RegisterEffect(e2,true) end
	tc2=g:GetNext() end
	Duel.SpecialSummonComplete()
	tc2=g:GetFirst()
	while tc2 do
	tc2:CompleteProcedure() 
	tc2=g:GetNext() end
	Duel.AdjustInstantly()
	Duel.BreakEffect()

	if count~=0 and Duel.IsExistingMatchingCard(c13726.opfilter,tp,LOCATION_EXTRA,0,1,nil,e,tp) and ct2>0 then
	if count>ct2 then count=ct2 end
	local ect2=c29724053 and Duel.IsPlayerAffectedByEffect(1-tp,29724053) and c29724053[1-tp]
	if Duel.IsPlayerAffectedByEffect(1-tp,59822133) then count=1 end 
	if ect and count>ect then count=ect end   
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g2=Duel.SelectMatchingCard(tp,c13726.opfilter,tp,LOCATION_EXTRA,0,count,count,nil,e,tp)
	local tc=g2:GetFirst()
	while tc do
	if tc and Duel.SpecialSummonStep(tc,SUMMON_TYPE_XYZ,tp,1-tp,false,false,POS_FACEUP_DEFENSE)~=0 then
		local e3=Effect.CreateEffect(e:GetHandler())
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetCode(EFFECT_DISABLE)
		e3:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
		e3:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e3,true)
		local e4=e3:Clone()
		e4:SetCode(EFFECT_DISABLE_EFFECT)
		tc:RegisterEffect(e4,true) end
		tc=g2:GetNext() end 
	Duel.SpecialSummonComplete()
	tc=g2:GetFirst()
	while tc do
	tc:CompleteProcedure() 
	tc=g2:GetNext() end
   end
end
