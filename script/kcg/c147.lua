--究極封印神エクゾディオス
function c147.initial_effect(c)
	c:EnableReviveLimit()
	--cannot special summon
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	c:RegisterEffect(e1)
	--special summon
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SPSUMMON_PROC)
	e2:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e2:SetRange(LOCATION_HAND)
	e2:SetCondition(c147.spcon)
	e2:SetOperation(c147.spop)
	c:RegisterEffect(e2)
	--to grave
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(13893596,0))
	e3:SetCategory(CATEGORY_TOGRAVE)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e3:SetCode(EVENT_ATTACK_ANNOUNCE)
	e3:SetTarget(c147.tgtg)
	e3:SetOperation(c147.tgop)
	c:RegisterEffect(e3)
	--atkup
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCode(EFFECT_UPDATE_ATTACK)
	e4:SetValue(c147.atkval)
	c:RegisterEffect(e4)
	--cannot destroyed
    local e0=Effect.CreateEffect(c)
    e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e0:SetValue(1)
	c:RegisterEffect(e0)
	--immune
	local e121=Effect.CreateEffect(c)
	e121:SetType(EFFECT_TYPE_SINGLE)
	e121:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e121:SetRange(LOCATION_MZONE)
	e121:SetCode(EFFECT_IMMUNE_EFFECT)
	e121:SetValue(c147.efilter)
	c:RegisterEffect(e121)
	--negate
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_ATTACK_ANNOUNCE)
	e5:SetOperation(c147.negop1)
	c:RegisterEffect(e5)
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e6:SetCode(EVENT_BE_BATTLE_TARGET)
	e6:SetOperation(c147.negop2)
	c:RegisterEffect(e6)
	if not c147.global_check then
		c147.global_check=true
		--register
		local ge1=Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_TO_GRAVE)
		ge1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
		ge1:SetOperation(c147.choperation)
		Duel.RegisterEffect(ge1,0)
	end	
end
c147.a=0 c147.b=0 c147.c=0 c147.d=0 c147.e=0

function c147.cfilter(c)
	return not c:IsAbleToDeckOrExtraAsCost()
end
function c147.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	local g=Duel.GetMatchingGroup(Card.IsType,tp,LOCATION_GRAVE,0,nil,TYPE_MONSTER)
	return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and g:GetCount()>0
		and not g:IsExists(c147.cfilter,1,nil)
end
function c147.spop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=Duel.GetMatchingGroup(Card.IsType,tp,LOCATION_GRAVE,0,nil,TYPE_MONSTER)
	Duel.SendtoDeck(g,nil,2,REASON_COST)
end

function c147.tgtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_TOGRAVE,nil,1,tp,LOCATION_DECK)
end
function c147.tgfilter(c)
	return c:IsSetCard(0x40) and c:IsAbleToGrave() and c:IsType(TYPE_MONSTER)
end
function c147.tgop(e,tp,eg,ep,ev,re,r,rp)
	local WIN_REASON_EXODIUS = 0x14
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local c=e:GetHandler()
	local g=Duel.SelectMatchingCard(tp,c147.tgfilter,tp,LOCATION_HAND+LOCATION_DECK,0,1,1,nil)
	local tc=g:GetFirst()
	if tc then Duel.SendtoGrave(tc,REASON_EFFECT) end
	-- if tc and Duel.SendtoGrave(tc,REASON_EFFECT+147)~=0 and tc:IsLocation(LOCATION_GRAVE)
	-- 	and c:IsRelateToEffect(e) and c:IsFaceup() then
	-- 	tc:CreateRelation(c,RESET_EVENT+0x1fe0000)
    --         if tc:IsCode(8124921) then c147.a=1 end
    --         if tc:IsCode(44519536) then c147.b=1 end
    --         if tc:IsCode(70903634) then c147.c=1 end
    --         if tc:IsCode(7902349) then c147.d=1 end
    --         if tc:IsCode(33396948) then c147.e=1 end end
	-- 	local g2=Duel.GetFieldGroup(tp,LOCATION_GRAVE,0)
	-- 	if not g2:IsExists(Card.IsCode,1,nil,8124921) then c147.a=0
	-- 	elseif not g2:IsExists(Card.IsCode,1,nil,44519536) then c147.b=0
	-- 	elseif not g2:IsExists(Card.IsCode,1,nil,70903634) then c147.c=0
	-- 	elseif not g2:IsExists(Card.IsCode,1,nil,7902349) then c147.d=0
	-- 	elseif not g2:IsExists(Card.IsCode,1,nil,33396948) then c147.e=0 end
	-- 	if c147.a==1 and c147.b==1 and c147.c==1 and c147.d==1 and c147.e==1 then
	-- 		Duel.Win(tp,WIN_REASON_EXODIUS)
	-- 	end
end

function c147.choperation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()  
	if not re or not (re:GetHandler():IsCode(13893596) or re:GetHandler():IsCode(511000244)) then return end
	for tc in aux.Next(eg) do
		if tc:IsCode(8124921) then c147.a=1
		elseif tc:IsCode(44519536) then c147.b=1
		elseif tc:IsCode(70903634) then c147.c=1
		elseif tc:IsCode(7902349) then c147.d=1
		elseif tc:IsCode(33396948) then c147.e=1
		else end
	end
	if c147.a==1 and c147.b==1 and c147.c==1 and c147.d==1 and c147.e==1 then
		Duel.Win(tp,WIN_REASON_EXODIUS)
	end
end

function c147.exfilter(c)
	return c:IsSetCard(0x40) and c:IsType(TYPE_MONSTER)
end
function c147.atkval(e,c)
	return Duel.GetMatchingGroupCount(c147.exfilter,c:GetControler(),LOCATION_GRAVE,0,nil)*1000
end

function c147.efilter(e,te)
	return te:IsActiveType(TYPE_QUICKPLAY+TYPE_COUNTER+TYPE_SPELL+TYPE_TRAP+TYPE_EFFECT)
              and te:GetHandler():GetControler()~=e:GetHandler():GetControler()
end

function c147.negop1(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local d=Duel.GetAttackTarget()
	if d~=nil then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_BATTLE)
		d:RegisterEffect(e1)
		local e2=Effect.CreateEffect(e:GetHandler())
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_BATTLE)
		d:RegisterEffect(e2)
	end
end
function c147.negop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local a=Duel.GetAttacker()
	if a~=nil then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_BATTLE)
		a:RegisterEffect(e1)
		local e2=Effect.CreateEffect(e:GetHandler())
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_DISABLE_EFFECT)
		e2:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_BATTLE)
		a:RegisterEffect(e2)
	end
end
