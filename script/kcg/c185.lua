--ダークネス·ネオスフィア
function c185.initial_effect(c)
	c:EnableUnsummonable()

	--special summon
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(60417395,0))
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_ATTACK_ANNOUNCE)
	e2:SetRange(LOCATION_HAND)
	e2:SetCondition(c185.spcon)
	e2:SetCost(c185.spcost)
	e2:SetTarget(c185.sptg)
	e2:SetOperation(c185.spop)
	c:RegisterEffect(e2)

	--battle indestructable
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e3:SetValue(1)
	c:RegisterEffect(e3)

	-- local e41=Effect.CreateEffect(c)
	-- e41:SetDescription(aux.Stringid(100000590,1))
	-- e41:SetType(EFFECT_TYPE_IGNITION)
	-- e41:SetCountLimit(1)
	-- e41:SetRange(LOCATION_MZONE)
	-- e41:SetTarget(c185.thtg2)
	-- e41:SetOperation(c185.thop2)
	--c:RegisterEffect(e41)  
   
	--lp4000
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e5:SetCountLimit(1)
	e5:SetCode(EVENT_PHASE+PHASE_END)
	e5:SetRange(LOCATION_MZONE)
	e5:SetOperation(c185.lpoperation)
	c:RegisterEffect(e5)

	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(185,0))
	e4:SetType(EFFECT_TYPE_QUICK_O)
	e4:SetCode(EVENT_FREE_CHAIN)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCountLimit(1)
	e4:SetTarget(c185.rvtg)	
	e4:SetOperation(c185.operation)
	c:RegisterEffect(e4)

	local e7=Effect.CreateEffect(c)
	e7:SetDescription(aux.Stringid(185,1))
	e7:SetType(EFFECT_TYPE_QUICK_O)
	e7:SetCode(EVENT_FREE_CHAIN)
	e7:SetRange(LOCATION_MZONE)
	e7:SetCountLimit(1)
	e7:SetTarget(c185.retg)
	e7:SetOperation(c185.reop)
	c:RegisterEffect(e7)
	
	local e8=Effect.CreateEffect(c)
	e8:SetDescription(aux.Stringid(185,2))
	e8:SetType(EFFECT_TYPE_QUICK_O)
	e8:SetCode(EVENT_FREE_CHAIN)
	e8:SetRange(LOCATION_MZONE)
	e8:SetCountLimit(1)
	e8:SetTarget(c185.retg2)
	e8:SetOperation(c185.reop2)
	c:RegisterEffect(e8)	
end
function c185.spcon(e,tp,eg,ep,ev,re,r,rp)
	return ep~=tp
end
function c185.cfilter1(c)
	return c:IsFaceup() and c:IsRace(RACE_FIEND) and c:IsAbleToGraveAsCost()
end
function c185.cfilter2(c)
	return c:IsRace(RACE_FIEND) and c:IsAbleToGraveAsCost()
end
function c185.spcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c185.cfilter1,tp,LOCATION_MZONE,0,1,nil)
		and Duel.IsExistingMatchingCard(c185.cfilter2,tp,LOCATION_HAND,0,1,e:GetHandler()) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local g1=Duel.SelectMatchingCard(tp,c185.cfilter1,tp,LOCATION_MZONE,0,1,1,nil)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local g2=Duel.SelectMatchingCard(tp,c185.cfilter2,tp,LOCATION_HAND,0,1,1,e:GetHandler())
	g1:Merge(g2)
	Duel.SendtoGrave(g1,REASON_COST)
end
function c185.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>-1
		and e:GetHandler():IsCanBeSpecialSummoned(e,0,tp,true,false) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
end
function c185.spop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	if e:GetHandler():IsRelateToEffect(e) and Duel.SpecialSummon(e:GetHandler(),0,tp,tp,true,false,POS_FACEUP)~=0 then
		e:GetHandler():CompleteProcedure()
	end
end
function c185.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_TRAP) and c:IsAbleToHand()
end
function c185.thtg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chk==0 then return Duel.IsExistingTarget(c185.filter,tp,LOCATION_ONFIELD,0,1,nil) end
	local g=Duel.GetMatchingGroup(c185.filter,tp,LOCATION_ONFIELD,0,nil)
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,g,g:GetCount(),0,0)
end
function c185.thop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c185.filter,tp,LOCATION_ONFIELD,0,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,nil,REASON_EFFECT)
	end
end

function c185.lpoperation(e,tp,eg,ep,ev,re,r,rp)
	local p=e:GetHandler():GetControler()
	if Duel.GetLP(tp)<4000 then Duel.SetLP(tp,4000,REASON_EFFECT) end
end

function c185.filterset(c)
	return (c:IsType(TYPE_TRAP) or c:IsType(TYPE_SPELL) ) and c:IsFaceup()
end
function c185.thtg2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c185.filterset,tp,LOCATION_SZONE,0,1,nil) end
	Duel.RegisterFlagEffect(tp,60417395,RESET_PHASE+PHASE_END,0,1)
end
function c185.setfilterset(c)
	return c:GetFlagEffect(60417395)~=0 --and c:IsFacedown() 
end
function c185.thop2(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c185.filterset,tp,LOCATION_SZONE,LOCATION_SZONE,nil)
	Duel.ChangePosition(g,POS_FACEDOWN)
	g=Duel.GetMatchingGroup(Card.IsFacedown,tp,LOCATION_SZONE,LOCATION_SZONE,nil)
	  local a=0
	local tc=g:GetFirst()
	e:GetHandler():RegisterFlagEffect(60417395,RESET_PHASE+PHASE_END,0,1)
	while tc do 
		Duel.SendtoHand(tc,nil,REASON_RULE)
		--Duel.Remove(tc,POS_FACEDOWN,REASON_RULE)
		tc:RegisterFlagEffect(60417395,RESET_EVENT+0x1fe0000,0,1)
			if tc:IsControler(tp) then a=1 end
			if tc:IsControler(1-tp) then a=2 end
		tc=g:GetNext()
	end
	  if a==1 then Duel.ShuffleHand(tp) end
	  if a==2 then Duel.ShuffleHand(1-tp) end
	local sgg=Duel.GetMatchingGroup(c185.setfilterset,tp,LOCATION_HAND,LOCATION_HAND,nil)
	tc=sgg:GetFirst()
	while tc do 
		Duel.DisableShuffleCheck()
		Duel.SendtoDeck(tc,nil,1,REASON_RULE)
		--Duel.Remove(tc,POS_FACEDOWN,REASON_RULE)
		tc:RegisterFlagEffect(60417395,RESET_EVENT+0x1fe0000,0,1)
		tc=sgg:GetNext()
	end
	--local sg=Duel.GetMatchingGroup(c100000590.setfilterset,tp,LOCATION_REMOVED,0,nil)
	local sg=Duel.GetMatchingGroup(c185.setfilterset,tp,LOCATION_DECK,LOCATION_DECK,nil)
	local tcc=nil
	while sg:GetCount()>0 do 
		tcc=sg:RandomSelect(tp,1):GetFirst()
		sg:RemoveCard(tcc)
		Duel.DisableShuffleCheck()
			if tcc:IsControler(tp) then
				Duel.MoveToField(tcc,tp,tp,LOCATION_SZONE,POS_FACEDOWN,false) 
			else Duel.MoveToField(tcc,tp,1-tp,LOCATION_SZONE,POS_FACEDOWN,false)  end
	end
end

function c185.rvfilterset(c)
	return c:GetFlagEffect(186)~=0 and c:IsFacedown() 
end
function c185.rvtg(e,tp,ev,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c185.rvfilterset,tp,LOCATION_SZONE,0,1,nil) end
end
function c185.operation(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c185.rvfilterset,tp,LOCATION_SZONE,0,nil)
	if #g>0 then Duel.ConfirmCards(tp, g) end
end

function c185.refilter(c)
	return c:GetSequence()<5
end
function c185.retg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c185.refilter,tp,LOCATION_SZONE,0,1,nil) or Duel.IsExistingMatchingCard(c185.refilter,1-tp,LOCATION_SZONE,0,1,nil) end
end
function c185.getflag(g,tp)
	local flag = 0
	for c in aux.Next(g) do
		flag = flag|((1<<c:GetSequence())<<(8+(16*c:GetControler())))
	end
	if tp~=0 then
		flag=((flag<<16)&0xffff)|((flag>>16)&0xffff)
	end
	return ~flag
end
function c185.reop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(c185.refilter,tp,LOCATION_SZONE,0,nil)
	local g2=Duel.GetMatchingGroup(c185.refilter,1-tp,LOCATION_SZONE,0,nil)	
	if #g<1 and #g2<1 then return end
	local ag=g
	ag:Merge(g2)
	local try=1
	local filter=0 local filter2=0
	while #ag>0 do
		if try==0 and not Duel.SelectYesNo(tp, aux.Stringid(185,1)) then break end
		try=0
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SELECT)
		local p=ag:Select(tp,1,1,nil):GetFirst()
		local ttp=p:GetControler()
		--c185.getflag(ag,ttp)
		local afilter=0	
		ag:RemoveCard(p)	
		if ttp==tp then 
			g:RemoveCard(p)
			afilter=filter|(0x100<<p:GetSequence())|0xffffe0ff
		else 
			g2:RemoveCard(p) 
			afilter=filter2|(0x100<<p:GetSequence()<<16)|0xe0ffffff
		end				
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOZONE)
		local zone
		if ttp==tp then 
			zone=Duel.SelectFieldZone(tp,1,LOCATION_SZONE,0,afilter)
			--filter=filter|zone
		else 
			zone=Duel.SelectFieldZone(tp,1,0,LOCATION_SZONE,afilter)
			--filter2=filter2|zone 
			zone=zone>>16
		end
		local seq=math.log(zone>>8,2)
		local oc=Duel.GetFieldCard(ttp,LOCATION_SZONE,seq)
		if oc then
			Duel.SwapSequence(p,oc)
		else
			Duel.MoveSequence(p,seq)
		end	
	end
end

function c185.setfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_SPELL+TYPE_TRAP) and c:IsSSetable(true)
end
function c185.retg2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c185.setfilter,tp,LOCATION_SZONE,0,1,nil) or Duel.IsExistingMatchingCard(c185.setfilter,1-tp,LOCATION_SZONE,0,1,nil) end
end
function c185.reop2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(c185.setfilter,tp,LOCATION_SZONE,LOCATION_SZONE,nil)
	if #g<1 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SET)
	local p=g:Select(tp,1,99,nil)
	for ap in aux.Next(p) do
	    Duel.ChangePosition(ap, POS_FACEDOWN)
		Duel.RaiseEvent(ap,EVENT_SSET,e,REASON_EFFECT,tp,tp,0)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
		e1:SetCode(EFFECT_TRAP_ACT_IN_SET_TURN)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		ap:RegisterEffect(e1)
	end		
end