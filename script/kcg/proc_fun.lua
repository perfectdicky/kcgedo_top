HINT_MUSIC = 21
HINT_ANIME = 22
HINT_BGM   = 23

EFFECT_GOD_IMMUNE                = 500
EFFECT_ULTIMATE_IMMUNE           = 501
EFFECT_OVERINFINITE_ATTACK       = 502
EFFECT_OVERINFINITE_DEFENSE      = 503
EFFECT_EQUIP_MONSTER             = 504
EFFECT_ORICA                     = 505
EFFECT_ORICA_SZONE               = 506
EFFECT_SANCT                     = 507
EFFECT_SANCT_MZONE               = 508
EFFECT_LINK_RANK                 = 509
EFFECT_LINK_RANK_S               = 510
EVENT_OVERLAY                    = 511
EFFECT_RANK_LINK                 = 512
EFFECT_RANK_LINK_S               = 513
EFFECT_LEVEL_LINK                = 514
EFFECT_LEVEL_LINK_S              = 515
EFFECT_LINK_LEVEL                = 516
EFFECT_LINK_LEVEL_S              = 517
EFFECT_LEVEL_RANK_LINK           = 518
EFFECT_LEVEL_RANK_LINK_S         = 519
EVENT_PREEFFECT_DRAW             = 520

LOCATION_RMZONE = 0x400
LOCATION_RSZONE = 0x800

-- Duel.LoadScript("proc_AI.lua")
Duel.LoadScript("wixoss.lua")

local playchk = 0
aux.AISystem = 0

aux.AIchk0 = 0
aux.AIchk1 = 0
Auxiliary.list = {
    [1] = 13701,
    [2] = 13702,
    [3] = 13703,
    [4] = 13704,
    [5] = 90126061,
    [6] = 511002090,
    [7] = 511010007,
    [8] = 511001374,
    [9] = 511010009,
    [10] = 11411223,
    [11] = 80117527,
    [12] = 511010012,
    [13] = 511015132,
    [14] = 511000184,
    [15] = 511001999,
    [16] = 513000018,
    [17] = 511002077,
    [18] = 23649496,
    [19] = 511001779,
    [20] = 47805931,
    [21] = 511010021,
    [22] = 73445448,
    [23] = 511000183,
    [24] = 93713837,
    [25] = 511010025,
    [26] = 39622156,
    [27] = 8387138,
    [28] = 511000512,
    [29] = 54191698,
    [30] = 511010030,
    [31] = 511015133,
    [32] = 511010032,
    [33] = 511002091,
    [34] = 32003339,
    [35] = 511010035,
    [36] = 50260683,
    [37] = 511001273,
    [38] = 511001275,
    [39] = 60,
    [40] = 511002830,
    [41] = 90590303,
    [42] = 511002054,
    [43] = 511001776,
    [44] = 511001997,
    [45] = 29208536,
    [46] = 511001998,
    [47] = 31320433,
    [48] = 511015118,
    [49] = 16259549,
    [50] = 511002060,
    [51] = 56292140,
    [52] = 511002058,
    [53] = 511010053,
    [54] = 511002727,
    [55] = 46871387,
    [56] = 511010056,
    [57] = 53244294,
    [58] = 511002873,
    [59] = 82697249,
    [60] = 66011101,
    [61] = 511002088,
    [62] = 150,
    [63] = 89642993,
    [64] = 511010064,
    [65] = 511010065,
    [66] = 76067258,
    [67] = 35772782,
    [68] = 23085002,
    [69] = 237,
    [70] = 80796456,
    [71] = 59479050,
    [72] = 511015129,
    [73] = 511002092,
    [74] = 16037007,
    [75] = 71166481,
    [76] = 92015800,
    [77] = 357,
    [78] = 29085954,
    [79] = 71921856,
    [80] = 513000062,
    [81] = 49032236,
    [82] = 511002064,
    [83] = 511010083,
    [84] = 26556950,
    [85] = 42421606,
    [86] = 63504681,
    [87] = 89516305,
    [88] = 151,
    [89] = 95474755,
    [90] = 8165596,
    [91] = 511002059,
    [92] = 240,
    [93] = 389,
    [94] = 62070231,
    [95] = 511000515,
    [96] = 55727845,
    [97] = 28400508,
    [98] = 55470553,
    [99] = 210,
    [100] = 13714
}

-- Auxiliary.list2={[1]=13701,[2]=13702,[3]=13703,[4]=13704,[5]=90126061,[6]=204,[7]=455,
-- [8]=236,[9]=231,[10]=11411223,[11]=80117527,[12]=368,[13]=458,[14]=511000184,
-- [15]=234,[16]=546,[17]=460,[18]=23649496,[19]=461,[20]=47805931,[21]=100000580,
-- [22]=462,[23]=511000183,[24]=93713837,[25]=463,[26]=39622156,[27]=8387138,[28]=511000512,[29]=54191698,[30]=465,
-- [31]=459,[32]=232,[33]=230,[34]=32003339,[35]=511000516,[36]=50260683,
-- [37]=359,[38]=360,[39]=60,[40]=235,[41]=90590303,[42]=466,[43]=273,[44]=11370137,[45]=29208536,[46]=153,
-- [47]=31320433,[48]=467,[49]=16259549,[50]=468,[51]=56292140,[52]=469,[53]=242,[54]=11370139,
-- [55]=46871387,[56]=55935416,[57]=53244294,[58]=470,[59]=82697249,[60]=66011101,[61]=471,[62]=150,[63]=89642993,
-- [64]=472,[65]=473,[66]=76067258,[67]=35772782,[68]=23085002,[69]=237,[70]=511000517,[71]=59479050,[72]=475,
-- [73]=247,[74]=16037007,[75]=71166481,[76]=92015800,[77]=357,[78]=29085954,[79]=71921856,[80]=476,[81]=49032236,[82]=478,
-- [83]=479,[84]=511000514,[85]=42421606,[86]=63504681,[87]=89516305,[88]=151,[89]=95474755,[90]=8165596,
-- [91]=480,[92]=240,[93]=389,[94]=62070231,[95]=192,[96]=55727845,[97]=28400508,[98]=55470553,[99]=210,[100]=13714}

Auxiliary.cxlist = {
    [77631175] = 13030280,
    [51960178] = 23454876,
    [14152862] = 41147577,
    [15914410] = 511600284,
    [3814632] = 49202331,
    [40424929] = 76419637,
    [30741334] = 88754763,
    [787] = 13718,
    [31563350] = 511002000
}
Auxiliary.notoclist = {
    [1] = 79747096,
    [2] = 583,
    [3] = 584,
    [4] = 585,
    [5] = 69757518,
    [6] = 6387204,
    [8] = 16398003,
    [9] = 32559361,
    [11] = 16398005,
    [15] = 33776843,
    [39] = 66970002,
    [40] = 69170557,
    [43] = 32446630,
    [46] = 16398047,
    [47] = 16398077,
    [48] = 16398079,
    [50] = 16398053,
    [53] = 23998625,
    [54] = 16398066,
    [56] = 16398033,
    [57] = 16398048,
    [60] = 16398044,
    [61] = 16398039,
    [62] = 31801517,
    [64] = 16398068,
    [65] = 49195710,
    [69] = 11522979,
    [72] = 16398059,
    [73] = 96864105,
    [74] = 16398060,   
    [80] = 20563387,
    [88] = 6165656,
    [92] = 47017574,
    [93] = 16398076,
    [95] = 16398086,
    [96] = 77205367,
    [98] = 16398073,
    [100] = 57314798,
    [101] = 12744567,
    [102] = 67173574,
    [103] = 20785975,
    [104] = 49456901,
    [105] = 85121942,
    [106] = 55888045,
    [107] = 68396121,
    [1000] = 13715
}
Auxiliary.cnolist = {
    [1] = 13705,
    [2] = 583,
    [3] = 584,
    [4] = 585,   
    [5] = 13700060,
    [6] = 511001781,
    [8] = 16398003,
    [9] = 511001659,
    [11] = 16398005,
    [15] = 246,
    [39] = 62,
    [40] = 251,
    [43] = 511001777,
    [46] = 16398047,
    [47] = 16398077,
    [48] = 16398079,
    [50] = 16398053,
    [53] = 492,
    [54] = 16398066,
    [56] = 16398033,
    [57] = 16398048,
    [60] = 16398044,
    [61] = 16398039,   
    [62] = 491,
    [64] = 16398068,
    [65] = 511010165,
    [69] = 244,
    [72] = 16398059,
    [73] = 511010173,   
    [74] = 16398060,   
    [80] = 477,
    [88] = 170000217,
    [92] = 250,
    [93] = 16398076,
    [95] = 16398086,
    [96] = 77205367,
    [98] = 16398073,
    [100] = 494,
    [101] = 96,
    [102] = 255,
    [103] = 260,
    [104] = 257,
    [105] = 253,
    [106] = 249,
    [107] = 98,
    [1000] = 13715
}

function Auxiliary.BeginPuzzle(effect)
end

function Auxiliary.SwapEntity(g1, g2)
    if type(g1)~=type(g1) then return end
    if type(g1)=="Group" then
        if #g1~=#g2 then return end
        for tc in aux.Next(g1) do
            local tac=g2:GetFirst()
            g2:RemoveCard(tac)
            local code1=tc:GetOriginalCode()
            local code2=tac:GetOriginalCode()
            tc:SetEntityCode(code2, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
            tac:SetEntityCode(code1, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
        end
    else
        local code1=g1:GetOriginalCode()
        local code2=g2:GetOriginalCode()
        g1:SetEntityCode(code2, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
        g2:SetEntityCode(code1, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
    end
end

function Auxiliary.GetValueType(v)
    local t = type(v)
    if t == "userdata" then
        local mt = getmetatable(v)
        if mt == Group then
            return "Group"
        elseif mt == Effect then
            return "Effect"
        else
            return "Card"
        end
    else
        return t
    end
end

function aux.addcode(c, code)
    if not c:IsCode(code) then
        local e1 = Effect.CreateEffect(c)
        e1:SetCode(EFFECT_ADD_CODE)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_UNCOPYABLE)
        e1:SetValue(code)
        c:RegisterEffect(e1, true)
    end
end
function aux.addsetcard(c, setcard)
    if not c:IsSetCard(setcard) then
        local e1 = Effect.CreateEffect(c)
        e1:SetCode(EFFECT_ADD_SETCODE)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_UNCOPYABLE)
        e1:SetValue(setcard)
        c:RegisterEffect(e1, true)
    end
end

-- function Auxiliary.Stringid(code,id)

-- 	return (id&0xfffff)|code<<20
-- end


function aux.chkfilter(c, tp, code)
    return c:IsCode(code) and c:GetOwner() == tp
end
function aux.overfilter(c, tp, code)
    return (c:GetOverlayGroup():FilterCount(aux.chkfilter, nil, tp, code) > 0) or
               (c:IsCode(code) and c:GetOwner() == tp)
end

local fieldg = Duel.GetFieldGroup
function Duel.GetFieldGroup(tp, s, o)
    if tp == nil then
        tp = 0
    end
    local g = fieldg(tp, s, o)
    local sp = g:Filter(function(c) return c:GetFlagEffect(157)~=0 end, nil)
    g:Sub(sp)
    return g
end
local fieldgcount = Duel.GetFieldGroupCount
function Duel.GetFieldGroupCount(tp, s, o)
    if tp == nil then
        tp = 0
    end
    local ccount = 0
    local acount = 0
    acount = Duel.GetFieldGroup(tp, s, o):GetCount()

    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        if bit.band(s, LOCATION_MZONE) ~= 0 then
            ccount = ccount + Duel.GetFieldGroup(tp, LOCATION_MZONE, 0):GetCount()
        end
        if bit.band(s, LOCATION_SZONE) ~= 0 then
            ccount = ccount + Duel.GetFieldGroup(tp, LOCATION_SZONE, 0):GetCount()
        end
        if bit.band(s, LOCATION_GRAVE) ~= 0 then
            ccount = ccount + fieldgcount(tp, LOCATION_GRAVE, 0)
        end
        if bit.band(s, LOCATION_HAND) ~= 0 then
            ccount = ccount + fieldgcount(tp, LOCATION_HAND, 0)
        end
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        if bit.band(o, LOCATION_MZONE) ~= 0 then
            ccount = ccount + Duel.GetFieldGroup(1 - tp, LOCATION_MZONE, 0):GetCount()
        end
        if bit.band(o, LOCATION_SZONE) ~= 0 then
            ccount = ccount + Duel.GetFieldGroup(1 - tp, LOCATION_SZONE, 0):GetCount()
        end
        if bit.band(o, LOCATION_GRAVE) ~= 0 then
            ccount = ccount + fieldgcount(tp, 0, LOCATION_GRAVE)
        end
        if bit.band(o, LOCATION_HAND) ~= 0 then
            ccount = ccount + fieldgcount(tp, 0, LOCATION_HAND)
        end
    end

    return acount - ccount
end

function bit.exclude(a, b)
    if bit.band(a, b) ~= 0 then
        return math.abs(a - b)
    else
        return a
    end
end

local exist = Duel.IsExistingMatchingCard
function Duel.IsExistingMatchingCard(f, tp, s, o, count, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end
    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end
    return exist(nf, tp, s, o, count, ex, ...)
end
local selectc = Duel.SelectMatchingCard
function Duel.SelectMatchingCard(sel_player, f, tp, s, o, mint, maxt, cancel, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local params={ex,...}
    if cancel==nil or type(cancel)=="Card" or type(cancel)=="Group" then
        ex=cancel
        cancel=false
    else params={...}
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end
    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end
    return selectc(sel_player, nf, tp, s, o, mint, maxt, cancel, ex, table.unpack(params))
end
local existt = Duel.IsExistingTarget
function Duel.IsExistingTarget(f, tp, s, o, count, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end
    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end
    return existt(nf, tp, s, o, count, ex, ...)
end
local selectt = Duel.SelectTarget
function Duel.SelectTarget(sel_player, f, tp, s, o, mint, maxt, cancel, ex, ...)
    if tp == nil then
        tp = 0
    end
    if f == nil then
        f = aux.TRUE
    end
    local params={ex,...}
    if cancel==nil or type(cancel)=="Card" or type(cancel)=="Group" then
        ex=cancel
        cancel=false
    else params={...}
    end
    local nf = function(c, ...) return f(c, ...) and c:GetFlagEffect(157)==0 end

    if Duel.GetFlagEffect(tp, 782) ~= 0 then
        s = bit.exclude(s, LOCATION_MZONE)
        s = bit.exclude(s, LOCATION_SZONE)
        s = bit.exclude(s, LOCATION_GRAVE)
        s = bit.exclude(s, LOCATION_HAND)
    end
    if Duel.GetFlagEffect(1 - tp, 782) ~= 0 then
        o = bit.exclude(o, LOCATION_MZONE)
        o = bit.exclude(o, LOCATION_SZONE)
        o = bit.exclude(o, LOCATION_GRAVE)
        o = bit.exclude(o, LOCATION_HAND)
    end
    return selectt(sel_player, nf, tp, s, o, mint, maxt, cancel, ex, table.unpack(params))
end

local yesno = Duel.SelectYesNo
function Duel.SelectYesNo(tp, desc)
    if desc==aux.Stringid(4014, 6) then return true end
    return yesno(tp, desc)
end

local overlay = Duel.Overlay
function Duel.Overlay(c, g, reason)
    local re = Duel.GetChainInfo(0, CHAININFO_TRIGGERING_EFFECT)
    if reason==nil then
        if re then reason=REASON_EFFECT
        else reason=REASON_RULE end
    end
    return overlay(c, g, reason)
end

function Auxiliary.GetMustMaterialGroup(tp, code)
    local g = Group.CreateGroup()
    local ce = {Duel.IsPlayerAffectedByEffect(tp, code)}
    for _, te in ipairs(ce) do
        local tc = te:GetHandler()
        if tc then
            g:AddCard(tc)
        end
    end
    return g
end
function Auxiliary.MustMaterialCheck(v, tp, code)
    local g = Auxiliary.GetMustMaterialGroup(tp, code)
    if not v then
        if code == EFFECT_MUST_BE_XMATERIAL and Duel.IsPlayerAffectedByEffect(tp, 67120578) then
            return false
        end
        return #g == 0
    end
    local t = Auxiliary.GetValueType(v)
    for tc in Auxiliary.Next(g) do
        if (t == "Card" and v ~= tc) or (t == "Group" and not v:IsContains(tc)) then
            return false
        end
    end
    return true
end

-- for additional registers
local regeff = Card.RegisterEffect
function Card.RegisterEffect(c, e, forced, ...)
	if c:IsStatus(STATUS_INITIALIZING) and not e then
		error("Parameter 2 expected to be Effect, got nil instead.",2)
	end
	--1 == 511002571 - access to effects that activate that detach an Xyz Material as cost
	--2 == 511001692 - access to Cardian Summoning conditions/effects
	--4 ==  12081875 - access to Thunder Dragon effects that activate by discarding
	--8 == 511310036 - access to Allure Queen effects that activate by sending themselves to GY
	local reg_e = regeff(c,e,forced)
	if not reg_e then
		return nil
	end
	local reg={...}
	local resetflag,resetcount=e:GetReset()
	for _,val in ipairs(reg) do
		local prop=EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_SET_AVAILABLE
		if e:IsHasProperty(EFFECT_FLAG_UNCOPYABLE) then prop=prop|EFFECT_FLAG_UNCOPYABLE end
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetProperty(prop,EFFECT_FLAG2_MAJESTIC_MUST_COPY)
		if val==1 then
			e2:SetCode(511002571)
		elseif val==2 then
			e2:SetCode(511001692)
		elseif val==4 then
			e2:SetCode(12081875)
		elseif val==8 then
			e2:SetCode(511310036)
		end
		e2:SetLabelObject(e)
		e2:SetLabel(c:GetOriginalCode())
		if resetflag and resetcount then
			e2:SetReset(resetflag,resetcount)
		elseif resetflag then
			e2:SetReset(resetflag)
		end
		c:RegisterEffect(e2)
	end
    ---------kdiy-----------
    if reg[1] == 10 then
        return reg_e
    end

    if c:IsType(TYPE_TRAP) and e:GetOperation() ~= nil then
        local code = c:GetOriginalCode()
        local mt = _G["c" .. code]
        if mt.trap == nil then
            mt.trap = {e}
        else
            local chk = 0
            for _, te in ipairs(mt.trap) do
                if te:GetCondition() == e:GetCondition() and te:GetTarget() == e:GetTarget() and te:GetOperation() ==
                    e:GetOperation() and te:GetProperty() == e:GetProperty() then
                    chk = 1
                end
            end
            if chk == 0 then
                table.insert(mt.trap, e)
            end
        end
    end

    if e:GetRange()~=nil and bit.band(e:GetRange(),LOCATION_MZONE)~=0 and bit.band(e:GetRange(),LOCATION_SZONE)==0 then
        local e2=e:Clone()
        e2:SetRange(LOCATION_SZONE)
        local cond=e:GetCondition()
        if cond then e2:SetCondition(function(...) return cond(...) and c:IsHasEffect(EFFECT_ORICA_SZONE) end)
        else e2:SetCondition(function(...) return c:IsHasEffect(EFFECT_ORICA_SZONE) end) end
        c:RegisterEffect(e2,true,10)
    elseif e:GetRange()~=nil and bit.band(e:GetRange(),LOCATION_SZONE)~=0 and bit.band(e:GetRange(),LOCATION_MZONE)==0 then
        local e2=e:Clone()
        e2:SetRange(LOCATION_MZONE)
        local cond=e:GetCondition()
        if cond then e2:SetCondition(function(...) return cond(...) and c:IsHasEffect(EFFECT_SANCT_MZONE) end)
        else e2:SetCondition(function(...) return c:IsHasEffect(EFFECT_SANCT_MZONE) end) end
        c:RegisterEffect(e2,true,10)
    end
      
    return reg_e
end

function Xyz.AddProcedureX(c, f, lv, ct, alterf, desc, maxct, op, mustbemat, exchk)
    -- exchk for special xyz, checking other materials
    -- mustbemat for Startime Magician
    if not maxct then
        maxct = ct
    end
    if c.xyz_filter == nil then
        local mt = c:GetMetatable()
        mt.xyz_filter = function(mc, ignoretoken, xyz, tp)
            return
                mc and (not f or f(mc, xyz, SUMMON_TYPE_XYZ | MATERIAL_XYZ, tp)) and (not lv or mc:IsXyzLevel(c, lv)) and
                    (not mc:IsType(TYPE_TOKEN) or ignoretoken)
        end
        mt.xyz_parameters = {mt.xyz_filter, lv, ct, alterf, desc, maxct, op, mustbemat, exchk}
        mt.minxyzct = ct
        mt.maxxyzct = maxct
    end

    local chk1 = Effect.CreateEffect(c)
    chk1:SetType(EFFECT_TYPE_SINGLE)
    chk1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_SET_AVAILABLE)
    chk1:SetCode(946)
    chk1:SetCondition(Xyz.Condition(f, lv, ct, maxct, mustbemat, exchk))
    chk1:SetTarget(Xyz.Target(f, lv, ct, maxct, mustbemat, exchk))
    chk1:SetOperation(Xyz.OperationX(f, lv, ct, maxct, mustbemat, exchk))
    c:RegisterEffect(chk1)
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetDescription(1173)
    e1:SetCode(EFFECT_SPSUMMON_PROC)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e1:SetRange(LOCATION_EXTRA)
    e1:SetCondition(Xyz.Condition(f, lv, ct, maxct, mustbemat, exchk))
    e1:SetTarget(Xyz.Target(f, lv, ct, maxct, mustbemat, exchk))
    e1:SetOperation(Xyz.OperationX(f, lv, ct, maxct, mustbemat, exchk))
    e1:SetValue(SUMMON_TYPE_XYZ)
    e1:SetLabelObject(chk1)
    c:RegisterEffect(e1)
    if alterf then
        local chk2 = chk1:Clone()
        chk2:SetDescription(desc)
        chk2:SetCondition(Xyz.Condition2(alterf, op))
        chk2:SetTarget(Xyz.Target2(alterf, op))
        chk2:SetOperation(Xyz.Operation2(alterf, op))
        c:RegisterEffect(chk2)
        local e2 = e1:Clone()
        e2:SetDescription(desc)
        e2:SetCondition(Xyz.Condition2(alterf, op))
        e2:SetTarget(Xyz.Target2(alterf, op))
        e2:SetOperation(Xyz.Operation2(alterf, op))
        c:RegisterEffect(e2)
    end
    if not xyztemp then
        xyztemp = true
        xyztempg0 = Group.CreateGroup()
        xyztempg0:KeepAlive()
        xyztempg1 = Group.CreateGroup()
        xyztempg1:KeepAlive()
        local e3 = Effect.CreateEffect(c)
        e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        e3:SetCode(EVENT_STARTUP)
        e3:SetOperation(Xyz.MatGenerate)
        Duel.RegisterEffect(e3, 0)
    end
end
function Xyz.OperationX(f, lv, minc, maxc, mustbemat, exchk)
    return function(e, tp, eg, ep, ev, re, r, rp, c, must, og, min, max)
        local g = e:GetLabelObject()
        if not g then
            return
        end
        local remg = g:Filter(Card.IsHasEffect, nil, 511002116)
        remg:ForEach(function(c)
            c:RegisterFlagEffect(511002115, RESET_EVENT + RESETS_STANDARD, 0, 0)
        end)
        g:Remove(Card.IsHasEffect, nil, 511002116)
        g:Remove(Card.IsHasEffect, nil, 511002115)
        local sg = Group.CreateGroup()
        for tc in aux.Next(g) do
            local sg1 = tc:GetOverlayGroup()
            sg:Merge(sg1)
        end
        --------kdiy------
        -- Duel.SendtoGrave(sg,REASON_RULE)
        Duel.Overlay(c, sg)
        --------kdiy------           
        c:SetMaterial(g)
        Duel.Overlay(c, g:Filter(function(c)
            return c:GetEquipTarget()
        end, nil))
        Duel.Overlay(c, g)
        g:DeleteGroup()
    end
end

function Synchro.DarkCheck2(sg, card1, card2, plv, nlv, sc, tp, f1, f2, ...)
    if sg:IsExists(Synchro.CheckHand, 1, nil, sg) then
        return false
    end
    --[[local c=sg:GetFirst()
	while c do
		if c:IsHasEffect(EFFECT_HAND_SYNCHRO+EFFECT_SYNCHRO_CHECK) then
			local teg={c:GetCardEffect(EFFECT_HAND_SYNCHRO+EFFECT_SYNCHRO_CHECK)}
			local hanchk=false
			for i=1,#teg do
				local te=teg[i]
				local tgchk=te:GetTarget()
				local res=tgchk(te,c,sg,Group.CreateGroup(),Group.CreateGroup(),Group.CreateGroup(),Group.CreateGroup())
				--if not res then return false end
				if res then
					hanchk=true
					break
				end
			end
			if not hanchk then return false end
		end
		c=sg:GetNext()
	end]]
    local reqm = {...}
    if (f1 and not f1(card1, sc, SUMMON_TYPE_SYNCHRO | MATERIAL_SYNCHRO, tp)) or
        (f2 and not f2(card2, sc, SUMMON_TYPE_SYNCHRO | MATERIAL_SYNCHRO, tp)) or
        not card2:IsType(TYPE_TUNER, sc, SUMMON_TYPE_SYNCHRO | MATERIAL_SYNCHRO, tp) or not card2:IsSetCard(0x600) then
        return false
    end
    local lvchk = false
    for _, reqmat in ipairs(reqm) do
        if not reqmat(sg, sc, tp) then
            return false
        end
    end
    if sg:IsExists(Card.IsHasEffect, 1, nil, EFFECT_SYNCHRO_MATERIAL_CUSTOM) then
        local g = sg:Filter(Card.IsHasEffect, nil, EFFECT_SYNCHRO_MATERIAL_CUSTOM)
        for tc in aux.Next(g) do
            local teg = {tc:GetCardEffect(EFFECT_SYNCHRO_MATERIAL_CUSTOM)}
            for _, te in ipairs(teg) do
                local op = te:GetOperation()
                local ok, tlvchk = op(te, Group.CreateGroup(), Group.CreateGroup(), sg, plv, sc, tp, nlv, card1, card2)
                if not ok then
                    return false
                end
                lvchk = lvchk or tlvchk
            end
        end
    end
    if Synchro.CheckAdditional and not Synchro.CheckAdditional(tp, sg, sc) then
        return false
    end
    if sc:IsLocation(LOCATION_EXTRA) then
        if Duel.GetLocationCountFromEx(tp, tp, sg, sc) <= 0 then
            return false
        end
    else
        if Duel.GetMZoneCount(tp, sg, tp) <= 0 then
            return false
        end
    end
    if lvchk then
        return true
    end
    local ntlv = card1:GetSynchroLevel(sc)
    local ntlv1 = ntlv & 0xffff
    local ntlv2 = ntlv >> 16
    local tlv = card2:GetSynchroLevel(sc)
    local tlv1 = tlv & 0xffff
    local tlv2 = tlv >> 16
    if card1:GetFlagEffect(100000147) > 0 then
        ----------kdiy---------
        -- if tlv1==nlv-ntlv1 then return true end
        -- if (tlv2>0 or card2:IsStatus(STATUS_NO_LEVEL)) and (ntlv2>0 or card1:IsStatus(STATUS_NO_LEVEL)) then
        -- 	return tlv2==nlv-ntlv1 or tlv1==nlv-ntlv2 or tlv2==nlv-ntlv2
        -- elseif tlv2>0 or card2:IsStatus(STATUS_NO_LEVEL) then
        -- 	return tlv2==nlv-ntlv1
        -- elseif ntlv2>0 or card1:IsStatus(STATUS_NO_LEVEL) then
        -- 	return tlv1==nlv-ntlv2
        -- end
        if tlv == plv - ntlv then
            return true
        end
        ----------kdiy---------	
        return false
    else
        ----------kdiy---------	
        -- if tlv1==plv+ntlv1 then return true end
        -- if (tlv2>0 or card2:IsStatus(STATUS_NO_LEVEL)) and (ntlv2>0 or card1:IsStatus(STATUS_NO_LEVEL)) then
        -- 	return tlv2==plv+ntlv1 or tlv1==plv+ntlv2 or tlv2==plv+ntlv2
        -- elseif tlv2>0 or card2:IsStatus(STATUS_NO_LEVEL) then
        -- 	return tlv2==nlv-ntlv1
        -- elseif ntlv2>0 or card1:IsStatus(STATUS_NO_LEVEL) then
        -- 	return tlv1==nlv-ntlv2
        -- end
        if tlv == plv + ntlv then
            return true
        end
        ----------kdiy---------	
        return false
    end
end