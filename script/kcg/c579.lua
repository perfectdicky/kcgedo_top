-- 欧贝利斯克之巨神兵
local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()

	-- cannot special summon
	local e0 = Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SPSUMMON_CONDITION)
	c:RegisterEffect(e0)

	-- 特殊召唤方式
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(s.spcon)
	e1:SetOperation(s.spop)
	c:RegisterEffect(e1)

	-- 解放2只怪破坏对方场上怪兽
	local e4 = Effect.CreateEffect(c)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e4:SetDescription(aux.Stringid(10000000, 1))
	e4:SetCategory(CATEGORY_DESTROY)
	e4:SetType(EFFECT_TYPE_IGNITION)
	e4:SetRange(LOCATION_ONFIELD)
	e4:SetCountLimit(1)
	e4:SetCost(s.descost)
	e4:SetTarget(s.destg)
	e4:SetOperation(s.desop)
	c:RegisterEffect(e4)

	-- 不受陷阱效果以及魔法效果怪兽生效一回合
	local e82 = Effect.CreateEffect(c)
	e82:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e82:SetCode(EVENT_CHAINING)
	e82:SetRange(LOCATION_ONFIELD)
	e82:SetCondition(s.sdcon2)
	e82:SetOperation(s.sdop2)
	-- c:RegisterEffect(e82)
	local e80 = Effect.CreateEffect(c)
	e80:SetType(EFFECT_TYPE_SINGLE)
	e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
	e80:SetRange(LOCATION_ONFIELD)
	e80:SetCode(EFFECT_IMMUNE_EFFECT)
	e80:SetValue(s.efilterr)
	-- c:RegisterEffect(e80)  

	-- 解放2只怪提升攻击为无限
	local e6 = Effect.CreateEffect(c)
	e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e6:SetDescription(aux.Stringid(10000010, 0))
	e6:SetCategory(CATEGORY_ATKCHANGE)
	e6:SetType(EFFECT_TYPE_QUICK_O)
	e6:SetCode(EVENT_FREE_CHAIN)
	e6:SetHintTiming(TIMING_BATTLE_PHASE)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCountLimit(1)
	e6:SetCondition(s.atkcon)
	e6:SetCost(s.atkcost)
	e6:SetOperation(s.atkop)
	-- c:RegisterEffect(e6)

	-- 特召后成为攻击目标
	local e17 = Effect.CreateEffect(c)
	e17:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
	e17:SetRange(LOCATION_MZONE)
	e17:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e17:SetCode(EVENT_ATTACK_ANNOUNCE)
	e17:SetCondition(s.atcon2)
	e17:SetOperation(s.atop)
	c:RegisterEffect(e17)
	local e7 = Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_FIELD)
	e7:SetRange(LOCATION_ONFIELD)
	e7:SetTargetRange(LOCATION_MZONE, 0)
	e7:SetProperty(EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_CANNOT_DISABLE)
	e7:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
	e7:SetCondition(s.atcon)
	e7:SetTarget(s.atlimit)
	e7:SetValue(Auxiliary.imval1)
	-- c:RegisterEffect(e7)
	-- destroy replace
	local e72 = Effect.CreateEffect(c)
	e72:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e72:SetCode(EFFECT_DESTROY_REPLACE)
	e72:SetRange(LOCATION_ONFIELD)
	e72:SetCondition(s.atcon)
	e72:SetTarget(s.reptg)
	e72:SetValue(s.repval)
	e72:SetOperation(s.repop)
	c:RegisterEffect(e72)

	-- 通常召唤、特殊召唤、反转召唤不会被无效化
	local e8 = Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
	e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e8)
	local e9 = e8:Clone()
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	c:RegisterEffect(e9)
	local e10 = e9:Clone()
	e10:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
	c:RegisterEffect(e10)

	-- 不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变成里侧表示、作为特殊召唤素材
	local e100 = Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_ONFIELD)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101 = e100:Clone()
	e101:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e101)
	local e102 = e101:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103 = e102:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104 = e103:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e105 = e104:Clone()
	e105:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e105)
	local e106 = e105:Clone()
	e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e106)
	local e107 = e106:Clone()
	e107:SetCode(EFFECT_CANNOT_TURN_SET)
	c:RegisterEffect(e107)
	local e108 = e107:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109 = e108:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110 = e109:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e111 = e109:Clone()
	e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e111)

	local e9 = Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e9:SetCode(EFFECT_CANNOT_ATTACK)
	e9:SetCondition(s.atcon)
	-- c:RegisterEffect(e9)

	local e21 = Effect.CreateEffect(c)
	e21:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
	e21:SetRange(LOCATION_ONFIELD)
	e21:SetCode(EFFECT_IMMUNE_EFFECT)
	e21:SetType(EFFECT_TYPE_SINGLE)
	e21:SetValue(s.eefilter)
	e21:SetCondition(s.ocon)
	c:RegisterEffect(e21)
	local e22 = Effect.CreateEffect(c)
	e22:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_F)
	e22:SetRange(LOCATION_ONFIELD)
	e22:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e22:SetCountLimit(1)
	e22:SetCode(EVENT_PHASE + PHASE_END)
	e22:SetCondition(s.ocon2)
	e22:SetOperation(s.ermop)
	c:RegisterEffect(e22)

	local e400 = Effect.CreateEffect(c)
	e400:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e400:SetCode(EVENT_SUMMON_SUCCESS)
	e400:SetOperation(s.sumsuc)
	c:RegisterEffect(e400)
end

function s.sumsuc(e, tp, eg, ep, ev, re, r, rp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

function s.ofilter2(c)
	return c:IsFaceup() and c:IsSetCard(0x900) and c:IsType(TYPE_FIELD)
end
function s.ocon(e)
	return Duel.IsExistingMatchingCard(s.ofilter2, e:GetHandlerPlayer(), LOCATION_SZONE, LOCATION_SZONE, 1, nil)
end
function s.eefilter(e, te)
	return te:GetOwner() ~= e:GetOwner()
end

function s.ocon2(e)
	return not Duel.IsExistingMatchingCard(s.ofilter2, e:GetHandlerPlayer(), LOCATION_SZONE, LOCATION_SZONE, 1, nil)
end
function s.ermop(e, tp, eg, ep, ev, re, r, rp, c)
	Duel.Remove(e:GetHandler(), POS_FACEUP, REASON_RULE)
end
------------------------------------------------------------------------
function s.spmfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsReleasableByEffect()
end -----------------------------------------------------------
function s.spcon(e, c)
	if c == nil then
		return true
	end
	local tp = c:GetControler()
	local a = Duel.GetMatchingGroupCount(s.spmfilter, tp, LOCATION_MZONE, 0, nil)
	return a >= 3 and (Duel.GetLocationCount(tp, LOCATION_MZONE)>=-3)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
	if Duel.GetLocationCount(tp, LOCATION_MZONE)<-3 then return end
	local g = Duel.SelectMatchingCard(c:GetControler(), s.spmfilter, c:GetControler(), LOCATION_ONFIELD, 0, 3, 3, nil)
	Duel.Release(g, REASON_COST)
end

-------------------------------------------------------------------------------------------------------------------------------------------
function s.tgcon(e, tp, eg, ep, ev, re, r, rp)
	return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.ofilter(c)
	return c:IsFaceup() and c:IsSetCard(0x900)
end

function s.descost(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.CheckReleaseGroup(tp, s.ofilter, 2, e:GetHandler())
	end
	local g = Duel.SelectReleaseGroup(tp, s.ofilter, 2, 2, e:GetHandler())
	Duel.Release(g, REASON_COST)
end

function s.filter(c, atk)
	return ((c:IsPosition(POS_FACEUP_ATTACK) and c:GetAttack() < atk) or
			   (c:IsPosition(POS_FACEUP_DEFENSE) and c:GetDefense() < atk) or c:IsFacedown())
end

function s.destg(e, tp, eg, ep, ev, re, r, rp, chk)
	local c = e:GetHandler()
	local atk = c:GetAttack()
	if chk == 0 then
		return Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil, atk)
	end
	local g = Duel.GetMatchingGroup(s.filter, tp, 0, LOCATION_MZONE, nil, atk)
	Duel.SetOperationInfo(0, CATEGORY_DESTROY, g, g:GetCount(), 0, 0)
end

function s.desop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	local atk = c:GetAttack()
	local g = Duel.GetMatchingGroup(s.filter, tp, 0, LOCATION_MZONE, nil, atk)
	Duel.Hint(HINT_ANIME, tp, aux.Stringid(828, 0))
	Duel.Destroy(g, REASON_RULE)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetCode(EFFECT_DIRECT_ATTACK)
	e1:SetRange(LOCATION_MZONE)
	e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
	c:RegisterEffect(e1)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.efilterr(e, te)
	local c = e:GetHandler()
	local tc = te:GetOwner()
	return te:IsActiveType(TYPE_TRAP) or
			   ((te:IsActiveType(TYPE_MONSTER) or te:IsActiveType(TYPE_SPELL)) and tc ~= e:GetOwner())
	--  and (( te:GetType()==EFFECT_TYPE_FIELD or te:GetType()==EFFECT_TYPE_EQUIP)  
	--  and Duel.GetTurnCount()-tc:GetTurnID()>=1 ))
	-- and tc:GetFlagEffect(100000020)~=0 and tc:GetFlagEffectLabel(100000020)==c:GetFieldID()) )
end

function s.sdcon2(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	local tc = re:GetHandler()
	return re:GetOwner() ~= e:GetOwner() and (tc:GetFlagEffect(100000020) == 0 or
			   (tc:GetFlagEffect(100000020) ~= 0 and tc:GetFlagEffectLabel(100000020) ~= c:GetFieldID()))
end
function s.sdop2(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	local tc = re:GetHandler()
	-- tc:RegisterFlagEffect(10000002,RESET_EVENT+0x1fe00000,0,1) 
	if re:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and tc:GetFlagEffect(8888) == 0 then
		tc:RegisterFlagEffect(8888, 0, 0, 0)
	end
	local e83 = Effect.CreateEffect(c)
	e83:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e83:SetRange(LOCATION_MZONE)
	e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
	e83:SetCountLimit(1)
	e83:SetCode(EVENT_PHASE + PHASE_END)
	e83:SetLabelObject(re)
	e83:SetOperation(s.setop2)
	-- c:RegisterEffect(e83) 
end
function s.setop2(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	local te = e:GetLabelObject()
	local tc = te:GetHandler()
	if tc:GetFlagEffect(8888) ~= 0 then
		return
	end
	if tc:GetFlagEffect(10000002) == 0 then
		return
	end
	tc:RegisterFlagEffect(100000020, RESET_EVENT + 0x1fe00000, 0, 1)
	tc:SetFlagEffectLabel(100000020, c:GetFieldID())
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.atkcon(e, tp, eg, ep, ev, re, r, rp)
	local ph = Duel.GetCurrentPhase()
	return (ph >= PHASE_BATTLE_START and ph <= PHASE_BATTLE)
end

function s.atkcost(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.CheckReleaseGroup(tp, s.ofilter, 2, e:GetHandler())
	end
	local g = Duel.SelectReleaseGroup(tp, s.ofilter, 2, 2, e:GetHandler())
	Duel.Release(g, REASON_COST)
end

function s.atkop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	if c:IsFacedown() or not c:IsRelateToEffect(e) then
		return
	end
	local e1 = Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetValue(999999)
	e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
	c:RegisterEffect(e1)
	Duel.Hint(HINT_ANIME, tp, aux.Stringid(828, 1))
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.atcon(e)
	return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL and
			   e:GetHandler():GetTurnID() == Duel.GetTurnCount()
end
function s.atcon2(e, tp, eg, ep, ev, re, r, rp)
	local tc = Duel.GetAttacker()
	local tc2 = Duel.GetAttackTarget()
	return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL and tc:IsFaceup() and
			   tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler()
end
function s.atop(e, tp, eg, ep, ev, re, r, rp)
	local tc = Duel.GetAttacker()
	local tc2 = Duel.GetAttackTarget()
	if tc:IsFaceup() and tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler() then
		Duel.ChangeAttackTarget(e:GetHandler())
	end
end

function s.atlimit(e, c)
	return c ~= e:GetHandler()
end

function s.repfilter(c, tc, tp)
	return c:IsControler(tp) and c ~= tc and c:IsLocation(LOCATION_MZONE) and
			   (c:IsReason(REASON_EFFECT) or c:IsReason(REASON_RULE)) and not c:IsReason(REASON_REPLACE) and
			   not c:IsStatus(STATUS_DESTROY_CONFIRMED + STATUS_BATTLE_DESTROYED)
end
function s.reptg(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return eg:IsExists(s.repfilter, 1, e:GetHandler(), e:GetHandler(), tp) and
				   not e:GetHandler():IsStatus(STATUS_DESTROY_CONFIRMED + STATUS_BATTLE_DESTROYED)
	end
	return Duel.SelectYesNo(tp, aux.Stringid(19333131, 0))
end
function s.repval(e, c)
	return s.repfilter(c, e:GetHandler(), e:GetHandlerPlayer())
end
function s.repop(e, tp, eg, ep, ev, re, r, rp)
	Duel.Destroy(e:GetHandler(), REASON_RULE + REASON_REPLACE)
end

function s.ignicon(e, tp, eg, ep, ev, re, r, rp)
	return e:GetHandler():GetFlagEffect(12) ~= 0
end
