--Red-eyes Black Dragon Sword
function c288.initial_effect(c)
    --fusion material
	c:EnableReviveLimit()

	--Activate
	local e0=Effect.CreateEffect(c)
	e0:SetCategory(CATEGORY_EQUIP)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e0:SetTarget(c288.target)
	e0:SetOperation(c288.operation)
	c:RegisterEffect(e0)

    --Equip Limit
    local e1=Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_EQUIP_LIMIT)
	e1:SetValue(c288.eqlimit)
	c:RegisterEffect(e1)

    --Atk Boost
    local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_EQUIP)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetValue(c288.value)
	c:RegisterEffect(e2)
	local e4=e2:Clone()
	e4:SetCode(EFFECT_UPDATE_DEFENSE)
	c:RegisterEffect(e4)	
end        

function c288.filter(c)
	return c:IsFaceup() 
end
function c288.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:GetLocation()==LOCATION_MZONE and c288.filter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(c288.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EQUIP)
	Duel.SelectTarget(tp,c288.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_EQUIP,e:GetHandler(),1,0,0)
end
function c288.operation(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if e:GetHandler():IsRelateToEffect(e) and tc:IsRelateToEffect(e) and tc:IsFaceup() then
		Duel.Equip(tp,e:GetHandler(),tc)
	end
end

function c288.eqlimit(e,c)
    return c:IsFaceup()
end
function c288.value(e,c)
	return Duel.GetMatchingGroupCount(c288.atkfilter,c:GetControler(),LOCATION_MZONE+LOCATION_GRAVE,LOCATION_MZONE+LOCATION_GRAVE,nil)*500+1000
end
function c288.atkfilter(c)
	return c:IsRace(RACE_DRAGON) and c:IsFaceup()
end
function c288.ffilter(c)
	return c:IsCode(170000153) and c:IsType(TYPE_SPELL)
end
