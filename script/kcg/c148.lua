--究級封印-解放儀式
function c148.initial_effect(c)
       local e1=Effect.CreateEffect(c)
       e1:SetCategory(CATEGORY_TODECK+CATEGORY_LEAVE_GRAVE+CATEGORY_TOGRAVE+CATEGORY_SPECIAL_SUMMON)
       e1:SetType(EFFECT_TYPE_ACTIVATE)
       e1:SetCode(EVENT_FREE_CHAIN)
       e1:SetRange(LOCATION_GRAVE+LOCATION_HAND)
       e1:SetCondition(c148.condition)
       e1:SetTarget(c148.target)
       e1:SetOperation(c148.operation)
       c:RegisterEffect(e1)
end

function c148.cfilter(c)
	return c:IsSetCard(0x40) and c:IsType(TYPE_MONSTER)
end
function c148.cfilter1(c)
	return c:IsSetCard(0x40) and c:IsType(TYPE_MONSTER) and c:IsAbleToDeck()
end
function c148.cfilter2(c)
	return c:IsSetCard(0x40) and c:IsType(TYPE_MONSTER) and c:IsAbleToGrave() 
end
function c148.spfilter(c,e,tp)
	return c:IsCode(13893596) and c:IsCanBeSpecialSummoned(e,0,tp,true,false) 
end
function c148.condition(e,tp,eg,ep,ev,re,r,rp) 
      local g=Duel.GetMatchingGroup(c148.cfilter,tp,LOCATION_HAND+LOCATION_GRAVE,0,nil)
      -- local tc=g:GetFirst()
      -- local a=0 local b=0 local c=0 local d=0 local e=0
      -- while tc do
      -- if tc:IsCode(8124921) then a=1 end
      -- if tc:IsCode(44519536) then b=1 end
      -- if tc:IsCode(70903634) then c=1 end
      -- if tc:IsCode(7902349) then d=1 end
      -- if tc:IsCode(33396948) then e=1 end
      -- tc=g:GetNext() end
      return g:GetCount()>=5 and g:GetClassCount(Card.GetCode)>=5
end
function c148.target(e,tp,eg,ep,ev,re,r,rp,chk)
      local g1=Duel.GetMatchingGroup(c148.cfilter1,tp,LOCATION_GRAVE,0,nil)
      local g2=Duel.GetMatchingGroup(c148.cfilter2,tp,LOCATION_HAND,0,nil)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and g1:GetCount()>0 and g2:GetCount()>0 and Duel.IsExistingMatchingCard(c148.spfilter,tp,LOCATION_HAND+LOCATION_DECK,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_TODECK,g1,g1:GetCount(),tp,LOCATION_GRAVE)
      Duel.SetOperationInfo(0,CATEGORY_TOGRAVE,g2,g2:GetCount(),tp,LOCATION_HAND)
      Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g3,1,tp,LOCATION_HAND+LOCATION_DECK)
end
function c148.operation(e,tp,eg,ep,ev,re,r,rp,c)
      local g=Duel.GetMatchingGroup(c148.cfilter1,tp,LOCATION_GRAVE,0,nil)
      local g2=Duel.GetMatchingGroup(c148.cfilter2,tp,LOCATION_HAND,0,nil)
      if g:GetCount()==0 or g2:GetCount()==0 then return end
      Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
      local g3=Duel.SelectMatchingCard(tp,c148.spfilter,tp,LOCATION_HAND+LOCATION_DECK,0,1,1,nil,e,tp)
      if not g3 or Duel.SendtoDeck(g,nil,2,REASON_EFFECT)<1 or Duel.SendtoGrave(g2,REASON_EFFECT)<1 then return end
      if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
      -- local tc=g2:GetFirst()
      -- while tc do
      -- if tc:IsCode(8124921) then c147.a=1 end
      -- if tc:IsCode(44519536) then c147.b=1 end
      -- if tc:IsCode(70903634) then c147.c=1 end
      -- if tc:IsCode(7902349) then c147.d=1 end
      -- if tc:IsCode(33396948) then c147.e=1 end
      -- tc=g2:GetNext() end
      if Duel.SpecialSummon(g3,0,tp,tp,true,false,POS_FACEUP)>0 then
	local e2=Effect.CreateEffect(e:GetHandler())
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_IGNORE_RANGE)
	e2:SetCode(EFFECT_TO_GRAVE_REDIRECT)
	e2:SetRange(LOCATION_GRAVE)
	e2:SetTarget(c148.rmtarget)
	e2:SetTargetRange(0xff,0xff)
	e2:SetValue(LOCATION_DECK)
	e2:SetReset(RESET_EVENT+0x1fe0000)
	e:GetHandler():RegisterEffect(e2) end
end
function c148.rmtarget(e,c)
	return not c:IsLocation(0x80) and c:IsSetCard(0x40) and c:GetReason()~=REASON_EFFECT+147
end
