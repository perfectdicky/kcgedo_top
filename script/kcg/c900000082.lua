--メタル·リフレクト·スライム
function c900000082.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_BATTLE_DAMAGE)
	e1:SetCondition(c900000082.condition)
	e1:SetTarget(c900000082.target)
	e1:SetOperation(c900000082.activate)
	c:RegisterEffect(e1)
end

function c900000082.condition(e,tp,eg,ep,ev,re,r,rp)
	return ep==tp and Duel.GetAttacker()~=nil and Duel.GetAttacker():IsControler(1-tp)
	and Duel.GetAttacker():GetLevel()>0
end
function c900000082.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	local ec=Duel.GetAttacker()
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and 
		Duel.IsPlayerCanSpecialSummonMonster(tp,c:GetCode(),0,ec:GetType(),ec:GetAttack(),ec:GetDefense(),ec:GetLevel(),ec:GetRace(),ec:GetAttribute(),POS_FACEUP_DEFENSE) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
end
function c900000082.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetAttacker()
	if not c:IsRelateToEffect(e) then return end
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0
		or not Duel.IsPlayerCanSpecialSummonMonster(tp,c:GetCode(),0,g:GetType(),0,g:GetDefense()*3/4,g:GetLevel(),g:GetRace(),g:GetAttribute(),POS_FACEUP_DEFENSE) then return end
	c:AddMonsterAttribute(g:GetType()|TYPE_TRAP,g:GetAttribute(),g:GetRace(),g:GetLevel(),0,g:GetDefense()*3/4)
	Duel.SpecialSummonStep(c,0,tp,tp,true,false,POS_FACEUP_DEFENSE)
	c:AddMonsterAttributeComplete()
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SET_BASE_DEFENSE)
	e0:SetReset(RESET_EVENT+RESETS_STANDARD)
	e0:SetValue(g:GetAttack()*3/4)
	c:RegisterEffect(e0,true)  
	local e1=Effect.CreateEffect(c)
	e1=e0:Clone()
	e1:SetCode(EFFECT_ADD_CODE)
	e1:SetValue(g:GetOriginalCodeRule())
	c:RegisterEffect(e1,true) 
	local e2=Effect.CreateEffect(c)
	e2=e0:Clone()
	e2:SetCode(EFFECT_CHANGE_TYPE)
	e2:SetValue(g:GetType())
	c:RegisterEffect(e2,true) 
	local e3=Effect.CreateEffect(c)
	e3=e0:Clone()
	e3:SetCode(EFFECT_CHANGE_RACE)
	e3:SetValue(g:GetRace())
	c:RegisterEffect(e3,true) 
	local e4=Effect.CreateEffect(c)
	e4=e0:Clone()
	e4:SetCode(EFFECT_CHANGE_ATTRIBUTE)
	e4:SetValue(g:GetAttribute())
	c:RegisterEffect(e4,true) 
	local e5=Effect.CreateEffect(c)
	e5=e0:Clone()
	e5:SetCode(EFFECT_CHANGE_LEVEL)
	e5:SetValue(g:GetLevel())
	c:RegisterEffect(e5,true)
	if g:IsSetCard(0x48) then
	local mt=_G["c" .. 900000082]
	mt.xyz_number=g.xyz_number end
	Duel.SpecialSummonComplete()	
end
