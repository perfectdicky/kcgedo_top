--Time Magic Hammer
function c170000195.initial_effect(c)
      --fusion material
	c:EnableReviveLimit()
	c:EnableCounterPermit(0x87)

	--Activate
	local e0=Effect.CreateEffect(c)
	e0:SetCategory(CATEGORY_EQUIP)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e0:SetTarget(c170000195.target)
	e0:SetOperation(c170000195.operation)
	c:RegisterEffect(e0)
        
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_COUNTER)
	e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e1:SetCode(EVENT_ATTACK_ANNOUNCE)
    e1:SetRange(LOCATION_SZONE)
    e1:SetCountLimit(1)
    e1:SetCondition(c170000195.setcon)
    e1:SetOperation(c170000195.set)
    c:RegisterEffect(e1)

     local e3=Effect.CreateEffect(c)
     e3:SetType(EFFECT_TYPE_SINGLE)
     e3:SetCode(EFFECT_EQUIP_LIMIT)
     e3:SetValue(c170000195.eqlimit)
     c:RegisterEffect(e3)
end
    
function c170000195.filter(c)
	return c:IsFaceup() 
end
function c170000195.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:GetLocation()==LOCATION_MZONE and c170000195.filter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(c170000195.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EQUIP)
	Duel.SelectTarget(tp,c170000195.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_EQUIP,e:GetHandler(),1,0,0)
end
function c170000195.operation(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if e:GetHandler():IsRelateToEffect(e) and tc:IsRelateToEffect(e) and tc:IsFaceup() then
		Duel.Equip(tp,e:GetHandler(),tc)
	end
end

function c170000195.setcon(e,tp,eg,ep,ev,re,r,rp)
      return Duel.GetAttacker()==e:GetHandler():GetEquipTarget()
end
function c170000195.set(e,tp,eg,ep,ev,re,rp)
	local et=e:GetHandler():GetEquipTarget()
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_EQUIP)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_DAMAGE)
	e1:SetValue(1)
    e:GetHandler():RegisterEffect(e1)
	local e3=Effect.CreateEffect(e:GetHandler())
	e3:SetType(EFFECT_TYPE_EQUIP)
	e3:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
	e3:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_DAMAGE)
	e3:SetValue(1)
    e:GetHandler():RegisterEffect(e3)

    local g=Duel.GetFieldGroup(tp,0,LOCATION_MZONE)
    if #g>0 then
        local tc=g:GetFirst()    
        while tc do
            local dice=Duel.TossDice(tp,1)
            tc:AddCounter(0x87,dice)
            --e:GetHandler():SetCardTarget(tc)
            tc=g:GetNext()
        end
        --Future Swing
        local e5=Effect.CreateEffect(e:GetHandler()) 
        e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS) 
        e5:SetCode(EVENT_PHASE+PHASE_STANDBY) 
        e5:SetCountLimit(1) 
        e5:SetRange(LOCATION_SZONE) 
        e5:SetCondition(c170000195.con) 
        e5:SetOperation(c170000195.act)
        e5:SetLabel(10)
        e5:SetReset(RESET_EVENT+RESETS_STANDARD)
        e:GetHandler():RegisterEffect(e5)     
    end
end

function c170000195.eqlimit(e,c)
        return c:IsFaceup()
end
function c170000195.ffilter(c)
	return c:IsCode(170000153) and c:IsType(TYPE_SPELL)
end
function c170000195.con(e,tp,eg,ep,ev,re,r,rp)
    return Duel.GetTurnPlayer()~=tp
end
function c170000195.filter(c,rc,ct)
    return rc:GetCardTarget():IsContains(c) 
    and c:GetFlagEffect(170000195)>0 and c:GetFlagEffectLabel(170000195)==ct
end
function c170000195.cfilter(c)
    return Duel.GetTurnCount()==c:GetTurnID()+c:GetFlagEffect(170000195)*2
end
function c170000195.act(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
    if e:GetLabel()==10 then
        local g2=Duel.GetMatchingGroup(Card.IsAbleToRemove,tp,0,LOCATION_MZONE,nil)
        Duel.Remove(g2,nil,REASON_EFFECT+REASON_TEMPORARY)
        local rg=Duel.GetOperatedGroup()
        if #rg<1 then return end
        local maxct=0
        local tc=rg:GetFirst()    
        while tc do
            local dice=tc:GetCounter(0x87)
            c:SetCardTarget(tc)
            tc:RegisterFlagEffect(170000195,RESET_EVENT+RESETS_REDIRECT,0,1,dice)
            maxct=math.max(dice,maxct)
            tc=g:GetNext()
        end
		if maxct==7 then
			c:RegisterFlagEffect(170000195,RESET_EVENT+RESETS_STANDARD,0,1)
			e:SetLabel(7)
		else
			e:SetLabel(6)
		end
	elseif e:GetLabel()>0 then  
 		if c:GetFlagEffect(170000195)>0 then 
			c:SetTurnCounter(8-e:GetLabel())
			g=Duel.GetMatchingGroup(c170000195.filter,tp,LOCATION_REMOVED,LOCATION_REMOVED,nil,c,8-e:GetLabel())
		else
			c:SetTurnCounter(7-e:GetLabel())
			g=Duel.GetMatchingGroup(c170000195.filter,tp,LOCATION_REMOVED,LOCATION_REMOVED,nil,c,7-e:GetLabel())
		end
		if #g>0 then
			local ft=Duel.GetLocationCount(1-tp,LOCATION_MZONE)
			if #g>=ft then
				Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOFIELD)
				local sg=g:Select(1-tp,ft,ft,nil)
				for tc in aux.Next(sg) do
					Duel.ReturnToField(tc)
					g:RemoveCard(tc)
				end
				Duel.SendtoGrave(g,REASON_RULE+REASON_RETURN)
			else
				for tc in aux.Next(g) do
					Duel.ReturnToField(tc)
				end
			end
		end
		e:SetLabel(e:GetLabel()-1)
		if e:GetLabel()==0 then e:Reset() end
	end
end

function c170000195.splimit(e,se,sp,st)
	return e:GetHandler():GetLocation()~=LOCATION_EXTRA
end
