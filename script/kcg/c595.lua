--次元の裂け目
function c595.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c595.condition)
	e1:SetOperation(c595.op)
	c:RegisterEffect(e1)

	local e5=Effect.CreateEffect(c)
	--e5:SetCategory(CATEGORY_TODECK)
	e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	--e5:SetRange(LOCATION_SZONE)
	e5:SetCode(EVENT_PHASE+PHASE_END)
	e5:SetCountLimit(1)
	e5:SetCondition(c595.condition2)
	--e5:SetTarget(c595.target)
	e5:SetOperation(c595.activate)
	Duel.RegisterEffect(e5,0)
end

function c595.filter1(c)
	return c:IsFaceup() and c:IsCode(41418852)
end
function c595.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c595.filter1,tp,LOCATION_SZONE,0,1,nil)
end
function c595.op(e,tp,eg,ep,ev,re,r,rp)
	e:GetHandler():RegisterFlagEffect(595, RESET_PHASE+PHASE_END, 0, 1)
end

function c595.filter(c)
	return c:IsSetCard(0x14b) and c:IsAbleToDeck() and (c:IsType(TYPE_SPELL) or c:IsType(TYPE_TRAP)) 
	and c:GetTurnID()==Duel.GetTurnCount() and not c:IsHasEffect(EFFECT_NECRO_VALLEY)
end
function c595.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c595.filter,tp,LOCATION_GRAVE,LOCATION_GRAVE,1,nil) end
    local g=Duel.GetMatchingGroup(c595.filter, tp, LOCATION_GRAVE,LOCATION_GRAVE, nil)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,g,g:GetCount(),tp,0)
end
function c595.activate(e,tp,eg,ep,ev,re,r,rp)
    local g=Duel.GetMatchingGroup(c595.filter, tp, LOCATION_GRAVE,LOCATION_GRAVE, nil)
	if g:GetCount()>0 then
		Duel.SendtoDeck(g,nil,2,REASON_EFFECT)
	end
end

function c595.condition2(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(595)>0
end