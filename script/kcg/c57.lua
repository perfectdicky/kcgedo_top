--宙讀之儀 (KDIY)
local s,id=GetID()
function s.initial_effect(c)
	local e2=Ritual.AddProcEqual(c,s.ritualfil,nil,nil,nil,nil,s.mritualfil,nil,LOCATION_EXTRA)
	
	--act qp in hand
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e0:SetRange(LOCATION_HAND)
	e0:SetTargetRange(LOCATION_HAND,0)
	e0:SetCondition(s.condition)
	c:RegisterEffect(e0)
	-- Special Summon
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_TOKEN)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_DESTROYED)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DELAY + EFFECT_FLAG_CARD_TARGET)
	e1:SetTarget(s.sptg)
	e1:SetOperation(s.spop)
	c:RegisterEffect(e1)	
end
s.listed_names = {76794549, 56, 55}

function s.ritualfil(c)
	return c:IsCode(55) and c:IsRitualMonster()
end
function s.mritualfil(c)
	return c:IsCode(13331639)
end

function s.condition(e,tp,eg,ep,ev,re,r,rp)
	local ph=Duel.GetCurrentPhase()
	return tp~=Duel.GetTurnPlayer() 
	and (ph>=PHASE_BATTLE_START and ph<=PHASE_BATTLE)
end

function s.spcfilter(c, e, tp)
	return c:IsPreviousControler(tp) and c:IsPreviousLocation(LOCATION_ONFIELD)
end
function s.spcfilterchk(c)
    return c:IsCode(76794549)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
	local c = e:GetHandler()
    if chkc then return s.spcfilterchk(chkc) end
	if chk == 0 then
		return eg:IsExists(s.spcfilter, 1, nil, e, tp) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
        and Duel.IsExistingMatchingCard(s.spcfilterchk,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE,0,1,nil)
        and Duel.IsPlayerCanSpecialSummonMonster(tp,56,0x98,TYPES_TOKEN,2500,2000,7,RACE_SPELLCASTER,ATTRIBUTE_DARK)
	end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	local g=Duel.SelectMatchingCard(tp,s.spcfilterchk,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil)
    if g:GetFirst():IsLocation(LOCATION_HAND) then Duel.ConfirmCards(1-tp, g) end
    Duel.SetTargetCard(g)
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,1,0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,c,1,0,0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	if not c:IsRelateToEffect(e) or not Duel.GetFirstTarget() or Duel.GetLocationCount(tp,LOCATION_MZONE)<1 or not Duel.IsPlayerCanSpecialSummonMonster(tp,56,0x98,TYPES_TOKEN,2500,2000,7,RACE_SPELLCASTER,ATTRIBUTE_DARK) then
		return
	end
    local token=Duel.CreateToken(tp,56)
	Duel.SpecialSummon(token, 1, tp, tp, false, false, POS_FACEUP)
end


