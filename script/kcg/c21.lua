--ユベル－Das Abscheulich Ritter
function c21.initial_effect(c)
	c:EnableReviveLimit()

	--battle
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_REFLECT_BATTLE_DAMAGE)
	e1:SetValue(1)
	c:RegisterEffect(e1)

	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e2:SetValue(1)
	c:RegisterEffect(e2)

	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_REMOVE_TYPE)
	e0:SetValue(TYPE_FUSION)
	c:RegisterEffect(e0)

	--destroy
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_DESTROY)
	e4:SetDescription(aux.Stringid(4779091,1))
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCountLimit(1)
	e4:SetProperty(EFFECT_FLAG_REPEAT)
	e4:SetCode(EVENT_PHASE+PHASE_END)
	  e4:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e4:SetCondition(c21.descon)
	e4:SetTarget(c21.destg)
	  e4:SetOperation(c21.desop) 
	c:RegisterEffect(e4)

	--special summon
	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(4779091,2))
	e5:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e5:SetCode(EVENT_LEAVE_FIELD)
	e5:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e5:SetCondition(c21.spcon)
	e5:SetTarget(c21.sptg)
	e5:SetOperation(c21.spop)
	c:RegisterEffect(e5)

	--cannot special summon
	local e6=Effect.CreateEffect(c)
	e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e6:SetType(EFFECT_TYPE_SINGLE)
	e6:SetCode(EFFECT_SPSUMMON_CONDITION)
	c:RegisterEffect(e6)
end
c21.listed_names={31764700}

function c21.damop2(e,tp,eg,ep,ev,re,r,rp)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_REFLECT_BATTLE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1,0)
	e1:SetReset(RESET_PHASE+PHASE_DAMAGE_CAL)
	Duel.RegisterEffect(e1,tp)
end

function c21.descon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()==tp
end
function c21.sfilter(c)
	return c:IsOnField() and aux.TRUE
end
function c21.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
		local g1count=Duel.GetMatchingGroupCount(c21.sfilter,tp,LOCATION_MZONE,0,e:GetHandler())
		local g2count=Duel.GetMatchingGroupCount(c21.sfilter,1-tp,LOCATION_MZONE,0,e:GetHandler())
	  local count=g1count+g2count
	  if g1count<g2count then count=g1count*2 end
	  Duel.SetOperationInfo(0,CATEGORY_DESTROY,nil,count,0,0) 
end
function c21.desop(e,tp,eg,ep,ev,re,r,rp)
		local g1=Duel.GetMatchingGroup(c21.sfilter,tp,LOCATION_MZONE,0,e:GetHandler())
	  if g1~=nil then
	if Duel.Destroy(g1,REASON_EFFECT)>0 then
	  g1=Duel.GetOperatedGroup()
	  Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	  local g2=Duel.SelectMatchingCard(tp,c21.sfilter,tp,0,LOCATION_MZONE,g1:GetCount(),g1:GetCount(),nil)
	  if g2~=nil then
	Duel.Destroy(g2,REASON_EFFECT) end end end
end

function c21.spcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return c:IsPreviousPosition(POS_FACEUP)
end
function c21.filter(c,e,tp)
	return c:IsCode(31764700) and c:IsCanBeSpecialSummoned(e,0,tp,true,true) and not c:IsHasEffect(EFFECT_NECRO_VALLEY)
end
function c21.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION)>0
		and Duel.IsExistingMatchingCard(c21.filter,tp,LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA)
end
function c21.spop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c21.filter,tp,LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_EXTRA,0,1,1,nil,e,tp)
	if g:GetCount()>0 then
		Duel.SpecialSummon(g,0,tp,tp,true,true,POS_FACEUP)
	end
end
