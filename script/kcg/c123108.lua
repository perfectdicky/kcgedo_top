--奧雷卡爾克斯之神
function c123108.initial_effect(c)
	  c:EnableReviveLimit()

	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	--cannot special summon
	local e0=Effect.CreateEffect(c)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SPSUMMON_CONDITION)
	e0:SetValue(aux.FALSE)
	c:RegisterEffect(e0)

	--activate
	local e51=Effect.CreateEffect(c)
	e51:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e51:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e51:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e51:SetCode(EVENT_LEAVE_FIELD)
	e51:SetRange(LOCATION_EXTRA)
	e51:SetTarget(c123108.sptg2)
	e51:SetOperation(c123108.spop2)
	c:RegisterEffect(e51)

	  --Cannot Lose
	local e18=Effect.CreateEffect(c)
	e18:SetType(EFFECT_TYPE_FIELD)
	e18:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e18:SetCode(EFFECT_CANNOT_LOSE_LP)
	e18:SetRange(LOCATION_MZONE)
	e18:SetTargetRange(1,0)
	e18:SetValue(1)
	c:RegisterEffect(e18)
	local e19=e18:Clone()
	e19:SetCode(EFFECT_CANNOT_LOSE_DECK)
	c:RegisterEffect(e19)
	local e20=e18:Clone()
	e20:SetCode(EFFECT_CANNOT_LOSE_EFFECT)
	c:RegisterEffect(e20)

	
	  --extra damage	  
	  local e7=Effect.CreateEffect(c)
	e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	  e7:SetCategory(CATEGORY_DAMAGE+CATEGORY_RECOVER+CATEGORY_TOHAND)
	  e7:SetType(EFFECT_TYPE_QUICK_F)
	  e7:SetRange(LOCATION_MZONE)
	  e7:SetCode(EVENT_BATTLE_DESTROYING)
	  e7:SetTarget(c123108.tar)
	  e7:SetCondition(c123108.rdcon2)
	  e7:SetOperation(c123108.rdop2)
	  e7:SetCountLimit(1)
	  c:RegisterEffect(e7)

	--特殊召唤不会被无效化
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e9)

	--不能被各种方式解放
	local e12=Effect.CreateEffect(c)
	e12:SetType(EFFECT_TYPE_SINGLE)
	e12:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e12:SetRange(LOCATION_MZONE)
	e12:SetCode(EFFECT_UNRELEASABLE_SUM)
	e12:SetValue(1)
	c:RegisterEffect(e12)
	local e13=e12:Clone()
	e13:SetCode(EFFECT_UNRELEASABLE_NONSUM)
	c:RegisterEffect(e13)
	local e14=e12:Clone()
	e14:SetCode(EFFECT_UNRELEASABLE_EFFECT)
	c:RegisterEffect(e14)

	--不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变为里侧表示、作为特殊召唤素材
	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e101)
	local e102=e101:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103=e102:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104=e103:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e105=e104:Clone()
	e105:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e105)
	local e106=e105:Clone()
	e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e106)
	local e107=e106:Clone()
	e107:SetCode(EFFECT_CANNOT_TURN_SET)
	c:RegisterEffect(e107)
	local e108=e107:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109=e108:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110=e109:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e1102=e109:Clone()
	e1102:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e1102)
	local e111=e109:Clone()
	e111:SetCode(EFFECT_CANNOT_RELEASE)
	c:RegisterEffect(e111)
	local e112=e109:Clone()
	e112:SetCode(EFFECT_CANNOT_USE_AS_COST)
	c:RegisterEffect(e112)
	local e114=e104:Clone()
	e114:SetCode(EFFECT_CANNOT_DISEFFECT)
	c:RegisterEffect(e114)
 
	--immune
	local e121=Effect.CreateEffect(c)
	e121:SetType(EFFECT_TYPE_SINGLE)
	e121:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e121:SetRange(LOCATION_MZONE)
	e121:SetCode(EFFECT_IMMUNE_EFFECT)
	e121:SetValue(c123108.efilter)
	c:RegisterEffect(e121) 

	local e1235=Effect.CreateEffect(c)
	e1235:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1235:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e1235:SetCode(EVENT_LEAVE_FIELD)
	e1235:SetOperation(c123108.lose)
	c:RegisterEffect(e1235)
end

function c123108.lpcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetLP(e:GetHandlerPlayer())~=4
end
function c123108.lpop(e,tp,eg,ep,ev,re,r,rp) 
	Duel.SetLP(e:GetHandlerPlayer(),4)
end 

function c123108.rdop11(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local ttp=c:GetControler()
	if Duel.GetBattleDamage(ttp)>=Duel.GetLP(ttp) then
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1,0)
	e3:SetValue(Duel.GetLP(ttp)-1)
	e3:SetReset(RESET_PHASE+PHASE_DAMAGE)
	Duel.RegisterEffect(e3,ttp) end
end
function c123108.rdcon11(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tp=c:GetControler()
	local e1=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_DAMAGE)
	local e2=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_RECOVER)
	local rd=e1 and not e2
	local rr=not e1 and e2
	local ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_DAMAGE)
	if ex and (cp==tp or cp==PLAYER_ALL) and not rd and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) then 
		return true 
	end
	ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_RECOVER)
	return ex and (cp==tp or cp==PLAYER_ALL) and rr and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) 
end
function c123108.rdop112(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tp=c:GetControler()
	local e1=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_DAMAGE)
	local e2=Duel.IsPlayerAffectedByEffect(tp,EFFECT_REVERSE_RECOVER)
	local rd=e1 and not e2
	local rr=not e1 and e2
	local ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_DAMAGE)
	if ex and (cp==tp or cp==PLAYER_ALL) and not rd and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) then 
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1,0)
	e3:SetValue(Duel.GetLP(tp)-1)
	e3:SetReset(RESET_EVENT+EVENT_CHAIN_SOLVED)
	Duel.RegisterEffect(e3,tp) 
	end
	ex,cg,ct,cp,cv=Duel.GetOperationInfo(ev,CATEGORY_RECOVER)
	if ex and (cp==tp or cp==PLAYER_ALL) and rr and not Duel.IsPlayerAffectedByEffect(tp,EFFECT_NO_EFFECT_DAMAGE) and cv>=Duel.GetLP(tp) then
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1,0)
	e3:SetValue(Duel.GetLP(tp)-1)
	e3:SetReset(RESET_EVENT+EVENT_CHAIN_SOLVED)
	Duel.RegisterEffect(e3,tp) 
	end 
end
function c123108.dc(e)
local tp=e:GetOwner():GetControler()
return Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)<1
end

function c123108.damval(e,re,val,r,rp,rc)
	if bit.band(r,REASON_EFFECT)~=0 then return 0 end
	return val
end
function c123108.rdcon(e,tp,eg,ep,ev,re,r,rp)
	  local c=e:GetHandler()
	  local ttp=c:GetControler()
	return (c==Duel.GetAttackTarget() or c==Duel.GetAttacker())
	  and c:GetAttack()>200000 and Duel.GetLP(1-ttp)>Duel.GetBattleDamage(1-ttp)
end
function c123108.rdop(e,tp,eg,ep,ev,re,r,rp)
	  local c=e:GetHandler()
	  local X=Duel.GetLP(1-tp)
	  local ttp=c:GetControler()
	  if c:GetAttack()>200000 and Duel.GetLP(1-ttp)>Duel.GetBattleDamage(1-ttp) then
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1,0)
	e3:SetValue(X)
	e3:SetReset(RESET_PHASE+PHASE_DAMAGE)
	Duel.RegisterEffect(e3,ttp) end
end

function c123108.rdcon2(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return c:IsRelateToBattle() and c:IsStatus(STATUS_OPPO_BATTLE) and e:GetHandler()==Duel.GetAttacker()
end
function c123108.tar(e,tp,eg,ep,ev,re,r,rp,chk)
	local a=Duel.GetAttackTarget()
	  local X=a:GetAttack()*2+2000
	  if chk==0 then return true end
	  Duel.SetOperationInfo(0,CATEGORY_DAMAGE,1-tp,1,0,X)
	  Duel.SetOperationInfo(0,CATEGORY_RECOVER,tp,1,0,X)
end
function c123108.rdop2(e,tp,eg,ep,ev,re,r,rp)
local a=Duel.GetAttackTarget()
local X=a:GetAttack()*2+2000
if Duel.Damage(1-tp,X,REASON_EFFECT)>0 then
Duel.Recover(tp,X,REASON_EFFECT) end
end

function c123108.efilter(e,te)
	return te:GetOwnerPlayer()~=e:GetOwnerPlayer()
end

function c123108.spcon(e,tp,eg,ep,ev,re,r,rp)
		return e:GetHandler():IsReason(REASON_DESTROY) 
end
function c123108.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
		if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
				and e:GetHandler():IsCanBeSpecialSummoned(e,0,tp,false,false)
				end
		Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
end
function c123108.spop(e,tp,eg,ep,ev,re,r,rp)
		local c=e:GetHandler()
		if c:IsRelateToEffect(e) and Duel.SpecialSummon(c,0,tp,tp,false,false,POS_FACEUP)>0 then
				local e1=Effect.CreateEffect(c)
				e1:SetType(EFFECT_TYPE_SINGLE)
				e1:SetCode(EFFECT_CANNOT_ATTACK)
				e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
				e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
				c:RegisterEffect(e1)

				local e2=Effect.CreateEffect(c)
				e2:SetType(EFFECT_TYPE_SINGLE)
				e2:SetCode(EFFECT_SET_ATTACK)
				e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
				e2:SetValue(2000)
				c:RegisterEffect(e2)
				Duel.BreakEffect() 
		end
end

function c123108.cfilter(c)
	   return c:IsSetCard(0x900) and c:IsType(TYPE_FIELD) and c:IsFaceup()
end
function c123108.cfilter2(c,tp)
	return c:IsCode(123106) and c:IsPreviousControler(tp) and c:IsPreviousLocation(LOCATION_ONFIELD)
end
function c123108.spfilter(c,e,tp)
	return c:IsCode(123106) 
	  and (not c:IsLocation(LOCATION_MZONE) or c:IsControler(1-tp))
end
function c123108.sptg2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:IsExists(c123108.cfilter2,1,nil,tp)
		and Duel.IsExistingMatchingCard(c123108.cfilter,tp,LOCATION_SZONE,0,1,nil)
		and Duel.GetLocationCountFromEx(tp,tp,nil,e:GetHandler())>0 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
end
function c123108.spop2(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCountFromEx(tp,tp,nil,e:GetHandler())==0 then return end
	Duel.SpecialSummon(e:GetHandler(),1,tp,tp,true,true,POS_FACEUP)
end

function c123108.lose(e,tp,eg,ep,ev,re,r,rp)
	  local tc=e:GetHandler() 
	  local ttp=tc:GetOwner() 
	  local WIN_REASON_DIVINE_SERPENT=0x103
	  Duel.Win(1-ttp,WIN_REASON_DIVINE_SERPENT)
end