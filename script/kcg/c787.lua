--CXyz Barian, the King of Wishes
local s, id = GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,4,3,nil,nil,99)
	c:EnableReviveLimit()

	--special summon 
	local e00=Effect.CreateEffect(c)  
	e00:SetType(EFFECT_TYPE_FIELD) 
	e00:SetCode(EFFECT_SPSUMMON_PROC)  
	e00:SetProperty(EFFECT_FLAG_UNCOPYABLE)  
	e00:SetRange(LOCATION_EXTRA)  
	e00:SetCondition(s.spcon)  
	e00:SetOperation(s.spop)  
	e00:SetValue(SUMMON_TYPE_XYZ)
	c:RegisterEffect(e00)  

	--atk
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetType(EFFECT_TYPE_SINGLE)
	-- e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	-- e2:SetRange(LOCATION_MZONE)
	-- e2:SetCode(EFFECT_SET_ATTACK)
	-- e2:SetValue(s.atkval)
	-- c:RegisterEffect(e2)

	local e33=Effect.CreateEffect(c)
	e33:SetDescription(aux.Stringid(13718,10))
	e33:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e33:SetType(EFFECT_TYPE_IGNITION)
	e33:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e33:SetRange(LOCATION_MZONE)
	e33:SetCountLimit(1)
	e33:SetCost(s.c101cost)
	--e33:SetCondition(s.c101condition)
	e33:SetTarget(s.c101target2)
	e33:SetOperation(s.c101operation2)
	c:RegisterEffect(e33,false,REGISTER_FLAG_DETACH_XMAT)

	-- local e4=Effect.CreateEffect(c)
	-- e4:SetDescription(aux.Stringid(12744567,0))
	-- e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	-- e4:SetCode(EVENT_ATTACK_ANNOUNCE)
	-- e4:SetProperty(EFFECT_FLAG_CARD_TARGET)
	-- e4:SetRange(LOCATION_MZONE)
	-- e4:SetCondition(s.condition)
	-- e4:SetTarget(s.target)
	-- e4:SetOperation(s.operation)
	-- c:RegisterEffect(e4)

	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e5:SetRange(LOCATION_MZONE)
	e5:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
	e5:SetCondition(s.indcon)  
	e5:SetValue(aux.imval1) 
	c:RegisterEffect(e5)
	local e6=e5:Clone()
	e6:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
	e6:SetCondition(s.indcon)  
	e6:SetValue(aux.tgoval)  
	c:RegisterEffect(e6)
end
s.listed_series={0x48,0x901,0x912}

function s.spcon(e,c)
	if c==nil then return true end
	local g=Duel.GetMatchingGroup(s.ovfilter,c:GetControler(),LOCATION_MZONE,LOCATION_MZONE,nil,c)
	return Duel.IsExistingMatchingCard(s.ovfilter2,c:GetControler(),LOCATION_MZONE,LOCATION_MZONE,3,nil,c) 
	 and g:GetCount()>3
	 and Duel.GetLocationCountFromEx(c:GetControler(),c:GetControler(),g,c)>0
end
function s.ovfilter2(c,tc)
	local no=c.xyz_number
	return c:IsFaceup()
	  and (no and no>=101 and no<=107 and c:IsSetCard(0x48))
end
function s.ovfilter(c,tc)
	local no=c.xyz_number
	return c:IsFaceup() and c:IsCanBeXyzMaterial(tc)
	  and ((no and no>=101 and no<=107 and c:IsSetCard(0x48,tc,SUMMON_TYPE_XYZ,tc:GetControler()))
	  or (c:IsXyzLevel(tc,4)))
end
function s.spop(e,tp,eg,ep,ev,re,r,rp,c)
	local c=e:GetHandler()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
	local tg=Duel.SelectMatchingCard(tp,s.ovfilter,c:GetControler(),LOCATION_MZONE,LOCATION_MZONE,3,99,nil,c)
	local ag=tg
	local tc=tg:GetFirst()
	while tc do
		local ttc=tc:GetOverlayGroup()
		if ttc~=nil then
		local btc=ttc:GetFirst()
		while btc do
		Duel.Overlay(e:GetHandler(),btc)
		btc=ttc:GetNext() end end
		Duel.Overlay(e:GetHandler(),tc)
		ag:Merge(ttc)
		tc=tg:GetNext() 
	end
	c:SetMaterial(tg)
end

function s.atkval(e,c)
		return c:GetOverlayCount()*500
end

function s.nofilter(c,e,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x48) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x2048)
	and Duel.IsExistingMatchingCard(s.cnofilter,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED,0,1,nil,e,tp,no) 
	--or Duel.GetFlagEffect(tp,90999980)~=0)
end
function s.cnofilter(c,e,tp,no)
	return c.xyz_number and c.xyz_number==no
	and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false) and not c:IsHasEffect(EFFECT_NECRO_VALLEY) and c:IsSetCard(0x1048) 
end
--101
-- function s.c101condition(e,tp,eg,ep,ev,re,r,rp)
-- 	return e:GetHandler():GetOverlayGroup():IsExists(s.nofilter,1,nil,e,tp)
-- end
function s.c101cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckLPCost(tp,400) and e:GetHandler():GetOverlayGroup():IsExists(s.nofilter,1,nil,e,tp) end
	Duel.PayLPCost(tp,400)
	local o=e:GetHandler():GetOverlayGroup() 
	local g=o:FilterSelect(tp,s.nofilter,1,1,e:GetHandler(),e,tp)
	if Duel.SendtoGrave(g,REASON_COST)<1 then return end
	e:SetLabel(g:GetFirst().xyz_number)
end
function s.c101target2(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chk==0 then return Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_XYZ)>0 end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,0,1,0,0)
end
function s.c101operation2(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)==0 or e:GetLabel()==nil or not (Duel.IsExistingMatchingCard(s.cnofilter,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED,0,1,nil,e,tp,e:GetLabel()) or Duel.GetFlagEffect(tp,90999980)~=0) then return end
	local c=e:GetHandler()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Group.CreateGroup()
	g=Duel.SelectMatchingCard(tp,s.cnofilter,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED,0,1,1,nil,e,tp,e:GetLabel())
	-- if g:GetCount()<1 and Duel.GetFlagEffect(tp,90999980)~=0 then
	-- local cno=e:GetLabel()
	-- local cx=0
	-- if cno==101 then cx=96  end
	-- if cno==107 then cx=98  end
	-- if cno==106 then cx=249  end
	-- if cno==105 then cx=253  end
	-- if cno==102 then cx=255  end
	-- if cno==104 then cx=257  end
	-- if cno==103 then cx=260  end
	-- local sg=Duel.CreateToken(tp,cx) 
	-- g:AddCard(sg) end
	if Duel.SpecialSummon(g,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)>0 then
	  local e0=Effect.CreateEffect(c)
	  e0:SetType(EFFECT_TYPE_SINGLE)
	  e0:SetCode(EFFECT_IMMUNE_EFFECT)
	  e0:SetValue(1)
	  e0:SetReset(RESET_EVENT+RESETS_STANDARD)
	  g:GetFirst():RegisterEffect(e0,true)
	  local e1=e0:Clone()
	  e1:SetCode(EFFECT_INDESTRUCTABLE)
	  g:GetFirst():RegisterEffect(e1,true)
	g:GetFirst():CompleteProcedure()
	if Duel.IsExistingMatchingCard(s.spovfilter,tp,LOCATION_GRAVE+LOCATION_DECK,0,1,nil) and Duel.SelectYesNo(tp,aux.Stringid(41546,0)) then 
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_RTOHAND)
	local g2=Duel.SelectMatchingCard(tp,s.spovfilter,tp,LOCATION_GRAVE+LOCATION_DECK,0,1,1,nil)  
	Duel.SendtoHand(g2,tp,REASON_EFFECT)
	end end
end
function s.spovfilter(c,e,tp)
	  return c:IsSetCard(0x901) or  c:IsSetCard(0x912)
end

-- function s.cnofilters(c)
-- 	local no=c.xyz_number
-- 	return no and no>=101 and no<=107 and c:IsSetCard(0x48)
-- end
-- function s.condition(e,tp,eg,ep,ev,re,r,rp)
-- 	local c=e:GetHandler()
-- 	return eg:IsExists(s.cnofilters,1,nil)
-- end
-- function s.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
-- 	if chkc then return chkc:IsLocation(LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED) and chkc:IsControler(tp) and s.cnofilters(chkc) end
-- 	if chk==0 then return e:GetHandler():IsType(TYPE_XYZ) and Duel.IsExistingTarget(s.cnofilters,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED,0,1,nil) end
-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
-- 	Duel.SelectTarget(tp,s.cnofilters,tp,LOCATION_GRAVE+LOCATION_EXTRA+LOCATION_REMOVED,0,1,1,nil)
-- end
-- function s.operation(e,tp,eg,ep,ev,re,r,rp)
-- 	local c=e:GetHandler()
-- 	local tc=Duel.GetFirstTarget()
-- 	if c:IsRelateToEffect(e) and tc:IsRelateToEffect(e) and not tc:IsImmuneToEffect(e) then
-- 		Duel.Overlay(c,Group.FromCards(tc))
-- 	end
-- end

function s.pnofilters(c)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x48) and c:IsFaceup()
end
function s.indcon(e)  
	return Duel.IsExistingMatchingCard(s.pnofilters,e:GetHandlerPlayer(),LOCATION_MZONE,0,1,e:GetHandler())  
end  