--
function c821.initial_effect(c)
	local e1=aux.AddNormalSummonProcedure(c,true,false,3,3)
	local e2=aux.AddNormalSetProcedure(c,true,false,3,3)		
	--解放3只祭品通召
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	-- e1:SetType(EFFECT_TYPE_SINGLE)
	-- e1:SetCode(EFFECT_LIMIT_SUMMON_PROC)
	-- e1:SetCondition(c821.ttcon)
	-- e1:SetOperation(c821.ttop)
	-- e1:SetValue(SUMMON_TYPE_TRIBUTE)
	-- c:RegisterEffect(e1)
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	-- e2:SetType(EFFECT_TYPE_SINGLE)
	-- e2:SetCode(EFFECT_LIMIT_SET_PROC)
	-- e2:SetCondition(c821.ttcon)
	-- e2:SetOperation(c821.ttop)
	-- c:RegisterEffect(e2)

 	--通常召唤、特殊召唤、反转召唤不会被无效化
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE)
	e8:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
	e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e8)
	local e9=e8:Clone()
	e9:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	c:RegisterEffect(e9)
	local e10=e9:Clone()
	e10:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
    c:RegisterEffect(e10)
       
	local e400=Effect.CreateEffect(c)
	e400:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e400:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e400:SetCode(EVENT_SUMMON_SUCCESS)
	e400:SetOperation(c821.sumsuc)
    c:RegisterEffect(e400)
    local e401=e400:Clone()
    e401:SetCode(EVENT_SPSUMMON_SUCCESS)	
    c:RegisterEffect(e401)
    local e402=e400:Clone()
    e402:SetCode(EVENT_FLIP_SUMMON_SUCCESS)	
	c:RegisterEffect(e402)   
	
	local e200=Effect.CreateEffect(c)
	e200:SetType(EFFECT_TYPE_SINGLE)
	e200:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e200:SetCode(EFFECT_ADD_RACE)
	e200:SetValue(RACE_DRAGON)
	c:RegisterEffect(e200)	
end

function c821.ttcon(e,c,minc)
	if c==nil then return true end
	return minc<=3 and Duel.CheckTribute(c,3)
end
function c821.ttop(e,tp,eg,ep,ev,re,r,rp,c)
	local g=Duel.SelectTribute(tp,c,3,3)
	c:SetMaterial(g)
	Duel.Release(g,REASON_SUMMON+REASON_MATERIAL)
end

function c821.sumsuc(e,tp,eg,ep,ev,re,r,rp)
    Duel.SetChainLimitTillChainEnd(aux.FALSE)
    local opt=Duel.SelectOption(e:GetHandlerPlayer(),aux.Stringid(821,0),aux.Stringid(821,1))
	if opt==0 then 
	e:GetHandler():SetEntityCode(709,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
	else e:GetHandler():SetEntityCode(822,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,true) 
	end
end