-- 最强决斗者之力
function c157.initial_effect(c)
    local e40 = Effect.CreateEffect(c)
    e40:SetDescription(aux.Stringid(13709, 2))
    e40:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e40:SetType(EFFECT_TYPE_IGNITION)
    e40:SetRange(LOCATION_REMOVED)
    e40:SetCondition(c157.xyzcondition85)
    e40:SetTarget(c157.distg)
    e40:SetOperation(c157.xyzactivate85)
    c:RegisterEffect(e40)

    local e10 = Effect.CreateEffect(c)
    e10:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e10:SetType(EFFECT_TYPE_IGNITION)
    e10:SetRange(LOCATION_REMOVED)
    e10:SetCondition(c157.SynCondition9)
    e10:SetTarget(c157.distg)
    e10:SetOperation(c157.SynOperation9)
    --c:RegisterEffect(e10)

    local e15 = Effect.CreateEffect(c)
    e15:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e15:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e15:SetCode(EVENT_PHASE_START + PHASE_MAIN1)
    e15:SetRange(LOCATION_REMOVED)
    e15:SetCountLimit(1)
    e15:SetCondition(c157.MCondition)
    e15:SetCost(c157.SynCost)
    e15:SetOperation(c157.MOp)
    c:RegisterEffect(e15)

    local e20 = Effect.CreateEffect(c)
    e20:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e20:SetType(EFFECT_TYPE_QUICK_O)
    e20:SetCode(EVENT_CHAINING)
    e20:SetRange(LOCATION_REMOVED)
    e20:SetCondition(c157.act20condition)
    e20:SetTarget(c157.distg)
    e20:SetOperation(c157.activate2)
    e20:SetCountLimit(1)
    c:RegisterEffect(e20)

    local e33 = Effect.CreateEffect(c)
    e33:SetDescription(aux.Stringid(111011901, 1))
    e33:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e33:SetType(EFFECT_TYPE_IGNITION)
    e33:SetRange(LOCATION_REMOVED)
    e33:SetCondition(c157.condition32)
    e33:SetTarget(c157.distg)
    e33:SetOperation(c157.activate32)
    e33:SetCountLimit(1)
    c:RegisterEffect(e33)

    -- local e135 = Effect.CreateEffect(c)
    -- e135:SetCode(EVENT_ADJUST)
    -- e135:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    -- e135:SetRange(LOCATION_REMOVED)
    -- e135:SetTarget(c157.adtg)
    -- c:RegisterEffect(e135)
    -- local e136 = e135:Clone()
    -- e136:SetCode(EVENT_CHAIN_END)
    -- e136:SetTarget(c157.adtg2)
    -- c:RegisterEffect(e136)

    -- synchro effect
    local e8 = Effect.CreateEffect(c)
    e8:SetDescription(aux.Stringid(50091196, 1))
    e8:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_QUICK_O)
    e8:SetCode(EVENT_FREE_CHAIN)
    e8:SetHintTiming(0, 0x1c0 + TIMING_MAIN_END)
    e8:SetRange(LOCATION_REMOVED)
    e8:SetCondition(c157.syncondition0)
    e8:SetTarget(c157.distg)
    e8:SetOperation(c157.synactivate)
    c:RegisterEffect(e8)

    local e40 = Effect.CreateEffect(c)
    e40:SetDescription(aux.Stringid(13717, 3))
    e40:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e40:SetType(EFFECT_TYPE_IGNITION)
    e40:SetRange(LOCATION_REMOVED)
    e40:SetCondition(c157.xyzcondition)
    e40:SetTarget(c157.distg)
    e40:SetOperation(c157.xyzactivate)
    --c:RegisterEffect(e40)
    local e41 = e40:Clone()
    e41:SetDescription(aux.Stringid(13717, 4))
    e41:SetCondition(c157.xyzcondition2)
    e41:SetOperation(c157.xyzactivate2)
    --:RegisterEffect(e41)
    local e42 = e40:Clone()
    e42:SetDescription(aux.Stringid(13717, 5))
    e42:SetCondition(c157.xyzcondition3)
    e42:SetOperation(c157.xyzactivate3)
    --c:RegisterEffect(e42)
    local e45 = e40:Clone()
    e45:SetDescription(aux.Stringid(13717, 6))
    e45:SetCondition(c157.xyzcondition4)
    e45:SetOperation(c157.xyzactivate4)
    --c:RegisterEffect(e45)
    local e46 = e40:Clone()
    e46:SetDescription(aux.Stringid(13713, 0))
    e46:SetCondition(c157.xyzcondition5)
    e46:SetOperation(c157.xyzactivate5)
    --c:RegisterEffect(e46)
    local e6 = e40:Clone()
    e6:SetDescription(aux.Stringid(13717, 12))
    e6:SetCondition(c157.syncondition)
    e6:SetOperation(c157.synactivate)
    --c:RegisterEffect(e6)
    local e61 = e40:Clone()
    e61:SetDescription(aux.Stringid(13717, 13))
    e61:SetCondition(c157.syncondition2)
    e61:SetOperation(c157.synactivate2)
    --c:RegisterEffect(e61)
    local e62 = e40:Clone()
    e62:SetDescription(aux.Stringid(13717, 14))
    e62:SetCondition(c157.syncondition3)
    e62:SetOperation(c157.synactivate3)
    --c:RegisterEffect(e62)
    local e63 = e40:Clone()
    e63:SetDescription(aux.Stringid(13717, 15))
    e63:SetCondition(c157.syncondition4)
    e63:SetOperation(c157.synactivate4)
    --c:RegisterEffect(e63)
    local e64 = e40:Clone()
    e64:SetDescription(aux.Stringid(13714, 0))
    e64:SetCondition(c157.syncondition5)
    e64:SetOperation(c157.synactivate5)
    --c:RegisterEffect(e64)
    local e65 = e40:Clone()
    e65:SetDescription(aux.Stringid(13714, 1))
    e65:SetCondition(c157.syncondition6)
    e65:SetOperation(c157.synactivate6)
    --c:RegisterEffect(e65)
    local e66 = e40:Clone()
    e66:SetDescription(aux.Stringid(13714, 12))
    e66:SetCondition(c157.syncondition7)
    e66:SetOperation(c157.synactivate7)
    --c:RegisterEffect(e66)

    local e4 = Effect.CreateEffect(c)
    e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_SINGLE_RANGE)
    e4:SetType(EFFECT_TYPE_SINGLE)
    e4:SetRange(LOCATION_REMOVED)
    e4:SetCode(EFFECT_CANNOT_DISABLE)
    c:RegisterEffect(e4)
    -- immune
    local e121 = Effect.CreateEffect(c)
    e121:SetType(EFFECT_TYPE_SINGLE)
    e121:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e121:SetRange(LOCATION_REMOVED)
    e121:SetCode(EFFECT_IMMUNE_EFFECT)
    e121:SetValue(function(e,te) return te:GetOwner()~=e:GetOwner() end)
    c:RegisterEffect(e121)
    local e122 = Effect.CreateEffect(c)
    e122:SetType(EFFECT_TYPE_SINGLE)
    e122:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
    e122:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e122:SetRange(LOCATION_REMOVED)
    e122:SetValue(1)
    c:RegisterEffect(e122)
    local e123 = e121:Clone()
    e123:SetCode(EFFECT_GOD_IMMUNE)
    c:RegisterEffect(e123)

    local e784 = Effect.CreateEffect(c)
    e784:SetDescription(aux.Stringid(826, 5))
    e784:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e784:SetType(EFFECT_TYPE_IGNITION)
    e784:SetRange(LOCATION_REMOVED)
    e784:SetCountLimit(1, EFFECT_COUNT_CODE_DUEL)
    e784:SetCondition(c157.lmCondition)
    e784:SetTarget(c157.distg)
    e784:SetOperation(c157.lmOperation)
    c:RegisterEffect(e784)
    local e7842 = Effect.CreateEffect(c)
    e7842:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e7842:SetCode(EVENT_TOSS_DICE_NEGATE)
    e7842:SetRange(LOCATION_REMOVED)
    e7842:SetCondition(c157.lmCondition2)
    e7842:SetTarget(c157.distg)
    e7842:SetOperation(c157.lmOperation2)
    c:RegisterEffect(e7842)
    local e7843 = Effect.CreateEffect(c)
    e7843:SetDescription(aux.Stringid(826, 8))
    e7843:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e7843:SetType(EFFECT_TYPE_QUICK_O)
    e7843:SetCode(EVENT_FREE_CHAIN)
    e7843:SetRange(LOCATION_REMOVED)
    e7843:SetCountLimit(1)
    e7843:SetCondition(c157.lmCondition3)
    e7843:SetTarget(c157.distg)
    e7843:SetOperation(c157.lmOperation3)
    c:RegisterEffect(e7843)
    local e7844 = Effect.CreateEffect(c)
    e7844:SetDescription(aux.Stringid(826, 9))
    e7844:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e7844:SetType(EFFECT_TYPE_IGNITION)
    e7844:SetRange(LOCATION_REMOVED)
    e7844:SetCountLimit(1, EFFECT_COUNT_CODE_DUEL)
    e7844:SetCondition(c157.lmCondition4)
    e7844:SetTarget(c157.distg)
    e7844:SetOperation(c157.lmOperation4)
    c:RegisterEffect(e7844)

    local e104 = Effect.CreateEffect(c)
    e104:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e104:SetType(EFFECT_TYPE_IGNITION)
    e104:SetCountLimit(1)
    e104:SetRange(LOCATION_REMOVED)
    e104:SetCondition(c157.phantomCondition)
    e104:SetTarget(c157.distg)
    e104:SetOperation(c157.phantomOperation)
    c:RegisterEffect(e104)

    local e105 = Effect.CreateEffect(c)
    e105:SetDescription(aux.Stringid(823, 0))
    e105:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
    e105:SetType(EFFECT_TYPE_QUICK_O)
    e105:SetCode(EVENT_CHAINING)
    e105:SetRange(LOCATION_REMOVED)
    e105:SetCondition(c157.yugicon)
    e105:SetTarget(c157.distg)
    e105:SetOperation(c157.yugiop)
    c:RegisterEffect(e105)
end

function c157.xyzfilter(c)
    return c:IsFaceup() and c:IsType(TYPE_XYZ) and (c:IsSetCard(0x1048) or c:IsSetCard(0x1073))
end
function c157.filter2(c, e, tp)
    local no = c.xyz_number
    return no and no >= 101 and no <= 107 and c:IsSetCard(0x1048)
end

function c157.chk(c, code)
    return c:GetCode() == code
end
function c157.astral(c)
    return c:IsType(TYPE_XYZ) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x1073) and
               Duel.GetMatchingGroupCount(c157.astral2, c:GetControler(), LOCATION_EXTRA, 0, c, c:GetOriginalCode()) ==
               0
end
function c157.astral2(c, code)
    return c:IsType(TYPE_XYZ) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x1073) and c:GetOriginalCode() == code
end
function c157.rankfiler(c)
    return c:GetRank() >= 10 and c:IsFaceup()
end
function c157.costfilter(c, code)
    return c:GetCode() == code and c:IsAbleToRemoveAsCost()
end
function c157.hopefiler(c)
    return (c:IsCode(84013237) or c:IsCode(84124261)) and c:GetOverlayCount() > 0
end
function c157.zspfilter(c, e, tp)
    return not c:IsAttribute(ATTRIBUTE_LIGHT) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end
function c157.filterno(c, e, tp, tc)
    return c:IsSetCard(0x48) and c:IsType(TYPE_XYZ)
end

-- Cartoon World
function c157.DTMfiler(c, tp)
    return Duel.GetMatchingGroupCount(c157.DTMfiler2, tp, LOCATION_DECK + LOCATION_HAND, 0, nil) > 0 and
               Duel.GetMatchingGroupCount(Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 15259703) > 0
end
function c157.DTMfiler2(c)
    return c:IsSetCard(0x62) and c:IsType(TYPE_MONSTER)
end
function c157.DTMfiler3(c)
    return c:IsSetCard(0x62) and c:IsFaceup()
end
function c157.DTMfiler4(c)
    return c:IsCode(15259703) and c:IsFaceup()
end
-- Gods
function c157.DGDfiler(c, tp)
    return c:IsCode(10000020) and c:IsFaceup() and c:IsRace(RACE_CREATORGOD) and
               Duel.CheckReleaseGroup(tp, c157.DGDfiler2, 1, nil, c, tp, c:GetTurnID())
end
function c157.DGDfiler2(c, tc1, tp, TID)
    local g = Group.FromCards(tc1, c)
    return c:IsCode(10000000) and c:IsFaceup() and c ~= tc1 and c:IsRace(RACE_CREATORGOD) and
               Duel.CheckReleaseGroup(tp, c157.DGDfiler3, 1, nil, g, tp, TID)
end
function c157.DGDfiler3(c, g, tp, TID)
    local ag = g
    if ag:IsContains(c) then
        return false
    end
    ag:AddCard(c)
    return c:IsCode(10000010) and c:IsFaceup() and
               Duel.GetLocationCountFromEx(tp, tp, ag, TYPE_FUSION) > 0 and c:IsRace(RACE_CREATORGOD)
end
-- Hope
function c157.DDPfiler(c, qt, tp)
    return Duel.GetMatchingGroupCount(Card.IsCode, tp, 0x13 + LOCATION_REMOVED, 0, nil, c:GetCode()) < qt
end
function c157.DDCfiler(c)
    return c:IsSetCard(0x7f) and c:IsSetCard(0x1048)
end
-- Syncho
function c157.RDSfiler(c)
    return c:IsCode(44508094) or c:IsCode(70902743)
end

function c157.RRDSfiler(c)
    return c:IsCode(2403771)
end
function c157.RRDSfiler2(c, qt)
    return c:IsType(TYPE_TUNER) and c:GetLevel() == qt
end

function c157.SynCondition9(e, tp, eg, ep, ev, re, r, rp)
    local ttp = e:GetHandler():GetControler()
    return Duel.GetFlagEffect(ttp, 90799980) ~= 0 and Duel.CheckReleaseGroup(ttp, c157.DGDfiler, 1, nil, ttp)
end
function c157.SynCost(e, tp, eg, ep, ev, re, r, rp, chk)
    local ttp = e:GetHandler():GetControler()
    if chk == 0 then
        return Duel.GetFlagEffect(ttp, 93999970) == 0
    end
    Duel.RegisterFlagEffect(ttp, 93999970, RESET_EVENT + EVENT_CHAIN_END, 0, 1)
end
function c157.SynOperation9(e, tp, eg, ep, ev, re, r, rp)
    local ttp = e:GetHandler():GetControler()
    if Duel.GetFlagEffect(ttp, 90799980) ~= 0 and Duel.CheckReleaseGroup(ttp, c157.DGDfiler, 1, nil, tp) then
        -- if Duel.SelectYesNo(ttp,aux.Stringid(110000010,6)) then
        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(826, 6))
        local gt1 = Duel.SelectMatchingCard(ttp, c157.DGDfiler, ttp, LOCATION_MZONE, 0, 1, 1, nil, ttp):GetFirst()
        local gt2 = Duel.SelectMatchingCard(ttp, c157.DGDfiler2, ttp, LOCATION_MZONE, 0, 1, 1, nil, gt1, ttp,
                        gt1:GetTurnID()):GetFirst()
        local gt3 = Duel.SelectMatchingCard(ttp, c157.DGDfiler3, ttp, LOCATION_MZONE, 0, 1, 1, nil,
                        Group.FromCards(gt1, gt2), ttp, gt1:GetTurnID()):GetFirst()
        local agt = Group.FromCards(gt1, gt2, gt3)
        Duel.Release(agt, REASON_COST + REASON_RULE)
        local e8 = Effect.CreateEffect(e:GetHandler())
        e8:SetType(EFFECT_TYPE_FIELD)
        e8:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
        e8:SetCode(10000042)
        e8:SetTargetRange(0, 1)
        e8:SetValue(1)
        e8:SetReset(RESET_CHAIN)
        Duel.RegisterEffect(e8, ttp)
        local token = Duel.CreateToken(ttp, 805, nil, nil, nil, nil, nil, nil)
        -- Duel.Remove(token,POS_FACEUP,REASON_RULE) 
        Duel.SendtoDeck(token, ttp, 0, REASON_RULE)
        Duel.SpecialSummon(token, 805, ttp, ttp, true, true, POS_FACEUP_ATTACK)
    end
end

function c157.MCondition(e, tp, eg, ep, ev, re, r, rp)
    local ttp = e:GetHandler():GetControler()
    return Duel.GetFlagEffect(ttp, 90899980) ~= 0 and
               Duel.GetMatchingGroupCount(c157.DTMfiler3, ttp, LOCATION_MZONE, 0, nil) > 0 and
               Duel.GetMatchingGroupCount(c157.DTMfiler4, ttp, LOCATION_SZONE, 0, nil) > 0
end
function c157.MOp(e, tp, eg, ep, ev, re, r, rp)
    local ttp = e:GetHandler():GetControler()
    if Duel.GetFlagEffect(ttp, 90899980) ~= 0 then
        if Duel.SelectYesNo(ttp, aux.Stringid(826, 7)) then
            Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(826, 7))
            local dg = Duel.GetFieldGroup(ttp, 0, LOCATION_ONFIELD + LOCATION_HAND)
            Duel.ConfirmCards(ttp, dg)
        end
    end
end

function c157.act20condition(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 93999980) ~= 0 and (Duel.GetLP(1 - tp) >= Duel.GetLP(tp)) and
               re:GetHandler():IsControler(tp) and
               (re:GetHandler():IsCode(24696097) or
                   (re:GetHandler():IsCode(159) and re:GetHandler():GetCounter(0x83) ~= 0)) and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               e:GetHandlerPlayer() and
               Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_DECK, 0, 1, nil, TYPE_TUNER)
end
function c157.distg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    Duel.SetChainLimit(aux.FALSE)
end
function c157.activate2(e, tp, eg, ep, ev, re, r, rp)
    local g = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_DECK, 0, nil, TYPE_TUNER)
    if #g<1 then return end
    if g:GetCount() >= 5 then
        g = g:RandomSelect(tp, 5)
    end
    if g:GetCount() < 5 then
        g = g:RandomSelect(tp, g:GetCount())
    end
    local tac2=Duel.GetDecktopGroup(tp, #g)
    if #tac2<1 then return end
    aux.SwapEntity(g, tac2)
    -- local tc = g:GetFirst()
    -- while tc do
    --     Duel.MoveSequence(tc, 0)
    --     tc = g:GetNext()
    -- end
    --Duel.DisableShuffleCheck()
    if Duel.GetFlagEffect(tp, 89999996) ~= 0 then
        Duel.Hint(HINT_CARD, 0, 510000109)
    end
    Duel.Hint(HINT_MESSAGE, 1 - tp, aux.Stringid(13709, 15))
    Duel.RegisterFlagEffect(tp, 70, 0, 0, 1)
end

function c157.ovfilter1(c)
    return c:IsFaceup() and c:IsCode(84013237)
end
function c157.xyzcondition(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 91999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.ovfilter1, tp, LOCATION_MZONE, 0, 1, nil) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 56840427)
end
function c157.xyzactivate(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 56840427) then
        return
    end
    local g = Duel.CreateToken(tp, 61, nil, nil, nil, nil, nil, nil)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)  
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    local mg = Duel.GetMatchingGroup(c157.ovfilter1, tp, LOCATION_MZONE, 0, nil)
    if mg:GetCount() < 1 then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
    local ng = mg:FilterSelect(tp, c157.ovfilter1, 1, 1, nil)
    if ng:GetCount() ~= 1 then
        return
    end
    local og = ng:GetFirst():GetOverlayGroup()
    if og:GetCount() ~= 0 then
        Duel.Overlay(g, og)
    end
    g:SetMaterial(ng)
    Duel.Overlay(g, ng)
    Duel.SpecialSummon(g, SUMMON_TYPE_XYZ, tp, tp, true, true, POS_FACEUP)
    g:CompleteProcedure()
    --g:RegisterFlagEffect(157, 0, 0, 1)
end

function c157.xyzcondition2(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 91999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.ovfilter1, tp, LOCATION_MZONE, 0, 1, nil) and Duel.GetLP(tp) <=
               Duel.GetLP(1 - tp) - 3000 and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 65) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 86532744)
end
function c157.xyzactivate2(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 65) or
        Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 86532744) then
        return
    end
    local g = Duel.CreateToken(tp, 65, nil, nil, nil, nil, nil, nil)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE) 
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    local mg = Duel.GetMatchingGroup(c157.ovfilter1, tp, LOCATION_MZONE, 0, nil)
    if mg:GetCount() < 1 then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
    local ng = mg:FilterSelect(tp, c157.ovfilter1, 1, 1, nil)
    if ng:GetCount() ~= 1 then
        return
    end
    local og = ng:GetFirst():GetOverlayGroup()
    if og:GetCount() ~= 0 then
        Duel.Overlay(g, og)
    end
    g:SetMaterial(ng)
    Duel.Overlay(g, ng)
    Duel.SpecialSummon(g, SUMMON_TYPE_XYZ, tp, tp, true, true, POS_FACEUP)
    g:CompleteProcedure()
    --g:RegisterFlagEffect(157, 0, 0, 1)
end

function c157.ovfilter3(c)
    return c:IsFaceup() and c:IsType(TYPE_XYZ)
end
function c157.xyzcondition3(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 91999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.ovfilter3, tp, LOCATION_MZONE, 0, 2, nil) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 65305468) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 209)
end
function c157.xyzactivate3(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 65305468) or
        Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 209) then
        return
    end
    local sg = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 209, nil, nil, nil, nil, nil, nil)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    local mg = Duel.GetMatchingGroup(c157.ovfilter3, tp, LOCATION_MZONE, 0, nil)
    if mg:GetCount() < 2 then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
    local ag = mg:FilterSelect(tp, c157.ovfilter3, 2, 2, nil)
    if ag:GetCount() ~= 2 then
        return
    end
    local tc1 = ag:GetFirst()
    local tc2 = ag:GetNext()
    sg:Merge(tc1:GetOverlayGroup())
    sg:Merge(tc2:GetOverlayGroup())
    if sg:GetCount() > 0 then
        Duel.Overlay(g, sg)
    end
    g:SetMaterial(ag)
    Duel.Overlay(g, ag)
    Duel.SpecialSummon(g, SUMMON_TYPE_XYZ, tp, tp, true, true, POS_FACEUP)
    g:CompleteProcedure()
    --g:RegisterFlagEffect(157, 0, 0, 1)
end

function c157.ovfilter4(c)
    return c:IsFaceup() and (c:IsSetCard(0x2048) or c:IsCode(86532744) or c:IsCode(52653092)) and c:IsType(TYPE_XYZ)
end
function c157.xyzcondition4(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 91999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.ovfilter4, tp, LOCATION_MZONE, 0, 3, nil) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 364) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 52653092)
end
function c157.xyzactivate4(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 364) or
        Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 52653092) then
        return
    end
    local sg = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 364, nil, nil, nil, nil, nil, nil)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    local mg = Duel.GetMatchingGroup(c157.ovfilter4, tp, LOCATION_MZONE, 0, nil)
    if mg:GetCount() < 3 then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
    local ng = mg:FilterSelect(tp, c157.ovfilter4, 3, 3, nil)
    if ng:GetCount() ~= 3 then
        return
    end
    local tc1 = ng:GetFirst()
    local tc2 = ng:GetNext()
    local tc3 = ng:GetNext()
    sg:Merge(tc1:GetOverlayGroup())
    sg:Merge(tc2:GetOverlayGroup())
    sg:Merge(tc3:GetOverlayGroup())
    if sg:GetCount() ~= 0 then
        Duel.Overlay(g, sg)
    end
    g:SetMaterial(ng)
    Duel.Overlay(g, ng)
    Duel.SpecialSummon(g, SUMMON_TYPE_XYZ, tp, tp, true, true, POS_FACEUP)
    g:CompleteProcedure()
    --g:RegisterFlagEffect(157, 0, 0, 1)
end

function c157.ovfilter5(c)
    return c:IsFaceup() and c:IsSetCard(0x7e) and c:GetLevel() == 5
end
function c157.xyzcondition5(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 91999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.ovfilter5, tp, LOCATION_MZONE, 0, 2, nil) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 60992364)
end
function c157.xyzactivate5(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 60992364) then
        return
    end
    local sg = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 265, nil, nil, nil, nil, nil, nil)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    local mg = Duel.GetMatchingGroup(c157.ovfilter5, tp, LOCATION_MZONE, 0, nil)
    if mg:GetCount() < 2 then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
    local ng = mg:FilterSelect(tp, c157.ovfilter5, 2, 2, nil)
    if ng:GetCount() ~= 2 then
        return
    end
    local tc1 = ng:GetFirst()
    local tc2 = ng:GetNext()
    sg:Merge(tc1:GetOverlayGroup())
    sg:Merge(tc2:GetOverlayGroup())
    if sg:GetCount() ~= 0 then
        Duel.Overlay(g, sg)
    end
    g:SetMaterial(ng)
    Duel.Overlay(g, ng)
    Duel.SpecialSummon(g, SUMMON_TYPE_XYZ, tp, tp, true, true, POS_FACEUP)
    g:CompleteProcedure()
    --g:RegisterFlagEffect(157, 0, 0, 1)
end

function c157.syncondition0(e, tp, eg, ep, ev, re, r, rp)
    return not e:GetHandler():IsStatus(STATUS_CHAINING) and Duel.GetFlagEffect(tp, 93999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ~=
               tp and Duel.IsExistingMatchingCard(c157.tunfilter, tp, LOCATION_MZONE, 0, 1, nil, tp) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 24696097)
end

function c157.matfilter(c, syncard)
    return c:IsFaceup() and c:IsCanBeSynchroMaterial(syncard)
end
function c157.tunfilter(c, tp)
    return c:IsType(TYPE_TUNER) and c:IsType(TYPE_SYNCHRO) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and
               Duel.IsExistingMatchingCard(c157.ntunfilter, tp, LOCATION_MZONE, 0, 1, c, c:GetLevel())
end
function c157.ntunfilter(c, lv)
    return c:IsCode(44508094) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and not c:IsType(TYPE_TUNER) and
               c:GetLevel() + lv == 10
end
function c157.stunfilter(c)
    return c:IsType(TYPE_SYNCHRO) and c:IsFaceup()
end
function c157.sntunfilter(c)
    return c:IsCode(44508094) and c:IsFaceup()
end
function c157.syncondition(e, tp, eg, ep, ev, re, r, rp)
    return not e:GetHandler():IsStatus(STATUS_CHAINING) and Duel.GetFlagEffect(tp, 93999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.tunfilter, tp, LOCATION_MZONE, 0, 1, nil, tp) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 24696097)
end
function c157.synactivate(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 24696097) then
        return
    end
    local ag = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 100, nil, nil, nil, nil, nil, nil)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    local mg = Duel.GetMatchingGroup(c157.matfilter, tp, LOCATION_MZONE, 0, nil, g)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t1 = mg:FilterSelect(tp, c157.tunfilter, 1, 1, nil, tp)
    local m1 = t1:GetFirst()
    if m1 == nil then
        return
    end
    ag:AddCard(m1)
    local lv1 = m1:GetSynchroLevel(g)
    mg:RemoveCard(m1)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t2 = mg:FilterSelect(tp, c157.ntunfilter, 1, 1, nil, lv1)
    local m2 = t2:GetFirst()
    ag:AddCard(m2)
    if ag:GetCount() == 2 then
        g:SetMaterial(ag)
        if Duel.SendtoGrave(ag, REASON_MATERIAL + REASON_SYNCHRO) then
            Duel.SpecialSummon(g, SUMMON_TYPE_SYNCHRO, tp, tp, true, true, POS_FACEUP)
            g:CompleteProcedure()
            -- Duel.SynchroSummon(tp,g,nil)
            --g:RegisterFlagEffect(157, 0, 0, 1)
        end
    end
end

function c157.tunfilter2(c, tp)
    return c:IsType(TYPE_TUNER) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and c:IsCode(21159309) and
               Duel.IsExistingMatchingCard(c157.ntunfilter2, tp, LOCATION_MZONE, 0, 1, c, c:GetLevel())
end
function c157.ntunfilter2(c, lv)
    return c:IsCode(44508094) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and not c:IsType(TYPE_TUNER) and
               Duel.IsExistingMatchingCard(c157.ntunfilter22, tp, LOCATION_MZONE, 0, 1, c, c:GetLevel() + lv)
end
function c157.ntunfilter22(c, lv)
    return c:IsFaceup() and c:IsCanBeSynchroMaterial() and not c:IsType(TYPE_TUNER) and c:GetLevel() + lv == 10
end
function c157.syncondition2(e, tp, eg, ep, ev, re, r, rp)
    local xyztempg0=Duel.GetMatchingGroup(function(c) return c:GetFlagEffect(157)~=0 end, tp, LOCATION_REMOVED, 0, nil)
    return Duel.GetFlagEffect(tp, 93999980) ~= 0 
    and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() == tp 
    and Duel.IsExistingMatchingCard(function(c) return c:IsCode(7841112) and c:IsSynchroSummonable(nil) end, tp, 0x900, 0, 1, nil)
    and not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 7841112)
end
function c157.synactivate2(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 7841112) then
        return
    end
    local g = Duel.GetFirstMatchingCard(function(c) return c:IsCode(7841112) and c:IsSynchroSummonable(nil) end, tp, 0x900, 0, nil)
    --local g = Duel.CreateToken(tp, 7841112, nil, nil, nil, nil, nil, nil)
    --local mg = Duel.GetMatchingGroup(c157.matfilter, tp, LOCATION_MZONE, 0, nil, g)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    if not g then return end
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    Duel.SynchroSummon(tp,g,nil)
    -- Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    -- local t1 = mg:FilterSelect(tp, c157.tunfilter2, 1, 1, nil, tp)
    -- local m1 = t1:GetFirst()
    -- if m1 == nil then
    --     return
    -- end
    -- ag:AddCard(m1)
    -- local lv1 = m1:GetSynchroLevel(g)
    -- mg:RemoveCard(m1)
    -- Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    -- local t2 = mg:FilterSelect(tp, c157.ntunfilter2, 1, 1, nil, lv1)
    -- local m2 = t2:GetFirst()
    -- ag:AddCard(m2)
    -- local lv2 = lv1 + m2:GetSynchroLevel(g)
    -- mg:RemoveCard(m2)
    -- Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    -- local t3 = mg:FilterSelect(tp, c157.ntunfilter22, 1, 1, nil, lv2)
    -- ag:Merge(t3)
    -- if ag:GetCount() == 3 then
    --     g:SetMaterial(ag)
    --     if Duel.SendtoGrave(ag, REASON_MATERIAL + REASON_SYNCHRO) then
    --         Duel.SpecialSummon(g, SUMMON_TYPE_SYNCHRO, tp, tp, true, true, POS_FACEUP)
    --         g:CompleteProcedure()
    --         g:RegisterFlagEffect(157, 0, 0, 1)
    --     end
    -- end
end

function c157.tunfilter3(c, tp)
    return c:IsType(TYPE_TUNER) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and c:IsCode(21159309) and
               Duel.IsExistingMatchingCard(c157.ntunfilter3, tp, LOCATION_MZONE, 0, 1, c, c:GetLevel())
end
function c157.ntunfilter3(c, lv)
    return c:IsCode(70902743) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and not c:IsType(TYPE_TUNER) and
               Duel.IsExistingMatchingCard(c157.ntunfilter22, tp, LOCATION_MZONE, 0, 1, c, c:GetLevel() + lv)
end
function c157.syncondition3(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 93999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.tunfilter3, tp, LOCATION_MZONE, 0, 1, nil, tp) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 67030233)
end
function c157.synactivate3(e, tp, eg, ep, ev, re, r, rp)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 67030233) then
        return
    end
    local ag = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 513000078, nil, nil, nil, nil, nil, nil)
    local mg = Duel.GetMatchingGroup(c157.matfilter, tp, LOCATION_MZONE, 0, nil, g)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t1 = mg:FilterSelect(tp, c157.tunfilter3, 1, 1, nil, tp)
    local m1 = t1:GetFirst()
    if m1 == nil then
        return
    end
    ag:AddCard(m1)
    mg:RemoveCard(m1)
    local lv1 = m1:GetSynchroLevel(g)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t2 = mg:FilterSelect(tp, c157.ntunfilter3, 1, 1, nil, lv1)
    local m2 = t2:GetFirst()
    ag:AddCard(m2)
    mg:RemoveCard(m2)
    local lv2 = lv1 + m2:GetSynchroLevel(g)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t3 = mg:FilterSelect(tp, c157.ntunfilter22, 1, 1, nil, lv2)
    ag:Merge(t3)
    if ag:GetCount() == 3 then
        g:SetMaterial(ag)
        if Duel.SendtoGrave(ag, REASON_MATERIAL + REASON_SYNCHRO) then
            Duel.SpecialSummon(g, SUMMON_TYPE_SYNCHRO, tp, tp, true, true, POS_FACEUP)
            g:CompleteProcedure()
            --g:RegisterFlagEffect(157, 0, 0, 1)
        end
    end
end

function c157.tunfilter4(c, tp)
    return c:IsType(TYPE_TUNER) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and
               Duel.IsExistingMatchingCard(c157.tunfilter42, tp, LOCATION_MZONE, 0, 1, c, tp, c:GetLevel())
end
function c157.tunfilter42(c, tp, lv)
    return c:IsType(TYPE_TUNER) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and
               Duel.IsExistingMatchingCard(c157.ntunfilter4, tp, LOCATION_MZONE, 0, 1, c, lv + c:GetLevel())
end
function c157.ntunfilter4(c, lv)
    return c:IsCode(70902743) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and not c:IsType(TYPE_TUNER) and
               c:GetLevel() + lv == 10
end
function c157.syncondition4(e, tp, eg, ep, ev, re, r, rp, tuner)
    return Duel.GetFlagEffect(tp, 93999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.tunfilter4, tp, LOCATION_MZONE, 0, 1, nil, tp) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 97489701)
end
function c157.synactivate4(e, tp, eg, ep, ev, re, r, rp, tuner)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 97489701) then
        return
    end
    local ag = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 513000013, nil, nil, nil, nil, nil, nil)
    local mg = Duel.GetMatchingGroup(c157.matfilter, tp, LOCATION_MZONE, 0, nil, g)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t1 = mg:FilterSelect(tp, c157.tunfilter4, 1, 1, nil, tp)
    local m1 = t1:GetFirst()
    if m1 == nil then
        return
    end
    ag:AddCard(m1)
    mg:RemoveCard(m1)
    local lv1 = m1:GetSynchroLevel(g)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t2 = mg:FilterSelect(tp, c157.tunfilter42, 1, 1, nil, tp, lv1)
    local m2 = t2:GetFirst()
    ag:AddCard(m2)
    mg:RemoveCard(m2)
    local lv2 = lv1 + m2:GetSynchroLevel(g)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t3 = mg:FilterSelect(tp, c157.ntunfilter4, 1, 1, nil, lv2)
    ag:Merge(t3)
    if ag:GetCount() == 3 then
        g:SetMaterial(ag)
        if Duel.SendtoGrave(ag, REASON_MATERIAL + REASON_SYNCHRO) then
            Duel.SpecialSummon(g, SUMMON_TYPE_SYNCHRO, tp, tp, true, true, POS_FACEUP)
            g:CompleteProcedure()
            --g:RegisterFlagEffect(157, 0, 0, 1)
        end
    end
end

function c157.tunfilter5(c, tp)
    local g = Duel.GetMatchingGroup(c157.ntunfilter5, tp, LOCATION_MZONE, 0, nil)
    return c:IsType(TYPE_TUNER) and c:IsType(TYPE_SYNCHRO) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and
               g:CheckWithSumEqual(Card.GetLevel, 12 - c:GetLevel(), 2, 4)
end
function c157.ntunfilter5(c)
    return not c:IsType(TYPE_TUNER) and c:IsType(TYPE_SYNCHRO) and c:IsFaceup() and c:IsCanBeSynchroMaterial()
end
function c157.syncondition5(e, tp, eg, ep, ev, re, r, rp, tuner)
    return Duel.GetFlagEffect(tp, 93999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.tunfilter5, tp, LOCATION_MZONE, 0, 1, nil, tp) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 35952884)
end
function c157.synactivate5(e, tp, eg, ep, ev, re, r, rp, tuner)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 35952884) then
        return
    end
    local ag = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 101, nil, nil, nil, nil, nil, nil)
    local mg = Duel.GetMatchingGroup(c157.matfilter, tp, LOCATION_MZONE, 0, nil, g)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t1 = mg:FilterSelect(tp, c157.tunfilter5, 1, 1, nil, tp)
    local m1 = t1:GetFirst()
    if m1 == nil then
        return
    end
    ag:AddCard(m1)
    mg:RemoveCard(m1)
    local lv1 = m1:GetSynchroLevel(g)
    local mg2 = mg:Filter(c157.ntunfilter5, m1)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local m3 = mg2:SelectWithSumEqual(tp, Card.GetLevel, 12 - lv1, 2, 4)
    ag:Merge(m3)
    if ag:GetCount() > 2 then
        g:SetMaterial(ag)
        if Duel.SendtoGrave(ag, REASON_MATERIAL + REASON_SYNCHRO) then
            Duel.SpecialSummon(g, SUMMON_TYPE_SYNCHRO, tp, tp, true, true, POS_FACEUP)
            g:CompleteProcedure()
            --g:RegisterFlagEffect(157, 0, 0, 1)
        end
    end
end

function c157.syncondition6(e, tp, eg, ep, ev, re, r, rp, tuner)
    return Duel.GetFlagEffect(tp, 93999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.tunfilter5, tp, LOCATION_MZONE, 0, 1, nil, tp) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 26268488) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 511000774)
end
function c157.synactivate6(e, tp, eg, ep, ev, re, r, rp, tuner)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 26268488) or
        Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 511000774) then
        return
    end
    local ag = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 511000774, nil, nil, nil, nil, nil, nil)
    local mg = Duel.GetMatchingGroup(c157.matfilter, tp, LOCATION_MZONE, 0, nil, g)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t1 = mg:FilterSelect(tp, c157.tunfilter5, 1, 1, nil, tp)
    local m1 = t1:GetFirst()
    if m1 == nil then
        return
    end
    ag:AddCard(m1)
    mg:RemoveCard(m1)
    local lv1 = m1:GetSynchroLevel(g)
    local mg2 = mg:Filter(c157.ntunfilter5, m1)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local m3 = mg2:SelectWithSumEqual(tp, Card.GetLevel, 12 - lv1, 2, 4)
    ag:Merge(m3)
    if ag:GetCount() > 2 then
        g:SetMaterial(ag)
        if Duel.SendtoGrave(ag, REASON_MATERIAL + REASON_SYNCHRO) then
            Duel.SpecialSummon(g, SUMMON_TYPE_SYNCHRO, tp, tp, true, true, POS_FACEUP)
            g:CompleteProcedure()
            --g:RegisterFlagEffect(157, 0, 0, 1)
        end
    end
end

function c157.tunfilter7(c, tp)
    return c:IsType(TYPE_TUNER) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and
               Duel.IsExistingMatchingCard(c157.ntunfilter7, tp, LOCATION_MZONE, 0, 1, c, c:GetLevel())
end
function c157.ntunfilter7(c, lv)
    return not c:IsType(TYPE_TUNER) and c:IsCode(2403771) and c:IsFaceup() and c:IsCanBeSynchroMaterial() and
               c:GetLevel() + lv == 8
end
function c157.syncondition7(e, tp, eg, ep, ev, re, r, rp, tuner)
    return Duel.GetFlagEffect(tp, 93999980) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnPlayer() ==
               tp and Duel.IsExistingMatchingCard(c157.tunfilter7, tp, LOCATION_MZONE, 0, 1, nil, tp) and
               not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 25165047)
end
function c157.synactivate7(e, tp, eg, ep, ev, re, r, rp, tuner)
    if Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 25165047) then
        return
    end
    local ag = Group.CreateGroup()
    local g = Duel.CreateToken(tp, 169, nil, nil, nil, nil, nil, nil)
    local mg = Duel.GetMatchingGroup(c157.matfilter, tp, LOCATION_MZONE, 0, nil, g)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t1 = mg:FilterSelect(tp, c157.tunfilter7, 1, 1, nil, tp)
    local m1 = t1:GetFirst()
    if m1 == nil then
        return
    end
    ag:AddCard(m1)
    mg:RemoveCard(m1)
    local lv1 = m1:GetSynchroLevel(g)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SMATERIAL)
    local t2 = mg:FilterSelect(tp, c157.ntunfilter7, 1, 1, nil, lv1)
    local m2 = t2:GetFirst()
    ag:AddCard(m2)
    if ag:GetCount() == 2 then
        g:SetMaterial(ag)
        if Duel.SendtoGrave(ag, REASON_MATERIAL + REASON_SYNCHRO) then
            Duel.SpecialSummon(g, SUMMON_TYPE_SYNCHRO, tp, tp, true, true, POS_FACEUP)
            g:CompleteProcedure()
            --g:RegisterFlagEffect(157, 0, 0, 1)
        end
    end
end

function c157.condition32(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetTurnCount() ~= 1 and tp == Duel.GetTurnPlayer() and
               Duel.IsExistingMatchingCard(c157.rmhfiler, tp, LOCATION_HAND, 0, 1, nil) and Duel.GetLP(tp) <= 2000 and
               Duel.GetFlagEffect(tp, 91999980) ~= 0 and Duel.GetFlagEffect(tp, 91999980) < 4
end
function c157.rmhfiler(c)
    return c:IsSetCard(0x95)
end
function c157.activate32(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
    local g = Duel.SelectMatchingCard(tp, c157.rmhfiler, tp, LOCATION_HAND, 0, 1, 1, nil)
    local tc = g:GetFirst()
    c157.announce_filter = {0x95, OPCODE_ISSETCARD}
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE) 
    local code = Duel.AnnounceCard(tp, 0x95, OPCODE_ISSETCARD,OPCODE_ALLOW_ALIASES)
    local a = Duel.CreateToken(tp, code)
    local ocode = a:GetOriginalCodeRule()
    tc:SetEntityCode(code, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
    -- tc:ReplaceEffect(code,0)
    -- tc:SetCardData(CARDDATA_ALIAS,ocode)

    if Duel.GetFlagEffect(tp, 89999997) ~= 0 and Duel.GetFlagEffect(ttp, 91999980) == 1 then
        Duel.Hint(HINT_CARD, 0, 510000103)
    end
    if Duel.GetFlagEffect(tp, 89999997) ~= 0 and Duel.GetFlagEffect(ttp, 91999980) == 2 then
        Duel.Hint(HINT_CARD, 0, 510000104)
    end
    if Duel.GetFlagEffect(tp, 89999997) ~= 0 and Duel.GetFlagEffect(ttp, 91999980) == 3 then
        Duel.Hint(HINT_CARD, 0, 510000105)
    end
    Duel.Hint(HINT_MESSAGE, 1 - tp, aux.Stringid(111011901, 2))
    Duel.RegisterFlagEffect(tp, 91999980, 0, 0, 1)
end

function c157.actcon(e, tp, eg, ep, ev, re, r, rp)
    return re:GetHandler() == e:GetHandler()
end

function c157.usefilter(c)
    return c:IsType(TYPE_LINK) and c:IsType(TYPE_SPELL) and c:CheckActivateEffect(false, false, false) ~= nil
end
function c157.lmCondition(e, tp, eg, ep, ev, re, r, rp)
    local ttp = e:GetHandler():GetControler()
    return Duel.GetFlagEffect(ttp, 784) ~= 0 and Duel.GetLP(ttp) <= Duel.GetFlagEffectLabel(ttp, 784) and
               Duel.GetMatchingGroupCount(c157.usefilter, ttp, LOCATION_DECK, 0, nil) > 0
end
function c157.lmOperation(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local ttp = e:GetHandler():GetControler()
    if Duel.GetFlagEffect(ttp, 784) ~= 0 and Duel.GetLP(ttp) <= Duel.GetFlagEffectLabel(ttp, 784) and
        Duel.GetMatchingGroupCount(c157.usefilter, ttp, LOCATION_DECK, 0, nil) > 0 then
        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(826, 5))
        local g = Duel.GetMatchingGroup(c157.usefilter, ttp, LOCATION_DECK, 0, nil)
        Duel.ConfirmCards(ttp, g)
        local g2 = g:Select(ttp, 1, 1, nil)
        local tc = g2:GetFirst()
        if tc then
            local te, eg, ep, ev, re, r, rp = tc:CheckActivateEffect(true, true, true)
            local tep = tc:GetControler()
            local condition = te:GetCondition()
            local operation = te:GetOperation()
            if not Duel.MoveToField(tc, ttp, ttp, LOCATION_SZONE, POS_FACEUP, true) then
                return
            end
            Duel.Hint(HINT_CARD, 0, tc:GetOriginalCode())
            tc:CreateEffectRelation(te)
            Duel.BreakEffect()
            if operation then
                operation(te, tep, eg, ep, ev, re, r, rp)
            end
            tc:ReleaseEffectRelation(te)

            Duel.ShuffleDeck(ttp)

            local e1 = Effect.CreateEffect(c)
            e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_SINGLE_RANGE)
            e1:SetType(EFFECT_TYPE_SINGLE)
            e1:SetRange(LOCATION_SZONE)
            e1:SetCode(EFFECT_CANNOT_DISABLE)
            e1:SetReset(RESET_EVENT + RESETS_STANDARD)
            tc:RegisterEffect(e1, true)
            local e2 = e1:Clone()
            e2:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
            tc:RegisterEffect(e2, true)
        end
    end
end

function c157.lusefilter(c)
    return c:IsSetCard(0x577) and c:IsType(TYPE_LINK)
end
function c157.lmCondition2(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 784) ~= 0 and Duel.GetMatchingGroupCount(c157.lusefilter, tp, 0xff, 0, nil) > 0
end
function c157.lmOperation2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if Duel.SelectYesNo(tp, aux.Stringid(35772782, 1)) then
        local dc = {Duel.GetDiceResult()}
        local ac = 1
        local val, val2
        local ct = bit.band(ev, 0xff) + bit.rshift(ev, 16)
        Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(35772782, 2))
        val = Duel.AnnounceNumber(tp, 1, 2, 3, 4, 5, 6)
        -- if ct>1 then
        -- Duel.Hint(HINT_SELECTMSG,tp,aux.Stringid(35772782,2))
        -- val2=Duel.AnnounceNumber(tp,1,2,3,4,5,6)
        -- end
        dc[0] = val
        -- dc[1]=val2
        Duel.SetDiceResult(val)
    end
end

function c157.lmCondition3(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 784) ~= 0 and
               (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
end
function c157.lmOperation3(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    Duel.Hint(HINT_MESSAGE, 1 - tp, aux.Stringid(826, 8))
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE) 
    local code = Duel.AnnounceCard(tp, TYPE_LINK, OPCODE_ISTYPE, RACE_CYBERSE, OPCODE_ISRACE, OPCODE_AND,OPCODE_ALLOW_ALIASES)
    local token = Duel.CreateToken(tp, code)
    if token:IsRace(RACE_CYBERSE) and token:IsType(TYPE_LINK) then
        Duel.SendtoDeck(token, tp, 0, REASON_RULE)
    end
end

function c157.lmCondition4(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 784) ~= 0 and Duel.GetLP(tp) <= 1000
end
function c157.lmOperation4(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    Duel.Hint(HINT_MESSAGE, 1 - tp, aux.Stringid(826, 9))
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CODE) 
    local code = Duel.AnnounceCard(tp, TYPE_LINK, OPCODE_ISTYPE,OPCODE_ALLOW_ALIASES)
    local token = Duel.CreateToken(tp, code)
    if token:IsType(TYPE_LINK) then
        Duel.SendtoDeck(token, tp, 0, REASON_RULE)
    end
end

-- function c157.adfilter(c)
--     return c:GetFlagEffect(135) == 0 and c:IsCode(24094653)
-- end
-- function c157.adtg(e, tp, eg, ep, ev, re, r, rp, chk)
--     if chk == 0 then
--         return
--             Duel.IsExistingMatchingCard(c157.adfilter, tp, 0xff, 0xff, 1, nil) and Duel.GetFlagEffect(0, 135) == 0 and
--                 Duel.GetFlagEffect(1, 135) == 0
--     end
--     local c = e:GetHandler()
--     Duel.RegisterFlagEffect(0, 135, 0, 0, 0)
--     Duel.RegisterFlagEffect(1, 135, 0, 0, 0)
--     local g = Duel.GetMatchingGroup(c157.adfilter, tp, 0xff, 0xff, nil)
--     for tc in aux.Next(g) do
--         local e1 = Effect.CreateEffect(c)
--         e1:SetCategory(CATEGORY_SPECIAL_SUMMON + CATEGORY_FUSION_SUMMON)
--         e1:SetType(EFFECT_TYPE_ACTIVATE)
--         e1:SetCode(EVENT_FREE_CHAIN)
--         e1:SetDescription(aux.Stringid(13713, 7))
--         e1:SetTarget(c157.target11)
--         e1:SetOperation(c157.activate11)
--         tc:RegisterEffect(e1, true)
--         tc:RegisterFlagEffect(135, 0, 0, 0)
--     end
-- end
-- function c157.adtg2(e, tp, eg, ep, ev, re, r, rp, chk)
--     if chk == 0 then
--         return Duel.IsExistingMatchingCard(c157.adfilter, tp, 0xff, 0xff, 1, nil)
--     end
--     local c = e:GetHandler()
--     local g = Duel.GetMatchingGroup(c157.adfilter, tp, 0xff, 0xff, nil)
--     for tc in aux.Next(g) do
--         local e1 = Effect.CreateEffect(c)
--         e1:SetCategory(CATEGORY_SPECIAL_SUMMON + CATEGORY_FUSION_SUMMON)
--         e1:SetType(EFFECT_TYPE_ACTIVATE)
--         e1:SetCode(EVENT_FREE_CHAIN)
--         e1:SetDescription(aux.Stringid(13713, 7))
--         e1:SetTarget(c157.target11)
--         e1:SetOperation(c157.activate11)
--         tc:RegisterEffect(e1, true)
--         tc:RegisterFlagEffect(135, 0, 0, 0)
--     end
-- end
-- function c157.filter1(c, e)
--     return not c:IsImmuneToEffect(e)
-- end
-- function c157.fusfilter(c)
--     local code = c:GetCode()
--     return code == 79856792 or code == 79407975
-- end
-- function c157.target11(e, tp, eg, ep, ev, re, r, rp, chk)
--     local chkf = Duel.GetLocationCount(tp, LOCATION_MZONE)
--     local mg = Duel.GetMatchingGroup(c157.filter1, tp, LOCATION_MZONE + LOCATION_HAND, 0, nil, e)
--     local g11 = Group.CreateGroup()
--     g12 = Group.CreateGroup()
--     g16 = Group.CreateGroup()
--     local b1 = (mg:FilterCount(Card.IsCode, nil, 89943723) > 0 and mg:FilterCount(Card.IsCode, nil, 78371393) > 0 and
--                    not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 5126490))
--     if b1 then
--         g11 = mg:Filter(Card.IsCode, nil, 89943723)
--         local g12 = mg:Filter(Card.IsCode, nil, 78371393)
--         g11:Merge(g12)
--         if chkf > 0 or g11:FilterCount(Card.IsLocation, nil, LOCATION_MZONE) > 0 then
--             b1 = true
--         else
--             b1 = false
--         end
--     end
--     local b2 = (mg:FilterCount(Card.IsCode, nil, 89943723) > 0 and mg:FilterCount(c157.fusfilter, nil) > 0 and
--                    not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 86346643))
--     if b2 then
--         g12 = mg:Filter(Card.IsCode, nil, 89943723)
--         local g22 = mg:Filter(c157.fusfilter, nil)
--         g12:Merge(g22)
--         if chkf > 0 or g12:FilterCount(Card.IsLocation, nil, LOCATION_MZONE) > 0 then
--             b2 = true
--         else
--             b2 = false
--         end
--     end
--     local b6 = (mg:FilterCount(Card.IsCode, nil, 57116033) > 0 and mg:FilterCount(Card.IsCode, nil, 47297616) > 0 and
--                    not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 18631392))
--     if b6 then
--         g16 = mg:Filter(Card.IsCode, nil, 57116033)
--         local g26 = mg:Filter(Card.IsCode, nil, 47297616)
--         g16:Merge(g26)
--         if chkf > 0 or g16:FilterCount(Card.IsLocation, nil, LOCATION_MZONE) > 0 then
--             b6 = true
--         else
--             b6 = false
--         end
--     end
--     if chk == 0 then
--         return (b1 or b2 or b6) and Duel.GetFlagEffect(tp, 92999980) ~= 0
--     end
--     Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_EXTRA)
--     Duel.SetChainLimit(aux.FALSE)
-- end
-- function c157.activate11(e, tp, eg, ep, ev, re, r, rp)
--     local chkf = Duel.GetLocationCount(tp, LOCATION_MZONE)
--     local mg = Duel.GetMatchingGroup(c157.filter1, tp, LOCATION_MZONE + LOCATION_HAND, 0, nil, e)
--     local g11 = Group.CreateGroup()
--     g12 = Group.CreateGroup()
--     g16 = Group.CreateGroup()
--     local b1 = (mg:FilterCount(Card.IsCode, nil, 89943723) > 0 and mg:FilterCount(Card.IsCode, nil, 78371393) > 0 and
--                    not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 5126490))
--     if b1 then
--         g11 = mg:Filter(Card.IsCode, nil, 89943723)
--         local g12 = mg:Filter(Card.IsCode, nil, 78371393)
--         g11:Merge(g12)
--         if chkf > 0 or g11:FilterCount(Card.IsLocation, nil, LOCATION_MZONE) > 0 then
--             b1 = true
--         else
--             b1 = false
--         end
--     end
--     local b2 = (mg:FilterCount(Card.IsCode, nil, 89943723) > 0 and mg:FilterCount(c157.fusfilter, nil) > 0 and
--                    not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 86346643))
--     if b2 then
--         g12 = mg:Filter(Card.IsCode, nil, 89943723)
--         local g22 = mg:Filter(c157.fusfilter, nil)
--         g12:Merge(g22)
--         if chkf > 0 or g12:FilterCount(Card.IsLocation, nil, LOCATION_MZONE) > 0 then
--             b2 = true
--         else
--             b2 = false
--         end
--     end
--     local b6 = (mg:FilterCount(Card.IsCode, nil, 57116033) > 0 and mg:FilterCount(Card.IsCode, nil, 47297616) > 0 and
--                    not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, 18631392))
--     if b6 then
--         g16 = mg:Filter(Card.IsCode, nil, 57116033)
--         local g26 = mg:Filter(Card.IsCode, nil, 47297616)
--         g16:Merge(g26)
--         if chkf > 0 or g16:FilterCount(Card.IsLocation, nil, LOCATION_MZONE) > 0 then
--             b6 = true
--         else
--             b6 = false
--         end
--     end
--     if not (b1 or b2 or b6) then
--         return
--     end
--     local tc
--     local off = 1
--     local ops = {}
--     local opval = {}
--     if b1 then
--         ops[off] = aux.Stringid(13713, 1)
--         opval[off - 1] = 1
--         off = off + 1
--     end
--     if b2 then
--         ops[off] = aux.Stringid(13713, 2)
--         opval[off - 1] = 2
--         off = off + 1
--     end
--     if b6 then
--         ops[off] = aux.Stringid(13713, 6)
--         opval[off - 1] = 3
--         off = off + 1
--     end
--     local op = Duel.SelectOption(tp, table.unpack(ops))
--     local g1 = Group.CreateGroup()
--     local mgg = Group.CreateGroup()
--     local chkf2 = Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and PLAYER_NONE or tp
--     if opval[op] == 1 then
--         tc = Duel.CreateToken(tp, 45, nil, nil, nil, nil, nil, nil)
--         -- Duel.Remove(tc,POS_FACEUP,REASON_RULE)
--         Duel.SendtoDeck(tc, tp, 0, REASON_RULE)
--         g1 = mg:Filter(Card.IsCode, nil, 89943723)
--         local g12 = mg:Filter(Card.IsCode, nil, 78371393)
--         g1:Merge(g12)
--         Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FMATERIAL)
--         local mgg = Duel.SelectFusionMaterial(tp, tc, g1, nil, chkf2)
--         tc:SetMaterial(mgg)
--         Duel.SendtoGrave(mgg, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
--         Duel.BreakEffect()
--         Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, true, POS_FACEUP)
--         tc:CompleteProcedure()
--         tc:RegisterFlagEffect(157, 0, 0, 1)
--     end

--     if opval[op] == 2 then
--         tc = Duel.CreateToken(tp, 86346643, nil, nil, nil, nil, nil, nil)
--         -- Duel.Remove(tc,POS_FACEUP,REASON_RULE)
--         Duel.SendtoDeck(tc, tp, 0, REASON_RULE)
--         g1 = mg:Filter(Card.IsCode, nil, 89943723)
--         local g12 = mg:Filter(c157.fusfilter, nil)
--         g1:Merge(g12)
--         Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FMATERIAL)
--         local mgg = Duel.SelectFusionMaterial(tp, tc, g1, nil, chkf2)
--         tc:SetMaterial(mgg)
--         Duel.SendtoGrave(mgg, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
--         Duel.BreakEffect()
--         Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, true, POS_FACEUP)
--         tc:CompleteProcedure()
--         tc:RegisterFlagEffect(157, 0, 0, 1)
--     end

--     if opval[op] == 3 then
--         tc = Duel.CreateToken(tp, 201, nil, nil, nil, nil, nil, nil)
--         -- Duel.Remove(tc,POS_FACEUP,REASON_RULE)
--         Duel.SendtoDeck(tc, tp, 0, REASON_RULE)
--         g1 = mg:Filter(Card.IsCode, nil, 57116033)
--         local g12 = mg:Filter(Card.IsCode, nil, 47297616)
--         g1:Merge(g12)
--         Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FMATERIAL)
--         local mgg = Duel.SelectFusionMaterial(tp, tc, g1, nil, chkf2)
--         tc:SetMaterial(mgg)
--         Duel.SendtoGrave(mgg, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
--         Duel.BreakEffect()
--         Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, true, POS_FACEUP)
--         tc:CompleteProcedure()
--         tc:RegisterFlagEffect(157, 0, 0, 1)
--     end
-- end

function c157.phantomCondition(e, tp, eg, ep, ev, re, r, rp)
    local ttp = e:GetHandler():GetControler()
    return Duel.GetFlagEffect(ttp, 10000004) ~= 0 and
               (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 69890967) > 0 or
                   Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 32491822) > 0 or
                   Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 6007213) > 0) and
               Duel.GetMatchingGroupCount(nil, ttp, LOCATION_HAND, 0, nil) > 0
end
function c157.phantomOperation(e, tp, eg, ep, ev, re, r, rp)
    local ttp = e:GetHandler():GetControler()
    if Duel.GetFlagEffect(ttp, 10000004) ~= 0 and
        (Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 69890967) > 0 or
            Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 32491822) > 0 or
            Duel.GetMatchingGroupCount(Card.IsCode, ttp, LOCATION_DECK, 0, nil, 6007213) > 0) and
        Duel.GetMatchingGroupCount(nil, ttp, LOCATION_HAND, 0, nil) > 0 then
        Duel.Hint(HINT_MESSAGE, 1 - ttp, aux.Stringid(826, 10))
        Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
        local g1 = Duel.SelectMatchingCard(ttp, Card.IsCode, ttp, LOCATION_DECK, 0, 1, 1, nil, 69890967, 32491822,
                       6007213):GetFirst()             
        local type = 0
        if g1:IsCode(69890967) then
            type = TYPE_MONSTER
        end
        if g1:IsCode(32491822) then
            type = TYPE_SPELL
        end
        if g1:IsCode(6007213) then
            type = TYPE_TRAP
        end
        Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
        local g2 = Duel.SelectMatchingCard(ttp, nil, ttp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
        Duel.ShuffleDeck(ttp)
        aux.SwapEntity(g1, g2)
        -- local code1 = g1:GetOriginalCode()
        -- local code2 = g2:GetOriginalCode()
        -- local seq2 = g2:GetSequence()
        -- local seq = g1:GetSequence()
        -- Duel.Exile(g2, REASON_RULE)
        -- Duel.Exile(g1, REASON_RULE)
        -- g1 = Duel.CreateToken(ttp, code1)
        -- g2 = Duel.CreateToken(ttp, code2)
        -- Duel.DisableShuffleCheck()
        -- Duel.SendtoHand(g1, ttp, REASON_RULE)
        -- Duel.BreakEffect()
        -- Duel.DisableShuffleCheck()
        -- Duel.SendtoDeck(g2, ttp, 0, REASON_RULE)
        -- Duel.BreakEffect()
        -- Duel.MoveSequence(g2, seq)
        if Duel.GetMatchingGroupCount(nil, ttp, LOCATION_HAND, 0, nil) > 0 and
            Duel.GetMatchingGroupCount(Card.IsType, ttp, LOCATION_DECK, 0, nil, type) > 0 and
            Duel.SelectYesNo(ttp, aux.Stringid(826, 10)) then
            Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
            local g1 = Duel.SelectMatchingCard(ttp, Card.IsType, ttp, LOCATION_DECK, 0, 1, 1, nil, type):GetFirst()
            Duel.Hint(HINT_SELECTMSG, ttp, HINTMSG_TARGET)
            local g2 = Duel.SelectMatchingCard(ttp, nil, ttp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
            Duel.ShuffleDeck(ttp)
            aux.SwapEntity(g1, g2)
            -- local seq2 = g2:GetSequence()
            -- local seq = g1:GetSequence()
            -- local code1 = g1:GetOriginalCode()
            -- local code2 = g2:GetOriginalCode()
            -- Duel.Exile(g2, REASON_RULE)
            -- Duel.Exile(g1, REASON_RULE)
            -- g1 = Duel.CreateToken(ttp, code1)
            -- g2 = Duel.CreateToken(ttp, code2)
            -- Duel.SendtoHand(g1, ttp, REASON_RULE)
            -- Duel.BreakEffect()
            -- Duel.SendtoDeck(g2, ttp, 0, REASON_RULE)
            -- Duel.BreakEffect()
            -- Duel.MoveSequence(g2, seq)
        end
    end
end

function c157.yugicon(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(tp, 90799980) ~= 0 and not e:GetHandler():IsStatus(STATUS_CHAINING) and
               re:IsHasCategory(CATEGORY_TOHAND + CATEGORY_SEARCH) and rp == tp
end
function c157.yugiop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TARGET)
    local destinytc = Duel.SelectMatchingCard(tp, nil, tp, LOCATION_DECK, 0, 1, 1, nil):GetFirst()
    Duel.ShuffleDeck(ttp)
    local tac2=Duel.GetDecktopGroup(ttp, 1):GetFirst()
    if not tac2 then return end
    aux.SwapEntity(destinytc, tac2)      
    --Duel.MoveSequence(destinytc, 0)
end

function c157.ovfilter85(c, tp)
    local code = 0
    if c:IsCode(84013237) then
        code = 56840427
    end
    if c:IsCode(65676461) then
        code = 49221191
    end
    return c:IsFaceup() and c:IsType(TYPE_XYZ) 
    and c:IsSetCard(0x48) and not c:IsSetCard(0x1048) and not c:IsSetCard(0x2048) and not c:IsCode(65, 326, 364, 493, 52653092, 56832966, 86532744)
    and (not c:IsCode(84013237, 65676461) or
        not Duel.IsExistingMatchingCard(aux.overfilter, tp, 0xff, LOCATION_ONFIELD, 1, nil, tp, code))
    and Duel.GetLocationCountFromEx(tp,tp,c,TYPE_XYZ)>0    
end
function c157.xyzcondition85(e, tp, eg, ep, ev, re, r, rp)
    return (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) 
    and Duel.GetTurnPlayer() == tp and Duel.GetLP(tp) <= 1000 
    and Duel.IsExistingMatchingCard(c157.ovfilter85, tp, LOCATION_MZONE, 0, 1, nil, tp)
end
function c157.xyzactivate85(e, tp, eg, ep, ev, re, r, rp)
    if not Duel.IsExistingMatchingCard(c157.ovfilter85, tp, LOCATION_MZONE, 0, 1, nil, tp) or Duel.GetLP(tp) > 1000 then
        return
    end
    local mg = Duel.GetMatchingGroup(c157.ovfilter85, tp, LOCATION_MZONE, 0, nil, tp)
    if mg:GetCount() < 1 then
        return
    end
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
    local ng = mg:FilterSelect(tp, c157.ovfilter85, 1, 1, nil, tp)
    if ng:GetCount() ~= 1 then
        return
    end
    local code = 28
    if ng:GetFirst():IsCode(84013237) then
        code = 61
    end
    if ng:GetFirst():IsCode(65676461) then
        code = 511010032
    end
    local g = Duel.CreateToken(tp, code, nil, nil, nil, nil, nil, nil)
    -- Duel.Remove(g,POS_FACEUP,REASON_RULE)  
    Duel.SendtoDeck(g, tp, 0, REASON_RULE)
    local og = ng:GetFirst():GetOverlayGroup()
    if og:GetCount() ~= 0 then
        Duel.Overlay(g, og)
    end
    g:SetMaterial(ng)
    Duel.Overlay(g, ng)
    Duel.SpecialSummonStep(g, SUMMON_TYPE_XYZ, tp, tp, true, true, POS_FACEUP)
    if code == 28 then
        aux.chaos(g, ng:GetFirst())
    end
    Duel.SpecialSummonComplete()
    g:CompleteProcedure()
end

function aux.chaos(xyz, tc)
    local code = tc:GetOriginalCodeRule()
    xyz:SetEntityCode(code, 28, {tc:GetOriginalSetCard(), xyz:GetOriginalSetCard()}, nil, nil, nil, nil, nil, nil, nil, nil, nil, false)
    -- self destroy
    local e8 = Effect.CreateEffect(xyz)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetCode(EFFECT_DISABLE)
    e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e8:SetRange(LOCATION_MZONE)
    e8:SetCondition(function(...)
        return Duel.GetLP(xyz:GetControler()) > 1000
    end)
    xyz:RegisterEffect(e8, true)
    local e82 = e8:Clone()
    e82:SetCode(EFFECT_DISABLE_EFFECT)
    xyz:RegisterEffect(e82, true)
    -- atk/def
    local e9 = Effect.CreateEffect(xyz)
    e9:SetDescription(aux.Stringid(49221191, 1))
    e9:SetCategory(CATEGORY_ATKCHANGE)
    e9:SetProperty(EFFECT_FLAG_CARD_TARGET + EFFECT_FLAG_DAMAGE_STEP)
    e9:SetType(EFFECT_TYPE_QUICK_O)
    e9:SetCode(EVENT_FREE_CHAIN)
    e9:SetRange(LOCATION_MZONE)
    e9:SetHintTiming(TIMING_DAMAGE_STEP)
    e9:SetCondition(function(...)
        return (Duel.GetCurrentPhase() ~= PHASE_DAMAGE or not Duel.IsDamageCalculated())
    end)
    e9:SetCost(function(e, tp, eg, ep, ev, re, r, rp, chk)
        if chk == 0 then
            return e:GetHandler():CheckRemoveOverlayCard(tp, 1, REASON_COST) and Duel.IsExistingMatchingCard(
                       function(c)
                    return c:IsType(TYPE_MONSTER) and c:IsAttribute(xyz:GetAttribute()) and c:IsAbleToRemoveAsCost()
                end, tp, LOCATION_GRAVE, 0, 1, nil)
        end
        e:GetHandler():RemoveOverlayCard(tp, 1, 1, REASON_COST)
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
        local g = Duel.SelectMatchingCard(tp, function(c)
            return c:IsType(TYPE_MONSTER) and c:IsAttribute(xyz:GetAttribute()) and c:IsAbleToRemoveAsCost()
        end, tp, LOCATION_GRAVE, 0, 1, 1, nil)
        local atk = g:GetFirst():GetAttack()
        e:SetLabel(atk)
        Duel.Remove(g, POS_FACEUP, REASON_COST)
    end)
    e9:SetTarget(function(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
        if chkc then
            return chkc:IsLocation(LOCATION_MZONE) and c233.filter(chkc)
        end
        if chk == 0 then
            return Duel.IsExistingTarget(aux.rfilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil)
        end
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
        Duel.SelectTarget(tp, aux.rfilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, nil)
    end)
    e9:SetOperation(function(e, tp, eg, ep, ev, re, r, rp)
        local tc = Duel.GetFirstTarget()
        local atk = e:GetLabel()
        if tc:IsRelateToEffect(e) and tc:IsFaceup() then
            local e1 = Effect.CreateEffect(e:GetHandler())
            e1:SetType(EFFECT_TYPE_SINGLE)
            e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
            e1:SetCode(EFFECT_UPDATE_ATTACK)
            e1:SetValue(-atk)
            tc:RegisterEffect(e1)
        end
    end)
    xyz:RegisterEffect(e9, true, REGISTER_FLAG_DETACH_XMAT)
end

function aux.rfilter(c)
    return c:IsFaceup() and c:GetAttack() > 0
end