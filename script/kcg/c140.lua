--E－HERO ダーク·ガイア
function c140.initial_effect(c)
	--fusion material
	c:EnableReviveLimit()
	Fusion.AddProcFun2(c,aux.FilterBoolFunction(Card.IsType,TYPE_MONSTER),aux.FilterBoolFunction(Card.IsType,TYPE_MONSTER),true)
	  --fusion material
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetType(EFFECT_TYPE_SINGLE)
	-- e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	-- e1:SetCode(EFFECT_FUSION_MATERIAL)
	-- e1:SetCondition(c140.fscondition)
	-- e1:SetOperation(c140.fsoperation)
	--c:RegisterEffect(e1)

	  --spsummon condition
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e2:SetCode(EFFECT_SPSUMMON_CONDITION)
	e2:SetValue(c140.splimit)
	--c:RegisterEffect(e2)

	--atk
	local e3=Effect.CreateEffect(c)   
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	e3:SetOperation(c140.atkop)
	c:RegisterEffect(e3)
end
c140.material_setcode=0x8
c140.dark_calling=true

-- function c140.fscondition(e,mg,gc)
-- 	if mg==nil then return false end
-- 	if gc then return false end
-- 	return mg~=nil and Duel.IsExistingMatchingCard(c140.filter1,tp,LOCATION_MZONE,LOCATION_MZONE,2,nil,e,tp) 
-- end
-- function c140.filter1(c,e,tp)
-- 		return c:IsCanBeFusionMaterial() and not c:IsImmuneToEffect(e) and (c:IsControler(tp) or c:IsFaceup())
-- end
-- function c140.fsoperation(e,tp,eg,ep,ev,re,r,rp,gc)
-- 	--Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
-- 	--local g1=eg:FilterSelect(tp,c140.spfilter,1,1,nil,eg)
-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
-- 	local g1=eg:FilterSelect(tp,c140.filter1,2,126,nil,e,tp)
-- 	Duel.SetFusionMaterial(g1)
-- end

function c140.splimit(e,se,sp,st)
	return bit.band(st,SUMMON_TYPE_FUSION+0x20)~=0
end
function c140.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=c:GetMaterial()
	local s=0
	local tc=g:GetFirst()
	  local att=tc:GetOriginalAttribute()
	  local race=tc:GetOriginalRace()
	while tc do
		local code=tc:GetOriginalCode()
		local a=tc:GetAttack()
		if a<0 then a=0 end
		s=s+a
	c:CopyEffect(code,RESET_EVENT+EVENT_TO_DECK+RESET_DISABLE,1)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_ADD_ATTRIBUTE)
	e1:SetValue(att)  
	e1:SetReset(RESET_EVENT+EVENT_TO_DECK+RESET_DISABLE) 
	c:RegisterEffect(e1) 
	local e3=e1:Clone()
	e3:SetCode(EFFECT_ADD_RACE)
	e3:SetValue(race)   
	e3:SetReset(RESET_EVENT+EVENT_TO_DECK+RESET_DISABLE)
	c:RegisterEffect(e3) 
	tc=g:GetNext()
	end
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetValue(s)
	e1:SetReset(RESET_EVENT+EVENT_TO_DECK+RESET_DISABLE)
	c:RegisterEffect(e1)
end
function c140.postg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(Card.IsDEFENSEPos,tp,0,LOCATION_MZONE,1,nil) end
	local g=Duel.GetMatchingGroup(Card.IsDEFENSEPos,tp,0,LOCATION_MZONE,nil)
	Duel.SetOperationInfo(0,CATEGORY_POSITION,g,g:GetCount(),0,0)
end
function c140.posop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(Card.IsDEFENSEPos,tp,0,LOCATION_MZONE,nil)
	Duel.ChangePosition(g,0,0,POS_FACEUP_ATTACK,POS_FACEUP_ATTACK,true)
end
