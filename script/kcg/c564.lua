--奧雷卡爾克斯奇甲之盾星 (KA)
function c564.initial_effect(c)
	--synchro summon
	--synchro summon
	Synchro.AddProcedure(c,aux.FilterBoolFunctionEx(Card.IsSetCard,0x900),1,1,Synchro.NonTunerEx(Card.IsSetCard,0x900),1,99)
	c:EnableReviveLimit()
	
	--immue a
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_IMMUNE_EFFECT)
	e1:SetRange(LOCATION_ONFIELD)
	e1:SetTargetRange(LOCATION_ONFIELD,0)
	e1:SetTarget(c564.tfilter)
	e1:SetValue(c564.efilter)
	c:RegisterEffect(e1)
	
	--auto be attacked
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	--e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetCode(EFFECT_ONLY_BE_ATTACKED)
	--e2:SetRange(LOCATION_ONFIELD)
	--e2:SetValue(aux.imval1)
	c:RegisterEffect(e2) 
	
	--DEFENSE up
	local e3=Effect.CreateEffect(c)
	e3:SetCategory(CATEGORY_DEFCHANGE)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e3:SetCode(EVENT_BE_BATTLE_TARGET)
	e3:SetCost(c564.ducost)
	e3:SetCondition(c564.ducon)
	e3:SetOperation(c564.duop)
	c:RegisterEffect(e3) 
end
c564.listed_series={0x900}
c564.material_setcode={0x900}

function c564.efilter(e,te)
		return te:GetOwnerPlayer()~=e:GetOwnerPlayer()
end

function c564.tfilter(e,c)
	return c:IsSetCard(0x900) and c:IsType(TYPE_MONSTER)
	and (c:IsLocation(LOCATION_MZONE) or c:GetFlagEffect(12)~=0) 
end

function c564.ducon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler()==Duel.GetAttackTarget()
end

function c564.costfilter(c)
	return c:IsAbleToGraveAsCost()
end

function c564.ducost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c564.costfilter,tp,LOCATION_HAND,0,1,e:GetHandler()) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local cg=Duel.SelectMatchingCard(tp,c564.costfilter,tp,LOCATION_HAND,0,1,4,nil)
	Duel.SendtoGrave(cg,REASON_COST)
	e:SetLabel(cg:GetCount())
end

function c564.duop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local cg=e:GetLabel()*600
	if c:IsFaceup() then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_DEFENSE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		e1:SetValue(cg)
		c:RegisterEffect(e1)
	end
end
