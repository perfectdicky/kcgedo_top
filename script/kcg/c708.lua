-- 太阳神之翼神龙
local isextra = Card.IsAbleToExtra
function Card.IsAbleToExtra(c)
    if c:IsCode(10000010) and c:IsType(TYPE_FUSION) then
        return true
    else
        return isextra(c)
    end
end

local s, id = GetID()
function s.initial_effect(c)
    -- c:SetUniqueOnField(1,1,708)

    -- 解放3只祭品通召
    -- local e1=Effect.CreateEffect(c)
    -- e1:SetDescription(aux.Stringid(10000080,0))
    -- e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
    -- e1:SetType(EFFECT_TYPE_SINGLE)
    -- e1:SetCode(EFFECT_LIMIT_SUMMON_PROC)
    -- e1:SetCondition(s.ttcon)
    -- e1:SetOperation(s.ttop)
    -- e1:SetValue(SUMMON_TYPE_TRIBUTE)
    -- c:RegisterEffect(e1)
    -- local e2=Effect.CreateEffect(c)
    -- e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
    -- e2:SetType(EFFECT_TYPE_SINGLE)
    -- e2:SetCondition(s.ttcon)
    -- e2:SetOperation(s.ttop)
    -- e2:SetCode(EFFECT_LIMIT_SET_PROC)
    -- c:RegisterEffect(e2)
    local e1 = aux.AddNormalSummonProcedure(c, true, false, 3, 3, SUMMON_TYPE_TRIBUTE, aux.Stringid(10000080, 0))
    local e2 = aux.AddNormalSetProcedure(c, true, false, 3, 3, SUMMON_TYPE_TRIBUTE, aux.Stringid(10000080, 0))
    local e02 = Effect.CreateEffect(c)
    e02:SetDescription(aux.Stringid(10000080, 1))
    e02:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_SPSUM_PARAM)
    e02:SetType(EFFECT_TYPE_SINGLE)
    e02:SetCode(EFFECT_LIMIT_SUMMON_PROC)
    e02:SetTargetRange(POS_FACEUP_ATTACK, 1)
    e02:SetCondition(s.ttcon2)
    e02:SetTarget(s.tttg2)
    e02:SetOperation(s.ttop2)
    e02:SetValue(SUMMON_TYPE_TRIBUTE)
    c:RegisterEffect(e02)

    -- control return
    local e00 = Effect.CreateEffect(c)
    e00:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e00:SetCode(EVENT_ADJUST)
    e00:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
    e00:SetRange(LOCATION_MZONE)
    e00:SetCondition(s.retcon)
    e00:SetOperation(s.retop)
    c:RegisterEffect(e00)
    -- attack limit
    local e06 = Effect.CreateEffect(c)
    e06:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e06:SetType(EFFECT_TYPE_SINGLE)
    e06:SetCode(EFFECT_CANNOT_ATTACK)
    e06:SetCondition(s.retcon3)
    c:RegisterEffect(e06)
    local e111 = e06:Clone()
    e111:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e111:SetRange(LOCATION_MZONE)
    e111:SetCode(EFFECT_CANNOT_TRIGGER)
    c:RegisterEffect(e111)
    local e112 = e06:Clone()
    e112:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e112:SetCode(EFFECT_CANNOT_CHANGE_POSITION)
    e112:SetRange(LOCATION_MZONE)
    c:RegisterEffect(e112)

    -- cannot be target
    local e07 = Effect.CreateEffect(c)
    e07:SetType(EFFECT_TYPE_SINGLE)
    e07:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e07:SetCode(EFFECT_IGNORE_BATTLE_TARGET)
    e07:SetRange(LOCATION_MZONE)
    e07:SetCondition(s.retcon3)
    e07:SetValue(aux.imval1)
    c:RegisterEffect(e07)
    local e08 = e07:Clone()
    e08:SetCode(EFFECT_CANNOT_BE_EFFECT_TARGET)
    e08:SetValue(aux.tgoval)
    c:RegisterEffect(e08)

    -- 特召回合结束送去特召前区域
    local e3 = Effect.CreateEffect(c)
    -- e3:SetDescription(aux.Stringid(708,2))
    e3:SetCategory(CATEGORY_TOGRAVE + CATEGORY_TOHAND + CATEGORY_TODECK + CATEGORY_REMOVE)
    e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_F)
    e3:SetRange(LOCATION_MZONE)
    e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e3:SetCountLimit(1)
    e3:SetCode(EVENT_PHASE + PHASE_END)
    e3:SetCondition(s.tgcon)
    e3:SetTarget(s.tgtg)
    e3:SetOperation(s.tgop)
    c:RegisterEffect(e3)

    local te = Effect.CreateEffect(c)
    te:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    te:SetRange(LOCATION_MZONE)
    te:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    te:SetCountLimit(1)
    te:SetCode(EVENT_PHASE + PHASE_END)
    te:SetOperation(s.reop2)
    c:RegisterEffect(te)

    -- 通常召唤后攻守数值
    local e4 = Effect.CreateEffect(c)
    e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e4:SetType(EFFECT_TYPE_SINGLE)
    e4:SetCode(EFFECT_MATERIAL_CHECK)
    e4:SetValue(s.valcheck)
    c:RegisterEffect(e4)
    local e5 = Effect.CreateEffect(c)
    e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e5:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
    e5:SetCode(EVENT_SUMMON_SUCCESS)
    e5:SetLabelObject(e4)
    e5:SetOperation(s.atkop)
    c:RegisterEffect(e5)

    -- 支付LP1000破坏对方场上所有怪兽
    local e6 = Effect.CreateEffect(c)
    e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e6:SetDescription(aux.Stringid(10000011, 1))
    e6:SetCategory(CATEGORY_DESTROY)
    e6:SetType(EFFECT_TYPE_QUICK_O)
    e6:SetCode(EVENT_FREE_CHAIN)
    e6:SetRange(LOCATION_MZONE)
    e6:SetCountLimit(1)
    e6:SetCost(s.descost)
    e6:SetTarget(s.destg)
    e6:SetOperation(s.desop)
    c:RegisterEffect(e6)

    local e70 = Effect.CreateEffect(c)
    e70:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e70:SetDescription(aux.Stringid(10000011, 0))
    e70:SetCategory(CATEGORY_ATKCHANGE)
    e70:SetType(EFFECT_TYPE_QUICK_O)
    e70:SetCode(EVENT_FREE_CHAIN)
    e70:SetRange(LOCATION_MZONE)
    e70:SetCountLimit(1)
    e70:SetCost(s.otkcost2)
    e70:SetOperation(s.otkop2)
    c:RegisterEffect(e70)

    -- LP减
    local e7 = Effect.CreateEffect(c)
    e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e7:SetDescription(aux.Stringid(708, 0))
    e7:SetCategory(CATEGORY_ATKCHANGE)
    e7:SetType(EFFECT_TYPE_QUICK_O)
    e7:SetCode(EVENT_FREE_CHAIN)
    e7:SetRange(LOCATION_MZONE)
    e7:SetCountLimit(1)
    e7:SetCost(s.otkcost)
    e7:SetCondition(s.otkcon)
    e7:SetOperation(s.otkop)
    c:RegisterEffect(e7)

    -- Unstoppable Attack
    local e203 = Effect.CreateEffect(c)
    e203:SetType(EFFECT_TYPE_SINGLE)
    e203:SetCode(EFFECT_UNSTOPPABLE_ATTACK)
    e203:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_SINGLE_RANGE)
    e203:SetRange(LOCATION_MZONE)
    e203:SetCondition(s.retcon2)
    c:RegisterEffect(e203)

    -- 不受陷阱效果以及魔法效果怪兽生效一回合
    local e82 = Effect.CreateEffect(c)
    e82:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e82:SetCode(EVENT_CHAIN_SOLVED)
    e82:SetRange(LOCATION_MZONE)
    e82:SetCondition(s.sdcon2)
    e82:SetOperation(s.sdop2)
    c:RegisterEffect(e82)    
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e8:SetRange(LOCATION_MZONE)
    e8:SetCode(EFFECT_IMMUNE_EFFECT)
    e8:SetValue(s.efilterr)
    c:RegisterEffect(e8)

    -- 特召后成为攻击目标
    local e17 = Effect.CreateEffect(c)
    e17:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
    e17:SetRange(LOCATION_MZONE)
    e17:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e17:SetCode(EVENT_ATTACK_ANNOUNCE)
    e17:SetCondition(s.atcon2)
    e17:SetOperation(s.atop)
    c:RegisterEffect(e17)
    local e9 = Effect.CreateEffect(c)
    e9:SetType(EFFECT_TYPE_FIELD)
    e9:SetRange(LOCATION_MZONE)
    e9:SetTargetRange(LOCATION_MZONE, 0)
    e9:SetProperty(EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_CANNOT_DISABLE)
    e9:SetCode(EFFECT_CANNOT_BE_BATTLE_TARGET)
    e9:SetCondition(s.atcon)
    e9:SetTarget(s.atlimit)
    e9:SetValue(Auxiliary.imval1)
    -- c:RegisterEffect(e9)
    -- destroy replace
    local e72 = Effect.CreateEffect(c)
    e72:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
    e72:SetCode(EFFECT_DESTROY_REPLACE)
    e72:SetRange(LOCATION_MZONE)
    e72:SetCondition(s.atcon)
    e72:SetTarget(s.reptg)
    e72:SetValue(s.repval)
    e72:SetOperation(s.repop)
    c:RegisterEffect(e72)

    -- 通常召唤、特殊召唤、反转召唤不会被无效化
    local e10 = Effect.CreateEffect(c)
    e10:SetType(EFFECT_TYPE_SINGLE)
    e10:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
    e10:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    c:RegisterEffect(e10)
    local e11 = e10:Clone()
    e11:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
    c:RegisterEffect(e11)
    local e12 = e11:Clone()
    e12:SetCode(EFFECT_CANNOT_DISABLE_FLIP_SUMMON)
    c:RegisterEffect(e12)

    -- 同时当作创造神族怪兽使用
    local e14 = Effect.CreateEffect(c)
    e14:SetType(EFFECT_TYPE_SINGLE)
    e14:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e14:SetCode(EFFECT_ADD_RACE)
    e14:SetValue(RACE_CREATORGOD + RACE_DRAGON)
    c:RegisterEffect(e14)

    -- 特殊召唤时必须全场攻击和直接攻击
    local e21 = Effect.CreateEffect(c)
    e21:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e21:SetType(EFFECT_TYPE_SINGLE)
    e21:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
    e21:SetValue(1)
    e21:SetCondition(s.atcon)
    c:RegisterEffect(e21)
    local e22 = Effect.CreateEffect(c)
    e22:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e22:SetType(EFFECT_TYPE_SINGLE)
    e22:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
    e22:SetValue(1)
    e22:SetCondition(s.atcon)
    c:RegisterEffect(e22)
    local e23 = e22:Clone()
    e23:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e23:SetRange(LOCATION_MZONE)
    e23:SetCode(EFFECT_IMMUNE_EFFECT)
    e23:SetValue(s.eefilter)
    c:RegisterEffect(e23)

    -- 不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变成里侧表示、作为特殊召唤素材
    local e100 = Effect.CreateEffect(c)
    e100:SetType(EFFECT_TYPE_SINGLE)
    e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e100:SetRange(LOCATION_MZONE)
    e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
    e100:SetValue(1)
    c:RegisterEffect(e100)
    local e101 = e100:Clone()
    e101:SetCode(EFFECT_CANNOT_REMOVE)
    c:RegisterEffect(e101)
    local e102 = e101:Clone()
    e102:SetCode(EFFECT_CANNOT_TO_HAND)
    c:RegisterEffect(e102)
    local e103 = e102:Clone()
    e103:SetCode(EFFECT_CANNOT_TO_DECK)
    c:RegisterEffect(e103)
    local e104 = e103:Clone()
    e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
    c:RegisterEffect(e104)
    local e105 = e104:Clone()
    e105:SetCode(EFFECT_CANNOT_DISABLE)
    c:RegisterEffect(e105)
    local e106 = e105:Clone()
    e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
    e106:SetValue(s.retcon2)
    c:RegisterEffect(e106)
    local e107 = e106:Clone()
    e107:SetCode(EFFECT_CANNOT_TURN_SET)
    c:RegisterEffect(e107)
    local e108 = e107:Clone()
    e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
    c:RegisterEffect(e108)
    local e109 = e108:Clone()
    e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
    c:RegisterEffect(e109)
    local e110 = e109:Clone()
    e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
    c:RegisterEffect(e110)
    local e111 = e109:Clone()
    e111:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
    c:RegisterEffect(e111)

    local e400 = Effect.CreateEffect(c)
    e400:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e400:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
    e400:SetCode(EVENT_SUMMON_SUCCESS)
    e400:SetCondition(s.sumsuccon)
    e400:SetOperation(s.sumsuc)
    c:RegisterEffect(e400)
    local e402 = e400:Clone()
    e402:SetCode(EVENT_SPSUMMON_SUCCESS)
    c:RegisterEffect(e402)
    local e403 = e400:Clone()
    e403:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
    c:RegisterEffect(e403)
end
-------------------------------------------------------------------------------
function s.sumsuccon(e, tp, eg, ep, ev, re, r, rp)
    return not (Duel.IsPlayerAffectedByEffect(e:GetHandlerPlayer(), 10) or Duel.IsEnvironment(269012, e:GetHandlerPlayer()) or Duel.GetFlagEffect(e:GetHandlerPlayer(), 8241) ~= 0)
end
function s.sumsuc(e, tp, eg, ep, ev, re, r, rp)
    local type = e:GetHandler():GetType()
    Duel.SetChainLimitTillChainEnd(aux.FALSE)
    if Duel.IsPlayerAffectedByEffect(e:GetHandlerPlayer(), 10) or Duel.IsEnvironment(269012, e:GetHandlerPlayer()) then
        return
    end
    if Duel.GetFlagEffect(e:GetHandlerPlayer(), 7081) ~= 0 then
        return
    end
    Duel.RegisterFlagEffect(e:GetHandlerPlayer(), 7081, 0, 0, 1)
    Duel.Hint(HINT_MESSAGE, e:GetHandlerPlayer(), aux.Stringid(708, 6))
    local opt =
        Duel.SelectOption(e:GetHandlerPlayer(), aux.Stringid(708, 4), aux.Stringid(708, 3), aux.Stringid(708, 7))
    if opt == 1 then
        Duel.RegisterFlagEffect(e:GetHandlerPlayer(), 7082, 0, 0, 1)
    else
        e:GetHandler():SetEntityCode(10000048,nil)
        e:GetHandler():SetCardData(CARDDATA_TYPE, type)
    end
end
-----------------------------------------------------------------------------
function s.ttcon(e, c, minc)
    if c == nil then
        return true
    end
    return minc <= 3 and Duel.CheckTribute(c, 3)
end
function s.ttop(e, tp, eg, ep, ev, re, r, rp, c)
    local g = Duel.SelectTribute(tp, c, 3, 3)
    c:SetMaterial(g)
    Duel.Release(g, REASON_SUMMON + REASON_MATERIAL)
end

function s.ttcon2(e, c, minc, zone, relzone, exeff)
    if c == nil then
        return true
    end
    if exeff then
        local ret = exeff:GetValue()
        if type(ret) == "function" then
            ret = {ret(exeff, c)}
            if #ret > 1 then
                zone = (ret[2] >> 16) & 0x7f
            end
        end
    end
    local tp = c:GetControler()
    local mg = Duel.GetFieldGroup(tp, 0, LOCATION_MZONE)
    mg = mg:Filter(Auxiliary.IsZone, nil, relzone, tp)
    return minc <= 3 and Duel.CheckTribute(c, 3, 3, mg, 1 - tp, zone)
end
function s.tttg2(e, tp, eg, ep, ev, re, r, rp, chk, c, minc, zone, relzone, exeff)
    if exeff then
        local ret = exeff:GetValue()
        if type(ret) == "function" then
            ret = {ret(exeff, c)}
            if #ret > 1 then
                zone = (ret[2] >> 16) & 0x7f
            end
        end
    end
    local mg = Duel.GetFieldGroup(tp, 0, LOCATION_MZONE)
    mg = mg:Filter(Auxiliary.IsZone, nil, relzone, tp)
    local g = Duel.SelectTribute(tp, c, 3, 3, mg, 1 - tp, zone, true)
    if g and #g > 0 then
        g:KeepAlive()
        e:SetLabelObject(g)
        return true
    end
    return false
end
function s.ttop2(e, tp, eg, ep, ev, re, r, rp, c, minc, zone, relzone, exeff)
    local g = e:GetLabelObject()
    c:SetMaterial(g)
    Duel.Release(g, REASON_SUMMON + REASON_MATERIAL)
    g:DeleteGroup()
end

function s.retcon(e, tp, eg, ep, ev, re, r, rp)
    return Duel.GetFlagEffect(e:GetHandler():GetControler(), 7082) == 0
end
function s.retcon2(e, te)
    return e:GetHandler():GetOriginalCode() ~= 10000048
end
function s.retcon3(e)
    return e:GetHandler():GetOriginalCode() == 10000048
end
function s.retop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetOwner()
    local type = c:GetType()
    local opt = 0
    if Duel.IsPlayerAffectedByEffect(e:GetHandler():GetControler(), 10) or
        Duel.IsEnvironment(269012, e:GetHandler():GetControler()) then
        return
    end
    if Duel.GetFlagEffect(e:GetHandler():GetControler(), 7082) ~= 0 then
        return
    end
    if (Duel.IsPlayerAffectedByEffect(1 - e:GetHandler():GetControler(), 10) or
        Duel.IsEnvironment(269012, 1 - e:GetHandler():GetControler())) and
        Duel.GetLocationCount(1 - e:GetHandler():GetControler(), LOCATION_MZONE) > 0 then
        if Duel.SelectYesNo(1 - e:GetHandler():GetControler(), aux.Stringid(11508758, 0)) then
            c:ResetEffect(EFFECT_CANNOT_CHANGE_CONTROL, RESET_CODE)
            Duel.GetControl(c, 1 - e:GetHandler():GetControler())
            local e100 = Effect.CreateEffect(c)
            e100:SetType(EFFECT_TYPE_SINGLE)
            e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
            e100:SetRange(LOCATION_MZONE)
            e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
            e100:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
            e100:SetValue(s.retcon2)
            c:RegisterEffect(e100)
            if e:GetHandler():GetOriginalCode() == 10000048 then
                e:GetHandler():SetEntityCode(708,nil)
            end
        end
        return
    end

    if Duel.GetFlagEffect(1 - e:GetHandler():GetControler(), 7081) == 0 and
        not (Duel.IsPlayerAffectedByEffect(1 - e:GetHandler():GetControler(), 10) or
            Duel.IsEnvironment(269012, 1 - e:GetHandler():GetControler())) then
        Duel.RegisterFlagEffect(1 - e:GetHandler():GetControler(), 7081, 0, 0, 1)
        Duel.Hint(HINT_MESSAGE, 1 - e:GetHandlerPlayer(), aux.Stringid(708, 6))
        local opt = Duel.SelectOption(1 - e:GetHandlerPlayer(), aux.Stringid(708, 3), aux.Stringid(708, 4),
                        aux.Stringid(708, 7))
        if opt == 0 then
            Duel.RegisterFlagEffect(1 - e:GetHandler():GetControler(), 7082, 0, 0, 1)
        else
            if not e:GetHandler():GetOriginalCode() == 10000048 then
                e:GetHandler():SetEntityCode(10000048,nil)
                e:GetHandler():SetCardData(CARDDATA_TYPE, type)
            end
        end
    end

    if Duel.GetFlagEffect(1 - e:GetHandler():GetControler(), 7082) ~= 0 and
        Duel.GetLocationCount(1 - e:GetHandler():GetControler(), LOCATION_MZONE) > 0 then
        if Duel.SelectYesNo(1 - e:GetHandler():GetControler(), aux.Stringid(11508758, 0)) then
            c:ResetEffect(EFFECT_CANNOT_CHANGE_CONTROL, RESET_CODE)
            Duel.GetControl(c, 1 - e:GetHandler():GetControler())
            local e100 = Effect.CreateEffect(c)
            e100:SetType(EFFECT_TYPE_SINGLE)
            e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
            e100:SetRange(LOCATION_MZONE)
            e100:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
            e100:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
            e100:SetValue(s.retcon2)
            c:RegisterEffect(e100)
            if e:GetHandler():GetOriginalCode() == 10000048 then
                e:GetHandler():SetEntityCode(708,nil)
            end
        end
        return
    end

    if Duel.GetFlagEffect(e:GetHandler():GetControler(), 7081) == 0 and
        Duel.GetFlagEffect(1 - e:GetHandler():GetControler(), 7082) == 0 then
        local opt = Duel.SelectOption(e:GetHandlerPlayer(), aux.Stringid(708, 3), aux.Stringid(708, 4),
                        aux.Stringid(708, 7))
        if opt == 0 then
            Duel.RegisterFlagEffect(e:GetHandler():GetControler(), 7082, 0, 0, 1)
            if e:GetHandler():GetOriginalCode() == 10000048 then
                e:GetHandler():SetEntityCode(708,nil)
            end
        else
            if not e:GetHandler():GetOriginalCode() == 10000048 then
                e:GetHandler():SetEntityCode(10000048,nil)
                e:GetHandler():SetCardData(CARDDATA_TYPE, type)
            end
        end
    end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.tgcon(e, tp, eg, ep, ev, re, r, rp)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL
end

function s.tgtg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
        Duel.SetOperationInfo(0, CATEGORY_TOGRAVE, e:GetHandler(), 1, 0, 0)
    elseif e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
        Duel.SetOperationInfo(0, CATEGORY_REMOVE, e:GetHandler(), 1, 0, 0)
    elseif e:GetHandler():GetPreviousLocation() == LOCATION_HAND then
        Duel.SetOperationInfo(0, CATEGORY_TOHAND, e:GetHandler(), 1, 0, 0)
    elseif e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
        Duel.SetOperationInfo(0, CATEGORY_TODECK, e:GetHandler(), 1, 0, 0)
    else
        return
    end
end

function s.tgop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsRelateToEffect(e) and c:IsFaceup() then
        if e:GetHandler():GetPreviousLocation() == LOCATION_GRAVE then
            Duel.SendtoGrave(c, REASON_RULE)
        elseif e:GetHandler():GetPreviousLocation() == LOCATION_REMOVED then
            Duel.Remove(c, 0, REASON_RULE)
        elseif e:GetHandler():GetPreviousLocation() == LOCATION_HAND then
            Duel.SendtoHand(c, nil, REASON_RULE)
        elseif e:GetHandler():GetPreviousLocation() == LOCATION_DECK then
            Duel.SendtoDeck(c, nil, 2, REASON_RULE)
        else
            Duel.MoveToField(c, tp, c:GetPreviousControler(), c:GetPreviousLocation(), c:GetPreviousPosition(), true)
        end
    end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.valcheck(e, c)
    local g = c:GetMaterial()
    local tc = g:GetFirst()
    local tatk = 0
    local tdef = 0
    while tc do
        local atk = tc:GetAttack()
        local def = tc:GetDefense()
        if atk < 0 then
            atk = 0
        end
        if def < 0 then
            def = 0
        end
        tatk = tatk + atk
        tdef = tdef + def
        tc = g:GetNext()
    end
    Original_ATK = tatk
    Original_DEF = tdef
end

function s.atkop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:GetMaterialCount() == 0 then
        return
    end
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_SET_ATTACK)
    e1:SetValue(Original_ATK)
    e1:SetReset(RESET_EVENT + 0x1fe0000)
    c:RegisterEffect(e1)
    local e2 = e1:Clone()
    e2:SetCode(EFFECT_SET_DEFENSE)
    e2:SetValue(Original_DEF)
    c:RegisterEffect(e2)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.descost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.CheckLPCost(tp, 1000)
    end
    Duel.PayLPCost(tp, 1000)
end

function s.destg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsExistingMatchingCard(nil, tp, 0, LOCATION_MZONE, 1, nil)
    end
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetProperty(EFFECT_FLAG_OATH)
    e1:SetCode(EFFECT_CHANGE_CODE)
    e1:SetValue(10000049)
    e1:SetReset(RESET_EVENT + 0x1fe0000 + RESET_PHASE + PHASE_END)
    -- e:GetHandler():RegisterEffect(e1)
    local g = Duel.GetMatchingGroup(nil, tp, 0, LOCATION_MZONE, nil)
    Duel.SetOperationInfo(0, CATEGORY_DESTROY, g, g:GetCount(), 0, 0)
end

function s.desop(e, tp, eg, ep, ev, re, r, rp)
    local type = e:GetHandler():GetType()
    local code = e:GetHandler():GetOriginalCode()
    e:GetHandler():SetEntityCode(10000049,nil)
    e:GetHandler():SetCardData(CARDDATA_TYPE, type)
    local g = Duel.GetMatchingGroup(nil, tp, 0, LOCATION_MZONE, nil)
    Duel.Destroy(g, REASON_RULE)
    e:GetHandler():SetEntityCode(code,nil)
end
function s.eefilter(e, te)
    return te:GetOwner() ~= e:GetOwner()
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.otkcost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.GetLP(tp) > 1
    end
    local lp = Duel.GetLP(tp)
    Duel.SetLP(tp, 1)
    e:SetLabel(lp - 1)
end
function s.otkcon(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    return not c:IsType(TYPE_FUSION)
end
function s.otkop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    if c:IsFaceup() and c:IsRelateToEffect(e) and not c:IsType(TYPE_FUSION) then
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
        e1:SetRange(LOCATION_MZONE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(e:GetLabel())
        e1:SetReset(RESET_EVENT + 0x1fe0000)
        c:RegisterEffect(e1)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_UPDATE_DEFENSE)
        c:RegisterEffect(e2)
        local e3 = e1:Clone()
        e3:SetCode(EFFECT_ADD_TYPE)
        e3:SetValue(TYPE_FUSION)
        c:RegisterEffect(e3)
        local e4 = Effect.CreateEffect(c)
        e4:SetProperty(EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DAMAGE_CAL)
        e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        e4:SetCode(EVENT_ADJUST)
        e4:SetRange(LOCATION_MZONE)
        e4:SetCondition(s.lpcon)
        e4:SetOperation(s.lpop)
        e4:SetReset(RESET_EVENT + 0x1fe0000)
        c:RegisterEffect(e4)
        local e5 = Effect.CreateEffect(c)
        e5:SetProperty(EFFECT_FLAG_DAMAGE_STEP + EFFECT_FLAG_DAMAGE_CAL)
        e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
        e5:SetRange(LOCATION_MZONE)
        e5:SetCode(EVENT_CHAIN_SOLVED)
        e5:SetLabelObject(e4)
        e5:SetCondition(s.lpcon2)
        e5:SetOperation(s.lpop2)
        e5:SetReset(RESET_EVENT + 0x1fe0000 + EVENT_CHAIN_SOLVED)
        c:RegisterEffect(e5)
    end
end

function s.lpcon(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local ttp = c:GetControler()
    local lp = Duel.GetLP(ttp)
    return c:IsType(TYPE_FUSION) and lp > 1
end
function s.lpop(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local ttp = c:GetControler()
    local lp = Duel.GetLP(ttp)
    if c:IsType(TYPE_FUSION) and lp > 1 then
        Duel.SetLP(ttp, 1)
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
        e1:SetRange(LOCATION_MZONE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(lp - 1)
        e1:SetReset(RESET_EVENT + 0x1fe0000)
        c:RegisterEffect(e1)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_UPDATE_DEFENSE)
        c:RegisterEffect(e2)
    end
end
function s.lpcon2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    return c:IsType(TYPE_FUSION) and re:GetHandler():IsCode(95286165)
end
function s.lpop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local ttp = c:GetControler()
    local atk = c:GetAttack()
    if c:IsType(TYPE_FUSION) and re:GetHandler():IsCode(95286165) then
        if e:GetLabelObject() then
            e:GetLabelObject():Reset()
        end
        local e1 = Effect.CreateEffect(c)
        e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_SET_ATTACK_FINAL)
        e1:SetValue(0)
        e1:SetReset(RESET_EVENT + 0x1fe0000)
        c:RegisterEffect(e1, true)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_SET_DEFENSE_FINAL)
        c:RegisterEffect(e2, true)
        local e3 = e1:Clone()
        e3:SetCode(EFFECT_REMOVE_TYPE)
        e3:SetValue(TYPE_FUSION)
        c:RegisterEffect(e3, true)
        Duel.Recover(ttp, atk, REASON_EFFECT)

        c:ResetFlagEffect(10000082)
    end
end
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function s.otkcost2(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.CheckReleaseGroup(tp, nil, 1, e:GetHandler())
    end
    local g = Duel.SelectReleaseGroup(tp, nil, 1, 99, e:GetHandler())
    local tc = g:GetFirst()
    local tatk = 0
    local tdef = 0
    while tc do
        local atk = tc:GetAttack()
        local def = tc:GetDefense()
        if atk < 0 then
            atk = 0
        end
        if def < 0 then
            def = 0
        end
        tatk = tatk + atk
        tdef = tdef + def
        tc = g:GetNext()
    end
    e:SetLabelObject({tatk,tdef})
    Duel.Release(g, REASON_COST)
end
function s.otkop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tatk=e:GetLabelObject()[1]
    local tdef=e:GetLabelObject()[2]
    if c:IsFaceup() and c:IsRelateToEffect(e) then
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
        e1:SetRange(LOCATION_MZONE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(tatk)
        e1:SetReset(RESET_EVENT + 0x1fe0000)
        c:RegisterEffect(e1)
        local e2 = e1:Clone()
        e2:SetCode(EFFECT_UPDATE_DEFENSE)
        e2:SetValue(tdef)
        c:RegisterEffect(e2)
    end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.efilterr(e, te)
    local c = e:GetHandler()
    local tc = te:GetOwner()
    local turncount = Duel.GetTurnCount() - tc:GetTurnID()
    if c:GetTurnID() == Duel.GetTurnCount() then
        turncount = 0
    end
    if tc == e:GetOwner() or te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) then
        return false
    else
        return te:GetActiveType() == TYPE_TRAP
        --   or ((te:IsActiveType(TYPE_MONSTER) or te:IsActiveType(TYPE_SPELL)) and tc~=e:GetOwner())
        --    and ((te:GetType()==EFFECT_TYPE_FIELD and not (tc:GetTurnID()>=Duel.GetTurnCount() or (tc:GetFlagEffect(818)~=0 and tc:GetFlagEffectLabel(818)==c:GetFieldID())) )
        -- 	 or te:GetType()==EFFECT_TYPE_EQUIP and Duel.GetTurnCount()-tc:GetTurnID()>=1)) 
    end
end

function s.sdcon2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=1
                    break 
                end
            end
        end
    end
    return eff==1
end
function s.sdop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local tc = re:GetHandler()
    local eff=0
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if tc~=c and te:GetOwner()==tc and (te:GetType()==EFFECT_TYPE_SINGLE or te:GetType()==EFFECT_TYPE_EQUIP) and te:GetHandler()==c and not te:IsHasProperty(EFFECT_FLAG_IGNORE_IMMUNE) and not te:GetOwner():IsHasEffect(EFFECT_ULTIMATE_IMMUNE)
                and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then 
                    eff=te
                    break 
                end
            end
        end
    end
    if eff==0 then return end
    local e83 = Effect.CreateEffect(c)
    e83:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
    e83:SetRange(LOCATION_MZONE)
    e83:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_CANNOT_DISABLE)
    e83:SetCountLimit(1)
    e83:SetCode(EVENT_PHASE + PHASE_END)
    e83:SetLabelObject(eff)
    e83:SetOperation(s.setop2)
    e83:SetReset(RESET_PHASE + PHASE_END)
    c:RegisterEffect(e83) 
end
function s.setop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    local te = e:GetLabelObject()
    if not te or te==0 then return end
    te:Reset()
end   

function s.efilter(e, te)
    if (te:IsActiveType(TYPE_MONSTER) or te:IsActiveType(TYPE_SPELL) and te:GetHandler() ~= e:GetOwner()) then
        -- and (te:GetHandler():GetFlagEffect(7080)==0 or (te:GetHandler():GetFlagEffect(7080)~=0  and te:GetHandler():GetFlagEffectLabel(7080)~=e:GetHandler():GetFieldID()))
        -- and (te:GetHandler():GetFlagEffect(708)==0 or (te:GetHandler():GetFlagEffect(708)~=0 and te:GetHandler():GetFlagEffectLabel(708)~=e:GetHandler():GetFieldID())) then
        te:GetHandler():RegisterFlagEffect(708, RESET_EVENT + 0x1ff0000, 0, 1)
        te:GetHandler():SetFlagEffectLabel(708, e:GetHandler():GetFieldID())
    end
    return te:IsActiveType(TYPE_TRAP) and te:GetHandler():GetFlagEffect(7080) ~= 0 and
               te:GetHandler():GetFlagEffect(708) == 0
    -- and (te:GetHandler()~=e:GetOwner() or (te:GetHandler():GetFlagEffect(7080)~=0 and te:GetHandler():GetFlagEffectLabel(7080)==e:GetHandler():GetFieldID()))
end
function s.setfilter(c, tc)
    return c:GetFlagEffect(708) ~= 0 and c:GetFlagEffectLabel(708) == tc:GetFieldID()
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.atcon(e)
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL
end
function s.atcon2(e, tp, eg, ep, ev, re, r, rp)
    local tc = Duel.GetAttacker()
    local tc2 = Duel.GetAttackTarget()
    return bit.band(e:GetHandler():GetSummonType(), SUMMON_TYPE_SPECIAL) == SUMMON_TYPE_SPECIAL and tc:IsFaceup() and
               tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler()
end
function s.atop(e, tp, eg, ep, ev, re, r, rp)
    local tc = Duel.GetAttacker()
    local tc2 = Duel.GetAttackTarget()
    if tc:IsFaceup() and tc:IsControler(1 - tp) and not tc:IsStatus(STATUS_ATTACK_CANCELED) and tc2 ~= e:GetHandler() then
        Duel.ChangeAttackTarget(e:GetHandler())
    end
end

function s.atlimit(e, c)
    return c ~= e:GetHandler()
end

function s.repfilter(c, tc, tp)
    return c:IsControler(tp) and c ~= tc and c:IsLocation(LOCATION_MZONE) and
               (c:IsReason(REASON_EFFECT) or c:IsReason(REASON_RULE)) and not c:IsReason(REASON_REPLACE)
end
function s.reptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return eg:IsExists(s.repfilter, 1, e:GetHandler(), e:GetHandler(), tp) and
                   not e:GetHandler():IsStatus(STATUS_DESTROY_CONFIRMED + STATUS_BATTLE_DESTROYED)
    end
    return Duel.SelectYesNo(tp, aux.Stringid(19333131, 0))
end
function s.repval(e, c)
    return s.repfilter(c, e:GetHandler(), e:GetHandlerPlayer())
end
function s.repop(e, tp, eg, ep, ev, re, r, rp)
    Duel.Destroy(e:GetHandler(), REASON_RULE + REASON_REPLACE)
end

function s.reop2(e, tp, eg, ep, ev, re, r, rp)
    local c = e:GetHandler()
    for i = 1, 428 do
        if c:IsHasEffect(i) then
            local ae = {c:IsHasEffect(i)}
            for _, te in ipairs(ae) do
                if te:GetOwner() ~= e:GetOwner() and (te:IsActiveType(TYPE_SPELL) or te:IsActiveType(TYPE_MONSTER)) then
                    local e80 = Effect.CreateEffect(c)
                    e80:SetType(EFFECT_TYPE_SINGLE)
                    e80:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
                    e80:SetRange(LOCATION_MZONE)
                    e80:SetCode(EFFECT_IMMUNE_EFFECT)
                    e80:SetValue(function(e, te2)
                        return te2 == te
                    end)
                    c:RegisterEffect(e80)
                end
            end
        end
    end
end
