--Seventh Arrival
function c11370083.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_BE_BATTLE_TARGET)
	e1:SetCondition(c11370083.condition)
	e1:SetCost(c11370083.cost)
	e1:SetTarget(c11370083.target)
	e1:SetOperation(c11370083.activate)
	c:RegisterEffect(e1)
end
function c11370083.condition(e,tp,eg,ep,ev,re,r,rp)
	local at=Duel.GetAttackTarget()
	return at and at:IsFaceup() and at:IsControler(tp) and at:IsType(TYPE_XYZ)
end
function c11370083.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckReleaseGroup(tp,nil,1,nil) end
	local g0=Duel.GetReleaseGroup(tp)
	local ct0=g0:FilterCount(Card.IsType,nil,TYPE_XYZ)
	local count=Duel.Release(g0,REASON_COST)
	e:SetLabel(count)
end
function c11370083.filter(c,e,tp)
	return c:IsType(TYPE_XYZ) and c:IsCanBeSpecialSummoned(e,0,tp,false,false) and c:GetFlagEffect(11370082)~=0
end
function c11370083.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_REMOVED) and c11370083.filter(chkc,e,tp) end
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingTarget(c11370083.filter,tp,LOCATION_REMOVED,LOCATION_REMOVED,1,nil,e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectTarget(tp,c11370083.filter,tp,LOCATION_REMOVED,LOCATION_REMOVED,1,1,nil,e,tp)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,1,0,0)
end
function c11370083.activate(e,tp,eg,ep,ev,re,r,rp)
	local count=e:GetLabel()+1
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if c:IsRelateToEffect(e) and tc:IsRelateToEffect(e) then
		if Duel.SpecialSummon(tc,0,tp,tp,false,false,POS_FACEUP)==0 then return end
	end
	local g=Duel.GetMatchingGroup(c11370083.filter2,tp,LOCATION_REMOVED,0,c)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
	local dg=g:Select(tp,count,count,nil)
	Duel.Overlay(tc,dg)
	tc:RegisterFlagEffect(11370083,RESET_EVENT+0x1ff0000,0,1)
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_OVERLAY_REMOVE_REPLACE)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCondition(c11370083.rcon)
	e4:SetOperation(c11370083.rop)
	e4:SetReset(RESET_EVENT+0x1fe0000)
	--tc:RegisterEffect(e4)
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DAMAGE)
	e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e1:SetCountLimit(1)
	e1:SetCode(EVENT_PHASE+PHASE_BATTLE)
	e1:SetTarget(c11370083.destg)
	e1:SetOperation(c11370083.desop)
	e1:SetReset(RESET_PHASE+PHASE_END) 
	Duel.RegisterEffect(e1,tp) 
end
function c11370083.filter2(c,e,tp)
	local no=c.xyz_number
	return no and no>=101 and no<=107 and c:IsSetCard(0x1048)
end
function c11370083.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local dam=Duel.GetFieldGroupCount(tp,LOCATION_HAND,LOCATION_HAND)*300
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,PLAYER_ALL,dam)
end
function c11370083.desop(e,tp,eg,ep,ev,re,r,rp)
	local count=Duel.GetFieldGroupCount(tp,LOCATION_HAND,LOCATION_HAND)*300
	Duel.Damage(tp,count,REASON_EFFECT)
	Duel.Damage(1-tp,count,REASON_EFFECT)
end

function c11370083.rcon(e,tp,eg,ep,ev,re,r,rp)
	return bit.band(r,REASON_COST)~=0 and e:GetHandler():GetOverlayCount()>0 and re:GetHandler()==e:GetHandler()
end
function c11370083.rop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetCode(EFFECT_DISABLE)
	e4:SetReset(RESET_EVENT+0x1fe0000+RESET_EVENT+EVENT_ADJUST)
	c:RegisterEffect(e4)
end
