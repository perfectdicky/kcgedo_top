--幻神之影
function c50.initial_effect(c)
	--解放任意数量幻神特召幻神之影衍生物
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(13706,15))
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCost(c50.cost)
	e1:SetTarget(c50.target)
	e1:SetOperation(c50.activate)
	c:RegisterEffect(e1)
end

function c50.rfilter(c)
	return c:IsRace(RACE_DIVINE) and not c:IsType(TYPE_TOKEN)
end

function c50.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetFlagEffect(tp,50)==0 and Duel.CheckReleaseGroup(tp,c50.rfilter,1,nil) end
	local g=Duel.SelectReleaseGroup(tp,c50.rfilter,1,10,nil)
	e:SetLabel(g:GetCount())
	Duel.Release(g,REASON_COST)
	Duel.RegisterFlagEffect(tp,50,RESET_PHASE+PHASE_END,EFFECT_FLAG_OATH,1)
end

function c50.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>-1
		and Duel.IsPlayerCanSpecialSummonMonster(tp,13,0x101b,0x4011,4000,4000,10,RACE_DIVINE,ATTRIBUTE_DEVINE) end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,e:GetLabel(),0,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,e:GetLabel(),0,0)
end

function c50.activate(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<e:GetLabel() then return end
	if Duel.IsPlayerCanSpecialSummonMonster(tp,13,0x101b,0x4011,4000,4000,10,RACE_DIVINE,ATTRIBUTE_DEVINE) then
		for i=1,e:GetLabel() do
			local token=Duel.CreateToken(tp,13)
			Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
		end
		Duel.SpecialSummonComplete()
	end
end
