--アマゾネス転生術
function c752.initial_effect(c)
	-- local e0=Effect.CreateEffect(c)
	-- e0:SetProperty(EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_SINGLE_RANGE)
	-- e0:SetType(EFFECT_TYPE_SINGLE)
	-- e0:SetCode(EFFECT_CHANGE_TYPE)
	-- e0:SetRange(LOCATION_SZONE)
	-- e0:SetValue(TYPE_CONTINUOUS+TYPE_TRAP)
	-- c:RegisterEffect(e0)	
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY+CATEGORY_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)	
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c752.target)
	e1:SetOperation(c752.activate)
	c:RegisterEffect(e1)
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_DESTROY)
	-- e2:SetType(EFFECT_TYPE_IGNITION) 
	-- e2:SetRange(LOCATION_HAND)
	-- e2:SetCondition(c752.con)
	-- e2:SetTarget(c752.target)
	-- e2:SetOperation(c752.activate2)
	--c:RegisterEffect(e2) 
end

function c752.con(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsHasEffect(740) 
end

function c752.dfilter(c)
	return c:IsSetCard(0x316) and c:IsType(TYPE_MONSTER)
end
function c752.spfilter(c,e,tp)
	return c:IsSetCard(0x316) and c:IsCanBeSpecialSummoned(e,0,tp,false,false,POS_FACEUP_DEFENSE)
end
function c752.ofilter(c)
	return c:IsType(TYPE_SPELL) or c:IsType(TYPE_TRAP)
end
function c752.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
		--(Duel.IsExistingMatchingCard(c752.dfilter,tp,LOCATION_MZONE,0,1,nil) 
		--or (e:GetHandler():GetFlagEffect(752)~=0 and Duel.IsExistingMatchingCard(c752.ofilter,tp,LOCATION_DECK+LOCATION_HAND,0,1,nil)) ) 
		--or e:GetHandler():GetFlagEffect(752)~=0 end
	if Duel.IsExistingMatchingCard(c752.dfilter,tp,LOCATION_DECK,0,1,nil) and e:GetHandler():GetFlagEffect(752)~=0 then		
		local g=Duel.GetMatchingGroup(c752.dfilter,tp,LOCATION_DECK,0,nil)
		Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
	end
end
function c752.darkness(c)
	return c:IsFaceup() and c:GetFlagEffect(752)~=0 and c:IsType(TYPE_TRAP+TYPE_CONTINUOUS) and c:IsSetCard(0x316)
end
function c752.activate(e,tp,eg,ep,ev,re,r,rp)
	-- e:GetHandler():CancelToGrave(true)
	if e:GetHandler():GetFlagEffect(752)==0 then return end	
	-- local sqc=Duel.GetMatchingGroup(c752.darkness,tp,LOCATION_SZONE,0,e:GetHandler())
	-- for i=1,sqc:GetCount() do
	-- if e:GetHandler():GetFlagEffect(752)~=0 and Duel.SelectYesNo(tp,aux.Stringid(12744567,0)) then
	-- 	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_XMATERIAL)
	-- 	local og=Duel.SelectMatchingCard(tp,c752.ofilter,tp,LOCATION_DECK+LOCATION_HAND,0,1,1,nil):GetFirst()
	-- 	if og and not og:IsImmuneToEffect(e) then
	-- 		Duel.Overlay(e:GetHandler(),og)
	--     end
	-- end
	-- end	

	local g=Duel.GetMatchingGroup(c752.dfilter,tp,LOCATION_DECK,0,nil)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	local efg=g:Select(tp,1,1,nil)	
	local ct=Duel.Destroy(efg,REASON_EFFECT)
	local ft=Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_FUSION)
	if ft>ct then ft=ct end
	if ft>0 then
	if Duel.IsPlayerAffectedByEffect(tp,59822133) then ft=1 end
	local sg=Duel.GetMatchingGroup(aux.NecroValleyFilter(c752.spfilter),tp,LOCATION_EXTRA,0,nil,e,tp)
	if sg:GetCount()>0 and Duel.SelectYesNo(tp,aux.Stringid(6459419,0)) then
		Duel.BreakEffect()
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local fg=sg:Select(tp,ft,ft,nil)
		Duel.SpecialSummon(fg,0,tp,tp,false,false,POS_FACEUP_DEFENSE)
	end
	end

	-- local og2=e:GetHandler():GetOverlayGroup():Filter(c752.ofilter,nil)
	-- if og2:GetCount()<1 then return end
	-- for tc in aux.Next(og2) do
	-- 	for sqtc in aux.Next(sqc) do
	-- 		sqtc:RegisterFlagEffect(tc:GetOriginalCode(),RESET_EVENT+0x1fe0000+RESET_CHAIN+RESET_PHASE+PHASE_END,0,1)
	--     end	
	-- 	c752.zero(tc,e,tp)
	-- end
end

function c752.zero(tc,e,tep)
	local te=tc:GetActivateEffect()
	if te==nil or tc:CheckActivateEffect(true,false,false)==nil then return end
	local condition=te:GetCondition()
	local cost=te:GetCost()
	local target=te:GetTarget()
	local operation=te:GetOperation()
	e:SetProperty(te:GetProperty())
	Duel.ClearTargetCard()
	if cost then cost(te,tep,eg,ep,ev,re,r,rp,1) end
	if target then target(te,tep,eg,ep,ev,re,r,rp,1) end
	tc:CreateEffectRelation(te)
	local gg=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	if gg then  
		local etc=gg:GetFirst()	
		while etc do
			etc:CreateEffectRelation(te)
			etc=gg:GetNext()
		end
	end						
	if operation then operation(te,tep,eg,ep,ev,re,r,rp) end
	tc:ReleaseEffectRelation(te)					
	if gg then  
		local etc=gg:GetFirst()												 
		while etc do
			etc:ReleaseEffectRelation(te)
			etc=gg:GetNext()
		end
	end 
end

function c752.activate2(e,tp,eg,ep,ev,re,r,rp)
	c752.activate(e,tp,eg,ep,ev,re,r,rp)
	Duel.SendtoGrave(e:GetHandler(),REASON_RULE)
end
