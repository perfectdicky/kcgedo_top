--Rank-Up-Magic Admire Des Thousand
function c588.initial_effect(c)
	--Special Summon and Rank-Up
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(13732,1))
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetTarget(c588.target)
	e1:SetOperation(c588.operation)
	c:RegisterEffect(e1)	
end

function c588.filter1(c,e,tp)
	local rk=c:GetRank()
	return c:IsType(TYPE_XYZ) and c:IsFaceup() and Duel.IsExistingMatchingCard(c588.filter3,tp,LOCATION_EXTRA,0,1,nil,rk,e,tp,c) 
	and c:IsSetCard(0x1048) and c:IsCanBeSpecialSummoned(e,0,tp,false,false) and c:IsFaceup() and not c:IsHasEffect(EFFECT_NECRO_VALLEY) and aux.MustMaterialCheck(c,tp,EFFECT_MUST_BE_XMATERIAL)
end
function c588.filter2(c,e,tp,rk)
	return c:IsType(TYPE_XYZ) and c:IsFaceup() and c:GetRank()==rk and c:IsSetCard(0x1048) and c:IsCanBeSpecialSummoned(e,0,tp,false,false) 
	and Duel.IsExistingMatchingCard(c588.filter3,tp,LOCATION_EXTRA,0,1,nil,rk,e,tp,c) and not c:IsHasEffect(EFFECT_NECRO_VALLEY)
end
function c588.filter3(c,rk,e,tp,tc)
	return c:GetRank()==rk+10 and c:IsSetCard(0x1048) and c:IsSetCard(0x14b) and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_XYZ,tp,false,false) and tc:IsCanBeXyzMaterial(c,tp)
end
function c588.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_GRAVE+LOCATION_REMOVED) and c588.filter1(chkc,e,tp) end
	if chk==0 then return Duel.IsPlayerCanSpecialSummonCount(tp,2) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and Duel.GetLocationCountFromEx(tp,tp,nil,TYPE_XYZ)>0 and Duel.IsExistingTarget(c588.filter1,tp,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,nil,e,tp) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectTarget(tp,c588.filter1,tp,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,1,nil,e,tp)
	if g:GetCount()>0 and Duel.IsExistingTarget(c588.filter2,tp,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,g:GetFirst(),e,tp,g:GetFirst():GetRank()) then
	if Duel.GetLocationCount(tp,LOCATION_MZONE)>1 and Duel.SelectYesNo(tp,aux.Stringid(6459419,0)) then
	local g2=Duel.SelectTarget(tp,c588.filter2,tp,LOCATION_GRAVE+LOCATION_REMOVED,LOCATION_GRAVE+LOCATION_REMOVED,1,Duel.GetLocationCount(tp,LOCATION_MZONE),g:GetFirst(),e,tp,g:GetFirst():GetRank())
	  g:Merge(g2) end end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,g,g:GetCount(),tp,LOCATION_GRAVE+LOCATION_REMOVED)
end
function c588.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local ft=Duel.GetLocationCount(tp,LOCATION_MZONE)
	if not c:IsRelateToEffect(e) then return end
	if ft<=0 then return end
	local ag=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	if not ag or #ag<=0 then return end
	local g=ag:Filter(Card.IsRelateToEffect,nil,e)
	if #g<=0 then return end
	local sg=g:Filter(Card.IsLocation,nil,LOCATION_GRAVE+LOCATION_REMOVED)
	local lesser=false
	if sg:GetCount()~=0 then
	if ft<sg:GetCount() then
	   lesser=true
	end
	if lesser==true then
	   if Duel.IsPlayerAffectedByEffect(tp,59822133) then ft=1 end 
	   Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	   local sg2=sg:Select(tp,ft,ft,nil)
	   sg=sg2 
	end
	if sg:GetCount()>ft then return end
	local chk=0
	local tsc=sg:GetFirst()
	local tscg=Group.CreateGroup()
	for i=1,sg:GetCount() do
		if i==sg:GetCount() and Duel.GetLocationCountFromEx(tp,tp,tscg,TYPE_XYZ)<1 then
			local zone=0
			for j=0,4 do
				if Duel.CheckLocation(tp,LOCATION_MZONE,j) then zone=zone|0x1<<j end
			end		
			Duel.SpecialSummon(tsc,0,tp,tp,false,false,POS_FACEUP,zone)
		else 
			Duel.SpecialSummon(tsc,0,tp,tp,false,false,POS_FACEUP) 
		end
		tscg:AddCard(tsc)
		tsc=sg:GetNext()
	end
	Duel.BreakEffect()
	local sgg=tscg
	if sgg:GetCount()<1 then return end
	local xg=sgg:Filter(Card.IsLocation,nil,LOCATION_MZONE)
	local ct=xg:GetCount()
	if ct<1 then return end
	local xyzg=Group.CreateGroup()
	for sc in aux.Next(xg) do
		xyzg:Merge(Duel.GetMatchingGroup(c588.filter3,tp,LOCATION_EXTRA,0,nil,sc:GetRank(),e,tp,sc))
	end	
	if xyzg:GetCount()>0 then
		if Duel.GetLocationCountFromEx(tp,tp,xg,TYPE_XYZ)<1 or not aux.MustMaterialCheck(xg,tp,EFFECT_MUST_BE_XMATERIAL) then return end
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local sc=xyzg:FilterSelect(tp,c588.filter3,1,1,nil,xg:GetFirst():GetRank(),e,tp,xg:GetFirst()):GetFirst()
		if not sc then return end
		sc:SetMaterial(xg)
		Duel.Overlay(sc,xg)
		Duel.SpecialSummon(sc,SUMMON_TYPE_XYZ,tp,tp,false,false,POS_FACEUP)
		sc:CompleteProcedure() 
	end 
    end
end