--奧雷卡爾克斯眼星 (KA)
function c568.initial_effect(c)
	--synchro summon
	Synchro.AddProcedure(c,aux.FilterBoolFunctionEx(Card.IsSetCard,0x900),1,1,Synchro.NonTunerEx(Card.IsSetCard,0x900),1,99)
	c:EnableReviveLimit()
	
	--atk
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetValue(c568.adval)
	c:RegisterEffect(e1)  
	
	--Destroy replace
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_DESTROY_REPLACE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_ONFIELD)
	e2:SetTarget(c568.desreptg)
	e2:SetOperation(c568.desrepop)
	c:RegisterEffect(e2) 
end
c568.listed_series={0x900}
c568.material_setcode={0x900}

function c568.filter(c)
	return c:IsFaceup() and c:GetCode()~=568
	and (c:IsLocation(LOCATION_MZONE) or c:GetFlagEffect(12)~=0)
end

function c568.adval(e,c)
	local g=Duel.GetMatchingGroup(c568.filter,e:GetHandlerPlayer(),LOCATION_ONFIELD,LOCATION_MZONE,nil)
	if g:GetCount()==0 then 
		return 0
	else
	  local tc=g:GetMaxGroup(Card.GetBaseAttack):GetFirst()
		if tc:GetBaseAttack()<=0 then return 0
		else return tc:GetBaseAttack()*2 end
	end
end

function c568.desreptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(Card.IsDiscardable,tp,LOCATION_HAND,0,nil)
	if chk==0 then return not c:IsReason(REASON_BATTLE) and not c:IsReason(REASON_REPLACE) and c:IsOnField() and g:GetCount()>0 end
	if Duel.SelectYesNo(tp,aux.Stringid(13715,1)) then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DISCARD)
		local gc=g:Select(tp,1,1,nil)
		local gcs=gc:GetFirst()
		e:SetLabelObject(gcs)
		Duel.HintSelection(gc)
		
		
		return true
	else return false end
end

function c568.desrepop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetLabelObject()
	Duel.SendtoGrave(tc,REASON_EFFECT+REASON_DISCARD)
end

