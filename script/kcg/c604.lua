--RR－ファジー・レイニアス
function c604.initial_effect(c)
	--spsummon
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_TOHAND)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(c604.spcon)
	e1:SetTarget(c604.sptg)
	e1:SetOperation(c604.spop)
	c:RegisterEffect(e1)
end

function c604.cfilter(c)
	return c:IsFaceup() and c:IsSetCard(0x14b) and not c:IsCode(604)
end
function c604.spcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(c604.cfilter,tp,LOCATION_MZONE,0,1,nil)
end
function c604.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and e:GetHandler():IsCanBeSpecialSummoned(e,0,tp,false,false) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,e:GetHandler(),1,0,0)
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK)
end
function c604.filter2(c)
	return c:IsSetCard(0x95) and c:IsAbleToHand()
end
function c604.spop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsRelateToEffect(e) then
		Duel.SpecialSummon(c,0,tp,tp,false,false,POS_FACEUP)
		Duel.BreakEffect()	
		if Duel.IsExistingMatchingCard(c604.filter2,tp,LOCATION_DECK,0,1,nil) then
	    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	    local g=Duel.SelectMatchingCard(tp,c604.filter2,tp,LOCATION_DECK,0,1,1,nil)
	    Duel.SendtoHand(g,nil,REASON_EFFECT) end
           end
end
