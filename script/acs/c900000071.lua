--黑魔术师S
local s,id=GetID()
function s.initial_effect(c)
	aux.EnableGeminiAttribute(c)
	
	--破坏对方场上所有魔法陷阱
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(900000071,2))
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(aux.IsGeminiState)
	--e1:SetCost(s.cost)
	--e1:SetTarget(s.target)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetAttackAnnouncedCount()==0 end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_OATH)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	e:GetHandler():RegisterEffect(e1)
end

function s.filter(c)
	return c:IsType(TYPE_SPELL+TYPE_TRAP) and aux.TRUE
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return Duel.IsExistingMatchingCard(s.filter,tp,0,LOCATION_ONFIELD,1,c) end
	local sg=Duel.GetMatchingGroup(s.filter,tp,0,LOCATION_ONFIELD,c)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,sg,sg:GetCount(),0,0)
end

function s.operation(e,tp,eg,ep,ev,re,r,rp)
	e:GetHandler():SetEntityCode(732, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
	-- local sg=Duel.GetMatchingGroup(s.filter,tp,0,LOCATION_ONFIELD,e:GetHandler())
	-- Duel.Destroy(sg,REASON_EFFECT)
end
