--黑暗大邪神 佐克·内洛法
local s, id = GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	c:SetUniqueOnField(1,1,900000098)
	
	local e00=Effect.CreateEffect(c)
	e00:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e00:SetType(EFFECT_TYPE_SINGLE)
	e00:SetCode(EFFECT_REMOVE_TYPE)
	e00:SetValue(TYPE_FUSION)
	c:RegisterEffect(e00)

	--特殊召唤方式
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_EXTRA)
	e1:SetCondition(s.spcon)
	c:RegisterEffect(e1)
	
	--特殊召唤限制
	local e2=Effect.CreateEffect(c)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_SPSUMMON_CONDITION)
	c:RegisterEffect(e2)
	
	--特殊召唤不会被无效化
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_CANNOT_DISABLE_SPSUMMON)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	c:RegisterEffect(e3)

	local e18=Effect.CreateEffect(c)
	e18:SetType(EFFECT_TYPE_FIELD)
	e18:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e18:SetCode(EFFECT_CANNOT_LOSE_LP)
	e18:SetRange(LOCATION_MZONE)
	e18:SetTargetRange(1,0)
	e18:SetValue(1)
	c:RegisterEffect(e18)
	local e19=e18:Clone()
	e19:SetCode(EFFECT_CANNOT_LOSE_DECK)
	c:RegisterEffect(e19)
	local e20=e18:Clone()
	e20:SetCode(EFFECT_CANNOT_LOSE_EFFECT)
	c:RegisterEffect(e20)	

	--不受其他卡片效果影响
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE)
	e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCode(EFFECT_IMMUNE_EFFECT)
	e4:SetValue(s.efilter)
	c:RegisterEffect(e4)
	
	--战斗时直接破坏对方怪兽和造成伤害
	local e5=Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(900000098,2))
	e5:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e5:SetCode(EVENT_BATTLE_START)
	e5:SetTarget(s.destg)
	e5:SetOperation(s.desop)
	c:RegisterEffect(e5)
	--local e6=e5:Clone()
	--e6:SetCode(EVENT_DAMAGE_STEP_END)
	--e6:SetTarget(s.damtg)
	--e6:SetOperation(s.damop)
	--c:RegisterEffect(e6)
	
	--直接攻击时攻击力变成无限
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_SINGLE)
	e7:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e7:SetRange(LOCATION_MZONE)
	e7:SetCode(EFFECT_SET_ATTACK)
	e7:SetCondition(s.condtion)
	e7:SetValue(999999)
	c:RegisterEffect(e7)
	
	--特殊召唤时场上怪兽全部变成里侧守备
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e8:SetCode(EVENT_SPSUMMON_SUCCESS)
	e8:SetTarget(s.postg)
	e8:SetOperation(s.posop)
	c:RegisterEffect(e8)
	
	--每回合投掷骰子发动效果
	local e9=Effect.CreateEffect(c)
	e9:SetDescription(aux.Stringid(900000098,2))
	e9:SetCategory(CATEGORY_DESTROY+CATEGORY_DICE)
	e9:SetType(EFFECT_TYPE_IGNITION)
	e9:SetRange(LOCATION_MZONE)
	e9:SetCountLimit(1)
	e9:SetCost(s.diccost)
	e9:SetTarget(s.dictg)
	e9:SetOperation(s.dicop)
	c:RegisterEffect(e9)
	
	--离场时决斗失败
	local e11=Effect.CreateEffect(c)
	e11:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e11:SetCode(EVENT_SPSUMMON_SUCCESS)
	e11:SetOperation(s.sucop)
	c:RegisterEffect(e11)
	
	--玩家不会受到战斗伤害
	-- local e12=Effect.CreateEffect(c)
	-- e12:SetType(EFFECT_TYPE_FIELD)
	-- e12:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
	-- e12:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	-- e12:SetRange(LOCATION_MZONE)
	-- e12:SetTargetRange(1,0)
	-- e12:SetValue(0)
	-- c:RegisterEffect(e12)
	
	--玩家不会受到效果伤害
	-- local e13=Effect.CreateEffect(c)
	-- e13:SetType(EFFECT_TYPE_FIELD)
	-- e13:SetCode(EFFECT_CHANGE_DAMAGE)
	-- e13:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	-- e13:SetRange(LOCATION_MZONE)
	-- e13:SetTargetRange(1,0)
	-- e13:SetValue(s.damval)
	-- c:RegisterEffect(e13)
	
	--卡组抽完不会输掉
	-- local e14=Effect.CreateEffect(c)
	-- e14:SetType(EFFECT_TYPE_FIELD)
	-- e14:SetCode(EFFECT_DRAW_COUNT)
	-- e14:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	-- e14:SetRange(LOCATION_MZONE)
	-- e14:SetTargetRange(1,0)
	-- e14:SetValue(s.dc)
	-- c:RegisterEffect(e14)
	
	--对方抽到艾克佐迪亚部件时除外
	-- local e15=Effect.CreateEffect(c)
	-- e15:SetDescription(aux.Stringid(900000098,2))
	-- e15:SetRange(LOCATION_MZONE)
	-- e15:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	-- e15:SetCode(EVENT_DRAW)
	-- e15:SetCondition(s.drcon)
	-- e15:SetCost(s.drcost)
	-- e15:SetTarget(s.drtg)
	-- e15:SetOperation(s.drop)
	-- c:RegisterEffect(e15)
	
	--不能被各种方式解放
	local e16=Effect.CreateEffect(c)
	e16:SetType(EFFECT_TYPE_SINGLE)
	e16:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e16:SetRange(LOCATION_MZONE)
	e16:SetCode(EFFECT_UNRELEASABLE_SUM)
	e16:SetValue(1)
	c:RegisterEffect(e16)
	local e17=e16:Clone()
	e17:SetCode(EFFECT_UNRELEASABLE_NONSUM)
	c:RegisterEffect(e17)
	local e21=e17:Clone()
	e21:SetCode(EFFECT_UNRELEASABLE_EFFECT)
	c:RegisterEffect(e21)

	--不会被卡的效果破坏、除外、返回手牌和卡组、送去墓地、无效化、改变控制权、变成里侧表示、作为特殊召唤素材
	local e100=Effect.CreateEffect(c)
	e100:SetType(EFFECT_TYPE_SINGLE)
	e100:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e100:SetRange(LOCATION_MZONE)
	e100:SetCode(EFFECT_INDESTRUCTABLE)
	e100:SetValue(1)
	c:RegisterEffect(e100)
	local e101=e100:Clone()
	e101:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e101)
	local e102=e101:Clone()
	e102:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e102)
	local e103=e102:Clone()
	e103:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e103)
	local e104=e103:Clone()
	e104:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e104)
	local e105=e104:Clone()
	e105:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e105)
	local e106=e105:Clone()
	e106:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
	c:RegisterEffect(e106)
	local e107=e106:Clone()
	e107:SetCode(EFFECT_CANNOT_TURN_SET)
	c:RegisterEffect(e107)
	local e108=e107:Clone()
	e108:SetCode(EFFECT_CANNOT_BE_FUSION_MATERIAL)
	c:RegisterEffect(e108)
	local e109=e108:Clone()
	e109:SetCode(EFFECT_CANNOT_BE_SYNCHRO_MATERIAL)
	c:RegisterEffect(e109)
	local e110=e109:Clone()
	e110:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	c:RegisterEffect(e110)
	local e112=e109:Clone()
	e112:SetCode(EFFECT_CANNOT_BE_LINK_MATERIAL)
	c:RegisterEffect(e112)	
	local e113=e109:Clone()
	e113:SetCode(EFFECT_CANNOT_BE_MATERIAL)
	c:RegisterEffect(e113)		
	local e111=e109:Clone()
	e111:SetCode(EFFECT_CANNOT_RELEASE)
	c:RegisterEffect(e111)
	local e112=e109:Clone()
	e112:SetCode(EFFECT_CANNOT_USE_AS_COST)
	c:RegisterEffect(e112)
	local e114=e104:Clone()
	e114:SetCode(EFFECT_CANNOT_DISEFFECT)
	c:RegisterEffect(e114)	
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.spfilter(c)
	return c:IsSetCard(0x2222) and (not c:IsOnField() or c:IsFaceup())
end

function s.spcon(e,c)
	if c==nil then return true end
	if Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)<=0 then return false end
	local g=Duel.GetMatchingGroup(s.spfilter,c:GetControler(),LOCATION_HAND+LOCATION_ONFIELD,0,nil)
	local ct=g:GetClassCount(Card.GetCode)
	return ct>7
end
-----------------------------------------------------------------------------------------
function s.efilter(e, te)
    return te:GetOwner() ~= e:GetOwner()
end
--------------------------------------------------
function s.destg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	local tc=Duel.GetAttacker()
	if tc==c then tc=Duel.GetAttackTarget() end
	if chk==0 then return tc end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,tc,1,0,0)
    Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,tc:GetAttack())
end

function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=Duel.GetAttacker()
	if tc==c then tc=Duel.GetAttackTarget() end
	if tc:IsRelateToBattle() then 
      Duel.Destroy(tc,REASON_RULE) 
      local g=Duel.GetOperatedGroup():GetFirst()
      if g then Duel.Damage(1-tp,g:GetAttack(),REASON_EFFECT) end 
      end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetAttackTarget()~=nil end
	local c=e:GetHandler()
	local d=Duel.GetAttackTarget()
	--if d==c then d=Duel.GetAttacker() end
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,d:GetAttack())
end

function s.damop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Damage(p,d,REASON_EFFECT)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.condtion(e)
	local ph=Duel.GetCurrentPhase()
	return (ph==PHASE_DAMAGE or ph==PHASE_DAMAGE_CAL)
      and Duel.GetAttacker()==e:GetHandler() and Duel.GetAttackTarget()==nil
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.postg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local sg=Duel.GetMatchingGroup(Card.IsFaceup,tp,LOCATION_MZONE,LOCATION_MZONE,e:GetHandler())
	Duel.SetOperationInfo(0,CATEGORY_POSITION,sg,sg:GetCount(),0,0)
end

function s.posop(e,tp,eg,ep,ev,re,r,rp)
	local sg=Duel.GetMatchingGroup(Card.IsFaceup,tp,LOCATION_MZONE,LOCATION_MZONE,e:GetHandler())
	if sg:GetCount()>0 then
		Duel.ChangePosition(sg,POS_FACEDOWN_DEFENSE,0,POS_FACEDOWN_DEFENSE,0)
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.diccost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetAttackAnnouncedCount()==0 end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_OATH)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	e:GetHandler():RegisterEffect(e1)
end

function s.dictg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_DICE,nil,0,tp,1)
	local g1=Duel.GetMatchingGroup(aux.TRUE,tp,0,LOCATION_MZONE,nil)
	local g2=Duel.GetMatchingGroup(aux.TRUE,tp,LOCATION_MZONE,0,nil)
	if g1:GetCount()~=0 and g2:GetCount()~=0 then
		g1:Merge(g2)
		Duel.SetOperationInfo(0,CATEGORY_DESTROY,g1,1,0,0)
	end
end

function s.dicop(e,tp,eg,ep,ev,re,r,rp)
	local d=Duel.TossDice(tp,1)
	if d==1 or d==2 or d==3 then
		local g=Duel.GetMatchingGroup(aux.TRUE,tp,0,LOCATION_ONFIELD,e:GetHandler())
		Duel.Destroy(g,REASON_RULE)
	elseif d==6 then
		local g=Duel.GetMatchingGroup(aux.TRUE,tp,LOCATION_ONFIELD,0,e:GetHandler())
		Duel.Destroy(g,REASON_RULE)
	else
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
		local g=Duel.GetMatchingGroup(aux.TRUE,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,e:GetHandler())
		Duel.Destroy(g,REASON_RULE)
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.sucop(e,tp,eg,ep,ev,re,r,rp)
	Duel.SetLP(tp,999999)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_LEAVE_FIELD)
	e1:SetOperation(s.lose)
	e:GetHandler():RegisterEffect(e1)
end

function s.lose(e,tp,eg,ep,ev,re,r,rp)
	Duel.Win(1-tp,0x4)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.damval(e,re,val,r,rp,rc)
	if bit.band(r,REASON_EFFECT)~=0 then return 0 end
	return val
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.dc(e)
	local tp=e:GetHandler():GetControler()
	if Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)>0 then
		return 1
		else
		return 0
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.drcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()==1-tp and Duel.GetCurrentPhase()==PHASE_DRAW
end

function s.filter(c)
	return c:IsSetCard(0x40) and c:IsType(TYPE_MONSTER)
end

function s.drcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return ep==1-tp and eg:IsExists(s.filter,1,nil) end
	local g=eg:Filter(s.filter,nil)
	if g:GetCount()==1 then
		Duel.ConfirmCards(tp,g)
		Duel.ShuffleHand(1-tp)
		e:SetLabelObject(g:GetFirst())
	else
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CONFIRM)
		local sg=g:Select(1-tp,1,1,nil)
		Duel.ConfirmCards(tp,sg)
		Duel.ShuffleHand(1-tp)
		e:SetLabelObject(sg:GetFirst())
	end
end

function s.drtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local tc=e:GetLabelObject()
	tc:CreateEffectRelation(e)
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,tc,1,0,0)
end

function s.drop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetLabelObject()
	if not e:GetHandler():IsRelateToEffect(e) or not tc:IsRelateToEffect(e) then return end
	Duel.Remove(tc,POS_FACEUP,REASON_EFFECT)
end
