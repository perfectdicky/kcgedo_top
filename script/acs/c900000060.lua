-- 门之守护神S
local s, id = GetID()
function s.initial_effect(c)
	c:EnableCounterPermit(0x222)
    c:EnableReviveLimit()
    Fusion.AddProcMix(c, true, true, 25955164, 62340868, 98434877)
    Fusion.AddContactProc(c, s.contactfil, s.contactop, s.splimit)

    -- 融合特召限制
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
    e1:SetCode(EFFECT_SPSUMMON_CONDITION)
    e1:SetValue(s.splimit)
    c:RegisterEffect(e1)

    -- 融合特召规则
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_FIELD)
    e2:SetCode(EFFECT_SPSUMMON_PROC)
    e2:SetProperty(EFFECT_FLAG_UNCOPYABLE)
    e2:SetRange(LOCATION_EXTRA)
    e2:SetCondition(s.spcon)
    e2:SetOperation(s.spop)
    c:RegisterEffect(e2)

    -- 特召成功增加守护魔神指示物
    local e3 = Effect.CreateEffect(c)
    e3:SetDescription(aux.Stringid(900000060, 2))
    e3:SetCategory(CATEGORY_COUNTER)
    e3:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_F)
    e3:SetCode(EVENT_SPSUMMON_SUCCESS)
    e3:SetTarget(s.addct)
    e3:SetOperation(s.addc)
    c:RegisterEffect(e3)

    -- 每个守护魔神指示物增加攻击和攻击次数
    local e4 = Effect.CreateEffect(c)
    e4:SetType(EFFECT_TYPE_SINGLE)
    e4:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e4:SetRange(LOCATION_MZONE)
    e4:SetCode(EFFECT_UPDATE_ATTACK)
    e4:SetValue(s.attackup)
    c:RegisterEffect(e4)
    local e5 = e4:Clone()
    e5:SetCode(EFFECT_EXTRA_ATTACK)
    e5:SetValue(s.val)
    c:RegisterEffect(e5)

    -- 被破坏时去除守护魔神指示物代替
    local e6 = Effect.CreateEffect(c)
    e6:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_SINGLE)
    e6:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e6:SetCode(EFFECT_DESTROY_REPLACE)
    e6:SetRange(LOCATION_MZONE)
    e6:SetTarget(s.reptg)
    c:RegisterEffect(e6)

    -- 1回合1次不被战斗破坏
    local e7 = Effect.CreateEffect(c)
    e7:SetType(EFFECT_TYPE_SINGLE)
    e7:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e7:SetRange(LOCATION_MZONE)
    e7:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
    e7:SetCountLimit(1)
    e7:SetValue(s.valcon)
    c:RegisterEffect(e7)

    -- 效果不会被无效化
    local e8 = Effect.CreateEffect(c)
    e8:SetType(EFFECT_TYPE_SINGLE)
    e8:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE)
    e8:SetRange(LOCATION_MZONE)
    e8:SetCode(EFFECT_CANNOT_DISABLE)
    e8:SetValue(1)
    c:RegisterEffect(e8)
end
s.listed_names={25955164,62340868,98434877}
-----------------------------------------------------------------------------------------
function s.contactfil(tp)
    return Duel.GetMatchingGroup(Card.IsAbleToRemoveAsCost, tp, LOCATION_ONFIELD, 0, nil)
end
function s.contactop(g, tp)
    Duel.ConfirmCards(1 - tp, g)
    Duel.Remove(g, POS_FACEUP, REASON_COST + REASON_MATERIAL)
end
function s.splimit(e, se, sp, st)
    return not e:GetHandler():IsLocation(LOCATION_EXTRA)
end
--------------------------------------------------
function s.splimit(e, se, sp, st)
    return e:GetHandler():GetLocation() ~= LOCATION_EXTRA
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.spfilter(c, code)
    return c:IsFaceup() and c:IsCode(code)
end

function s.spcon(e, c)
    if c == nil then
        return true
    end
    local tp = c:GetControler()
    return Duel.GetLocationCount(tp, LOCATION_MZONE) > -3 and
               Duel.IsExistingMatchingCard(s.spfilter, tp, LOCATION_ONFIELD, 0, 1, nil, 25955164) and
               Duel.IsExistingMatchingCard(s.spfilter, tp, LOCATION_ONFIELD, 0, 1, nil, 62340868) and
               Duel.IsExistingMatchingCard(s.spfilter, tp, LOCATION_ONFIELD, 0, 1, nil, 98434877)
end

function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
    local g1 = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_ONFIELD, 0, 1, 1, nil, 25955164)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
    local g2 = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_ONFIELD, 0, 1, 1, nil, 62340868)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
    local g3 = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_ONFIELD, 0, 1, 1, nil, 98434877)
    Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
    g1:Merge(g2)
    g1:Merge(g3)
    Duel.Remove(g1, POS_FACEUP, REASON_COST)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.addct(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return true
    end
    Duel.SetOperationInfo(0, CATEGORY_COUNTER, nil, 1, 0, 0x2222)
end

function s.addc(e, tp, eg, ep, ev, re, r, rp)
    if e:GetHandler():IsRelateToEffect(e) then
        e:GetHandler():AddCounter(0x222, 2)
    end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.attackup(e, c)
    return c:GetCounter(0x222) * 1250
end

function s.val(e, c)
    return c:GetCounter(0x222)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.reptg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():IsReason(REASON_EFFECT + REASON_BATTLE + REASON_RULE) and
                   e:GetHandler():IsCanRemoveCounter(tp, 0x222, 1, REASON_COST)
    end
    e:GetHandler():RemoveCounter(tp, 0x222, 1, REASON_EFFECT)
    return true
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.valcon(e, re, r, rp)
    return bit.band(r, REASON_BATTLE) ~= 0
end
