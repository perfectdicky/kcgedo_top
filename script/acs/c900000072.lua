--真红眼黑龙S
local s,id=GetID()
function s.initial_effect(c)
	aux.EnableGeminiAttribute(c)
	
	local e13=Effect.CreateEffect(c)
	e13:SetDescription(aux.Stringid(8491961,1))
	e13:SetCategory(CATEGORY_TOHAND)
	e13:SetType(EFFECT_TYPE_IGNITION)
	e13:SetRange(LOCATION_MZONE)
	e13:SetCountLimit(1)
	e13:SetCondition(aux.IsGeminiState)
	e13:SetTarget(s.thtg)
	e13:SetOperation(s.thop)
	c:RegisterEffect(e13)

	--给与对方2400点伤害
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetDescription(aux.Stringid(900000072,2))
	-- e1:SetCategory(CATEGORY_DAMAGE)
	-- e1:SetType(EFFECT_TYPE_IGNITION)
	-- e1:SetRange(LOCATION_MZONE)
	-- e1:SetCountLimit(1)
	-- e1:SetCondition(aux.IsGeminiState)
	-- e1:SetCost(s.cost)
	-- e1:SetTarget(s.target)
	-- e1:SetOperation(s.operation)
	-- c:RegisterEffect(e1)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.thfilter(c)
	return c:IsType(TYPE_SPELL+TYPE_TRAP) and c:IsAbleToHand() and c:IsSetCard(0x3b)
end
function s.thtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(s.thfilter,tp,LOCATION_GRAVE+LOCATION_DECK,0,1,nil) end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_GRAVE+LOCATION_DECK)
end
function s.thop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,s.thfilter,tp,LOCATION_GRAVE+LOCATION_DECK,0,1,1,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,tp,REASON_EFFECT)
	end
end

function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetAttackAnnouncedCount()==0 end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_OATH)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	e:GetHandler():RegisterEffect(e1)
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetTargetPlayer(1-tp)
	Duel.SetTargetParam(2400)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,2400)
end

function s.operation(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Damage(p,d,REASON_EFFECT)
end
