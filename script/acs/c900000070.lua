--青眼白龙S
local s,id=GetID()
function s.initial_effect(c)
	aux.EnableGeminiAttribute(c)
	
	--破坏对方场上所有怪兽
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetDescription(aux.Stringid(900000070,2))
	-- e1:SetCategory(CATEGORY_DESTROY)
	-- e1:SetType(EFFECT_TYPE_IGNITION)
	-- e1:SetRange(LOCATION_MZONE)
	-- e1:SetCountLimit(1)
	-- e1:SetCondition(aux.IsGeminiState)
	-- e1:SetCost(s.cost)
	-- e1:SetTarget(s.target)
	-- e1:SetOperation(s.operation)
	-- c:RegisterEffect(e1)

	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_IMMUNE_EFFECT)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCondition(aux.IsGeminiState)
	e1:SetValue(s.efilter)
	c:RegisterEffect(e1)	
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.efilter(e,re)
	return e:GetHandlerPlayer()~=re:GetHandlerPlayer() and re:IsActiveType(TYPE_SPELL)
end

function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetAttackAnnouncedCount()==0 end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_OATH)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	e:GetHandler():RegisterEffect(e1)
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(aux.TRUE,tp,0,LOCATION_MZONE,1,nil) end
	local sg=Duel.GetMatchingGroup(aux.TRUE,tp,0,LOCATION_MZONE,nil)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,sg,sg:GetCount(),0,0)
end

function s.operation(e,tp,eg,ep,ev,re,r,rp)
	local sg=Duel.GetMatchingGroup(aux.TRUE,tp,0,LOCATION_MZONE,nil)
	Duel.Destroy(sg,REASON_EFFECT)
end
