--千年天秤
function c900000092.initial_effect(c)
	
	--发动效果
	local e1=Effect.CreateEffect(c)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	c:RegisterEffect(e1)
	
	--投硬币破坏怪兽
	--local e2=Effect.CreateEffect(c)
	--e2:SetDescription(aux.Stringid(900000092,2))
	--e2:SetProperty(EFFECT_FLAG_CARD_TARGET)
	--e2:SetCategory(CATEGORY_DESTROY+CATEGORY_COIN)
	--e2:SetType(EFFECT_TYPE_IGNITION)
	--e2:SetCountLimit(1)
	--e2:SetRange(LOCATION_SZONE)
	--e2:SetTarget(c900000092.destg)
	--e2:SetOperation(c900000092.desop)
	--c:RegisterEffect(e2)

	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(13719,4))
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_FUSION_SUMMON)
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetCountLimit(1)
	e2:SetRange(LOCATION_SZONE)
    --e2:SetCondition(c900000092.con)
	e2:SetTarget(Fusion.SummonEffTG(nil))
	e2:SetOperation(Fusion.SummonEffOP(nil))
	c:RegisterEffect(e2)
    --   local e21=e2:Clone()
    --   e21:SetDescription(aux.Stringid(13719,7))
    --   e21:SetCondition(c900000092.racon)
	-- e21:SetTarget(c900000092.ratg)
	-- e21:SetOperation(c900000092.raop)
	-- c:RegisterEffect(e21)
	-- local e22=Effect.CreateEffect(c)
    --   e22:SetDescription(aux.Stringid(13719,15))
	-- e22:SetType(EFFECT_TYPE_IGNITION)
	-- e22:SetCountLimit(1)
	-- e22:SetRange(LOCATION_SZONE)
    --   e22:SetCondition(c900000092.racon2)
	-- e22:SetTarget(c900000092.ratg2)
	-- e22:SetOperation(c900000092.raop2)
	-- c:RegisterEffect(e22)

	--效果不会被无效化
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_CANNOT_DISABLE)
	c:RegisterEffect(e3)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000092.destg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(1-tp) and chkaux.TRUE end
	if chk==0 then return Duel.IsExistingTarget(aux.TRUE,tp,0,LOCATION_MZONE,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	local g=Duel.SelectTarget(tp,aux.TRUE,tp,0,LOCATION_MZONE,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_COIN,nil,0,tp,1)
end

function c900000092.desop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc and tc:IsRelateToEffect(e) then
		local c1=Duel.TossCoin(tp,1)
		if c1<1 then return end
		Duel.Destroy(tc,REASON_EFFECT)
	end
end

function c900000092.con(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(900000092)==0
end
function c900000092.filter1(c,e)
	return c:IsCanBeFusionMaterial() and not c:IsImmuneToEffect(e)
end
function c900000092.filter2(c,e,tp,m,f,chkf)
	return c:IsType(TYPE_FUSION) and (not f or f(c))
		and c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,false,false) and c:CheckFusionMaterial(m,nil,chkf)
end
function c900000092.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then
		local chkf=Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and PLAYER_NONE or tp
		local mg1=Duel.GetMatchingGroup(c900000092.filter1,tp,LOCATION_HAND+LOCATION_MZONE,0,nil,e)
		local res=Duel.IsExistingMatchingCard(c900000092.filter2,tp,LOCATION_EXTRA,0,1,nil,e,tp,mg1,nil,chkf)
		if not res then
			local ce=Duel.GetChainMaterial(tp)
			if ce~=nil then
				local fgroup=ce:GetTarget()
				local mg2=fgroup(ce,e,tp)
				local mf=ce:GetValue()
				res=Duel.IsExistingMatchingCard(c900000092.filter2,tp,LOCATION_EXTRA,0,1,nil,e,tp,mg2,mf,chkf)
			end
		end
		return res
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
      e:GetHandler():RegisterFlagEffect(900000092,RESET_PHASE+PHASE_END,0,1)
end
function c900000092.activate(e,tp,eg,ep,ev,re,r,rp)
	local chkf=Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and PLAYER_NONE or tp
	local mg1=Duel.GetMatchingGroup(c900000092.filter1,tp,LOCATION_HAND+LOCATION_MZONE,0,nil,e)
	local sg1=Duel.GetMatchingGroup(c900000092.filter2,tp,LOCATION_EXTRA,0,nil,e,tp,mg1,nil,chkf)
	local mg2=nil
	local sg2=nil
	local ce=Duel.GetChainMaterial(tp)
	if ce~=nil then
		local fgroup=ce:GetTarget()
		mg2=fgroup(ce,e,tp)
		local mf=ce:GetValue()
		sg2=Duel.GetMatchingGroup(c900000092.filter2,tp,LOCATION_EXTRA,0,nil,e,tp,mg2,mf,chkf)
	end
	if sg1:GetCount()>0 or (sg2~=nil and sg2:GetCount()>0) then
		local sg=sg1:Clone()
		if sg2 then sg:Merge(sg2) end
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
		local tg=sg:Select(tp,1,1,nil)
		local tc=tg:GetFirst()
		if sg1:IsContains(tc) and (sg2==nil or not sg2:IsContains(tc) or not Duel.SelectYesNo(tp,ce:GetDescription())) then
			local mat1=Duel.SelectFusionMaterial(tp,tc,mg1,nil,chkf)
			tc:SetMaterial(mat1)
			Duel.SendtoGrave(mat1,REASON_EFFECT+REASON_MATERIAL+REASON_FUSION)
			Duel.BreakEffect()
			Duel.SpecialSummon(tc,SUMMON_TYPE_FUSION,tp,tp,false,false,POS_FACEUP)
		else
			local mat2=Duel.SelectFusionMaterial(tp,tc,mg2,nil,chkf)
			local fop=ce:GetOperation()
			fop(ce,e,tp,tc,mat2)
		end
		tc:CompleteProcedure()
	end
end

function c900000092.racon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(900000092)==0
end
function c900000092.rafilter(c,e,tp)
	return c:GetOriginalCode()==900000082 and not c:IsImmuneToEffect(e)
             and Duel.IsExistingTarget(c900000092.rafilter2,tp,LOCATION_MZONE,0,1,c,e)
end
function c900000092.rafilter2(c,e)
	return c:IsType(TYPE_MONSTER) and c:IsType(TYPE_EFFECT) and not c:IsImmuneToEffect(e)
end
function c900000092.ratg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(tp) and c900000092.rafilter(chkc,e,tp) end
	if chk==0 then return Duel.IsExistingTarget(c900000092.rafilter,tp,LOCATION_MZONE,0,1,nil,e,tp) end
      Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	local g=Duel.SelectTarget(tp,c900000092.rafilter,tp,LOCATION_MZONE,0,1,1,nil,e,tp) 
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_EXTRA)
      e:GetHandler():RegisterFlagEffect(900000092,RESET_PHASE+PHASE_END,0,1)
end
function c900000092.filter22(c,e,tp)
	return c:IsCanBeSpecialSummoned(e,SUMMON_TYPE_FUSION,tp,true,false) and c:GetOriginalCode()==136
end
function c900000092.raop(e,tp,eg,ep,ev,re,r,rp)
      local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if not (tc:IsRelateToEffect(e) and tc:IsFaceup()) then return end
	if tc then
      Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
      local g2=Duel.SelectTarget(tp,c900000092.rafilter2,tp,LOCATION_MZONE,0,1,4,tc,e)
      Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
      local g3=Duel.GetFirstMatchingCard(c900000092.filter22,tp,LOCATION_EXTRA,0,nil,e,tp)
      local gc=g2:GetFirst()
      local atk=tc:GetAttack()
      local def=tc:GetDefense()
      local code=tc:GetOriginalCode()
      local type=tc:GetType()
      local race=tc:GetRace()
      local att=tc:GetAttribute()
      local lev=0
      local rank=0
      if not tc:IsType(TYPE_XYZ) then 
      lev=tc:GetLevel() end
      if tc:IsType(TYPE_XYZ) then 
      lev=tc:GetRank() end
      g2:Merge(Group.FromCards(tc))
      g3:SetMaterial(g2)
      Duel.SendtoGrave(g2,REASON_EFFECT+REASON_MATERIAL+REASON_FUSION)
      Duel.BreakEffect()
      Duel.SpecialSummonStep(g3,SUMMON_TYPE_FUSION,tp,tp,true,false,POS_FACEUP_DEFENSE)
	local e0=Effect.CreateEffect(tc)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SET_DEFENSE)
	--e0:SetReset(RESET_EVENT+0x1fe0000)
      e0:SetValue(def)
	g3:RegisterEffect(e0)
 	local e01=Effect.CreateEffect(tc)
	e01=e0:Clone()
	e01:SetCode(EFFECT_SET_ATTACK)
      e01:SetValue(atk)
	g3:RegisterEffect(e01)      
 	local e1=Effect.CreateEffect(tc)
	e1=e0:Clone()
	e1:SetCode(EFFECT_CHANGE_CODE)
      e1:SetValue(code)
	g3:RegisterEffect(e1) 
      local e2=Effect.CreateEffect(tc)
      e2=e0:Clone()
      e2:SetCode(EFFECT_CHANGE_TYPE)
      e2:SetValue(type)
      g3:RegisterEffect(e2) 
      local e3=Effect.CreateEffect(tc)
      e3=e0:Clone()
      e3:SetCode(EFFECT_CHANGE_RACE)
      e3:SetValue(race)
      g3:RegisterEffect(e3) 
      local e4=Effect.CreateEffect(tc)
      e4=e0:Clone()
      e4:SetCode(EFFECT_CHANGE_ATTRIBUTE)
      e4:SetValue(att)
      g3:RegisterEffect(e4) 
      if not tc:IsType(TYPE_XYZ) then
      local e5=Effect.CreateEffect(tc)
      e5=e0:Clone()
      e5:SetCode(EFFECT_CHANGE_LEVEL)
      e5:SetValue(lev)
      g3:RegisterEffect(e5) end
      if tc:IsType(TYPE_XYZ) then
      local e6=Effect.CreateEffect(tc)
      e6=e0:Clone()
      e6:SetCode(EFFECT_CHANGE_RANK)
      e6:SetValue(rank)
      g3:RegisterEffect(e6) end
      while gc do
            local code=gc:GetOriginalCode()
		--local code2=gc:GetOriginalCode()
            --if code2==89312388 or code2==140 or code2==170000151 or code2==170000152 or code2==170000153 or code2==26905245 or code2==900000086
               --or code2==5373478 or code2==23893227 or code2==26439287 or code2==59281922 then
            --code=code2 end
      g3:CopyEffect(code,0,0)
      --g3:RegisterFlagEffect(tp,136,0,0,0)
      gc=g2:GetNext() end 
      Duel.SpecialSummonComplete()
      g3:CompleteProcedure() 
	end
end

function c900000092.racon2(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(900000092)==0
end
function c900000092.rafilter3(c,e,tp)
	return c:GetOriginalCode()==900000099 and not c:IsImmuneToEffect(e)
             and Duel.IsExistingTarget(c900000092.rafilter4,tp,LOCATION_MZONE,0,1,c,e)
end
function c900000092.rafilter4(c,e)
	return c:IsType(TYPE_MONSTER) and not c:IsImmuneToEffect(e)
end
function c900000092.ratg2(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(tp) and c900000092.rafilter3(chkc,e,tp) end
	if chk==0 then return Duel.IsExistingTarget(c900000092.rafilter3,tp,LOCATION_MZONE,0,1,nil,e,tp) end
      Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET) 
	local g=Duel.SelectTarget(tp,c900000092.rafilter3,tp,LOCATION_MZONE,0,1,1,nil,e,tp) 
      e:GetHandler():RegisterFlagEffect(900000092,RESET_PHASE+PHASE_END,0,1)
end
function c900000092.raop2(e,tp,eg,ep,ev,re,r,rp)
      local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if not (tc:IsRelateToEffect(e) and tc:IsFaceup()) then return end
      Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
      local g2=Duel.SelectTarget(tp,c900000092.rafilter4,tp,LOCATION_MZONE,0,1,4,tc,e)
      local gc=g2:GetFirst()
      local tatk=0
      while gc do
      local atk=tc:GetAttack()
      if atk<0 then atk=0 end
      tatk=tatk+atk
            local code=gc:GetOriginalCode()
		--local code2=gc:GetOriginalCode()
            --if code2==89312388 or code2==140 or code2==170000151 or code2==170000152 or code2==170000153 or code2==26905245 or code2==900000086
               --or code2==5373478 or code2==23893227 or code2==26439287 or code2==59281922 then
            --code=code2 end
      tc:CopyEffect(code,0,0)
      gc=g2:GetNext() end
      Duel.SendtoGrave(g2,REASON_EFFECT)
	if tc then
	local e0=Effect.CreateEffect(tc)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_UPDATE_ATTACK)
      e0:SetValue(tatk)
	tc:RegisterEffect(e0)
	end
end
