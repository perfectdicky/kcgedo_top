--黑魔导少女S
local s, id = GetID()
function s.initial_effect(c)
	aux.EnableGeminiAttribute(c)

	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(aux.IsGeminiState)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1)

	--墓地存在黑魔术师攻击力上升
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetType(EFFECT_TYPE_SINGLE)
	-- e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	-- e1:SetCode(EFFECT_UPDATE_ATTACK)
	-- e1:SetRange(LOCATION_MZONE)
	-- e1:SetValue(s.val)
	-- c:RegisterEffect(e1)
	
	--特召手牌和卡组的黑魔术师
	-- local e2=Effect.CreateEffect(c)
	-- e2:SetDescription(aux.Stringid(900000074,2))
	-- e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	-- e2:SetType(EFFECT_TYPE_IGNITION)
	-- e2:SetRange(LOCATION_MZONE)
	-- e2:SetCountLimit(1)
	-- e2:SetTarget(s.sptg)
	-- e2:SetOperation(s.spop)
	-- c:RegisterEffect(e2)
	
	-- --场上的黑魔术师攻击力上升
	-- local e3=Effect.CreateEffect(c)
	-- e3:SetDescription(aux.Stringid(900000074,2))
	-- e3:SetProperty(EFFECT_FLAG_CARD_TARGET)
	-- e3:SetCategory(CATEGORY_ATKCHANGE)
	-- e3:SetType(EFFECT_TYPE_QUICK_O)
	-- e3:SetCode(EVENT_FREE_CHAIN)
	-- e3:SetHintTiming(TIMING_BATTLE_PHASE)
	-- e3:SetRange(LOCATION_MZONE)
	-- e3:SetCountLimit(1)
	-- e3:SetCondition(s.atkcon)
	-- e3:SetCost(s.atkcost)
	-- e3:SetTarget(s.atktg)
	-- e3:SetOperation(s.atkop)
	-- c:RegisterEffect(e3)
end
-----------------------------------------------------------------------------------------
function s.operation(e,tp,eg,ep,ev,re,r,rp)
	e:GetHandler():SetEntityCode(105, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, true)
end
--------------------------------------------------
function s.atkfilter(c)
	local code=c:GetCode()
	return code==46986414 or code==30208479
end

function s.val(e,c)
	return Duel.GetMatchingGroupCount(s.atkfilter,c:GetControler(),LOCATION_GRAVE,LOCATION_GRAVE,nil)*500
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.spfilter(c)
	return c:IsCode(46986414)
end

function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>-2
		and Duel.IsExistingMatchingCard(s.spfilter,tp,LOCATION_HAND+LOCATION_DECK,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_DECK+LOCATION_HAND)
end

function s.spop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,s.spfilter,tp,LOCATION_DECK+LOCATION_HAND,0,1,1,nil,e,tp)
	if g:GetCount()>0 then
		Duel.SpecialSummon(g,0,tp,tp,false,false,POS_FACEUP)
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.atkcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetCurrentPhase()==PHASE_BATTLE
end

function s.atkcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():GetAttackAnnouncedCount()==0 end
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_OATH)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
	e:GetHandler():RegisterEffect(e1)
end

function s.filter(c)
	return c:IsFaceup() and c:IsCode(46986414)
end

function s.atktg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(tp) and s.filter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(s.filter,tp,LOCATION_MZONE,0,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
	Duel.SelectTarget(tp,s.filter,tp,LOCATION_MZONE,0,1,1,nil)
end

function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if tc and tc:IsFaceup() and tc:IsRelateToEffect(e) then
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(2000)
		e1:SetReset(RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e1)
	end
end
