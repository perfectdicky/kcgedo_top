--人造人-念力震慑者
function c900000075.initial_effect(c)
	
	--召唤成功时破坏场上所有陷阱
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetOperation(c900000075.activate)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
	c:RegisterEffect(e2)
	local e3=e1:Clone()
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e3)
	
	--破坏放置的陷阱
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e4:SetCode(EVENT_SSET)
	e4:SetRange(LOCATION_MZONE)
	e4:SetTarget(c900000075.target)
	e4:SetOperation(c900000075.operation)
	c:RegisterEffect(e4)
	
	--无效场上的陷阱
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetCode(EFFECT_CANNOT_TRIGGER)
	e5:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e5:SetRange(LOCATION_MZONE)
	e5:SetTargetRange(0xa,0xa)
	e5:SetTarget(c900000075.distg)
	c:RegisterEffect(e5)
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD)
	e6:SetCode(EFFECT_DISABLE)
	e6:SetRange(LOCATION_MZONE)
	e6:SetTargetRange(LOCATION_SZONE,LOCATION_SZONE)
	e6:SetTarget(c900000075.distg)
	c:RegisterEffect(e6)
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_FIELD)
	e7:SetCode(EFFECT_DISABLE_TRAPMONSTER)
	e7:SetRange(LOCATION_MZONE)
	e7:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
	e7:SetTarget(c900000075.distg)
	c:RegisterEffect(e7)
	
	--陷阱卡不能发动
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e8:SetCode(EVENT_CHAIN_SOLVING)
	e8:SetRange(LOCATION_MZONE)
	e8:SetOperation(c900000075.disop)
	c:RegisterEffect(e8)
	
	--效果不会被无效化
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_SINGLE)
	e9:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e9:SetRange(LOCATION_MZONE)
	e9:SetCode(EFFECT_CANNOT_DISABLE)
	e9:SetValue(1)
	c:RegisterEffect(e9)
	end
-------------------------------------------------------------------------------------------------------------------------------------------
	function c900000075.filter(c)
	return c:IsType(TYPE_TRAP) and aux.TRUE
end

function c900000075.activate(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(Card.IsFacedown,tp,LOCATION_SZONE,LOCATION_SZONE,nil)
	if g:GetCount()>0 then
		Duel.ConfirmCards(tp,g)
		local sg=Duel.GetMatchingGroup(c900000075.filter,tp,LOCATION_SZONE,LOCATION_SZONE,nil)
		Duel.Destroy(sg,REASON_EFFECT)
	end
	local tg=Duel.GetMatchingGroup(c900000075.filter,tp,LOCATION_SZONE,LOCATION_SZONE,nil)
	Duel.Destroy(tg,REASON_EFFECT)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000075.filter2(c,tp)
	return c:IsFacedown()
end

function c900000075.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:IsExists(c900000075.filter2,1,nil,tp) end
	Duel.SetTargetCard(eg)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,eg,eg:GetCount(),0,0)
end

function c900000075.filter3(c,e)
	return c:IsFacedown() and c:IsType(TYPE_TRAP) and c:IsRelateToEffect(e)
end

function c900000075.operation(e,tp,eg,ep,ev,re,r,rp)
	Duel.ConfirmCards(tp,eg)
	local g=eg:Filter(c900000075.filter3,nil,e,tp)
	Duel.Destroy(g,REASON_EFFECT)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000075.distg(e,c)
	return c:IsType(TYPE_TRAP)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function c900000075.disop(e,tp,eg,ep,ev,re,r,rp)
	local tl=Duel.GetChainInfo(ev,CHAININFO_TRIGGERING_LOCATION)
	if tl==LOCATION_SZONE and re:IsActiveType(TYPE_TRAP) then
		Duel.NegateEffect(ev)
		if re:GetHandler():IsRelateToEffect(re) then
		Duel.Destroy(re:GetHandler(),REASON_EFFECT)
		end
	end
end
