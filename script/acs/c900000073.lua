--青眼究极龙S
local s,id=GetID()
function s.initial_effect(c)
	c:EnableReviveLimit()
	c:EnableCounterPermit(0x223)
	Fusion.AddProcMixN(c, true, true, CARD_BLUEEYES_W_DRAGON, 3)
	
	--融合特召限制
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(s.splimit)
	c:RegisterEffect(e1)
	
	--特召成功增加龙首指示物
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(900000073,2))
	e2:SetCategory(CATEGORY_COUNTER)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e2:SetCode(EVENT_SPSUMMON_SUCCESS)
	e2:SetTarget(s.addct)
	e2:SetOperation(s.addc)
	c:RegisterEffect(e2)
	
	--每个龙首指示物增加攻击和攻击次数
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetValue(s.attackup)
	c:RegisterEffect(e3)
	local e4=e3:Clone()
	e4:SetCode(EFFECT_EXTRA_ATTACK)
	e4:SetValue(s.val)
	c:RegisterEffect(e4)
	
	--被破坏时去除龙首指示物代替
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)
	e5:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e5:SetCode(EFFECT_DESTROY_REPLACE)
	e5:SetRange(LOCATION_MZONE)
	e5:SetTarget(s.reptg)
	c:RegisterEffect(e5)
	
	--效果不会被无效化
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE)
	e6:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCode(EFFECT_CANNOT_DISABLE)
	e6:SetValue(1)
	c:RegisterEffect(e6)
end
s.material_setcode=0xdd
-------------------------------------------------------------------------------------------------------------------------------------------
function s.splimit(e,se,sp,st)
	return not e:GetHandler():IsLocation(LOCATION_EXTRA) or bit.band(st,SUMMON_TYPE_FUSION)==SUMMON_TYPE_FUSION
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.addct(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_COUNTER,nil,1,0,0x223)
end

function s.addc(e,tp,eg,ep,ev,re,r,rp)
	if e:GetHandler():IsRelateToEffect(e) then
		e:GetHandler():AddCounter(0x223,2)
	end
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.attackup(e,c)
	return c:GetCounter(0x223)*1500
end

function s.val(e,c)
	return c:GetCounter(0x223)
end
-------------------------------------------------------------------------------------------------------------------------------------------
function s.reptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsReason(REASON_EFFECT+REASON_BATTLE+REASON_RULE)
	and e:GetHandler():IsCanRemoveCounter(tp,0x223,1,REASON_COST) end
	e:GetHandler():RemoveCounter(tp,0x223,1,REASON_EFFECT)
	return true
end
